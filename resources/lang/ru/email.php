
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Email messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Activate your website account'   => 'Активация учетной записи на сайте :Website',
    'To activate the account' => "Для активации аккаунта пройдите ",
    'follow this link' => 'по этой ссылке',

    'Your login' => 'Ваш логин',
    'Your password' => 'Ваш пароль',

    'Message sent from website' => 'Письмо отправлено с сайта :Website',
    'After activating your account' => 'После активации аккаунта можете перейти',
    'To create a new password' => 'Для создания нового пароля пройдите ',




    '' => '',
    '' => '',
    'subjects' => [
        'Password reset' => 'Сброс пароля',
        'Account activation' => 'Активация аккаунта',

        '' => '',
        '' => '',
        '' => '',
        '' => '',
    ],



];