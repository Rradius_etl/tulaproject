<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'users'                        => 'Пользователи',
    'dashboard'                        => 'Панель управления',
    'home'                        => 'Главная',
    'homepage'                        => 'Главная',
    'login'                        => 'Авторизация',
    'register'                        => 'Регистрация',
    'news'                        => 'Новости',
    'about'                        => 'О портале',
    'contacts'                        => 'Контакты',
    'new-consultation'                        => 'Заявка на телемедицинскую клиническую консультацию',
    'messages'                        => 'Сообщения',
    'messages.unread'                        => 'Непрочитанные сообщения',
    'messages.outgoing'                        => 'Исходящие сообщения',
    ''                        => '',
    ''                        => '',
    ''                        => '',

];
