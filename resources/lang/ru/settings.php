<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'mail'                        => 'Отправка почты',

    'mail_driver'      =>      'Драйвер',
    'mail_encryption'      =>      'Шифрование',
    'mail_host'      =>      'Хост',
    'mail_port'      =>      'Порт отправки почты',
    'mail_username'      =>      'Имя пользователя для авторизации',
    'mail_password'      =>      'Пароль для авторизации в почтовом сервере',
    'mail_email'      =>      'От имени какой почты отправлять email (Желательно сделать одинаковым с именем пользователя)',
    // website-settings
    'website-settings'                        => 'Основные настройки сайта',
    'website-settings_slider-autoplay'                        => 'Автоматический прокручивать слайдер',
    'website-settings_slider-time'                        => 'Время показа одного слайда (в секундах)',
    'website-settings_site-name'                        => 'Название сайта',
    'website-settings_meta-description' => 'Мета тэг description',
    'website-settings_meta-keywords' => 'Мета тэг keywords',
    'website-settings_welcome-text' => 'Текст приветствия на главной странице',
    'website-settings_welcome-enabled' => 'Показывать текст приветствия на главной странице',
    // scheme
    'color-scheme'                        => 'Цветовая схема типов видеоконференции',
    'color-scheme_consultation'                        => 'Консультация',
    'color-scheme_meeting'                        => 'Совещание',
    'color-scheme_seminar'                        => 'Семинар',
    ''                        => '',
    ''                        => '',
    ''                        => '',


];
