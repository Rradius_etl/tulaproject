<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Your account has been created' => 'Ваш аккаунт создан. Проверьте Email для активации.',
    'Your account has been created but dont activate' => 'Ваш аккаунт создан. Для активации обратитесь к администратору портала.',
    'Account activated' => 'Аккаунт активирован.',
    'failed' => 'Имя пользователя и пароль не совпадают.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',
    'User not found' => 'Пользователь с таким E-Mail в системе не найден.',
    'User does not exists' => 'Такого пользователя не существует.',

    'message not sent' => 'Ошибка отправки письма.',
    'activation message not sent' => 'Ошибка отправки письма активации.',
    'wrong credentials' => 'Неправильный логин или пароль',
    'account locked' => 'Количество попыток ввести верный пароль исчерпано. Блокировка на :delay секунд.',
    'same email exists' => 'Такой Email уже зарегистрирован.',
    'Invalid or expired activation code' => 'Неверный или просроченный код активации.',
    'Account is not activated' => 'Для активации аккаунта обратитесь к администратору портала',
    'Account is not activated. Message sent again' => 'Ваш аккаунт не ативирован! Поищите в своем почтовом ящике письмо со ссылкой для активации (Вам отправлено повторное письмо). ',
    'Failed to register' => 'Ошибка регистрации нового пользователя',
    'Password reset successfully' => 'Пароль сброшен.',
    'Invalid or expired password reset code' => 'Неверный или просроченный код сброса пароля.',
    '' => '',
    '' => '',
    '' => '',

];