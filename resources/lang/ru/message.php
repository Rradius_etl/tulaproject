<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logged'                  => 'Вы успешно вошли!',
    'someproblems'            => 'There were some problems with your input.',
    'siginsession'            => 'Sign in to start your session',
    'remember'                => 'Remember Me',
    'buttonsign'              => 'Sign In',
    'forgotpassword'          => 'I forgot my password',
    'registermember'          => 'Register a new membership',
    'terms'                   => 'I agree to the terms',
    'conditions'              => 'Terms and conditions',
    'register'                => 'Register',
    'login'                   => 'Login',
    'membreship'              => 'I already have a membership',
    'passwordclickreset'      => 'Click here to reset your password:',
    'signGithub'              => 'Sign in using Github',
    'signFacebook'            => 'Sign in using Facebook',
    'signTwitter'             => 'Sign in using Twitter',
    'signGoogle+'             => 'Sign in using Google+',
    'sendpassword'            => 'Send Password Reset Link',
    'passwordreset'           => 'Reset password',
    'pagenotfound'            => 'Page not found',
    '404error'                => '404 Error Page',
    'notfindpage'             => 'We could not find the page you were looking for.',
    'mainwhile'               => 'Meanwhile, you may',
    'returndashboard'         => 'return to dashboard',
    'usingsearch'             => 'or try using the search form.',
    'search'                  => 'Search',
    'servererror'             => 'Server Error',
    '500error'                => '500 Error Page',
    'somethingwrong'          => 'Something went wrong.',
    'wewillwork'              => 'We will work on fixing that right away.',
    'serviceunavailable'      => 'Service unavailable',
    '503error'                => '503 Error Page',

    'createdby'               => 'Created by',

    'online'                  => 'Online',
    'home'                    => 'Home',
    

    'viewall'                 => 'View all',

    'friends'                 => 'Friends',
    'profile'                 => 'Profile',
    'signout'                 => 'Sign out',

    'address'                 => 'Address',

    'yourname'                => 'Your Name',
    'emailaddress'            => 'Email Address',
    'enteremail'              => 'Enter Email',
    'yourtext'                => 'Your Text',
    'submit'                  => 'SUBMIT',
    'email'                   => 'Email',
    'password'                => 'Password',
    'retrypepassword'         => 'Retype password',
    'fullname'                => 'Full Name',
    'errors'                => [
        'wrong credentials' => 'Не правильный логин или пароль'
    ],


];
