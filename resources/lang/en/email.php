
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Email messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Activate your website account'   => 'Activate your :Website website account',
    'To activate the account' => "To activate the account ",
    'follow this link' => 'follow this link',

    'Your login' => 'Your login',
    'Your password' => 'Your password',

    'Message sent from website' => 'Message sent from :Website website  ',
    'After activating your account' => 'After activating your account ',
    'To create a new password' => 'To create a new password ',

    '' => '',
    '' => '',
    'subjects' => [
        'Password reset' => 'Password reset',
        'Account activation' => 'Account activation',

        '' => '',
        '' => '',
        '' => '',
        '' => '',
    ],

];