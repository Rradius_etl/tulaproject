<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Your account has been created' => 'Your account has been created. Check your Email to complete your activation.',
    'Account activated' => 'Account is activated.',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'User not found' => 'User with this E-Mail is not found in the system.',
    'User does not exists' => 'User does not exists.',

    'message not sent' => 'Message sent error.',
    'activation message not sent' => 'Activation message sent error.',
    'wrong credentials' => 'Incorrect login and password.',
    'account locked' => 'Your account has been suspended for :delay seconds',
    'same email exists' => 'This Email is already registered.',
    'Invalid or expired activation code' => 'Invalid or expired activation code.',
    'Account is not activated. Message sent again' => 'Account is not activated! Look in your inbox an email with an activation link. (You will be sent a further letter).',
    'Failed to register' => 'New user registration error',
    'Password reset successfully' => 'Password reset successfully.',
    'Invalid or expired password reset code' => 'Invalid or expired password reset code.',
    '' => '',



];
