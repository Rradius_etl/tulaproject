/**
 * Backend.js on 13/06/2016.
 */
$(function() {
    var pgurl = window.location.href.replace('#','');
    console.log('url=' + pgurl)
    $(".sidebar-menu li").each(function(){
        if($(this).find('a').attr("href") == pgurl || $(this).find('a').attr("href") == '' )
            $(this).addClass("active");
        else
            $(this).removeClass("active");

    });

    
    // HOWTO: To use toggle functionality make hidden input after checkbox with class "toggle" with attribute 'data-id' of input field name
    $('input[type=checkbox].toggle').change(function() {
        var id = $(this).data('id')
        console.log( 'id', id )
        if( $(this).is(':checked')) {
            $('[name="'+id+'"]').val(1)
        } else {
            $('[name="'+id+'"]').val(0)
        }
    })

});
