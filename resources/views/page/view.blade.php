@extends('layouts.main')

@section('content')
@section('breadcrumbs', Breadcrumbs::render('page', $page))

@section('contentheader_title', $page->title)
<div class="box box-white">
    <div class="box-header">

    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <article>
            <div>
                <img src="{{$page->image('large')}}" style="max-width: 320px; float: left" alt="">
                {!! $page->content !!}
            </div>

            <div class="clearfix"></div>
        </article>


    </div>
    <!-- /.box-body -->
    <script src="//yastatic.net/share2/share.js"></script>
</div>

@endsection

