@extends('layouts.app')

@section('htmlheader_title', 'Панель Администратора')


@section('contentheader_title' , 'Панель Администратора')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Приветствуем</div>

                    <div class="panel-body">
                        {{ trans('adminlte_lang::message.logged') }}
                    </div>
                </div>
            </div>

            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </div>
@endsection
