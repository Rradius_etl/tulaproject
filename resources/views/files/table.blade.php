<table class="table table-responsive" id="files-table">
    <thead>
    <th width="54px"></th>
    @include('layouts.partials.universal-filter', ['fields' => [

      ["file_name"=>false],
      ["link"=>false],
      ["downloads_count"=>true],

    ],"route"=>"files.uploaded"])


        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($files as $file)
        <tr>
            <?php
            $ext_img = 'images/file-formats/' .$file->extension. '.png';
            if(!File::exists($ext_img)) { $ext_img = 'images/file-formats/_blank.png'; } ?>
            <td  ><img src="/{{$ext_img}}" alt="{{$file->original_name}}"></td>
            <td><a target="_blank" href="{{route('files.get', $file->id)}}"> {{$file->original_name}}</a></td>
            <td><a download target="_blank" class="btn btn-xs btn-primary" href="@if($file->is_public == false){{route('files.get', $file->id)}} @else /{{$file->file_path}} @endif"> <i class="fa fa-download"></i>&nbsp; Скачать </a></td>
            <td>{!! $file->downloads_count !!}</td>
            <td>
                {!! Form::button('<i class="fa fa-link"></i> Доступ по ссылке', ['type' => 'submit', 'title' => 'Доступ по ссылке', 'class' => 'btn btn-success btn-xs', 'onclick' => "showPermissionModal({$file->id})"]) !!}

                {!! Form::open(['route' => ['admin.files.destroy', $file->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Удалить', ['type' => 'submit', 'title' => 'Удалить', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $files])
@include('files.permissions')
<style>
    td> img {
        max-width: 100%;
    }
</style>