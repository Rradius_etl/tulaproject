@extends('layouts.main')
<?php $title =  'Файлы в открытом доступе' ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('common.errors')
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary box-white">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-12">

                     &nbsp;
                    </div>
                    <div class="col-sm-4 pull-right">
                        <form action="">
                            <label for=""></label>
                            <div class="input-group">

                                <input type="text" name="search" class="form-control" placeholder="Расширенный поиск" value="{{Input::get('search'), ''}}" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">

                @include('files.open-table')

            </div>
        </div>
    </div>
@endsection

