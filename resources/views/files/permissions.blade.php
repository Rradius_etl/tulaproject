<script src="/js/clipboard.min.js"></script>
<div id="permissions-modal-dialog" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" style="width: 340px" role="document">


        <div class="box box-white  col-xs-12 col-sm-3">

            <!-- /.box-header -->
            <div class="">
                <div class="pull-right">
                    <!-- Rounded switch -->
                    <label class="switch">
                        <input type="checkbox" id="permission-status" onchange="saveChanges(this)" >
                        <div class="slider round"></div>
                    </label>
                </div>
                <div style="margin-bottom: 12px;">Доступ для неавторизованных </div>

                {!! Former::open_vertical()->id('file-permission-form')->method('GET') !!}
                <span id="permission-message"></span>

                <div class="input-group download-link-group"> <input id="download-link-input" class="form-control" >
                    <span data-placement="left" data-toggle="tooltip" title="Скопировать в буфер обмена"  class="input-group-btn"> <button data-clipboard-target="#download-link-input"   class="clipboard btn btn-default" type="button"><i class="fa fa-clipboard"></i></button> </span> </div>

                {!! Former::close() !!}

                <script>
                    $(document).ready(function () {
                        var clipboard = new Clipboard('.clipboard');

                    })
                    function showPermissionModal(file_id) {
                        $.get('/files/' + file_id + '/permissions', function (data) {
                            console.log('data', data);

                            $('#permission-status').data('file', file_id);
                            // Заполнить значения
                            fillModalData(data);
                            // Забиндить


                            $('#permissions-modal-dialog').modal('show');
                        })
                    }
                    function saveChanges(el) {
                        setTimeout(function () {
                            var $this = $(el), file_id = $this.data('file');
                            console.info('SAVE CHANGES')
                            if($this.is(":checked")) {
                                $.get('/files/' + file_id + '/permissions/on', function (data) {
                                    console.log('data', data)
                                    fillModalData(data);

                                })
                            } else {
                                $.get('/files/' + file_id + '/permissions/off', function (data) {
                                    console.log('data', data)
                                    fillModalData(data);

                                })
                            }
                        },200)


                    }
                    function fillModalData(data) {
                        $('#permission-status').prop('checked', data.state);
                        $('#permission-message').html(data.message);
                        if( data.state ) {
                            $('#download-link-input').val(data.url);
                            $('.download-link-group').show();
                        } else {
                            $('#download-link-input').val('');
                            $('.download-link-group').hide()
                        }
                    }

                </script>
                <small>* Позволяет дать доступ для скачивания файлов неавторизованным пользователям </small>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>

<style>
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 18px;
        margin-top: 5px;
    }

    /* Hide default HTML checkbox */
    .switch input {display:none;}

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 22px;
        width: 22px;
        left: 0;
        bottom: -1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.4);
        -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.4);
        box-shadow: 0 1px 3px rgba(0,0,0,0.4);
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(22px);
        -ms-transform: translateX(22px);
        transform: translateX(22px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 16px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>


