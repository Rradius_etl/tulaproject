<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $consultPattern->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{{ $consultPattern->name }}</p>
</div>

<!-- Telemed Center Id Field -->
<div class="form-group">
    {!! Form::label('telemed_center_id', trans('backend.Telemed Center Id') . ':' ) !!}
    <p>{{ $consultPattern->telemed_center_id }}</p>
</div>

<!-- Medical Profile Field -->
<div class="form-group">
    {!! Form::label('medical_profile', trans('backend.Medical Profile') . ':' ) !!}
    <p>{{ $consultPattern->medical_profile}}</p>
</div>

<!-- Doctor Fullname Field -->
<div class="form-group">
    {!! Form::label('doctor_fullname', trans('backend.Doctor Fullname') . ':' ) !!}
    <p>{{ $consultPattern->doctor_fullname }}</p>
</div>

<!-- Doctor Spec Field -->
<div class="form-group">
    {!! Form::label('doctor_spec', trans('backend.Doctor Spec') . ':' ) !!}
    <p>{{ $consultPattern->doctor_spec }}</p>
</div>

<!-- Consult Type Field -->
<div class="form-group">
    {!! Form::label('consult_type', trans('backend.Consult Type') . ':' ) !!}
    <p>{{ $consultPattern->consultationType()->name }}</p>
</div>

<!-- Med Care Fac Id Field -->
<div class="form-group">
    {!! Form::label('med_care_fac_id', trans('backend.Med Care Fac Id') . ':' ) !!}
    <p>{{ $consultPattern->med_care_fac_id }}</p>
</div>

<!-- Desired Consultant Field -->
<div class="form-group">
    {!! Form::label('desired_consultant', trans('backend.Desired Consultant') . ':' ) !!}
    <p>{{ $consultPattern->desired_consultant }}</p>
</div>

<!-- Desired Date Field -->
<div class="form-group">
    {!! Form::label('desired_date', trans('backend.Desired Date') . ':' ) !!}
    <p>{{ $consultPattern->desired_date }}</p>
</div>

<!-- Desired Time Field -->
<div class="form-group">
    {!! Form::label('desired_time', trans('backend.Desired Time') . ':' ) !!}
    <p>{{ $consultPattern->desired_time }}</p>
</div>

<!-- Patient Uid Or Fname Field -->
<div class="form-group">
    {!! Form::label('patient_uid_or_fname', trans('backend.Patient Uid Or Fname') . ':' ) !!}
    <p>{{ $consultPattern->patient_uid_or_fname}}</p>
</div>

<!-- Patient Birthday Field -->
<div class="form-group">
    {!! Form::label('patient_birthday', trans('backend.Patient Birthday') . ':' ) !!}
    <p>{{ $consultPattern->patient_birthday}}</p>
</div>

<!-- Patient Gender Field -->
<div class="form-group">
    {!! Form::label('patient_gender', trans('backend.Patient Gender') . ':' ) !!}
    <p>{{ $consultPattern->patient_gender }}</p>
</div>

<!-- Patient Address Field -->
<div class="form-group">
    {!! Form::label('patient_address', trans('backend.Patient Address') . ':' ) !!}
    <p>{{ $consultPattern->patient_address }}</p>
</div>

<!-- Patient Social Status Field -->
<div class="form-group">
    {!! Form::label('patient_social_status', trans('backend.Patient Social Status') . ':' ) !!}
    <p>{{ $consultPattern->patient_social_status }}</p>
</div>

<!-- Patient Indications For Use Field -->
<div class="form-group">
    {!! Form::label('patient_indications_for_use', trans('backend.Patient Indications For Use') . ':' ) !!}
    <p>{{ $consultPattern->patient_indications_for_use}}</p>
</div>

<!-- Patient Indications For Use Other Field -->
<div class="form-group">
    {!! Form::label('patient_indications_for_use_other', trans('backend.Patient Indications For Use Other') . ':' ) !!}
    <p>{{ $consultPattern->patient_indications_for_use_other }}</p>
</div>

<!-- Patient Questions Field -->
<div class="form-group">
    {!! Form::label('patient_questions', trans('backend.Patient Questions') . ':' ) !!}
    <p>{{ $consultPattern->patient_questions }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $consultPattern->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $consultPattern->updated_at }}</p>
</div>

