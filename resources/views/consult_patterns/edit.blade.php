@extends('layouts.main')

@section('content')
    <?php $title = trans('backend.ConsultPattern') . ' &#187; ' . trans('backend.editing_existed');
    ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
<script src="{{ asset('plugins/jquery/jquery-2.1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<!-- datetimepicker -->
<script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
@section('content')
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row">
                    {!!    Former::vertical_open()
                          ->route('consult-patterns.update', $consultPattern->id)
                          ->id('consult-requres-form')
                          ->secure()
                          ->method('patch')  !!}
                    @include('consult_patterns.fields')
                    <div class="form-group">
                        {!!  Former::large_primary_submit('Сохранить') !!}

                    </div>
                    {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection



@section('additional-scripts')
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <style>
        .input-group input[type=file] {
            width: 100%;
            background: #dbecf6;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('input[type=file]').parent().find('.btn').click(function () {
                $('input[type=file]')[0].click();
            })

            $('#desired_time_group').datetimepicker({
                format: 'HH:mm'
            });
            $('#pattern-toggle').click(function () {
                $('.pattern-templates').toggle();
            })

            // Init Selectize
            $('#med_care_fac_id').selectize();
            $('#patient_gender').selectize();
            $('#consult_type').selectize();
            $('#medical_profile').selectize();
            $('#patient_social_status').selectize();



            if ($("#patient_indications_for_use_id").val() == "other") {
                $("#patient_indications_for_use_other").parent().show();
                $("#patient_indications_for_use_other").show();
                $("#patient_indications_for_use_other").prop("disabled", false);
            }
            else {
                $("#patient_indications_for_use_other").hide();
                $("#patient_indications_for_use_other").prop("disabled", true);
                $("#patient_indications_for_use_other").parent().hide();
            }
            $("#patient_indications_for_use_id").change(function () {
                if ($("#patient_indications_for_use_id").val() == "other") {
                    $("#patient_indications_for_use_other").parent().show();
                    $("#patient_indications_for_use_other").show();
                    $("#patient_indications_for_use_other").prop("disabled", false);
                }
                else {
                    $("#patient_indications_for_use_other").hide();
                    $("#patient_indications_for_use_other").prop("disabled", true);
                    $("#patient_indications_for_use_other").parent().hide();
                }
            });

            $('#patient_birthday_group').datetimepicker({
                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: '01-01-1800'
            });


            $('.btn[id^=reset]').click(function () {
                var selector = '#' + this.id.replace('reset_', '');
                console.log(selector);

                $(selector).val('');

            })


        });
    </script>

@endsection
