<!-- Name Field -->
{!! Former::text('name') !!}

<?php
$array = [];
$tmcs = TmcRoleManager::getTmcs();
foreach ($tmcs as $tm) {
    $array[$tm['id']] = $tm['name'];
}
?>


{!!  Former::texts('doctor_fullname')
      ->class('form-control')
      ->value('')
      ->required()  !!}


{!!  Former::texts('doctor_spec') ->class('form-control') !!}

{!!  Former::select('med_care_fac_id')->options($mcfs)->class('form-control')->label('Выбор ЛПУ-консультанта')->required() !!}

{!!  Former::texts('desired_consultant') ->class('form-control') !!}


<div class="form-group">
    {!! Former::label('desired_time') !!}
    {!!  Former::group()->Class('input-group date')->id('desired_time_group')  !!}

    {!!  Former::text('desired_time') ->class('form-control')!!}
    <span class="input-group-addon date">
                    <i class="fa fa-clock-o text-primary"></i>
                </span>
    {!! Former::closeGroup()   !!}
</div>
<div class="form-group ">
    <div class="btn btn-danger btn-sm pull-right" id="reset_desired_time">
        Сбросить {{trans('validation.attributes.desired_time')}}</div>
    <div class="clearfix"></div>
</div>

{!!  Former::select('consult_type')->options($consult_types->prepend('-- Выберите из списка --',null), null)->class('form-control')->required() !!}

{!!  Former::select('medical_profile')->options($medical_profiles)->class('form-control')->required() !!}

{!!  Former::text('patient_uid_or_fname') ->class('form-control')->required() !!}

{!!  Former::select('patient_gender')->options($genders, 'male')->class('form-control')->required() !!}

{!!  Former::select('patient_social_status')->options($socstatuses) ->class('form-control') !!}

{!!  Former::texts('patient_address') ->class('form-control') !!}

{!!  Former::select('patient_indications_for_use_id')->options($ind_for_use)->class('form-control')->required() !!}
{!!  Former::texts('patient_indications_for_use_other') ->class('form-control')->setAttribute('style', 'display:none')->disabled() !!}
{!!  Former::texts('patient_questions') ->class('form-control') !!}


<div class="clearfix"></div>

