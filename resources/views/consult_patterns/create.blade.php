@extends('layouts.main')

@section('content')
<?php $title = trans('backend.ConsultPattern') . ' &#187; ' . trans('backend.adding_new'); ?>
<script src="{{ asset('plugins/jquery/jquery-2.1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<!-- datetimepicker -->
<script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                {!!    Former::vertical_open()
                ->route('consult-patterns.store')
                ->id('consult-requres-form')
                ->secure()
                ->rules([
                    '' => 'required'
                ])
                ->method('POST')  !!}

                        @include('consult_patterns.fields')
                    <div class="form-group">
                        {!!  Former::large_primary_submit('Сохранить') !!}

                    </div>
                    {!! Former::close() !!}
                    <script>
                        $(document).ready(function () {
                            $('input[name=desired_time]').datetimepicker({
                                format: 'HH:mm'
                            });


                            if ($("#patient_indications_for_use_id").val() == "other") {
                                $("#patient_indications_for_use_other").parent().show();
                                $("#patient_indications_for_use_other").show();
                                $("#patient_indications_for_use_other").prop("disabled", false);
                            }
                            else {
                                $("#patient_indications_for_use_other").hide();
                                $("#patient_indications_for_use_other").prop("disabled", true);
                                $("#patient_indications_for_use_other").parent().hide();
                            }
                            $("#patient_indications_for_use_id").change(function () {
                                if ($("#patient_indications_for_use_id").val() == "other") {
                                    $("#patient_indications_for_use_other").parent().show();
                                    $("#patient_indications_for_use_other").show();
                                    $("#patient_indications_for_use_other").prop("disabled", false);
                                }
                                else {
                                    $("#patient_indications_for_use_other").hide();
                                    $("#patient_indications_for_use_other").prop("disabled", true);
                                    $("#patient_indications_for_use_other").parent().hide();
                                }
                            });
                            $('input[name=desired_date]').datetimepicker({

                                locale: 'ru',
                                format: 'DD.MM.YYYY',
                                minDate: moment()
                            });

                            $('input[name=patient_birthday]').datetimepicker({
                                locale: 'ru',
                                format: 'DD.MM.YYYY',
                                minDate: '01-01-1800'
                            });


                            $('.btn[id^=reset]').click(function () {
                                var selector = '#' + this.id.replace('reset_', '');
                                console.log(selector);

                                $(selector).val('');

                            })


                        });</script>
                </div>
            </div>
        </div>
    </div>
@endsection
