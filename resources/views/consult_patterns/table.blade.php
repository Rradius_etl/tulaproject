<table class="table table-responsive" id="consultPatterns-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'consultPatterns', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'consultPatterns', 'field'=>'created_at', 'name' => trans('backend.Created At')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($consultPatterns as $consultPattern)
        <tr>
            <td>{{ $consultPattern->name }}</td>
            <td>{{ $consultPattern->created_at }}</td>
            <td>
                {!! Form::open(['route' => ['consult-patterns.destroy', $consultPattern->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('consult-patterns.edit', [$consultPattern->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>