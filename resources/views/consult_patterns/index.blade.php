@extends('layouts.cabinet')
<?php $title = trans('backend.ConsultPatterns'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('consult-patterns.create') !!}">
                       <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                   </a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    @include('consult_patterns.table')
                </div>
                @include('common.paginate', ['records' => $consultPatterns])
            </div>
        </div>
    </div>
@endsection

