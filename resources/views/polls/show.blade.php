@extends('layouts.cabinet')
<?php $title = trans('backend.Poll') . ' "' . $poll->title . '"'; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
<?php $arr = ["нет", "да"];  $class = ['danger', 'success']?>
@section('content')
    <script>
        $(document).ready(function(){
            var csrf = '{{ csrf_token() }}';
            var poll_id = {{$poll->id}};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': csrf
                }
            });
            $('#sendbutton').click(function(){
                event.preventDefault();
                var variant = $('[name="variant"]:checked').val();
                if(variant !==undefined)
                {
                    $.ajax({
                        url: "/send-answer",
                        data: {poll_id:poll_id,poll_row_id:variant},
                        type: 'POST',
                        success: function (data) {
                            if(data.success)
                            {
                                $('#content').load('/public_polls/'+poll_id+"?need_result");
                                $('#sendbutton').remove();
                                $('#panel-footer').append('<a href="#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-bell"></span> Вы уже ответили</a>');
                                alert("Ваш голос был учтен!");

                            }
                        }
                    })
                }
                else
                {
                    alert("Выберите вариант ответа");
                }
            });
        });
    </script>
    <div class="content">
        <div class="panel panel-default" style="width:60%; margin:0px auto 50px auto;">
            <div class="panel-heading">
                <div class="createopr"></div>
                <div class="form-group" style="width:50%;">
                    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                    <p>{!! $poll->created_at !!}</p>
                </div>

                <strong>Тема опроса </strong> {{$poll->title}}

            </div>

            <div class="panel-body">



                    <div class="form-group" style="display:none;">
                        {!! Form::label('author_id', trans('backend.Author Id') . ':' ) !!}
                        <p>{!! $poll->user->getfName() !!}</p>
                    </div>
                    <!-- Active Field -->
                    <div class="form-group" style="display:none;">
                        {!! Form::label('active', trans('backend.Active') . ':' ) !!}
                        <p><span class="label label-{{$class[$poll->active]}}">{{$arr[$poll->active]}}</span></p>
                    </div>

                    @include("polls.showresults",['poll'=>$poll,'state'=>true])
                    <a href="/public_polls/{{$poll->id}}" class="pull-right">Ссылка на опрос</a>

                <div class="panel-footer">
                    @if($poll->active==0)
                        <div class="alert alert-warning"> Опубликуйте опрос, чтобы люди могли проголосовать. <br>Ссылка на опрос недействительна, пока опрос не опубликован</div>
                    @else
                    @endif
                    <a href="{{route('polls.my')}}" class="btn btn-round btn-danger podtv">Назад в мои опросы</a>

                </div>
            </div>


        </div>


    </div>
@endsection