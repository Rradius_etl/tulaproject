<?php $arr = ["нет", "да"];
$classes = ["label label-danger", "label label-success"] ?>
<table class="table table-responsive" id="polls-table">
    <thead>

    @include('layouts.partials.universal-filter', ['fields' => [

      ["title"=>true],
      ["author_id"=>false],
      ["active"=>true],
      ['telemed_center_id'=>true,'name'=>'ТМЦ'],
      ["finished_at"=>true],

    ],"route"=>"polls.all"])

    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($polls as $poll)
        <tr>
            <td>{{ $poll->title }}</td>
            <td>{{ $poll->user->email }}</td>
            <td><span class="{{$classes[$poll->active]}}"> {!! $arr[$poll->active] !!} </span></td>
            @if( $poll->telemedCenter == null )
                <td><span class="label label-danger">ТМЦ удален</span></td>
            @else
                <td>{{ $poll->telemedCenter->name }}</td>
                @endif

            <td>{{ $poll->finished_at }}</td>
            <td>
                <div class='btn-group'>
                    <a href="/public_polls/{{$poll->id}}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>