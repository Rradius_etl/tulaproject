<div id="modal_dialog_add" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" role="document">

        <div class="imess"></div>
        <div class="box box-white  col-xs-12 col-sm-3">
            <div class="box-header text-center">
                <h2>Создать опрос</h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                $myTmcsOptions = [];
                foreach (TmcRoleManager::getTmcs() as $item) {
                    $myTmcsOptions[$item['id'] ] =  $item['name'];
                } ;

                ?>
                {!! Former::open()->route('polls.add')->method('POST') !!}

                <div class="form-group">
                    {!! Form::label('tmc_from_id', "Выберите ТМЦ от имени которого хотите создать опрос:", ['class' => 'edlabelmes']) !!}
                    {{ Form::select('tmc_from_id',  $myTmcsOptions, count($myTmcsOptions) > 0 ? array_first($myTmcsOptions) : '', ['class' => 'form-control']) }}
                </div>

                    <div class="text-center">
                        <a href="#" id="next-step-link" class="btn btn-primary link-to-next-step">&nbsp; &nbsp; Продолжить &nbsp; &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i> <i class="fa fa-angle-right" aria-hidden="true"></i> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>

                {!! Former::close() !!}


                <script>
                    $(document).ready(function () {

                        $a = $("#next-step-link");

                        var defaultValue = $("select[name=tmc_from_id] option:first").val();
                        if( typeof  defaultValue  == 'undefined') {

                        } else {
                            $a.attr('href', '/polls/create/' + defaultValue);
                        }


                        $("select[name=tmc_from_id]").change(function () {
                            var val = $(this).val();
                            console.log(val);
                            $a.attr('href', '/polls/create/' + val);
                        });

                    });
                </script>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>


