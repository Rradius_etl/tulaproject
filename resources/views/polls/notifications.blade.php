@extends('layouts.cabinet')
<?php $title = trans('backend.Notifications') . ' об опросах'; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-white">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-right">

                            <a class="btn btn-primary" id="mark-button" href="#">
                                <i class="fa fa-check-square-o"></i> Отметить как прочитанные </a>

                            <a class="btn btn-primary" href="{{route('notifications.index')}}">
                                <i class="fa fa-refresh"></i> Обновить</a>
                        </div>
                        <ul class="nav nav-tabs">
                            <li role="presentation"><a href="{{route('polls.my')}}">Опросы</a></li>
                            <li role="presentation"><a href="{{route('polls.all')}}">Все опросы</a></li>
                            <li role="presentation"><a href="{{route('polls.archive.index')}}">Архив</a> </li>
                            <li role="presentation"><a href="{{route('polls.notifications.unread')}}">Непрочитанные Уведомления</a></li>
                            <li role="presentation"  class="active"><a href="{{route('polls.notifications.index')}}">Уведомления</a></li>

                        </ul>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <form action='' id="notifications-form">
                            <label for=""></label>
                            <div class="input-group">

                                <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск"
                                       aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @if(count($notifications) > 0)
                <div class="box-body table-responsive" id="notifications">
                    @include('notifications.table')
                </div>
            @else
                <div class="col-sm-12" ><div class="alert alert-info" role="alert">У вас нет уведомлений</div></div>
                <div class="clearfix"></div>
            @endif
        </div>
    </div>

    <script>

        $(document).ready(function () {

            var csrf = '{{ csrf_token() }}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': csrf
                }
            });

            var current_page = {{$notifications->currentPage()}};
            $('#mark-button').click(function (evt) {
                evt.preventDefault();
                var inputs = $('input[name^=data]:checked');
                if( inputs.length > 0) {
                    var data  =[];
                    inputs.each(function (idx, obj) {

                        data.push($(obj).data('id'));

                    });

                    $.ajax({
                        url: "/notifications/mark-as-read/allpolls",
                        data: { data:data, page: current_page },
                        type: 'POST',
                        success: function (resp) {
                            $('#notifications').html(resp);
                        }
                    })

                } else {
                    alert('Сперва выберите хотя бы одну запись');
                }

            });




        });

    </script>
@endsection

