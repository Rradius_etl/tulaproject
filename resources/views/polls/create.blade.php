@extends('layouts.cabinet')
<?php $title = trans('backend.Poll') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid" style="width:60%; margin:auto;">
            <div class="createopr"> </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['polls.store',$TelemedId]]) !!}

                        @include('polls.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
