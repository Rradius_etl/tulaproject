@extends('layouts.cabinet')
<?php $title = trans('backend.Poll') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid" style="width:60%; margin:auto;">
           <div class="createopr"> </div>
           <div class="box-body">
               <div class="row">
                   {!! Form::model($poll, ['route' => ['polls.update',$TelemedId ,$poll->id], 'method' => 'patch']) !!}

                        @include('polls.fields',['model'=>$poll])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection