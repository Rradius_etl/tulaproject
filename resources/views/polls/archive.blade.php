@extends('layouts.cabinet')
<?php $title = 'Архив опросов';
        $arr = ['Не опубликован', 'Опубликован']; $classes = ['label label-danger', 'label label-success']
?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

       @include('common.errors')

        <div class="clearfix"></div>

        <div class="box box-primary box-white">
            <div class="box-header">

                <div class="pull-right" >
                    <button class="btn btn-primary"  data-toggle="modal" data-target="#modal_dialog_notify" >Уведомить</button>
                    <button class="btn btn-success"  data-toggle="modal" data-target="#modal_dialog_add" > <i class="fa fa-plus"></i> &nbsp;Добавить опрос</button>
                </div>
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="{{route('polls.my',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Мои опросы</a></li>
                    <li role="presentation"><a href="{{route('polls.all',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Активные опросы</a></li>
                    <li role="presentation" class="active"><a href="{{route('polls.archive.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Архив</a> </li>
                    <li role="presentation"><a href="{{route('polls.notifications.unread')}}">Непрочитанные Уведомления</a></li>
                    <li role="presentation"><a href="{{route('polls.notifications.index')}}">Уведомления</a></li>
                </ul>
            </div>

            <div class="box-body">
                <form class="form-group" action="">
                    <div class="input-group col-sm-4 pull-right">

                        <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Поиск по теме" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                    </div>
                    <div class="input-group col-sm-4 pull-right">
                        {!! Form::select('organizator',[""=>'Поиск по организатору']+$tmclist,$organizator,['class'=>'form-control']) !!}

                    </div>
                    <div class="input-group col-sm-4 pull-right ">
                        <input class="form-control" type="text" name="daterange"><span class="input-group-addon for-date"><i class="fa fa-calendar text-primary"></i></span>
                    </div>
                    <div class="clearfix"></div>
                </form>
                @include('polls.table_my', ['is_archive' => true])

            </div>
            @include('common.paginate', ['records' => $polls])
        </div>
    </div>
    @include('polls.notify')
    @include('polls.add')
@section('additional-scripts')
    @include('partials.daterange-filter')
@stop
@endsection

