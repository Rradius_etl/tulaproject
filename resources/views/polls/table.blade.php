<?php $arr = ["нет", "да"];
$classes = ["label label-danger", "label label-success"] ?>
<table class="table table-responsive" id="polls-table">
    <thead>
    @include('layouts.partials.universal-filter', ['fields' => [["title"=>true],["email"=>false],["active"=>true],["finished_at"=>true]],"route"=>"polls.my"])
    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($polls as $poll)
        <tr>

            <td>{{ $poll->title }}</td>
            <td>{{ $poll->user->email }}</td>
            <td> <span class="{{$classes[$poll->active]}}">{{$arr[$poll->active]}}</span> </td>
            <td>{{ $poll->finished_at }}</td>
            <td>
                {!! Form::open(['route' => ['polls.destroy', $poll->telemed_center_id ,$poll->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('polls.show', [$poll->telemed_center_id,$poll->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('polls.edit', [$poll->telemed_center_id,$poll->id]) !!}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>