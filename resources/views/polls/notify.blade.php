<div id="modal_dialog_notify" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" role="document">

        <div class="imess"></div>
        <div class="box box-white  col-xs-12 col-sm-3">
            <div class="box-header text-center">
                <h2>Уведомить</h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <link rel="stylesheet" href="/plugins/select2/select2.css">
                <script src="/plugins/select2/select2.js"></script>
                <script src="/plugins/select2/i18n/ru.js"></script>
                <style>
                    .select2-dropdown {
                        z-index: 99999999;
                    }
                    .select2-container--default .select2-search__field, .select2-container--default .select2-search.select2-search--inline {
                        width: 100% !important;
                    }
                </style>
                {!! Former::open()->route('polls.notify')->method('POST') !!}


                <div class="form-group">
                    {!! Form::label('poll_id', "Выберите опрос" . ':', ['class' => 'edlabelmes']) !!}
                    <select name="poll_id" class="ed-controlmess js-example-responsive js-states form-control" style="width: 100%;" >
                    </select>
                </div>


                <div class="form-group">
                    {!! Form::label('recipients[]', "Выберите ТМЦ которые хотите уведомить" . ':', ['class' => 'edlabelmes']) !!}
                    <select name="recipients[]" class="ed-controlmess js-example-responsive js-states form-control"  multiple="multiple"  style="width: 100%;" >
                    </select>
                </div>


                <div class="form-group" >
                    {!! Former::checkbox('send_email')
                        ->text('Уведомить по e-mail')
                        ->label(false)
                        ->check() !!}
                </div>

                {!!  Former::actions()->large_round_primary_submit('Отправить уведомление') !!}

                {!! Former::close() !!}



                <script>
                    $(document).ready(function(){
                        $("select[name^=recipients]").select2({
                            language: "ru",
                            allowClear: true,
                            placeholder: "Выберите ТМЦ которые хотите уведомить",
                            ajax: {
                                url: "/findtmcs",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        search: params.term, // search term
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function (item) {
                                            return {
                                                text: item.name,
                                                id: item.id
                                            }
                                        })
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                            minimumInputLength: 2,
                        });

                        $("select[name=poll_id]").select2({
                            language: "ru",
                            allowClear: true,
                            placeholder: "Поиск опроса по вопросу",
                            ajax: {
                                url: "/findpolls",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        search: params.term, // search term
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function (item) {
                                            return {
                                                text: item.title,
                                                id: item.id
                                            }
                                        })
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                            minimumInputLength: 2,
                        });
                    });
                </script>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>