<style>
    .margin-top {
        margin-top: 100px;
    }
</style>
<!-- Title Field -->
<div class="margin-top">


    <div class="form-group  col-sm-12">

        {!! Form::label('title', 'Вопрос:',['class' => 'edlabelmes']) !!}
        {!! Form::text('title', null, ['class' => 'ed-controlmess']) !!}


    </div>

    <div class="form-group col-sm-12">

        {!! Form::label('end_time', 'Время окончания опроса:',['class' => 'edlabelmes']) !!}
        {!! Form::text('end_time', null, ['class' => 'ed-controlmess']) !!}
    </div>
    {!! Form::hidden('finished_at') !!}
    <div class="form-group col-sm-6">
        {!! Form::label('active', trans('backend.Active') . ':') !!} <br>
        {!! Form::checkbox('toggle', 0, isset($poll) ? $poll->active :  0, ['class' => 'form-control toggle', 'data-id' => 'active' ,'data-toggle' => 'toggle', 'data-on' => 'Опубликован', 'data-off' => 'Не опубликован', 'data-onstyle' => 'success' ]) !!}

    </div>
    {{ Form::hidden('active', isset($poll) ? $poll->active :  0 ) }}

    <div class="form-group col-sm-12">

        <div class="alert alert-warning">При удалении варианта ответа, также и удалятся все ответы пользователей на этот вариант ответа</div>
        @if(isset($model))
            <h5><b class="edlabelmes">Варианты: </b></h5>
            @foreach($model->pollRows as $row)
                <div class="thumbnail " data-id="{{$row->id}}">
                    {{$row->title}}
                    {!! Form::button('X',['class' => "deletebutton btn btn-sm btn-danger pull-right"]) !!}
                    <div class="clearfix"></div>
                </div>
            @endforeach
        @endif
        {!! Form::button('добавить вариант',['class' => "addbutton btn btn-sm btn-primary orta"]) !!}
    </div>
    <div id="newitems"></div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-round btn-primary']) !!}
        <a href="{!! route('polls.index',$TelemedId) !!}"
           class="btn btn-round btn-default">{{ trans('backend.cancel') }} </a>
    </div>
    <div style="display:none;" class="hiddenitems">

    </div>
</div>

<script>
    var i = 0;
    function softdelete(elem) {
        $(elem).parent().remove();
    }
    $(document).ready(function () {

        var defaultDate = moment().add(1, 'days');

        $('input[name=finished_at]').val(defaultDate.format('YYYY-MM-DD HH:mm:ss'))
        $('input[name=end_time]').datetimepicker({

            locale: 'ru',
            format: 'DD-MM-YYYY',
            minDate: defaultDate
        }).on('dp.change', function (el) {

            $('input[name=finished_at]').val(el.date.utc().endOf('day').format('YYYY-MM-DD HH:mm:ss'))
            
        });

        $('.deletebutton').click(function () {
            if ($(this).hasClass("btn-primary")) {
                $('[name="deleteditems[' + $(this).parent().data('id') + ']"]').remove();
                console.log($('[name="hiddenitems[' + $(this).parent().data('id') + ']"]'));
                $(this).removeClass("btn-primary");
                $(this).addClass("btn-danger");
                $(this).text("удалить");
            }
            else {
                $('.hiddenitems').append("<input type='hidden' name='deleteditems[" + $(this).parent().data('id') + "]'>");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-primary");
                $(this).text("отменить удаление");
            }
        });
        $('.addbutton').click(function () {
            $('#newitems').append('<div><div class="form-group col-sm-9"><input type="text" name="newitemname[' + i + ']" class="form-control"></div><button type="button" onclick="softdelete(this)" class="sofrdeltebutton btn btn-sm btn-danger pull-left">Удалить</button><div class="clearfix"></div></div>');
            i++;
        });
    });

</script>