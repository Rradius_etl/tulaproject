@extends('layouts.cabinet')
<?php $title = trans('backend.Polls') . ' телемедицинского центра ' . $tmc->name; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white box-solid">

            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  pull-right" style="" href="{!! route('polls.create',$TelemedId) !!}">
                                   <i class="fa fa-plus"></i>   Добавить опрос
                               </a>
                </div>
                <div class="col-sm-4 pull-right">
                    <form action="">

                        <div class="input-group">

                            <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                        <div class="input-group">
                            <input class="form-control" type="text" name="daterange"><span class="input-group-addon for-date"><i class="fa fa-calendar text-primary"></i></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body">
                    
                    @include('polls.table')
                    
            </div>
            @include('common.paginate', ['records' => $polls])
        </div>
    </div>
@endsection

@section('additional-scripts')
    @include('partials.daterange-filter')
@stop