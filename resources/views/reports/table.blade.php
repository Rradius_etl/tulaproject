<table class="table table-striped" id="reports-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'reports', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th> Автор </th>
        <th>{{ trans('backend.Tmcs') }}</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'reports', 'field'=>'type', 'name' => trans('backend.Type')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td>{!! $report->name !!}</td>
            <td>{!! $report->user->getfName() !!}</td>
            <td>
                <ul class="list-group">
                    <?php  $tmcs = $report->telemedcenters();?>
                    @foreach($tmcs as $key => $telemedcenter)

                        <li class="list-group-item">{{  $telemedcenter->name }}</li>
                        @if ($key == 5)
                            <li class="list-group-item label-default"> ..еще {{ count($tmcs) - 6 }} ТМЦ</li>
                            @break
                        @endif
                    @endforeach
                </ul>

            </td>
            <td>{!! $report->showReadableType() !!}</td>
            <td>

                {!! Form::open(['route' => ['reports.destroy', $report->id], 'method' => 'delete']) !!}
                <a href="{{route('reports.init.table', $report->id)}}" class="btn btn-block btn-default btn-xs" style="margin-bottom: 3px;"> <i class="fa fa-table"></i> Таблица</a>
                <a href="{{route('reports.init.graphics', $report->id)}}" class="btn btn-block btn-default btn-xs" style="margin-bottom: 3px;"><i class="fa fa-bar-chart-o"></i> График</a>

                @if($report->isCreatedByMcfAdmin())
                    <a href="{!! route('reports.show', [$report->id]) !!}" class='btn btn-block btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i> Просмотр</a>
                    <a href="{!! route('reports.edit', [$report->id]) !!}" class='btn btn-block btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i> Редактировать</a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Удалить', ['type' => 'submit', 'class' => 'btn btn-block  btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                @endif
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>