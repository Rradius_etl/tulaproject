@extends('layouts.app')
<?php $title = trans('backend.Report'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row" style="padding-left: 20px;padding-right: 20px;">
                    @include('reports.show_fields')
                    <a href="{!! route('reports.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
