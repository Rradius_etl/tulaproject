@extends('layouts.cabinet')
<?php $title = trans('backend.Report') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($report, ['route' => ['reports.update', $report->id], 'method' => 'patch']) !!}

                        @include('reports.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('scripts')
    <link rel="stylesheet" href="/bower_components/multiselect/css/multi-select.css">
    <style>
        body .ms-container {
            width: 100%;
        }
    </style>
    <script src="/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script>
        $('#rows').multiSelect({
            disabledClass: 'label-info',
            selectableHeader: "<div class='custom-header'>Доступные поля</div>",
            selectionHeader: "<div class='custom-header'>Выбранные поля</div>"
        })

        $('#tmcs').multiSelect({
            selectableOptgroup: true,
            disabledClass: 'label-info',
            selectableHeader: "<div class='custom-header'>Выберите ТМЦ</div>",
            selectionHeader: "<div class='custom-header'>Выбранные ТМЦ</div>"
        })
    </script>
@endsection