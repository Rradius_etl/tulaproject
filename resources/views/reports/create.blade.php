@extends('layouts.cabinet')
<?php $title = trans('backend.Report') . ' &#187; ' . trans('backend.adding_new');
$step = isset($step) ? $step : 1;
?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('common.errors')

        <div class="box box-primary box-solid">

            @include('admin.reports.steps-header', ['step' => $step])

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'reports.store']) !!}

                    @include('reports.create-step-'.$step)

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <link rel="stylesheet" href="/bower_components/multiselect/css/multi-select.css">
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <style>
        body .ms-container {
            width: 100%;
        }
    </style>
    <script src="/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <script>
        $('#rows').multiSelect({
            disabledClass: 'label-info',
            selectableHeader: "<div class='custom-header'>Доступные поля</div>",
            selectionHeader: "<div class='custom-header'>Выбранные поля</div>"
        })

        $('#tmcs').multiSelect({
            selectableOptgroup: true,
            disabledClass: 'label-info',
            selectableHeader: "<div class='custom-header'>Выберите ТМЦ</div>",
            selectionHeader: "<div class='custom-header'>Выбранные ТМЦ</div>"
        })

        $('#tmc_id').selectize({
            placeholder: 'Выберите пожалуйста'
        });
        // Нажатие на кнопку Выбрать все ТМЦ
        $('#selectAllTmcBtn').click(function () {
            $('#tmcs').multiSelect('select_all');
        })
        // Нажатие на кнопку Убать все ТМЦ
        $('#deselectAllTmcBtn').click(function () {
            $('#tmcs').multiSelect('deselect_all');
        })
    </script>
@endsection