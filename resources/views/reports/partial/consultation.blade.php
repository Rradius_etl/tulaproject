@if( $full )
    @extends('layouts.report')
@endif
@section('content')
    <?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
    @if(count($reports) > 0)
    <table class="table table-striped" id="polls-table">
        @include('reports.partial.header')
        <tbody>
        <?php $i = 1;   $mcfs = []; ?>

            @foreach($reports as $rKey => $report)
                <tr>
                    @if(in_array('num', $headers))
                        <td>{{$i}}</td>
                        <?php $i++; ?>
                    @endif

                    @foreach($headers as $header )
                        @if( in_array($header, $headers) && $header != 'num')
                        <!--  check for key exists -->
                            @if( array_key_exists($header, $report))

                                @if($header == 'tmc_name')
                                    @if(!array_key_exists($report->mcf_id,$mcfs))
                                        <?php
                                        $mcfs[$report->mcf_id]['count'] = 1;
                                        ?>
                                    @endif
                                    <td>{{$mcfs[$report->mcf_id]['count'].") ".$report->tmc_name}}</td>

                                    <?php
                                    $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;
                                    ?>
                                @else
                                    @if( $report->{$header} == 0 )
                                        <td class="text-center">{{$report->{$header} }}</td>
                                    @elseif($report->{$header} === null  )
                                        <td><span style="text-align: center">-</span></td>
                                    @else
                                        <td>{{$report->{$header} }}</td>
                                    @endif

                                @endif

                            @else
                                <td><span style="text-align: center">-</span></td>
                            @endif
                        <!-- end check for key exists -->

                        @endif
                    @endforeach
                </tr>
            @endforeach

        </tbody>
    </table>
    @else
        <div style="text-align: center;">
            <h3>Нет данных по вашим критериям поиска </h3>
        </div>
    @endif

@endsection