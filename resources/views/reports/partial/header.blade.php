<thead>
<tr>

    @if(in_array("num", $headers))
        <th>{{$allHeaders['num']}}</th>
    @endif

    @foreach($headers as $header)
        @if($header != 'num')

            <th>{{$allHeaders[$header]}}</th>
        @endif
    @endforeach
</tr>
</thead>