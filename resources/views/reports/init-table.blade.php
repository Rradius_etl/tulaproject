@extends('layouts.cabinet')
<?php $title = trans('backend.Report') . ": " . $report->name; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">

                @if(Sentinel::inRole('admin'))
                    <div class="pull-right">
                        <a class="btn btn-xs btn-default" href="{{route('admin.reports.index')}}">Перейти в конструктор
                            отчетов</a>
                        <a class="btn btn-xs btn-default" href="{{route('admin.reports.edit', $report->id)}}">Редактировать</a>
                        &nbsp;
                    </div>
                @endif
            </div>
            <div class="box-body">
                <div class="row" style="padding-left: 20px;padding-right: 20px;">

                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="{{ route('reports.init.table', $report->id) }}">Таблица</a>
                        </li>
                        <li role="presentation"><a href="{{ route('reports.init.graphics', $report->id) }}">График</a>
                        </li>
                    </ul>

                    <br>

                    <div class="page-header">
                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-4')
                        ->dataId('tmc_report')
                        ->dataUrl('/tmc_report/x')
                         ->label('Временной промежуток')
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::select('format')
                        ->addGroupClass('col-md-4')
                        ->options(['pdf' => 'Adobe pdf', 'docx' => 'Microsoft Word', 'odt' => 'OpenDocument Format', 'xls' => 'Microsoft Excel'])
                        ->label('Формат вывода')
                        ->value('pdf')
                        ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}

                        <div class="form-group col-md-3"><label class="control-label" for="">&nbsp; </label>
                            {!!  Former::large_block_primary_button('Скачать отчет')->id('download-btn') !!}
                        </div>


                        <div class="clearfix"></div>
                        {!! Former::close() !!}
                    </div>

                    <div class="page-content table-responsive" id="report-wrapper">

                    </div>
                    <div class="pull-right">
                        <small>*Тип графика: {{\App\Models\Report::$reportTypes[$report->type]}} </small>
                    </div>
                    <a href="{!! route('reports.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('additional-scripts')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>

    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script>

                @if(count($ranges) == 0)
        var startdate = moment().subtract(6, 'days');
        var enddate = moment();
                @else
        var startdate = moment("{{$ranges[0]['date']}}");
        var enddate = moment("{{$ranges[1]['date']}}");
                @endif

        var dateRangeDefaults = {
                    timePicker: false,
                    startDate: startdate,
                    endDate: enddate,

                    locale: {
                        format: 'DD-MM-YYYY',
                        "separator": " - ",
                        "applyLabel": "Применить",
                        "cancelLabel": "Очистить",
                        "fromLabel": "От",
                        "toLabel": "До",
                        "customRangeLabel": "Выбрать диапазон",
                        "weekLabel": "W",
                        "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                        "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                        "firstDay": 1
                    },
                    ranges: {
                        'За сегодня': [moment(), moment()],
                        'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                        'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                        'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                        'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }

        $(document).ready(function () {

            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {

                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                    $('#tmc_report-chart').attr('src', '/images/wait.gif');

                    window.params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD'),
                    };


                    var url = '/reports/render/table/' + {{request()->id}};

                    console.log(url, 'url')


                    $('#report-wrapper').load(url + '?' + $.param(window.params));


                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

            });

            // Кнопка скачивания
            $('#download-btn').click(function () {
                var url = '/reports/download/table/' + {{request()->id}} +'/' + $('#format').val() + '?' + $.param(window.params);
                //console.log(url)
                openInNewTab(url);

            })
            setTimeout(function () {
                $('input[name="daterange"]').trigger('click');
                $('.daterangepicker.dropdown-menu .btn-success').trigger('click');
                console.log('qqq')
            }, 400)


        })

    </script>
@endsection