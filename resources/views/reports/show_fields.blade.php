<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id' . ':' ) !!}
    <p>{!! $report->id !!}</p>
</div>
<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{!! $report->name !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', trans('backend.User Id') . ':' ) !!}
    <p>{!! $report->user->getfName()  !!} ({{$report->user->email}})</p>
</div>

<!-- Headers Field -->
<div class="form-group">
    {!! Form::label('headers', trans('backend.Rows') . ':' ) !!}
    <div class="clearfix"></div>
    <div class="col-md-6 col-sm-12">
        <ul class="list-group">

            @foreach(json_decode($report->headers) as $header)
                <li class="list-group-item">{{ Config::get("reports.rows.{$report->type}.{$header}")  }}</li>
            @endforeach
        </ul>
    </div>
    <div class="clearfix"></div>

</div>

<!-- Tmcs Field -->
<div class="form-group">
    {!! Form::label('tmcs', trans('backend.Tmcs') . ':' ) !!}
    <div class="clearfix"></div>
    <div class="col-md-6 col-sm-12">
        <ul class="list-group">

            @foreach($report->telemedcenters() as $telemedcenter)
                <li class="list-group-item">{{  $telemedcenter->name }}</li>
            @endforeach
        </ul>
    </div>
    <div class="clearfix"></div>


</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', trans('backend.Type') . ':' ) !!}
    <p>{!! $report->showReadableType() !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{!! $report->created_at !!}</p>
</div>
