<?php
$navItems = [
        1 => 'Выберите вид',
        2 => 'Выберите поля',
        3 => 'Выберите какие ТМЦ',
        'final' => 'Завершение',
    //'error' => 'Ошибка'
]
?>
<ul class="nav nav-pills nav-justified">
    @foreach($navItems as $k => $v)
        <li role="presentation" class="{{ $step == $k ? 'active' : '' }}"><a href="#">{{is_numeric($k) ? $k.'.' : ''}} {{$v}} </a></li>
    @endforeach
</ul>