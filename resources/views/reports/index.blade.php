@extends('layouts.cabinet')
<?php $title = trans('backend.Reports'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                    @if(Sentinel::inRole('admin'))
                        <a class="btn btn-xs btn-default" href="{{route('admin.reports.index')}}">Перейти в конструктор
                            отчетов</a>
                    @endif
                    @if(TmcRoleManager::hasRole('mcf_admin'))
                        <a class="btn btn-default  btn-xs pull-right" style="" href="{!! route('reports.create') !!}">
                            <i class="fa fa-plus"></i> {{ trans('backend.addnew')  }} новый отчет
                        </a>
                    @endif
                    @if(TmcRoleManager::hasRole("coordinator")||TmcRoleManager::hasRole("admin"))
                            <a class="btn btn-xs btn-default pull-left " href="/statistics">Сформировать отчет по своему ТМЦ
                                </a>
                    @endif
                    &nbsp;
                </div>
            </div>
            <div class="box-body">

                @include('reports.table')

            </div>
        </div>
    </div>
@endsection


<style>
    body .list-group-item {
        padding: 4px 10px;
    }

    .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
        vertical-align: top;
    }

    .container .box-body {
        padding: 0;
    }

    .container .table > thead > tr, .container .table > tbody > tr, .container .table > tfoot > tr {
        background: unset;
    }
</style>
