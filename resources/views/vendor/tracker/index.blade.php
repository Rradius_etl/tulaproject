@extends('layouts.stats')

@section('page-contents')
	<table id="table_div" class="display" cellspacing="0" width="100%"></table>
@stop

@section('inline-javascript')
    @include('vendor.tracker._datatables', $datatables_data)
@stop
