@extends('layouts.stats')

@section('page-contents')
	<table id="table_div" class="display" cellspacing="0" width="100%"></table>
@stop

@section('inline-javascript')
    @include(
        'vendor.tracker._datatables',
        array(
            'datatables_ajax_route' => route('tracker.stats.api.paths'),
            'datatables_columns' =>
            '
                { "data" : "path",    "title" : "'.trans('tracker::tracker.path').'", "orderable": true, "searchable": false },
                { "data" : "hits",    "title" : "'.trans('tracker::tracker.hits').'", "orderable": false, "searchable": false },
            '
        )
    )
@stop
