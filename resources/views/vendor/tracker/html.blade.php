<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@lang("tracker::tracker.tracker_title")</title>

	<script src="/templates/sb-admin-2/bower_components/jquery/dist/jquery.min.js"></script>

	@yield('required-scripts-top')

    <!-- Core CSS - Include with every page -->
    <link href="/templates/sb-admin-2/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/templates/sb-admin-2/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="/templates/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="/templates/sb-admin-2/bower_components/morrisjs/morris.css" rel="stylesheet">
    

    <!-- SB Admin CSS - Include with every page -->
    <link href="/templates/sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">

    <link href="/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/css/flags16.css"
	/>
</head>

<body>
    @yield('body')

    <!-- Core Scripts - Include with every page -->
    <script src="/templates/sb-admin-2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/templates/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <script src="/templates/sb-admin-2/bower_components/raphael/raphael-min.js"></script>
    <script src="/templates/sb-admin-2/bower_components/morrisjs/morris.min.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="/templates/sb-admin-2/dist/js/sb-admin-2.js"></script>

    <script src="/bower_components/moment/min/moment.min.js"></script>
    <script src="/bower_components/moment/locale/ru.js"></script>

    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>

	@yield('required-scripts-bottom')

    <script>
	    @yield('inline-javascript')
    </script>
</body>

</html>
