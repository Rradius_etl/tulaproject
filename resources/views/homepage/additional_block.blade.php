@if(\App\Models\InterfaceElement::isVisible('additional_blocks'))
    <div class="row">
        <div class="secondary-block col-sm-12">
            <div class="box box-white">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-title">
                        <h2 style="margin-top: 0">{{$block->title}}</h2>
                    </div>

                    {!! $block->block_content !!}
                </div>
                <!-- /.box-body -->

            </div>
        </div>
    </div>
@endif