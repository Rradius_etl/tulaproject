<!-- Request Id Field -->


<div class="box box-info box-solid">
    <div class="box-header">
        Часть ЛПУ консультанта
    </div>

    <div class="box-body" id="zebr" >
        <div class="row" >
            <?php
            $consultSide = $telemedConsultRequest->consultantSide;
            ?>
            @if($consultSide != null)
                <div class="form-group">
                    {!! Form::label('created_at', 'Выбранный доктор и его специальность:' ) !!}
                    <p>{{ $consultSide->doctor->surname." ". $consultSide->doctor->name." ". $consultSide->doctor->middlename." : ". $consultSide->doctor->spec }}&nbsp;</p>
                </div>
                <div class="form-group">
                    {!! Form::label('created_at', 'Запланированная дата и время:' ) !!}
                    <p>{{ $consultSide->planned_date }} &nbsp;</p>
                </div>
                <div class="form-group">
                    {!! Form::label('created_at', trans('backend.Comment') . ':' ) !!}
                    <p>{{ $consultSide->telemedConsultRequest->comment }}&nbsp;</p>
                </div>
                <div class="form-group">
                    {!! Form::label('created_at', 'ФИО принявшего заявку(ЛПУ-координатора):' ) !!}
                    <p>{{ $consultSide->coordinator_fullname }}&nbsp;</p>
                </div>
                <div class="form-group">
                    Файлы:
                    @foreach($consultSide->files as $file)
                        <div class="form-group">
                            <a href="/getfile/{{$file->file->id}}" target="_blank">{{$file->file->original_name}}</a>
                        </div>
                    @endforeach
                </div>

                <div class="form-group">
                    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                    <p>{{ $consultSide->created_at }}&nbsp;</p>
                </div>
                <div class="form-group">
                    {!! Form::label('created_at', trans('backend.Decision').";") !!}
                    <p>{{ trans("backend.".$consultSide->telemedConsultRequest->decision) }}&nbsp;</p>
                </div>

            @else
                Координатор ЛПУ еще не заполнил свою форму
            @endif
        </div>
    </div>
</div>
<div class="box @if($telemedConsultRequest->telemed_center_id!=null)  box-success @else  box-info @endif box-solid">
    <div class="box-header">
        Куда была направлена заявка
    </div>

    <div class="box-body" id="zebr">
        @if($telemedConsultRequest->telemed_center_id!=null)
            @if($telemedConsultRequest->telemedCenter!=null)
                <a data-toggle="tooltip" title="Нажмите чтобы узнать подробно об этом ТМЦ" href="{{route('general-telemed-centers.show',$telemedConsultRequest->telemedCenter->id)}}">{{ $telemedConsultRequest->telemedCenter->name }}</a>
            @else
                ТМЦ был удален
            @endif
        @else
            не направлено к ТМЦ@endif

    </div>
</div>
<div class="box box-info box-solid">
    <div class="box-header">
        Часть ЛПУ абонента
    </div>

    <div class="box-body" id="zebr">
        <div class="row" >
            <!-- Comment Field -->
            <?php
            $abonentSide = $telemedConsultRequest->abonentSide
            ?>
            <div class="form-group">
                {!! Form::label('doctor_fullname', trans('validation.attributes.doctor_fullname') . ':' ) !!}
                <p>{{ $abonentSide->doctor_fullname }} &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('doctor_spec', trans('validation.attributes.doctor_spec') . ':' ) !!}
                <p> @if(!empty($abonentSide->doctor_spec)){{ $abonentSide->doctor_spec }} @else <span
                            class="label label-info">не указана</span> @endif &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('comment', trans('validation.attributes.consult_type') . ':' ) !!}
                <p>{{  $abonentSide->ConsultationType->name }}  &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('comment', trans("backend.MCF") . ':' ) !!}
                <p>{{ $abonentSide->medCareFac->name }} &nbsp;</p>
            </div>

            <div class="form-group">
                {!! Form::label('desired_consultant', trans("validation.attributes.desired_consultant") . ':' ) !!}
                <p>{{ $abonentSide->desired_consultant }} &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('desired_date', trans("validation.attributes.desired_date") . ':' ) !!}
                <p>{!!  $abonentSide->getDesiredDate() !!} &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('desired_time', trans("validation.attributes.desired_time") . ':' ) !!}
                <p>{!!  date("H:i",strtotime($abonentSide->desired_time))  !!}&nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_uid_or_fname', trans("validation.attributes.patient_uid_or_fname") . ':' ) !!}
                <p>{{ $abonentSide->patient_uid_or_fname }}&nbsp;</p>
            </div>

            <div class="form-group">
                {!! Form::label('patient_address', trans('validation.attributes.patient_address') . ':' ) !!}
                <p>{{ $abonentSide->patient_address }}&nbsp;</p>
            </div>

            <div class="form-group">
                {!! Form::label('patient_birthday', trans('validation.attributes.patient_birthday') . ':' ) !!}
                <p>{!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $abonentSide->patient_birthday)->format('d.m.Y')!!}&nbsp;</p>
            </div>

            <div class="form-group">
                {!! Form::label('decision', trans('validation.attributes.patient_social_status') . ':' ) !!}
                <p>@if($abonentSide->socialStatus != null){{ $abonentSide->socialStatus->name }} @else Не указано @endif</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_indications_for_use_id', trans('validation.attributes.patient_indications_for_use') . ':' ) !!}
                <p>{{$abonentSide->getIndForUse() }}&nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_indications_for_use_other', trans('validation.attributes.patient_indications_for_use').' - другое:' ) !!}
                <div class="col-sm-6 pull-right">
                    <p>{{ $abonentSide->patient_indications_for_use_other }}&nbsp;</p>
                </div>
                <div class="clearfix"></div>

            </div>

            <div class="form-group">
                {!! Form::label('patient_questions', trans('validation.attributes.patient_questions') . ':' ) !!}
                <p>{{ $abonentSide->patient_questions }}</p>
                &nbsp;
            </div>
            <!-- Telemed Center Id Field -->
            <div class="form-group">
                {!! Form::label('name', trans('backend.TMC') . ':' ) !!}
                <p>
                @if($abonentSide->telemed_center_id != null)
                    <?php $tmc1 = $abonentSide->telemedCenter ?>
                    @if($tmc1 != null)
                        <td>   {{ $abonentSide->telemedCenter->name }}</td>
                    @else
                        ТМЦ был удален
                    @endif
                @endif
                </p>
            </div>
            <!-- Created At Field -->
            <div class="form-group">
                {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                <p>{{ $abonentSide->created_at }}</p>
            </div>
            <!-- Updated At Field -->
            <div class="form-group">
                {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
                <p>{{ $abonentSide->updated_at}}</p>
            </div>

            <div class="form-group">
                {!! Form::label('updated_at',   'Загруженные файлы:' ) !!}
                @if(count($files)> 0)
                    <ul>
                        @foreach($files as $file)
                            <li><a href="/getfile/{{$file->file_id}}">{{$file->original_name}}</a></li>
                        @endforeach
                    </ul>
                @else
                    <p class="">Отсутствуют</p>
                @endif
            </div>
        </div>
    </div>
</div>