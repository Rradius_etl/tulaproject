


<!-- Telemed Center Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telemed_center_id','Перенаправить в ТМЦ:') !!}
    {!! Form::select('telemed_center_id',$tmcs->prepend('--- Выберите ---', ''), null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tmc-telemed-consult-requests.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
