<li class="{{ Request::is('/') }}"><a href="/#home" class="smoothScroll">{{ trans('adminlte_lang::message.home') }}</a>
</li>

<li class="{{Request::is('news*') ? 'active' : ''}}"><a href="/news">Новости</a></li>

{{--  Проверка: Покыватьстатические страницы в меню или нет --}}
@if(\App\Models\InterfaceElement::isVisible('menu_static_pages'))
    {{-- Вывести все статические страницы в которых отчмечено показывать в меню --}}
    @foreach( \App\Models\Admin\Page::inMenu()->orderBy('updated_at')->get() as $page)
        <li class="{{Request::is($page->slug .'*') ? 'active' : ''}}"><a href="/{{$page->slug}}">{{ $page->title }}</a>
        </li>
    @endforeach
@endif

@if(Sentinel::check())
    <li class="{{Request::is('profile*') ? 'active' : ''}}"><a href="/profile">Профиль</a></li>
@else
    <li class=""><a href="/login">Личный кабинет</a></li>
@endif
<li @if(Request::is('openfiles')) class="active"   @endif><a href="/openfiles">Файлы</a></li>
@if(Sentinel::check() && Sentinel::inRole('admin'))
    <li><a href="/admin">Админ панель</a></li>
@endif

@if(Sentinel::check())
    <li><a href="/relogin">Выйти</a></li>
@endif
@if(Sentinel::guest())
    <li><a href="/login">Войти</a></li>
@endif