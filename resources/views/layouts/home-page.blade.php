<!DOCTYPE html>
<html lang="<?= App::getLocale(); ?>">

@section('htmlheader')
    @include('layouts.frontend_partials.htmlheader')
@show

<body data-spy="scroll" data-offset="0" data-target="#navigation">
@include('partials.impaired-version')
@include('layouts.main.top-bar')
        <!-- Header with navigation -->
<header>

    <div class="logo container"><a href="/"><img src="/images/logo.png" alt=""></a></div>
    @include('layouts.main.navigation')
</header>

<!-- /END of Header with navigation -->
<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    @yield('content')
</section><!-- /.content -->


@include('layouts.main.footer')

@section('scripts')
@include('layouts.main.scripts')
@show
        <!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>
