<!DOCTYPE html>

<html lang="<?= App::getLocale(); ?>">

@section('htmlheader_backend')
    @include('layouts.partials.htmlheader_backend')
@show

<body class="skin-blue layout-top-nav">
<div class="wrapper">
            

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
                @section('breadcrumbs')
                @endsection

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts_backend')
@show

</body>
</html>
