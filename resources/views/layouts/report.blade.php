<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @if( isset($format))
        @if($format != 'xls' && $format != 'docx')
            <style>

                body {
                    font: normal medium/1.4 sans-serif;

                }

                table {
                    border-collapse: collapse;
                    width: 100%;

                }

                th, td {
                    padding: 0.25rem;
                    text-align: left;
                    border: 1px solid #000;
                }

                tbody tr:nth-child(odd) {
                    background: #eee;
                }

                thead {
                    display: table-header-group;
                }

                tfoot {
                    display: table-row-group;
                }

                tr {
                    page-break-inside: avoid;
                }

                td.text-center {
                    text-align: center;
                }
            </style>
        @endif
    @endif
</head>
<body>
@yield('content')
</body>
</html>
