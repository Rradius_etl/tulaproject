<head>
    <meta charset="UTF-8">
    <title> @yield('meta_title',  Config::get('website-settings.site-name')) </title>
    <meta name="description" content="@yield('meta_description',  Config::get('website-settings.meta-description') )">
    <meta name="keywords" content="@yield('meta_keywords', Config::get('website-settings.meta-keywords') )">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dastiw1 - bitbuchet.com/dastiw1">
    <meta name="csrf_token" content="{{csrf_token()}}">






    <!-- FontAwesome -->
    <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Selectize -->
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">

    <link rel="stylesheet" href="/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <!-- ngDialog -->
    <link rel="stylesheet" href="/bower_components/ng-dialog/css/ngDialog.min.css">
    <link rel="stylesheet" href="/bower_components/v-accordion/dist/v-accordion.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->


    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>

    <script src="/bower_components/moment/min/moment.min.js"></script>
    <script src="/bower_components/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
    <script src="/bower_components/moment/locale/ru.js"></script>

    <!-- Angular -->
    <script src="/bower_components/angularjs/angular.min.js"></script>

    <!-- angular-moment -->
    <script src="/bower_components/angular-animate/angular-animate.min.js"></script>
    <!-- V-Accordion -->
    <script src="/bower_components/v-accordion/dist/v-accordion.min.js"></script>
    <!-- Angular Modules -->
    <script src="/bower_components/ng-dialog/js/ngDialog.min.js"></script>
    <script src="/bower_components/angular-ui-calendar/src/calendar.js"></script>
    <!-- Fullcalendar -->
    <script src="/js/fullcalendar.js"></script>
    <script src="/bower_components/fullcalendar/dist/lang/ru.js"></script>
    <!-- angular-tree-control -->
    <script src="/bower_components/angular-tree-control/angular-tree-control.js"></script>

    <script src="/js/angular-selectize.js"></script>


    <!--  angular-bootstrap-datetimepicker -->
    <script src="/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js"></script>
    <script src="/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js"></script>
    <!-- angular-date-time-input directive -->
    <script src="/bower_components/angular-date-time-input/src/dateTimeInput.js"></script>
    <!--  ng-file-upload -->
    <script src="/bower_components/ng-file-upload/ng-file-upload.min.js"></script>
    <!-- angularjs-dropdown-multiselect -->
    <script src="/bower_components/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min.js"></script>
    <script src="/bower_components/lodash/dist/lodash.min.js"></script>

    <script src="/app/js/app.js"></script>
    <script src="/app/js/services.js"></script>

    <!-- datetimepicker -->
    <script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>


    <script src="{{ asset('/js/smoothscroll.js') }}"></script>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
