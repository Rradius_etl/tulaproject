<div data-ng-show="!event.isNew" class="request-status request-status-@{{ checkForStatus(newRequest) }}">
    <p>Статус: <span data-ng-repeat="a in decisionsArray" data-ng-if="checkForStatus(newRequest) == a.id" >@{{ a.name }}</span></p>
</div>

{!!  Former::text('request_uid')
          ->class('form-control')
          ->ngModel('abonentSideModel.request_uid')
          ->ngReadonly('true')
          ->disabled()
          !!}


{!!  Former::hidden('request_id')
          ->ngModel('abonentSideModel.request_id')
          !!}


{!!  Former::text('created_time')
    ->class('form-control')
    ->ngModel('abonentSideModel.created_time')
    ->ngReadonly('true')
    ->readonly()
      !!}



<div class="form-group">
    <label for="telemed_center_id">Отправить от имени ТМЦ: </label>
    <div class="input-group">
        <select name="telemed_center_id" id="telemed_center_id" data-ng-disabled="true" data-ng-model="abonentSideModel.telemed_center_id" class="form-control"
                ng-options="option.id as option.name for option in telemedCenters" >
        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>


</div>

{!!  Former::text('doctor_fullname')
      ->class('form-control')
      ->ngModel('abonentSideModel.doctor_fullname')
      ->ngReadonly('true')
      ->required()  !!}


{!!  Former::text('doctor_spec')->class('form-control') ->ngReadonly('true') ->ngModel('abonentSideModel.doctor_spec') !!}

<div class="form-group">
    <label for="med_care_fac_id">Выбор ЛПУ-консультанта: </label>

    <div class="input-group">
        <select name="med_care_fac_id" id="med_care_fac_id"  data-ng-disabled="true"  data-ng-model="abonentSideModel.med_care_fac_id"
                ng-options="option.id as option.name for option in allMcfs" class="form-control" >

        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>

</div>



{!!  Former::text('desired_consultant')->ngModel('abonentSideModel.desired_consultant')->ngReadonly('true')->class('form-control') !!}

<div class="dropdown">
    <a class="dropdown-toggle" id="datetime-dropdown" role="button" data-toggle="dropdown" data-target="#"
       href="#">
        <label>Желаемая дата и время  консультации</label>
        <div class="input-group">
            <input type="text" class="form-control"  data-ng-disabled="true" data-ng-model="abonentSideModel.date"
                   data-date-time-input="Do MMMM YYYY HH:mm" data-ng-required="required">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <datetimepicker data-ng-model="abonentSideModel.date"
                        data-datetimepicker-config="datetimepickerOptions" data-before-render="filterDatetimepicker($dates)"></datetimepicker>
    </ul>
</div>


<div class="form-group"  >
    <label for="consult_type">Тип клинической консультации: </label>
    <selectize name="consult_type" options="consult_types" id="consult_type" ng-model="abonentSideModel.consult_type" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>

</div>

<div class="form-group" >
    <label for="medical_profile">Профиль медицинской помощи: </label>

    <div class="input-group">
        <select  data-ng-disabled="true" data-ng-options="value.id as value.name group by value.category_name for value in medical_profiles"
                name="medical_profile" id="medical_profile"  data-ng-model="abonentSideModel.medical_profile" class="form-control" >

        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>



</div>


{!!  Former::text('patient_uid_or_fname')->ngModel('abonentSideModel.patient_uid_or_fname')->ngReadonly('true')->class('form-control')->required() !!}

<div class="dropdown">
    <a class="dropdown-toggle" id="date-dropdown" role="button" data-toggle="dropdown" data-target="#"
       href="#">
        <label> {{ trans('validation.attributes.patient_birthday') }}</label>
        <div class="input-group">
            <input type="text" class="form-control"  data-ng-disabled="true" data-ng-model="abonentSideModel.patient_birthday"
                   data-date-time-input="Do MMMM YYYY" data-ng-required="required">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <datetimepicker data-ng-model="abonentSideModel.patient_birthday"
                        data-datetimepicker-config="datepickerBirthday" data-before-render="patientBirthdayFilter($dates)"></datetimepicker>
    </ul>
</div>


<div class="form-group" >
    <label for="patient_gender">{{ trans('validation.attributes.patient_gender') }} </label>

    <div class="input-group">
        <select name="patient_gender" id="patient_gender"  data-ng-disabled="true"
                data-ng-model="abonentSideModel.patient_gender" class="form-control" >
            <option value="@{{option.id}}" data-ng-repeat="option in genders track by option.id" >@{{ option.name }}</option>
        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>

</div>

<div class="form-group" >
    <label for="patient_social_status">{{ trans('validation.attributes.patient_social_status') }} </label>


    <div class="input-group">
        <select name="patient_social_status" id="patient_social_status" data-ng-disabled="true"
                data-ng-model="abonentSideModel.patient_social_status" class="form-control"
               ng-options="option.id as option.name for option in social_statuses" >

        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>


</div>

{!!  Former::text('patient_address')->ngModel('abonentSideModel.patient_address')->ngReadonly('true') ->class('form-control') !!}

<div class="form-group" >
    <label for="patient_indications_for_use_id">{{ trans('validation.attributes.patient_indications_for_use') }}* </label>
    <selectize name="patient_indications_for_use_id"
               options="ind_for_use"
               id="patient_indications_for_use_id"
               ng-model="abonentSideModel.patient_indications_for_use_id"
               ng-disabled="true"
               config="singleConfig"></selectize>


</div>
<div class="form-group" data-ng-if="abonentSideModel.patient_indications_for_use == 'other'" >
    <label for="patient_indications_for_use_other"> {{trans('validation.attributes.patient_indications_for_use_other')}}</label>
    <input type="text" data-ng-disabled="true" class="form-control" data-ng-model="abonentSideModel.patient_indications_for_use_other" name="patient_indications_for_use_other" id="patient_indications_for_use_other">
</div>

{!!  Former::text('patient_questions')->ngModel('abonentSideModel.patient_questions')->ngReadonly('true')->class('form-control') !!}

{!!  Former::text('coordinator_fullname')->ngModel('abonentSideModel.coordinator_fullname')->ngReadonly('true')->class('form-control')->readonly() !!}
{!!  Former::text('coordinator_phone')->ngModel('abonentSideModel.coordinator_phone')->ngReadonly('true')->class('form-control')->readonly() !!}

