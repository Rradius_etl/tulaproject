<div class="box box-white">
    <div class="box-header text-center">

        <h2 data-ng-show="newRequest.event_type == 'meeting'">

            <div>
                <span>В этот день назначено следующее совещание</span>
            </div>
        </h2>
        <h2 data-ng-show="newRequest.event_type == 'seminar'">
            <div>
                <span>В этот день назначен следующий семинар</span>
            </div>
        </h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        {!!    Former::vertical_open_for_files()
                   ->id('consult-requres-form')
                     ->name('new-request-form')
                   ->secure()
                   ->method('POST')  !!}

        <div class="request-status request-status-@{{ checkForStatus(newRequest) }}">
            <p>Статус: <span data-ng-repeat="a in decisionsArray" data-ng-if="checkForStatus(newRequest) == a.id" >@{{ a.name }}</span></p>
        </div>



        <div class="form-group" ng-init="newRequest.telemed_center_id = SelectedTMC">
            <label for="telemed_center_id">Отправить от имени ТМЦ: </label>
            <select name="telemed_center_id" id="telemed_center_id"  disabled
                    data-ng-model="newRequest.telemed_center_id"  class="form-control"
                    data-ng-options="option.id as option.name for option in myTelemedCenters" >
            </select>
        </div>

        {!!  Former::text('initiator')
            ->class('form-control')
            ->ngModel('newRequest.initiator')
            ->ngDisabled('true')
            ->ngRequired('required')
              !!}


        <div class="form-group form-group-multiselect">
            <label>Участники</label>
            <div ng-dropdown-multiselect="" search-filter="customFilter" options="participantsArray"
                 selected-model="newRequest.participants" extra-settings="multiParticipantsOptions"
                 group-by="mcf_name" translation-texts="multiParticipantsOptions.translationTexts"
                 ng-dropdown-multiselect-disabled="true" ></div>
            <button type="button" class="btn btn-info" data-ng-click="openParticipantsModal(participantsArray,newRequest.participants, false)" > <i class="fa fa-folder-open-o"></i> Показать список участников</button>
            <div class="clearfix"></div>
            <div class="help-row">
                <button type="button" class="btn btn-info" data-ng-click="selectDeliveryGroup(participantsArray)"  data-ng-disabled="true" > <i class="fa fa-users"></i> Выбрать группу рассылки</button>
                <button type="button" class="btn btn-info" data-ng-click="openParticipantsModal(participantsArray,newRequest.participants, true)"  > <i class="fa fa-floppy-o"></i> Сохранить группу рассылки</button>
            </div>



        </div>


        <div class="dropdown">
            <a class="dropdown-toggle" id="datetime-dropdown" role="button" data-toggle="dropdown" data-target="#"
               href="#">
                <label>Дата и время</label>
                <div class="input-group">
                    <input type="text" class="form-control" data-ng-model="newRequest.date"
                           data-date-time-input="Do MMMM YYYY HH:mm" data-ng-required="required" data-ng-disabled="true">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <datetimepicker data-ng-model="newRequest.date"
                                data-datetimepicker-config="datetimepickerOptions" data-before-render="filterDatetimepicker($dates)"></datetimepicker>
            </ul>
        </div>


        <div class="form-group">
            <label for="event_type">Тип видеоконференции</label>
            <select name="event_type" id="event_type" ng-disabled="true" data-ng-model="newRequest.event_type" class="form-control" >
                <option value="@{{option.id}}" ng-selected="newRequest.event_type == option.id" data-ng-repeat="option in eventTypesArray track by option.id" >@{{ option.name }}</option>
            </select>
        </div>


        {!! Former::inline_radios('importance')
        ->radios([
            'низкая' => ['name' => 'importance', 'value' => 'low', 'ng-model' => 'newRequest.importance'],
            'средняя' => ['name' => 'importance', 'value' => 'medium', 'ng-model' => 'newRequest.importance'],
            'высокая' => ['name' => 'importance', 'value' => 'high', 'ng-model' => 'newRequest.importance'],
        ])->ngRequired('required')->ngDisabled('true') !!}

        <div class="flclear"> {!!  Former::text('title')->label('Тема видеоконференции')
           ->class('form-control')
           ->ngModel('newRequest.title')
           ->ngRequired('required')
           ->ngDisabled('true') !!} </div>

        <div class="form-group">
            <label for="files">Прикрепить файл</label>
            <div class="input-group">
                <input type="file" class="form-control" ngf-select ng-model="files" name="files" id="files"
                       ngf-multiple="true"
                       multiple="multiple" ngf-max-size="20MB" ng-disabled="true" >
                </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" uploadfile type="button">Обзор</button>
                  </span>
            </div>

            <ul class="event-files">
                <li data-ng-repeat="eventFile in eventFiles" >
                    <i class="fa fa-file-o"></i>
                    <span>@{{ eventFile.file.original_name }}</span>
                    <a href="/getfile/@{{ eventFile.file_id }}" >(Скачать)</a>
                    <button ng-disabled="true" class="btn btn-xs btn-danger"  data-ng-show="eventFile.deleted != true"  data-ng-click="deleteFile(eventFile)">(Удалить)</button>
                    <button ng-disabled="true" class="btn btn-xs btn-danger"  data-ng-show="eventFile.deleted == true"  data-ng-click="undeleteFile(eventFile)">(Отменить удаление)</button>
                </li>
            </ul>
        </div>

        <div class="form-group" data-ng-if="!event.isNew" >
            {!! Former::checkbox('notify')
                ->text('Уведомить об изменениях всех участников')
                ->label(false)
                ->check()
                ->ngModel('newRequest.notify')
               !!}
        </div>

        <div class="form-group" data-ng-if="newRequest.notify" >
            {!! Former::checkbox('send_email')
                ->text('Уведомить по e-mail')
                ->label(false)
                ->check()
                ->ngModel('newRequest.send_email')
                !!}
        </div>

        <div class="alert alert-danger" data-ng-if="newRequest.errors">
            <div data-ng-repeat="fields in newRequest.errors">
                <div data-ng-repeat="error in fields">@{{ error }}</div>
            </div>
        </div>

        <div class="form-group"  data-ng-show="readMode == true">
            <button ng-click="closeThisDialog()" class="btn btn-round btn-danger"> Закрыть </button>
        </div>

        <div class="form-group">
            <div data-ng-show="checkForInvitationStatus(newRequest) == 'rejected'" class="label label-warning" >
                Вы отказались от участия в данном мероприятии
            </div>
            <div data-ng-show="checkForInvitationStatus(newRequest) == 'agreed'" class="label label-success" >
                Вы согласились участвовать в данном мероприятии
            </div>
            <br><br><br>

            <button type="button" ng-click="AcceptInvitation(newRequest)"
                    data-ng-hide="checkForInvitationStatus(newRequest) == 'agreed' || checkForInvitationStatus(newRequest) == 'rejected'"
                    class="btn btn-round btn-success"><i class="fa fa-check"></i> Пойду </button>

            <button type="button" ng-click="DeclineInvitation(newRequest)"
                    data-ng-hide="checkForInvitationStatus(newRequest) == 'rejected' || checkForInvitationStatus(newRequest) == 'agreed'"
                    class="btn btn-round btn-danger"><i class="fa fa-close"></i> Не пойду </button>

            <button ng-click="closeThisDialog()" class="btn btn-round btn-danger"> Закрыть</button>
        </div>



        {!! Former::close() !!}
    </div>
</div>