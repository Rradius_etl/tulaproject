
<div class="box-body">
    {!!    Former::vertical_open()
             ->route('abonent-telemed-consult-requests.store')
             ->id('consult-requres-form')
             ->secure()
             ->rules([
                 '' => 'required'
             ])
             ->method('POST')  !!}

    <div data-ng-show="!event.isNew" class="request-status request-status-@{{ checkForStatus(newRequest) }}">
        <p>Статус: <span data-ng-repeat="a in decisionsArray" data-ng-if="checkForStatus(newRequest) == a.id" >@{{ a.name }}</span></p>
    </div>

    @include('layouts.angular.dialogs.consultation_fields')

    <div class="alert alert-danger" data-ng-if="newRequest.errors">
        <div data-ng-repeat="fields in newRequest.errors">
            <div data-ng-repeat="error in fields">@{{ error }}</div>
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group" data-ng-show="readMode == true">
        <button type="button" ng-click="closeThisDialog()" class="btn btn-round btn-danger"> Закрыть</button>
    </div>
    <div class="form-group" data-ng-show="readMode == false">
        <button data-ng-if="!editMode" data-ng-show="event.isNew" class="btn btn-round btn-primary" type="button"
                data-ng-click="submitForm(files,newRequest,event, 'abonent-side')"
                ng-disabled="new-request-form.$invalid">Направить заявку
        </button>

        <button type="button" data-ng-if="!event.isNew" data-ng-show="!HideThisButton"
                data-ng-click="MakeFieldsEditable(); HideThisButton = true" class="btn btn-round btn-success">Внести правки
        </button>



        <button type="button" data-ng-if="!event.isNew" data-ng-show="!editMode" class="btn btn-round btn-primary"
                type="button" data-ng-click="submitForm(files,newRequest, event, 'abonent-side')"
                ng-disabled="new-request-form.$invalid">Сохранить изменения
        </button>


        <button type="button" ng-click="cancelMyEvent(newRequest)"  data-ng-if="!event.isNew" data-ng-hide="checkForStatus(newRequest) == 'canceled' || checkForStatus(newRequest) == 'completed'"
                class="btn btn-round btn-danger"><i class="fa fa-close"></i> Отменить заявку</button>

        <button type="button" ng-click="closeThisDialog()"
                class="btn btn-round btn-danger"> Закрыть </button>


    </div>



    {!!  Former::close()  !!}
</div>
