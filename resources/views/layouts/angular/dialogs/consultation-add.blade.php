<div class="box box-white">
    <div class="box-header text-center">
        <div data-ng-show="readMode == true">
            <h2>В этот день назначена следующая телемедицинская консультация</h2>
        </div>

        <div data-ng-show="readMode == false">
            <h2 data-ng-show="event.isNew">Заявка на создание телемедицинской консультации</h2>
            <h2 data-ng-show="!event.isNew">Редактирование заявки на телемедицинскую консультацию</h2>
        </div>

        <button type="button" data-ng-show="!editMode" class="btn btn-default navbar-btn pull-right"
                data-ng-click="showTemplates = !showTemplates"><span class="fa fa-bars"></span> &nbsp;Заполнить из шаблона </button>
        <div class="clearfix"></div>
        <div class="pattern-templates" data-ng-show="showTemplates">

            {!!  Former::group()->Class('form-group')->id('pattern')  !!}
            {!! Former::label('Выберите шаблон из списка:')->Class('control-label') !!}
            <selectize name="pattern" options="requestPatterns" id="pattern" ng-model="selectedPattern"  ng-change="applyRequestPattern()"  ng-disabled="editMode" required="" config="singleConfig"></selectize>

            {!! Former::closeGroup()   !!}


        </div>

    </div>
    <!-- /.box-header -->

    @include('layouts.angular.dialogs.consultation')
</div>