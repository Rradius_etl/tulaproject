<div class="box box-white"  data-ng-show="activeTab == 1" >
    <div class="box-header text-center">
        <h2>@{{ event.title }}</h2>

    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {!!    Former::vertical_open()
                  ->id('consult-requres-form')
                  ->secure()
                  ->rules([
                      '' => 'required'
                  ])
                  ->method('POST')  !!}
        <div class="ngdialog-content">
            {!! Former::text('datetime')->label('Дата и время приема заявки')->ngReadonly()->value(\Carbon\Carbon::now()->format('d.m.Y'))->required() !!}
           {!! Former::hidden('request_id')->ngModel('newRequest.request_id') !!}


                {!!  Former::group()->Class('form-group')->id('appointed_person_id')  !!}
            {!! Former::label('appointed_person_id')->Class('control-label') !!}
                <selectize name="appointed_person_id" options="tmcUsers" id="appointed_person_id" ng-model="newRequest.appointed_person_id" ng-disabled="editMode" required="" config="selectizeConfig"></selectize>

                {!! Former::closeGroup()   !!}





            <div class="dropdown">
                <a class="dropdown-toggle" id="planned_date-dropdown" role="button" data-toggle="dropdown" data-target="#"
                   href="#">
                    <label> {{ trans('validation.attributes.planned_date') }}<sup>*</sup></label>
                    <div class="input-group">
                        <input type="text" class="form-control"  data-ng-disabled="editMode" data-ng-model="newRequest.planned_date"
                               data-date-time-input="Do MMMM YYYY HH:mm" data-ng-required="required">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <datetimepicker data-ng-model="newRequest.planned_date"
                                    data-datetimepicker-config="datetimepickerOptions" ></datetimepicker>
                </ul>
            </div>

            <div class="form-group" >
                <label for="doctor_id">Врач-консультант и его специальность<sup>*</sup> </label>

                    <selectize name="doctor_id" options="doctorsArray" id="doctor_id" ng-model="newRequest.doctor_id" ng-disabled="editMode" required="" config="selectizeConfig"></selectize>


            </div>


            {!! Former::textarea('comment')->label('Комментарий ЛПУ-консультанта')->ngDisabled('editMode')->ngModel('newRequest.comment') !!}


            {!! Former::text('coordinator_fullname')->ngModel('newRequest.coordinator_fullname')->label('Заявку принял координатор ТМЦ')->readonly()->required()  !!}


            <div class="form-group">
                <label for="files">Прикрепить файл</label>
                <div class="input-group">
                    <input type="file" class="form-control" ngf-select ng-model="files" name="files" id="files"
                           ngf-multiple="true"
                           multiple="multiple" ngf-max-size="20MB" ng-disabled="editMode" >
                    </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" uploadfile type="button">Обзор</button>
                  </span>
                </div>
                <!-- Только что загруженныке файлы -->
                <ul class="event-files">
                    <li data-ng-repeat="attachedFile in files" >
                        <i class="fa fa-file-o"></i>
                        <span>@{{ attachedFile.name }}</span>
                    </li>
                </ul>
                <ul class="event-files">
                    <li data-ng-repeat="eventFile in eventFiles" >
                        <i class="fa fa-file-o"></i>
                        <span>@{{ eventFile.original_name }}</span>
                        <a href="/getfile/@{{ eventFile.file_id }}" >(Скачать)</a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="alert alert-danger" data-ng-if="newRequest.errors">
            <div data-ng-repeat="fields in newRequest.errors">
                <div data-ng-repeat="error in fields">@{{ error }}</div>
            </div>
        </div>

        <div class="form-group"  data-ng-show="readMode == false" >
            <button type="button" data-ng-if="!event.isNew" data-ng-show="!HideThisButton" data-ng-click="MakeFieldsEditable(); HideThisButton = true" class="btn btn-round btn-success">Внести правки</button>
            <button type="button"  ng-click="closeThisDialog()" class="btn btn-round btn-danger">Закрыть </button>
            <button type="button"  data-ng-show="editMode == false" class="btn btn-round btn-primary" data-ng-click="submitForm(files,newRequest, event, 'consultant-side', eventFiles)"   ng-disabled="new-request-form.$invalid"  >Согласовать</button>
            <button type="button"  data-ng-show="editMode == false" class="btn btn-round btn-danger" data-ng-click="Decline(event)" >Отклонить</button>


        </div>

        <div class="form-group" >
            <button class="btn btn-round btn-primary"  data-ng-click="writeConclusion()">Написать заключение</button>
        </div>


        {!!  Former::close()  !!}
    </div>
</div>
