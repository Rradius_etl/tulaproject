<form name="DeliveryPatternForm"  class="ngdialog-content" >

    <h3 data-ng-if="newDeliveryGroup.saveMode == false" >Список участников</h3>
    <h3 data-ng-if="newDeliveryGroup.saveMode == true" >Сохранить выбранные ТМЦ как группу рассылки </h3>
    <ul class="list-group">
        <li class="list-group-item" data-ng-repeat="(key, value) in participants | groupBy: 'mcf_name'" >
            <h4 class="list-group-item-heading">@{{ key }}</h4>
            <p class="list-group-item-text" data-ng-repeat="item in value" >@{{item.name}}</p>
        </li>
    </ul>

    <div class="form-group" data-ng-if="newDeliveryGroup.saveMode == true">
        <label for="delivery_name">Имя групп рассылки</label>
        <input type="text" data-ng-model="newDeliveryGroup.delivery_name" name="delivery_name" class="form-control"  required >
    </div>
    <div class="alert alert-danger" data-ng-if="newDeliveryGroup.errors">
        <div data-ng-repeat="fields in newDeliveryGroup.errors">
            <div data-ng-repeat="error in fields">@{{ error }}</div>
        </div>
    </div>
    <div class="alert alert-success" data-ng-if="newDeliveryGroup.success == true">
        <div>@{{ newDeliveryGroup.successMessage}}</div>
    </div>

    <div class="ngdialog-buttons">
        <button ng-disabled="DeliveryPatternForm.$invalid || newDeliveryGroup.success == true" data-ng-if="newDeliveryGroup.saveMode == true"  type="button" class="btn btn-round btn-primary" data-ng-click="saveDeliveryPattern(newRequest.participants, newDeliveryGroup.delivery_name)">Сохранить</button>
        <button type="button" class="btn btn-round btn-danger" ng-click="closeThisDialog()">Закрыть</button>
    </div>

</form>