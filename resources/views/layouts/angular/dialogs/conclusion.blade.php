<div class="box box-white">
    <div class="box-header text-center">
        <h2 class="box-title">Написать заключение</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {!!    Former::vertical_open_for_files()
                   ->id('consult-requres-form')
                     ->name('new-request-form')
                   ->secure()
                   ->rules([
                       '' => 'required'
                   ])
                   ->ngDisabled('cantWriteConclusion')
                   ->method('POST')  !!}




        {!!  Former::textarea('comment')
            ->class('form-control')
            ->label('Текст заключения')
            ->ngModel('Conclusion.comment')
             ->ngDisabled('cantWriteConclusion')
            !!}

        {!!  Former::number('calls_count')
           ->class('form-control')
           ->label('Количество вызовов')
           ->ngModel('Conclusion.calls_count')
            ->ngDisabled('cantWriteConclusion')
           ->min(0)
            !!}

        <fieldset>
            <legend>Продолжительность</legend>
            {!!  Former::number('minutes')
           ->class('form-control')
           ->label('Минуты')
           ->ngModel('Conclusion.minutes')
            ->ngDisabled('cantWriteConclusion')
           ->min(0)
            !!}
            {!!  Former::number('seconds')
               ->class('form-control')
               ->label('Секунды')
               ->ngModel('Conclusion.seconds')
                ->ngDisabled('cantWriteConclusion')
               ->min(0)
                !!}
        </fieldset>



        <div class="form-group">
            <label for="files">Прикрепить файл</label>
            <div class="input-group">
                <input type="file" class="form-control" ngf-select ng-model="files" name="files" id="files"
                       ngf-multiple="true"
                       multiple="multiple" ngf-max-size="20MB" ng-disabled="cantWriteConclusion" >
                </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" uploadfile type="button">Обзор</button>
                  </span>
            </div>
            <ul class="event-files">
                <li data-ng-repeat="attachedFile in files" >
                    <i class="fa fa-file-o"></i>
                    <span>@{{ attachedFile.name }}</span>
                </li>
            </ul>
            <ul class="event-files">
                <li data-ng-repeat="eventFile in eventFiles" >
                    <i class="fa fa-file-o"></i>
                    @{{ eventFile.file.original_name }}
                    <a href="/getfile/@{{ eventFile.file_id }}" >(Скачать)</a>
                    <button  data-ng-show="eventFile.deleted == true"  data-ng-click="undeleteFile(eventFile)">(Отменить удаление)</button>
                </li>
            </ul>
        </div>



        <div class="alert alert-danger" data-ng-if="Conclusion.errors">
            <div>
                @{{ Conclusion.errors[0] }}
            </div>
        </div>

        <div>
            <button type="button" class="btn btn-round btn-primary" data-ng-click="submitConclusion(files,Conclusion,eventFiles)" ng-disabled="new-request-form.$invalid" >
                Отправить заключение
            </button>
            <button ng-click="closeThisDialog()" class="btn btn-round btn-danger"> Закрыть </button>
        </div>


        {!! Former::close() !!}
    </div>
</div>