{!!  Former::text('request_uid')
          ->class('form-control')
          ->ngModel('newRequest.request_uid')
          ->ngReadonly('editMode')
          ->disabled()
          !!}


{!!  Former::hidden('request_id')
          ->ngModel('newRequest.request_id')
          !!}


{!!  Former::text('created_time')
    ->class('form-control')
    ->ngModel('newRequest.created_time')
    ->ngReadonly('true')
    ->readonly()
      !!}




{!!  Former::text('doctor_fullname')
      ->class('form-control')
      ->ngModel('newRequest.doctor_fullname')
      ->ngReadonly('editMode')
      ->required()  !!}


{!!  Former::text('doctor_spec')->class('form-control') ->ngReadonly('editMode') ->ngModel('newRequest.doctor_spec') !!}

<div class="form-group">
    <label for="med_care_fac_id">Выбор ЛПУ-консультанта*: </label>
    <selectize name="med_care_fac_id" options="mcfs" id="med_care_fac_id" ng-model="newRequest.med_care_fac_id" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>
</div>



{!!  Former::text('desired_consultant')->ngModel('newRequest.desired_consultant')->ngReadonly('editMode')->class('form-control') !!}

<div class="dropdown">
    <a class="dropdown-toggle" id="datetime-dropdown" role="button" data-toggle="dropdown" data-target="#"
       href="#">
        <label>Желаемая дата и время  консультации</label>
        <div class="input-group">
            <input type="text" class="form-control"  data-ng-disabled="editMode" data-ng-model="newRequest.date"
                   data-date-time-input="Do MMMM YYYY HH:mm" data-ng-required="required">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <datetimepicker data-ng-model="newRequest.date"
                        data-datetimepicker-config="datetimepickerOptions" data-before-render="filterDatetimepicker($dates)"></datetimepicker>
    </ul>
</div>


<div class="form-group"  >
    <label for="consult_type">Тип клинической консультации*: </label>
    <selectize name="consult_type" options="consult_types" id="consult_type" ng-model="newRequest.consult_type" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>


</div>

<div class="form-group" >
    <label for="medical_profile">Профиль медицинской помощи*: </label>

    <div class="input-group">
        <select  data-ng-disabled="editMode" data-ng-options="value.id as value.name group by value.category_name for value in medical_profiles"
                 name="medical_profile" id="medical_profile"  data-ng-model="newRequest.medical_profile" class="form-control" >

        </select>
        <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
    </div>
</div>

{!!  Former::text('patient_uid_or_fname')->ngModel('newRequest.patient_uid_or_fname')->ngReadonly('editMode')->class('form-control')->required() !!}

<div class="dropdown">
    <a class="dropdown-toggle" id="date-dropdown" role="button" data-toggle="dropdown" data-target="#"
       href="#">
        <label> {{ trans('validation.attributes.patient_birthday') }}*</label>
        <div class="input-group">
            <input type="text" class="form-control"  data-ng-disabled="editMode"  data-ng-model="newRequest.patient_birthday"
                   data-date-time-input="Do MMMM YYYY" data-ng-required="true">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <datetimepicker data-ng-model="newRequest.patient_birthday" ng-required="true" data-datetimepicker-config="datepickerBirthday" data-before-render="patientBirthdayFilter($dates)"></datetimepicker>
    </ul>
</div>


<div class="form-group" >
    <label for="patient_gender">{{ trans('validation.attributes.patient_gender') }}* </label>
    <selectize name="patient_gender" options="genders" id="patient_gender" ng-model="newRequest.patient_gender" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>

</div>

<div class="form-group" >
    <label for="patient_social_status">{{ trans('validation.attributes.patient_social_status') }} </label>

    <selectize name="patient_social_status" options="social_statuses" id="patient_social_status" ng-model="newRequest.patient_social_status" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>
</div>

{!!  Former::text('patient_address')->ngModel('newRequest.patient_address')->ngReadonly('editMode') ->class('form-control') !!}

<div class="form-group" >
    <label for="patient_indications_for_use_id">{{ trans('validation.attributes.patient_indications_for_use') }}* </label>

    <selectize name="patient_indications_for_use_id" options="ind_for_use" id="patient_indications_for_use_id" ng-model="newRequest.patient_indications_for_use_id" ng-disabled="editMode ? 'disable' : ''" required="true" config="singleConfig"></selectize>

    {{--<div class="input-group">
        <select name="patient_indications_for_use_id" id="patient_indications_for_use"   data-ng-disabled="editMode"
                data-ng-model="newRequest.patient_indications_for_use_id" class="form-control" >
            <option value="@{{option.id}}" data-ng-repeat="option in ind_for_use track by option.id" >@{{ option.name }}</option>
        </select>
        <span class="input-group-addon" input-addon ><i class="fa fa-chevron-down text-primary"></i></span>

    </div>--}}

</div>
<div class="form-group" data-ng-if="newRequest.patient_indications_for_use_id == 'other'" >
    <label for="patient_indications_for_use_other"> {{trans('validation.attributes.patient_indications_for_use_other')}}</label>
    <input type="text" data-ng-disabled="editMode" class="form-control" data-ng-model="newRequest.patient_indications_for_use_other" name="patient_indications_for_use_other" id="patient_indications_for_use_other">
</div>




{!!  Former::text('patient_questions')->ngModel('newRequest.patient_questions')->ngReadonly('editMode')->class('form-control') !!}

<div class="form-group">
    <label for="files">Прикрепить файл</label>
    <div class="input-group">
        <input type="file" class="form-control" ngf-select ng-model="files" name="files" id="files"
               ngf-multiple="true"
               multiple="multiple" ngf-max-size="20MB" ng-disabled="editMode"  >
        </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" uploadfile type="button">Обзор</button>
                  </span>
    </div>
    <!-- Если редактирование -->
    <ul class="event-files" data-ng-if="!event.isNew">
        <li data-ng-repeat="eventFile in files" >
            <i class="fa fa-file-o"></i>
            <span>@{{ eventFile.original_name }}</span>
            <a href="/getfile/@{{ eventFile.file_id }}" >(Скачать)</a>
        </li>
    </ul>
    <!-- Если добавление нового события -->
    <ul class="event-files">
        <li data-ng-repeat="attachedFile in files" >
            <i class="fa fa-file-o"></i>
            <span>@{{ attachedFile.name }}</span>
        </li>
    </ul>
</div>

{!!  Former::text('coordinator_fullname')->ngModel('newRequest.coordinator_fullname')->ngReadonly('true')->class('form-control')->readonly() !!}
{!!  Former::text('coordinator_phone')->ngModel('newRequest.coordinator_phone')->ngReadonly('true')->class('form-control')->readonly() !!}



<div class="form-group" data-ng-if="!event.isNew" >
    {!! Former::checkbox('notify')
        ->text('Уведомить об изменениях всех участников')
        ->label(false)
        ->check()
        ->ngModel('newRequest.notify')
        ->ngDisabled('editMode')!!}
</div>

<div class="form-group" data-ng-if="newRequest.notify" >
    {!! Former::checkbox('send_email')
        ->text('Уведомить по e-mail')
        ->label(false)
        ->check()
        ->ngModel('newRequest.send_email')
        ->ngDisabled('editMode')!!}
</div>