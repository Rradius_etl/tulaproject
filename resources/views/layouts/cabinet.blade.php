<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="<?= App::getLocale(); ?>">
@section('htmlheader')
    @include('layouts.frontend_partials.htmlheader')
@show

<body data-spy="scroll" data-offset="0" data-target="#navigation">
@include('partials.impaired-version')
@include('layouts.main.top-bar')
        <!-- Header with navigation -->
<header>

    <div class="logo container"><a href="/"><img src="/images/logo.png" alt=""></a></div>
    @include('layouts.main.navigation')
</header>

<!-- /END of Header with navigation -->
<!-- Main content -->
<section class="content">


    <div class="row">
        <div class="container" id="min-width-container">

            <div class="col col-md-12 col-sm-12">
                @include('partials.pageheader')

                @section('breadcrumbs')
                @endsection

                @yield('content')


            </div>


        </div>

    </div>
</section>
<!-- /.content -->


@include('layouts.main.footer')

@section('scripts')
@include('layouts.main.scripts')
<script src="/bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
<script>

    /**  Partners page */
    // HOWTO: To use toggle functionality make hidden input after checkbox with class "toggle" with attribute 'data-id' of input field name
    $('input[type=checkbox].toggle').change(function() {
        var id = $(this).data('id')
        console.log( 'id', id )
        if( $(this).is(':checked')) {
            $('[name="'+id+'"]').val(1)
        } else {
            $('[name="'+id+'"]').val(0)
        }
    })

</script>
@show

        <!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
