<head>
    <meta charset="UTF-8">
    <title> @yield('meta_title',  Config::get('website-settings.site-name')) </title>
    <meta name="description" content="@yield('meta_description',  Config::get('website-settings.meta-description') )">
    <meta name="keywords" content="@yield('meta_keywords', Config::get('website-settings.meta-keywords') )">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dastiw1 - bitbuchet.com/dastiw1">







    <!-- FontAwesome -->
    <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- ngDialog -->
    <link rel="stylesheet" href="/bower_components/ng-dialog/css/ngDialog.min.css">
    <!-- bootstraop toggle -->
    <link rel="stylesheet" href=" {{asset('/bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css')}}">

    <!-- slick carousel -->
    <link rel="stylesheet" href="/bower_components/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/bower_components/slick-carousel/slick/slick-theme.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->


    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('/js/smoothscroll.js') }}"></script>
    <script src="{{ asset('/bower_components/slick-carousel/slick/slick.min.js') }}"></script>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
