<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://bitbucket.org/dastiw1/laravel-starter-kit"></a><b>Laravel Starter Kit</b></a>. {{ trans('adminlte_lang::message.descriptionpackage') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="http://imgn.kz">Интернет-Агентство IMAGINE</a>.</strong>
</footer>