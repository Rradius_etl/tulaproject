<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>C</b>Pnl</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Control</b>Panel</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li><a target="_blank" href="/"><i class="fa fa-home"></i>&nbsp;&nbsp;&nbsp;Главная</a></li>


                @if (Sentinel::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{Sentinel::getUser()->avatar()}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Sentinel::check()->first_name  }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{Sentinel::getUser()->avatar()}}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ ( !Sentinel::check()->first_name  == null ) ?  Sentinel::check()->first_name  : Sentinel::check()->email }}
                                    <small>{{  Sentinel::check()->last_login }} </small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="{{route('telemed-centers.index')}}">ТМЦ</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="{{route('consultant-telemed-consult-requests.index')}}">Заявки</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="/calendar">Календарь</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{route('profile')}}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.signout') }}</a>
                                </div>
                            </li>

                        </ul>
                    </li>
                @endif


            </ul>
        </div>
    </nav>
</header>
