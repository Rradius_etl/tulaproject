<?php

$model = camel2dashed($model);

if (!isset($name)) $name = trans("backend.$field") ?>

@if(isset($route))
    @if(Request::input("orderBy")== $field &&(Request::input("sortedBy")=="asc"||Request::input("sortedBy")==null)||(Request::input("orderBy") != $field && Request::input("sortedBy")!=null))
        {{ link_to_route($route.".index",  $name  , array_merge(Request::all(),[ 'orderBy'=>$field, "sortedBy"=> 'desc' ] )) }}
        {!!  (Request::input("orderBy") == $field) ? Html::icon('fa-sort-alpha-asc') : '' !!}
    @else
        {{ link_to_route($route.".index",  $name  , array_merge(Request::all(),[ 'orderBy'=>$field, "sortedBy"=> 'asc' ] )) }}
        {!! (Request::input("orderBy") == $field) ? Html::icon('fa-sort-alpha-desc') : '' !!}
    @endif
@else
    @if(Request::input("orderBy")== $field &&(Request::input("sortedBy")=="asc"||Request::input("sortedBy")==null)||(Request::input("orderBy") != $field && Request::input("sortedBy")!=null))
        {{ link_to_route("$model.index",  $name  , array_merge(Request::all(),[ 'orderBy'=>$field, "sortedBy"=> 'desc' ] )) }}
        {!!  (Request::input("orderBy") == $field) ? Html::icon('fa-sort-alpha-asc') : '' !!}
    @else
        {{ link_to_route("$model.index",  $name  , array_merge(Request::all(),[ 'orderBy'=>$field, "sortedBy"=> 'asc' ] )) }}
        {!! (Request::input("orderBy") == $field) ? Html::icon('fa-sort-alpha-desc') : '' !!}
    @endif
@endif
