<!-- REQUIRED JS SCRIPTS -->


<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>

<!-- Bootstrap-toggle -->
<script src="{{asset('/bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js')}}"></script>


<!-- AdminLTE App -->
<script src="{{ asset('/js/backend.min.js') }}" type="text/javascript"></script>


<script>
      $('.textarea').each(function(e){
            CKEDITOR.replace( this.id, { customConfig: '/js/config.js' });
      });
      $('.textarea').ckeditor(); // if class is prefered.


</script>


@yield('required-scripts-bottom')

<script>
      @yield('inline-javascript')
</script>
