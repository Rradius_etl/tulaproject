<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (Sentinel::check())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{Sentinel::getUser()->avatar()}}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ ( !Sentinel::check()->first_name  == null ) ?  Sentinel::check()->first_name  : Sentinel::check()->email }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
            @endif

            <!-- search form (Optional) -->
            <form action="" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" value="{{Request::input("search")}}"
                           placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">{{ trans('backend.mains') }}</li>
                <!-- Optionally, you can add icons to the links -->
                <li><a href="{{ url('home') }}"><i class='fa fa-home'></i>
                        <span>Панель управления</span></a></li>
                <li><a href="{{ route('admin.pages.index') }}"><i class='fa fa-file-text-o'></i><span>{{ trans('backend.Pages') }}</span></a></li>
                <li><a href="{{ route('admin.news.index') }}"><i class='fa fa-newspaper-o'></i><span>{{ trans('backend.News') }}</span></a></li>
                <li><a href="{{ route('admin.slides.index') }}"><i class='fa fa-picture-o'></i><span>{{ trans('backend.Slides') }}</span></a></li>
                <li><a href="{{ route('admin.external-resources.index') }}"><i class='fa fa-external-link'></i><span>{{ trans('backend.ExternalResources') }}</span></a></li>
                <li><a href="{{ route('admin.files.index') }}"><i class='fa fa-file-text'></i><span>{{ trans('backend.Files') }}</span></a></li>
                <li><a href="{{ route('admin.med-care-facs.index',['orderBy'=>'name','sortedBy'=>'asc']) }}"><i class='fa fa-hospital-o'></i><span>{{ trans('backend.MCF') }}</span></a></li>
                <li><a href="{{ route('admin.telemed-centers.index',['orderBy'=>'med_care_fac_id','sortedBy'=>'asc']) }}"><i class='fa fa-stethoscope'></i><span>{{ trans('backend.TMC') }}</span></a></li>

                <li class="header">Работа с заявками</li>
                <li><a href="{{ route('admin.telemed-register-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']) }}"><i class='fa fa-file-text'></i><span>{{ trans('backend.TelemedRegisterRequests') }}</span></a></li>

                <li><a href="{{ route('admin.reports.index') }}"><i class='fa fa-area-chart'></i><span>{{ trans('backend.ReportConstructor') }}</span></a></li>
                <li class="header">Справочники </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-medkit'></i> <span>Медицинские профили</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.medical-profiles.index') }}">Мед. профили</a></li>
                        <li><a href="{{ route('admin.medical-profile-categories.index') }}">Категории мед. профилей</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('admin.social-statuses.index') }}"><i class='fa fa-child'></i><span>{{ trans('backend.SocialStatuses') }}</span></a></li>
                <li><a href="{{ route('admin.indication-for-uses.index') }}"><i class='fa fa-info-circle'></i><span>{{ trans('backend.IndicationForUses') }}</span></a></li>
                <li><a href="{{ route('admin.consultation-types.index') }}"><i class='fa fa-hourglass-half'></i><span>{{ trans('backend.ConsultationType') }}</span></a></li>
                <li class="header">Системные</li>
                <li><a href="{{ route('admin.interfaces.index') }}"><i class='fa fa-window-restore'></i><span>{{ trans('backend.InterfaceElements') }}</span></a></li>

                <li><a href="{{ route('admin.users.index') }}"><i class='fa fa-user'></i><span>{{ trans('backend.users') }}</span></a></li>
                <li><a href="{{ route('admin.roles.index') }}"><i class='fa fa-lock'></i><span>{{ trans('backend.Roles') }}</span></a></li>
                <li><a href="{{ route('admin.routes.index') }}"><i class='fa fa-ban'></i><span>{{ trans('backend.Routes') }}</span></a></li>
                <li><a href="{{ route('admin.site-settings') }}"><i class='fa fa-cog'></i><span>{{ trans('backend.Site Settings') }}</span></a></li>



                <li class="treeview">
                    <a href="#"><i class='fa fa-line-chart'></i> <span>Статистика</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.analytics.pageviews')}}"> <i class="fa fa-bar-chart fa-fw"></i> Посещаемость портала</a></li>
                        <li><a href="{{route('admin.analytics.online-users')}}"> <i class="fa fa-dashboard fa-fw"></i> Сейчас на сайте</a></li>
                        <li><a href="{{route('admin.analytics.pages')}}" > <i class="fa fa-user fa-fw"></i>Визиты</a></li>

                        {{--<li>
                            <a href="{{route('tracker.stats.index')}}?page=visits" class="{{ Session::get('tracker.stats.page') =='visits' ? 'active' : '' }}" ><i class="fa fa-dashboard fa-fw"></i> @lang("tracker::tracker.visits") <span class="{{ Session::get('tracker.stats.page') =='visits' ? 'fa arrow' : '' }}"></span></a>
                        </li>
                        <li>
                            <a href="{{route('tracker.stats.index')}}?page=summary" class="{{ Session::get('tracker.stats.page') =='summary' ? 'active' : '' }}"><i class="fa fa-bar-chart-o fa-fw"></i> @lang("tracker::tracker.summary") <span class="{{ Session::get('tracker.stats.page') =='summary' ? 'fa arrow' : '' }}"></span></a>
                        </li>
                        <li>
                            <a href="{{route('tracker.stats.index')}}?page=users" class="{{ Session::get('tracker.stats.page') =='users' ? 'active' : '' }}"><i class="fa fa-user fa-fw"></i> @lang("tracker::tracker.users") <span class="{{ Session::get('tracker.stats.page') =='users' ? 'fa arrow' : '' }}"></span></a>
                        </li>
                        <li>
                            <a href="{{route('tracker.stats.index')}}?page=events" class="{{ Session::get('tracker.stats.page') =='events' ? 'active' : '' }}"><i class="fa fa-bolt fa-fw"></i> @lang("tracker::tracker.events") <span class="{{ Session::get('tracker.stats.page') =='events' ? 'fa arrow' : '' }}"></span></a>
                        </li>
                        <li>
                            <a href="{{route('tracker.stats.index')}}?page=errors" class="{{ Session::get('tracker.stats.page') =='errors' ? 'active' : '' }}"><i class="fa fa-exclamation fa-fw"></i>@lang("tracker::tracker.errors") <span class="{{ Session::get('tracker.stats.page') =='errors' ? 'fa arrow' : '' }}"></span></a>
                        </li>--}}
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#"><i class='fa fa-link'></i> <span>Дополнительно</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.blocks.index')}}">Дополнительные блоки</a></li>
                        <li><a href="{{route('admin.generate-sitemap')}}">Сгененрировать карту сайта</a></li>

                    </ul>
                </li>
            </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
