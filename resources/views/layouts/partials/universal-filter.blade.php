@foreach($fields as $field)
    <th>
    <?php
     $filter = reset($field);
     if(count($field)>1)
     {
         $fieldtext = $field['name'];
         $fieldname = key($field);
     }
     else
     {
         $fieldtext = key($field);
         $fieldname = key($field);
     }
    if(!isset($params))
        {
            $params = [];
        }
        ?>
    @if($filter == true)
        @if(Request::input("orderBy")== $fieldname &&(Request::input("sortedBy")=="asc"||Request::input("sortedBy")==null)||(Request::input("orderBy") != $fieldname && Request::input("sortedBy")!=null))
            {{ link_to_route($route, Lang::has("validation.attributes.".$fieldtext) ?  trans("validation.attributes.".$fieldtext) : $fieldtext, array_merge(Request::all(),$params + [ 'orderBy'=>$fieldname, "sortedBy"=> 'desc' ] )) }}
            {!!  (Request::input("orderBy") == $fieldname) ? Html::icon('fa-sort-alpha-asc') : '' !!}
        @else
            {{ link_to_route($route,Lang::has("validation.attributes.".$fieldtext) ?  trans("validation.attributes.".$fieldtext) : $fieldtext, array_merge(Request::all(),$params +[ 'orderBy'=>$fieldname, "sortedBy"=> 'asc' ] )) }}
            {!! (Request::input("orderBy") == $fieldname) ? Html::icon('fa-sort-alpha-desc') : '' !!}
        @endif
    @else
        {{ Lang::has("validation.attributes.".$fieldname) ?  trans("validation.attributes.".$fieldname) : $fieldname}}
    @endif
    </th>
@endforeach