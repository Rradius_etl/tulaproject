<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
$pageInputValue = Input::get('page');
switch ($pageInputValue) {
    case 'visits':
        $title = 'Статистика посещении';
        break;
    case 'summary':
        $title = 'Суммарная статистика';
        break;
    case 'users':
        $title = 'Статистика пользователей';
        break;
    case 'events':
        $title = 'Статистика  событии';
        break;
    case 'errors':
        $title = 'Статистика ошибок';
        break;
    default:
        $title = 'Статистика';

}

?>
@section('htmlheader_title',$title )
@section('contentheader_title',$title )

<html lang="<?= App::getLocale(); ?>">
@section('header-additional')

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="/templates/sb-admin-2/bower_components/morrisjs/morris.css" rel="stylesheet">

    <link href="/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/flags16.css" />
@stop
@section('htmlheader_backend')
    @include('layouts.partials.htmlheader_backend')
@show

<body class="skin-blue sidebar-mini">
<div class="wrapper">
    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
                @section('breadcrumbs')
                @endsection

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <div id="page-wrapper">
                <div class="row">

                    <div class="col-lg-12">
                        <ol class="breadcrumb text-right">
                            <li><span>Выбрать срок: </span></li>
                            <li {{ Session::get('tracker.stats.days') == '0' ? 'class="active"' : '' }}>
                                <a href="{{route('tracker.stats.index')}}?days=0">@lang("tracker::tracker.today")</a>
                            </li>

                            <li {{ Session::get('tracker.stats.days') == '1' ? 'class="active"' : '' }}>
                                <a href="{{route('tracker.stats.index')}}?days=1">@choice("tracker::tracker.no_days",1, ["count" => 1])</a>
                            </li>

                            <li {{ Session::get('tracker.stats.days') == '7' ? 'class="active"' : '' }}>
                                <a href="{{route('tracker.stats.index')}}?days=7">@choice("tracker::tracker.no_days",7, ["count" => 7])</a>
                            </li>

                            <li {{ Session::get('tracker.stats.days') == '30' ? 'class="active"' : '' }}>
                                <a href="{{route('tracker.stats.index')}}?days=30">@choice("tracker::tracker.no_days",30, ["count" => 30])</a>
                            </li>

                            <li {{ Session::get('tracker.stats.days') == '365' ? 'class="active"' : '' }}>
                                <a href="{{route('tracker.stats.index')}}?days=365">@choice("tracker::tracker.no_years",1, ["count" => 1])</a>
                            </li>
                        </ol>

                        @yield('page-contents')
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @yield('page-secondary-contents')
                    </div>
                </div>

                <!-- /.row -->
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->



@section('required-scripts-bottom')
<!-- Core Scripts - Include with every page -->

<script src="/templates/sb-admin-2/bower_components/morrisjs/morris.min.js"></script>
<script src="/templates/sb-admin-2/bower_components/raphael/raphael-min.js"></script>
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/moment/locale/ru.js"></script>

<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
@stop
@section('scripts')
    @include('layouts.partials.scripts_backend')
@show

</body>
</html>
