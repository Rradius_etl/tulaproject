<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<link rel="stylesheet" href="/plugins/iCheck/minimal/blue.css">
<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<!-- datetimepicker -->
<script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Impared version navbar -->
<script src="/js/navbar.js"></script>
<script src="/js/scripts.js"></script>
@yield('additional-scripts')