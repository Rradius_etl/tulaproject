<!--  navbar -->
<nav id="navigation" class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        @if(\App\Models\InterfaceElement::isVisible('menu'))
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    @include('layouts.menu')
                </ul>
                @if(Sentinel::check())
                    @include('partials.sidebar_menu',['second_line' => true])
                @endif
            </div><!--/.nav-collapse -->
        @endif
    </div>
</nav>
