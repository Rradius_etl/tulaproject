{{-- Верхняя плашка над меню и лого --}}
@if(\App\Models\InterfaceElement::isVisible('top_bar'))
    <!-- Fixed top bar -->
    <div id="top-bar" class="navbar navbar-inverse"> <!--  navbar-fixed-top -->
        <div class="container">
            <div class="navbar">
                <ul class="nav navbar-nav">
                    <li><a href="/search"><i class="fa fa-search"></i>&nbsp; Поиск</a></li>
                    @if (Sentinel::check())
                        @if(TmcRoleManager::hasRole("mcf_admin"))<li><a href="{{route('telemed-register-requests.create')}}">Добавить заявку на добавление
                                ТМЦ</a></li>@endif
                        @if(TmcRoleManager::hasRole("admin")||TmcRoleManager::hasRole("coordinator")||TmcRoleManager::hasRole("mcf_admin"))
                        <li><a href="{{route('new-consultation-first')}}">Подать заявку на ТМК</a></li>
                        @endif
                    @endif


                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" onclick="toggleImpaired()" title="Версия сайта для слабовидящих"><i
                                    class="fa fa-font">
                                <small><i class="fa fa-font"></i></small>
                            </i></a></li>
                </ul>

                {{ Form::open(['url' => url('/login'), 'method' => 'POST', 'class' => 'navbar-form navbar-right']) }}
                <div class="form-group">{{ Form::input('email', 'email', null, ['class' => ' form-control input-sm', 'placeholder' => 'Email']) }}</div>
                <div class="form-group">{{ Form::input('password', 'password', null, ['class' => ' form-control input-sm', 'placeholder' => 'Пароль']) }}</div>
                <button type="submit" class="btn btn-sm btn-primary">Войти</button>
                {{ Form::close() }}


            </div><!--/.nav-collapse -->
        </div>
    </div>
@endif
