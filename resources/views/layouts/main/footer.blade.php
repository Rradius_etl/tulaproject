<footer>
    <?php

    $pageViews[] = Tracker::pageViews(60 * 24 * 30, false)->where('method', 'GET')->get()->sum('total');
    $pageViews[] = Tracker::pageViews(60 * 24 * 7, false)->where('method', 'GET')->get()->sum('total');
    $pageViews[] = Tracker::pageViews(60 * 24 * 0, false)->where('method', 'GET')->get()->sum('total');
    ?>
    <div class="container">
        <div class="row">
            <div class="col col-md-3">
                <div class="btn btn-group" role="group">
                    <a class="btn btn-link" role="group" href="/sitemap.xml">Карта сайта </a>

                </div>
            </div>
            <div class="col col-md-6">
                @if( \App\Models\InterfaceElement::isVisible('visits_counter') )
                    <div class="counter" style="color: #fffffa">
                        <small>Счетчик посещений</small>
                        <br>
                        <span class="badge" title="За Месяц"><small>За Месяц: {{$pageViews[0] or ''}}</small></span>
                        <span class="badge" title="За Неделю"><small>За Неделю: {{$pageViews[1] or ''}}</small></span>
                        <span class="badge" title="За Сегодня"><small>За Сегодня: {{$pageViews[2] or ''}}</small></span>
                    </div>
                @endif
            </div>
            <div class="col col-md-3">
                <a href="/about-developer" class="btn btn-square"><b>Информация о разработчике</b></a>
            </div>
        </div>
        <div class="row">
            <div class="copyrights text-center">
                <h5>© Региональный телемедицинский портал 2016</h5>
            </div>
        </div>
    </div>
</footer>