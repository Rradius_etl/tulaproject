<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="<?= App::getLocale(); ?>">

@section('htmlheader')
    @include('layouts.angular.htmlheader')
@show

<body data-spy="scroll" id="calendar-layout" data-offset="0" data-target="#navigation" class="angular-body">

@include('layouts.main.top-bar')
        <!-- Header with navigation -->
<header>

    <div class="logo container"><a href="/"><img src="/images/logo.png" alt=""></a></div>
    @include('layouts.main.navigation')
</header>

<!-- /END of Header with navigation -->
<!-- Main content -->
<section class="content"  ng-app="App" >


        <div class="row">
            <div class="container ">
                <div class="col col-md-12 col-sm-12">
                    @include('partials.pageheader')
                    @section('breadcrumbs')
                    @endsection
                </div>


            </div>
            <div class="container-fluid ">
                <div class="col col-md-12 col-sm-12">

                    @yield('content')
                </div>


            </div>

        </div>
    </section>
<!-- /.content -->






@include('layouts.main.footer')

@section('scripts')
@include('layouts.main.scripts')
@show
        <!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
