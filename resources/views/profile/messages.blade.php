@extends('layouts.cabinet')
<?php $title = "Профиль"; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-white">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="thumbnail"><img src="/images/no-avatar.jpg" alt=""></div>
                        <div class="form-group">
                            <a href="/messages" class="btn btn-default btn-block"> <i class="famess"></i>
                                Сообщения @include('messenger.unread-count')</a>
                        </div>
                        <div class="form-group">
                            <a href="/profile/edit" class="btn btn-default btn-block"> <i
                                        class="faedi"></i> Редактировать профиль</a>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <h2>{{$user->getfName()}}</h2>
                        <hr>

                        <dl class="dl-horizontal">
                            <dt>Должность:</dt>
                            <dd>{!! $user->position !!} </dd>
                        </dl>

                    </div>
                </div>


            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Действия доступные Вам</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Ссылки</th>
                    </tr>
                    @if(TmcRoleManager::hasRole("mcf_admin"))
                        <tr>
                            <td><a href="{{route('telemed-register-requests.index')}}">Ваши заявки на добавление ТМЦ</a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{route('telemed-register-requests.create')}}">Добавить заявку на добавление
                                    ТМЦ</a></td>
                        </tr>
                    @endif
                    @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin"))
                        <tr>
                            <td><a href="{{route('telemed-centers.index')}}">Мои телемедицинские центры</a></td>
                        </tr>
                    @endif
                    @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin")||TmcRoleManager::hasRole("coordinator"))
                        <tr>
                            <td><a href="{{route('abonent-telemed-consult-requests.index')}}">Мои заявки на
                                    телемицинские консультации</a></td>
                        </tr>
                    @endif
                    @if(TmcRoleManager::hasRole("mcf_admin"))
                        <tr>
                            <td><a href="{{route('telemed-consult-requests.index')}}"> Заявки в мое ЛПУ </a></td>
                        </tr>
                    @endif
                    @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin")||TmcRoleManager::hasRole("coordinator"))
                        <tr>
                            <td><a href="{{route('consultant-telemed-consult-requests.index')}}">Заявки моего ТМЦ</a>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endsection