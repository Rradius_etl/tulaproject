<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('email', 'Email',['class' => 'edlabel']) !!}
    {!! Form::text('email', null, ['class' => 'ed-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Имя', 'Имя',['class' => 'edlabel']) !!}
    {!! Form::text('name', null, ['class' => 'ed-control']) !!}
</div>
<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Фамилия', 'Фамилия', ['class' => 'edlabel']) !!}
    {!! Form::text('surname', null, ['class' => 'ed-control']) !!}
</div>
<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Отчество', "Отчество" , ['class' => 'edlabel']) !!}
    {!! Form::text('middlename', null, ['class' => 'ed-control']) !!}
</div>

<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Отчество', "Должность", ['class' => 'edlabel']) !!}
    {!! Form::text('position', null, ['class' => 'ed-control']) !!}
</div>

<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Отчество', "Телефон" , ['class' => 'edlabel']) !!}
    {!! Form::text('phone_number', null, ['class' => 'ed-control']) !!}
</div>

<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Отчество', "Стаж", ['class' => 'edlabel']) !!}
    {!! Form::text('experience', null, ['class' => 'ed-control']) !!}
</div>

<!-- Date Of Birth Field -->
<div class="form-group predit">
    {!! Form::label('Отчество', "Город", ['class' => 'edlabel']) !!}
    {!! Form::text('city', null, ['class' => 'ed-control']) !!}
</div>



<div class="form-group predit">
    {!! Form::label('avatar', "Аватар", ['class' => 'edlabel']) !!}
    <div class="input-group avpredit">
        {!! Form::file('avatar', ['class' => 'ed-controlav']) !!}
        </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="window.document.querySelector('input[name=avatar]').click()" >Обзор</button>
                  </span>
    </div>
</div>




<!-- Submit Field -->
<div class="form-group predit">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-round btn-primary']) !!}
    <a href="{!! route('profile') !!}" class="btn btn-round btn-danger">{{ trans('backend.cancel') }} </a>
</div>
