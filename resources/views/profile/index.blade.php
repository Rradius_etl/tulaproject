@extends('layouts.cabinet')
<?php $title = "Профиль"; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-blue">
        <div class="box-body">
        <h2>Добро пожаловать! Ваша роль в системе - @if(Sentinel::inRole("admin")) Администратор портала @elseif(TmcRoleManager::hasRole("mcf_admin")) Администратор ЛПУ @elseif(TmcRoleManager::hasRole("admin")) Администратор ТМЦ @elseif(TmcRoleManager::hasRole("coordinator")) Координатор ТМЦ @else Пользователь  @endif</h2>
        </div>
        </div>
        <div class="box box-primary box-white">
            <div class="box-body">
                @include('flash::message')
                <div class="row">
                    <div class="col-sm-3">
                        <div class="thumbnail"><img src="{{Sentinel::getUser()->avatar()}}" alt=""></div>
                        <div class="form-group">
                            <a href="/messages" class="btn btn-default btn-block">
                               <i class="fa fa-comments text-primary"></i> Сообщения @include('messenger.unread-count')</a>
                        </div>
                        <div class="form-group">
                            <a href="/profile/edit" class="btn btn-default btn-block"><i class="fa fa-pencil-square-o text-primary"></i> Редактировать профиль</a>
                        </div>
                        @if(Sentinel::getUser()->avatar_file_id != null)
                        <div class="form-group">
                            <a href="/profile/deleteavatar" class="btn btn-default btn-block"> <i
                                        class="fa fa-remove"></i> Удалить аватар</a>
                        </div>
                        @endif
                    </div>
                    <div class="col-sm-5">
                        <h2>{{$user->getfName()}}</h2>
                        <hr>

                        <dl class="dl-horizontal">
                            <dt>Должность:</dt>
                            <dd>{!! $user->position !!} </dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Стаж:</dt>
                            <dd>@if($user->experience != null){{ $user->experience }} @else не указан @endif </dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Номер телефона:</dt>
                            <dd>@if($user->phone_number != null){{ $user->phone_number }} @else не указан @endif </dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Город:</dt>
                            <dd>@if($user->city != null){{ $user->city }} @else не указан @endif </dd>
                        </dl>
                    </div>
                    <div class="col-sm-4">

                        <div class="list-group">
                            @if(TmcRoleManager::hasRole("mcf_admin"))
                                <a class="list-group-item"  href="{{route('telemed-register-requests.index')}}">Ваши заявки на добавление ТМЦ</a>

                                <a class="list-group-item"  href="{{route('telemed-register-requests.create')}}">Добавить заявку на добавление ТМЦ</a>@endif
                            @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin"))
                                <a class="list-group-item"  href="{{route('telemed-centers.index')}}">Мои телемедицинские центры</a>@endif
                            @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin")||TmcRoleManager::hasRole("coordinator"))
                                <a class="list-group-item"  href="{{route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Мои заявки на
                                    телемедицинские консультации</a>@endif
                                <a class="list-group-item"  href="{{ route('consult-patterns.index') }}">Мои шаблоны заполнения</a>
                            @if(TmcRoleManager::hasRole('admin')|TmcRoleManager::hasRole('mcf_admin')|TmcRoleManager::hasRole('coordinator')) <a class="list-group-item list-group-item-success"  href="{{route('new-consultation-first')}}">Создать заявку на телемедицинскую консультацию</a> @endif
                            @if(TmcRoleManager::hasRole("mcf_admin"))
                                <a class="list-group-item list-group-item-info"  href="{{route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}"> Заявки в мое ЛПУ <small>(Доступно только лпу-администратору) </small></a>@endif
                            @if(TmcRoleManager::hasRole("mcf_admin")||TmcRoleManager::hasRole("admin")||TmcRoleManager::hasRole("coordinator"))
                                <a class="list-group-item list-group-item-info"  href="{{route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Заявки моего ТМЦ</a>
                            @endif


                        </div>


                    </div>
                </div>


            </div>
        </div>

    </div>
    @if(Session::has('afterLogin') && Session::get('afterLogin') == true)
        <script>
            $(document).ready(function () {
                $('#login-dialog').modal();
            })
        </script>

        @include('partials.last-incoming-popup')

    @endif
@endsection