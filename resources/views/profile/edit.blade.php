@extends('layouts.cabinet')
<?php $title = "Профиль" . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-white">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($user, ['route' => 'profile.update', 'method' => 'post','files'=>true]) !!}

                    @include('profile.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection