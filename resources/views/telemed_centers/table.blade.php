<table class="table table-striped" id="telemedCenters-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'name', 'name' => 'название ТМЦ'])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'director_fullname', 'name' => trans('backend.Director Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'coordinator_fullname', 'name' => trans('backend.Coordinator Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'coordinator_phone', 'name' => trans('backend.Coordinator Phone')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_fullname', 'name' => trans('backend.Tech Specialist Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_phone', 'name' => trans('backend.Tech Specialist Phone')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_contacts', 'name' => trans('backend.Tech Specialist Contacts')])</th>
        <th>Название терминала</th>
        <th>Номер терминала</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedCenters as $telemedCenter)
        <tr>
            <td>{{ $telemedCenter->name }}</td>
            <td>{{ $telemedCenter->director_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_fullname }}</td>
            <td>{{ $telemedCenter->tech_specialist_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_contacts }}</td>
            <td>{{$telemedCenter->terminal_name}}</td>
            <td>{{$telemedCenter->terminal_number}}</td>
            <td>
                @if(TmcRoleManager::hasRole("mcf_admin")||(TmcRoleManager::hasRole("admin")))
                <div class='btn-group'>
                    <a href="{!! route('telemed-centers.show', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('telemed-centers.edit', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('doctors.index', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-user-md" aria-hidden="true"></i></a>
                    <a href="{!! route('invitations.index', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-meetup" aria-hidden="true"></i></a>
                    <a href="{!! route('telemed-user-pivots.index', [$telemedCenter->id]) !!}" class='btn btn-info btn-xs'  data-toggle="tooltip" title="Управлять пользовтелями ТМЦ" >
                        <i class="glyphicon glyphicon-user"></i></a>
                    <a href="{!! route('polls.index', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'  data-toggle="tooltip" title="Управлять списоком опросов ТМЦ"> <i class='fa fa-bar-chart' aria-hidden="true"></i></a>
                </div>
                @else
                У вас нету прав редактировать эту запись
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>