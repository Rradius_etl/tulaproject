@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedCenters'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-white">
            <div class="box-header">
                <div class="col-sm-4 pull-right">
                    <form action="">

                        <div class="input-group">

                            <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body table-responsive" style="padding: 5px;">
                    @include('common.paginate', ['records' => $telemedCenters])

                    @include('telemed_centers.table')
                    @include('common.paginate', ['records' => $telemedCenters])

            </div>
        </div>
    </div>
@endsection

