@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedCenter'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('telemed_centers.show_fields')
                    <a href="{!! route('telemed-centers.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
