@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedCenter') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($telemedCenter, ['route' => ['telemed-centers.update', $telemedCenter->id], 'method' => 'patch']) !!}

                        @include('telemed_centers.fields',['model'=>$telemedCenter])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection