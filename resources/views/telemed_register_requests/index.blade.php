@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedRegisterRequests'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content full-width">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box   box-white">

            <div class="box-header">
                <div class="col-sm-4 pull-right">
                    <form action="">
                        <div class="input-group">
                            <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span></div>
                    </form>
                </div>
                <div class="pull-right">
                   <a class="btn btn-success btn-social   pull-right" style="" href="{!! route('telemed-register-requests.create') !!}">
                                   <i class="fa fa-plus"></i> Добавить новую заявку
                               </a>
                </div>
            </div>
            <div class="box-body">
                    @include('common.paginate', ['records' => $telemedRegisterRequests])

                    @include('telemed_register_requests.table')
                    @include('common.paginate', ['records' => $telemedRegisterRequests])

            </div>
        </div>
    </div>
@endsection

