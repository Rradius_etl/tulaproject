

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'название ТМЦ:' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->name }}</div>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Адрес:' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->address }}</div>
</div>

<!-- Director Fullname Field -->
<div class="form-group">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->director_fullname }}</div>
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->coordinator_fullname }}</div>
</div>

<!-- Coordinator Phone Field -->
<div class="form-group">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->coordinator_phone }}</div>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->tech_specialist_fullname }}</div>
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->tech_specialist_phone }}</div>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->tech_specialist_contacts }}</div>
</div>




<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Videoconference Equipment') . ':') !!}
    <div class="clearfix"> {{ $telemedRegisterRequest->videoconf_equipment }}</div>
</div>

<?php $da_net = ['нет','да'] ?>
<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Digital Image Demonstration') . ':') !!}
    <div class="clearfix">{{ $da_net[$telemedRegisterRequest->digit_img_demonstration] }}</div>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    <div class="clearfix"> {{ $telemedRegisterRequest->equipment_location }}</div>
</div>



<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->created_at }}</div>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <div class="clearfix">{{ $telemedRegisterRequest->updated_at }}</div>
</div>

<!-- Decision Field -->
<div class="form-group">
    {!! Form::label('decision', trans('backend.Decision') . ':' ) !!}
    <div class="clearfix decision decision-{{$telemedRegisterRequest->decision}}">{{ trans('backend.'.$telemedRegisterRequest->decision) }}</div>
</div>
<style>
    #zebr .form-group .clearfix {
        padding-top: 8px;
    }
</style>