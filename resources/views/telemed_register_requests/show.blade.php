@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedRegisterRequest'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content sml-width">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row" id="zebr">
                    @include('telemed_register_requests.show_fields')
                    <a href="{!! route('telemed-register-requests.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
