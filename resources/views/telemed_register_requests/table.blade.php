<table class="table table-responsive" id="telemedRegisterRequests-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'name', 'name' => 'название ТМЦ'])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'director_fullname', 'name' => trans('backend.Director Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'coordinator_fullname', 'name' => trans('backend.Coordinator Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'coordinator_phone', 'name' => trans('backend.Coordinator Phone')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'decision', 'name' => trans('backend.Decision')])</th>
        <th>Название терминала</th>
        <th>Номер терминала</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'telemedRegisterRequests', 'field'=>'created_at', 'name' =>"Создано в"])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    <?php
    $statuses = ["pending"=>"ожидает просмотра", "agreed"=>"одобрен","rejected"=>"отклонен","completed"=>"завершен"];
    ?>
    @foreach($telemedRegisterRequests as $telemedRegisterRequest)
        <tr>
            <td>{{ $telemedRegisterRequest->name }}</td>
            <td>{{ $telemedRegisterRequest->director_fullname }}</td>
            <td>{{ $telemedRegisterRequest->coordinator_fullname }}</td>
            <td>{{ $telemedRegisterRequest->coordinator_phone }}</td>

            <td class="decision decision-{{$telemedRegisterRequest->decision}}">{!! $statuses[$telemedRegisterRequest->decision] !!}</td>
            <td>{{$telemedRegisterRequest->terminal_name}}</td>
            <td>{{$telemedRegisterRequest->terminal_number}}</td>
            <td>{{$telemedRegisterRequest->created_at}}</td>
            <td>

                <div class='btn-group'>
                    <a href="{!! route('telemed-register-requests.show', [$telemedRegisterRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>

                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>