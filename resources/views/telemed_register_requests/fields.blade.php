
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'название ТМЦ:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Адрес:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Director Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':') !!}
    {!! Form::text('director_fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':') !!}
    {!! Form::text('coordinator_fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Coordinator Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':') !!}
    {!! Form::text('coordinator_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':') !!}
    {!! Form::text('tech_specialist_fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':') !!}
    {!! Form::text('tech_specialist_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':') !!}
    {!! Form::text('tech_specialist_contacts', null, ['class' => 'form-control']) !!}
</div>




<!-- Tech Specialist Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_fullname', trans('backend.Videoconference Equipment') . ':') !!}
    {!! Form::text('videoconf_equipment', null, ['class' => 'form-control']) !!}
</div>

<!-- Tech Specialist Phone Field -->
<input type="hidden" name="digit_img_demonstration" value="0" class="form-control">
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_phone', trans('backend.Digital Image Demonstration') . ':') !!}
    <input name="digit_img_demonstration" id="digit_img_demonstration" type="checkbox" value="1" @if($model->digit_img_demonstration==1) checked @endif>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    {!! Form::text('equipment_location', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('equipment_location', 'Номер терминала:') !!}
    {!! Form::number('terminal_number', null, ['class' => 'form-control','required']) !!}
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_location',  'Название терминала::') !!}
    {!! Form::text('terminal_name', null, ['class' => 'form-control','required']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">

    {!! Form::submit( 'Отправить заявку', ['class' => 'btn btn-round btn-primary']) !!}
    <a href="{!! route('telemed-register-requests.index') !!}" class="btn btn-round btn-danger">{{ trans('backend.cancel') }} </a>

</div>

<script>
    $(document).ready(function(){
        $('input#digit_img_demonstration').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    });
</script>