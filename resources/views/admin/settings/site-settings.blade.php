@extends('layouts.app')
<?php $title = trans('backend.Options') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        @include('common.errors')
        {!! Form::open(['route' => 'admin.site-settings', 'method' => 'patch']) !!}
        @include('admin.settings.fields')

        <div class="form-group">
            {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
            <a href="{!! route('admin.site-settings') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection