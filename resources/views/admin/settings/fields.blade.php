@foreach($settings as $group => $setting)

    <div class="box box-primary box-solid">
        <div class="box-header">
            <div class="box-title"> {{ trans('settings.'.$group) }}</div>
        </div>
        <div class="box-body table-responsive">
            <div class="row">

                @foreach($setting as $config => $value)
                    <?php $item = $group . '.' . $config;    ?>

                    @if( array_key_exists($item, $fieldTypes) )
                        <?php $type = $fieldTypes[$item]; ?>
                        @include('admin.fields.'.$type, ['config'=> str_replace('.', '_', $item), 'value' => $value])

                    @endif
                @endforeach



            </div>

        </div>
    </div>
@endforeach


