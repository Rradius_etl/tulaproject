@extends('layouts.app')
<?php $title = trans('backend.TelemedUserPivot'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body table-responsive">
                <div class="row" style="padding-left: 20px">
                    @include('admin.telemed_user_pivots.show_fields')
                    <a href="{!! route('admin.telemed-user-pivots.index',$TelemedId) !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
