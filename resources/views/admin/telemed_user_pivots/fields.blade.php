<link rel="stylesheet" href="/plugins/select2/select2.css">
<script src="/plugins/select2/select2.js"></script>
<script src="/plugins/select2/i18n/ru.js"></script>
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', "Привязанные пользователь" . ':') !!}
    <select name="user_id"  class="js-data-example-ajax form-control">
        @if(isset($telemedUserPivot)&&$telemedUserPivot->user_id != null)
            <option value="{{$telemedUserPivot->user_id}}" selected="selected">{{$telemedUserPivot->user->email}}</option>
        @else
            <option value="" selected="selected">Выберите пользователя из списка</option>
        @endif
    </select>
</div>


            <input type="hidden" name="telemed_center_id" value="{{$TelemedId}}">
<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', trans('backend.Role') . ':') !!}
    {{Form::Select("value",['coordinator'=>'Координатор',"admin"=>"Администратор"],null,['class'=>'form-control'])}}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.telemed-user-pivots.index',$TelemedId) !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
<script>
    $(document).ready(function(){
        $(".js-data-example-ajax").select2({
            language: "ru",
            ajax: {
                url: "/finduser",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.email,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 2,
        });
    });
</script>