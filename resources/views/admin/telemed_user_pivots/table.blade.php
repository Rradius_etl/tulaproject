<table class="table table-responsive" id="telemedUserPivots-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedUserPivots', 'field'=>'user_id', 'name' => trans('backend.User')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedUserPivots', 'field'=>'value', 'name' => "Роль"])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedUserPivots as $telemedUserPivot)
        <tr>
            <td>{{ $telemedUserPivot->user->email }}</td>
            <td>{{ $telemedUserPivot->value }}</td>
            <td>
                {!! Form::open(['route' => ['admin.telemed-user-pivots.destroy', $telemedUserPivot->id,"TelemedId"=>$TelemedId], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.telemed-user-pivots.show', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.telemed-user-pivots.edit', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>