@extends('layouts.app')
<?php $title = trans('backend.TelemedUserPivot') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                <div class="row">
                    {!! Form::open(['url'=>"/admin/telemed-user-pivots/".$TelemedId]) !!}

                        @include('admin.telemed_user_pivots.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
