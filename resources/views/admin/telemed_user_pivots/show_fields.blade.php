<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', "Пользователь" . ':' ) !!}
    <p>{{ $telemedUserPivot->user->email ." - ".$telemedUserPivot->user->getFname() }}</p>
</div>



<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', "Роль" . ':' ) !!}
    <p>{{ $telemedUserPivot->value }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', "Назначена роль в" . ':' ) !!}
    <p>{{ $telemedUserPivot->updated_at }}</p>
</div>
