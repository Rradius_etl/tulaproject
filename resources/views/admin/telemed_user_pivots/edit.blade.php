@extends('layouts.app')
<?php $title = trans('backend.TelemedUserPivot') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($telemedUserPivot,['route' => ['admin.telemed-user-pivots.update', 'telemedUserPivot'=>$telemedUserPivot->id,'TelemedId'=>$TelemedId ], 'method' => 'patch']) !!}

                        @include('admin.telemed_user_pivots.fields',['TelemedId'=>$TelemedId])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection