<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $page->id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', trans('backend.Title') . ':' ) !!}
    <p>{{ $page->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', trans('backend.Slug') . ':' ) !!}
    <p>{{ $page->slug }}</p>
</div>

<!-- Img Path Field -->
<div class="form-group">
    {!! Form::label('img_path', trans('backend.Img Path') . ':' ) !!}
    <p>{{ $page->img_path }}</p>
</div>

<!-- Short Description Field -->
<div class="form-group">
    {!! Form::label('short_description', trans('backend.Short Description') . ':' ) !!}
    <p>{{ $page->short_description }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', trans('backend.Content') . ':' ) !!}
    <p>{{ $page->content }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', trans('backend.Created By') . ':' ) !!}
    <p>{{ $page->created_by }}</p>
</div>

<!-- Modified By Field -->
<div class="form-group">
    {!! Form::label('modified_by', trans('backend.Modified By') . ':' ) !!}
    <p>{{ $page->modified_by }}</p>
</div>

<!-- Published Field -->
<div class="form-group">
    {!! Form::label('published', trans('backend.Published') . ':' ) !!}
    <p>{{ $page->published }}</p>
</div>

<!-- Hits Field -->
<div class="form-group">
    {!! Form::label('hits', trans('backend.Hits') . ':' ) !!}
    <p>{{ $page->hits }}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', trans('backend.Meta Description') . ':' ) !!}
    <p>{{ $page->meta_description }}</p>
</div>

<!-- Meta Keywords Field -->
<div class="form-group">
    {!! Form::label('meta_keywords', trans('backend.Meta Keywords') . ':' ) !!}
    <p>{{ $page->meta_keywords }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $page->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $page->updated_at }}</p>
</div>

