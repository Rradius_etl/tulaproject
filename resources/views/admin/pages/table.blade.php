<?php $arr=["нет","да"]; $classes = ["label label-danger", "label label-success"]  ?>
<table class="table table-responsive" id="pages-table">
    <thead>
    <th>{{trans('backend.Img Path')}}</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'title', 'name' => trans('backend.Title')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'created_by', 'name' => trans('backend.Created By')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'modified_by', 'name' => trans('backend.Modified By')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'published', 'name' => trans('backend.Published')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'in_menu', 'name' => 'Показать в меню'])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'hits', 'name' => trans('backend.Hits')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'pages', 'field'=>'updated_at', 'name' => trans('backend.Updated At')])</th>
    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($pages as $page)
        <tr>
            <td><img src="{{$page->image('small')}}" alt=""></td>
            <td><a href="{{route('page.view', $page->slug)}}" target="_blank" data-toggle="tooltip" title="Нажмите чтобы перейти на страницу" >{{ $page->title }}</a> </td>
            <td>{{ $page->author->email }}</td>
            <td>{{ $page->lastModifier->email }}</td>
            <td><span class="{{$classes[$page->published]}}"> {{ $arr[$page->published] }} </span></td>
            <td><span class="{{$classes[$page->in_menu]}}"> {{ $arr[$page->in_menu] }} </span></td>
            <td>{{ $page->hits() }}</td>
            <td>{{ $page->updated_at }}</td>

            <td>
                {!! Form::open(['route' => ['admin.pages.destroy', $page->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('page.view', [$page->slug]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.pages.edit', [$page->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>