<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('backend.Title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', trans('backend.Slug') . ':') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Img Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img_path', trans('backend.Img Path') . ':') !!}
    {!! Form::file('img_path', ['class' => 'form-control']) !!}
    @if(!empty($page))
        <div class="col-xs-6 col-md-4">
            <a href="#" class="thumbnail">
                {{ HTML::image($page->image('small'), 'Изображение') }}
            </a>
        </div>
    @endif
</div>

<!-- Short Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short_description', trans('backend.Short Description') . ':') !!}
    {!! Form::textarea('short_description', null, ['class' => 'form-control', 'rows' => 3]) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', trans('backend.Content') . ':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control textarea']) !!}
</div>


<!-- Meta Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('meta_description', trans('backend.Meta Description') . ':') !!}
    {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>

<!-- Meta Keywords Field -->
<div class="form-group col-sm-12">
    {!! Form::label('meta_keywords', trans('backend.Meta Keywords') . ':') !!}
    {!! Form::text('meta_keywords', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-md-6">
    {!! Form::label('published', trans('backend.Published') . ':') !!}
    {!! Form::checkbox('toggle', 0, isset($page) ? $page->published :  0, ['class' => 'form-control toggle', 'data-id' => 'published' ,'data-toggle' => 'toggle', 'data-on' => 'Опубликован', 'data-off' => 'Не опубликован', 'data-onstyle' => 'success' ]) !!}
    {{ Form::hidden('published', isset($page) ? $page->published :  0 ) }}
</div>

<div class="form-group col-sm-12 col-md-6">
    {!! Form::label('in_menu', 'Показать ссылку в меню:') !!}
    {!! Form::checkbox('toggle', 0, isset($page) ? $page->in_menu :  0, ['class' => 'form-control toggle', 'data-id' => 'in_menu' ,'data-toggle' => 'toggle', 'data-on' => 'Показывать', 'data-off' => 'Не показывать', 'data-onstyle' => 'success' ]) !!}
    {{ Form::hidden('in_menu', isset($page) ? $page->in_menu :  0 ) }}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.pages.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
