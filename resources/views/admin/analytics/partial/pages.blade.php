@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')
    <table class="table">
        <thead>

        <tr>
            <th>№ по порядку</th>
            @foreach($headers as $header)
                <th>{{$header}}</th>
            @endforeach
        </tr>

        </thead>
        <tbody>
        <?php $i = 1?>
        @foreach($reports as $item)
            <tr>
                <td> {{$i}}</td>
                <td> {{$item->client_ip}}</td>
                <td> {{ $item->path }}</td>
                <td> {{ $item->hits }}</td>
                <td> {{ $item->updated_at }}</td>
            </tr>
            <?php $i++ ?>
        @endforeach

        </tbody>

    </table>
@endsection