@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')

    @if(count($reports))
    <table class="table">
        <thead>

        <tr>
            <th>№ по порядку</th>
            @foreach($headers as $header)
                <th>{{$header}}</th>
            @endforeach
        </tr>

        </thead>
        <tbody>
        <?php $i = 1?>
        @foreach($reports as $session)
            <tr>
                <td> {{$i}}</td>
                <td>{{$session->email}}</td>
                <td>{{$session->updated_at}}</td>
            </tr>
            <?php $i++ ?>
        @endforeach
        </tbody>

    </table>
    @else
        <div class="text-center">
            <h4>На данный момент нет онлайн пользователей кроме Вас</h4>
        </div>
    @endif
@endsection