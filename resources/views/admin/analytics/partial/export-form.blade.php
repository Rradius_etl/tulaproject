@section('modal_dialog_content')

    @extends('partials.admin-dialog-template')

@section('modal_dialog_title','Экспорт статистики')

<style>
    .select2-dropdown {
        z-index: 99999999;
    }
    .select2-container--default .select2-search__field, .select2-container--default .select2-search.select2-search--inline {
        width: 100% !important;
    }
    .daterangepicker.dropdown-menu {
        z-index: 999999 !important;
    }
</style>
{!! Former::open_vertical()->id('report-export-form')->route('polls.notify')->method('POST')->dataUrl(isset($dataUrl) ? $dataUrl : '/admin/analytics/online-users') !!}

{!! Former::select('format')
    ->options(['pdf' => 'Adobe pdf', 'docx' => 'Microsoft Word', 'odt' => 'OpenDocument Format', 'xls' => 'Microsoft Excel'])
    ->label('Формат вывода')
    ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}


{!!  Former::text('export-daterange')->addGroupClass('export-daterange')
                  ->class('form-control')
                  ->label('Временной диапазон')
                  ->dataExclude('yes')
                  ->append('<i class="fa fa-calendar text-primary"></i>') !!}


{!! Former::hidden('type')->dataExclude('yes') !!}

{!! Former::hidden('from') !!}
{!! Former::hidden('to') !!}
<a href="" class="file-download-link hidden"></a>

{!!  Former::large_round_primary_submit('Скачать отчет') !!}

<button type="button" class="btn btn-round btn-default" data-dismiss="modal">Закрыть</button>


{!! Former::close() !!}


<script>
    var exportDateRange;
    $(document).ready(function () {


        $('#report-export-form input[name=from]').val(dateRangeDefaults.startDate.format('YYYY-MM-DD'));
        $('#report-export-form input[name=to]').val(dateRangeDefaults.endDate.format('YYYY-MM-DD'));


        exportDateRange = $("[name='export-daterange']");
        $(exportDateRange).daterangepicker(dateRangeDefaults);

        $(exportDateRange).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

            $('#report-export-form input[name=from]').val(picker.startDate.format('YYYY-MM-DD'))
            $('#report-export-form input[name=to]').val(picker.endDate.format('YYYY-MM-DD'))


        });



        $(exportDateRange).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('#report-export-form').submit(function (event) {
            var $form = $(this);
            event.preventDefault();

            var dataUrl = $form.data('url');

            var params = $form.find('[data-exclude!=yes]').serializeArray();

            var url = dataUrl + '?' + $.param(params);

            console.log(params, url)

            var a = $('.file-download-link').attr('href', url)[0].click();


        })
    });
</script>

@endsection