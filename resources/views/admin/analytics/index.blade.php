@extends('layouts.app')
<script src="/bower_components/downloadjs/download.min.js"></script>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
<?php

?>
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <!--  ====  SECTION: 3nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">

                <ul class="nav nav-tabs">
                    <li role="presentation"  @if(!Request::is('analytics') ) class="active" @endif><a href="/admin/analytics">Графический</a></li>
                    <li role="presentation" @if(Request::is('analytics/pages') ) class="active" @endif ><a href="/admin/analytics/pages">Таблица</a></li>
                </ul>
                <div class="row">
                    {!! Former::open_vertical() !!}

                    {!!  Former::text('daterange')
                    ->class('form-control')->addGroupClass('col-md-4')
                    ->label(false)
                    ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                    {!! Former::select('diagram')
                    ->options([null => 'Выбрать тип графика', 'linechart'=> 'график', 'barchart'=> 'диаграмма'])
                    ->value(null)
                    ->label(false)
                    ->addGroupClass('col-md-4')
                      ->append('<i class="fa fa-bar-chart"></i>')
                      !!}

                    {!! Former::close() !!}
                </div>




            </div>
            <div class="box-body">


                <div class="clearfix"></div>


                <div class="thumbnail">
                    <img id="tmc_report-chart" src="" alt="">

                </div>
                <div class="download-links" style="display: none;">
                    &nbsp;<a class="btn btn-primary pull-right" style="margin-left: 10px;" id="pdf-download-link" href="#" ><i class="fa fa-picture-o"></i> cкачать как PDF</a> &nbsp;
                    &nbsp;<a  download="Посещаемость портала.jpeg" class="btn btn-primary pull-right" href="#" id="img-download-link" ><i class="fa fa-picture-o"></i>  cкачать как JPEG изображение</a> &nbsp;
                </div>

            </div>
        </div>

    </div>

    @include('admin.analytics.partial.export-form')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        .box-header .form-group {
            margin-bottom: 0;
        }
    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>

        var from = '{{Input::get('from')}}' != '' ? '{{Input::get('from')}}' : moment().subtract(6, 'days').format('YYYY-MM-DD');
        var to = '{{Input::get('to')}}' != '' ? '{{Input::get('to')}}' : moment().format('YYYY-MM-DD');

        if ( (typeof from === 'string' || from instanceof String) ) {
            var startDate  = moment(from,'YYYY-MM-DD');
        }
        if ( (typeof to === 'string' || to instanceof String) ) {
            var endDate =  moment(to, 'YYYY-MM-DD');
        }

        var dateRangeDefaults = {
            timePicker: false,
            startDate: startDate.isValid() ? startDate : moment().subtract(6, 'days'),
            endDate: endDate.isValid() ? endDate :  moment(),

            locale: {
                format: 'DD-MM-YYYY',
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Очистить",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Выбрать диапазон",
                "weekLabel": "W",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            ranges: {
                'За сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }


        $(function () {
            var routBase = location.protocol + '//' + location.host + location.pathname;
            console.log('routBase', routBase)
            moment.locale('ru');

            if( routBase.search('online-users') != -1 ) {
                $('.daterange-wrapper').hide();
            }

            getBarChart( '/admin/analytics/charts?from='+from+ '&to='+to);

            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                    $('#tmc_report-chart').attr('src', '/images/wait.gif' );
                    $('#analytics_report').empty();

                    var params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD'),
                        diagram: $('#diagram').val()
                    };

                    var chart_url = '/admin/analytics/charts?' + $.param(params);
                    getBarChart( chart_url)

                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    window.location.href = routBase;
                });

            });

            $('input[name="daterange"]').trigger('change')


        });


        function getBarChart(chart_url) {
            $.ajax( {
                url: chart_url,
                success: function (data) {

                    $('#tmc_report-chart').attr('src',  chart_url + '&download=url' );
                    $('#img-download-link').attr('href',  chart_url + '&download=url' );
                    $('#pdf-download-link').attr('href',  chart_url + '&download=pdf' );
                    $('.download-links').show();
                },
                error: function (err) {
                    alert(err.responseJSON.error);
                }
            } );
        }

        function openModal(url, title) {
            console.log('open-modal', url, title)

            $('#modal-dialog .modal-title').text(title);

            $('#report-export-form').data('url', url);
            $('#modal-dialog').modal('show')
        }
    </script>
@endsection