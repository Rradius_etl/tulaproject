@extends('layouts.app')
<script src="/bower_components/downloadjs/download.min.js"></script>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
<?php

?>
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <!--  ====  SECTION: 3nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <ul class="nav nav-tabs">
                    <li role="presentation"  @if(Request::is('admin/analytics') ) class="active" @endif><a href="/admin/analytics">Графический</a></li>
                    <li role="presentation" @if(Request::is('admin/analytics/pages') ) class="active" @endif ><a href="/admin/analytics/pages">Таблица</a></li>
                </ul>

                <div class="pull-right"><a href="#" class="export-button" style="display: block;"
                                           onclick="openModal( '{{$url}}', '{{ $title }}')">Выгрузить</a></div>

                <div class="col-md-4 pull-right daterange-wrapper">

                    {!! Former::open_vertical() !!}

                    {!!  Former::text('daterange')
                    ->class('form-control')->addGroupClass('col-md-12')
                    ->label(false)
                    ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                    {!! Former::close() !!}


                </div>

            </div>
            <div class="box-body">


                <div class="clearfix"></div>
                <div id="analytics_report" class="table-responsive"></div>


                <div id="analytics_report-chart"></div>
            </div>
        </div>

    </div>

    @include('admin.analytics.partial.export-form')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        .box-header .form-group {
            margin-bottom: 0;
        }
    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>

        var from = '{{Input::get('from')}}' != '' ? '{{Input::get('from')}}' : moment().subtract(6, 'days').format('YYYY-MM-DD');
        var to = '{{Input::get('to')}}' != '' ? '{{Input::get('to')}}' : moment().format('YYYY-MM-DD');

       console.log( from, to);
        var dateRangeDefaults = {
            timePicker: false,
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),

            locale: {
                format: 'DD-MM-YYYY',
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Очистить",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Выбрать диапазон",
                "weekLabel": "W",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            ranges: {
                'За сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }


        $(function () {
            var routBase = location.protocol + '//' + location.host + location.pathname;

            moment.locale('ru');
            var a = routBase.search('online-users') != -1;

            if( routBase.search('online-users') != -1 ) {
                $('.daterange-wrapper').hide();
                $('.export-daterange').hide();
            }

            $('#analytics_report').load('{{$url}}?format=ajax&from='+from+ '&to='+to);

            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {
                    $(exportDateRange).data('daterangepicker').setStartDate(picker.startDate);
                    $(exportDateRange).data('daterangepicker').setEndDate(picker.endDate);

                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                    var params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD')
                    };

                    $('#analytics_report').load(routBase + '?format=ajax&' +  $.param(params));

                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    window.location.href = routBase;
                });

            });

            $('input[name="daterange"]').trigger('change')


        });



        function openModal(url, title) {
            console.log('open-modal', url, title)

            $('#modal-dialog .modal-title').text(title);

            $('#report-export-form').data('url', url);
            $('#modal-dialog').modal('show')
        }
    </script>
@endsection