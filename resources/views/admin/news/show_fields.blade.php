<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $news->id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', trans('backend.Title') . ':' ) !!}
    <p>{{ $news->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', trans('backend.Slug') . ':' ) !!}
    <p>{{ $news->slug }}</p>
</div>

<!-- Img Path Field -->
<div class="form-group">
    {!! Form::label('img_path', trans('backend.Img Path') . ':' ) !!}
    <p>{{ $news->img_path }}</p>
</div>

<!-- Short Description Field -->
<div class="form-group">
    {!! Form::label('short_description', trans('backend.Short Description') . ':' ) !!}
    <p>{{ $news->short_description }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', trans('backend.Content') . ':' ) !!}
    <p>{{ $news->content }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', trans('backend.Created By') . ':' ) !!}
    <p>{{ $news->created_by }}</p>
</div>

<!-- Modified By Field -->
<div class="form-group">
    {!! Form::label('modified_by', trans('backend.Modified By') . ':' ) !!}
    <p>{{ $news->modified_by }}</p>
</div>

<!-- Published Field -->
<div class="form-group">
    {!! Form::label('published', trans('backend.Published') . ':' ) !!}
    <p>{{ $news->published }}</p>
</div>

<!-- Hits Field -->
<div class="form-group">
    {!! Form::label('hits', trans('backend.Hits') . ':' ) !!}
    <p>{{ $news->hits }}</p>
</div>

<!-- Source Field -->
<div class="form-group">
    {!! Form::label('source', trans('backend.Source') . ':' ) !!}
    <p>{{ $news->source }}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', trans('backend.Meta Description') . ':' ) !!}
    <p>{{ $news->meta_description }}</p>
</div>

<!-- Meta Keywords Field -->
<div class="form-group">
    {!! Form::label('meta_keywords', trans('backend.Meta Keywords') . ':' ) !!}
    <p>{{ $news->meta_keywords }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $news->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $news->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', trans('backend.Deleted At') . ':' ) !!}
    <p>{{ $news->deleted_at }}</p>
</div>

