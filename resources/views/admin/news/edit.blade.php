@extends('layouts.app')
<?php $title = trans('backend.News') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($news, ['route' => ['admin.news.update', $news->id], 'method' => 'patch','files' => true]) !!}

                        @include('admin.news.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>

@endsection