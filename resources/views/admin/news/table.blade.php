<?php $arr = ["нет", "да"];
$classes = ["label label-danger", "label label-success"] ?>
<table class="table table-responsive" id="news-table">
    <thead>
    <th>{{ trans('backend.Img Path') }}</th>
    <th>@include('layouts.partials.filter', ['model' => 'news', 'field'=>'title', 'name' => trans('backend.Title')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'news', 'field'=>'created_by', 'name' => trans('backend.Created By')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'news', 'field'=>'modified_by', 'name' => trans('backend.Modified By')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'news', 'field'=>'published', 'name' => trans('backend.Published')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'news', 'field'=>'hits', 'name' => trans('backend.Hits')])</th>
    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($news as $news)
        <tr>
            <td><img src="{{$news->image('small')}}" alt=""></td>
            <td>{{ $news->title }}</td>
            <td>@if($news->author != null) {{$news->author->email }} @else пользователь был удален @endif</td>
            <td>@if($news->lastModifier != null) {{$news->lastModifier->email }} @else пользователь был удален @endif</td>
            <td><span class="{{$classes[$news->published]}}"> {{ $arr[$news->published] }} </span></td>
            <td>{{ $news->hits }}</td>
            <td>
                {!! Form::open(['route' => ['admin.news.destroy', $news->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.news.show', [$news->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.news.edit', [$news->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>