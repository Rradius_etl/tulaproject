@extends('layouts.app')
<?php $title = trans('backend.News'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('admin.news.create') !!}">
                                   <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                               </a>
                </div>
            </div>
            <div class="box-body table-responsive">
                    @include('common.paginate', ['records' => $news])

                    @include('admin.news.table')
                    @include('common.paginate', ['records' => $news])

            </div>
        </div>
    </div>
@endsection

