<table class="table table-responsive" id="medicalProfiles-table">
    <thead>
    <th>@include('layouts.partials.filter', ['model' => 'medicalProfiles', 'field'=>'name', 'name' => trans('backend.Name')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'medicalProfiles', 'field'=>'category_id', 'name' => trans('backend.Category Id')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($medicalProfiles as $medicalProfile)
        <tr>
            <td>{{ $medicalProfile->name }}</td>
            <td>{{ $medicalProfile->medicalProfileCategory->name }}</td>
            <td>
                {!! Form::open(['route' => ['admin.medical-profiles.destroy', $medicalProfile->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.medical-profiles.show', [$medicalProfile->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.medical-profiles.edit', [$medicalProfile->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>