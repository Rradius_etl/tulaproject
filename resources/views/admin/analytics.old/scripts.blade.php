<link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<style>
    .box-header .form-group {
        margin-bottom: 0;
    }
</style>
<script src="/plugins/daterangepicker/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>

    var dateRangeDefaults = {
        timePicker: false,
        startDate: moment().subtract(6, 'days'),
        endDate: moment(),

        locale: {
            format: 'DD-MM-YYYY',
            "separator": " - ",
            "applyLabel": "Применить",
            "cancelLabel": "Очистить",
            "fromLabel": "От",
            "toLabel": "До",
            "customRangeLabel": "Выбрать диапазон",
            "weekLabel": "W",
            "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            "firstDay": 1
        },
        ranges: {
            'Последний 1 день': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
            'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
            'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };

    $(function () {
        var $form = $('#analytics-form'),
                $link = '{{Request::url()}}',
                $currentpage = {{Input::get('page', 1)}};
        moment.locale('ru');



        $('input[name="daterange"]').each(function (idx, el) {

            $(el).daterangepicker(dateRangeDefaults);

            $(el).on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));


                $form.find('[name=from]').val(picker.startDate.format('YYYY-MM-DD'));
                $form.find('[name=to]').val(picker.endDate.format('YYYY-MM-DD'));
                var params = {
                    from: picker.startDate.format('YYYY-MM-DD'),
                    to: picker.endDate.format('YYYY-MM-DD'),
                    page: $currentpage
                };

                var urlParams = $.param( params);
                console.log($link, $currentpage, urlParams);

                window.location.href = $link + '?' + urlParams;


            });

            $(el).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

        });


    });

</script>