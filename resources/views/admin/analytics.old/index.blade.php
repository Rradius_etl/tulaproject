@extends('layouts.app')
<?php  $title = 'Статистика сайта'?>
@section('htmlheader_title', $title)


@section('contentheader_title' , $title)

@section('content')
    <div class="container spark-screen">
        <div class="row">


            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Самые посещаемые страницы</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        @if(count($mostVisitedPages) > 0)
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Заголовок траницы</th>
                                    <th style="width: 40px">Просмотров</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($mostVisitedPages as $index=> $page)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        <td><a href="{{URL::to($page['url'])}}">{{$page['pageTitle']}}</a></td>

                                        <td><span class="badge bg-light-blue">{{$page['pageViews']}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">Нет данных</div>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col-md-10  -->
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-info">
                        Другие аналитические данные:
                    </a>
                    <a href="{{ route('analytics.visitors-and-pageviews') }}" class="list-group-item">Просмотры и посещения </a>
                    <a href="{{ route('analytics.new-visitors') }}" class="list-group-item">Сеансы новых и вернувшихся пользователей </a>
                    <a href="{{ route('analytics.session-duration') }}" class="list-group-item">Продолжительность сессии  </a>
                    <a href="{{ route('analytics.total-visitors-and-pageviews') }}" class="list-group-item"> Количество визитов </a>
                    <a href="{{ route('analytics.top-referrers') }}" class="list-group-item"> Источники трафика </a>
                </div>
            </div>

        </div>
    </div>
@endsection
