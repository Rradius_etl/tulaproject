@extends('layouts.app')
<?php  $title = 'Статистика сайта >> Источники трафика'?>
@section('htmlheader_title', $title)


@section('contentheader_title' , $title)

@section('content')
    <div class="container spark-screen">
        <div class="row">


            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <div class="col-md-8 pull-left">
                            <h4 class="box-title"></h4>
                        </div>

                        <div class="col-md-4 pull-right">
                            {!! Former::open_vertical(Request::path())->unsafe()->id('analytics-form') !!}

                            {!!  Former::text('daterange')
                            ->class('form-control')
                            ->dataId('reports')
                            ->label(false)
                            ->append('<i class="fa fa-calendar text-primary"></i>') !!}
                            {!! Former::hidden('from')->value(Input::get('from','')) !!}
                            {!! Former::hidden('to')->value(Input::get('to','')) !!}
                            {!! Former::close() !!}
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        @if(count($data) > 0)

                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>URL</th>
                                    <th style="width: 40px">Переходов</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $searchEngines = ['google', 'yandex', 'yahoo', 'bing', 'yandex.ru', 'vk.com/away.php' ,'go.mail.ru', 'rambler']  ?>
                                @foreach($data as $index=> $page)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        @if($page['url'] == '(direct)')
                                            <td><span class="label label-info">По прямой ссылке</span></td>
                                        @elseif( in_array($page['url'], $searchEngines ) )
                                            <td><span class="label label-success">{{ explode('/', $page['url'])[0]}}</span></td>
                                        @else

                                            <td><a target="_blank" href="http://{{$page['url']}}">
                                                    http://{{ $page['url']}}</a></td>
                                        @endif

                                        <td><span class="badge bg-light-blue">{{$page['pageViews']}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>


                        @else
                            <div class="text-center">Нет данных</div>
                        @endif

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col-md-10  -->
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-info">
                        Другие аналитические данные:
                    </a>
                    <a href="{{ route('analytics.visitors-and-pageviews', [], false) }}"
                       class="list-group-item {{ Request::is(substr(route('analytics.visitors-and-pageviews', [] ,false), 1)) ? 'active' : '' }}">Просмотры
                        и посещения </a>
                    <a href="{{ route('analytics.new-visitors', [], false) }}"
                       class="list-group-item {{ Request::is(substr(route('analytics.new-visitors', [] ,false), 1)) ? 'active' : '' }}">Сеансы
                        новых и вернувшихся пользователей </a>
                    <a href="{{ route('analytics.session-duration', [], false) }}"
                       class="list-group-item {{ Request::is(substr(route('analytics.session-duration', [] ,false), 1)) ? 'active' : '' }}">Продолжительность
                        сессии </a>
                    <a href="{{ route('analytics.total-visitors-and-pageviews', [], false) }}"
                       class="list-group-item {{ Request::is(substr(route('analytics.total-visitors-and-pageviews', [] ,false), 1)) ? 'active' : '' }}">
                        Количество визитов </a>
                    <a href="{{ route('analytics.top-referrers', [], false) }}"
                       class="list-group-item {{ Request::is(substr(route('analytics.top-referrers', [] ,false), 1)) ? 'active' : '' }}">
                        Источники трафика </a>
                </div>
            </div>

        </div>
    </div>
    @include('admin.analytics.scripts')
@endsection
