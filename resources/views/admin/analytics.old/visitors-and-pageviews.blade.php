@extends('layouts.app')
<?php  $title = 'Статистика сайта >> Просмотры и посещения'?>
@section('htmlheader_title', $title)


@section('contentheader_title' , $title)

@section('content')
    <div class="container spark-screen">
        <div class="row">


            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <div class="col-md-8 pull-left">
                            <h4 class="box-title">  </h4>
                        </div>

                        <div class="col-md-4 pull-right">
                            {!! Former::open_vertical(Request::path())->unsafe()->id('analytics-form') !!}

                            {!!  Former::text('daterange')
                            ->class('form-control')
                            ->dataId('reports')
                            ->label(false)
                            ->append('<i class="fa fa-calendar text-primary"></i>') !!}
                            {!! Former::hidden('from')->value(Input::get('from','')) !!}
                            {!! Former::hidden('to')->value(Input::get('to','')) !!}
                            {!! Former::close() !!}
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        @if(count($data) > 0)
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Заголовок траницы</th>
                                    <th style="width: 40px">Просмотров</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($data as $index=> $page)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        <td><a href="{{URL::to($page['url'])}}">{{$page['pageTitle']}}</a></td>

                                        <td><span class="badge bg-light-blue">{{$page['pageViews']}}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        @else
                            <div class="text-center">Нет данных</div>
                        @endif
                            <?php echo $data->render(); ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col-md-10  -->
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-info">
                        Другие аналитические данные:
                    </a>
                    <a href="{{ route('analytics.visitors-and-pageviews', [], false) }}" class="list-group-item {{ Request::is(substr(route('analytics.visitors-and-pageviews', [] ,false), 1)) ? 'active' : '' }}">Просмотры и посещения </a>
                    <a href="{{ route('analytics.new-visitors', [], false) }}" class="list-group-item {{ Request::is(substr(route('analytics.new-visitors', [] ,false), 1)) ? 'active' : '' }}">Сеансы новых и вернувшихся пользователей </a>
                    <a href="{{ route('analytics.session-duration', [], false) }}" class="list-group-item {{ Request::is(substr(route('analytics.session-duration', [] ,false), 1)) ? 'active' : '' }}">Продолжительность сессии  </a>
                    <a href="{{ route('analytics.total-visitors-and-pageviews', [], false) }}" class="list-group-item {{ Request::is(substr(route('analytics.total-visitors-and-pageviews', [] ,false), 1)) ? 'active' : '' }}"> Количество визитов </a>
                    <a href="{{ route('analytics.top-referrers', [], false) }}" class="list-group-item {{ Request::is(substr(route('analytics.top-referrers', [] ,false), 1)) ? 'active' : '' }}"> Источники трафика </a>
                </div>
            </div>

        </div>
    </div>
   @include('admin.analytics.scripts')
@endsection
