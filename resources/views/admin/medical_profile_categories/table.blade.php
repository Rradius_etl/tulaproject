<table class="table table-responsive" id="medicalProfileCategories-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'medicalProfileCategories', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($medicalProfileCategories as $medicalProfileCategory)
        <tr>
            <td>{{ $medicalProfileCategory->name }}</td>
            <td>
                {!! Form::open(['route' => ['admin.medical-profile-categories.destroy', $medicalProfileCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.medical-profile-categories.show', [$medicalProfileCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.medical-profile-categories.edit', [$medicalProfileCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>