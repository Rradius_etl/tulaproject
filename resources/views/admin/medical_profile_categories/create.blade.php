@extends('layouts.app')
<?php $title = trans('backend.MedicalProfileCategory') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                <div class="row">
                    {!! Form::open(['route' => 'admin.medical-profile-categories.store']) !!}

                        @include('admin.medical_profile_categories.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
