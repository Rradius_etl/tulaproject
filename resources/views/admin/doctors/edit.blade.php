@extends('layouts.app')
<?php $title = trans('backend.Doctor') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($doctor, ['route' => ['admin.doctors.update',$TelemedId, $doctor->id], 'method' => 'patch']) !!}

                        @include('admin.doctors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection