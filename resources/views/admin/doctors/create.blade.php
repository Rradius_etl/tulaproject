@extends('layouts.app')
<?php $title = trans('backend.Doctors') . ' &#187; ' .'Добавить Врача'; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['admin.doctors.store',$TelemedId]]) !!}

                        @include('admin.doctors.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
