<table class="table table-responsive" id="externalResources-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'externalResources', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'externalResources', 'field'=>'link', 'name' => trans('backend.Link')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'externalResources', 'field'=>'order', 'name' => trans('backend.Order')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'externalResources', 'field'=>'logo_path', 'name' => trans('backend.Logo Path')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($externalResources as $externalResource)
        <tr>
            <td>{{ $externalResource->name }}</td>
            <td>{{ $externalResource->link }}</td>
            <td>{{ $externalResource->order }}</td>
            <td><img src="{{$externalResource->logo()}}" alt=""></td>
            <td>
                {!! Form::open(['route' => ['admin.external-resources.destroy', $externalResource->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.external-resources.show', [$externalResource->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.external-resources.edit', [$externalResource->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>