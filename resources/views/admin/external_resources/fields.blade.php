<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', trans('backend.Name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', trans('backend.Link') . ':') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo_path', trans('backend.Logo Path') . ':') !!}
    {!! Form::file('logo_path', ['class' => 'form-control']) !!}
    @if(!empty($externalResource))
        <div class="col-xs-6 col-md-4">
            <a href="#" class="thumbnail">
                {{ HTML::image($externalResource->logo('small'), 'Логотип') }}
            </a>
        </div>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.external-resources.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
