<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $externalResource->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{{ $externalResource->name }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', trans('backend.Link') . ':' ) !!}
    <p>{{ $externalResource->link }}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', trans('backend.Order') . ':' ) !!}
    <p>{{ $externalResource->order }}</p>
</div>

<!-- Logo Path Field -->
<div class="form-group">
    {!! Form::label('logo_path', trans('backend.Logo Path') . ':' ) !!}
    <p>{{ $externalResource->logo_path }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $externalResource->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $externalResource->updated_at }}</p>
</div>

