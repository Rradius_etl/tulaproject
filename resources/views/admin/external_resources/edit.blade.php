@extends('layouts.app')
<?php $title = trans('backend.ExternalResource') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($externalResource, ['route' => ['admin.external-resources.update', $externalResource->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.external_resources.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection