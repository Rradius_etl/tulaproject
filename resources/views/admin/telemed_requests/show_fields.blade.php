<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $telemedRequest->id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', trans('backend.Title') . ':' ) !!}
    <p>{{ $telemedRequest->title }}</p>
</div>

<!-- Importance Field -->
<div class="form-group">
    {!! Form::label('importance', trans('backend.Importance') . ':' ) !!}
    <p>{{ $telemedRequest->importance }}</p>
</div>

<!-- Decision At Field -->
<div class="form-group">
    {!! Form::label('decision_at', trans('backend.Decision At') . ':' ) !!}
    <p>{{ $telemedRequest->decision_at }}</p>
</div>

<!-- Event Type Field -->
<div class="form-group">
    {!! Form::label('event_type', trans('backend.Event Type') . ':' ) !!}
    <p>{{ $telemedRequest->event_type }}</p>
</div>

<!-- Attached File Id Field -->
<div class="form-group">
    {!! Form::label('attached_file_id', trans('backend.Attached File Id') . ':' ) !!}
    <p>{{ $telemedRequest->attached_file_id }}</p>
</div>

<!-- Initiator Id Field -->
<div class="form-group">
    {!! Form::label('initiator_id', trans('backend.Initiator Id') . ':' ) !!}
    <p>{{ $telemedRequest->initiator_id }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', trans('backend.Start Date') . ':' ) !!}
    <p>{{ $telemedRequest->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', trans('backend.End Date') . ':' ) !!}
    <p>{{ $telemedRequest->end_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $telemedRequest->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $telemedRequest->updated_at }}</p>
</div>

