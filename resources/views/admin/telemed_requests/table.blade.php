<table class="table table-responsive" id="telemedRequests-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'title', 'name' => trans('backend.Title')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'importance', 'name' => trans('backend.Importance')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'decision_at', 'name' => trans('backend.Decision At')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'event_type', 'name' => trans('backend.Event Type')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'attached_file_id', 'name' => trans('backend.Attached File Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'initiator_id', 'name' => trans('backend.Initiator Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'start_date', 'name' => trans('backend.Start Date')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRequests', 'field'=>'end_date', 'name' => trans('backend.End Date')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedRequests as $telemedRequest)
        <tr>
            <td> {{ $telemedRequest->title }}</td>
            <td> {{ $telemedRequest->importance }}</td>
            <td> {{ $telemedRequest->decision_at }}</td>
            <td> {{ $telemedRequest->event_type }}</td>
            <td> {{ $telemedRequest->attached_file_id }}</td>
            <td> {{ $telemedRequest->initiator_id }}</td>
            <td> {{ $telemedRequest->start_date }}</td>
            <td> {{ $telemedRequest->end_date }}</td>
            <td>
                {!! Form::open(['route' => ['admin.telemed-requests.destroy', $telemedRequest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.telemed-requests.show', [$telemedRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.telemed-requests.edit', [$telemedRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>