@extends('layouts.app')
<?php $title = trans('backend.TelemedRequest') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                <div class="row">
                    {!! Form::open(['route' => 'admin.telemed-requests.store']) !!}

                        @include('admin.telemed_requests.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
