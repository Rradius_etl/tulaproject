<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('backend.Title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Importance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importance', trans('backend.Importance') . ':') !!}
    {!! Form::text('importance', null, ['class' => 'form-control']) !!}
</div>

<!-- Decision At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decision_at', trans('backend.Decision At') . ':') !!}
    {!! Form::date('decision_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_type', trans('backend.Event Type') . ':') !!}
    {!! Form::text('event_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Attached File Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('attached_file_id', trans('backend.Attached File Id') . ':') !!}
    {!! Form::number('attached_file_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Initiator Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('initiator_id', trans('backend.Initiator Id') . ':') !!}
    {!! Form::number('initiator_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', trans('backend.Start Date') . ':') !!}
    {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', trans('backend.End Date') . ':') !!}
    {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.telemed-requests.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
