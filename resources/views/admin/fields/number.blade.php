<div class="form-group col-sm-12">
    {!! Form::label($config, trans('settings.' . $config) . ':') !!}
    {!! Form::number($config, $value, ['class' => 'form-control']) !!}
</div>
