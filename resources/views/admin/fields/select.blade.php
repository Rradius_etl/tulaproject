<div class="form-group col-sm-12">
    {!! Form::label($config, trans('settings.' . $config) . ':') !!}
    {!! Form::select($config, $selectOptions[$item], $value, ['class' => 'form-control']) !!}
</div>
