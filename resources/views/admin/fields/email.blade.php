<div class="form-group col-sm-12">
    {!! Form::label($config, trans('settings.' . $config) . ':') !!}
    {!! Form::email($config, $value, ['class' => 'form-control']) !!}
</div>
