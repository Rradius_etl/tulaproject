<div class="form-group col-sm-12">
    {!! Form::label($config, trans('settings.' . $config) . ':') !!}
    {!! Form::color($config, $value, ['class' => 'form-control']) !!}
</div>
