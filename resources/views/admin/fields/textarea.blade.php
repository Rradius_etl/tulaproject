<div class="form-group col-sm-12">
    {!! Form::label($config, trans('settings.' . $config) . ':') !!}
    {!! Form::textarea($config, $value, ['class' => 'form-control', 'rows' => 3]) !!}
</div>
