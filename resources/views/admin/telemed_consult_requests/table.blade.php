<table class="table table-responsive" id="telemedConsultRequests-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'request_id', 'name' => trans('backend.Request Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'comment', 'name' => trans('backend.Comment')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'uid_code', 'name' => trans('backend.Uid Code')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'uid_year', 'name' => trans('backend.Uid Year')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'uid_id', 'name' => trans('backend.Uid Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'decision', 'name' => trans('backend.Decision')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'telemed_center_id', 'name' => trans('backend.Telemed Center Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'decision_at', 'name' => trans('backend.Decision At')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedConsultRequests as $telemedConsultRequest)
        <tr>
            <td>{{ $telemedConsultRequest->request_id }}</td>
            <td>{{ $telemedConsultRequest->comment }}</td>
            <td>{{ $telemedConsultRequest->uid_code }}</td>
            <td>{{ $telemedConsultRequest->uid_year }}</td>
            <td>{{ $telemedConsultRequest->uid_id }}</td>
            <td>{{ $telemedConsultRequest->decision }}</td>
            <td>{{ $telemedConsultRequest->telemed_center_id }}</td>
            <td>{{ $telemedConsultRequest->decision_at }}</td>
            <td>
                {!! Form::open(['route' => ['admin.telemed-consult-requests.destroy', $telemedConsultRequest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.telemed-consult-requests.show', [$telemedConsultRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.telemed-consult-requests.edit', [$telemedConsultRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>