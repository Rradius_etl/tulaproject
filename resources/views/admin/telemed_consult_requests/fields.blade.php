<!-- Request Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('request_id', trans('backend.Request Id') . ':') !!}
    {!! Form::number('request_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Comment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comment', trans('backend.Comment') . ':') !!}
    {!! Form::text('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Uid Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uid_code', trans('backend.Uid Code') . ':') !!}
    {!! Form::number('uid_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Uid Year Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uid_year', trans('backend.Uid Year') . ':') !!}
    {!! Form::number('uid_year', null, ['class' => 'form-control']) !!}
</div>

<!-- Uid Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uid_id', trans('backend.Uid Id') . ':') !!}
    {!! Form::number('uid_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Decision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decision', trans('backend.Decision') . ':') !!}
    {!! Form::text('decision', null, ['class' => 'form-control']) !!}
</div>

<!-- Telemed Center Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telemed_center_id', trans('backend.Telemed Center Id') . ':') !!}
    {!! Form::number('telemed_center_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Decision At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decision_at', trans('backend.Decision At') . ':') !!}
    {!! Form::date('decision_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.telemed-consult-requests.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
