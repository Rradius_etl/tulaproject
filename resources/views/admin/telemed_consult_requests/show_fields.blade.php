<!-- Request Id Field -->
<div class="form-group">
    {!! Form::label('request_id', trans('backend.Request Id') . ':' ) !!}
    <p>{{ $telemedConsultRequest->request_id }}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', trans('backend.Comment') . ':' ) !!}
    <p>{{ $telemedConsultRequest->comment }}</p>
</div>

<!-- Uid Code Field -->
<div class="form-group">
    {!! Form::label('uid_code', trans('backend.Uid Code') . ':' ) !!}
    <p>{{ $telemedConsultRequest->uid_code }}</p>
</div>

<!-- Uid Year Field -->
<div class="form-group">
    {!! Form::label('uid_year', trans('backend.Uid Year') . ':' ) !!}
    <p>{{ $telemedConsultRequest->uid_year }}</p>
</div>

<!-- Uid Id Field -->
<div class="form-group">
    {!! Form::label('uid_id', trans('backend.Uid Id') . ':' ) !!}
    <p>{{ $telemedConsultRequest->uid_id }}</p>
</div>

<!-- Decision Field -->
<div class="form-group">
    {!! Form::label('decision', trans('backend.Decision') . ':' ) !!}
    <p>{{ $telemedConsultRequest->decision }}</p>
</div>

<!-- Telemed Center Id Field -->
<div class="form-group">
    {!! Form::label('telemed_center_id', trans('backend.Telemed Center Id') . ':' ) !!}
    <p>{{ $telemedConsultRequest->telemed_center_id }}</p>
</div>

<!-- Decision At Field -->
<div class="form-group">
    {!! Form::label('decision_at', trans('backend.Decision At') . ':' ) !!}
    <p>{{ $telemedConsultRequest->decision_at }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $telemedConsultRequest->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $telemedConsultRequest->updated_at }}</p>
</div>

