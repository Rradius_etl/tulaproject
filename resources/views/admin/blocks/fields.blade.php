<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('backend.Title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Block Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('block_content', trans('backend.Block Content') . ':') !!}
    {!! Form::textarea('block_content', null, ['class' => 'form-control textarea']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.blocks.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
