<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', trans('backend.Title') . ':' ) !!}
    <p>{{ $block->title }}</p>
</div>

<!-- Block Content Field -->
<div class="form-group">
    {!! Form::label('block_content', trans('backend.Block Content') . ':' ) !!}
    <p>{{ $block->block_content }}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', trans('backend.Order') . ':' ) !!}
    <p>{{ $block->order }}</p>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $block->id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $block->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $block->updated_at }}</p>
</div>

