<table class="table table-responsive" id="blocks-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'blocks', 'field'=>'title', 'name' => trans('backend.Title')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'blocks', 'field'=>'order', 'name' => trans('backend.Order')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($blocks as $block)
        <tr>
            <td>{{ $block->title }}</td>
            <td>{{ $block->order }}</td>
            <td>
                {!! Form::open(['route' => ['admin.blocks.destroy', $block->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.blocks.show', [$block->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.blocks.edit', [$block->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>