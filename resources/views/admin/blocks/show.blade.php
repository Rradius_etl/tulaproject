@extends('layouts.app')
<?php $title = trans('backend.Block'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body table-responsive">
                <div class="row" style="padding-left: 20px">
                    @include('admin.blocks.show_fields')
                    <a href="{!! route('admin.blocks.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
