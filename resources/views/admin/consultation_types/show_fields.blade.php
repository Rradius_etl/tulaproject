<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{!! $consultationType->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{!! $consultationType->name !!}</p>
</div>

<!-- Days Field -->
<div class="form-group">
    {!! Form::label('days', trans('backend.Days') . ':' ) !!}
    <p>{!! $consultationType->days !!}</p>
</div>

<!-- Hours Field -->
<div class="form-group">
    {!! Form::label('hours', trans('backend.Hours') . ':' ) !!}
    <p>{!! $consultationType->hours !!}</p>
</div>

<!-- Minutes Field -->
<div class="form-group">
    {!! Form::label('minutes', trans('backend.Minutes') . ':' ) !!}
    <p>{!! $consultationType->minutes !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{!! $consultationType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{!! $consultationType->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', trans('backend.Deleted At') . ':' ) !!}
    <p>{!! $consultationType->deleted_at !!}</p>
</div>

