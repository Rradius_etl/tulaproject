<table class="table table-responsive" id="consultationTypes-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'consultationTypes', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'consultationTypes', 'field'=>'days', 'name' => trans('backend.Days')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'consultationTypes', 'field'=>'hours', 'name' => trans('backend.Hours')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'consultationTypes', 'field'=>'minutes', 'name' => trans('backend.Minutes')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($consultationTypes as $consultationType)
        <tr>
            <td>{!! $consultationType->name !!}</td>
            <td>{!! $consultationType->days !!}</td>
            <td>{!! $consultationType->hours !!}</td>
            <td>{!! $consultationType->minutes !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.consultation-types.destroy', $consultationType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.consultation-types.show', [$consultationType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.consultation-types.edit', [$consultationType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>