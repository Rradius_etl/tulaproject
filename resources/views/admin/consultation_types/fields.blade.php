<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', trans('backend.Name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('days', trans('backend.Days') . ':') !!}
    {!! Form::number('days', null, ['class' => 'form-control', 'min' => 0, 'max' => 127] ) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('minutes', trans('backend.Minutes') . ':') !!}
    {!! Form::number('minutes', null, ['class' => 'form-control', 'min' => 0, 'max' => 60] ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('hours', trans('backend.Hours') . ':') !!}
    {!! Form::number('hours', null, ['class' => 'form-control', 'min' => 0, 'max' => 23] ) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.consultation-types.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
