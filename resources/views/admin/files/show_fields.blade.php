<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $file->id }}</p>
</div>

<!-- Original Name Field -->
<div class="form-group">
    {!! Form::label('original_name', trans('backend.Original Name') . ':' ) !!}
    <p>{{ $file->original_name }}</p>
</div>

<!-- File Path Field -->
<div class="form-group">
    {!! Form::label('file_path', trans('backend.File Path') . ':' ) !!}
    <p>{{ $file->file_path }}</p>
</div>

<!-- Extension Field -->
<div class="form-group">
    {!! Form::label('extension', trans('backend.Extension') . ':' ) !!}
    <p>{{ $file->extension }}</p>
</div>

<!-- Downloads Count Field -->
<div class="form-group">
    {!! Form::label('downloads_count', trans('backend.Downloads Count') . ':' ) !!}
    <p>{{ $file->downloads_count }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', trans('backend.User Id') . ':' ) !!}
    <p>{{ $file->user->emal }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $file->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $file->updated_at }}</p>
</div>

