<!-- File Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', trans('backend.File') . ':') !!}
    {!! Form::file('file', ['class' => 'form-control']) !!}

</div>

<!-- Original Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comment',  'Комментарий к файлу:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control', 'max' => 500]) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.files.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
