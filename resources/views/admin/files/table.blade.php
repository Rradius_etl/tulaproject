<table class="table table-responsive" id="files-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'original_name', 'name' => trans('backend.Original Name')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'file_path', 'name' => trans('backend.File Path')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'extension', 'name' => trans('backend.Extension')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'downloads_count', 'name' => trans('backend.Downloads Count')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'user_id', 'name' => trans('backend.User Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'files', 'field'=>'created_at', 'name' => 'Загружен в'])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($files as $file)
        <tr>
            <td>{{ $file->original_name }}</td>
            <td>{{ $file->file_path }}</td>
            <td>{{ $file->extension }}</td>
            <td>{{ $file->downloads_count }}</td>
            <td>{{ $file->user->email }}</td>
            <td>{{ $file->created_at }}</td>
            <td>
                {!! Form::open(['route' => ['admin.files.destroy', $file->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a target="_blank" download title="Скачать" href=" @if($file->is_public == false){{ route('files.get', [$file->id]) }} @else /{{$file->file_path}} @endif" class='btn btn-default btn-xs'><i class="glyphicon glyphicon glyphicon-download-alt"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>