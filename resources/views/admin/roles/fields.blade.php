 
<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', trans('backend.Slug') . ':') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', trans('backend.Name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
 
 <!-- Permissions Field -->

<div class="col col-sm-12">
    <div class="row">
        <div class="col col-sm-12">
            {!! Form::label('permissions', 'Permissions:') !!}
            {!! Form::button("добавить", ['class' => 'pull-right btn btn-sm btn-info','onclick'=>"addelem()"]) !!}
        </div>

    </div>

    <?php $i = 0; ?>
    <div class="col" id="add">
        @if(isset($role))

            @foreach($role->permissions as $k=>$v)

                <div class="row">
                    <div class="form-group col-sm-4">
                        {!! Form::text('permissionsKey['.$i.']', $k, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-sm-4">
                        {{ Form::select('permissionsValue['.$i.']',
                        [  false => "нет",
                           true => "да"], $v , ['class' => 'form-control']
                        )  }}
                    </div>
                    <div class="form-group col-sm-4">
                        {!! Form::button("удалить", ['class' => 'btn btn-danger','onclick'=>"destroyAll(this)"]) !!}
                    </div>
                </div>

                <?php $i++ ?>
            @endforeach

        @endif
    </div>
</div>
<div id="deleteditems">

</div>
<script>
    var i = {{$i}};
    function destroyAll(elem) {
        elem.parentNode.parentNode.parentNode.removeChild(elem.parentNode.parentNode);
    }
    function addelem() {
        $("#add").append('<div class="row" ><div class="form-group col-sm-4"><input name="permissionsKey[' + i + ']" type="text" class="form-control"></div> <div class="form-group col-sm-4"><select name="permissionsValue[' + i + ']" class="form-control">' +
                '<option value="0">нет</option><option value="1">да</option></select></div><div class="form-group col-sm-4"><input type="button" onclick="destroyAll(this)" class="btn btn-danger" value="удалить"></div><div>');
        i++;

    }
</script>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.roles.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
