{!! $roles->render() !!}
<table class="table table-responsive" id="roles-table">
    <thead>
    <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'name', 'name' => trans('backend.Name')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'slug', 'name' => trans('backend.Slug')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'permissions', 'name' => trans('backend.Permissions')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'created_at', 'name' => trans('backend.Created At')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'updated_at', 'name' => trans('backend.Updated At')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($roles as $role)
        <tr>
            <td>{{ $role->name }}</td>
            <td>{{ $role->slug }}</td>
            <td>
            @if( !empty($role->permissions) )
                @foreach($role->permissions as $k=>$v)
                     {{$k}} -  {{$v}}
                @endforeach
            @endif
            </td>
            <td>{{ $role->created_at }}</td>
            <td>{{ $role->updated_at }}</td>
            <td>
                {!! Form::open(['route' => ['admin.roles.destroy', $role->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roles.show', [$role->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.roles.edit', [$role->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $roles->render() !!}