@extends('layouts.app')
<?php $title = trans('backend.adding_new') . trans('backend.Role'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                <div class="row">
                    {!! Form::open(['route' => 'admin.roles.store']) !!}

                        @include('admin.roles.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
