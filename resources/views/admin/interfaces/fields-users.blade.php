<form method="post" action="" autocomplete="off" class="">

    <table class="table">
        <thead>
        <tr>
            <th>Email</th>
            <th>ФИО</th>
            <th>Доступ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->email }}</td>
                <td>{{ $user->getfName() }}</td>
                <td>
                    <div class="flipswitch">
                        <input type="checkbox"
                               name="flipswitch-{{$user->id}}"
                               onchange="changeUsersAccess(this)"
                               data-id="{{$user->id}}"
                               class="flipswitch-cb"
                               id="fs-{{$user->id}}"
                               @if( in_array($user->id, $disabledUsers)) @else checked @endif >
                        <label class="flipswitch-label" for="fs-{{$user->id}}">
                            <div class="flipswitch-inner"></div>
                            <div class="flipswitch-switch"></div>
                        </label>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</form>


<script>
    changeUsersAccess = function (el) {
        var id = $(el).data('id');
        var token =  '{{csrf_token()}}';

        var val = $(el).is(':checked') ? true : false;
        $.ajax('/admin/interfaces/update-users/{{$interfaceElement->id}}',{
            'method' : 'POST',
            'data' : {
                '_token': token,
                'user_id' : id,
                'val' : val
            },
            success: function (data) {
                console.info(data);
            },
            error: function (err) {
                console.error(err)
            }
        })
    }
</script>