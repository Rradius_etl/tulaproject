<table class="table table-responsive" id="interfaceElements-table">
    <thead>
        <th>Название </th>
        <th>Описание </th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($interfaceElements as   $element)
        <tr>
            <td>{{ $element->name }}</td>
            <td>{{ $element->description }}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('admin.interfaces.edit', [$element->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i>изменить</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>