@extends('layouts.app')
<?php $title = trans('backend.InterfaceElements') . ' &#187; ' . $interfaceElement->name . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-white">
           <div class="box-header">
               <ul class="nav nav-tabs">
                   <li role="presentation"  class="active" ><a href="№">Редактирование</a></li>
                   <li role="presentation"><a href="{{route('admin.interfaces.edit-users', $interfaceElement->id)}}">Доступ пользователей</a></li>
               </ul>
           </div>
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($interfaceElement, ['route' => ['admin.interfaces.update', $interfaceElement->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.interfaces.fields.'.$interfaceElement->id)
                        @include('admin.interfaces.role-access')
                        @include('admin.interfaces.user-access')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('additional-scripts')
@section('required-scripts-bottom')
    <link rel="stylesheet" href="/bower_components/multiselect/css/multi-select.css">
    <style>
        body .ms-container {
            width: 100%;
        }
    </style>
    <script src="/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script>


        $('#roles').multiSelect({
            selectableOptgroup: true,
            disabledClass: 'label-info',
            selectableHeader: "<div class='custom-header'>Роли имеющие доступ</div>",
            selectionHeader: "<div class='custom-header'>Роли без доступа</div>"
        })
    </script>
@endsection
@endsection