<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.interfaces.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
