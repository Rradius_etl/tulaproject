@extends('layouts.app')
<?php $title = trans('backend.InterfaceElements'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                 &nbsp;
                </div>
            </div>
            <div class="box-body table-responsive">


                    @include('admin.interfaces.table')


            </div>
        </div>
    </div>
@endsection

