<table class="table table-responsive" id="socialStatuses-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'socialStatuses', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($socialStatuses as $socialStatus)
        <tr>
            <td>{{ $socialStatus->name }}</td>
            <td>
                {!! Form::open(['route' => ['admin.social-statuses.destroy', $socialStatus->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.social-statuses.show', [$socialStatus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.social-statuses.edit', [$socialStatus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>