@extends('layouts.app')

@section('htmlheader_title' , trans('backend.users'))
@section('contentheader_title' , trans('backend.users'))



@section('content')

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary box-solid">
            <div class="box-header">
                <h3 class="box-title"> {{ trans('backend.users')  }} </h3>
                <span class="pull-right">
                    <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('admin.users.create') !!}">
                        <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                    </a>

                </span>
            </div>
            <div class="box-body table-responsive">

                    @include('admin.users.table')

            </div>
        </div>



@endsection

