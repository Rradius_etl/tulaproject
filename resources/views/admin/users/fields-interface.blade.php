<form method="post" action="" autocomplete="off" class="">

    <table class="table">
        <thead>
        <tr>
            <th>Название элемента</th>
            <th>Описание</th>
            <th>Доступ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($interfaceElements as $element)
            <tr>
                <td>{{ $element->name }}</td>
                <td>{{ $element->description }}</td>
                <td>
                    <div class="flipswitch">
                        <input type="checkbox"
                               name="flipswitch-{{$element->id}}"
                               onchange="changeInterfaceAccess(this)"
                               data-id="{{$element->id}}"
                               class="flipswitch-cb"
                               id="fs-{{$element->id}}"
                               @if( in_array($element->id, $disabledInterfaceElements)) @else checked @endif >
                        <label class="flipswitch-label" for="fs-{{$element->id}}">
                            <div class="flipswitch-inner"></div>
                            <div class="flipswitch-switch"></div>
                        </label>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</form>


<script>
    changeInterfaceAccess = function (el) {
        var id = $(el).data('id');
        var token =  '{{csrf_token()}}';

        var val = $(el).is(':checked') ? true : false;
        $.ajax('/admin/users/edit-interface/{{$user->id}}',{
            'method' : 'POST',
            'data' : {
                '_token': token,
                'element_id' : id,
                'val' : val
            },
            success: function (data) {
                console.info(data);
            },
            error: function (err) {
                console.error(err)
            }
        })
    }
</script>