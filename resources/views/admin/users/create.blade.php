@extends('layouts.app')
<?php $title = 'Добавление пользователя'; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')


    @include('adminlte-templates::common.errors')
    <div class="box box-primary box-solid">
        <div class="box-header">
            <h3 class="box-title"> {{ trans('backend.user')  }} </h3>
        </div>

        <div class="box-body table-responsive">
            <div class="row">
                {!! Form::open(['route' => 'admin.users.store']) !!}

                @include('admin.users.fields_add')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
