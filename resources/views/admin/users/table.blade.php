@include('common.paginate', ['records' => $users])
<table class="table table-responsive" id="users-table">
    <thead>
        <th>{{ trans('validation.attributes.email')  }} </th>
        <th>{{ trans('validation.attributes.name')  }}</th>
        <th>{{ trans('validation.attributes.surname')  }}</th>
        <th>{{ trans('validation.attributes.middlename')  }}</th>
        <th>{{ trans('validation.attributes.position')  }}</th>
        <th>{{ trans('backend.lastlogin')  }}</th>
        <th colspan="3"> {{ trans('backend.action')  }}</th>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td> {{ Html::nullOrEmpty($user->email) }} </td>
            <td> {{ Html::nullOrEmpty($user->name) }} </td>
            <td> {{ Html::nullOrEmpty($user->surname) }} </td>
            <td> {{ Html::nullOrEmpty($user->middlename) }} </td>
            <td> {{ Html::nullOrEmpty($user->position) }} </td>
            <td> {{ Html::nullOrEmpty($user->last_login)}}</td>
            <td>
                {!! Form::open(['route' => ['admin.users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-eye"></i></a>
                    <a href="{!! route('admin.users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-pencil"></i></a>
                    {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $users])