@extends('layouts.app')

@section('content')

    <div class="box box-primary box-solid ">
        <div class="box-header">
            <h3 class="box-title"> {{ trans('backend.user')  }} </h3>
        </div>
        <div class="box-body table-responsive">
            <div class="row" style="padding-left: 20px">
              @include('admin.users.show_fields')
              <a href="{!! route('admin.users.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
            </div>
        </div>
    </div>

@endsection
