@extends('layouts.app')
<?php $title = 'Редактировать пользователя ' . $user->email; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

   @include('adminlte-templates::common.errors')
   <div class="box box-primary box-white">
       <div class="box-header">
           <ul class="nav nav-tabs">
               <li role="presentation" ><a href="{{route('admin.users.edit', $user->id)}}">Редактирование</a></li>
               <li role="presentation" class="active" ><a href="#">Элементы интерфейса</a></li>
           </ul>
       </div>

       <div class="box-body table-responsive">
           <div class="row">
               <div class="col col-sm-12">
                   {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) !!}

                   @include('admin.users.fields-interface')

                   {!! Form::close() !!}
               </div>

           </div>
       </div>
   </div>

@endsection

<style>
    .flipswitch {
        position: relative;
        width: 86px;
        -webkit-user-select:none;
        -moz-user-select:none;
        -ms-user-select: none;
    }
    .flipswitch input[type=checkbox] {
        display: none;
    }
    .flipswitch-label {
        display: block;
        overflow: hidden;
        cursor: pointer;
        border: 0px solid #999999;
        border-radius: 50px;
    }
    .flipswitch-inner {
        width: 200%;
        margin-left: -100%;
        -webkit-transition: margin 0.3s ease-in 0s;
        -moz-transition: margin 0.3s ease-in 0s;
        -ms-transition: margin 0.3s ease-in 0s;
        -o-transition: margin 0.3s ease-in 0s;
        transition: margin 0.3s ease-in 0s;
    }
    .flipswitch-inner:before, .flipswitch-inner:after {
        float: left;
        width: 50%;
        height: 24px;
        padding: 0;
        line-height: 24px;
        font-size: 16px;
        color: white;
        font-family: Trebuchet, Arial, sans-serif;
        font-weight: bold;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }
    .flipswitch-inner:before {
        content: "вкл";
        padding-left: 13px;
        background-color: #256799;
        color: #FFFFFF;
    }
    .flipswitch-inner:after {
        content: "выкл";
        padding-right: 13px;
        background-color: #EBEBEB;
        color: #888888;
        text-align: right;
    }
    .flipswitch-switch {
        width: 14px;
        margin: 5px;
        background: #FFFFFF;
        border: 0px solid #999999;
        border-radius: 50px;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 60px;
        -webkit-transition: all 0.3s ease-in 0s;
        -moz-transition: all 0.3s ease-in 0s;
        -ms-transition: all 0.3s ease-in 0s;
        -o-transition: all 0.3s ease-in 0s;
        transition: all 0.3s ease-in 0s;
    }
    .flipswitch-cb:checked + .flipswitch-label .flipswitch-inner {
        margin-left: 0;
    }
    .flipswitch-cb:checked + .flipswitch-label .flipswitch-switch {
        right: 0;
    }
</style>