@extends('layouts.app')
<?php $title = 'Редактировать пользователя ' . $user->email; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')
     
   @include('adminlte-templates::common.errors')
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
   <div class="box box-primary box-white">
        
       @if(!$isActive)
       <a class="btn btn-danger" href="{{ route('manual-activate', $user->id) }}">Активировать пользователя</a>
       @endif
       
       <div class="box-header">
           <ul class="nav nav-tabs">
               <li role="presentation" class="active"><a href="#">Редактирование</a></li>
               <li role="presentation"><a href="{{route('admin.users.edit-interface', $user->id)}}">Элементы интерфейса</a></li>
           </ul>
       </div>

       <div class="box-body table-responsive">
           <div class="row">
               <div class="col col-sm-12">
                   {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) !!}

                   @include('admin.users.fields')

                   {!! Form::close() !!}
               </div>

           </div>
       </div>
   </div>

@endsection