
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position', trans('validation.attributes.position') . ':') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('name', trans('validation.attributes.name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Surname Field -->
<div class="form-group col-sm-4">
    {!! Form::label('surname', trans('validation.attributes.surname') . ':') !!}
    {!! Form::text('surname', null, ['class' => 'form-control']) !!}
</div>

<!-- Middlename Field -->
<div class="form-group col-sm-4">
    {!! Form::label('middlename', trans('validation.attributes.middlename') . ':') !!}
    {!! Form::text('middlename', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group col-sm-12">
    <label for="roles">Роли</label>
    {{ Form::select('roles[]', $roles, [2], ['class'=> 'form-control', 'multiple' => 'multiple']) }}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.users.index') !!}" class="btn btn-default"> {{ trans('backend.cancel') }} </a>
</div>
