<form method="post" action="" autocomplete="off" class="">

   <div class="form-group">
        {!! Former::text('name')->value($user->name) !!}
    </div>

    <div class="form-group">
        {!! Former::text('surname')->value($user->surname)  !!}
    </div>

    <div class="form-group">
        {!! Former::text('middlename')->value($user->middlename)  !!}
    </div>

    <div class="form-group">
        {!! Former::text('position')->value($user->position)  !!}
    </div>
    <div class="form-group">
        {!! Former::text('password') !!}
    </div>


    <div class="form-group">
        <label for="roles">Роли</label>
        {{ Form::select('roles[]', $roles, $current_rules, ['class'=> 'form-control', 'multiple' => 'multiple']) }}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('admin.users.index') !!}" class="btn btn-default"> {{ trans('backend.cancel') }} </a>
    </div>

</form>