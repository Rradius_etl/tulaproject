@extends('layouts.app')
<?php $title = trans('backend.Report') . ' &#187; Передать другой ТМЦ'; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($report,
                    ['route' => ['admin.reports.transferProcess', $report->id],
                     'method' => 'post',
                      'onsubmit' => "return confirm('Вы уверены что хотите передать отчеты этому ТМЦ. Вы потом не можете реактировать или удалить текущий отчет?');"]) !!}


                   <div class="form-group col-sm-12">
                       {!! Former::select('tmc_id')->options($tmcs)->required()
                       ->label('Выберите какие телемедицинские центры включить в отчетность:')
                        !!}
                   </div>
                   <!-- Submit Field -->
                   <div class="form-group col-sm-12">
                       {!! Form::submit( 'Передать выбранным ТМЦ', ['class' => 'btn btn-primary']) !!}

                   </div>
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('required-scripts-bottom')
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <style>
        body .ms-container {
            width: 100%;
        }
    </style>

    <script>

        $('#tmc_id').selectize();

    </script>
@endsection