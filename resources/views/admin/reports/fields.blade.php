<div class="form-group col-sm-12">
    {!! Former::text('name')->value($name)->required() !!}
</div>


<div class="form-group col-sm-12">
    {!! Former::multiselect('rows')->options($reportRows, $defaultSelectedRows)->required()
    ->label('Выберите поля для показа в отчете:')
     !!}
</div>


<div class="form-group col-sm-12">
    {!! Former::multiselect('tmcs')->options($tmcs, $defaultTmcs)->required()
    ->label('Выберите какие телемедицинские центры включить в отчетность:')
     !!}

    <div class="row">
        <br>
        <div class="col-sm-6">
            <button type="button" id="selectAllTmcBtn" class="btn btn-xs btn-success">Выбрать все ТМЦ</button>
        </div>
        <div class="col-sm-6  ">
            <button type="button" id="deselectAllTmcBtn" class="btn pull-right btn-xs btn-danger">Исключить все ТМЦ</button>
        </div>
    </div>
</div>

{!! Form::hidden('report_type', $reportType) !!}
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( 'Сохранить изменения', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.reports.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
    <a href="{!! route('admin.reports.transfer', $report->id) !!}" class="btn btn-info">Передать другой ТМЦ </a>
</div>


<br>
<hr>
<div  class="form-group col-sm-12">
    <p>*
        <small> Пункты с цветом <span class="label label-info">&nbsp;&nbsp;&nbsp;</span> обязательные поля, их нельзя исключить </small>
    </p>
</div>



