
<div class="form-group col-sm-12">
    {!! Former::select('report_type')->options( $reportTypes)->required()->label('Выберите вид отчетности')->value(null) !!}
</div>

{!! Form::hidden('step', 2) !!}
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( 'Далее', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.reports.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>

