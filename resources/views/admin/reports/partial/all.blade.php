@if( $full )
    @extends('layouts.report')
@endif
@section('content')

    <?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
    <?php
    $i = 1;
    $onlyConsultationFields = [
            'create_date',
            'consult_type',
            'cons_uid',
            'medic',
            'cons_tmc_name',
            'consult_doctor',
            'patient_indications_for_use_id',
            'part_count',
            'importance'
    ];
    $onlySeminarFields = [
            'title',
    ];
    $onlyMeetingFields = [
            'title',
    ];
    $tmcFields = [
            'mcf_name',
            'tmc_name',
            'tmc_address'
    ];
    $importances = ["high" => "высокая", "low" => "низкая", "medium" => "средняя"];
    $eventTypes = [
            "meeting" => "совещание",
            "seminar" => "семинар",
            "consultation" => "консультация",
    ];

    ?>
    @if(count($reports) > 0)
        <table class="table table-striped" id="polls-table">
            @include('admin.reports.partial.header', ['headers' => $headers])
            <tbody>
            @foreach($reports as $r)
                @foreach($r['reports'] as $report)
                    <tr>

                        @if(in_array('num', $headers))
                            <td>{{$i}}</td>
                            <?php $i++; ?>
                        @endif

                        @foreach($headers as $header )

                            @if( in_array($header, $headers) && $header != 'num' )

                                @if( in_array($header, $onlyConsultationFields) && $report->event_type !== 'consultation' )
                                    <td class="text-center"><span style="text-align: center">-</span></td>
                                @elseif(in_array($header, $onlySeminarFields) && $report->event_type !== 'seminar')
                                    <td class="text-center"><span style="text-align: center">-</span></td>
                                @elseif(in_array($header, $onlySeminarFields) && $report->event_type !== 'meeting')
                                    <td class="text-center"><span style="text-align: center">-</span></td>
                                @elseif($header == 'initiator_id')
                                    <td> @if($r['tmc']->id != $report->initiator_id) Входящая @else
                                            Исходящая @endif </td>
                                @elseif( $header == 'patient_indications_for_use_id')
                                    <td> @if($report->patient_indications_for_use_id != 'other')
                                            {{$indForUse[$report->patient_indications_for_use_id]}}
                                        @else
                                            Иное: {{$report->patient_indications_for_use_other}}
                                        @endif
                                    </td>
                                @elseif( $header == 'mcf_name')
                                    <td>{{ $r['mcf']->name }}</td>
                                @elseif( $header == 'tmc_name')
                                    <td>{{ $r['tmc']->name  }}</td>
                                @elseif( $header == 'tmc_address')
                                    <td>{{ $r['tmc']->address  }}</td>
                                @elseif( $header == 'event_type')
                                    <td>{{ $eventTypes[$report->event_type] }}</td>
                                @elseif( $header == 'title_meeting')
                                    @if($report->event_type == "meeting")
                                        <td>{{ $report->title  }}</td>
                                    @else
                                        <td class="text-center"><span style="text-align: center">-</span></td>
                                    @endif
                                @elseif( $header == 'title_seminar')
                                    @if($report->event_type == "seminar")
                                        <td>{{ $report->title  }}</td>
                                    @else
                                        <td class="text-center"><span style="text-align: center">-</span></td>
                                    @endif
                                @elseif( $header == 'importance')
                                    @if($report->importance !== null)
                                        <td>{{ $importances[$report->importance] }}</td>
                                    @else
                                        <td class="text-center"><span style="text-align: center">-</span></td>
                                    @endif
                                @else
                                    <td> {{ $report->{$header} }}</td>
                                @endif
                            @endif
                        @endforeach
                    </tr>
            @endforeach
            @endforeach
        </table>
    @else
        <div style="text-align: center;">
            <h3>Нет данных по вашим критериям поиска </h3>
        </div>
        @endif
        </tbody>
@endsection