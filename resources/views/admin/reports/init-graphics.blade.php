@extends('layouts.app')
<?php $title = trans('backend.Report') . ": " . $report->name; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-header">
                <h4 class="box-title">{{  $report->name }}</h4>
            </div>
            <div class="box-body">
                @include('flash::message')
                <div class="row" style="padding-left: 20px;padding-right: 20px;">

                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="{{ route('reports.init.table', $report->id) }}">Таблица</a>
                        </li>
                        <li role="presentation" class="active"><a
                                    href="{{ route('reports.init.graphics', $report->id) }}">График</a></li>
                    </ul>
                    <br>

                    <div class="page-header">
                        {!! Former::open_vertical()->id('draw-graphic-form') !!}

                        <div class="row">
                        {!! Former::select('text_field')
                          ->options($textFields)
                          ->value('tmc_name')
                          ->label('Выбрать текстовое поле')
                          ->addGroupClass('col-md-6')
                           ->append('<i class="fa fa-long-arrow-right text-primary"></i>')

                        !!}


                            @if($report->type != 'all' )
                                {!! Former::select('numeric_field')
                                   ->options($numericFields)
                                   ->value($defaultNumericKey or null)
                                   ->label('Выбрать числовое поле')
                                   ->addGroupClass('col-md-6')
                                    ->append('<i class="fa fa-long-arrow-up text-primary"></i>')
                                 !!}
                            @else
                                {!! Former::select('numeric_field')
                            ->options($numericFields+['count'=>"количество завершенных мероприятий"])
                            ->value($defaultNumericKey or null)
                            ->label('Выбрать числовое поле(только для диаграммы)')
                            ->addGroupClass('col-md-6')
                             ->append('<i class="fa fa-long-arrow-up text-primary"></i>')
                          !!}
                            @endif

                        </div>
                        @if(count($errors) < 1)

                            <div class="row">

                                {!!  Former::text('daterange')
                                ->class('form-control')->addGroupClass('col-md-3')
                                ->dataId('tmc_report')
                                ->dataUrl('/tmc_report/x')
                                ->label('Временной промежуток')
                                ->append('<i class="fa fa-calendar text-primary"></i>') !!}



                                {!! Former::select('diagram')
                                ->options([null => 'Выбрать тип графика', 'linechart'=> 'график', 'barchart'=> 'диаграмма'])
                                ->value(null)
                                ->label('Выбрать тип графика')
                                ->addGroupClass('col-md-3')
                                  ->append('<i class="fa fa-bar-chart"></i>')
                                  !!}

                                {!! Former::select('format')
                               ->addGroupClass('col-md-3')
                               ->options(['pdf' => 'Adobe pdf', 'jpg' => 'JPG'])
                               ->label('Формат вывода')
                               ->value('pdf')
                               ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}

                                <div class="form-group col-md-3"><label class="control-label" for="">&nbsp; </label>
                                    {!!  Former::large_block_primary_button('Скачать отчет')->id('download-btn') !!}
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        @endif

                        {!! Former::close() !!}
                    </div>


                    <div class="page-content thumbnail" id="report-wrapper">
                        <img src="" alt="">

                    </div>

                    <a href="{!! route('admin.reports.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('additional-scripts')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        .box-body {
            min-height: 700px;
        }
    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script>
                @if(count($ranges) == 0)
        var startdate = moment().subtract(6, 'days');
        var enddate = moment();
                @else
        var startdate = moment("{{$ranges[0]['date']}}");
        var enddate = moment("{{$ranges[1]['date']}}");
                @endif
        var dateRangeDefaults = {
                    timePicker: false,
                    startDate: startdate,
                    endDate: enddate,

                    locale: {
                        format: 'DD-MM-YYYY',
                        "separator": " - ",
                        "applyLabel": "Применить",
                        "cancelLabel": "Очистить",
                        "fromLabel": "От",
                        "toLabel": "До",
                        "customRangeLabel": "Выбрать диапазон",
                        "weekLabel": "W",
                        "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                        "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                        "firstDay": 1
                    },
                    ranges: {
                        'За сегодня': [moment(), moment()],
                        'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                        'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                        'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                        'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }
        function openInNewTab(url) {

            var file_path = url;
            var a = document.createElement('A');
            a.href = file_path;
            a.download = "отчет"+window.params.from+"-"+window.params.to+'.'+$('#format').val();
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }

        $(document).ready(function () {

            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {

                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                    $('#tmc_report-chart').attr('src', '/images/wait.gif');

                    /* Собрать данные */
                    var paramsFields = ['text_field', 'numeric_field', 'diagram'];
                    window.params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD')
                    };

                    var formData = $('#draw-graphic-form').serializeArray();

                    $.each(formData, function (index, item) {

                        if ($.inArray(item.name, paramsFields) >= 0 && item.value != '') {

                            window.params[item.name] = item.value;
                        }
                    });

                    var url = '/reports/render/charts/' + {{request()->id}};

                    console.log(url, 'url');

                    var img = $('<img/>', {
                        src: url + '?' + $.param(window.params)
                    });
                    $('#report-wrapper').html(img);

                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

            });

            // Кнопка скачивания
            $('#download-btn').click(function () {
                var url = '/reports/download/charts/' + {{request()->id}} +'/' + $('#format').val() + '?' + $.param(window.params);
                //console.log(url)
                openInNewTab(url);

            })
            setTimeout(function () {
                $('input[name="daterange"]').trigger('click');
                $('.daterangepicker.dropdown-menu .btn-success').trigger('click');
                console.log('qqq')
            }, 400)
        })

    </script>

@endsection