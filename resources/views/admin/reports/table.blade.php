<table class="table table-striped" id="reports-table">
    <thead>
    <th>@include('layouts.partials.filter', ['model' => 'reports', 'field'=>'name', 'name' => trans('backend.Name')])</th>
    <th> {{ trans('backend.User Id')}}</th>
    <th>{{ trans('backend.Tmcs') }}</th>
    <th>@include('layouts.partials.filter', ['model' => 'reports', 'field'=>'type', 'name' => trans('backend.Type')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'reports', 'field'=>'created_at', 'name' => trans('backend.Created At')])</th>
    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td>{!! $report->name !!}</td>
            <td>{!! $report->user->getfName() !!}</td>
            <td>
                <ul class="list-group">
                    <?php  $tmcs = $report->telemedcenters();?>
                    @foreach($tmcs as $key => $telemedcenter)

                        <li class="list-group-item">{{  $telemedcenter->name }}</li>
                        @if ($key == 5)
                            <li class="list-group-item label-default"> ..еще {{ count($tmcs) - 6 }} ТМЦ</li>
                            @break
                        @endif
                    @endforeach
                </ul>

            </td>
            <td>{!! $report->showReadableType() !!}</td>
            <td>{!! $report->created_at !!}</td>
            <td>
                <a href="{{route('reports.init.table', $report->id)}}" class="btn btn-block btn-default btn-xs"
                   style="margin-bottom: 3px;"> <i class="fa fa-table"></i> Таблица</a>
                <a href="{{route('reports.init.graphics', $report->id)}}" class="btn btn-block btn-default btn-xs"
                   style="margin-bottom: 3px;"><i class="fa fa-bar-chart-o"></i> График</a>
                {!! Form::open(['route' => ['admin.reports.destroy', $report->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    @if($report->tmc_id == null)
                        <a href="{!! route('admin.reports.show', [$report->id]) !!}" class='btn btn-block btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i> Просмотр</a>
                        <a href="{!! route('admin.reports.edit', [$report->id]) !!}" class='btn btn-block btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i> Редактировать</a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Удалить', ['type' => 'submit', 'class' => 'btn btn-block btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                    @else
                        <a href="{!! route('admin.reports.show', [$report->id]) !!}" class='btn btn-block btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i> Просмотр </a>
                        <div class="btn btn-block btn-warning btn-xs">Доступ ограничен</div>
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if(count($reports))
    @include('common.paginate', ['records' => $reports])
@endif