<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id',  'ID:' ) !!}
    <p>{{ $telemedCenter->id }}</p>
</div>

<!-- Med Care Fac Id Field -->
<div class="form-group">
    {!! Form::label('med_care_fac_id', trans('backend.Med Care Fac') . ':' ) !!}
    <p>{{ $telemedCenter->medCareFac->name }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'название ТМЦ:' ) !!}
    <p>{{ $telemedCenter->name }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Адрес:' ) !!}
    <p>{{ $telemedCenter->address }}</p>
</div>

<!-- Director Fullname Field -->
<div class="form-group">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':' ) !!}
    <p>{{ $telemedCenter->director_fullname }}</p>
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':' ) !!}
    <p>{{ $telemedCenter->coordinator_fullname }}</p>
</div>

<!-- Coordinator Phone Field -->
<div class="form-group">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':' ) !!}
    <p>{{ $telemedCenter->coordinator_phone }}</p>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':' ) !!}
    <p>{{ $telemedCenter->tech_specialist_fullname }}</p>
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':' ) !!}
    <p>{{ $telemedCenter->tech_specialist_phone }}</p>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':' ) !!}
    <p>{{ $telemedCenter->tech_specialist_contacts }}</p>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Videoconference Equipment') . ':') !!}
    {!! $telemedCenter->videoconf_equipment !!}
</div>


<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Digital Image Demonstration') . ':') !!}
    {!! $telemedCenter->digit_img_demonstration !!}
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    {!! $telemedCenter->equipment_location !!}
</div>



<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $telemedCenter->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $telemedCenter->updated_at }}</p>
</div>

