@extends('layouts.app')
<?php $title = trans('backend.TelemedCenter') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($telemedCenter, ['route' => ['admin.telemed-centers.update', $telemedCenter->id], 'method' => 'patch']) !!}

                        @include('admin.telemed_centers.fields',['model'=>$telemedCenter,'items'=>$items,'userpivots'=>$userpivots])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection