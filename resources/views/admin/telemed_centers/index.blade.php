@extends('layouts.app')
<?php $title = trans('backend.TelemedCenters'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('admin.telemed-centers.create') !!}">
                                   <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                               </a>
                </div>
            </div>
            <div class="box-body table-responsive">
                <form action="">
                    <input type="hidden" name="mode" value="{{$mode}}">

                        <div class="input-group col-sm-3 pull-right">

                            <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                    <div class="input-group col-sm-3 pull-right">
                        {!! Form::select('mcf_id',[''=>'Фильтр по ЛПУ']+$mcfs->toArray(),$mcf_id,['class'=>'form-control']) !!}

                    </div>

                </form>
                <ul class="nav nav-tabs">
                    <li role="presentation"  @if($mode == 'all') class="active" @endif><a href="?mode=all">Все</a></li>
                    <li role="presentation" @if($mode == 'trash') class="active" @endif ><a href="?mode=trash">Удаленные</a></li>
                </ul>
                    @include('common.paginate', ['records' => $telemedCenters])

                    @include('admin.telemed_centers.table')
                    @include('common.paginate', ['records' => $telemedCenters])

            </div>
        </div>
    </div>
@endsection

