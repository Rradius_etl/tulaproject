<table class="table" id="telemedCenters-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'med_care_fac_id', 'name' => trans('backend.Med Care Fac')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'name', 'name' => 'название ТМЦ'])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'director_fullname', 'name' => trans('backend.Director Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'coordinator_fullname', 'name' => trans('backend.Coordinator Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'coordinator_phone', 'name' => trans('backend.Coordinator Phone')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_fullname', 'name' => trans('backend.Tech Specialist Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_phone', 'name' => trans('backend.Tech Specialist Phone')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedCenters', 'field'=>'tech_specialist_contacts', 'name' => trans('backend.Tech Specialist Contacts')])</th>

        <th>Название терминала</th>
        <th>Номер терминала</th>

        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedCenters as $telemedCenter)
        <tr>
            <td>{{ $telemedCenter->medCareFac()->withTrashed()->first()->name }}</td>
            <td>{{ $telemedCenter->name }}</td>
            <td>{{ $telemedCenter->director_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_fullname }}</td>
            <td>{{ $telemedCenter->tech_specialist_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_contacts }}</td>
            <td>{{$telemedCenter->terminal_name}}</td>
            <td>{{$telemedCenter->terminal_number}}</td>
            <td>
                {!! Form::open(['route' => ['admin.telemed-centers.destroy', $telemedCenter->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    @if($mode == 'all')
                    <a href="{!! route('admin.telemed-centers.show', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs' data-toggle="tooltip" title="Посмотреть подробную информацию" >
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{!! route('admin.telemed-centers.edit', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs' data-toggle="tooltip" title="Редактировать ТМЦ">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <a href="{!! route('admin.telemed-user-pivots.index', [$telemedCenter->id]) !!}" class='btn btn-info btn-xs'  data-toggle="tooltip" title="Управлять пользовтелями ТМЦ" >
                        <i class="glyphicon glyphicon-user"></i></a>
                    <a href="{!! route('admin.doctors.index', [$telemedCenter->id]) !!}" class='btn btn-warning btn-xs'  data-toggle="tooltip" title="Управлять списоком врачей ТМЦ" ><i class="fa fa-user-md" aria-hidden="true"></i></a>

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                    @elseif($mode == 'trash')
                        {!! Form::button('<i class="fa fa-unlock" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-info btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                        {{--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['name'=> 'force_delete', 'value'=>true,'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('ВНИМАНИЕ!!! Вы точно хотите окончательно и безвозвратно удалить ТМЦ из базы данных? Отменить операцию невозможно')"]) !!}--}}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>