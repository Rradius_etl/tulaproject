<!-- Med Care Fac Id Field -->
<div class="form-group col-sm-6">
        {!! Form::label('med_care_fac_id', trans('backend.Med Care Fac') . ':') !!}
        {!! Form::text('med_care_fac_id', $telemedRegisterRequest->med_care_fac->name, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>



<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name','название ТМЦ:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Адрес:') !!}
    {!! Form::text('address', null, ['class' => 'form-control','disabled' => 'disabled']) !!}
</div>


<!-- Director Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':') !!}
    {!! Form::text('director_fullname', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':') !!}
    {!! Form::text('coordinator_fullname', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Coordinator Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':') !!}
    {!! Form::text('coordinator_phone', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':') !!}
    {!! Form::text('tech_specialist_fullname', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':') !!}
    {!! Form::text('tech_specialist_phone', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>
<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':') !!}
    {!! Form::text('tech_specialist_contacts', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>
<?php $decisions = [
        'pending' => trans('backend.pending'),
        'agreed' => trans('backend.agreed'),
        'rejected' => trans('backend.rejected'),
] ?>
<!-- Decision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decision', trans('backend.Decision') . ':') !!}
    {!! Form::select('decision',$decisions, null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_fullname', trans('backend.Videoconference Equipment') . ':') !!}
    {!! Form::text('videoconf_equipment', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>



<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    {!! Form::text('equipment_location', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_phone', 'Номер терминала:') !!}
    {!! Form::number('terminal_number', null, ['class' => 'form-control','required','disabled' => 'disabled']) !!}
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_location', 'Название терминала:') !!}
    {!! Form::text('terminal_name', null, ['class' => 'form-control','required','disabled' => 'disabled']) !!}
</div>

<!-- Tech Specialist Phone Field -->
<input type="hidden" name="digit_img_demonstration" value="0" class="form-control">
<div class="form-group col-sm-6">
    {!! Form::label('tech_specialist_phone', trans('backend.Digital Image Demonstration') . ':') !!}
    <input name="digit_img_demonstration" type="checkbox" disabled value="1"
           @if($model->digit_img_demonstration==1) checked @endif>
</div>


@if( $telemedRegisterRequest->decision == 'pending' )

    <div class="form-group col-sm-12">
        <a href="{!! route('admin.telemed-register-requests.agree',$telemedRegisterRequest->id) !!}"
           class="btn btn-success"><i class="fa fa-check"></i> Согласовать </a>
        <a href="{!! route('admin.telemed-register-requests.reject', $telemedRegisterRequest->id) !!}"
           class="btn btn-danger"> <i class="fa fa-close"></i> Отклонить</a>
    </div>
@endif



