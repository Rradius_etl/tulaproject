<?php $da_net = ['нет','да'] ?>
<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'название ТМЦ:' ) !!}
    <p>{{ $telemedRegisterRequest->name }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Адрес:' ) !!}
    <p>{{ $telemedRegisterRequest->address  }}</p>
</div>

<!-- Director Fullname Field -->
<div class="form-group">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->director_fullname }}</p>
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->coordinator_fullname }}</p>
</div>

<!-- Coordinator Phone Field -->
<div class="form-group">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->coordinator_phone }}</p>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->tech_specialist_fullname }}</p>
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->tech_specialist_phone }}</p>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->tech_specialist_contacts }}</p>
</div>

<!-- Decision Field -->
<div class="form-group">
    {!! Form::label('decision', trans('backend.Decision') . ':' ) !!}
    <p>{{ trans('backend.'.$telemedRegisterRequest->decision) }} </p>
</div>


<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Videoconference Equipment') . ':') !!}
    {{ $telemedRegisterRequest->videoconf_equipment }}
</div>


<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Digital Image Demonstration') . ':') !!}
    {{ $da_net[$telemedRegisterRequest->digit_img_demonstration] }}
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    {{ $telemedRegisterRequest->equipment_location }}
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $telemedRegisterRequest->updated_at }}</p>
</div>

