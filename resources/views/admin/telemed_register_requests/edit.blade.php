@extends('layouts.app')
<?php $title = trans('backend.TelemedRegisterRequest') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body table-responsive">
               <div class="row">
                   {!! Form::model($telemedRegisterRequest, ['route' => ['admin.telemed-register-requests.update', $telemedRegisterRequest->id], 'method' => 'patch']) !!}

                        @include('admin.telemed_register_requests.fields',['model'=>$telemedRegisterRequest])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection