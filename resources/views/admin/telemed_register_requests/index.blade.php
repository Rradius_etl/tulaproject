@extends('layouts.app')
<?php $title = trans('backend.TelemedRegisterRequests'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')
        <div class="col-sm-4 pull-right">
            <form action="">
                <label for=""></label>
                <div class="input-group">

                    <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                </div>
            </form>
        </div>
        <div class="clearfix"></div>

        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                    @include('common.paginate', ['records' => $telemedRegisterRequests])

                    @include('admin.telemed_register_requests.table')
                    @include('common.paginate', ['records' => $telemedRegisterRequests])

            </div>
        </div>
    </div>
@endsection

