<table class="table table-responsive" id="telemedRegisterRequests-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'med_care_fac_id', 'name' => trans('backend.Med Care Fac')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'name', 'name' => 'название ТМЦ'])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'director_fullname', 'name' => trans('backend.Director Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'coordinator_fullname', 'name' => trans('backend.Coordinator Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'coordinator_phone', 'name' => trans('backend.Coordinator Phone')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'tech_specialist_fullname', 'name' => trans('backend.Tech Specialist Fullname')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'tech_specialist_phone', 'name' => trans('backend.Tech Specialist Phone')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'tech_specialist_contacts', 'name' => trans('backend.Tech Specialist Contacts')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'decision', 'name' => trans('backend.Decision')])</th>
        <th>Название терминала</th>
        <th>Номер терминала</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedRegisterRequests', 'field'=>'created_at', 'name' =>"Создано в"])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedRegisterRequests as $telemedRegisterRequest)
        <tr>
            <td>{{ $telemedRegisterRequest->med_care_facs->name }}</td>
            <td>{{ $telemedRegisterRequest->name }}</td>
            <td>{{ $telemedRegisterRequest->director_fullname }}</td>
            <td>{{ $telemedRegisterRequest->coordinator_fullname }}</td>
            <td>{{ $telemedRegisterRequest->coordinator_phone }}</td>
            <td>{{ $telemedRegisterRequest->tech_specialist_fullname }}</td>
            <td>{{ $telemedRegisterRequest->tech_specialist_phone }}</td>
            <td>{{ $telemedRegisterRequest->tech_specialist_contacts }}</td>
            <td>@if($telemedRegisterRequest->decision=="pending")<a href="/admin/telemed-register-requests/agree/{{$telemedRegisterRequest->id}}" class="btn-success">Принять</a>
                <a href="/admin/telemed-register-requests/reject/{{$telemedRegisterRequest->id}}" class="btn-danger">Отклонить</a>
                @else
                {{trans('backend.'.$telemedRegisterRequest->decision)}}
                @endif
            </td>
            <td>{{$telemedRegisterRequest->terminal_name}}</td>
            <td>{{$telemedRegisterRequest->terminal_number}}</td>
            <td>{{$telemedRegisterRequest->created_at}}</td>
            <td>
                {!! Form::open(['route' => ['admin.telemed-register-requests.destroy', $telemedRegisterRequest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.telemed-register-requests.show', [$telemedRegisterRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.telemed-register-requests.edit', [$telemedRegisterRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>