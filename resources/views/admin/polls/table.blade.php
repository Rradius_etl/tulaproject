<table class="table table-responsive" id="polls-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'polls', 'field'=>'title', 'name' => trans('backend.Title')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'polls', 'field'=>'author_id', 'name' => trans('backend.Author Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'polls', 'field'=>'active', 'name' => trans('backend.Active')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($polls as $poll)
        <tr>
            <td>{{ $poll->title }}</td>
            <td>{{ $poll->author_id }}</td>
            <td>{{ $poll->active }}</td>
            <td>
                {!! Form::open(['route' => ['admin.polls.destroy', $poll->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.polls.show', [$poll->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.polls.edit', [$poll->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>