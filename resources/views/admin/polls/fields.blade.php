<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('backend.Title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Author Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author_id', trans('backend.Author Id') . ':') !!}
    {!! Form::number('author_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.polls.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
