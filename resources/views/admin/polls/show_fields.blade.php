<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $poll->id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', trans('backend.Title') . ':' ) !!}
    <p>{{ $poll->title }}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', trans('backend.Author Id') . ':' ) !!}
    <p>{{ $poll->author_id }}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', trans('backend.Active') . ':' ) !!}
    <p>{{ $poll->active }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $poll->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $poll->updated_at }}</p>
</div>

