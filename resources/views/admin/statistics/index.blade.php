@extends('layouts.app')
<script src="/bower_components/downloadjs/download.min.js"></script>
<?php $title = trans('backend.Statistics'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-white">
            <div class="box-header">
                <h4 class="box-title">Отчет о проведенных видеоконференциях в разрезе ТМЦ</h4>
                <div class="box-tools pull-right">

                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>

            <div class="box-body table-responsive">
                <div class="row">
                    <div class="col-md-5 pull-left">
                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-8')
                        ->dataId('reports')
                        ->dataUrl('/admin/reports')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::close() !!}
                    </div>

                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                               onclick="openModal('/admin/reports/',1)">Выгрузить</a></div>
                    <div class="clearfix"></div>
                </div>




                <div id="reports" class="table-responsive"></div>


            </div>
        </div>
        <!--  ====  SECTION: 2nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <h4 class="box-title">Отчет по клиническим консультациям в разрезе ТМЦ</h4>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body table-responsive">
                <div class="row">
                    <div class="col-md-5 pull-left">
                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-8')
                        ->dataId('consult_reports')
                        ->dataUrl('/admin/consult_reports')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::close() !!}
                    </div>
                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                                        onclick="openModal('/admin/consult_reports/',2)">Выгрузить</a></div>
                    <div class="clearfix"></div>
                </div>


                <div id="consult_reports" class="table-responsive"></div>


            </div>
        </div>

        <!--  ====  SECTION: 3nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <h5 class="box-title">Отчет по видеоконференциям по отдельному ТМЦ</h5>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="row">


                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-3')
                        ->dataId('tmc_report')
                        ->dataUrl('/admin/tmc_report/x')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::select('tmcs')->options($lists)->label(false)->addGroupClass('col-md-4 ')->dataSimple('yes') !!}

                        {!! Former::select('diagram')
                        ->options([null => 'Выбрать тип графика', 'linechart'=> 'график', 'barchart'=> 'диаграмма'])
                        ->value(null)
                        ->label(false)
                        ->addGroupClass('col-md-3')
                      ->append('<i class="fa fa-bar-chart"></i>')
                      !!}

                        {!! Former::close() !!}



                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                                        onclick="openModal('/admin/tmc_report/',3)">Выгрузить</a></div>
                    <div class="clearfix"></div>
                </div>

                <div id="tmc_report" class="table-responsive"></div>

                <div class="thumbnail">
                    <img id="tmc_report-chart" src="" alt="">

                </div>
                <div class="row download-links" style="display: none;">
                    &nbsp;<a class="btn btn-primary pull-right" style="margin-left: 10px;" id="pdf-download-link" href="#" ><i class="fa fa-picture-o"></i> cкачать как PDF</a> &nbsp;
                    &nbsp;<a  download="Отчет по видеоконференциям по отдельному ТМЦ.jpeg" class="btn btn-primary pull-right" href="#" id="img-download-link" ><i class="fa fa-picture-o"></i>  cкачать как JPEG изображение</a> &nbsp;
                </div>
            </div>
        </div>

        <style>
            .haha {
                width:400px;
                height: 100%;
            }
        </style>
        <!--  ====  SECTION: 4nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <h5 class="box-title">Отчет по видеоконференциям по различным ТМЦ</h5>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="row">


                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-4')
                        ->dataId('tmcs_report')
                        ->dataUrl('/admin/tmcs_report')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::multiselect('list')->options($lists)->value(array_first($lists))
                        ->label(false)->addClass('haha')
                        ->addGroupClass('col-md-5')
                        ->dataToggle('tooltip')->dataPlacement('left')->title('Зажмите кнопку ctrl или shift для множественной выборки') !!}

                        {!! Former::close() !!}

                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                               onclick="openModal('/admin/tmcs_report/',6)">Выгрузить</a></div>
                    <div class="clearfix"></div>

                </div>

                <div id="tmcs_report" class="table-responsive"></div>

            </div>
        </div>


        <!--  ====  SECTION: 5 nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <h4 class="box-title">Отчет по совещаниям в разрезе ТМЦ</h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="row">
                    <div class="col-md-5 pull-left">
                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-8')
                        ->dataId('conference_report')
                        ->dataUrl('/admin/conference_report')
                        ->dataType('meeting')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::close() !!}
                    </div>

                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                               onclick="openModal('/admin/conference_report/',4, 'meeting')">Выгрузить</a>
                    </div>
                </div>

                <div id="conference_report" class="table-responsive"></div>
            </div>
        </div>
        <!--  ====  SECTION: 6 nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">
                <h4 class="box-title">Отчет по обучающим семинарам в разрезе ТМЦ</h4>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="row">
                    <div class="col-md-5 pull-left">
                        {!! Former::open_vertical() !!}

                        {!!  Former::text('daterange')
                        ->class('form-control')->addGroupClass('col-md-8')
                        ->dataId('seminar_report')
                        ->dataUrl('/admin/conference_report')
                        ->dataType('seminar')
                        ->label(false)
                        ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                        {!! Former::close() !!}
                    </div>
                    <div class="col-md-2 pull-right"><a href="#" class="export-button" style="display: block;text-align:right;"
                                                        onclick="openModal('/admin/conference_report/',5,'seminar')">Выгрузить</a>

                </div>

                </div>

                <div id="seminar_report" class="table-responsive"></div>
            </div>
        </div>
    </div>

    @include('reports_and_statistics.admin-export-form')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        .box-header .form-group {
            margin-bottom: 0;
        }
    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
        var pageTitles = [
            '',
            'Отчет о проведенных видеоконференциях в разрезе ТМЦ',
            'Отчет по клиническим консультациям в разрезе ТМЦ',
            'Отчет по видеоконференциям по отдельному ТМЦ',
            'Отчет по совещаниям в разрезе ТМЦ',
            'Отчет по обучающим семинарам в разрезе ТМЦ',
            'Отчет по видеоконференциям по различным ТМЦ',
        ];

        var dateRangeDefaults = {
            timePicker: false,
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),

            locale: {
                format: 'DD-MM-YYYY',
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Очистить",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Выбрать диапазон",
                "weekLabel": "W",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            ranges: {
                'За сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }


        $(document).ready(function () {
            $('form.form-vertical').submit(function (event) {
                event.preventDefault();
            })
            moment.locale('ru');

            $('#report-export-form input[name=from]').val(dateRangeDefaults.startDate.format('YYYY-MM-DD'));
            $('#report-export-form input[name=to]').val(dateRangeDefaults.endDate.format('YYYY-MM-DD'));

            var exportDateRange= $("[name='export-daterange']");
            $(exportDateRange).daterangepicker(dateRangeDefaults);

            $(exportDateRange).on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                $('#report-export-form input[name=from]').val(picker.startDate.format('YYYY-MM-DD'))
                $('#report-export-form input[name=to]').val(picker.endDate.format('YYYY-MM-DD'))


            });

            $("select[name^=tmc][data-simple!='yes']:not([multiple])").select2({
                language: "ru",
                allowClear: true,
                placeholder: "Выберите ТМЦ",
                ajax: {
                    url: "/findtmcs",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term, // search term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 2,
            });

            $(exportDateRange).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#report-export-form').submit(function (event) {
                var $form = $(this);
                event.preventDefault();
                var dataUrl = $form.data('url');
                console.log('dataUrl',dataUrl)
                if (dataUrl == '/admin/tmc_report/') {

                    dataUrl = dataUrl + $form.find('select[name=tmc]').val() + '/';
                    var params = $form.find('[data-exclude!=yes]').serializeArray();
                } else if (dataUrl == '/admin/conference_report/') {

                    var params = $form.find('[data-exclude!=yes]').serializeArray();
                    params.push($form.find('[name=type]').serializeArray()[0]);

                } else if(dataUrl == '/admin/tmcs_report/') {

                    var params = $form.find('[data-exclude!=yes]').serializeArray(),  tmcs = "";
                    var selectedTmcs = $form.find('#tmcs').val();
                    console.log('selectedTmcs',selectedTmcs)
                    params.push ({'qiqiqiqi':'asdasdsdas'});
                    params.push ({
                        'name': 'tmcs',
                        'value': selectedTmcs.join(',')
                    });

                }else {

                    var params = $form.find('[data-exclude!=yes]').serializeArray();

                }


                var url = dataUrl + '?' + $.param(params);



                console.log('download url', params, QueryStringToHash(url),url)

                var a = $('.file-download-link').attr('href', url)[0].click();


            })

            /** Export end */
            $('[data-id=tmc_report]').data('url', '/admin/tmc_report/' + $('[name=tmcs]').val());

            $('[name=tmcs]').change(function () {
                console.log($('[data-id=tmc_report]').data('url'))
                $('[data-id=tmc_report]').data('url', '/admin/tmc_report/' + $(this).val());

            });


            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {
                    console.info(picker.startDate, picker.endDate)

                    $(exportDateRange).data('daterangepicker').setStartDate(picker.startDate);
                    $(exportDateRange).data('daterangepicker').setEndDate(picker.endDate);

                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));



                    var params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD'),
                        diagram: $('#diagram').val()
                    };


                    var url = $(this).data('url');
                    if ($(this).data('type') !== undefined) {
                        params.type = $(this).data('type');
                    }
                    if(url.search( '/admin/tmcs_report') != -1 )
                    {
                        $('#tmc_report-chart').attr('src', '/images/wait.gif' );
                        var tmcs = "";
                        $('#list :selected').each(function(i, selected){
                                if(i == $('#list :selected').length -1 )
                                {
                                    tmcs = tmcs  + selected.value ;
                                }
                                else
                                {
                                    tmcs = tmcs  + selected.value + ",";
                                }

                        });
                        params.tmcs = tmcs;

                    }

                    var selector = $(this).data('id');


                    $('#' + selector).load(url + '?' + $.param(params));
                    console.log('url:', url)
                    // Если Отчет по видеоконференциям по отдельному ТМЦ
                    if( url.search( '/admin/tmc_report') != -1 ) {
                         var chart_url = '/admin/tmc_chart/' +  $('[name=tmcs]').val() + '?' + $.param(params);
                        console.log('chart_url:', chart_url)
                        $.ajax( {
                            url: chart_url,
                            success: function (data) {

                                $('#tmc_report-chart').attr('src',    chart_url + '&download=url' );
                                $('#img-download-link').attr('href',    chart_url + '&download=url' );
                                $('#pdf-download-link').attr('href',  chart_url + '&download=pdf' );
                                $('.download-links').show();
                            },
                            error: function (err) {
                                alert(err.responseJSON.error);
                            }
                        } );
                    } else {
                        console.error('whoops!')
                    }
                    console.log( url, selector)
                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

            });
            $('input[name="daterange"]').trigger('change')

        });




        function openModal(url, titleID, secondaryParam) {

            var param = (typeof  secondaryParam !== undefined ) ? secondaryParam : '';

            if (url == '/admin/tmc_report/') {
                $('#report-export-form .tmc-group').show()
            } else {
                $('#report-export-form .tmc-group').hide()
            }

            if (url == '/admin/tmcs_report/') {
                $('#report-export-form .many-tmc-group').show()
            } else {
                $('#report-export-form .many-tmc-group').hide()
            }


            $('#report-export-form input[name=type]').val(param);


            $('#modal-dialog .modal-title').text(pageTitles[titleID]);

            $('#report-export-form').data('url', url);
            $('#modal-dialog').modal('show')
        }
    </script>
@endsection