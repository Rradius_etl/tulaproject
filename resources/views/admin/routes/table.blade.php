{!! $routes->render() !!}
<table class="table table-responsive" id="rotes-table">
    <thead>
    <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'name', 'name' => trans('backend.Name')])</th>
    <th>@include('layouts.partials.filter', ['model' => 'roles', 'field'=>'path', 'name' => trans('backend.Path')])</th>

    </thead>
    <tbody>
    @foreach($routes as $route)
        <tr>
            @if(  is_null($route['name']) )
                <td> <div class="label label-danger">Не указано</div></td>
            @else
                <td> {{ trans($route['name']) }}</td>
            @endif
            <td>{{ $route['path'] }}</td>

        </tr>
    @endforeach
    </tbody>
</table>
{!! $routes->render() !!}