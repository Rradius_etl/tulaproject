<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{!! $indicationForUse->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{!! $indicationForUse->name !!}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', trans('backend.Order') . ':' ) !!}
    <p>{!! $indicationForUse->order !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{!! $indicationForUse->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{!! $indicationForUse->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', trans('backend.Deleted At') . ':' ) !!}
    <p>{!! $indicationForUse->deleted_at !!}</p>
</div>

