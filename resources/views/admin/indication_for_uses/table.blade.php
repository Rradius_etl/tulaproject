<table class="table table-responsive" id="indicationForUses-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'indicationForUses', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'indicationForUses', 'field'=>'order', 'name' => trans('backend.Order')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($indicationForUses as $indicationForUse)
        <tr>
            <td>{!! $indicationForUse->name !!}</td>
            <td>{!! $indicationForUse->order !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.indication-for-uses.destroy', $indicationForUse->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.indication-for-uses.show', [$indicationForUse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.indication-for-uses.edit', [$indicationForUse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>