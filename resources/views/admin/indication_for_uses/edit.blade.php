@extends('layouts.app')
<?php $title = trans('backend.IndicationForUse') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($indicationForUse, ['route' => ['admin.indication-for-uses.update', $indicationForUse->id], 'method' => 'patch']) !!}

                        @include('admin.indication_for_uses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection