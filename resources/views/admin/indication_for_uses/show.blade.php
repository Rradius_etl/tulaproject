@extends('layouts.app')
<?php $title = trans('backend.IndicationForUse'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.indication_for_uses.show_fields')
                    <a href="{!! route('admin.indication-for-uses.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
