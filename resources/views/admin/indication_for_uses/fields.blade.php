<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', trans('backend.Name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order', trans('backend.Order') . ':') !!}
    {!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.indication-for-uses.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
