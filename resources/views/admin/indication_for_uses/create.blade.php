@extends('layouts.app')
<?php $title = trans('backend.IndicationForUse') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.indication-for-uses.store']) !!}

                        @include('admin.indication_for_uses.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
