<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $slide->id }}</p>
</div>

<!-- Img Path Field -->
<div class="form-group">
    {!! Form::label('img_path', trans('backend.Img Path') . ':' ) !!}
    <p>{{ $slide->img_path }}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', trans('backend.Order') . ':' ) !!}
    <p>{{ $slide->order }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', trans('backend.Content') . ':' ) !!}
    <p>{{ $slide->content }}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', trans('backend.Active') . ':' ) !!}
    <p>{{ $slide->active }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $slide->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $slide->updated_at }}</p>
</div>

