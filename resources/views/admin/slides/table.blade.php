<?php $arr = ["нет", "да"];
$classes = ["label label-danger", "label label-success"] ?>
<table class="table table-responsive" id="slides-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'slides', 'field'=>'img_path', 'name' => trans('backend.Img Path')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'slides', 'field'=>'order', 'name' => trans('backend.Order')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'slides', 'field'=>'active', 'name' => trans('backend.Active')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($slides as $slide)
        <tr>
            <td><img src="{{$slide->image('small')}}" alt=""></td>
            <td>{{ $slide->order }}</td>
            <td><span class="{{$classes[$slide->active]}}"> {{ $arr[$slide->active] }} </span></td>
            <td>
                {!! Form::open(['route' => ['admin.slides.destroy', $slide->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.slides.show', [$slide->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.slides.edit', [$slide->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@include('common.paginate',['records' => $slides])