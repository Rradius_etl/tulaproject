
<!-- Img Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img_path', trans('backend.Img Path') . ':') !!}
    {!! Form::file('img_path', ['class' => 'form-control']) !!}
    <p class="help-block">Разрешение загружаемого изображения желательно должно быть разрешения 1920 на 460 пикселей.  </p>
    @if(!empty($slide))
        <div class="col-xs-12 col-md-12">
            <a href="#" class="thumbnail">
                {{ HTML::image($slide->image('slide'), 'Изображение') }}
            </a>
        </div>
    @endif
</div>
<!-- Order Field -->
<div class="form-group col-sm-2">
    {!! Form::label('order', trans('backend.Order') . ':') !!}
    {!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('active', trans('backend.Active') . ':') !!} <br>
    {!! Form::checkbox('toggle', 0, isset($news) ? $news->active :  1, ['class' => 'form-control toggle', 'data-id' => 'active' ,'data-toggle' => 'toggle', 'data-on' => 'Опубликован', 'data-off' => 'Не опубликован', 'data-onstyle' => 'success' ]) !!}
    {{ Form::hidden('active', isset($news) ? $news->published :  1 ) }}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', trans('backend.Content') . ':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control textarea']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.slides.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
