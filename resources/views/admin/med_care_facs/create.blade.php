@extends('layouts.app')
<?php $title = trans('backend.MedCareFac') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body table-responsive">
                <div class="row">
                    {!! Form::open(['route' => 'admin.med-care-facs.store']) !!}

                        @include('admin.med_care_facs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
