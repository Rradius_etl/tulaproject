<table class="table table-responsive" id="medCareFacs-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'medCareFacs', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'medCareFacs', 'field'=>'code', 'name' => trans('backend.Code')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'medCareFacs', 'field'=>'user_id', 'name' => trans('backend.User')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($medCareFacs as $medCareFac)
        <tr>
            <td>{{ $medCareFac->name }}</td>
            <td>{{ $medCareFac->code }}</td>
            <?php
                $user_name= "";
                if(isset($medCareFac->user))
                    {

                        $user_name = $medCareFac->user->email;
                    }
                else{
                    $user_name = "не выбран";
                }?>
            <td>{{ $user_name }}</td>
            <td>
                {!! Form::open(['route' => ['admin.med-care-facs.destroy', $medCareFac->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    @if($mode == 'all')
                    <a href="{!! route('admin.med-care-facs.show', [$medCareFac->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.med-care-facs.edit', [$medCareFac->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                    @elseif($mode == 'trash')
                    {!! Form::button('<i class="fa fa-unlock" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-info btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>