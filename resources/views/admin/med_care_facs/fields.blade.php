<link rel="stylesheet" href="/plugins/select2/select2.css">
<script src="/plugins/select2/select2.js"></script>
<script src="/plugins/select2/i18n/ru.js"></script>
<!-- Name Field -->
<div class="form-group col-sm-8">
    {!! Form::label('name', trans('backend.Name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-8">
    {!! Form::label('name', "Привязанные пользователь" . ':') !!}
    <select name="user_id"  class="js-data-example-ajax form-control">
        @if(isset($medCareFac)&&$medCareFac->user_id != null)
        <option value="{{$medCareFac->user_id}}" selected="selected">{{$medCareFac->user->email}}</option>
        @else
        <option value="" selected="selected">Выберите пользователя из списка</option>
        @endif
    </select>
</div>
<div class="form-group col-sm-8">
    {!! Form::label('name', "Код" . ':') !!}
    {!! Form::number('code', null, ['class' => 'form-control','min'=>0]) !!}
    </div>
<div class="form-group col-sm-8">
    {!! Form::label('name', "Норма телемедициских консультаций" . ':') !!}
    {!! Form::number('consultations_norm', null, ['class' => 'form-control','min'=>0]) !!}
    {!! Form::label('name', "Ставте значение 0, если хотите указать поле пустым.",['class'=>'alert-success']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.med-care-facs.index') !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
<script>
    $(document).ready(function(){
        $(".js-data-example-ajax").select2({
            language: "ru",
            allowClear: true,
            placeholder: "Выберите пользователя из списка",
            ajax: {
                url: "/finduser",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.email,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 2,
        });
    });
</script>