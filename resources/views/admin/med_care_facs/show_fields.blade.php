<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $medCareFac->id }}</p>
</div>


<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{{ $medCareFac->name }}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', trans('backend.Code') . ':' ) !!}
    <p>{{ $medCareFac->code }}</p>
</div>


<?php
$user_name = "";
$user = $medCareFac->user;
if (isset($user)) {

    $user_name = $user->name . " " . $user->surname . " " . $user->middlename;
} else {
    $user_name = "не выбран";
}?>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.User') . ':' ) !!}
    <p>{{ $user_name }}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $medCareFac->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
    <p>{{ $medCareFac->updated_at }}</p>
</div>

