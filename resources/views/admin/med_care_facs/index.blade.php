@extends('layouts.app')
<?php $title = trans('backend.MedCareFacs'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('admin.med-care-facs.create') !!}">
                                   <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                               </a>
                </div>

            </div>
            <hr>
            <ul class="nav nav-tabs">
                <li role="presentation"  @if($mode == 'all') class="active" @endif><a href="?mode=all">Все</a></li>
                <li role="presentation" @if($mode == 'trash') class="active" @endif ><a href="?mode=trash">Удаленные</a></li>
            </ul>
            <div class="box-body table-responsive">
                    @include('common.paginate', ['records' => $medCareFacs])

                    @include('admin.med_care_facs.table')
                    @include('common.paginate', ['records' => $medCareFacs])

            </div>
        </div>
    </div>
@endsection

