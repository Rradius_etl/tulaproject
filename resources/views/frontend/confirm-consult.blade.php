@extends('layouts.main')

@section('content')
@section('contentheader_title', trans('routes.confirm-consultation'))
<div class="box box-white">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-info">
                {{ Session::get('success') }}
            </div>
        @endif

        {!!    Former::vertical_open()
                ->id('consult-requres-form')
                ->secure()
                ->rules([
                    '' => 'required'
                ])
                ->method('POST')  !!}


        {!!  Former::datetime('receive_date')
             ->class('form-control')
             ->value(date('Y-m-d H:i'))
             ->readonly()
               !!}



        {!!  Former::select('responsible_person_fullname')->options($doctors)->class('form-control')
        ->label('ФИО и должность ответственного лица, принявшего заявку')->required()
        ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}

        {!!  Former::select('appointed_consultant')->options($doctors)->class('form-control')
        ->label('Назначенный врач-консультант и его специальность')->required()
        ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}



        {!!  Former::datetime('planned_date')->required()
                     ->class('form-control')
                     ->label('Планируемые дата и время проведения консультации')
                     ->value(date('Y-m-d H:i'))
                     ->append('<i class="fa fa-calendar text-primary" for="planned_date"></i>')!!}
            <div class="form-group ">
                <div class="btn btn-danger btn-sm pull-right" id="reset_planned_date">
                    Сбросить {{trans('validation.attributes.desired_date')}}</div>
                <div class="clearfix"></div>
            </div>

            {!!  Former::select('duration')->options($durations)->class('form-control')
                   ->label('Продолжительность')->required()
                   ->append('<i class="fa fa-chevron-down text-primary" for="duration"></i>')!!}



        {!!  Former::text('coordinator_fullname')->value(Sentinel::getUser()->getfName())->readonly()->required() !!}

        {!! Form::submit('Согласовать', ['name' => 'confirm', 'class' => 'btn  btn-primary']) !!}
        {!! Form::submit('Отклонить', ['name' => 'reject', 'class' => 'btn  btn-warning']) !!}

        {!!  Former::close()  !!}
        <hr>

        <p class="text-center">
            <small>
                * Поля обязательны к заполнению
            </small>
        </p>


    </div>
    <!-- /.box-body -->
    <script>
        $(document).ready(function () {
            
            $('.input-group-addon').click(function () {
                var id = '#' + $(this).find('.fa').attr('for');
                if( !$(id).is(':focus')) {
                    $(id).focus();
                }


            })

            $('input[name=planned_date]').datetimepicker({

                locale: 'ru',
                format: 'DD.MM.YYYY HH:mm',
                minDate: moment(),
                allowInputToggle: true
            });


            $('.btn[id^=reset]').click(function () {
                var selector = '#' + this.id.replace('reset_', '');
                console.log(selector);

                $(selector).val('');

            })


        });
    </script>
</div>

@endsection
