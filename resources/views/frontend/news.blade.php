@extends('layouts.main')

@section('content')

@section('contentheader_title', trans('routes.news'))
<div class="box box-white news-page">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">


        @foreach($news as $item)
            <div class="news-item">
                <div class="news-date"> {{$item->getReadableDate()}}</div>
                <div class="item-image col col-sm-3">
                    <img src="{{$item->image('news-small')}}" alt="{{$item->title}}">

                </div>
                <div class="item-info col col-sm-9">
                    <a href="{{route('news.view', $item->slug)}}"><h2>{{$item->title}}</h2></a>
                    <p class="item-descr"> {{$item->short_description}}</p>
                    <a href="{{route('news.view', $item->slug)}}">читать далее...</a>
                </div>
                <div class="clearfix"></div>
            </div>
        @endforeach


        {!! with(new \App\Transformers\CustomPaginationTransformer($news))->render() !!}


    </div>
    <!-- /.box-body -->

</div>

@endsection
