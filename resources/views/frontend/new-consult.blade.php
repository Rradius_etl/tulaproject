@extends('layouts.main')
@section('meta_title', trans('routes.new-consultation'))
@section('contentheader_title', trans('routes.new-consultation'))

@section('content')

    <div class="box box-white">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @else

                <div class="form-group">
                    <button type="button" id="pattern-toggle" class="btn btn-default navbar-btn pull-right"><span
                                class="fa fa-bars"></span> Заполнить из шаблона
                    </button>
                </div>
                <div class="clearfix"></div>
                <div class="pattern-templates" style="display: none;">
                    <div class="form-group">
                        <label for="telemed_center_id">Выберите шаблон из списка: </label>
                        <div class="form-group">
                            <select name="request_pattern" id="request_pattern" class="form-control"></select>
                        </div>


                    </div>
                </div>

                @include('frontend.consultation-fields')

                {!!  Former::actions()->large_primary_submit('Направить') !!}


                {!!  Former::close()  !!}
                <hr>

                <p class="text-center">
                    <small>
                        * Поля обязательны к заполнению
                    </small>
                </p>

            @endif
        </div>
    </div>
    <!-- /.box-body -->
@endsection

@section('additional-scripts')
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <style>
        .input-group input[type=file] {
            width: 100%;
            background: #dbecf6;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('input[type=file]').parent().find('.btn').click(function () {
                $('input[type=file]')[0].click();
            })

            $('#desired_time_group').datetimepicker({
                format: 'HH:mm'
            });
            $('#pattern-toggle').click(function () {
                $('.pattern-templates').toggle();
            })
            var selectizeElemetns = {
                med_care_fac_id: $('#med_care_fac_id').selectize(),
                patient_gender: $('#patient_gender').selectize(),
                consult_type: $('#consult_type').selectize(),
                medical_profile: $('#medical_profile').selectize(),
                patient_social_status: $('#patient_social_status').selectize(),
                patient_indications_for_use_id: $('#patient_indications_for_use_id ').selectize()
            }
            // Init Selectize


            $.get('/api/get-patterns', function (data) {
                var requestPatterns = data;
                $('#request_pattern').selectize({
                    valueField: 'id',
                    labelField: 'name',
                    searchField: 'name',
                    create: false,
                    maxItems: 1,
                    dropdownParent: 'body',
                    preload: true,
                    options: data,
                    onChange: function (val) {
                        console.log(val)
                        var arr = jQuery.grep(requestPatterns, function (el, i) {
                            return ( el.id == val );
                        });
                        console.log(arr)
                        if (arr.length == 1)
                            $.each(arr[0], function (key, value) {

                                var $input = $('#consult-requres-form').find('#' + key + '');


                                if ($input.length > 0) {

                                    /* Set valuest to selectize elements*/
                                    if ($input.is('select')) {

                                        var selectize = selectizeElemetns[key][0].selectize;
                                        selectize.setValue(value);

                                    } else {
                                        /* set value to inputs */
                                        if (key == 'desired_date') {

                                            var date = moment(value, 'YYYY-MM-DD').format('DD.MM.YYYY');
                                            $input.val(date).trigger('change');

                                        } else if (key == 'patient_birthday') {

                                            var date = moment(value, 'YYYY-MM-DD').format('DD.MM.YYYY');
                                            $input.val(date).trigger('change');

                                        } else if (key == 'desired_time') {

                                            $input.val(moment(value, 'HH:mm:ss').format('HH:mm')).trigger('change');
                                        } else if (key != 'created_at' && key != 'updated_at') {

                                            $input.val(value);
                                        }
                                    }


                                }


                            })

                    }

                });
            })

            if ($("#patient_indications_for_use_id").val() == "other") {
                $("#patient_indications_for_use_other").parent().show();
                $("#patient_indications_for_use_other").show();
                $("#patient_indications_for_use_other").prop("disabled", false);
            }
            else {
                $("#patient_indications_for_use_other").hide();
                $("#patient_indications_for_use_other").prop("disabled", "disabled");
                $("#patient_indications_for_use_other").parent().hide();
            }
            $("#patient_indications_for_use_id").change(function () {
                if ($("#patient_indications_for_use_id").val() == "other") {
                    $("#patient_indications_for_use_other").parent().show();
                    $("#patient_indications_for_use_other").show();
                    $("#patient_indications_for_use_other").prop("disabled", false);
                }
                else {
                    $("#patient_indications_for_use_other").hide();
                    $("#patient_indications_for_use_other").prop("disabled", "disabled");
                    $("#patient_indications_for_use_other").parent().hide();
                }
            });
            $('#desired_date_group').datetimepicker({

                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: moment()
            });

            $('#patient_birthday_group').datetimepicker({
                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: '01-01-1800'
            });


            $('.btn[id^=reset]').click(function () {
                var selector = '#' + this.id.replace('reset_', '');
                console.log(selector);

                $(selector).val('');

            })


        });
    </script>

@endsection
