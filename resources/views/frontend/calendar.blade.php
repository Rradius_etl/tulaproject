@extends('layouts.angular')
<style>
    .fc .fc-toolbar > * > .fc-button.fc-close-button {
        position: absolute;
        right: 5px;
        top: -34px;
        background: transparent;
        box-shadow: none;
        color: #000;
        border: none;
        font-size: 30px;
        line-height: 34px;
        height: 34px;
        padding: 0 5px;
    }

    .calendar[calendar~=myCalendar]  .fc-toolbar > * > .fc-button.fc-close-button {
        display: none;
    }
</style>
<script>
    window.appConfigs = {
        conclusionIsEditable: 'editable',
    }




</script>
@section('meta_title', 'Календарь')
@section('contentheader_title', 'Календарь')
@section('content')

    <div class="box box-white news-page" id="calendar" ng-controller="CalendarController as ctrl">
        <div class="box-header">
            <div class="container">
                <div class="col-sm-9  calendar-controls">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="row">
                                <h4>Создать</h4>
                                <a class="btn btn-calendar" ng-click="newEventPopup('consultation')">
                                    <div class="ic ic_add"></div>
                                    <span>Заявку <br> на ТМК</span></a>
                                <div class="btn btn-calendar" ng-click="newEventPopup('meeting')">
                                    <div class="ic ic_meeting"></div>
                                    <span>Совещание</span>
                                </div>
                                <div class="btn btn-calendar" ng-click="newEventPopup('seminar')">
                                    <div class="ic ic_seminar"></div>
                                    <span>Семинар</span></div>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <h4>Представление</h4>
                                <div class="btn btn-calendar" ng-click="setView('agendaDay', $event)">
                                    <div class="ic ic_day"></div>
                                    <span>День</span>

                                </div>
                                <div class="btn btn-calendar" ng-click="setView('workingWeek', $event)">
                                    <div class="ic ic_workweek"></div>
                                    <span>Раб Неделя</span>
                                </div>
                                <div class="btn btn-calendar" ng-click="setView('agendaWeek', $event)">
                                    <div class="ic ic_week"></div>
                                    <span>Неделя</span>
                                </div>
                                <div class="btn btn-calendar" ng-click="setView('month', $event)">
                                    <div class="ic ic_month"></div>
                                    <span>Месяц</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <h4>Временные отрезки</h4>
                                <div class="btn btn-calendar" ng-click="setInterval('00:10:00', $event)">
                                    <div class="ic"><h4>10</h4></div>
                                    <span>10 минут</span>
                                </div>
                                <div class="btn btn-calendar" ng-click="setInterval('00:15:00', $event)">
                                    <div class="ic"><h4>15</h4></div>
                                    <span>15 минут</span>
                                </div>
                                <div class="btn btn-calendar active" ng-click="setInterval('00:30:00', $event)">
                                    <div class="ic"><h4>30</h4></div>
                                    <span>30 минут</span>
                                </div>
                                <div class="btn btn-calendar" ng-click="setInterval('00:60:00', $event)">
                                    <div class="ic"><h4>60</h4></div>
                                    <span>1 час</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 display-mode">
                            <div class="row">
                                <h4>Режим отображения</h4>
                                <div data-ng-repeat="type in eventTypesArray"
                                     data-ng-click="applyCalendarEventFilter(type.id, $event)"
                                     class="btn btn-calendar event-@{{ type.id }}">
                                    <div class="ic ic_@{{ type.id }}"></div>
                                    <span> @{{ type.name }} </span>
                                </div>

                                <div data-ng-click="applyCalendarEventFilter('all', $event)"
                                     class="btn btn-calendar event-all">
                                    <div class="ic ic_all"></div>
                                    <span>Все</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-3 cabinet-controls">
                    <div class="row">
                        <h4>ТМЦ от имени которого будете работать</h4>

                        <div class="input-group">
                            <select name="SelectedTMC" id="SelectedTMC" data-ng-model="SelectedTMC" class="form-control"
                                    ng-change="getEventsToCalendar('myCalendar', {my_calendar:1, telemed_id: SelectedTMC})"
                                    ng-options="option.id as option.name for option in myTelemedCenters" >
                            </select>
                            <span class="input-group-addon"><i class="fa fa-chevron-down text-primary"></i></span>
                        </div>

                    </div>

                </div>
            </div>


        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- SIDEBAR -->
                <div class="col-md-3 left-side">

                    <div data-calendar-id="0" ui-calendar="miniConfig.calendar" calendar="miniCalendar"
                         class="mini-calendar" ng-model="eventSources"></div>

                    <div class="tree-block" >
                        <h6>Управление календарями</h6>
                        <treecontrol class="tree-light" tree-model="allTelemedCenters"
                                     selected-nodes="selectedCalendars" on-selection="selectCalendar(node, selected)"
                                     options="treeOptions">
                            @{{node.name}}
                        </treecontrol>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-9 main-side">
                    <div id="workspace-wrapper">
                        <div id="workspace">

                        </div>
                    </div>


                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <script type="text/ng-template" id="day-dialog">
        <div class="ngdialog-header text-center">
            <h3>Дата: @{{ date.format('Do MMMM YYYY') }}</h3>
            <p>Выберите тип заявки</p>
            <div data-ng-if="eventType == null">
                <div class="form-group">
                    <button ng-click="selectEventType('consultation')" class="btn btn-round btn-primary">Клиническая
                        консультация
                    </button>
                </div>
                <div class="form-group">
                    <button ng-click="selectEventType('seminar')" class="btn btn-round btn-primary">Телеобучение,
                        обучающий семинар
                    </button>
                </div>
                <div class="form-group">
                    <button ng-click="selectEventType('meeting')" class="btn btn-round btn-primary">Видеоконференция,
                        совещание
                    </button>
                </div>
            </div>

        </div>


        <div class="ngdialog-buttons">
            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">Закрыть
            </button>
        </div>
    </script>


    <script type="text/ng-template" id="warning-dialog">
        <div class="ngdialog-message">
            <h3>@{{ ngDialogData.message }}</h3>
        </div>
        <div class="ngdialog-buttons">

            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">Закрыть
            </button>
            <button type="button"  data-ng-show="ngDialogData.showConfirmButton == true" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(ngDialogData.confirmValue)">@{{ ngDialogData.confirmButtonText }}
            </button>
        </div>
    </script>

    <script type="text/ng-template" id="participants-dialog">
        @include('layouts.angular.dialogs.delivery_group')
    </script>

    <script type="text/ng-template" id="get-participants-dialog">
        <div class="ngdialog-content">
            <h3>Выберите ранее сохраненную группу рассылки </h3>

            <!-- add `multiple` attribute to allow multiple sections to open at once -->
            <v-accordion class="vAccordion--default">

                <v-pane id="@{{ pane.id }}" ng-repeat="pane in deliveryGroupTemplates" expanded="pane.isExpanded">
                    <v-pane-header id="@{{ pane.id }}-header" aria-controls="@{{ pane.id }}-content">
                        <h5>@{{ pane.name }} </h5>
                    </v-pane-header>
                    <v-pane-content id="@{{ pane.id }}-content" aria-labelledby="@{{ pane.id }}-header">
                        <button type="button" class="btn btn-round btn-xs btn-primary"
                                data-ng-click="loadDeliveryPattern(pane.tmcs); closeThisDialog()">Выбрать
                        </button>
                        <button type="button" class="btn btn-round btn-xs btn-danger"
                                data-ng-click="deleteDeliveryPattern(pane);">Удалить
                        </button>

                        <ul class="list-group">
                            <li class="list-group-item" data-ng-repeat="(key, value) in pane.tmcs | groupBy: 'mcf_name'" >
                                <h4 class="list-group-item-heading">@{{ key }}</h4>
                                <p class="list-group-item-text" data-ng-repeat="item in value" >@{{item.name}}</p>
                            </li>
                        </ul>

                    </v-pane-content>
                </v-pane>

            </v-accordion>

        </div>
        <div class="ngdialog-buttons">

            <button type="button" class="btn btn-round btn-danger " ng-click="closeThisDialog()">Закрыть</button>
        </div>
    </script>


    <script type="text/ng-template" id="event-create-dialog">
        @include('layouts.angular.dialogs.event-out')
    </script>
    <script type="text/ng-template" id="event-view-dialog">
        @include('layouts.angular.dialogs.event-in')
    </script>

    <script type="text/ng-template" id="abonent-side-tpl">
        @include('layouts.angular.dialogs.consultation-add')
    </script>

    <script type="text/ng-template" id="consultation-side-tpl">
        <ul class="nav nav-pills">
            <li role="presentation" data-ng-class="activeTab == 0 ? 'active' : ''"><a href="#"
                                                                                      data-ng-click="changeTab(0)">Часть
                    ЛПУ-абонента</a></li>
            <li role="presentation" data-ng-class="activeTab == 1 ? 'active' : ''"><a href="#"
                                                                                      data-ng-click="changeTab(1)">Часть
                    ЛПУ-консультанта</a></li>

        </ul>
        <div class="tabs-wrapper">
            <div class="box box-white" data-ng-show="activeTab == 0">
                <div class="box-header text-center">
                </div>
                <div class="box-body">
                    {!!    Former::vertical_open()
                            ->route('abonent-telemed-consult-requests.store')
                            ->id('consult-requres-form')
                            ->secure()
                            ->rules([
                                '' => 'required'
                            ])
                            ->method('POST')  !!}
                    @include('layouts.angular.dialogs.consultation_fields_show')
                    {!! Former::close() !!}


                </div>
            </div>
            @include('layouts.angular.dialogs.event-details-template')
        </div>

    </script>

    <script type="text/ng-template" id="event-conclusion-dialog">
        @include('layouts.angular.dialogs.conclusion')
    </script>

    <script>



    </script>

@endsection


