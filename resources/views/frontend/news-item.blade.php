@extends('layouts.main')
@section('meta_title', $article->title)
@section('meta_description', $article->meta_description)
@section('meta_keywords', $article->meta_keywords)

@section('content')

@section('contentheader_title', $article->title)
<div class="box box-white" id="news-item">
    <div class="box-header">
        <?php $date = new Date($article->created_at) ?>
        <div class="news-date"> {{$date->ago()}}</div>
        <div class="pull-right all-news-link"><a href="{{route('news')}}"><i class="fa fa-chevron-left"></i> Новости</a></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="content">

        </div>
        <div>
            <img src="{{$article->image('large')}}" style="max-width: 320px;padding-right: 8px;padding-bottom: 8px;; float: left" alt="">
            {!! $article->content !!}
        </div>

        <div class="clearfix"></div>
        <hr>
        @if($article->source != '')
            <div>
                <p><i class="fa fa-globe"></i> Источник: <a href="{{ $article->source }}">{{ $article->source }}</a></p>
            </div>
            <hr>
        @endif

    </div>


</div>
<!-- /.box-body -->
<script src="//yastatic.net/share2/share.js"></script>


<style>
    #news-item {

    }
</style>
@endsection
