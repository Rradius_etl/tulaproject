@extends('layouts.main')

@section('content')
@section('contentheader_title', trans('routes.new-consultation'))
<div class="box box-white">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">


        @include('flash::message')
        <label for="">
            Выберите ТМЦ от имени, которого будет отправлена заявка на консультацию.
        </label>
        {!! Form::open(['route' => 'new-consultation','method'=>"get"]) !!}

        <div class="form-group">
            <select name="telemed_center_id" id="" class="form-control">
                @foreach(TmcRoleManager::getTmcs() as $tmc)
                    <option value="{{$tmc['id']}}">{{$tmc['name']}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <button class="btn btn-round btn-primary" type="submit">Продолжить</button>
        </div>


        {!! Form::close() !!}

    </div>

</div>

@endsection
