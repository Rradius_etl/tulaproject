@if(!empty($errors))
    @if($errors->any())
        <ul class="alert alert-danger" style="list-style-type: none">
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    @endif
@endif
@if(Session::has('flash_notification.message'))
    <div class="alert alert-{{Session::get('flash_notification.level')}}">
        {{ Session::get('flash_notification.message') }}
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-info">
        {{ Session::get('success') }}
    </div>
@endif