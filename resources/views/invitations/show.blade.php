@extends('layouts.cabinet')
<?php
        $arr = [
            'consultation' => ' на консультацию',
            'seminar' => ' на семинар',
            'meeting' => ' на совещание',
        ]
?>
<?php $title = trans('backend.Invitation' ) .  $arr[$invitation->telemedEventRequest->event_type]; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row">
                    @include('invitations.show_fields')
                    <a href="{!! route('invitations.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
