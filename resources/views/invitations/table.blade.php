<table class="table table-striped" id="invitations-table">
    <thead>
        <th>Тип события</th>
        <th>Инициатор</th>
        <th>Начало</th>
        <th>Конец</th>
        <th>Важность</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'invitations', 'field'=>'telemed_center_id', 'name' => trans('backend.TMC')])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'invitations', 'field'=>'decision', 'name' => "Решение"])</th>
        <th>@include('layouts.partials.noadminfilter', ['model' => 'invitations', 'field'=>'completed', 'name' => "Решение подтверждено"])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    <?php
    $comparray = ["нет","да"];
    $array = ["low"=>"низкая","medium"=>"средняя","high"=>"высокая"];
    ?>
    @foreach($invitations as $invitation)
        <?php
        $event = $invitation->telemedEventRequest;
        ?>
        <tr>
            <td>{{trans("backend.".$event->event_type)}}</td>
            <td>{{$event->telemedCenter->name}}</td>
            <td>{{$event->start_date}}</td>
            <td>{{$event->end_date}}</td>
            <td>{{$array[$event->importance]}}</td>
            <td>{{ $invitation->telemedCenter->name }}</td>
            <td>{{ trans("backend.".$invitation->decision) }}</td>
            <td>{{ $comparray[$invitation->completed] }}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('invitations.show', [$invitation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $invitations->render() !!}