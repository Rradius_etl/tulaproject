<?php
$event = $invitation->telemedEventRequest;
$participants = $event->telemedEventParticipants()->with('telemedCenter')->get();
$comparray = ["нет", "да"];
?>

<div class="form-group">
    {!! Form::label('telemed_center_id', "Тема" . ':' ) !!}
    <p>{{ $event->title }}</p>
</div>

<div class="form-group">
    {!! Form::label('telemed_center_id', "Запланированная дата и время" . ':' ) !!}
    <p>{{ $event->start_date }}</p>
</div>

        <!-- Telemed Center Id Field -->
<div class="form-group">
    {!! Form::label('telemed_center_id', trans('backend.TelemedCenter') . ':' ) !!}
    <p>{{ $invitation->telemedCenter->name }}</p>
</div>

<!-- Decision Field -->
<div class="form-group">
    {!! Form::label('decision', trans('backend.Decision') . ':' ) !!}
    <p>{{ trans("backend.".$invitation->decision) }}</p>
</div>

<!-- Completed Field -->
<div class="form-group">
    {!! Form::label('completed', trans('backend.Completed') . ':' ) !!}
    <p>{{ $comparray[$invitation->completed]}}</p>
</div>

<div class="form-group">

    <div class="box box-primary box-solid">
        <div class="box-body">
            {!! Form::label('completed',  'Участники:' ) !!}
            @foreach($participants as $part)
                <div class="form-group">
                    {{$part->telemedCenter->name}}
                </div>
            @endforeach
        </div>
    </div>

</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <p>{{ $invitation->created_at }}</p>
</div>
<a class="btn  btn-primary" href="?confirm">Согласовать</a>
<a class="btn  btn-warning" href="?reject">Отклонить</a>

