@extends('layouts.cabinet')
<?php $title = trans('backend.Invitations'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white">

            <div class="box-header">
                <div class="col-sm-4 pull-right">
                    <form action="" method="get" >
                        <label for="">Фильтр по ТМЦ</label>
                        <?php
                        $array = [["name"=>"Выберите ТМЦ",'id'=>""]];
                        foreach(TmcRoleManager::getTmcs() as $i)
                        {
                            array_push($array,["name"=>$i['name'],"id"=>$i['id']]);
                        }
                        ?>
                        <div class="input-group">
                            <select name="telemed_id" id="" class="form-control">
                                @foreach($array as $item)
                                    <option @if($telemedid == $item['id']) selected @endif value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
           <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                           class="fa fa-search"></span></button> </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body table-responsive">
                    
                    @include('invitations.table')
                    
            </div>
        </div>
    </div>
@endsection

