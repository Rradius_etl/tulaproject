<style>
    .progress {
        padding: 0;
    }
    .progress:after {
        content:  'a: ' +attr(data-count) ;
        width: 100%;
        height: 100%;
        color: #000;
        text-align: center;
        box-sizing: border-box;
    }
</style>
<hr>
<h5 class="text-danger">Результаты опроса:</h5>
<?php $total = $poll->totalVotes() ?>
@foreach($poll->pollRows as $index => $row)
    <?php $percent = $row->getVotes($total)?>
    {{$row->title}} ({{$percent['percent']}}%) :
    @if(isset($state))
    @if($state == $row->id)
        <i class="fa fa-check text-success" aria-hidden="true"></i> Ваш ответ
    @endif
    @endif


    <div class="progress active" data-count="{{$percent['count']}}">
        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{$percent['percent']}}"
             aria-valuemin="0" aria-valuemax="100" style="width: {{$percent['percent']}}%">
            <a style="font-size: 14px;color: #000000;" ><strong>{{$percent['count']}}</strong></a>
        </div>
    </div>
@endforeach
<hr>
<div class="pull-left">Проголосовало <b>{{$total}}</b> человек(а)</div>