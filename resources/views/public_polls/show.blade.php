@extends('layouts.cabinet')
<?php $title = trans('backend.Poll') . ' "' . $poll->title . '"'; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
<?php $arr = ["нет", "да"];  $class = ['danger','success']?>
@section('content')
    <script>
        $(document).ready(function(){
            var csrf = '{{ csrf_token() }}';
            var poll_id = {{$poll->id}};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': csrf
                }
            });
            $('#sendbutton').click(function(){
                event.preventDefault();
                var variant = $('[name="variant"]:checked').val();
                if(variant !==undefined)
                {
                    $.ajax({
                    url: "/send-answer",
                    data: {poll_id:poll_id,poll_row_id:variant},
                    type: 'POST',
                    success: function (data) {
                        if(data.success)
                        {
                            $('#content').load('/public_polls/'+poll_id+"?need_result");
                            $('#sendbutton').remove();
                            $('#panel-footer').append('<a href="#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-bell"></span> Вы уже ответили</a>');
                            alert("Ваш голос был учтен!");

                        }
                    }
                })
                }
                else
                {
                    alert("Выберите вариант ответа");
                }
            });
        });
    </script>

    <div class="content">
        <div class="panel panel-default" style="width:60%; margin:0px auto 50px auto;">
            <div class="panel-heading">
                <div class="createopr"></div>
                <div class="form-group" style="width:50%;">
                    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                    <p>{!! $poll->created_at !!}</p>
                </div>
                <strong>Тема опроса </strong> {{$poll->title}}

            </div>
            <div class="panel-body">
                @if(\Carbon\Carbon::now()->diffInSeconds(new \Carbon\Carbon($poll->finished_at),false) <= 0)
                    <div class="alert alert-info">Время активности опроса закончено.</div>
                @else
                @foreach($poll->pollRows as $index => $row)
                    <div class="radio">
                        <label class="radlb">
                            <input @if($state!=false)disabled @endif type="radio" name="variant" value="{{$row->id}}">
                            <strong>{{$index + 1 }}</strong> {{$row->title}}
                        </label>
                    </div>
                @endforeach
                @endif

                    @if($state == true || \Carbon\Carbon::now()->diffInSeconds(new \Carbon\Carbon($poll->finished_at),false) <= 0)
                        @include("public_polls.results  ",['poll'=>$poll,'state'=>$state])
                    @else
                        <div id="content"></div>
                    @endif

                    <div class="panel-footer" id="panel-footer">
                        @if($state != false)
                            <a href="#" class="btn btn-round btn-primary podtv"><span class="glyphicon glyphicon-bell"></span> Вы уже ответили</a>

                            <a href="{{route('polls.all')}}"  class="btn btn-round btn-danger podtv">Назад в список опросов</a>
                        @else

                            @if(\Carbon\Carbon::now()->diffInSeconds(new \Carbon\Carbon($poll->finished_at),false) > 0)
                            <a href="#" id ="sendbutton" class="btn btn-round btn-primary podtv">Ответить</a>
                            @endif

                        @endif
                    </div>
            </div>



        </div>


    </div>
@endsection
