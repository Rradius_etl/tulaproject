@extends('layouts.home-page')

@section('content')
    @if(count($slides) > 0 && \App\Models\InterfaceElement::isVisible('sidebar'))
        <section id="main-slider">

            <div id="carousel" class="slide-bg">
                @foreach($slides as $index => $slide)
                    <div class="item active">
                        <a href="#"><img src="{{$slide->image()}}"/></a>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 slider-descr ">
                                    {!! $slide->content !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="container">
                <div class="slider-controls">
                    <p class="arrows text-right">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    </p>
                </div>
            </div>
            </div>
        </section>
    @endif

    <!-- News Section -->
    <section>
        <div class="row">
            <div class="container ">
                @if(Config::get('website-settings.welcome-enabled') == 'true')
                    @if( \App\Models\InterfaceElement::isVisible('welcome_text'))
                        <div class="col-md-12 hidden-sm">
                            <div class="box box-primary box-blue">
                                <div class="box-body">
                                    <h2> {{$welcome_text}}</h2>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                @if(\App\Models\InterfaceElement::isVisible('sidebar'))
                    <div class="home-page-news col-md-9 col-sm-12">
                        @else
                            <div class="home-page-news col-md-12 col-sm-12">
                                @endif

                                <div class="page-header">
                                    <h1>Новости </h1>
                                    <div class="pull-right"><a href="/news">Все новости</a></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="row">
                                    <div class="home-page-news-left col-md-6 ">
                                        <div class="box box-primary">
                                            <div class="box-header ">
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                @if(isset($news[0]))
                                                    <ul class="news-list news-list-in-box">
                                                        <li class="item">
                                                            <div class="news-date">{{ $news[0]->getReadableDate() }}</div>
                                                            <div class="news-img">
                                                                <a href="{{route('news.view', $news[0]->slug)}}">
                                                                    <img src="{{$news[0]->image('news-small')}}" alt="">
                                                                </a>

                                                            </div>

                                                            <div class="news-title">
                                                                <h2>
                                                                    <a href="{{route('news.view', $news[0]->slug)}}"> {{$news[0]->title}}</a>
                                                                </h2>
                                                            </div>

                                                            <div class="news-description">
                                                                {{$news[0]->short_description}}
                                                            </div>

                                                        </li>
                                                    </ul>
                                                @endif
                                            </div>
                                            <!-- /.box-body -->

                                        </div>

                                    </div>
                                    <div class="home-page-news-right col-md-6">
                                        <div class="box box-white">
                                            <div class="box-header">
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                @if(count($news)> 1)
                                                    <ul class="news-list news-list-in-box">
                                                        @foreach($news as $key => $item)
                                                            @if($key>0)
                                                                <li class="item">
                                                                    <div class="news-date">{{$item->getReadableDate()}}</div>
                                                                    <div class="news-title">
                                                                        <h2>
                                                                            <small>
                                                                                <a href="{{route('news.view', $item->slug)}}">{{ $item->title }}
                                                                                </a>
                                                                            </small>
                                                                        </h2>
                                                                        <div class="text-right">
                                                                            <a href="{{route('news.view', $item->slug)}}">Читать
                                                                                далее</a>
                                                                        </div>
                                                                    </div>

                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                            <!-- /.box-body -->

                                        </div>

                                    </div>
                                </div>

                                @foreach($additional_blocks as $block)
                                    @include('homepage.additional_block')
                                @endforeach
                            </div>

                            @if(\App\Models\InterfaceElement::isVisible('sidebar'))
                                @include('partials.resources')
                            @endif

                    </div>

            </div>
    </section>

    <style>
        #main-slider {
            margin: 0 auto;
            width: 100%;
            max-width: 1920px;
        }

        #main-slider .item {
            min-height: 460px;
            text-align: center;
        }

        #main-slider .item .container {
            text-align: left;

        }

        .slider-controls .arrows {
            color: #cbcece;
        }

        .slider-controls {
            position: absolute;
            right: 100px;
            bottom: 10px;
        }

        .slider-controls .arrows .fa {
            padding: 10px;
            cursor: pointer;
        }

        .slider-descr {
            padding-bottom: 80px;
        }

    </style>
    <script>
        $(document).ready(function () {


            $("#carousel").slick({
                autoplay: {{Config::get('website-settings.slider-autoplay')}},
                autoplaySpeed: {{Config::get('website-settings.slider-time') * 1000}},
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                variableWidth: false,
                dots: false,
                prevArrow: '.slider-controls .fa-chevron-left',
                nextArrow: '.slider-controls .fa-chevron-right',
                responsive: [
                    {
                        breakpoint: 481,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        })
    </script>


@endsection