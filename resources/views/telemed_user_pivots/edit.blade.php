@extends('layouts.cabinet')
<?php $title = trans('backendtelemedUserPivot') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($telemedUserPivot,['route' => ['telemed-user-pivots.update', 'telemedUserPivot'=>$telemedUserPivot->id,'TelemedId'=>$TelemedId ], 'method' => 'patch']) !!}

                        @include('telemed_user_pivots.fields',['TelemedId'=>$TelemedId])

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection