@extends('layouts.cabinet')
<?php $title = 'Пользователь привязанный к телемедицинскому центру '; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary">
            <div class="box-body"  id="zebr">
                <div class="row">
                    @include('telemed_user_pivots.show_fields')
                    <br>
                    <a href="{!! route('telemed-user-pivots.index',$TelemedId) !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
