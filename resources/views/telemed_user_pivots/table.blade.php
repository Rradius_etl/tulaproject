@if(count($telemedUserPivots)>0)
<table class="table table-striped" id="telemedUserPivots-table">
    <thead>
    @include('layouts.partials.universal-filter', ['fields' => [

      ["users|email"=>true,'name'=>'пользователь'],
      ["value"=>true,'name'=>'роль'],

    ],"route"=>"telemed-user-pivots.index",'params'=>['TelemedId'=>$TelemedId]])
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedUserPivots as $telemedUserPivot)
        <tr>
            <td>{{ $telemedUserPivot->user->email }}</td>
            <td>{{ $telemedUserPivot->value }}</td>
            <td>
                @if($telemedUserPivot->value == "admin"&&TmcRoleManager::hasRole("mcf_admin"))
                {!! Form::open(['route' => ['telemed-user-pivots.destroy', $telemedUserPivot->id,"TelemedId"=>$TelemedId], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('telemed-user-pivots.show', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('telemed-user-pivots.edit', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
                @elseif(TmcRoleManager::hasRole("admin")&&$telemedUserPivot->value=="admin")
                У вас нет прав редактировать эту запись
                @else
                    <div class='btn-group'>
                        <a href="{!! route('telemed-user-pivots.show', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('telemed-user-pivots.edit', ["telemedUserPivot"=>$telemedUserPivot->id,"TelemedId"=>$TelemedId]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                    </div>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    Нету записей.
@endif