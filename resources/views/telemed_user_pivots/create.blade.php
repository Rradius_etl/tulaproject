@extends('layouts.cabinet')
<?php $title = 'Пользователи привязанные к телемедицинскому центру' . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['url'=>"/telemed-user-pivots/".$TelemedId]) !!}

                        @include('telemed_user_pivots.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
