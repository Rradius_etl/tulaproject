@extends('layouts.cabinet')
<?php $title = 'Пользователи привязанные к телемедицинскому центру '; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white">
            <div class="box-header">


                <div class="col-sm-4 pull-right">
                    <form action="">

                        <div class="input-group">

                            <input value="{{$search}}" type="text" name="search" class="form-control"
                                   placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                    </form>
                </div>

                <div class="pull-right">
                    <a class="btn btn-success btn-social   pull-right" style=""
                       href="{!! route('telemed-user-pivots.create',['TelemedId'=>$TelemedId]) !!}">
                        <i class="fa fa-plus"></i> {{ trans('backend.addnew')  }}
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="box-body table-responsive">

                @include('telemed_user_pivots.table')

            </div>
        </div>
    </div>
@endsection

