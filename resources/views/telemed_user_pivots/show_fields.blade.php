
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', "Пользователь" . ':' ) !!}
    <div class="clearfix">{{ $telemedUserPivot->user->email ." - ".$telemedUserPivot->user->getFname() }}</div>
</div>



<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', "Роль" . ':' ) !!}
    <div class="clearfix">{{ trans('validation.attributes.role.'.$telemedUserPivot->value) }}</div>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', "Назначена роль в" . ':' ) !!}
    <div class="clearfix">{{ $telemedUserPivot->updated_at }}</div>
</div>

