<?php $arr = ['Не прочитано', 'Прочитано'];
$classes = ['label-danger', 'label-success'] ?>
<table class="table " id="notifications-table">
    <thead>
    <th width="20px"></th>
    <th>Уведомление</th>
    <th>Прочтено</th>
    <th width="100px">Время</th>
    </thead>
    <tbody>
    @foreach($notifications as $idx => $value)
        <tr>
            <td>  @if($value->is_read == false ) <input type="checkbox" data-id="{{$value->id}}"
                                                        id="notification_{{$idx}}" name="data[]"> @endif </td>
            <td class="with-label"><label for="notification_{{$idx}}">{!! $value->notification->message !!}</label></td>
            <td><span class="label {{$classes[$value->is_read ]}}" data-id="{{$value->id}}"
                      data-mark-read="">{{$arr[$value->is_read ]}}</span></td>
            <?php $data = new \Carbon\Carbon($value->notification->created_at);  ?>
            <td> {{ $data->format('d-m-Y в H:i') }}</td>

        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $notifications])
<style>
    #notifications-table td.with-label {
        padding: 0;
    }
    #notifications-table td.with-label label{
        width: 100%;
    }
    td > label {
        cursor: pointer;
        padding: 8px;
    }
    td > label:hover {
        color: rgba(24, 5, 92, 0.95);
        background: #fffcd7;
    }
</style>