<table class="table table-striped" id="telemedConsultRequests-table">
    <thead>
    @include('layouts.partials.universal-filter', ['fields' => [
       ["comment"=>true],
       ["uid_id"=>true],
       ["decision"=>true],
       ["med_care_fac_id"=>true],
       ["desired_date"=>false],
       ["telemed_event_requests:request_id|canceled"=>true,"name"=>"отменено"],
       ["telemed_event_requests:request_id|start_date"=>true,"name"=>"запланированное время"],
         ["created_at"=>true,'name'=>'отправлено  в'],
       ["telemed_center_id"=>false],

   ],"route"=>"telemed-consult-requests.index"])

    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    <?php
    $st = ['<div class="alert alert-success">нет</div>','<div class="alert alert-danger">да</div>'];
    ?>
    @foreach($telemedConsultRequests as $telemedConsultRequest)
        <tr data-request-id="{{$telemedConsultRequest->request_id}}">
            <td>{{ $telemedConsultRequest->comment }}</td>
            <td>{{ $telemedConsultRequest->uid_code."-".$telemedConsultRequest->uid_year."-".$telemedConsultRequest->uid_id }}</td>
            <td class="decision decision-{{$telemedConsultRequest->decision}}">{{ trans('backend.'.$telemedConsultRequest->decision) }}</td>
            <td>{{ $telemedConsultRequest->medCareFac->name }}</td>
            <td>{!! $telemedConsultRequest->abonentSide->getDesiredDate() !!}
            </td>
            <?php
            $req = $telemedConsultRequest->request;
            ?>
            <td>{!! $st[intval($req->canceled)] !!}</td>
            <td>{{ $req->start_date }}</td>
            <td>{{ $telemedConsultRequest->created_at }}</td>
            <td>@if($telemedConsultRequest->telemed_center_id!=null)
                    @if($telemedConsultRequest->telemedCenter!=null)
                    {{ $telemedConsultRequest->telemedCenter->name }}
                    @else
                    ТМЦ был удален
                    @endif
                @else
                    не направлено к ТМЦ@endif</td>
            <td>
                <?php $consultSide = $telemedConsultRequest->consultantSide; ?>
                @if( $req->canceled != 1)
                    <div class='btn-group'>
                        <a href="{!! route('telemed-consult-requests.show', [$telemedConsultRequest->request_id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>

                        <a href="{!! route('telemed-consult-requests.edit', [$telemedConsultRequest->request_id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="#" data-id="{{$telemedConsultRequest->request_id }}"
                           onclick="setRequestIdForMessage({{$telemedConsultRequest->request_id }})"  data-toggle="modal" data-target="#message-dialog" title="Написать сообщение по поводу этой заявки"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-envelope"></i></a>
                    </div>
                @else
                    <div class='btn-group'>
                        <a href="{!! route('telemed-consult-requests.show', [$telemedConsultRequest->request_id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="#" class='btn btn-default btn-xs' data-toggle="tooltip"
                           title="Редактирование невозможно @if($req->canceled == 1) (событие отменено) @endif"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="#" data-id="{{$telemedConsultRequest->request_id }}"
                           onclick="setRequestIdForMessage({{$telemedConsultRequest->request_id }})"  data-toggle="modal" data-target="#message-dialog" title="Написать сообщение по поводу этой заявки"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-envelope"></i></a>
                    </div>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $telemedConsultRequests])

{{-- Попап отправки сообшении =>  --}}

@include('messenger.new-message', ['about_request' => true, 'modal_dialog_title' => 'Написать сообщение<br> по поводу ТМК'])
@include('messenger.about-request-scripts')