@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequest'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-white box-solid">
            <div class="box-body">
                <div class="row">
                    @include('telemed_consult_requests.show_fields')
                    <a href="{!! route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']) !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                    <a href="{!! route('telemed-consult-requests.edit', $telemedConsultRequest->request_id) !!}" class="btn btn-primary"> Принять решение</a>
                </div>
            </div>
        </div>
    </div>
@endsection
