@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequests'); ?>
@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white box-solid">
          <div class="box-header">
              <ul class="nav nav-tabs" >
                  <li role="presentation" ><a href="{{route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Входящие</a></li>
                  <li role="presentation" ><a data-toggle="tooltip" title="Просмотр статуса заявки"  href="{{route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Просмотр статуса заявки</a></li>
                  @if(\App\Models\TmcRoleManager::hasRole('mcf_admin'))<li role="presentation" class="active"><a href="{{route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Перенаправление заявок</a></li>@endif
                  <li role="presentation"><a href="{{route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Доступные заявки</a></li>
              </ul>
            </div>

            <div class="box-body table-responsive">
                <div class="row" style="margin-bottom: 15px;">
                    <form action="">
                            <div class="input-group col-sm-4 pull-right">

                                <input value="{{$uidsearch}}" type="text" name="uidsearch" class="form-control" placeholder="Поиск по номеру заявки" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                            </div>
                        <div class="input-group col-sm-4 pull-right">
                            {!! Form::select('canceled',[''=>'фильтр по активности','all'=>"все","canceled"=>'только отмененные','nocanceled'=>'только активные'],$canceled,['class'=>'form-control']) !!}
                        </div>
                        <div class="input-group col-sm-4 pull-left" data-toggle="tooltip" title="Фильтр по дате" >
                            <input class="form-control" placeholder="Фильтр по дате" type="text" name="daterange"><span class="input-group-addon for-date"><i class="fa fa-calendar text-primary"></i></span>

                        </div>
                    </form>
                </div>


                    @include('telemed_consult_requests.table')
                @include('common.paginate', ['records' => $telemedConsultRequests])
            </div>
        </div>
    </div>

@section('additional-scripts')
    @include('partials.daterange-filter')
@stop
@endsection

