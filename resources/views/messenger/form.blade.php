<link rel="stylesheet" href="/plugins/select2/select2.css">
<script src="/plugins/select2/select2.js"></script>
<script src="/plugins/select2/i18n/ru.js"></script>
<style>
    .select2-dropdown {
        z-index: 99999999;
    }
</style>
@if(isset($id))
    {!!    Former::vertical_open_for_files()
              ->id('message-form')
              ->route('messages.update',[$id])
                ->name('message-form')
              ->secure()
              ->method( isset($in) ? 'PUT' : 'POST')  !!}
@else
    {!!    Former::vertical_open_for_files()
              ->id('message-form')
              ->route('messages.store')
                ->name('message-form')
              ->secure()
              ->method( isset($in) ? 'PUT' : 'POST')  !!}
@endif
@if(!isset($in))
    {!!  Former::text('subject')->class('ed-controlmess')->label('Тема', ['class' => 'edlabelmes'])->required() !!}
@endif
{!!  Former::text('from')->class('form-control controlmessdisabl')
->value(Sentinel::getUser()->getfName())
           ->label('От кого', ['class' => 'edlabelmes'])
           ->disabled() !!}
@if(!isset($in))
    <div class="form-group">
        {!! Form::label('recipients', "Кому" . ':', ['class' => 'edlabelmes']) !!}
        <select name="recipients" class="ed-controlmess js-example-responsive js-states form-control" required multiple="multiple"  style="width: 100%;" >
        </select>
    </div>


@endif


{!!  Former::textarea('message')->label('Cообщение', ['class' => 'edlabelmes'])->class('ed-controlmess')->required() !!}


<div class="form-group ">
    <label  class="edlabelmes" for="files">Прикрепить файл</label>
    <div class="input-group">
        <input type="file"  name="files[]" id="files">
        </input>
                 <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">Обзор</button>
                  </span>
    </div>
</div>


{{--  Если по поводу определенной ТМК то рендерить hidden input --}}
@if(isset($about_request))
    {!! Former::hidden('request_id')->id('about-request') !!}
@endif
{{-- / End--}}
<div class="form-group" >
    {!! Former::checkbox('send_email')
        ->text('Уведомить по e-mail')
        ->label(false)
     !!}
</div>
{!!  Former::actions()->large_round_primary_submit('Отправить') !!}

{!! Former::close() !!}

<style>
    .select2-selection {
        background: #dbecf6 !important;
    }
    .select2-search.select2-search--inline, .select2-search__field {
        width: 100% !important;
    }
    .edlabelmes + .input-group input[type=file] {
        width: 100%;
        background: #dbecf6;
    }
</style>
<script>
    $('input[type=file]').parent().find('.btn').click(function () {
        $('input[type=file]')[0].click();
    });
    $(document).ready(function(){
        $(".js-example-responsive").select2({
            language: "ru",
            allowClear: true,
            placeholder: "Начинайте вводить email пользователя",
            ajax: {
                url: "/finduser",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.email,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 2,
        });
    });
</script>