<div id="{{ $modal_dialog_id or 'message-dialog' }}" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" role="document">

        <div class=" {{ $modal_dialog_image_class or 'imess' }}"> </div>
        <div class="box box-white  col-xs-12 col-sm-3">
            <div class="box-header text-center">
                <h2>{!!  $modal_dialog_title or 'Написать сообщение' !!} </h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if(isset($in)&&isset($id))
                    @include('messenger.form',['in'=>$in,'id'=>$id])
                @else
                    @include('messenger.form')
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>

