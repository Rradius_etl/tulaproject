@extends('layouts.cabinet')

@section('contentheader_title', trans('routes.messages'))

@section('meta_title', trans('routes.messages'))

@section('content')
    <div class="box box-white">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <div class="pull-right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#message-dialog">
                            <i class="fa fa-envelope"></i> Написать сообщение
                        </button>
                    </div>
                    <!-- TABS -->
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="{{route('messages')}}">Входящие</a></li>
                        <li role="presentation" ><a href="{{route('messages.unread')}}">Непрочитанные</a></li>

                        <li role="presentation"><a
                                    href="{{route('messages.outgoing')}}">Исходящие</a>
                        </li>


                    </ul>
                </div>

                <div class="col-sm-4 pull-right">
                    <form action="">

                        <div class="input-group">

                            <input value="{{Input::get('search','')}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('common.errors')
            @if (Session::has('error_message'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('error_message') }}
                </div>
            @endif
            @if($threads->count() > 0)
                <table class="table">
                    <thead>
                    @include('layouts.partials.universal-filter', ['fields' => [
                         ["От"=>false],
                         ["Участники"=>false],
                         ["Тема"=>false],
                         ["Последнее сообщение"=>false],
                         ["created_at"=>true],
                       ],"route"=>"messages"])

                    </thead>
                    <tbody>

                    @foreach($threads as $thread)

                        <?php $class = $thread->isUnread($currentUserId) ? 'info' : '';
						$latest = $thread->messages()->orderBy('updated_at', 'desc')->first();
                        $from = "пользователь был удален";
                        if($latest->user!= null)
                        {
                            $from = $latest->user->getfName();
                        }
						?>

                        <tr class="{{$class}}">
                            <td> {!! link_to('messages/' . $thread->id, $from) !!} </td>
                            <td>{{ $thread->participantsString(Sentinel::check()->id, ['surname','name',  'middlename']) }}</td>
                            <td>{!! link_to('messages/' . $thread->id, $thread->subject) !!}
                                @if( $thread->request()->exists() )
                                    {{ link_to_route('consultant-telemed-consult-requests.show', 'По поводу ТМК ' .$thread->request->getConsultation->getFullUid(), [$thread->request->id], ['class'=>'label label-info','data-toggle' => 'tooltip', 'title' => 'Нажмите чтобы посмотреть']) }}
                                @endif
                            </td>
                            <td><a href="/messages/{{$thread->id}} ">{{ str_limit($latest->body,100,"...") }} @if(count($latest->files)>0) <i class="glyphicon glyphicon-paperclip"></i> @endif</a> </td>
                            <td>{{ $latest->created_at }}</td>
                        </tr>


                    @endforeach
                    </tbody>
                </table>
            @else
                <p>Нет сообщений</p>
            @endif
            @include('common.paginate',['records' => $threads])
        </div>
        <!-- /.box-body -->

    </div>

    @include('messenger.new-message')
@endsection