@extends('layouts.cabinet')

@section('content')
@section('contentheader_title', 'Сообщения.  Тема: '. $thread->subject )
@section('meta_title', 'Сообщения.  Тема: '. $thread->subject )
<div class="box box-white">
    <div class="box-header">
        <div class="row">
            <div class="col-sm-12">

                <div class="pull-right">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#message-dialog">
                        <i class="fa fa-envelope"></i> Написать сообщение
                    </button>
                </div>
                <!-- TABS -->
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="{{route('messages')}}">Входящие</a></li>
                    <li role="presentation"><a href="{{route('messages.unread')}}">Непрочитанные</a></li>

                    <li role="presentation"><a href="{{route('messages.outgoing')}}">Исходящие</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-7">
                @foreach($thread->messages as $message)

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php $avatar = "";
                            $fname = "пользователь был удален";
                            $name = "пользователь был удален";
                            if($message->user != null)
                            {
                                $avatar = $message->user->avatar('small-avatar');
                                $fname = $message->user->getfName();
                                $name =  $message->user->name;
                            }
                            ?>
                            @if($message->user != null)
                                @endif
                            <div class="thumbnail col-sm-3"><img src="{{$avatar}}"
                                                                 alt="{{ $name }}"></div>
                            <div class="col-sm-9">
                                <small>Дата создания темы: {{ Date::parse($message->created_at)->diffForHumans() }}</small>
                                <h5 class="media-heading">{{ $fname }}</h5>
                                @if( $thread->request()->exists())
                                    <br>
                                    <small>Это сообщение по поводу ТМК: </small>
                                    <h5 class="media-heading">
                                        {{ link_to_route('consultant-telemed-consult-requests.show', $thread->request->getConsultation->getFullUid(), [$thread->request->id], ['data-toggle' => 'tooltip', 'data-placement'=>'bottom', 'title' => 'Нажмите чтобы посмотреть']) }}
                                    </h5>
                                @endif
                            </div>
                            <div class="col-sm-12">
                                <p>{{ $message->body }}</p>

                                <?php
                                $messagefiles = $message->files;
                                ?>
                                @if(count($messagefiles)>0)
                                    <div class="form-group">
                                        <b>Прикрепленные файлы:</b>
                                        @foreach($messagefiles as $file)
                                            <div class="form-group">
                                                <a href="/getfile/{{$file->file->id}}" target="_blank">
                                                    <?php
                                                    $ext_img = 'images/file-formats/' . $file->file->extension . '.png';
                                                    if (!File::exists($ext_img)) {
                                                        $ext_img = 'images/file-formats/_blank.png';
                                                    } ?>
                                                    <img src="/{{$ext_img}}"
                                                         alt="{{$file->file->original_name}}">
                                                    {{$file->file->original_name}}</a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
            <div class="col-sm-5">
                <div class="list-group">
                    @if($thread->participants->count() > 0)

                        <a href="#" class="list-group-item active">Участники чата</a>
                        @foreach($thread->participants as $user)

                            <a href="#" class="list-group-item">@if($user->user != null){{ $user->user->email }}
                                - {{ $user->user->getfName() }} @else пользователь был удален @endif</a>

                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h2 class="panel-title">Написать новое сообщение</h2>
            </div>
            <div class="panel-body">
                @include('messenger.form', ['in' => true,'id'=>$thread->id])
                <hr>
            </div>
        </div>


    </div>
</div>


@include('messenger.new-message')
@stop
