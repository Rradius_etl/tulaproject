@extends('layouts.cabinet')

@section('content')
<h1>Create a new message</h1>
{!! Form::open(['route' => 'messages.store','files'=>true]) !!}
<div class="col-md-6">
    <!-- Subject Form Input -->
    <div class="form-group">
        {!! Form::label('subject', 'Тема', ['class' => 'control-label']) !!}
        {!! Form::text('subject', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Message Form Input -->
    <div class="form-group">
        {!! Form::label('message', 'Сообщение', ['class' => 'control-label']) !!}
        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::file("files[]",["multiple"]) !!}
    </div>
    @if($users->count() > 0)

        {!!  Form::select('recipients', $users, null, ['class' => 'form-control', 'multiple' => true]) !!}

    @endif
    
    <!-- Submit Form Input -->
    <div class="form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>
{!! Form::close() !!}
@stop
