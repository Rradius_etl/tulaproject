@extends('layouts.cabinet')

@section('contentheader_title', 'Исходящие сообщения')
@section('meta_title', 'Исходящие сообщения')
@section('content')
    <div class="box box-white">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <div class="pull-right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#message-dialog">
                            <i class="fa fa-envelope"></i> Написать сообщение
                        </button>
                    </div>
                    <!-- TABS -->
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="{{route('messages')}}">Входящие</a></li>
                        <li role="presentation" ><a href="{{route('messages.unread')}}">Непрочитанные</a>
                        </li>

                        <li role="presentation" class="active"><a
                                    href="{{route('messages.outgoing')}}">Исходящие</a>
                        </li>


                    </ul>
                </div>
                <div class="col-sm-4 pull-right">
                    <form action="">

                        <div class="input-group">

                            <input value="{{Input::get('search','')}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            @if (Session::has('error_message'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('error_message') }}
                </div>
            @endif
            @if($messages->count() > 0)
                <table class="table">
                    <thead>
                    @include('layouts.partials.universal-filter', ['fields' => [

                         ["Участники"=>false],
                         ["Тема"=>false],
                         ["Последнее сообщение"=>false],
                         ["created_at"=>true],
                       ],"route"=>"messages.outgoing"])

                    </thead>
                    <tbody>

                    @foreach($messages as $msg)
                        <tr>
                            <td>
                                @if(count($msg->recipients) > 0 )
                                    @for($i=0;$i <  count($msg->recipients); $i++ )
                                        <?php if($i == 3) break; ?>
                                      <a class="label label-default" style="display: inline-block;">   @if($msg->recipients[$i]->user != null){{$msg->recipients[$i]->user->getfName()}}@else пользователь был удален @endif</a>
                                    @endfor
                                @endif
                            </td>
                            <td>{{ link_to('messages/' . $msg->thread->id, $msg->thread->subject ) }}
                                @if( $msg->thread->request()->exists() )
                                    {{ link_to_route('consultant-telemed-consult-requests.show', 'По поводу ТМК ' . $msg->thread->request->getConsultation->getFullUid(), [$msg->thread->request->id], ['class'=>'label label-info','data-toggle' => 'tooltip', 'title' => 'Нажмите чтобы посмотреть']) }}
                                @endif
                            </td>
                            <td>{{  link_to('messages/' . $msg->thread->id, $msg->body ) }}</td>
                            <td>{{ $msg->created_at }}</td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            @else
                <p>Нет сообщений</p>
            @endif
                @include('common.paginate',['records' => $messages])
        </div>
        <!-- /.box-body -->

    </div>

    @include('messenger.new-message')
@endsection