@extends('layouts.home-page')

@section('content')


        <!-- News Section -->
<section>
    <div class="row">
        <div class="container ">
            <div class="home-page-news col-md-9 col-sm-12">
                <div class="page-header">
                    <h1>Поиск </h1>
                    <div class="pull-right"><a href="/news">Все новости</a></div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="home-page-news-left col-md-12 ">
                        <div class="box box-primary">
                            <div class="box-header ">
                                <form action="/search">
                                    <div class="input-group">
                                        <span class="input-group-addon">Поиск по сайту</span>
                                        <input type="text" class="form-control" name="q" aria-label="" value="{{$q}}">
                                                 <span class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> найти</button>
      </span>

                                    </div>
                                </form>
                                <br>

                                @if(Session::has('flash_notification.message'))
                                    <div class="alert alert-{{Session::get('flash_notification.level')}}">
                                        {{ Session::get('flash_notification.message') }}
                                    </div>
                                @endif


                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                @if(isset($news))
                                    <ul class="news-list news-list-in-box">
                                        @foreach($news as $article)
                                            <li class="item">
                                                <div class="news-date">{{ $article->getReadableDate() }}</div>
                                                <div class="news-title">
                                                    <h2>
                                                        <a href="{{route('news.view',$article->slug)}}"> {{$article->title}}</a>
                                                    </h2>
                                                </div>

                                                <div class="news-description">
                                                    {{$article->short_description}}
                                                </div>

                                            </li>
                                        @endforeach
                                    </ul>
                                @endif

                                    @if(isset($news)) @include('common.paginate', ['records'=> $news])  @endif
                            </div>
                            <!-- /.box-body -->

                        </div>

                    </div>

                </div>


            </div>

            @include('partials.resources')

        </div>

    </div>
</section>


@endsection