@extends('layouts.main')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('contentheader_title')
  У вас нет доступа к этому разделу сайта
@endsection

@section('$contentheader_description')
@endsection

@section('content')

    <div class="box box-white">
        <div class="box-body">
            <div class="error-page">
                <h2 class="headline text-yellow"> <b>401</b> Доступ запрещен</h2>
                <div class="error-content">
                    <h3><i class="fa fa-warning text-yellow"></i>  {{ $errors->first() }}</h3>
                    <p>
                       Страница к которому вы хотите попасть предназначена для определенного круга лиц. Чтобы узанть подробности свяжитесь с администратором портала.
                        {{ trans('adminlte_lang::message.mainwhile') }} <a href="{{URL::to('/')}}">вы можете вернуться на главную страницу</a> {{ trans('adminlte_lang::message.usingsearch') }}
                    </p>
                    <form class='search-form'>
                        <div class='input-group'>
                            <input type="text" name="q" class='form-control' placeholder="{{ trans('adminlte_lang::message.search') }}"/>
                            <div class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                            </div>
                        </div><!-- /.input-group -->
                    </form>
                </div><!-- /.error-content -->
            </div><!-- /.error-page -->
        </div>
    </div>

@endsection