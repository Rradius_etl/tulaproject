<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', trans('validation.attributes.firstname') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Surname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('surname', trans('validation.attributes.lastname') . ':') !!}
    {!! Form::text('surname', null, ['class' => 'form-control']) !!}
</div>

<!-- Middlename Field -->
<div class="form-group col-sm-6">
    {!! Form::label('middlename', trans('validation.attributes.middlename') . ':') !!}
    {!! Form::text('middlename', null, ['class' => 'form-control']) !!}
</div>

<!-- Spec Field -->
<div class="form-group col-sm-6">
    {!! Form::label('spec', trans('validation.attributes.spec') . ':') !!}
    {!! Form::text('spec', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit( trans('backend.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('doctors.index',$TelemedId) !!}" class="btn btn-default">{{ trans('backend.cancel') }} </a>
</div>
