@extends('layouts.cabinet')
<?php $title = trans('backend.Doctors') . ' &#187; ' .'Добавить Врача'; ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['doctors.store',$TelemedId]]) !!}

                        @include('doctors.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
