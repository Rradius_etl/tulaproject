<table class="table table-responsive" id="doctors-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['id' => $TelemedId, 'model' => 'doctors', 'field'=>'name', 'name' => trans('validation.attributes.name')])</th>
        <th>@include('layouts.partials.noadminfilter', ['id' => $TelemedId,'model' => 'doctors', 'field'=>'surname', 'name' => trans('validation.attributes.surname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['id' => $TelemedId,'model' => 'doctors', 'field'=>'middlename', 'name' => trans('validation.attributes.middlename')])</th>
        <th>@include('layouts.partials.noadminfilter', ['id' => $TelemedId,'model' => 'doctors', 'field'=>'spec', 'name' => trans('validation.attributes.spec')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($doctors as $doctor)
        <tr>
            <td>{{ $doctor->name }}</td>
            <td>{{ $doctor->surname }}</td>
            <td>{{ $doctor->middlename }}</td>
            <td>{{ $doctor->spec }}</td>
            <td>
                {!! Form::open(['route' => ['doctors.destroy', $TelemedId,$doctor->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('doctors.show', [$TelemedId,$doctor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('doctors.edit', [$TelemedId,$doctor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>