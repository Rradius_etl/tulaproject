<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', trans('backend.Id') . ':' ) !!}
    <p>{{ $doctor->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{{ $doctor->name }}</p>
</div>

<!-- Surname Field -->
<div class="form-group">
    {!! Form::label('surname', trans('backend.Surname') . ':' ) !!}
    <p>{{ $doctor->surname }}</p>
</div>

<!-- Middlename Field -->
<div class="form-group">
    {!! Form::label('middlename', trans('backend.Middlename') . ':' ) !!}
    <p>{{ $doctor->middlename }}</p>
</div>

<!-- Spec Field -->
<div class="form-group">
    {!! Form::label('spec', trans('backend.Spec') . ':' ) !!}
    <p>{{ $doctor->spec }}</p>
</div>

<!-- Telemed Center Id Field -->
<div class="form-group">
    {!! Form::label('telemed_center_id', trans('backend.Telemed Center Id') . ':' ) !!}
    <p>{{ $doctor->telemed_center_id }}</p>
</div>

