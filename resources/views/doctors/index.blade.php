@extends('layouts.cabinet')
<?php $title = trans('backend.Doctors') . ' телемедицинского центра ' . $Telemed->name; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-white">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-primary btn-social  pull-right" style="" href="{!! route('doctors.create',$TelemedId) !!}">
                                   <i class="fa fa-plus"></i>  Добавить Врача
                               </a>
                </div>
            </div>
            <div class="box-body">
                    
                    @include('doctors.table')
                    
            </div>
        </div>
    </div>
@endsection

