@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedCenter') . ' "' . $telemedCenter->name . '"';  ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">

        <div class="box box-white">
            <div class="box-header">
                <h2 class="box-title">{{$telemedCenter->name}}</h2>
            </div>
            <div class="box-body" id="zebr">
                <div class="row">
                    @include('telemed_centers.show_fields')
                    <a href="{!! route('general-telemed-centers.index') !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
