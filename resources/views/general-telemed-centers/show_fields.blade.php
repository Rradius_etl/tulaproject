<?php  $arr = ['Нет', 'Да']?>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', trans('backend.Name') . ':' ) !!}
    <p>{{ $telemedCenter->name }}</p>
</div>

<!-- Director Fullname Field -->
<div class="form-group">
    {!! Form::label('director_fullname', trans('backend.Director Fullname') . ':' ) !!}
    <p>{{ $telemedCenter->director_fullname }}</p>
</div>

<!-- Coordinator Fullname Field -->
<div class="form-group">
    {!! Form::label('coordinator_fullname', trans('backend.Coordinator Fullname') . ':' ) !!}
    <p>{{ $telemedCenter->coordinator_fullname }}</p>
</div>

<!-- Coordinator Phone Field -->
<div class="form-group">
    {!! Form::label('coordinator_phone', trans('backend.Coordinator Phone') . ':' ) !!}
    <div class="clearfix">{{ $telemedCenter->coordinator_phone }}</div>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_fullname', trans('backend.Tech Specialist Fullname') . ':' ) !!}
    <div class="clearfix">{{ $telemedCenter->tech_specialist_fullname }}</div>
</div>

<!-- Tech Specialist Phone Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_phone', trans('backend.Tech Specialist Phone') . ':' ) !!}
    <div class="clearfix">{{ $telemedCenter->tech_specialist_phone }}</div>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('tech_specialist_contacts', trans('backend.Tech Specialist Contacts') . ':' ) !!}
    <div class="clearfix">{{ $telemedCenter->tech_specialist_contacts }}</div>
</div>

<!-- Tech Specialist Fullname Field -->
<div class="form-group">
    {!! Form::label('videoconf_equipment', trans('backend.Videoconference Equipment') . ':') !!}
    <div class="clearfix">{{ $telemedCenter->videoconf_equipment }}</div>
</div>


<div class="form-group">
    {!! Form::label('digit_img_demonstration', trans('backend.Digital Image Demonstration') . ':') !!}
    <div class="clearfix">{{ $arr[$telemedCenter->digit_img_demonstration] }}</div>
</div>

<!-- Tech Specialist Contacts Field -->
<div class="form-group">
    {!! Form::label('equipment_location', trans('backend.Equipment Location') . ':') !!}
    <div class="clearfix">{{ $telemedCenter->equipment_location }}</div>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
    <div class="clearfix">{{ $telemedCenter->created_at }}</div>
</div>


