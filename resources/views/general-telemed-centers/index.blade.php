@extends('layouts.cabinet')
<?php $title = 'Справочник телемедицинских центров'; ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('common.errors')
        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white">
            <div class="col-sm-4 pull-right">
                <form action="">
                    <label for=""></label>
                    <div class="input-group">

                        <input value="{{$search}}" type="text" name="search" class="form-control" placeholder="Расширенный поиск" aria-label="Поиск">
                                <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                                class="fa fa-search"></span></button> </span>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="box-body">
                <div class="row">
                    @include('general-telemed-centers.table')
                    @include('common.paginate', ['records' => $telemedCenters])
                </div>


            </div>
        </div>
    </div>
@endsection

