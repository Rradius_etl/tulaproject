<table class="table table-striped" id="telemedCenters-table">
    <thead>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'name', 'name' => trans('backend.Name')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'director_fullname', 'name' => trans('backend.Director Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'coordinator_fullname', 'name' => trans('backend.Coordinator Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'coordinator_phone', 'name' => trans('backend.Coordinator Phone')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'tech_specialist_fullname', 'name' => trans('backend.Tech Specialist Fullname')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'tech_specialist_phone', 'name' => trans('backend.Tech Specialist Phone')])</th>
        <th>@include('layouts.partials.noadminfilter', ['route'=>'general-telemed-centers','model' => 'telemedCenters', 'field'=>'tech_specialist_contacts', 'name' => trans('backend.Tech Specialist Contacts')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedCenters as $telemedCenter)
        <tr>
            <td>{{ $telemedCenter->name }}</td>
            <td>{{ $telemedCenter->director_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_fullname }}</td>
            <td>{{ $telemedCenter->coordinator_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_fullname }}</td>
            <td>{{ $telemedCenter->tech_specialist_phone }}</td>
            <td>{{ $telemedCenter->tech_specialist_contacts }}</td>
            <td>

                <div class='btn-group'>
                    <a href="{!! route('general-telemed-centers.show', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
                <div class='btn-group'>
                    <a href="{!! route('general-telemed-centers.send-message-tmc', [$telemedCenter->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-comments"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>