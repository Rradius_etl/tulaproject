@extends('layouts.cabinet')
@section('meta_title','Написать сообщение телемедицинскому центру' )

@section('content')
@section('contentheader_title','Написать сообщение телемедицинскому центру')

    <div class="box box-white">
        <div class="box-header">

            <div class="pull-right all-news-link"><a href="{{route('general-telemed-centers.index')}}"><i class="fa fa-chevron-left"></i> Справочник телемедицинских центров</a>
                <h1>Написать сообщение</h1>
        </div>
        <div class="box-body" style=" ">

            {!! Form::open(['files'=>true]) !!}

                    <!-- Subject Form Input -->
            <div class="form-group">
                {!! Form::label('subject', 'Тема', ['class' => 'control-label']) !!}
                {!! Form::text('subject', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::label('message', 'Сообщение', ['class' => 'control-label']) !!}
                {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::file("files[]",["multiple"]) !!}
            </div>
            <!-- Submit Form Input -->
            <div class="form-group">
                {!! Form::submit('Отправить', ['class' => 'btn btn-primary form-control']) !!}
            </div>

            {!! Form::close() !!}

        </div>
    </div>


@stop
