<p>{{ trans('email.Activate your website account', ['website'=> $_SERVER['HTTP_HOST']]) }}</p>
{{trans('email.To activate the account')}} <a href="{{ URL::to("activate/{$sentuser->getUserId()}/{$code}") }}"> {{trans('email.follow this link')}} </a>
<p>{{trans('email.Your login')}}: {{ $sentuser->email }} </p>
@if(isset($password))<p>{{trans('email.Your password')}}: {{$password}} </p>@endif

<p>{{trans('email.After activating your account')}} <a href="http://{{$_SERVER['HTTP_HOST']}}"> {{trans('email.follow this link')}} </a> </p>
<hr>
<p>{{ trans('email.Message sent from website', ['website'=> $_SERVER['HTTP_HOST']]) }} </p>
