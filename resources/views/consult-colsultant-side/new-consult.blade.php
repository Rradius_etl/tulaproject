 @extends('layouts.main')

 @section('meta_title', trans('routes.new-consultation'))
 @section('contentheader_title', trans('routes.new-consultation'))
 @section('content')
<div class="box box-white">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-info">
                {{ Session::get('success') }}
            </div>
        @endif

        {!!    Former::vertical_open()
                ->id('consult-requres-form')
                ->secure()
                ->rules([
                    '' => 'required'
                ])
                ->method('GET')  !!}



        {!!  Former::texts('request_uid')
                  ->class('form-control')
                  ->value($mfc->code."-".date('Y',time())."-".$id)
                  ->disabled()
                  !!}

        {!!  Former::date('created_time')
            ->class('form-control')
            ->value(date('Y-m-d-H:m:s'))
            ->disabled()
              !!}

        {!!  Former::texts('doctor_fullname')
              ->class('form-control')
              ->value('')
              ->required()  !!}


        {!!  Former::texts('doctor_spec') ->class('form-control') !!}

        {!!  Former::select('Выбор ЛПУ-консультанта')->options($mcfs)->class('form-control') !!}

        {!!  Former::texts('desired_consultant') ->class('form-control') !!}

        {!!  Former::text('desired_date') ->class('form-control')->append('<i class="fa fa-calendar text-primary"></i>') !!}
        {!!  Former::text('desired_time') ->class('form-control') !!}

        {!!  Former::select('consult_type')->options($consult_types->prepend('-- Выберите из списка --',null), null)->class('form-control')->required() !!}

        {!!  Former::select('medical_profile')->options($medical_profiles)->class('form-control')->required() !!}

        {!!  Former::text('patient_uid_or_fname') ->class('form-control')->required() !!}

        {!!  Former::text('patient_birthday')->class('form-control')->required()->append('<i class="fa fa-calendar text-primary"></i>') !!}

        {!!  Former::select('patient_gender')->options($genders, 'male')->class('form-control')->required() !!}

        {!!  Former::select('patient_social_status')->options($socstatuses) ->class('form-control') !!}

        {!!  Former::texts('patient_address') ->class('form-control') !!}

        {!!  Former::select('patient_indications_for_use_id')->options($ind_for_use)->class('form-control')->required() !!}
        {!!  Former::texts('patient_indications_for_use_other') ->class('form-control')->setAttribute('style', 'display:none')->disabled() !!}
        {!!  Former::texts('patient_questions') ->class('form-control') !!}

        {!!  Former::texts('coordinator_fullname') ->class('form-control')->value($tmc->coordinator_fullname)->disabled() !!}
        {!!  Former::texts('coordinator_phone') ->class('form-control')->value($tmc->coordinator_phone)->disabled() !!}

        {!!  Former::actions()->large__round_primary_submit('Направить') !!}


        {!!  Former::close()  !!}
        <hr>

        <p class="text-center">
            <small>
                * Поля обязательны к заполнению
            </small>
        </p>


    </div>
    <!-- /.box-body -->
</div>

@endsection
@section('additional-scripts')
    <script>
        $(document).ready(function () {
            $('input[name=desired_time]').datetimepicker({
                defaultDate: moment(),
                format: 'HH:mm'
            });


            // Init Selectize
            $('#med_care_fac_id').selectize();
            $('#patient_gender').selectize();
            $('#consult_type').selectize();
            $('#medical_profile').selectize();
            $('#patient_social_status').selectize();

            if($("#patient_indications_for_use_id").val()=="other")
            {
                $("#patient_indications_for_use_other").parent().show();
                $("#patient_indications_for_use_other").show();
                $("#patient_indications_for_use_other").prop("disabled",false);
            }
            else
            {
                $("#patient_indications_for_use_other").hide();
                $("#patient_indications_for_use_other").prop("disabled",true);
                $("#patient_indications_for_use_other").parent().hide();
            }
            $("#patient_indications_for_use_id").change(function()
            {
                if($("#patient_indications_for_use_id").val()=="other")
                {
                    $("#patient_indications_for_use_other").parent().show();
                    $("#patient_indications_for_use_other").show();
                    $("#patient_indications_for_use_other").prop("disabled",false);
                }
                else
                {
                    $("#patient_indications_for_use_other").hide();
                    $("#patient_indications_for_use_other").prop("disabled",true);
                    $("#patient_indications_for_use_other").parent().hide();
                }
            });

            $('input[name=desired_date]').datetimepicker({
                defaultDate: moment(),
                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: moment()
            });

            $('input[name=patient_birthday]').datetimepicker({

                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: '01-01-1800'
            });
        });
    </script>
@endsection