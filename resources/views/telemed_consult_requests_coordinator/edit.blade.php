@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequest') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($telemedConsultRequest, ['route' => ['telemed-consult-requests.update', $telemedConsultRequest->id], 'method' => 'patch']) !!}

                        @include('telemed_consult_requests_coordinator.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection