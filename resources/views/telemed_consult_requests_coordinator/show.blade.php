@extends('layouts.app')
<?php $title = trans('backend.TelemedConsultRequest'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-solid">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('telemed_consult_requests_coordinator.show_fields')
                    <a href="{!! route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']) !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
