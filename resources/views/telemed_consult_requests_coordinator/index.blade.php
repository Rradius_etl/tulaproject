@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequests'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary box-solid">
            <div class="box-header">
                <div class="pull-right">
                   <a class="btn btn-success btn-social  btn-xs pull-right" style="" href="{!! route('telemed-consult-requests.create') !!}">
                                   <i class="fa fa-plus"></i>  {{ trans('backend.addnew')  }}
                               </a>
                </div>
            </div>
            <div class="box-body">
                @include('common.paginate', ['records' => $telemedConsultRequests])
                    @include('telemed_consult_requests_coordinator.table')
                @include('common.paginate', ['records' => $telemedConsultRequests])
            </div>
        </div>
    </div>
@endsection

