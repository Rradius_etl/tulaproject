<table class="table table-responsive" id="telemedConsultRequests-table">
    <thead>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'request_id', 'name' => trans('backend.Request Id')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'comment', 'name' => trans('backend.Comment')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'uid_id', 'name' => "идентификатор заявки"])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'decision', 'name' => trans('backend.Decision')])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'telemed_center_id', 'name' =>"Заявка направлена в"])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'telemed_center_id', 'name' =>"ЛПУ абонент"])</th>
        <th>@include('layouts.partials.filter', ['model' => 'telemedConsultRequests', 'field'=>'decision_at', 'name' => trans('backend.Decision At')])</th>
        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    @foreach($telemedConsultRequests as $telemedConsultRequest)
        <?php
        $consultRequest = $telemedConsultRequest->consultRequest;
        ?>
        <tr>
            <td>{!! $consultRequest->request_id !!}</td>
            <td>{!! $consultRequest->comment !!}</td>
            <td>{!! $consultRequest->uid_code."-".$consultRequest->uid_year."-".$consultRequest->uid_id !!}</td>
            <td>{!! $consultRequest->decision !!}</td>
            <td>{!! $consultRequest->telemedCenter->medCareFac->name !!}</td>
            <td>{!! $consultRequest->telemedCenter->medCareFac->name !!}</td>
            <td>{!! $consultRequest->decision_at !!}</td>
            <td>
                {!! Form::open(['route' => ['telemed-consult-requests.destroy', $consultRequest->request_id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('telemed-consult-requests.show', [$consultRequest->request_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('telemed-consult-requests.edit', [$consultRequest->request_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('backend.areyousure')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>