@extends('layouts.cabinet')
<?php
$title = trans('backend.TelemedConsultRequest') . ' &#187; ' . trans('backend.editing_existed');

$patient_birthday = \Carbon\Carbon::createFromFormat( 'Y-m-d h:i:s',$telemedConsultRequest->patient_birthday);

$patient_birthday_string = implode([$patient_birthday->day, $patient_birthday->month, $patient_birthday->year], '.');
?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">

        <div class="box box-primary box-solid">
            <div class="box-body">
                @include('common.errors')


                {!!    Former::vertical_open_for_files()
               ->route('abonent-telemed-consult-requests.update', $telemedConsultRequest->request_id)
               ->id('consult-requres-form')
               ->secure()
               ->method('patch')  !!}

                <div class="request-status request-status-{{$telemedConsultRequest->consultRequest->decision}}">
                    <p>Статус: <span>{{trans('backend.'.$telemedConsultRequest->consultRequest->decision)}}</span></p>
                </div>

                @include('my_telemed_consult_requests.edit_fields')

                <div class="form-group">
                    {!!  Former::large_primary_submit('Внести поправки в заявку') !!}
                    <a target="_blank" class="btn btn-round btn-danger" href="{!! route('abonent-telemed-consult-requests.savepattern', [$telemedConsultRequest->request_id]) !!}" class='btn btn-default btn-xs' data-toggle="tooltip" title="сохранить в шаблоны"><i class="fa fa-floppy-o"></i> Сохранить в шаблоны</a>
                    <a href="{!! route('abonent-telemed-consult-requests.index') !!}"
                       class="btn btn-round btn-danger">{{ trans('backend.cancel') }} </a>

                </div>

                {!! Former::close() !!}

            </div>
        </div>
    </div>


@endsection

@section('additional-scripts')
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <script>
        $(document).ready(function () {
            $('input[name=desired_time]').datetimepicker({
                format: 'HH:mm'
            });
            $('input[type=file]').parent().find('.btn').click(function () {
                $('input[type=file]')[0].click();
            })

            // Enable selectize
            $('#med_care_fac_id').selectize();
            $('#consult_type').selectize();
            $('#patient_gender').selectize();
            $('#medical_profile').selectize();
            $('#patient_social_status').selectize();


            if ($("#patient_indications_for_use_id").val() == "other") {
                $("#patient_indications_for_use_other").parent().show();
                $("#patient_indications_for_use_other").show();
                $("#patient_indications_for_use_other").prop("disabled", false);
            }
            else {
                $("#patient_indications_for_use_other").hide();
                $("#patient_indications_for_use_other").prop("disabled", true);
                $("#patient_indications_for_use_other").parent().hide();
            }
            $("#patient_indications_for_use_id").change(function () {
                if ($("#patient_indications_for_use_id").val() == "other") {
                    $("#patient_indications_for_use_other").parent().show();
                    $("#patient_indications_for_use_other").show();
                    $("#patient_indications_for_use_other").prop("disabled", false);
                }
                else {
                    $("#patient_indications_for_use_other").hide();
                    $("#patient_indications_for_use_other").prop("disabled", true);
                    $("#patient_indications_for_use_other").parent().hide();
                }
            });

            $('input[name=desired_date]').datetimepicker({
                useCurrent: false,

                locale: 'ru',
                format: 'DD.MM.YYYY',

            });

            $('input[name=patient_birthday]').datetimepicker({
                useCurrent: false,
                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: '01-01-1800'
            });


            $('.btn[id^=reset]').click(function () {
                var selector = '#' + this.id.replace('reset_', '');
                console.log(selector);

                $(selector).val('');

            })


        });
    </script>

@endsection