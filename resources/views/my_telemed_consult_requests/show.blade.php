@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequest'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')
    <div class="content">
        <div class="box box-primary box-white">
            <div class="box-body">
                <div class="row">
                    @include('my_telemed_consult_requests.show_fields')
                    <a href="{!! route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']) !!}" class="btn btn-default"> {{ trans('backend.back')  }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
