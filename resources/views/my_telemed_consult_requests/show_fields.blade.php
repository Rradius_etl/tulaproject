<!-- Request Id Field -->

<div class="box box-primary box-solid">
    <div class="box-header">
        Часть ЛПУ абонента
    </div>
    <div class="box-body" id="zebr">
        <div class="row" >
<!-- Comment Field -->
            <div class="form-group">
                {!! Form::label('doctor_fullname', trans('validation.attributes.doctor_fullname') . ':' ) !!}
                <p>{{ $telemedConsultRequest->doctor_fullname }}</p>
            </div>
            <div class="form-group">
                {!! Form::label('doctor_spec', trans('validation.attributes.doctor_spec') . ':' ) !!}
                <p>{{ $telemedConsultRequest->doctor_spec }}</p>
            </div>
            <div class="form-group">
                {!! Form::label('consult_type', trans('validation.attributes.consult_type') . ':' ) !!}
                <p>{{ $telemedConsultRequest->consultationType->name}}</p>
            </div>
            <div class="form-group">
                {!! Form::label('MCF', trans("backend.MCF") . ':' ) !!}
                <p>{{ $telemedConsultRequest->medCareFac->name }}</p>
            </div>

            <div class="form-group">
                {!! Form::label('desired_consultant', trans("validation.attributes.desired_consultant") . ':' ) !!}
                <p>{{ $telemedConsultRequest->desired_consultant }}</p>
            </div>
            <div class="form-group">
                {!! Form::label('desired_date', trans("validation.attributes.desired_date") . ':' ) !!}
                <p>{!! $telemedConsultRequest->desired_date != null ? date("d-m-Y",strtotime($telemedConsultRequest->desired_date))  : "<span class=\"label label-info\">не указана</span>" !!}</p>
            </div>
            <div class="form-group">
                {!! Form::label('desired_time', trans("validation.attributes.desired_time") . ':' ) !!}
                <p>{{  date("H:i",strtotime($telemedConsultRequest->desired_time))  }}</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_uid_or_fname', trans("validation.attributes.patient_uid_or_fname") . ':' ) !!}
                <p>{{ $telemedConsultRequest->patient_uid_or_fname }} &nbsp;</p>
            </div>
            <!-- patient_address Field -->
            <div class="form-group">
                {!! Form::label('patient_address', trans('validation.attributes.patient_address') . ':' ) !!}
                <p>@if($telemedConsultRequest->patient_address != null){{ $telemedConsultRequest->patient_address }} @else Не указано @endif</p>
            </div>
            <!-- patient_address Field -->
            <div class="form-group">
                {!! Form::label('patient_birthday', trans('validation.attributes.patient_birthday') . ':' ) !!}
                <p>{{ $telemedConsultRequest->patient_birthday }} &nbsp;</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_social_status', trans('validation.attributes.patient_social_status') . ':' ) !!}
                <p>@if($telemedConsultRequest->socialStatus != null){{ $telemedConsultRequest->socialStatus->name }} @else Не указано @endif</p>
            </div>
            <div class="form-group">
                {!! Form::label('patient_indications_for_use_id', trans('validation.attributes.patient_indications_for_use_id') . ':' ) !!}
                <p>{{$telemedConsultRequest->getIndForUse() }}</p>
            </div>
            <div class="form-group">
                {!! Form::label('decision', trans('validation.attributes.patient_indications_for_use').' - другое:' ) !!}
                <div class="clearfix">{{ $telemedConsultRequest->patient_indications_for_use_other }}</div>
            </div>

            <div class="form-group">
                {!! Form::label('decision', trans('validation.attributes.patient_questions') . ':' ) !!}
                <div class="clearfix">{{ $telemedConsultRequest->patient_questions }}</div>
            </div>
            <!-- Telemed Center Id Field -->
            <div class="form-group">
                {!! Form::label('telemed_center_id', 'Инициатор:' ) !!}
                <?php $tmc1 =  $telemedConsultRequest->telemedCenter;?>
                <p>@if($tmc1 != null){{ $telemedConsultRequest->telemedCenter->name }} @else ТМЦ был удален @endif</p>
            </div>
            <!-- Created At Field -->
            <div class="form-group">
                {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                <p>{{ $telemedConsultRequest->created_at }}</p>
            </div>
            <!-- Updated At Field -->
            <div class="form-group">
                {!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}
                <p>{{ $telemedConsultRequest->updated_at }}</p>
            </div>
        </div>
    </div>
</div>

<div class="box @if($telemedConsultRequest->telemed_center_id!=null)  box-success @else  box-info @endif box-solid">
    <div class="box-header">
        Куда была направлена заявка
    </div>

    <div class="box-body" id="zebr">
        @if($telemedConsultRequest->telemed_center_id!=null)
            @if($telemedConsultRequest->telemedCenter!=null)
                <a data-toggle="tooltip" title="Нажмите чтобы узнать подробно об этом ТМЦ" href="{{route('general-telemed-centers.show',$telemedConsultRequest->telemedCenter->id)}}">{{ $telemedConsultRequest->telemedCenter->name }}</a>
            @else
                ТМЦ был удален
            @endif
        @else
            не направлено к ТМЦ@endif

 </div>
 </div>

<div class="box box-primary box-solid">
    <div class="box-header">
        Часть ЛПУ консультанта
    </div>

    <div class="box-body" id="zebr">
        <div class="row" >
            <?php
            $consultSide = $telemedConsultRequest->TelemedConsultConsultantSide;
            ?>
            @if($consultSide != null)
                <div class="form-group">
                     {!! Form::label('created_at', 'Выбранный доктор и его специальность:' ) !!}
                    <div class="clearfix">{{ $consultSide->doctor->surname." ". $consultSide->doctor->name." ". $consultSide->doctor->middlename." : ". $consultSide->doctor->spec }}</div>
                </div>
                <div class="form-group">
                     {!! Form::label('created_at', 'Запланированная дата и время:' ) !!}
                    <div class="clearfix">{{ $consultSide->planned_date }}</div>
                </div>
                <div class="form-group">
                     {!! Form::label('created_at', trans('backend.Comment') . ':' ) !!}
                     <div class="clearfix">{{ $consultSide->telemedConsultRequest->comment }}</div>
                </div>
                 <div class="form-group">
                     {!! Form::label('created_at', 'ФИО принявшего заявку(ЛПУ-координатора):' ) !!}
                     <div class="clearfix">{{ $consultSide->coordinator_fullname }}</div>
                 </div>
                <div class="form-group">
                    Файлы:
                        @foreach($consultSide->files as $file)
                            <div class="form-group">
                                <a href="/getfile/{{$file->file->id}}" target="_blank">{{$file->file->original_name}}</a>
                            </div>
                        @endforeach
                </div>

                    <div class="form-group">
                        {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}
                        <div class="clearfix">{{ $consultSide->created_at }}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('created_at', trans('backend.Decision').";") !!}
                        <div class="clearfix">{{ trans("backend.".$consultSide->telemedConsultRequest->decision) }}</div>
                    </div>

            @else
                Координатор ЛПУ еще не заполнил свою форму
            @endif
        </div>
    </div>
</div>