@extends('layouts.main')

@section('content')
@section('contentheader_title', trans('routes.new-consultation'))
<div class="box box-white">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        @include('common.errors')


        {!!    Former::vertical_open()
                ->route('abonent-telemed-consult-requests.store')
                ->id('consult-requres-form')
                ->secure()
                ->rules([
                    '' => 'required'
                ])
                ->method('POST')  !!}

        @include('my_telemed_consult_requests.fields',['tmc'=>$tmc])


                <!-- Submit Field -->
        <div class="form-group">
            {!!  Former::large__round_primary_submit('Подать заявку') !!}
            <a href="{!! route('abonent-telemed-consult-requests.index') !!}"
               class="btn btn-round btn-danger">{{ trans('backend.cancel') }} </a>
        </div>


        {!!  Former::close()  !!}
        <hr>

        <p class="text-center">
            <small>
                * Поля обязательны к заполнению
            </small>
        </p>


    </div>
    <!-- /.box-body -->
    <script>
        $(document).ready(function () {
            $('input[name=desired_time]').datetimepicker({
                format: 'HH:mm'
            });


            if ($("#patient_indications_for_use_id").val() == "other") {
                $("#patient_indications_for_use_other").parent().show();
                $("#patient_indications_for_use_other").show();
                $("#patient_indications_for_use_other").prop("disabled", false);
            }
            else {
                $("#patient_indications_for_use_other").hide();
                $("#patient_indications_for_use_other").prop("disabled", true);
                $("#patient_indications_for_use_other").parent().hide();
            }
            $("#patient_indications_for_use_id").change(function () {
                if ($("#patient_indications_for_use_id").val() == "other") {
                    $("#patient_indications_for_use_other").parent().show();
                    $("#patient_indications_for_use_other").show();
                    $("#patient_indications_for_use_other").prop("disabled", false);
                }
                else {
                    $("#patient_indications_for_use_other").hide();
                    $("#patient_indications_for_use_other").prop("disabled", true);
                    $("#patient_indications_for_use_other").parent().hide();
                }
            });
            $('input[name=desired_date]').datetimepicker({

                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: moment()
            });

            $('input[name=patient_birthday]').datetimepicker({
                locale: 'ru',
                format: 'DD.MM.YYYY',
                minDate: '01-01-1800'
            });


            $('.btn[id^=reset]').click(function () {
                var selector = '#' + this.id.replace('reset_', '');
                console.log(selector);

                $(selector).val('');

            })


        });
    </script>
</div>

@endsection
