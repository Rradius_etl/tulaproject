
<table class="table table-striped" id="telemedConsultRequests-table">
    <thead>
        @include('layouts.partials.universal-filter', ['fields' => [
          ["comment"=>false],
          ["telemed_consult_requests:request_id:request_id|uid_id"=>true,"name" => 'Идентификатор заявки'],
          ["telemed_consult_requests:request_id:request_id|decision"=>true, "name" => 'Статус заявки'],
          ["ЛПУ консультант"=>false],
          ["telemed_event_requests:request_id|canceled"=>true,"name"=>"отменено"],
          ["telemed_event_requests:request_id|start_date"=>true,"name"=>"запланированное время"],
          ['created_at'=>true,'name'=>'отправлено в']
      ],"route"=>"abonent-telemed-consult-requests.index"])


        <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    <?php
    $st = ['<label class="alert alert-info">нет</label>','<label class="alert alert-danger">да</label>'];
    ?>
    @foreach($telemedConsultRequests as $telemedConsultRequest)
        <?php
        $consultRequest = $telemedConsultRequest->consultRequest;
        $req = $consultRequest->request;
        ?>
        <tr>
            <td>{{ $consultRequest->comment }}</td>
            <td>{{ $consultRequest->uid_code."-".$consultRequest->uid_year."-".$consultRequest->uid_id }}</td>
            <td class="decision decision-{{$consultRequest->decision}}">{{ trans('backend.'.$consultRequest->decision) }}</td>
            <td>{{ $consultRequest->medCareFac->name }}</td>
            <td>{!! $st[intval($req->canceled)] !!}</td>
            <td>{{ $req->start_date }}</td>
            <td>{{$consultRequest->created_at}}</td>
            <td>
                @if($consultRequest->decision == "pending" && $req->canceled == 0)

                <div class='btn-group'>
                    <a href="{!! route('abonent-telemed-consult-requests.show', [$consultRequest->request_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('abonent-telemed-consult-requests.edit', [$consultRequest->request_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('abonent-telemed-consult-requests.savepattern', [$telemedConsultRequest->request_id]) !!}" data-id="{{$telemedConsultRequest->request_id}}" class='btn btn-default btn-xs savepattern' data-toggle="tooltip" title="сохранить в шаблоны"><i class="fa fa-floppy-o"></i></a>
                    <a href="#" data-id="{{$telemedConsultRequest->request_id }}"
                       onclick="setRequestIdForMessage({{$telemedConsultRequest->request_id }})"  data-toggle="modal" data-target="#message-dialog" title="Написать сообщение по поводу этой заявки"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-envelope"></i></a>
                </div>
                @else
                    <a href="{!! route('abonent-telemed-consult-requests.show', [$consultRequest->request_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="#" class='btn btn-default btn-xs' data-toggle="tooltip" title="Редактирование невозможно@if($req->canceled == 1)(событие отменено) @else (консультант принял решение) @endif"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('abonent-telemed-consult-requests.savepattern', [$telemedConsultRequest->request_id]) !!}" data-id="{{$telemedConsultRequest->request_id}}" class='btn btn-default btn-xs savepattern' data-toggle="tooltip" title="сохранить в шаблоны"><i class="fa fa-floppy-o"></i></a>
                    <a href="#" data-id="{{$telemedConsultRequest->request_id }}"
                       onclick="setRequestIdForMessage({{$telemedConsultRequest->request_id }})"  data-toggle="modal" data-target="#message-dialog" title="Написать сообщение по поводу этой заявки"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-envelope"></i></a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $telemedConsultRequests])

<script>

    $(document).ready(function () {
        $(function () {
            $( ".savepattern" ).click(function (e) {
                var selector = $('#modal-save-pattern');

                e.preventDefault();
                var href = $(this).attr('href');

                console.log('', href)
                selector.find('form').attr('action', href);
                selector.modal();


            });
        })
    })

</script>

@include('my_telemed_consult_requests.save-pattern')


{{-- Попап отправки сообшении =>  --}}
@section('modal_dialog_title', 'Написать сообщение<br> по поводу ТМК')
@include('messenger.new-message', ['about_request' => true, 'modal_dialog_title' => 'Написать сообщение<br> по поводу ТМК'])
@include('messenger.about-request-scripts')