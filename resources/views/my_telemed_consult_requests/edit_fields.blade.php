{!!  Former::texts('request_uid')
          ->class('form-control')
          ->value($uid)
          ->disabled()  !!}


{!!  Former::date('created_time')
    ->class('form-control')
    ->value(date('Y-m-d'))
    ->readonly()
      !!}

{!!  Former::texts('doctor_fullname')
      ->class('form-control')

      ->required()  !!}


{!!  Former::texts('doctor_spec') ->class('form-control') !!}

{!!  Former::select('med_care_fac_id')->options($mcfs)->class('form-control')->label('Выбор ЛПУ-консультанта')->required() !!}

{!!  Former::texts('desired_consultant') ->class('form-control') !!}

{!!  Former::text('desired_date') ->class('form-control')->append('<i class="fa fa-calendar text-primary"></i>')->forceValue($telemedConsultRequest->getDesiredDate(true)) !!}

<div class="form-group ">
    <div class="btn btn-danger btn-sm pull-right" id="reset_desired_date">
        Сбросить {{trans('validation.attributes.desired_date')}}</div>
    <div class="clearfix"></div>
</div>

{!!  Former::text('desired_time') ->class('form-control')->append('<i class="fa fa-clock-o text-primary"></i>') !!}

<div class="form-group ">
    <div class="btn btn-danger btn-sm pull-right" id="reset_desired_time">
        Сбросить {{trans('validation.attributes.desired_time')}}</div>
    <div class="clearfix"></div>
</div>
{!!  Former::select('consult_type')->options($consult_types->prepend('-- Выберите из списка --',null), null)->class('form-control')->required() !!}

{!!  Former::select('medical_profile')->options($medical_profiles)->class('form-control')->required() !!}

{!!  Former::text('patient_uid_or_fname') ->class('form-control')->required() !!}

{!!  Former::text('patient_birthday')->class('form-control')->required()->append('<i class="fa fa-calendar text-primary"></i>')->forceValue($patient_birthday_string) !!}

<div class="form-group ">
    <div class="btn btn-danger btn-sm pull-right" id="reset_patient_birthday">
        Сбросить {{trans('validation.attributes.patient_birthday')}}</div>
    <div class="clearfix"></div>
</div>

{!!  Former::select('patient_gender')->options($genders, 'male')->class('form-control')->required() !!}

{!!  Former::select('patient_social_status')->options($socstatuses) ->class('form-control') !!}

{!!  Former::texts('patient_address') ->class('form-control') !!}

{!!  Former::select('patient_indications_for_use_id')->options($ind_for_use)->class('form-control')->required() ->value($telemedConsultRequest->patient_indications_for_use_id)!!}
{!!  Former::texts('patient_indications_for_use_other') ->class('form-control')->setAttribute('style', $telemedConsultRequest->patient_indications_for_use_id == 'other' ? 'display:block' : 'display:none')->disabled() !!}

{!!  Former::texts('patient_questions') ->class('form-control') !!}


<div class="form-group ">
    <label  class="edlabelmes" for="file">Прикрепить файл</label>
    <div class="input-group">
        <input type="file" multiple name="file[]" id="files">
        </input>
         <span class="input-group-btn">
            <button class="btn btn-primary" type="button">Обзор</button>
         </span>
    </div>
    @if(count($files)> 0)
        <ul>
            <span style="margin-left: -40px;">Загруженные файлы:</span>
            @foreach($files as $file)
                <li><a href="/getfile/{{$file->file_id}}">{{$file->original_name}}</a></li>
            @endforeach
        </ul>
    @endif

</div>

{!!  Former::texts('coordinator_fullname') ->class('form-control')->value($tmc->coordinator_fullname)->readonly() !!}
{!!  Former::texts('coordinator_phone') ->class('form-control')->value($tmc->coordinator_phone)->readonly() !!}

<style>
    .input-group input[type=file] {
        width: 100%;
        background: #dbecf6;
    }
</style>