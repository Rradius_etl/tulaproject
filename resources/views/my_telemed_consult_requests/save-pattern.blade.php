<div id="{{ $modal_dialog_id or 'modal-save-pattern' }}" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" role="document">

        <div class=" {{ $modal_dialog_image_class or 'isave' }}"> </div>
        <div class="box box-white  col-xs-12 col-sm-3">
            <div class="box-header text-center">
                <h2>{!!  $modal_dialog_title or 'Сохранение шаблона' !!} </h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">


                {!!    Former::vertical_open_for_files()
                          ->id('save-pattern-form')
                          ->route('abonent-telemed-consult-requests.savepattern',0)
                            ->name('save-pattern-form')
                          ->secure()
                          ->method( 'POST')  !!}


                {!!  Former::text('name')->class('ed-controlmess')->label('Название', ['class' => 'edlabelmes'])->required() !!}


                {!!  Former::actions()->large_round_primary_submit('Сохранить') !!}

                {!! Former::close() !!}


            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>

