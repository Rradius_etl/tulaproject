<link rel="stylesheet"
      href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<!-- datetimepicker -->
<script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
<link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
<div class="form-group">
    {!!  Former::text('created_at')
        ->label('Дата и время приема заявки',['class' => 'edlabelmes'])
        ->class('form-control controlmessdisabl')
        ->value(Carbon\Carbon::parse(new Carbon\Carbon($consultside->created_at))->format('Y-m-d H:i'))
        ->readonly()
         ->disabled()
          !!}
</div>

<style>
    .request-status p {
        color: #000;
    }
    .selectize-input,  .selectize-input.full {
        background-color: #dbecf6;
    }
</style>
<!-- appointed_person_id Field -->
@if( Sentinel::inRole("admin") || Sentinel::inRole('mcf_admin') || Sentinel::inRole('tmc_admin' )|| Sentinel::inRole('coordinator')  )
    <div class="form-group">
        {!! Form::label('appointed_person_id', 'ФИО и должность ответственного лица, принявшего заявку:',['class' => 'edlabelmes']) !!}
        {!! Form::select('appointed_person_id',array_prepend($tmcusers, 'Выберите', ''),is_null($consultside->appointed_person_id) ? null : $consultside->appointed_person_id, ['class' => 'form-control']) !!}
    </div>

@endif

    <!-- doctor_id Field -->
    <div class="form-group">
        {!! Form::label('doctor_id', 'Назначенный врач-консультант и его специальность:',['class' => 'edlabelmes']) !!}
        {!! Form::select('doctor_id',['' => 'Выберите']+$doctors, $consultside->doctor_id,  ['class' => 'form-control','required']) !!}
    </div>



    <div class="form-group">
        {!!  Former::text('planned_date')
            ->label('Планируемая дата консультации',['class' => 'edlabelmes'])
             ->class('ed-controlmess')
                 ->value(Carbon\Carbon::parse(new Carbon\Carbon($consultside->planned_date))->format('Y-m-d H:i'))
               !!}
    </div>

    <div class="form-group">
        {!!  Former::text('coordinator_fullname')
          ->label('Заявку принял координатор ТМЦ',['class' => 'edlabelmes'])
            ->class('form-control')
            ->value($tmc->coordinator_fullname)
            ->readonly()->required()
            ->disabled()
              !!}
    </div>
    <!-- Comment Field -->
    <div class="form-group clear">
        {!! Form::label('comment', trans('backend.Comment') . ':',['class' => 'edlabelmes']) !!}
        {!! Form::textarea('comment', null, ['class' => 'form-control ed-controlmess']) !!}
    </div>
    <!-- Comment Field -->
    <div class="form-group ">
        {!! Form::label('file', 'Прикрепить файлы:') !!}
        {!! Form::file('files[]', ['multiple' => 'multiple'], ['class' => 'form-control']) !!}
    </div>
    @if(isset($model))
        <div class="form-group clear">
            @foreach($model->files as $file)
                <div class="thumbnail" data-id="{{$file->id}}">
                    <a href="/getfile/{{$file->file->id}}" target="_blank">{{$file->file->original_name}}</a>
                    {!! Form::button('удалить',['class' => "deletebutton btn btn-sm btn-danger pull-right"]) !!}
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>
    @endif
    <div style="display:none;" class="hiddenitems">

    </div>

    <div class="form-group col-sm-12">
        {!! Form::submit('Согласовать', ['name' => 'confirm', 'class' => 'btn btn-round btn-primary']) !!}
        {!! Form::submit('Отклонить', ['name' => 'reject', 'class' => 'btn btn-round btn-danger']) !!}
        <a href="{!! route('consultant-telemed-consult-requests.index') !!}"
           class="btn btn-round  btn-default">Закрыть</a>
    </div>
    <script>
        $(document).ready(function () {

            $appointedPersonId = $('#doctor_id').selectize()
            $appointedPersonId = $('#appointed_person_id').selectize({
                create: true,
                sortField: 'text'
            });
            var selectize = $appointedPersonId[0].selectize;
            var newVal = '<?php echo is_null($consultside->appointed_person_id) ? $consultside->appointed_person : $consultside->appointed_person_id ?>';

            selectize.setTextboxValue(newVal);

            $('input[name=planned_date]').datetimepicker({
                locale: 'ru',
                format: 'YYYY-MM-DD HH:mm'
            });
            $('.deletebutton').click(function () {
                if ($(this).hasClass("btn-primary")) {
                    $('[name="deleteditems[' + $(this).parent().data('id') + ']"]').remove();
                    console.log($('[name="hiddenitems[' + $(this).parent().data('id') + ']"]'));
                    $(this).removeClass("btn-primary");
                    $(this).addClass("btn-danger");
                    $(this).text("удалить");
                }
                else {
                    $('.hiddenitems').append("<input type='hidden' name='deleteditems[" + $(this).parent().data('id') + "]'>");
                    $(this).removeClass("btn-danger");
                    $(this).addClass("btn-primary");
                    $(this).text("отменить удаление");
                }
            });
        });

    </script>