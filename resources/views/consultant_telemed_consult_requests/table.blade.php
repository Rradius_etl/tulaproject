<table class="table table-striped" id="telemedConsultRequests-table">
    <thead>
    @include('layouts.partials.universal-filter', ['fields' => [
      ["comment"=>false],
      ["request_id"=>true,"name"=>"идентификатор"],
      ["decision"=>true,"name"=>"Решение"],
      ["ЛПУ консультант"=>false],
      ["ЛПУ абонент"=>false],
      ["Направлено в ТМЦ"=>false],
      ["telemed_event_requests:request_id|canceled"=>true,"name"=>"отменено"],
      ["telemed_event_requests:request_id|start_date"=>true,"name"=>"запланированное время"],
      ['created_at'=>true,'name'=>'отправлено в']
  ],"route"=>"consultant-telemed-consult-requests.index"])

    <th colspan="3">{{ trans('backend.action')}}</th>
    </thead>
    <tbody>
    <?php
    $st = ['<div class="alert alert-success">нет</div>', '<div class="alert alert-danger">да</div>'];
    ?>
    @foreach($telemedConsultRequests as $telemedConsultRequest)
        <?php $req = $telemedConsultRequest->request; ?>
        <tr>
            <td>{{ $telemedConsultRequest->comment }}</td>
            <td>{{ $telemedConsultRequest->uid_code."-".$telemedConsultRequest->uid_year."-".$telemedConsultRequest->uid_id }}</td>
            <td class="decision decision-{{$telemedConsultRequest->decision}}">{{trans('backend.'.$telemedConsultRequest->decision)}}</td>
            <td>@if($telemedConsultRequest->med_care_fac_id != null){!! $telemedConsultRequest->medCareFac->name !!}@else
                    не назначено @endif</td>
            <td>{{ $telemedConsultRequest->abonentSide->telemedCenter->name." - ".$telemedConsultRequest->abonentSide->telemedCenter->MedCareFac->name }}</td>
            <td>@if($telemedConsultRequest->telemed_center_id!=null){{ $telemedConsultRequest->telemedCenter->name }}@else
                    не направлено к ТМЦ@endif</td>
            <td>{!! $st[intval($req->canceled)] !!}</td>
            <td>{{ $req->start_date }}</td>
            <td>{{$telemedConsultRequest->created_at}}</td>
            <?php $conclusion = $req->telemedConclusion; ?>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('consultant-telemed-consult-requests.show', [$telemedConsultRequest->request_id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if($req->canceled == 1 || $conclusion != null)
                        <a href="#" class='btn btn-default btn-xs' data-toggle="tooltip"
                           title="Редактирование невозможно @if($req->canceled == 1) (событие отменено)@elseif($conclusion!=null)(событие имеет заключение) @endif"><i
                                    class="glyphicon glyphicon-edit"></i></a>
                    @else
                        <a href="{!! route('consultant-telemed-consult-requests.edit', [$telemedConsultRequest->request_id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @endif
                    <a href="#" data-id="{{$telemedConsultRequest->request_id }}"
                      onclick="setRequestIdForMessage({{$telemedConsultRequest->request_id }})"  data-toggle="modal" data-target="#message-dialog" title="Написать сообщение по поводу этой заявки"  class='btn btn-default btn-xs'><i class="glyphicon glyphicon-envelope"></i></a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('common.paginate', ['records' => $telemedConsultRequests])

{{-- Попап отправки сообшении =>  --}}
@section('modal_dialog_title', 'Написать сообщение<br> по поводу ТМК')
@include('messenger.new-message', ['about_request' => true, 'modal_dialog_title' => 'Написать сообщение<br> по поводу ТМК'])
@include('messenger.about-request-scripts')