@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequests'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )
@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-white box-white">
            <div class="box-header">
                <div class="row">
                    <div class="pull-right">
                        <a class="btn btn-primary" style=""
                           href="{!! route('new-consultation-first') !!}">
                            <i class="fa fa-plus"></i> {{ trans('backend.addnew_female')  }}
                        </a>

                        @if(\App\Models\TmcRoleManager::hasRole("mcf_admin") || \App\Models\TmcRoleManager::hasRole("admin"))<a class="btn btn-primary" href="{{route('telemed-register-requests.create')}}">
                            <i class="fa fa-plus"></i> Добавить заявку на добавление ТМЦ</a> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a
                                        href="{{route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Входящие</a></li>
                            <li role="presentation" ><a data-toggle="tooltip" title="Просмотр статуса заявки" href="{{route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Просмотр статуса заявки</a>
                            </li>
                            @if(\App\Models\TmcRoleManager::hasRole('mcf_admin'))
                                <li role="presentation"><a href="{{route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Перенаправление
                                        заявок</a></li>@endif
                            <li role="presentation"><a href="{{route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc'])}}">Доступные заявки</a></li>

                        </ul>
                    </div>
                </div>
                    <form action="">

                        <div class="input-group col-sm-6 pull-left">
                            {!! Form::select('conssearch',[''=>'поиск по ЛПУ-консультанту']+$mcfs->toArray(),$conssearch,['class'=>'form-control']) !!}
                            <span class="input-group-btn"> <button class="btn btn-primary" type="submit"><span
                                            class="fa fa-search"></span></button> </span>

                        </div>

                        <div class="input-group col-sm-6 pull-left" data-toggle="tooltip" title="Фильтр по дате" >
                            <input class="form-control" placeholder="Фильтр по дате" type="text" name="daterange"><span class="input-group-addon for-date"><i class="fa fa-calendar text-primary"></i></span>

                        </div>

                        <div class="row">
                            <div class=" col-sm-10 pull-left">

                                <input value="{{Input::get('search')}}" type="text" name="search" class="form-control"
                                       placeholder="Расширенный поиск" aria-label="Поиск">

                            </div>

                            <div class="col-sm-2">
                              <span class="input-group-btn"> <button class="btn btn-primary btn-block" type="submit"><i
                                              class="fa fa-search"></i> Применить </button> </span>
                            </div>
                        </div>




                </form>

        </div>
        <div class="box-body table-responsive">

            @include('consultant_telemed_consult_requests.table')

        </div>
    </div>
    </div>
@section('additional-scripts')
    @include('partials.daterange-filter')
@stop
@endsection

