@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequest') . ' &#187; ' . trans('backend.adding_new'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary box-solid">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'consultant-telemed-consult-requests.store']) !!}

                        @include('consultant_telemed_consult_requests.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
