@extends('layouts.cabinet')
<?php $title = trans('backend.TelemedConsultRequest') . ' &#187; ' .  trans('backend.editing_existed'); ?>

@section('htmlheader_title',$title )
@section('contentheader_title',$title )

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary box-solid">
           <div class="box-body">
               <div style="width:550px; margin:auto;">
                   <div class="uvedoml"> </div>
               <div class="row">
                   <h2 style="margin-bottom:50px;">Оформление заявки на телемедицинскую клиническую консультацию</h2>
                   {!! Form::model($telemedConsultRequest, ['route' => ['consultant-telemed-consult-requests.update', $telemedConsultRequest->request_id], 'method' => 'patch','files'=>true]) !!}
                   <div class="request-status request-status-{{$telemedConsultRequest->decision}}">
                       <p>Статус: <span>{{trans('backend.'.$telemedConsultRequest->decision)}}</span></p>
                   </div>
                   @include('consultant_telemed_consult_requests.fields',["doctors"=>$doctors,'model'=>$telemedConsultRequest])

                   {!! Form::close() !!}
               </div>
                   </div>
           </div>
       </div>
   </div>
@endsection