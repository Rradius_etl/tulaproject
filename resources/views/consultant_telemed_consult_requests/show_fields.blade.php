<div class="box box-primary box-solid">
    <div class="box-header">
        Часть ЛПУ консультанта
    </div>

    <div class="box-body">

        <div class="" style="">
            <?php
            $consultSide = $telemedConsultRequest->consultantSide;
            $genders = ['male' => 'Мужской', 'female' => 'Женский']
            ?>
            @if($consultSide != null)
                <table class=" table table-striped">
                    <tbody>
                    <tr>
                        <td> {!! Form::label('', 'Выбранный доктор и его специальность:' ) !!} </td>
                        <td>{{ $consultSide->doctor->surname." ". $consultSide->doctor->name." ". $consultSide->doctor->middlename." : ". $consultSide->doctor->spec }}</td>
                    </tr>
                    <tr>
                        <td>  {!! Form::label('planned_date', 'Запланированная дата и время:' ) !!}</td>
                        <td>{{ $consultSide->planned_date }}</td>
                    </tr>
                    <tr>
                        <td>  {!! Form::label('comment', trans('backend.Comment') . ':' ) !!}</td>
                        <td>{{ $consultSide->telemedConsultRequest->comment }}</td>
                    </tr>
                    <tr>
                        <td>  {!! Form::label('created_at', 'ФИО принявшего заявку(ЛПУ-координатора):' ) !!}</td>
                        <td>{{ $consultSide->coordinator_fullname   }}</td>
                    </tr>
                    <tr>
                        <td> Файлы:</td>
                        <td>
                            @foreach($consultSide->files as $file)
                                <div class="form-group">
                                    <a href="/getfile/{{$file->file->id}}"
                                       target="_blank">{{$file->file->original_name}}</a>
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td> {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}</td>
                        <td>{{ $consultSide->created_at }}</td>
                    </tr>
                    <tr>
                        <td> {!! Form::label('decision', trans('backend.Decision').":") !!}</td>
                        <td>{{ trans("backend.".$consultSide->telemedConsultRequest->decision) }}</td>
                    </tr>
                    </tbody>
                </table>
            @else
                Координатор ЛПУ еще не заполнил свою форму
            @endif
        </div>
    </div>
</div>

<div class="box @if($telemedConsultRequest->telemed_center_id!=null)  box-success @else  box-info @endif box-solid">
    <div class="box-header">
        Куда была направлена заявка
    </div>

    <div class="box-body" id="zebr">
        @if($telemedConsultRequest->telemed_center_id!=null)
            @if($telemedConsultRequest->telemedCenter!=null)
                <a data-toggle="tooltip" title="Нажмите чтобы узнать подробно об этом ТМЦ" href="{{route('general-telemed-centers.show',$telemedConsultRequest->telemedCenter->id)}}">{{ $telemedConsultRequest->telemedCenter->name }}</a>
            @else
                ТМЦ был удален
            @endif
        @else
            не направлено к ТМЦ
        @endif

    </div>
</div>

<div class="box box-primary box-solid">
    <div class="box-header">
        Часть ЛПУ абонента
    </div>

    <div class="box-body">
        <div class="row">
            <!-- Comment Field -->
            <?php $abonentSide = $telemedConsultRequest->abonentSide; ?>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>  {!! Form::label('doctor_fullname', trans('validation.attributes.doctor_fullname') . ':' ) !!}</td>
                    <td>{{ $abonentSide->doctor_fullname}} &nbsp;</td>
                </tr>
                <tr>
                    <td> {!! Form::label('doctor_spec', trans('validation.attributes.doctor_spec') . ':' ) !!}</td>
                    <td>{{ $abonentSide->doctor_spec}} &nbsp;</td>
                </tr>
                <tr>
                    <td>  {!! Form::label('consult_type', trans('validation.attributes.consult_type') . ':' ) !!}</td>
                    <td>   {{ $abonentSide->ConsultationType->name }} &nbsp;</td>
                </tr>
                <tr>
                    <td> {!! Form::label('MCF', trans("backend.MCF") . ':' ) !!}</td>
                    <td>      {{ $abonentSide->medCareFac->name }} &nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('desired_consultant', trans("validation.attributes.desired_consultant") . ':' ) !!}</td>
                    <td>   {{ $abonentSide->desired_consultant }} &nbsp;</td>
                </tr>
                <tr>
                    <td>   {!! Form::label('desired_date', trans("validation.attributes.desired_date") . ':' ) !!}</td>
                    <td>    {!!  $abonentSide->desired_date != null ? date("d-m-Y",strtotime($abonentSide->desired_date)) : "<span class=\"label label-warning\">не указана</span>" !!} &nbsp;</td>
                </tr>
                <tr>
                    <td>  {!! Form::label('desired_time', trans("validation.attributes.desired_time") . ':' ) !!}</td>
                    <td> {{  date("H:i",strtotime($abonentSide->desired_time))  }}&nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('patient_uid_or_fname', trans("validation.attributes.patient_uid_or_fname") . ':' ) !!}</td>
                    <td>      {{ $abonentSide->patient_uid_or_fname }}&nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('patient_birthday', trans('validation.attributes.patient_birthday') . ':' ) !!}</td>
                    <td>{{ $abonentSide->patient_birthday }}&nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('patient_address', trans('validation.attributes.patient_address') . ':' ) !!}</td>
                    <td>{{ $abonentSide->patient_address }}&nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('patient_gender', trans('validation.attributes.patient_gender') . ':' ) !!}</td>
                    <td>{{ $genders[$abonentSide->patient_gender] }}&nbsp;</td>
                </tr>
                <tr>
                    <td> {!! Form::label('patient_social_status', trans('validation.attributes.patient_social_status') . ':' ) !!}</td>
                    <td>&nbsp;@if($abonentSide->socialStatus != null){{ $abonentSide->socialStatus->name }}@else Не указано @endif</td>
                </tr>
                <tr>
                    <td> {!! Form::label('patient_indications_for_use', trans('validation.attributes.patient_indications_for_use') . ':' ) !!}</td>
                    <td><span>{{$abonentSide->getIndForUse() }}&nbsp;</span>&nbsp;</td>

                </tr>
                <tr>
                    <td> {!! Form::label('decision', trans('validation.attributes.patient_indications_for_use').' - другое:' ) !!}</td>
                    <td> {{ $abonentSide->patient_indications_for_use_other }}&nbsp;</td>
                </tr>
                <tr>
                    <td>  {!! Form::label('patient_questions', trans('validation.attributes.patient_questions') . ':' ) !!}</td>
                    <td> {{ $abonentSide->patient_questions }}&nbsp;</td>
                </tr>
                <tr>
                    <td>{!! Form::label('telemed_center_id', trans('backend.TMC') . ':' ) !!}</td>
                    @if($abonentSide->telemed_center_id != null)
                        <?php $tmc1 = $abonentSide->telemedCenter ?>
                        @if($tmc1 != null)
                            <td>   {{ $abonentSide->telemedCenter->name }}</td>
                        @else
                            ТМЦ был удален
                        @endif
                    @endif
                </tr>
                <tr>
                    <td> {!! Form::label('created_at', trans('backend.Created At') . ':' ) !!}</td>
                    <td> {{ $abonentSide->created_at }}</td>
                </tr>
                <tr>
                    <td>{!! Form::label('updated_at', trans('backend.Updated At') . ':' ) !!}</td>
                    <td>{{ $abonentSide->updated_at }}</td>
                </tr>
                </tbody>
            </table>
            @if(count($files)> 0)
                <ul>
                    <span style="margin-left: -40px;">Загруженные файлы:</span>
                    @foreach($files as $file)
                        <li><a href="/getfile/{{$file->file_id}}">{{$file->original_name}}</a></li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>