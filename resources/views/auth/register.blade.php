@extends('layouts.main')

@section('content')
@section('contentheader_title', trans('routes.register'))
<div class="box box-white">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        @if (count($errors->custom) > 0)
            <div class="alert alert-danger">
                {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->custom->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-info">
                {{ Session::get('success') }}
            </div>
        @endif


        <div class="col col-sm-8 col-sm-offset-2">
            {!! Former::horizontal_open()->method('POST')->url('/register') !!}
            <div class="form-group has-feedback">
                {!! Former::email('email') !!}
            </div>

            <div class="form-group has-feedback">
                {!! Former::text('name') !!}
            </div>

            <div class="form-group has-feedback">
                {!! Former::text('surname') !!}
            </div>

            <div class="form-group has-feedback">
                {!! Former::text('middlename') !!}
            </div>

            <div class="form-group has-feedback">
                {!! Former::text('position') !!}
            </div>

            <div class="form-group has-feedback">
                {!! Former::password('password') !!}
            </div>


            <div class="form-group has-feedback">
                {!! Former::password('password_confirmation')->label(trans('adminlte_lang::message.retrypepassword')) !!}
            </div>


            <div class="form-group">
                <div class="col-sm-8 col-md-offset-4">
                    <div class="checkbox checkbox_register">
                        <label class="">
                            <input type="checkbox"
                                   name="terms" class="text-center"> {{ trans('adminlte_lang::message.terms') }}
                        </label>
                    </div>
                </div><!-- /.col -->

            </div>

            <div class="clearfix"></div>
            <div class="text-center">
                {!! Former::submit( trans('adminlte_lang::message.register') )->class('btn btn-primary') !!}
            </div>


            {!! Former::close() !!}
            <hr>
            <div class="text-center">
                <a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.membreship') }}</a>
                <a href="{{ url('/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>
            </div>

        </div>


    </div>
    <!-- /.box-body -->
    @include('layouts.partials.scripts_auth')
</div>

@endsection

