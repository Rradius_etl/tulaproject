@extends('layouts.main')

@section('content')
    @section('contentheader_title', trans('routes.login'))
    <div class="box box-white">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Ошибка
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif


            {{ Form::open(['url' => url('/login'), 'method' => 'POST', 'class' => 'form-horizontal', 'style' => 'width: 40%; margin: 0 auto']) }}

            <div class="form-group has-feedback">
                {{Form::label(trans('adminlte_lang::message.email'), null, ['class' => 'col-sm-4 control-label'] ) }}
                <div class="col-sm-8">
                    {{ Form::input('email', 'email', null, ['class' => ' form-control']) }}
                </div>

            </div>
            <div class="form-group has-feedback">
                {{Form::label(trans('adminlte_lang::message.password'), null, ['class' => 'col-sm-4 control-label'] ) }}
                <div class="col-sm-8">
                    {{ Form::input('password', 'password', null, ['class' => ' form-control']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-md-offset-4">
                    <div class="checkbox">
                        <label class="">
                            <input type="checkbox"
                                   name="remember" class="text-center"> {{ trans('adminlte_lang::message.remember') }}
                        </label>
                    </div>
                </div><!-- /.col -->

            </div>
            <div class="text-center">
                <button type="submit"
                        class="btn btn-primary btn-flat">{{ trans('adminlte_lang::message.buttonsign') }}</button>
            </div>

            {{ Form::close() }}
            <hr>
            <div class="text-center">
                <a href="{{ url('/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>
                <a href="{{ url('/register') }}" class="">{{ trans('adminlte_lang::message.registermember') }}</a>
            </div>



        </div>
        <!-- /.box-body -->
        @include('layouts.partials.scripts_auth')
    </div>

@endsection
