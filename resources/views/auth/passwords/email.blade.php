@extends('layouts.main')

@section('meta_title', 'Восстановление пароля')

@section('contentheader_title', 'Восстановление пароля')
@section('content')

    <div class="box box-white">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Ошибка
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif
                {{ Form::open(['url' => url('/reset'), 'method' => 'POST', 'class' => 'form-horizontal', 'style' => 'width: 40%; margin: 0 auto']) }}


                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-round btn-flat">{{ trans('adminlte_lang::message.sendpassword') }}</button>
                    </div>
                {{ Form::close() }}
            <hr>
            <div class="text-center">
                <a href="{{ url('/login') }}">Войти</a><br>
                <a href="{{ url('/register') }}" >{{ trans('adminlte_lang::message.registermember') }}</a>
            </div>



        </div>
        <!-- /.box-body -->
        @include('layouts.partials.scripts_auth')

        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </div>

@endsection
