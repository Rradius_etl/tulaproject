@extends('layouts.main')
@section('meta_title', 'Восстановление пароля')
@section('contentheader_title', 'Восстановление пароля')

@section('content')
    <div class="box box-white">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Ошибка
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif


            {{ Form::open(['url' => Request::url(), 'method' => 'POST', 'class' => 'form-horizontal', 'style' => 'width: 40%; margin: 0 auto']) }}


            <div class="form-group has-feedback">
                <label  for="">Введите Email</label>
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <label  for="">Введите новый пароль</label>
                <input type="password" class="form-control" placeholder="Пароль" name="password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <label  for="">Введите повторно новый пароль</label>
                <input type="password" class="form-control" placeholder="Пароль" name="password_confirm"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-2">
                </div><!-- /.col -->
                <div class="col-xs-8">
                    <button type="submit"
                            class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.passwordreset') }}</button>
                </div><!-- /.col -->
                <div class="col-xs-2">
                </div><!-- /.col -->
            </div>


            {{ Form::close() }}
            <hr>
            <div class="text-center">
                <a href="{{ url('/login') }}">Войти</a><br>
                <a href="{{ url('/register') }}" >{{ trans('adminlte_lang::message.membreship') }}</a>
            </div>


        </div>
        <!-- /.box-body -->
        @include('layouts.partials.scripts_auth')
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </div>

@endsection


