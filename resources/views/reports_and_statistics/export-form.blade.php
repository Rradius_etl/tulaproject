<div id="modal-dialog" class="ngdialog ngdialog-theme-plain" role="alertdialog" style="display: none;">

    <div class="ngdialog-content" role="document">

        <div class="imess"></div>
        <div class="box box-white  col-xs-12 col-sm-3">
            <div class="box-header text-center">
                <h2>Экспорт статистики</h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <style>
                    .select2-dropdown {
                        z-index: 99999999;
                    }
                    .select2-container--default .select2-search__field, .select2-container--default .select2-search.select2-search--inline {
                        width: 100% !important;
                    }
                    .daterangepicker.dropdown-menu {
                        z-index: 999999 !important;
                    }
                </style>
                {!! Former::open_vertical()->id('report-export-form')->route('polls.notify')->method('POST')->dataUrl(isset($dataUrl) ? $dataUrl : '/tmc_report/') !!}

                {!! Former::select('format')
                    ->options(['pdf' => 'Adobe pdf', 'docx' => 'Microsoft Word', 'xls' => 'Microsoft Excel', 'odt' => 'OpenDocument'])
                    ->label('Формат вывода')
                    ->append('<i class="fa fa-chevron-down text-primary"></i>')!!}


                {!!  Former::text('export-daterange')
                                  ->class('form-control')
                                  ->label('Временной диапазон')
                                  ->dataExclude('yes')
                                  ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                {!! Former::select('tmc')
                                    ->options($tmcs)
                                    ->value($active_tmc)
                                    ->label( "Выберите ТМЦ")
                                      ->append('<i class="fa fa-chevron-down text-primary"></i>')
                                      !!}


                {!! Former::hidden('type')->dataExclude('yes') !!}

                {!! Former::hidden('from') !!}
                {!! Former::hidden('to') !!}
                <a href="" class="file-download-link hidden"></a>

                {!!  Former::large_round_primary_submit('Скачать отчет') !!}

                <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Закрыть</button>


                {!! Former::close() !!}


                <script>
                    var exportDateRange;
                    $(document).ready(function () {


                        $('#report-export-form input[name=from]').val(dateRangeDefaults.startDate.format('YYYY-MM-DD'));
                        $('#report-export-form input[name=to]').val(dateRangeDefaults.endDate.format('YYYY-MM-DD'));


                         exportDateRange = $("[name='export-daterange']");
                        $(exportDateRange).daterangepicker(dateRangeDefaults);

                        $(exportDateRange).on('apply.daterangepicker', function (ev, picker) {
                            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                            $('#report-export-form input[name=from]').val(picker.startDate.format('YYYY-MM-DD'))
                            $('#report-export-form input[name=to]').val(picker.endDate.format('YYYY-MM-DD'))


                        });



                        $(exportDateRange).on('cancel.daterangepicker', function (ev, picker) {
                            $(this).val('');
                        });

                        $('#report-export-form').submit(function (event) {
                            var $form = $(this);
                            event.preventDefault();

                            var dataUrl = $form.data('url');
                            if (dataUrl == '/admin/tmc_report/') {
                                dataUrl = dataUrl + $form.find('select[name=tmc]').val() + '/';
                                var params = $form.find('[data-exclude!=yes]').serializeArray();
                            } else if (dataUrl == '/tmc_report/') {
                                dataUrl = dataUrl + $form.find('select[name=tmc]').val() + '/';
                                var params = $form.find('[data-exclude!=yes]').serializeArray();
                            } else if (dataUrl == '/admin/conference_report/') {
                                var params = $form.find('[data-exclude!=yes]').serializeArray();
                                params.push($form.find('[name=type]').serializeArray()[0]);
                            } else {
                                var params = $form.find('[data-exclude!=yes]').serializeArray();
                            }


                            var url = dataUrl + '?' + $.param(params);



                            console.log(params, url)

                            var a = $('.file-download-link').attr('href', url)[0].click();


                        })
                    });
                </script>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="ngdialog-close close" data-dismiss="modal"></div>
    </div>
</div>

