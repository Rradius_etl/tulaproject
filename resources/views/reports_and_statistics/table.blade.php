@if($full)
    <style>

        body {
            font: normal medium/1.4 sans-serif;

        }
        table {
            border-collapse: collapse;
            width: 100%;

        }
        th, td {
            padding: 0.25rem;
            text-align: left;
            border: 1px solid #000;
        }
        tbody tr:nth-child(odd) {
            background: #eee;
        }
        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }
    </style>
@endif


<?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
<table class="table table-stripped" id="polls-table">
    <thead  >
         <th >№ по порядку</th>
         <th >Наименование учреждения здравоохранения</th>
         <th >Наименование ТМЦ</th>
         <th >E.164 ID (номер терминала)</th>
         <th >H.323 ID (название терминала)</th>
         <th >Общее количество видеоконференций</th>
         <th >Из них совещаний</th>
         <th >Из них обучающих семинаров</th>
         <th >Из них клинических консультаций</th>
         <th >Норма клинических консультаций</th>
         <th >Количество вызовов </th>
         <th >Минут разговора</th>
         <th >Секунд разговора</th>
    </thead>
    <tbody>
    <?php
    $i = 1;
    ?>
    @foreach($reports as $s)
        <tr  >
            <td>{{$i}}</td>
            <td>{{$s->mcf_name}}</td>
            <td>{{$s->tmc_name}}</td>
            <td>-</td>
            <td>-</td>
            <td>{{$s->count}}</td>
            <td>{{$s->meeting_count}}</td>
            <td>{{$s->seminar_count}}</td>
            <td>{{$s->consult_count}}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
        $i++;
        ?>
    @endforeach
    </tbody>
</table>