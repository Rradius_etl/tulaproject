@extends('layouts.cabinet')
<script src="/bower_components/downloadjs/download.min.js"></script>
<?php $title = trans('backend.Statistics'); ?>

@section('meta_title',$title )
@section('contentheader_title',$title )

@section('content')

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <!--  ====  SECTION: 3nd box   ===== -->
        <div class="box box-primary box-white">
            <div class="box-header">

                <div class="col-md-10 pull-left">

                    {!! Former::open_vertical() !!}

                    {!!  Former::text('daterange')
                    ->class('form-control')->addGroupClass('col-md-4')
                    ->dataId('tmc_report')
                    ->dataUrl('/tmc_report/x')
                    ->label(false)
                    ->append('<i class="fa fa-calendar text-primary"></i>') !!}

                    {!! Former::select('tmcs')
                    ->options($tmcs)
                    ->value($active_tmc)
                    ->label(false)
                    ->addGroupClass('col-md-4')
                      ->append('<i class="fa fa-chevron-down text-primary"></i>')
                      !!}


                    {!! Former::select('diagram')
                    ->options([null => 'Выбрать тип графика', 'linechart'=> 'график', 'barchart'=> 'диаграмма'])
                    ->value(null)
                    ->label(false)
                    ->addGroupClass('col-md-4')
                      ->append('<i class="fa fa-bar-chart"></i>')
                      !!}

                    {!! Former::close() !!}


                </div>
                <div class="pull-right"><a href="#" class="export-button" style="display: block;"
                                           onclick="openModal('/tmc_report/',3)">Выгрузить</a></div>
            </div>
            <div class="box-body">
                <h5 class="box-title">Отчет по видеоконференциям по отдельному ТМЦ</h5>

                <div class="clearfix"></div>
                <div id="tmc_report" class="table-responsive"></div>

                <div class="thumbnail">
                    <img id="tmc_report-chart" src="" alt="">

                </div>
                <div class="row download-links" style="display: none;">
                    &nbsp;<a class="btn btn-primary pull-right" style="margin-left: 10px;" id="pdf-download-link" href="#" ><i class="fa fa-picture-o"></i> cкачать как PDF</a> &nbsp;
                    &nbsp;<a  download="Отчет по видеоконференциям по отдельному ТМЦ.jpeg" class="btn btn-primary pull-right" href="#" id="img-download-link" ><i class="fa fa-picture-o"></i>  cкачать как JPEG изображение</a> &nbsp;
                </div>
            </div>
        </div>

    </div>

    @include('reports_and_statistics.export-form')
    <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        .box-header .form-group {
            margin-bottom: 0;
        }
    </style>
    <script src="/plugins/daterangepicker/moment.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
        var pageTitles = [
            '',
            'Отчет о проведенных видеоконференциях в разрезе ТМЦ',
            'Отчет по клиническим консультациям в разрезе ТМЦ',
            'Отчет по видеоконференциям по отдельному ТМЦ',
            'Отчет по совещаниям в разрезе ТМЦ',
            'Отчет по обучающим семинарам в разрезе ТМЦ',
        ];

        var dateRangeDefaults = {
            timePicker: false,
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),

            locale: {
                format: 'DD-MM-YYYY',
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Очистить",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Выбрать диапазон",
                "weekLabel": "W",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            ranges: {
                'За сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };


        function isInt(value) {
            return !isNaN(value) &&
                    parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        }


        $(function () {

            moment.locale('ru');

            $('[data-id=tmc_report]').data('url', '/tmc_report/' + $('[name=tmcs]').val());

            $('[name=tmcs]').change(function () {
                console.log($('[data-id=tmc_report]').data('url'))
                $('[data-id=tmc_report]').data('url', '/tmc_report/' + $(this).val());
                $('.download-links').hide();

            });


            $('input[name="daterange"]').each(function (idx, el) {

                $(el).daterangepicker(dateRangeDefaults);

                $(el).on('apply.daterangepicker', function (ev, picker) {

                    $(exportDateRange).data('daterangepicker').setStartDate(picker.startDate);
                    $(exportDateRange).data('daterangepicker').setEndDate(picker.endDate);

                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

                    $('#tmc_report-chart').attr('src', '/images/wait.gif' );

                    var params = {
                        from: picker.startDate.format('YYYY-MM-DD'),
                        to: picker.endDate.format('YYYY-MM-DD'),
                        diagram: $('#diagram').val()
                    };
                    if ($(this).data('type') !== undefined) {
                        params.type = $(this).data('type');
                    }


                    var selector = $(this).data('id');
                    var url = $(this).data('url');


                    $('#' + selector).load(url + '?' + $.param(params));

                    console.log( 'url with params', url + '?' + $.param(params))
                    if( url.search( '/tmc_report/') != -1 ) {
                        var chart_url =  '/tmc_chart/' + $('[name=tmcs]').val() + '?'  + $.param(params);
                        console.log('chart url', chart_url)
                        $.ajax( {
                            url: chart_url,
                            success: function (data) {

                                $('#tmc_report-chart').attr('src',  chart_url + '&download=url' );
                                $('#img-download-link').attr('href',  chart_url + '&download=url' );
                                $('#pdf-download-link').attr('href',  chart_url + '&download=pdf' );
                                $('.download-links').show();
                            },
                            error: function (err) {
                                alert(err.responseJSON.error);
                            }
                        } );
                    }



                });

                $(el).on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

            });

            $('input[name="daterange"]').trigger('change')


        });



        function openModal(url, titleID, secondaryParam) {
            /* TODO: secondaryParam Проверить если цифра то если нет то другое */

            var param = (typeof  secondaryParam !== undefined ) ? secondaryParam : '';
            if (url == '/tmc_report/') {
                $('#report-export-form .tmc-group').show()
            } else {
                $('#report-export-form .tmc-group').hide()
            }
            $('#report-export-form input[name=type]').val(param);


            $('#modal-dialog .modal-title').text(pageTitles[titleID]);

            $('#report-export-form').data('url', url);
            $('#modal-dialog').modal('show')
        }
    </script>
@endsection