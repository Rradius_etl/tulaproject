@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')

    <?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
    <table class="table table-stripped" id="polls-table">
        <thead>
        <tr>
            @foreach($headers as $header)
                <th> {{ $header }}</th>
            @endforeach
        </tr>

        </thead>
        <tbody>
        <?php
        $i = 1;
        $mcfs = [];
        ?>
        @foreach($reports as $s)
            <tr>
                <td>{{$i}}</td>
                <td>{{$s->mcf_name}}</td>
                <td>
                    @if(!array_key_exists($s->mcf_id,$mcfs))
                        <?php
                        $mcfs[$s->mcf_id]['count'] = 1;
                        ?>
                    @endif
                    {{$mcfs[$s->mcf_id]['count'].") ".$s->tmc_name}}
                    <?php
                    $mcfs[$s->mcf_id]['count'] = $mcfs[$s->mcf_id]['count'] + 1;
                    ?>
                </td>
                <td>{{$s->tmc_address}}</td>
                <td class="text-center">{{$s->terminal_number}}</td>
                <td class="text-center">{{$s->terminal_name}}</td>
                <td>{{$s->count}}</td>
                <td>{{$s->meeting_count}}</td>
                <td>{{$s->seminar_count}}</td>
                <td>{{$s->consult_count}}</td>
                <td>@if($s->cons_norm !== 0) {{$s->cons_norm}} @else  <span style="text-align: center">-</span> @endif
                </td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
            </tr>
            <?php
            $i++;
            ?>
        @endforeach
        </tbody>
    </table>
@endsection