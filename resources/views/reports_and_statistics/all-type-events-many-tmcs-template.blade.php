@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')
    <table class="table">
        <thead>

        <tr>
            @foreach($headers as $header)
                <th>{{$header}}</th>
            @endforeach
        </tr>

        </thead>
        <tbody>
        <?php $i = 1?>
        @foreach($reports as $r)
            @foreach($r['reports'] as $report)
                <tr>
                    <td> {{ $i }}</td>
                    <?php $i++; ?>
                    <td>{{ $r['mcf']->name  }}</td>
                    <td> {{ $r['tmc']->name  }}</td>
                    <td> {{$r['tmc']->address  }}</td>
                    @if( $report->event_type == 'consultation' )
                        <td> {{ $report->create_date }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif
                    <td>{{ $report->start_date }}</td>
                    <td> @if($r['tmc']->id != $report->initiator_id) Входящая @else Исходящая @endif </td>
                    <td> {{$eventTypes[$report->event_type]}} </td>
                    <!-- Тип ТМК -->
                    @if( $report->event_type == 'consultation' )
                        <td> {{ $report->consult_type }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif
                <!-- Номер заявки -->
                    @if( $report->event_type == 'consultation' )
                        <td> {{  $report->cons_uid }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif
                <!-- Лечащий врач -->
                    @if( $report->event_type == 'consultation' )
                        <td> {{  $report->medic }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif
                <!-- Наименование ТМЦ консультанта -->
                    @if( $report->event_type == 'consultation' )
                        <td> {{  $report->cons_tmc_name }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif

                <!-- Врач консультант -->
                    @if( $report->event_type == 'consultation' )
                        <td> {{  $report->consult_doctor }}
                        </td> @else
                        <td class="text-center"><span style="text-align: center">-</span></td>
                    @endif

                <!-- Показание к применению -->
                    @if( $report->event_type == 'consultation' )
                        <td> @if($report->patient_indications_for_use_id != 'other')
                                 @if(isset($indForUse[$report->patient_indications_for_use_id]))
                                {{$indForUse[$report->patient_indications_for_use_id]}}
                                 @else
                                    Показание к применению была удалено
                                 @endif
                            @else
                                Иное: {{$report->patient_indications_for_use_other}}
                            @endif
                        </td>
                    @else
                        <td class="text-center"><span style="text-align: center">-</span>
                        </td>
                    @endif

                <!-- Тема обучающего семинара -->

                    @if( $report->event_type == 'seminar' )
                        <td>   {{  $report->title }}  </td>
                    @else
                        <td class="text-center"><span style="text-align: center">-</span>
                        </td>
                    @endif

                <!-- Тема совещания -->

                    @if( $report->event_type == 'meeting' )
                        <td>   {{  $report->title }}  </td>
                    @else
                        <td class="text-center"><span style="text-align: center">-</span>
                        </td>
                    @endif

                <!-- Количество участников -->


                    @if( $report->event_type != 'consultation' )
                        <td>   {{  $report->part_count }}     </td>
                    @else
                        <td class="text-center"><span
                                    style="text-align: center">-</span></td>
                    @endif
                <!-- Степень важности -->

                    @if( $report->event_type != 'consultation' )
                        <td>    {{ $importances[$report->importance] }} </td>
                    @else
                        <td class="text-center"><span
                                    style="text-align: center">-</span></td>
                    @endif
                </tr>
            @endforeach
        @endforeach
        </tbody>

    </table>
@endsection