@section('modal_dialog_content')

    @extends('partials.admin-dialog-template')

@section('modal_dialog_title','Экспорт статистики')

<link rel="stylesheet" href="/plugins/select2/select2.css">
<script src="/plugins/select2/select2.js"></script>
<script src="/plugins/select2/i18n/ru.js"></script>
<style>
    .select2-dropdown {
        z-index: 99999999;
    }
    .select2-container--default .select2-search__field, .select2-container--default .select2-search.select2-search--inline {
        width: 100% !important;
    }
</style>
{!! Former::open_vertical()->id('report-export-form')->route('polls.notify')->method('POST') !!}

{!! Former::select('format')->options(['pdf' => 'Adobe pdf', 'docx' => 'Microsoft Word', 'xls' => 'Microsoft Excel', 'odt' => 'OpenDocument']) ->append('<i class="fa fa-chevron-down text-primary"></i>')->label('Формат вывода') !!}


{!!  Former::text('export-daterange')
                  ->class('form-control')
                  ->label('Временной диапазон')
                  ->dataExclude('yes')
                  ->append('<i class="fa fa-calendar text-primary"></i>') !!}


{!! Former::multiselect('tmcs')->options($lists)->value(array_first($lists))
->label(false)->dataExclude('yes')->addGroupClass('many-tmc-group')->append('<i class="fa fa-chevron-down text-primary"></i>')
 ->dataToggle('tooltip')->dataPlacement('top')->title('Зажмите кнопку ctrl или shift для множественной выборки') !!}

<div class="form-group tmc-group">
    {!! Form::label('tmc', "Выберите ТМЦ" . ':', ['class' => 'edlabelmes']) !!}
    <select name="tmc" class="ed-controlmess js-example-responsive js-states form-control" data-exclude="yes"  style="width: 100%;" >
    </select>
</div>
{!! Former::hidden('type')->dataExclude('yes') !!}

{!! Former::hidden('from') !!}
{!! Former::hidden('to') !!}


{!!  Former::large_round_primary_submit('Скачать отчет') !!}
<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

<a href="" class="file-download-link hidden"></a>
{!! Former::close() !!}


<script>
    var QueryStringToHash = function QueryStringToHash  (query) {
        var query_string = {};
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            pair[0] = decodeURIComponent(pair[0]);
            pair[1] = decodeURIComponent(pair[1]);
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    };
</script>

@endsection