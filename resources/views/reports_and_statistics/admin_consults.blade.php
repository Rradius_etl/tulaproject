@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')

    <?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
    <table class="table table-striped" id="polls-table">
        <thead>
        <tr>
            @foreach($headers as $header)
                <th>{{$header}}</th>
            @endforeach
        </tr>

        </thead>
        <tbody>
        <?php
        $i = 1;
        $mcfs = [];
        ?>
        @foreach($reports as $s)
            <tr>
                <td>{{$i}}</td>
                <td>{{$s->mcf_name}}</td>
                <td>
                    @if(!array_key_exists($s->mcf_id,$mcfs))
                        <?php
                        $mcfs[$s->mcf_id]['count'] = 1;
                        ?>
                    @endif
                    {{$mcfs[$s->mcf_id]['count'].") ".$s->tmc_name}}
                    <?php
                    $mcfs[$s->mcf_id]['count'] = $mcfs[$s->mcf_id]['count'] + 1;
                    ?>
                </td>
                <td>{{$s->tmc_address}}</td>
                <td class="text-center">{{$s->terminal_number}}</td>
                <td class="text-center">{{$s->terminal_name}}</td>
                <td>{{$s->count}}</td>
                @if($s->cons_norm !== 0)
                    <td>   {{$s->cons_norm}} </td>
                @else
                    <td class="text-center"><span style="text-align: center">-</span></td>
                @endif
            </tr>
            <?php
            $i++;
            ?>
        @endforeach
        </tbody>
    </table>

@endsection