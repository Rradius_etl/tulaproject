@if( $full )
    @extends('reports_and_statistics.report-layout')
@endif
@section('content')

    <?php $arr = ["нет", "да"]; $classes = ["label label-danger", "label label-success"]  ?>
    <table class="table table-striped" id="polls-table">
        <thead>
            <tr>
                @foreach($headers as $header)
                    <th> {{$header}}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        $mcfs = [];
        ?>
        @foreach($reports as $s)
            <tr>
                <td>{{$i}}</td>
                <td>{{$s->mcf_name}}</td>
                <td>
                    @if(!array_key_exists($s->mcf_id,$mcfs))
                    <?php
                        $mcfs[$s->mcf_id]['count'] = 1;
                        ?>
                    @endif
                        {{$mcfs[$s->mcf_id]['count'].") ".$s->tmc_name}}
                        <?php
                        $mcfs[$s->mcf_id]['count'] = $mcfs[$s->mcf_id]['count'] + 1;
                        ?>
                </td>
                <td>{{$s->tmc_address}}</td>
                <td class="text-center">  <span style="text-align: center !important;">{{$s->terminal_number}}</span></td>
                <td class="text-center">  <span style="text-align: center !important;">{{$s->terminal_name}}</span></td>
                <td>{{$s->count}}</td>
                <td>{{$s->high_count}}</td>
                <td>{{$s->medium_count}}</td>
                <td>{{$s->low_count}}</td>
            </tr>
            <?php
            $i++;
            ?>
        @endforeach
        </tbody>
    </table>

@endsection