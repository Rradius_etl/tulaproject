<nav class="navbar navbar-default navbar-custom" style="display: none;">
    <div class="container">


        <ul class="nav navbar-nav navbar-right pull-right">
            <li><div class="frameForSingle">
                    <button type="button" id="goToOriginal" class="btn btn-default"> <i class="fa fa-angle-left"></i> Обычная версия</button>
                </div></li>
        </ul>
        <div class="navbar-collapse">
            <ul class="nav navbar-nav">
                <li><div class="frame">
                        <span class="bold text-center">Размер шрифта</span>
                    </div>
                    <div class="text-center">
                        <button type="button" id="font-sm" class="btn btn-default" onclick="fontReset()">A</button>
                        <button type="button" id="font-md" class="btn btn-default" onclick="fontRescale(-4)">-A</button>
                        <button type="button" id="font-lg" class="btn btn-default" onclick="fontRescale(+4)">+A</button>
                    </div></li>
                <li><div class="frame">
                        <span class="bold text-center">Интервал</span>
                    </div>
                    <div class="text-center">
                        <button type="button" id="line-height-sm" class="btn btn-default" onclick="setLineHeight(-1)">-</button>
                        <button type="button" id="line-height-lg" class="btn btn-default" onclick="setLineHeight(1)">+</button>
                    </div></li>
                <li><div class="frame">
                        <span class="bold text-center">Цвет сайта</span>
                    </div>
                    <div class="text-center">
                        <button type="button" id="black" class="btn btn-default impaired-white bold" onclick="setColors('impaired-white')">Ц</button>
                        <button type="button" id="white" class="btn btn-default impaired-black bold" onclick="setColors('impaired-black')">Ц</button>
                        <button type="button" id="blue" class="btn btn-default impaired-darkblue bold" onclick="setColors('impaired-darkblue')">Ц</button>
                    </div></li>
                <li><div class="frame">
                        <span class="bold text-center">Изображения</span>
                    </div>
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default" onclick="setImageClass()">Вкл</button>
                        <button type="button" class="btn btn-default" onclick="setImageClass('hidden')">Выкл</button>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</nav>