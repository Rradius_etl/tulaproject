<?php  $lastNotification = \App\Models\NotificationManager::getLastUnreadNotification() ?>
@if($lastNotification != null)


    <!-- Modal -->
    <div id="login-dialog" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{'Здравствуйте, у вас ' . \App\Models\NotificationManager::getUnreadNotificationsCount() . ' новых уведомлений'}}</h4>
                </div>
                <div class="modal-body">

                    <div class="box-title">Последнее уведомление</div>
                    <div class="popupmes">
                        <div class="pull-left">
                            <div class="thumbnail"><img src="/images/no-avatar.jpg" alt=""></div>
                        </div>
                        <div class="popuptitle">
                            <p>{!! $lastNotification->created_at!!}</p>
                            <p>{!! $lastNotification->user->getfName() !!}</p>

                        </div>
                        <div style="clear:both;"></div>
                        <div style="margin-bottom: 20px">{!! $lastNotification->notification->message !!}</div>
                        <button type="button" onclick="markAsRead({{$lastNotification->id}})" class="btn btn-round btn-sm btn-primary ">
                            <span class="glyphicon glyphicon-bell"></span> Отметить прочитанным
                        </button>
                        <button type="button" class="btn btn-round btn-sm btn-danger" data-dismiss="modal"><span
                                    class="fa fa-close"></span> Закрыть
                        </button>
                        <a href="{{route('notifications.unread')}}" class="btn btn-round btn-sm btn-primary "><span
                                    class="fa fa-comments"></span> Посмотреть все </a>

                    </div>

                    <script>
                        function markAsRead(id) {
                            $.post('notifications/mark-as-read/unread', {
                                single: true,
                                _token: '{{csrf_token()}}',
                                data: {
                                    id: {{$lastNotification->id}}
                                }
                            })
                            $('#login-dialog').modal('hide')

                        }
                    </script>

                </div>

            </div>

        </div>
    </div>


@endif
