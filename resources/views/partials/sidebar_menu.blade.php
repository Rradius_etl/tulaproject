<ul class="{{ ( isset($second_line) ) ? 'nav navbar-nav second-line' : 'sidebar-menu' }} ">


    @if(\App\Models\InterfaceElement::isVisible('tab_create_request'))
        <li class="{{Request::is('consultant-telemed-consult-requests*') ? 'active' : ''}}">
            <a href="/consultant-telemed-consult-requests">
                <i class="fa fa-file-text-o"></i><span>Подать Заявку</span>
            </a>
        </li>
    @endif

    @if(\App\Models\InterfaceElement::isVisible('tab_calendar'))
            <li class="{{Request::is('calendar') ? 'active' : ''}}">
                <a href="/calendar">
                    <i class="fa fa-calendar"></i>
                    <span>Календарь</span>
                </a>
            </li>
    @endif

    @if(\App\Models\InterfaceElement::isVisible('tab_statistics'))
            <li class="{{Request::is('reports*') ? 'active' : ''}}">
                <a href="/reports">
                    <i class="fa fa-bar-chart"></i>
                    <span>Статистика</span>
                </a>
            </li>
    @endif

    @if(\App\Models\InterfaceElement::isVisible('tab_polls'))
            <li class="{{Request::is('polls*') ? 'active' : ''}}"><a href="/polls/my?orderBy=created_at&sortedBy=desc">
                    <i
                            class="fa fa-question-circle-o"></i>
                    <span>Опрос</span>
                </a>
            </li>
    @endif

    @if(\App\Models\InterfaceElement::isVisible('tab_notifications'))
            <li class="{{Request::is('notifications*') ? 'active' : ''}}">
                <a href="/notifications/">
                    <i class="fa fa-bell-o"></i>
                    <span>Уведомления </span>
                    <sup
                            class="badge">{{NotificationManager::getUnreadNotificationsCount()}}</sup></a></li>
    @endif

    @if(\App\Models\InterfaceElement::isVisible('tab_materials'))
            <li class="{{Request::is('files*') ? 'active' : ''}}">
                <a href="/files/uploaded">
                    <i class="fa fa-list-alt"></i>
                    <span>Материалы</span>
                </a>
            </li>
    @endif







</ul>
