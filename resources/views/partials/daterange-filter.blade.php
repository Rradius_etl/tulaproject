<link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<script src="/plugins/daterangepicker/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    var dateRangeDefaults = {
        timePicker: false,
        startDate: moment( '{{ Input::get('from', '2016-01-01') }}'),
        endDate: moment( '{{ Input::get('to', date('Y-m-d')) }}'),
        //autoApply: true,
        locale: {
            format: 'DD-MM-YYYY',
            "separator": " - ",
            "applyLabel": "Применить",
            "cancelLabel": "Очистить",
            "fromLabel": "От",
            "toLabel": "До",
            "customRangeLabel": "Выбрать диапазон",
            "weekLabel": "W",
            "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            "firstDay": 1
        },
        ranges: {
            'За сегодня': [moment(), moment()],
            'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
            'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
            'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            'Последний месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };

    $('input[name=daterange]').daterangepicker(dateRangeDefaults);

    $('input[name=daterange]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));

        var url1 = document.location.search;
        var url2 = addUrlParam(url1, 'from', picker.startDate.format('YYYY-MM-DD'));
        var url3 = addUrlParam(url2, 'to', picker.endDate.format('YYYY-MM-DD'));

        console.log(url1, url2,url3)
        document.location.href =  document.location.pathname +  url3;

    })

    $('input[name=daterange]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        document.location.href =  document.location.pathname;
    });
</script>