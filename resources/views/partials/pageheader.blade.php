<!-- Content Header (Page header) -->
<div class="page-header">
    @yield('breadcrumbs', Breadcrumbs::renderIfExists())
    <h1>
        @yield('contentheader_title', 'Page Header here')
        <small>@yield('contentheader_description')</small>
    </h1>
</div>