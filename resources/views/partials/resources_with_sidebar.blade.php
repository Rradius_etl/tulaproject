@if(\App\Models\InterfaceElement::isVisible('sidebar'))
    <!-- .main news column -->
    <sidebar class="col-md-3 col-md-offset-9 col-sm-12">

        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @include('partials.sidebar_menu')
            </div>
            <!-- /.box-body -->

        </div>


    </sidebar>
@endif