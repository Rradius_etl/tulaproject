<!-- Modal -->
<div id="modal-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@yield('modal_dialog_title')</h4>
            </div>
            <div class="modal-body">
                @yield('modal_dialog_content')
            </div>

        </div>

    </div>
</div>