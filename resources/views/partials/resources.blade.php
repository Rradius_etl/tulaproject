<!-- .main news column -->
<sidebar class="col-md-3 col-sm-12">

    <?php $externalResources = \App\Models\Admin\ExternalResource::all() ?>
    @if(count($externalResources) > 0 && \App\Models\InterfaceElement::isVisible('external_resources') )
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Сторонние ресурсы</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="news-list news-list-in-box">
                    @foreach($externalResources as $item)
                        <li class="item">
                            <div class="news-img img-sm">
                                <img src="{{$item->logo('logo')}}" alt="">
                            </div>
                            <div class="news-title">
                                <h2>
                                    <small><a href="{{$item->link}}">{{$item->name}}</a></small>
                                </h2>
                            </div>

                        </li>
                    @endforeach

                </ul>
            </div>
            <!-- /.box-body -->

        </div>
    @endif

    @if(Sentinel::check() && \App\Models\InterfaceElement::isVisible('top_download_files') )
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Самые скачивамые файлы</h4>
            </div>
        <?php $top_files = Sentinel::getUser()->getTopFiles(); ?>

        <!-- /.box-header -->
            <div class="box-body">
                <ul class="news-list news-list-in-box">
                    @if( count($top_files) > 0)
                        @foreach($top_files as $key => $file)
                            <li class="item">
                                <div class="news-title">
                                    <h2>
                                        <small><a target="_blank" href="/getfile/{{$file->id}}"> {{$key+1}}
                                                . {{$file->original_name}}</a> <span
                                                    class="pull-right badge">{{$file->downloads_count}}</span></small>
                                    </h2>
                                </div>
                            </li>
                        @endforeach
                    @else
                        <p class="text-center">Отсутствует</p>
                    @endif

                </ul>
            </div>
            <!-- /.box-body -->

        </div>
    @endif

    <div class="box box-white">

        <div class="box-body">
            <div class="news-img">
                <img src="/images/video-archive.png" alt="АРХИВ ВИДЕО">
            </div>
            <div class="news-title text-center">
                <h2>
                    <small><a href="http://172.30.32.101/">АРХИВ ВИДЕО </a></small>
                </h2>
            </div>

        </div>
        <!-- /.box-body -->

    </div>


</sidebar>