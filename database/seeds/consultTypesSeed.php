<?php

use Illuminate\Database\Seeder;

class consultTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if( Schema::hasTable('consult_types') ) {

            DB::table('consult_types')->insert([
                array(
                    'name' => 'экстренная',
                    'days' => 0,
                    'hours' => 0,
                    'minutes' => 20,
                    'created_at'  =>  \Carbon\Carbon::now(),
                    'updated_at'  =>  \Carbon\Carbon::now()
                ),
                array(
                    'name' => 'срочная',
                    'days' => 0,
                    'hours' => 2,
                    'minutes' => 0,
                    'created_at'  =>  \Carbon\Carbon::now(),
                    'updated_at'  =>  \Carbon\Carbon::now()
                ),
                array(
                    'name' => 'плановая',
                    'days' => 1,
                    'hours' => 0,
                    'minutes' => 0,
                    'created_at'  =>  \Carbon\Carbon::now(),
                    'updated_at'  =>  \Carbon\Carbon::now()
                )
            ]);

        }

    }
}
