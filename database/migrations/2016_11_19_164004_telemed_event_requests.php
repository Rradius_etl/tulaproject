<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedEventRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_event_requests', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('title', 255);
            $table->enum('importance', ['low', 'medium', 'high'])->nullable();
            $table->timestamp('decision_at')->nullable();
            $table->enum('decision',['pending','agreed','rejected'])->default('pending');
            $table->enum('event_type', ['consultation','meeting', 'seminar'])->nullable();
            $table->integer('initiator_id')->unsigned();
            $table->integer('telemed_center_id')->unsigned()->nullable();
            $table->boolean('canceled');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('telemed_center_id')
                ->references('id')->on('telemed_centers')
                ->onDelete('cascade');


            $table->foreign('initiator_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_event_requests');
    }
}
