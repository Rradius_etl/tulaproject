<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Doctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string("surname");
            $table->string("middlename");
            $table->string("spec");
            $table->integer("telemed_center_id")->unsigned();
            $table->foreign('telemed_center_id')
                ->references('id')
                ->on('telemed_centers')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
