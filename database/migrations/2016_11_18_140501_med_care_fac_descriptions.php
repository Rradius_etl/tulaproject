<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedCareFacDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('med_care_fac_descriptions', function (Blueprint $table) {
            $table->integer('med_care_fac_id')->unsigned();
            $table->string('key',500);
            $table->string('value', 500);
            $table->primary("med_care_fac_id");
            $table->foreign('med_care_fac_id')->references('id')->on('med_care_facs');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('med_care_fac_descriptions');
    }
}
