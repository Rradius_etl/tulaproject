<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedicalProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_profiles', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->string('name');

            $table->foreign('category_id')
                ->references('id')
                ->on('medical_profile_categories')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_profiles');
    }
}
