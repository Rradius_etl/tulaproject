<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollUserPivots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_user_pivots', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('poll_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('poll_row_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('poll_row_id')
                ->references('id')->on('poll_rows')
                ->onDelete('cascade');
            $table->foreign('poll_id')
                ->references('id')->on('polls')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poll_user_pivots');
    }
}
