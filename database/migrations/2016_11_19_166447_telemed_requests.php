<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_consult_requests', function (Blueprint $table){
            $table->integer('request_id')->unsigned();
            $table->string('comment', 1500)->comment();
            $table->integer('uid_code');
            $table->integer('uid_year');
            $table->integer('uid_id');
            $table->enum('decision', ['pending', 'agreed', 'rejected', 'completed'])->default('pending');
            $table->boolean('completed');
            $table->integer("med_care_fac_id")->unsigned()->nullable();
            $table->integer('telemed_center_id')->unsigned()->nullable();
            $table->timestamp('decision_at');
            $table->timestamp('tmc_at');
            $table->unique(['uid_code','uid_year','uid_id']);
            $table->boolean("mcf_notificated");
            $table->boolean("telemed_notificated");
            $table->timestamps();
            $table->primary("request_id");
            $table->foreign('med_care_fac_id')
                ->references('id')->on('med_care_facs')
                ->onDelete('cascade');

            $table->foreign('telemed_center_id')
                ->references('id')->on('telemed_centers')
                ->onDelete('cascade');

            $table->foreign('request_id')
                ->references('id')->on('telemed_event_requests')
                ->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_consult_requests');
    }
}
