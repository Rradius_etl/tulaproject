<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_resources', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('link');
            $table->smallInteger('order');
            $table->string('logo_path', 500);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('external_resources');
    }
}
