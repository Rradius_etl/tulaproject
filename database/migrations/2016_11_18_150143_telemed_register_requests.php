<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedRegisterRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_register_requests', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('med_care_fac_id')->unsigned();
            $table->string('name', 500);
            $table->string('director_fullname', 255);
            $table->string('coordinator_fullname', 255);
            $table->string('coordinator_phone', 50);
            $table->string('tech_specialist_fullname', 255);
            $table->string('tech_specialist_phone', 50);
            $table->string('tech_specialist_contacts', 500);
            $table->enum('decision', ['pending', 'agreed', 'rejected'])->default('pending');
            $table->boolean("completed");
            $table->string('videoconf_equipment', 1500);
            $table->boolean('digit_img_demonstration');
            $table->string('equipment_location', 500);
            $table->foreign('med_care_fac_id')->references('id')->on('med_care_facs');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_register_requests');
    }
}
