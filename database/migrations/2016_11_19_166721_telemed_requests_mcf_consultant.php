<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedRequestsMcfConsultant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_requests_mcf_consultant', function (Blueprint $table){
            $table->integer('request_id')->unsigned(); //fk
            
            $table->string('responsible_person_fullname');
            $table->integer('appointed_person_id')->unsigned()->nullable();
            $table->timestamp('planned_date');
            $table->string('coordinator_fullname');
            $table->integer('doctor_id')->unsigned()->nullable();
            $table->timestamps();
            $table->primary('request_id');

            $table->foreign('request_id')
                ->references('id')->on('telemed_event_requests')
                ->onDelete('cascade');
            $table->foreign('doctor_id')
                ->references('id')->on('doctors')
                ->onDelete('cascade');
            $table->foreign('appointed_person_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_requests_mcf_consultant');
    }
}
