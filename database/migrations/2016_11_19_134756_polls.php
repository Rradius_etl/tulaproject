<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Polls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('author_id')->unsigned(); //fk
            $table->integer('telemed_center_id')->unsigned();
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('author_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('telemed_center_id')
                ->references('id')->on('telemed_centers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('polls');
    }
}
