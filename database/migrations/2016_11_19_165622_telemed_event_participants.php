<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedEventParticipants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_event_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('telemed_center_id')->unsigned();
            $table->integer('request_id')->unsigned();
            $table->enum("",['pending','rejected','agreed'])->default("pending");
            $table->boolean("completed");
            $table->timestamps();


            $table->foreign('telemed_center_id')
                ->references('id')->on('telemed_centers')
                ->onDelete('cascade');

            $table->foreign('request_id')
                ->references('id')->on('telemed_event_requests')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_event_participants');
    }
}
