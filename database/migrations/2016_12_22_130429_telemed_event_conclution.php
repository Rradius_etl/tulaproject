<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedEventConclution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_event_conclusion', function (Blueprint $table) {
            $table->integer("request_id")->unsigned();
            $table->integer("minutes")->unsigned()->nullable();
            $table->integer("seconds")->unsigned()->nullable();
            $table->integer("calls_count")->unsigned()->nullable();
            $table->string("comment",1500)->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->primary('request_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('request_id')->references('id')->on('telemed_event_requests')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_event_conclusion');

    }
}
