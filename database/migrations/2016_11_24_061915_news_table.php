<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug', 500);
            $table->string('img_path',500);
            $table->string('short_description', 800);
            $table->longText('content');
            $table->integer('created_by')->unsigned();
            $table->integer('modified_by')->unsigned();
            $table->boolean('published');
            $table->integer('hits')->unsigned();
            $table->string('source',500);
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
