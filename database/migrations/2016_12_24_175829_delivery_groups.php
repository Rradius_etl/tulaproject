<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeliveryGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('telemed_center_ids');
            $table->integer("telemed_center_id")->unsigned();
            $table->foreign('telemed_center_id')
                ->references('id')
                ->on('telemed_centers')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_groups');
    }
}
