<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelemedUserPivots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemed_user_pivots', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('telemed_center_id')->unsigned();
            $table->string('value', 500);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('telemed_center_id')->references('id')->on('telemed_centers');
            $table->unique(['user_id','telemed_center_id']);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telemed_user_pivots');
    }
}
