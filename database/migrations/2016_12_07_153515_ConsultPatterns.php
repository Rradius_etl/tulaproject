<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConsultPatterns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_patterns', function(Blueprint $table){
            $table->increments('id');
            $table->string('name',500);
            $table->integer('telemed_center_id')->unsigned(); // fk
            $table->integer('medical_profile')->unsigned()->nullable();
            $table->string('doctor_fullname');
            $table->string('doctor_spec');
            $table->enum('consult_type', ['planned', 'express', 'emergency'])->nullable();
            //$table->enum('consult_type', ['consultation', 'meeting', 'seminar'])->nullable();
            $table->integer('med_care_fac_id')->unsigned()->nullable(); //fk
            $table->string('desired_consultant');
            $table->time('desired_time');
            $table->string('patient_uid_or_fname');
            $table->enum('patient_gender', ['male', 'female'])->default('male');
            $table->string('patient_address')->nullable();
            $table->integer('patient_social_status')->unsigned()->nullable();
            $table->enum('patient_indications_for_use',['diagnose_and_treatment','patient_consultation','hospital_possibility','meddata_equipment_decode','other'])->default('diagnose_and_treatment');
            $table->string("patient_indications_for_use_other",500)->nullable();
            $table->string('patient_questions',3000);
            $table->timestamps();

            $table->foreign('patient_social_status')
                ->references('id')->on('social_statuses');

            $table->foreign('medical_profile')
                ->references('id')->on('medical_profiles');

            $table->foreign('telemed_center_id')
                ->references('id')->on('telemed_centers')
                ->onDelete('cascade');

            $table->foreign('med_care_fac_id')
                ->references('id')->on('med_care_facs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consult_patterns');
    }
}
