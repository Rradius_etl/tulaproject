<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInterfaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interface_elements', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->string('name');
            $table->string('description');
            $table->longText('disabled_users');
            $table->longText('disabled_roles');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interface_elements');
    }
}
