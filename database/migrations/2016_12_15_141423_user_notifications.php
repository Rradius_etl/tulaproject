<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("message",3000);
            $table->integer("telemed_center_id")->unsigned()->nullable();
            $table->boolean("need_callback");
            $table->enum('type',['poll'])->nullable();
            $table->timestamps();
            $table->foreign('telemed_center_id')->references('id')->on('telemed_centers')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('notifications');

    }
}