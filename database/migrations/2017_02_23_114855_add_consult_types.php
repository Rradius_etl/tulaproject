<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsultTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->tinyInteger('days')->default(0);
            $table->tinyInteger('hours')->default(0);
            $table->tinyInteger('minutes')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });

        Artisan::call('db:seed', [
            '--class' => 'consultTypesSeed',
            '--force' => true
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consult_types');
    }
}
