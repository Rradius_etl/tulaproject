<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MedCareFacs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('med_care_facs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 500);
            $table->smallInteger('code');
            $table->integer("user_id")->unsigned()->nullable()->unique();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer("consultations_norm")->unsigned();
            $table->engine = 'InnoDB';
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('med_care_facs');
    }
}
