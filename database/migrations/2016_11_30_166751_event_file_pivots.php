<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventFilePivots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_file_pivots', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->integer('request_id')->unsigned();
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('request_id')->references('id')->on('telemed_event_requests')->onDelete('cascade');
            $table->enum('type',['conclusion'])->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_file_pivots');
    }
}
