<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollRows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_rows', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('order');
            $table->integer('poll_id')->unsigned();  // fk
            $table->integer('votes');
            $table->timestamps();
            $table->foreign('poll_id')
                ->references('id')->on('polls')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poll_rows');
    }
}
