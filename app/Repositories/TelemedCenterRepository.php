<?php

namespace App\Repositories;

use App\Models\Admin\TelemedCenter;
use InfyOm\Generator\Common\BaseRepository;

class TelemedCenterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' =>"like",
        'director_fullname'=>"like",
        'coordinator_fullname'=>"like",
        'coordinator_phone'=>"like",
        'tech_specialist_fullname'=>"like",
        'tech_specialist_phone'=>"like",
        'tech_specialist_contacts'=>'like',
        'address'=>'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TelemedCenter::class;
    }
}
