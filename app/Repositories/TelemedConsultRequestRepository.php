<?php

namespace App\Repositories;

use App\Models\TelemedConsultAbonentSide;
use App\Models\TelemedConsultRequest;
use InfyOm\Generator\Common\BaseRepository;

class TelemedConsultRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [


        
    ];


    /**
     * Configure the Model
     **/
    public function model()
    {
        return \App\Models\TelemedConsultRequest::class;
    }
}
