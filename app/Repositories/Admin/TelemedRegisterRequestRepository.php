<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TelemedRegisterRequest;
use InfyOm\Generator\Common\BaseRepository;

class TelemedRegisterRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like',
        'director_fullname'=>'like',
        'coordinator_fullname'=>'like',
        'coordinator_phone'=>'like',
        'tech_specialist_fullname'=>'like',
        'tech_specialist_phone'=>'like',
        'tech_specialist_contacts'=>'like',
        'decision'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        
        return TelemedRegisterRequest::class;
    }
}
