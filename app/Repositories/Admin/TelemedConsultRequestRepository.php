<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TelemedConsultRequest;
use InfyOm\Generator\Common\BaseRepository;

class TelemedConsultRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'comment',
        'uid_code',
        'uid_year',
        'uid_id',
        'decision',
        'completed',
        'med_care_fac_id',
        'telemed_center_id',
        'decision_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TelemedConsultRequest::class;
    }
}
