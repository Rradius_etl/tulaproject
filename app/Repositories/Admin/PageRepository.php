<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Page;
use InfyOm\Generator\Common\BaseRepository;

class PageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug',
        'img_path',
        'short_description',
        'content',
        'created_by',
        'modified_by',
        'published',
        'in_menu',
        'hits',
        'meta_description',
        'meta_keywords'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Page::class;
    }
}
