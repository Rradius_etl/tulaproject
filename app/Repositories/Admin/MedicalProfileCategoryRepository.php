<?php

namespace App\Repositories\Admin;

use App\Models\Admin\MedicalProfileCategory;
use InfyOm\Generator\Common\BaseRepository;

class MedicalProfileCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicalProfileCategory::class;
    }
}
