<?php

namespace App\Repositories\Admin;

use App\Models\Admin\News;
use InfyOm\Generator\Common\BaseRepository;

class NewsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug',
        'img_path',
        'short_description',
        'content',
        'created_by',
        'modified_by',
        'published',
        'hits',
        'source',
        'meta_description',
        'meta_keywords'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return News::class;
    }
}
