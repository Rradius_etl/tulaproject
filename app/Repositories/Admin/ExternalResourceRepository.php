<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ExternalResource;
use InfyOm\Generator\Common\BaseRepository;

class ExternalResourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'link',
        'order',
        'logo_path'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExternalResource::class;
    }
}
