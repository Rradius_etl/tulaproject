<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Poll;
use InfyOm\Generator\Common\BaseRepository;

class PollRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'author_id',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Poll::class;
    }
}
