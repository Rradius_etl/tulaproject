<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TelemedCenter;
use InfyOm\Generator\Common\BaseRepository;

class TelemedCenterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like',
        'director_fullname'=>'like',
        'coordinator_fullname'=>'like',
        'coordinator_phone'=>'like',
        'tech_specialist_fullname'=>'like',
        'tech_specialist_phone'=>'like',
        'tech_specialist_contacts'=>'like',
        'terminal_name' => 'like',
        'terminal_number'=>'like'
    ];
    public function withTrashed() {
        $this->model = $this->model->withTrashed();
        return $this;
    }
    public function onlyTrashed() {
        $this->model = $this->model->onlyTrashed();
        return $this;
    }

    public function restore()
    {
        $this->model->restore();
    }
    /**
     * Configure the Model
     **/
    public function model()
    {
        return TelemedCenter::class;
    }
}
