<?php

namespace App\Repositories\Admin;

use App\Models\Admin\MedicalProfile;
use InfyOm\Generator\Common\BaseRepository;

class MedicalProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicalProfile::class;
    }
}
