<?php

namespace App\Repositories\Admin;

use App\Models\Admin\IndicationForUse;
use InfyOm\Generator\Common\BaseRepository;

class IndicationForUseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IndicationForUse::class;
    }
}
