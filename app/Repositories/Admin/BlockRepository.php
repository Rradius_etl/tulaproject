<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Block;
use InfyOm\Generator\Common\BaseRepository;

class BlockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'block_content',
        'order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Block::class;
    }
}
