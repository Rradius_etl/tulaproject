<?php

namespace App\Repositories\Admin;

use App\Models\Admin\File;
use App\Models\FileUpload;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;

class FileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'original_name',
        'file_path',
        'extension',
        'downloads_count',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return File::class;
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->find($id);
        $originalModel = clone $model;

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        FileUpload::deletePublicFile($id);

        $deleted = $model->delete();

        event(new RepositoryEntityDeleted($this, $originalModel));

        return $deleted;
    }

}
