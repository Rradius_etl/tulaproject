<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TelemedRequest;
use InfyOm\Generator\Common\BaseRepository;

class TelemedRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'importance',
        'decision_at',
        'event_type',
        'attached_file_id',
        'initiator_id',
        'start_date',
        'end_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TelemedRequest::class;
    }
}
