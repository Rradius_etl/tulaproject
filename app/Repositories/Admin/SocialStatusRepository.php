<?php

namespace App\Repositories\Admin;

use App\Models\Admin\SocialStatus;
use InfyOm\Generator\Common\BaseRepository;

class SocialStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SocialStatus::class;
    }
}
