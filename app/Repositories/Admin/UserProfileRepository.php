<?php

namespace App\Repositories\Admin;

use App\Models\Admin\UserProfile;
use InfyOm\Generator\Common\BaseRepository;

class UserProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date_of_birth',
        'name',
        'surname',
        'middlename',
        'phone',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserProfile::class;
    }
}
