<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Slide;
use InfyOm\Generator\Common\BaseRepository;

class SlideRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'img_path',
        'order',
        'content',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slide::class;
    }
}
