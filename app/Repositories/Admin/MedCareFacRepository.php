<?php

namespace App\Repositories\Admin;

use App\Models\Admin\MedCareFac;
use InfyOm\Generator\Common\BaseRepository;

class MedCareFacRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>"like",
        'code'=>"like"
    ];

    public function withTrashed() {
        $this->model = $this->model->withTrashed();
        return $this;
    }
    public function onlyTrashed() {
        $this->model = $this->model->onlyTrashed();
        return $this;
    }

    public function restore()
    {
         $this->model->restore();

    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedCareFac::class;
    }
}
