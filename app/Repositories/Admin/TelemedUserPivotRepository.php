<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TelemedUserPivot;
use InfyOm\Generator\Common\BaseRepository;

class TelemedUserPivotRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user.email'=>'like',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TelemedUserPivot::class;
    }
}
