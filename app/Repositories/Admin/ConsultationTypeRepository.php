<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ConsultationType;
use InfyOm\Generator\Common\BaseRepository;

class ConsultationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'days',
        'hours',
        'minutes'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ConsultationType::class;
    }
}
