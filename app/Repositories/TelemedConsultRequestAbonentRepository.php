<?php

namespace App\Repositories;

use App\Models\TelemedConsultAbonentSide;
use App\Models\TelemedConsultRequest;
use InfyOm\Generator\Common\BaseRepository;

class TelemedConsultRequestAbonentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
/*        'request_id',
        'comment',
        'uid_code',
        'uid_year',
        'uid_id',
        'decision',
        'telemed_center_id',
        'decision_at'*/
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return \App\Models\TelemedConsultAbonentSide::class;
    }
}
