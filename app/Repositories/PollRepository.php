<?php

namespace App\Repositories;

use App\Models\Poll;
use InfyOm\Generator\Common\BaseRepository;

class PollRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title' => 'like',
    /*    'author_id',
        'active'*/
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Poll::class;
    }
}
