<?php

namespace App\Repositories;

use App\Models\Invitation;
use InfyOm\Generator\Common\BaseRepository;

class InvitationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'request_id',
        'telemed_center_id',
        'decision',
        'completed'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invitation::class;
    }
}
