<?php

namespace App\Repositories;

use App\Models\Doctor;
use InfyOm\Generator\Common\BaseRepository;

class DoctorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'surname',
        'middlename',
        'spec',
        'telemed_center_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doctor::class;
    }
}
