<?php

namespace App\Repositories;

use App\Models\ConsultPattern;
use InfyOm\Generator\Common\BaseRepository;

class ConsultPatternRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'telemed_center_id',
        'medical_profile',
        'doctor_fullname',
        'doctor_spec',
        'consult_type',
        'med_care_fac_id',
        'desired_consultant',
        'desired_date',
        'desired_time',
        'patient_uid_or_fname',
        'patient_birthday',
        'patient_gender',
        'patient_address',
        'patient_social_status',
        'patient_indications_for_use',
        'patient_indications_for_use_other',
        'patient_questions'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ConsultPattern::class;
    }
}
