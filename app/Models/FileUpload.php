<?php
namespace App\Models;

use App\Models\Admin\EventFilePivot;
use DB;
use Storage;
use App\Models\Admin\File as UploadFile;
use File;

/**
 * @SWG\Definition(
 *      definition="File",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="original_name",
 *          description="original_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="file_path",
 *          description="file_path",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extension",
 *          description="extension",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="downloads_count",
 *          description="downloads_count",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class FileUpload
{
    public function __construct()
    {

    }

    public static function saveAvatarFile($file)
    {
        $id = self::saveRequestPublicFile($file, \Sentinel::getUser()->id, "avatars/");
        return $id;
    }

    public static function saveMessageFiles($message_id, $files, $user_id, $locationDirectory)
    {
        foreach ($files as $file) {
            $result = self::saveRequestFile($file, $user_id, $locationDirectory, "");
            if ($result != false) {
                $pivot = new MessageFilePivot();
                $pivot->message_id = $message_id;
                $pivot->file_id = $result;
                $pivot->save();
            }
        }
    }


    public static function removeMessagePivot($request_id, $file_id)
    {
        $file = EventFilePivot::where(['id' => $file_id, 'request_id' => $request_id])->first();
        if ($file != null) {
            $file->delete();
            self::deleteFile($file->file_id);

        }
    }

    public static function removePivot($request_id, $file_id)
    {
        $file = EventFilePivot::where(['id' => $file_id, 'request_id' => $request_id])->first();
        if ($file != null) {
            $file->delete();
            self::deleteFile($file->file_id);

        }
    }

    public static function deletePublicFile($id)
    {
        $file = UploadFile::find($id);
        if ($file != null) {
            Storage::disk('public')->delete($file->file_path);
            $file->delete();
        }
    }


    protected static function deleteFile($id)
    {
        $file = UploadFile::find($id);
        if ($file != null) {
            Storage::disk('storage')->delete($file->file_path);
            $file->delete();
        }
    }

    public static function saveFiles($request_id, $files, $user_id, $locationDirectory)
    {
        foreach ($files as $file) {
            $result = self::saveRequestFile($file, $user_id, $locationDirectory, "");
            if ($result != false) {
                $pivot = new EventFilePivot();
                $pivot->request_id = $request_id;
                $pivot->file_id = $result;
                $pivot->save();
            }
        }
    }

    public static function saveConclusionFiles($request_id, $files, $user_id, $locationDirectory)
    {
        foreach ($files as $file) {
            $result = self::saveRequestFile($file, $user_id, $locationDirectory, "");
            if ($result != false) {
                $pivot = new EventFilePivot();
                $pivot->request_id = $request_id;
                $pivot->file_id = $result;
                $pivot->type = 'conclusion';
                $pivot->save();
            }
        }
    }

    /** Запись файлов ТМК  */
    public static function saveAbonentSideFile($request_id, $files, $user_id, $locationDirectory)
    {
        foreach ($files as $file) {
            $result = self::saveRequestFile($file, $user_id, $locationDirectory, "");
            if ($result != false) {
                $pivot = new EventFilePivot();
                $pivot->request_id = $request_id;
                $pivot->file_id = $result;
                $pivot->type = 'abonent_side';
                $pivot->save();
            }
        }
    }
    /**
     *
     * @param $request \App\Http\Requests\Admin\CreateFileRequest
     * @param $user_id integer
     * @param $locationDirectory string
    */
    public static function upload($request, $user_id, $locationDirectory)
    {
        $input = $request->all();
        $file= $request->file('file');
        $comment = $input['comment'];


        return self::saveRequestPublicFile($file, $user_id, $locationDirectory, $comment);


    }

    protected static function saveRequestFile($file, $user_id, $locationDirectory, $comment = "")
    {
        $mainImageFileName = "";
        try {
            DB::beginTransaction();
            $newfile = new UploadFile();
            $newfile->original_name = $file->getClientOriginalName();
            $newfile->extension = $file->getClientOriginalExtension();
            if( strtolower($file->getClientOriginalExtension()) == "php")
            {
                return false;
            }
            $newfile->downloads_count = 0;
            $newfile->user_id = $user_id;
            $newfile->comment = $comment;
            if ($comment == "") {
                $newfile->comment = null;
            }
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getClientOriginalName();
            $unique_name = str_slug(str_replace($extension, '', $filename), '-');
            $mainImageFileName = $locationDirectory . $unique_name . '_' . uniqid() . '.' . $extension;
            $newfile->file_path = $mainImageFileName;
            if (Storage::disk('storage')->put($mainImageFileName, File::get($file))) {
                \Log::info('Файл ' . $newfile->original_name . ' записан как ' . $newfile->file_path);
                $newfile->save();
                DB::commit();
                return $newfile->id;
            } else {
                \Log::warning('Файл ' . $newfile->original_name . ' не был записан');
                DB::rollBack();
                return false;
            }
        } catch (\Exception $e) {

            Storage::disk('storage')->delete($mainImageFileName);
            DB::rollback();
            return false;
        }
    }

    protected static function saveRequestPublicFile($file, $user_id, $locationDirectory, $comment = "")
    {
        $mainImageFileName = "";
        try {
            DB::beginTransaction();
            $newfile = new UploadFile();
            $newfile->original_name = $file->getClientOriginalName();
            $newfile->extension = $file->getClientOriginalExtension();
            if( strtolower($file->getClientOriginalExtension()) == "php")
            {
                return false;
            }
            $newfile->downloads_count = 0;
            $newfile->user_id = $user_id;
            $newfile->is_public = true;
            if ($comment == "") {
                $newfile->comment = null;
            }
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getClientOriginalName();
            $unique_name = str_slug(str_replace($extension, '', $filename), '-');
            $mainImageFileName = $locationDirectory . $unique_name . '_' . uniqid() . '.' . $extension;
            $newfile->file_path = $mainImageFileName;
            if (Storage::disk('public')->put($mainImageFileName, File::get($file))) {
                $newfile->save();
                DB::commit();
                return $newfile->id;
            } else {
                DB::rollBack();
                return false;
            }
        } catch (\Exception $e) {

            Storage::disk('storage')->delete($mainImageFileName);

            DB::rollback();
            return false;
        }
    }

}
