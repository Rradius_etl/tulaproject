<?php

namespace App\Models;

use App\Models\Admin\IndicationForUse;
use App\Models\Admin\TelemedRequest;
use Eloquent as Model;
use App\Models\Admin\SocialStatus;
use Jenssegers\Date\Date;

/**
 * @SWG\Definition(
 *      definition="TelemedConsultAbonentSide",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="telemed_id",
 *          description="telemed_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_fullname",
 *          description="doctor_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="doctor_spec",
 *          description="doctor_spec",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="consult_type",
 *          description="consult_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="medical_care_fac_id",
 *          description="medical_care_fac_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="desired_consultant",
 *          description="desired_consultant",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="desired_date",
 *          description="desired_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="patient_uid_or_fname",
 *          description="patient_uid_or_fname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_birthday",
 *          description="patient_birthday",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="patient_gender",
 *          description="patient_gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_address",
 *          description="patient_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_social_status",
 *          description="patient_social_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_indications_for_use",
 *          description="patient_indications_for_use",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_questions",
 *          description="patient_questions",
 *          type="string"
 *      )
 * )
 */
class TelemedConsultAbonentSide extends Model
{

    public $table = 'telemed_requests_mcf_abonent';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'telemed_id',
        'request_id',
        'doctor_fullname',
        'doctor_spec',
        'consult_type',
        'medical_profile',
        'medical_care_fac_id',
        'desired_consultant',
        'desired_date',
        'desired_time',
        'patient_uid_or_fname',
        'patient_birthday',
        'patient_gender',
        'patient_address',
        'patient_social_status',
       // 'patient_indications_for_use',
        'patient_indications_for_use_id',
        'patient_indications_for_use_other',
        'patient_questions',
        'med_care_fac_id'
    ];
    protected $primaryKey = "request_id";
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'integer',
        'telemed_id' => 'integer',
        'doctor_fullname' => 'string',
        'medical_profile' => 'integer',
        'doctor_spec' => 'string',
        'consult_type' => 'integer',
        'med_care_fac_id' => 'integer',
        'desired_consultant' => 'string',
        'desired_date' => 'date nullable',
        'patient_uid_or_fname' => 'string',
        'patient_birthday' => 'date',
        'patient_gender' => 'string',
        'patient_address' => 'string',
        'patient_social_status' => 'integer',
        //'patient_indications_for_use' => 'string',
        'patient_indications_for_use_id' => 'integer',
        'patient_questions' => 'string'
    ];

    public function getPatientIndicationsForUseIdAttribute($value)
    {
        if ( $value == null || $value == 0 ) {
            return 'other';
        } else {
            return $value;
        }
    }
    public function setPatientIndicationsForUseIdAttribute($value)
    {
        if ( $value == 'other' ) {
            $this->attributes['patient_indications_for_use_id'] = 0;
        } else {
            $this->attributes['patient_indications_for_use_id'] = $value;
        }
    }
    public function setDesiredDateAttribute($value)
    {
        if( $value == "" || $value == null) {
            $this->attributes['desired_date'] = null;
        } else {
            $this->attributes['desired_date'] = $value;
        }
    }
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

    public function medCareFac()
    {
        return $this->belongsTo(\App\Models\Admin\MedCareFac::class);
    }

    public function request()
    {
        return $this->belongsTo(TelemedRequest::class);
    }

    public function consultRequest(){

        return $this->belongsTo(TelemedConsultRequest::class,"request_id","request_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedEventRequest()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedEventRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }

    public function TelemedConsultConsultantSide()
    {
        return $this->belongsTo(\App\Models\TelemedConsultConsultantSide::class,"request_id","request_id");
    }

    public function socialStatus()
    {
        return $this->belongsTo(SocialStatus::class,"patient_social_status","id");
    }

    public function medicalProfile()
    {
        return $this->belongsTo(MedicalProfile::class, 'medical_profile', 'id');
    }

    public function getIndForUse()
    {
        $ind_for_use = IndicationForUse::find($this->patient_indications_for_use_id);
        if( $ind_for_use != null)
            return $ind_for_use->getAttribute('name');

        $ind_for_use = [
            "" => "----------------",
            "diagnose_and_treatment" => "Уточнение диагноза и лечения",
            "patient_consultation" => "Консультация по ведению больного",
            "hospital_possibility" => "Возможность госпитализации",
            "meddata_equipment_decode" => "Расшифровка данных медицинского оборудования",
            "other" => "Иное"
        ];

        return $ind_for_use[$this->patient_indications_for_use];
    }

    public function getDesiredDate($only_value = false)
    {
        if( is_null($this->desired_date)) {
            return  $only_value ? null : "<span class='label label-info'>не указан</span>";
        } else {
            return Date::parse($this->desired_date . ' ' . $this->desired_time)->toDateTimeString();
        }
    }
    /** Get Consultation type relation
     */
    public function consultationType()
    {
        return $this->belongsTo(\App\Models\Admin\ConsultationType::class, 'consult_type', 'id');
    }
}
