<?php

namespace App\Models;

use Eloquent as Model;

class ConsultPattern extends Model
{

    public $table = 'consult_patterns';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'telemed_center_id',
        'medical_profile',
        'doctor_fullname',
        'doctor_spec',
        'consult_type',
        'med_care_fac_id',
        'desired_consultant',
        'desired_time',
        'patient_uid_or_fname',
        'patient_birthday',
        'patient_gender',
        'patient_address',
        'patient_social_status',
       // 'patient_indications_for_use',
        'patient_indications_for_use_id',
        'patient_indications_for_use_other',
        'patient_questions',
        'patient_birthday'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'telemed_center_id' => 'integer',
        'medical_profile' => 'integer',
        'doctor_fullname' => 'string',
        'doctor_spec' => 'string',
        'consult_type' => 'integer',
        'med_care_fac_id' => 'integer',
        'desired_consultant' => 'string',
        'patient_uid_or_fname' => 'string',
        'patient_gender' => 'string',
        'patient_address' => 'string',
        'patient_social_status' => 'integer',
        'patient_indications_for_use' => 'string',
        'patient_indications_for_use_id' => 'integer nullable',
        'patient_indications_for_use_other' => 'string',
        'patient_questions' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function medCareFac()
    {
        return $this->belongsTo(\App\Models\MedCareFac::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function medicalProfile()
    {
        return $this->belongsTo(\App\Models\MedicalProfile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function socialStatus()
    {
        return $this->belongsTo(\App\Models\SocialStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\TelemedCenter::class);
    }

    public function consultationType()
    {
        return $this->belongsTo(\App\Models\Admin\ConsultationType::class);
    }
}
