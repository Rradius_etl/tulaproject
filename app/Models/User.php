<?php

namespace App\Models;

use App\Models\Admin\File;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedUserPivot;
use App\Role;
use App\Models\Admin\MedCareFac;
use \Cartalyst\Sentinel\Users\EloquentUser;
use Cmgmyr\Messenger\Traits\Messagable;
use Config;

class User extends EloquentUser
{

    use Messagable;
    public $table = 'users';


    public $fillable = [

        'email',
        'password',
        'name',
        'surname',
        'middlename',
        'position',
        'experience',
        'phone_number',
        'avatar_file_id',
        'city'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'password' => 'string',

    ];

    /**
     * The attributes excluded from the model's  form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'last_login'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required,email',
        'password' => 'required',

    ];


    protected static function boot()
    {
        parent::boot();

        static::deleting(function($model) { // before delete() method call this
           TelemedUserPivot::where('user_id', $model->id)->delete();
             
        });
        	
        static::deleted(function ($model) { // before delete() method call this
            $model->FileAccessTokens()->delete();
           
        });
    }

    public function getIsAdminAttribute()
    {
        //if( $this->inRole('admin'))
            return true;
    }

    public function avatar($size = 'avatar')
    {
        $size = '/' . $size . '/';
        if ($this->avatar_file_id == null) {
            return '/images/no-avatar.jpg';
        }

        $file = $this->avatarfile;
        $path = dirname($file->file_path);
        $filename = str_replace($path, '', $file->file_path);

        return '/' . Config::get('imagecache.route') . $size . $filename;
    }

    public function tmcIds()
    {
        return $this->belongsToMany(TelemedCenter::class, 'telemed_user_pivots', 'user_id', 'telemed_center_id')->groupBy('id');
    }

    public function tmc()
    {
        return $this->hasMany(TelemedUserPivot::class,'id','user_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function tmcRoles()
    {
        return $this->hasMany(TelemedUserPivot::class)->whereExists(function ($q)
        {
            $q->select(\DB::raw('1'))->from('telemed_centers as tc')->where('tc.id',\DB::raw('telemed_user_pivots.telemed_center_id'))
            ->whereNull('tc.deleted_at');
        });
    }

    /*public function invitations()
    {
        return $this->belongsToMany(Invitation::class, 'telemed_user_pivots', 'user_id', 'telemed_center_id')->groupBy('id');
    }*/

    public function avatarfile()
    {
        return $this->hasOne(File::class, "id", "avatar_file_id");
    }


    public function medcarefacs()
    {
        return $this->hasOne(MedCareFac::class);
    }

    public function getfName()
    {
        return implode([$this->surname, $this->name, $this->middlename], ' ');
    }

    public function uploadedFiles()
    {
        return $this->hasMany(File::class);
    }

    public function user_notification_recepients()
    {
        $this->hasMany(UserNotificationRecepient::class, 'id', 'user_id');
    }

    public function getLastNotification()
    {
        $this->getUnreadNotifications()->orderBy("created_at", 'desc')->first();
    }

    public function getNotifications()
    {
        return $this->hasMany(UserNotificationRecepient::class)->with("notification");
    }

    public function getPollNotifications()
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->whereExists(function ($query) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type', 'poll')->where('n.id', \DB::raw('notification_recepients.notification_id'));

        });
    }

    public function getUnreadNotificationsWithSearch($search)
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->where("is_read", false)->whereExists(function ($query) use ($search) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.id', \DB::raw('notification_recepients.notification_id'))->where('n.message','like','%'.$search.'%');
        });

    }
    public function getNotificationsWithSearch($search)
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->whereExists(function ($query) use ($search) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.id', \DB::raw('notification_recepients.notification_id'))->where('n.message','like','%'.$search.'%');
        });

    }

    public function getUnreadPollNotificationsWithSearch($search)
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->where("is_read", false)->whereExists(function ($query) use ($search) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type', 'poll')->where('n.id', \DB::raw('notification_recepients.notification_id'))->where('n.message','like','%'.$search.'%');
        });

    }
    public function getPollNotificationsWithSearch($search)
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->whereExists(function ($query) use ($search) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type', 'poll')->where('n.id', \DB::raw('notification_recepients.notification_id'))->where('n.message','like','%'.$search.'%');
        });

    }
    public function getUnreadPollNotifications()
    {

        return $this->hasMany(UserNotificationRecepient::class)->with(["notification"])->where("is_read", false)->whereExists(function ($query) {
            $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type', 'poll')->where('n.id', \DB::raw('notification_recepients.notification_id'));
        });

    }


    public function getUnreadNotifications()
    {
        return $this->hasMany(UserNotificationRecepient::class)->with("notification")->where("is_read", false);
    }

    public function getTopFiles()
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        $files = File::leftJoin("message_file_pivots as mfp", "mfp.file_id", "=", "files.id")->leftJoin("messages as m", "mfp.message_id", "=", "m.id")->leftJoin("event_file_pivots as e", "e.file_id", "=", "files.id")
            ->where(function ($query) use ($tmcs) {
                $query->whereExists(function ($q) use ($tmcs) {
                    $q->select('*')->from("participants as part")->where(['part.thread_id' => \DB::raw("m.thread_id"), "part.user_id" => $this->id]);
                });
                $query->orWhere(function($q2) use($tmcs)
                {
                    $q2->whereExists(function ($q) use ($tmcs) {
                        $q->select('*')->from("telemed_event_participants as tep")->where(['e.request_id' => "tep.request_id"])->whereIn("tep.telemed_center_id", $tmcs);
                    });
                });
                $query->orWhere(['files.user_id' => $this->id]);
                $query->orWhere(function ($q1) use ($tmcs) {
                    $q1->whereExists(function ($q) use ($tmcs) {
                        $q->select('*')->from("telemed_consult_requests as tcp")->where(['tcp.request_id' => \DB::raw("e.request_id")])->whereIn("tcp.telemed_center_id", $tmcs);
                    });
                });
                $query->orWhere(function ($q) use ($tmcs) {
                    $q->whereExists(function ($q2) use ($tmcs) {
                        $q2->select('*')->from("telemed_event_requests as ter")->whereIn("ter.telemed_center_id", $tmcs)->where(['e.request_id' => \DB::raw("ter.id")]);
                    });
                });
            })->select("files.*")->orderBy('files.downloads_count','desc')->limit(5)->get();

       return $files;
    }


    public function FileAccessTokens()
    {
        return $this->hasMany(FileAccessToken::class, 'user_id', 'id');
    }
    /** Get attached user interface elements
     * */
    public function disabledInterfaceElements()
    {
        return $this->belongsToMany(InterfaceElement::class, 'user_interface_elements', 'user_id', 'interface_element_id')->withTimestamps('created_at', 'updated_at');
    }
    

}