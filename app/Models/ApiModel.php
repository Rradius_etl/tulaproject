<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 07.12.2016
 * Time: 15:56
 */

namespace App\Models;
use DB;
use Carbon\Carbon;

class ApiModel
{
    public static function getTMCEvents($tmc_id,$params)
    {
        $fromDate = Carbon::createFromDate($params['year'], $params['month'], 20, 'Europe/Moscow')->subWeek(3);
        $toDate = Carbon::createFromDate($params['year'], $params['month'], 20, 'Europe/Moscow')->addWeeks(3);
        $events = \DB::table('telemed_event_requests as tr')
            ->leftJoin('telemed_event_conclusion as tec',"tec.request_id","=","tr.id")
            ->leftJoin('telemed_centers as tmc',"tmc.id","=","tr.telemed_center_id")
            ->leftJoin('telemed_consult_requests as tc',"tc.request_id","=","tr.id")
            ->leftJoin('telemed_centers as tmcc',"tmcc.id","=","tc.telemed_center_id")
            ->where(function ($q2) use ($tmc_id,$fromDate,$toDate) {
            $q2->where(function ($query) use ($tmc_id,$fromDate,$toDate) {
                $query->where("tr.event_type","=","consultation");
                $query->where(function($query) use($tmc_id){
                    $query->whereNotNull("tc.request_id")
                        ->where(function($query)use($tmc_id)
                        {
                            $query->where(['tc.telemed_center_id' => $tmc_id, 'tc.request_id' => \DB::raw("tr.id")])
                            ->orWhere(['tr.telemed_center_id' => $tmc_id]);
                        })
                        ->where(function ($q) use ($tmc_id) {
                            $q->whereExists(function ($q1) use ($tmc_id) {
                                $q1->select("*")->from("telemed_requests_mcf_abonent as tra")->where(['tra.request_id' => \DB::raw("tr.id")]);
                            });
                            $q->whereExists(function ($q1) use ($tmc_id) {
                                $q1->select("*")->from("telemed_requests_mcf_consultant as trc")->where(['trc.request_id' => \DB::raw("tr.id")]);
                            });
                });
                })->where(['tc.decision' => 'agreed']);
            });

            $q2->orWhere(function ($query) use ($tmc_id) {
                $query->where(function($query) use($tmc_id){
                    $query->whereExists(function ($query) use ($tmc_id) {
                        $query->from("telemed_event_participants as trp")->where(['trp.telemed_center_id' => $tmc_id, 'trp.request_id' => \DB::raw("tr.id")]);
                    });
                    $query->orWhere(["tr.telemed_center_id" => $tmc_id]);
                });
                $query->whereIn("tr.event_type",['meeting','seminar']);
            })->where(function ($query) use ($fromDate,$toDate) {

                $query->whereBetween('tr.start_date', [$fromDate, $toDate]);

            });
        })->where('tr.canceled','!=',1)
            ->whereNull("tmc.deleted_at")
            ->whereNull("tmcc.deleted_at")
            ->select("tr.*","tc.uid_code",'tc.uid_year','tc.uid_id','tec.request_id as with_conclusion','tc.telemed_center_id as consultant_tmc');
        return $events->orderBy('tr.start_date', 'asc')->get();
    }

    public static function getMyTMCEvents($tmcs,$params)
    {
        $fromDate = Carbon::createFromDate($params['year'], $params['month'], 20, 'Europe/Moscow')->subWeek(3);
        $toDate = Carbon::createFromDate($params['year'], $params['month'], 20, 'Europe/Moscow')->addWeeks(3);;
        $in = \DB::table('telemed_event_requests as tr')->leftJoin("telemed_requests_mcf_abonent as tra", "tra.request_id", "=", "tr.id")
            ->leftJoin('telemed_centers as tmc',"tmc.id","=","tr.telemed_center_id")
            ->leftJoin('telemed_consult_requests as tc',"tc.request_id","=","tr.id")
            ->leftJoin('telemed_event_conclusion as tec',"tec.request_id","=","tr.id")
            ->leftJoin('telemed_centers as tmcc',"tmcc.id","=","tc.telemed_center_id")
            ->leftJoin('telemed_event_participants as trp','trp.request_id','=','tr.id')
            ->where(function ($q2) use ($tmcs,$params,$fromDate,$toDate) {
                $q2->whereNotIn("tr.telemed_center_id",$tmcs);
                $q2->where(function ($query) use ($tmcs, $params,$fromDate,$toDate) {
                    $query->whereIn('trp.telemed_center_id', $tmcs);
                    $query->whereIn("tr.event_type",['meeting','seminar'])->where(function ($query) use ($params,$tmcs,$fromDate,$toDate) {
                        $query->whereBetween('tr.start_date', [$fromDate, $toDate]);
                    });
                });
                $q2->orWhere(function ($query) use ($tmcs, $params,$fromDate,$toDate) {
                    $query->whereNotNull('tra.request_id');
                    $query->where(function ($q3) use ($tmcs, $params,$fromDate,$toDate) {
                        $q3->whereIn('tc.telemed_center_id', $tmcs)->where(function ($q) use ($tmcs, $params,$fromDate,$toDate) {
                            $q->where(function ($query) use ($params,$fromDate,$toDate) {
                                $query->where(function ($q9) use ($params, $fromDate, $toDate) {
                                    $q9->whereNotNull("tr.start_date")->whereBetween('tr.start_date', [$fromDate, $toDate]);
                                })->orWhere(function ($q10) use ($params, $fromDate, $toDate) {
                                    $q10->whereNull("tr.start_date")->whereBetween('tra.desired_date', [$fromDate, $toDate]);
                                });

                            });
                        });

                    });});
            })
            ->where(function($q){
                $q->where(function($q1)
                {
                    $q1->whereNull("tmc.deleted_at");
                    $q1->whereNull("tmcc.deleted_at");
                    $q1->whereNull("tec.request_id");
                });
                $q->orWhere(function($q1)
                {
                    $q1->whereNotNull("tec.request_id");
                });
            })
            ->where('tr.canceled','!=',1)->select("tr.*","tra.desired_date","tc.uid_code","tc.uid_id","tc.uid_year","tra.desired_time","tc.decision as consult_decision","tc.completed",'tec.request_id as with_conclusion','trp.decision as part_decision');

        /// OUTCOMING

        $out = \DB::table('telemed_event_requests as tr')->leftJoin("telemed_requests_mcf_abonent as tra", "tra.request_id", "=", "tr.id")
            ->leftJoin('telemed_centers as tmc',"tmc.id","=","tr.telemed_center_id")
            ->leftJoin('telemed_event_conclusion as tec',"tec.request_id","=","tr.id")
            ->leftJoin('telemed_consult_requests as tc',"tc.request_id","=","tr.id")
            ->leftJoin('telemed_centers as tmcc',"tmcc.id","=","tc.telemed_center_id")
            ->whereIn('tr.telemed_center_id', $tmcs)->where(function ($q2) use ($params,$tmcs,$fromDate,$toDate) {
                $q2->where(function ($query) use ($tmcs, $params,$fromDate,$toDate) {
                    $query->whereRaw("tr.event_type = 'meeting' or tr.event_type = 'seminar'")->where(function ($query) use ($params,$fromDate,$toDate) {
                        $query->whereBetween('tr.start_date', [$fromDate, $toDate]);

                    });

                });

                $q2->orWhere(function ($query) use ($tmcs, $params,$fromDate,$toDate) {
                    $query->whereNotNull('tra.request_id');
                    $query->where("tr.event_type","consultation");
                    $query->where(function ($q3) use ($tmcs, $params,$fromDate,$toDate) {
                        $q3->where(function ($q9) use ($params, $fromDate, $toDate) {
                                    $q9->whereNotNull("tr.start_date")->whereBetween('tr.start_date', [$fromDate, $toDate]);
                                })->orWhere(function ($q10) use ($params, $fromDate, $toDate) {
                                    $q10->whereNull("tr.start_date")->whereBetween('tra.desired_date', [$fromDate, $toDate]);
                        });

                    });});
            })
            ->where(function($q){
                $q->where(function($q1)
                {
                    $q1->whereNull("tmc.deleted_at");
                    $q1->whereNull("tmcc.deleted_at");
                    $q1->whereNull("tec.request_id");
                });
                $q->orWhere(function($q1)
                {
                    $q1->whereNotNull("tec.request_id");
                });
            })
            ->where('tr.canceled','!=',1)
            ->select("tr.*","tra.desired_date","tc.uid_code","tc.uid_id","tc.uid_year","tra.desired_time","tc.decision as consult_decision","tc.decision as consult_decision","tc.completed",'tec.request_id as with_conclusion');
        return ['out'=>$out->orderBy('tr.start_date', 'asc')->get(),"in"=>$in->orderBy('tr.start_date', 'asc')->get()];
    }
    public static function checkTime($participants,$date,$enddate,$event_id = 0)
    {
       $results =  \DB::table('telemed_event_requests as tr')->where(function ($query) use ($participants, $event_id) {
            if($event_id != 0)
            {
                $query->where('tr.telemed_center_id', '!=', $event_id);
            }
            $query->orWhere(function ($query) use ($participants) {
                $query->whereExists(function ($q1) use ($participants) {
                    $q1->from("telemed_event_participants as trp")->where(['trp.request_id' => \DB::raw("tr.id")])->whereIn("trp.telemed_center_id", $participants);
                });
            })->orWhere(function ($query) use ($participants) {
                $query->whereIn("tr.telemed_center_id", $participants);
            });
        })->whereBetween("tr.start_date", [$date, $enddate])->where("tr.event_type", "!=", "consultation")
           ->where('tr.canceled',false)
           ->get();
        return $results;
    }


    public static function checkTimeConsultWithError($in_id,$out_id,$time,$format,$current_consult_id)
    {
        $check = self::checkTimeConsult($in_id,$out_id,$time,$format,$current_consult_id);
        if(count($check)>0)
        {
            return "На данное время уже назначена другая консультация между ТМЦ ".$check[0]->in_name."(абонент) и ТМЦ ".$check[0]->out_name."(консультант)";
        }
        return true;
    }

    public static  function checkTimeConsult($in_id,$out_id,$time,$format,$current_consult_id)
    {
       $consults = DB::table("telemed_event_requests as ter")
           ->join('telemed_consult_requests as tcr',"tcr.request_id","=","ter.id")
           ->join('telemed_requests_mcf_abonent as abon',"abon.request_id","=","ter.id")
           ->join('telemed_requests_mcf_consultant as consult',"consult.request_id","=","ter.id")
           ->join('telemed_centers as in',"in.id","=","ter.telemed_center_id")
           ->join('telemed_centers as out',"out.id","=","tcr.telemed_center_id")
           ->where(["ter.event_type"=>"consultation","tcr.decision"=>"agreed"])
           ->where("ter.id","!=",$current_consult_id)
           ->where(function($query) use($in_id,$out_id,$time){
               $query->where(["ter.telemed_center_id"=>$in_id])
               ->orWhere(["tcr.telemed_center_id"=>$in_id])
               ->orWhere(["ter.telemed_center_id"=>$out_id])
               ->orWhere(["tcr.telemed_center_id"=>$out_id]);
           })
           ->whereBetween("ter.start_date",[Carbon::createFromFormat($format,$time)->subMinute(),Carbon::createFromFormat($format,$time)->addHour()])
           ->where('ter.canceled',false)
           ->select("tcr.telemed_center_id as out",'ter.telemed_center_id as in',"in.name as in_name","out.name as out_name")
           ->get();
        return $consults;
    }
}