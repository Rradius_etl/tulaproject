<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="UserNotification",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telemed_center_id",
 *          description="telemed_center_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class UserNotification extends Model
{

    public $table = 'notifications';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected  $primaryKey = "id";

    public static $auto_prefix = "<br><p>Данное сообщение рассылается автоматически, просьба не отвечать на данное сообщение</p>";

    public $fillable = [
        'message',
        'telemed_center_id',
        'need_callback'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'message' => 'string',
        'telemed_center_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    
    public function notification_recepients()
    {
        return $this->hasMany(UserNotificationRecepient::class,"notification_id","id");
    }
}
