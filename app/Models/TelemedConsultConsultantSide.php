<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Admin\EventFilePivot;
use App\Models\TelemedConsultRequest;
/**
 * @SWG\Definition(
 *      definition="TelemedConsultConsultantSide",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="responsible_person_fullname",
 *          description="responsible_person_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="appointed_consultant_name",
 *          description="appointed_consultant_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="appointed_consultant_spec",
 *          description="appointed_consultant_spec",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_fullname",
 *          description="coordinator_fullname",
 *          type="string"
 *      )
 * )
 */
class TelemedConsultConsultantSide extends Model
{

    public $table = 'telemed_requests_mcf_consultant';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = "request_id";
    public $fillable = [
        'doctor_id',
        'appointed_person_id',
        'appointed_person',
        'planned_date',
        'coordinator_fullname',
        'appointed_person',
        'appointed_person_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'integer',
        'doctor_id'=>'integer nullable',
        'responsible_person_fullname' => 'string',
        'appointed_person' => 'string',
        'appointed_person_id' => 'integer nullable',
        'appointed_consultant_name' => 'string',
        'appointed_consultant_spec' => 'string',
        'coordinator_fullname' => 'string',
        'planned_date'=>"string"
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedConsultAbonentRequest()
    {
        return $this->belongsTo(TelemedConsultAbonentSide::class,"request_id","request_id");
    }
    public function telemedConsultRequest()
    {
        return $this->belongsTo(TelemedConsultRequest::class,"request_id","request_id");
    }
    public function files()
    {
        return $this->hasMany(EventFilePivot::class,"request_id","request_id");
    }
    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }

    public function getAppointedPerson()
    {
        if( is_null($this->appointed_person_id) ) {
            return $this->appointed_person;
        } else {
            return User::find($this->appointed_person_id)->email;
        }
    }
}
