<?php

namespace App\Models;

use App\Models\Admin\File;
use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="FileAccessToken",
 *      required={""},
 *      @SWG\Property(
 *          property="hash",
 *          description="hash",
 *          type="string"
 *      )
 * )
 */
class FileAccessToken extends Model
{

    public $table = 'open_file_tokens';

    protected $primaryKey = 'hash';
    protected $keyType = 'string';
    public $incrementing = false;

    const HASH_LENGTH = 40;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'hash',
        'file_id',
        'state',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'hash' => 'string',
        'state' => 'boolean',
        'file_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    /**
     * Boot function for using with FileAccessToken
     *
     * @return void
     */



    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->hash =  self::generateToken(self::HASH_LENGTH);

    }


    /** Generate token
     * getToken($length) creates an alphabet to use within the token and then creates a string of length $length.
     * */
    protected  static  function generateToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }
    /** works as a drop in replacement for rand() or mt_rand. It uses openssl_random_pseudo_bytes to help create a random number between $min and $max.
     */
    protected static  function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function scopeActive($query)
    {
        return $query->where('state', true);
    }

    public function changeState($status)
    {
        $this->update(['state'=> boolval($status)]);
    }
}
