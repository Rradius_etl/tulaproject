<?php

namespace App\Models;

use App\Models\Admin\IndicationForUse;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpWord\PhpWord;

class DocxReport extends Model
{
    public $fontStyle;
    public $headerStyle;
    public $tableStyle;

    public $phpWord;


    public $data;
    public $rows;
    public $allHeaders;
    public $type;

    public $title;
    public $fileName;

    public $section;
    public $table;

    public function __construct(array $params)
    {
        $this->init($params);
        $this->setFontStyles();
        $this->drawSkeleton();
    }

    public function download()
    {
        //return $this;

        $this->fillDocument();

        // Saving the document as DOCX file...

        if($this->fileFormat == 'odt') {
            $file = $this->phpWord->save($this->fileName . '.odt', 'ODText', true);
        } else {
            $file = $this->phpWord->save($this->fileName . '.docx', 'Word2007', true);
        }


        return $file;

    }

    protected function init(array $params)
    {
        $this->phpWord = new PhpWord();

        $this->type = $params['type'];
        $this->dateRange = $params['dateRange'];
        $this->name = $params['name'];
        $this->fileFormat = $params['fileFormat'];

        $from = date('d-m-Y', strtotime($this->dateRange[0]));
        $to = date('d-m-Y', strtotime($this->dateRange[1]));
        // Название файла и Заголовок файла
        $this->fileName = "{$this->name} {$from}-{$to}";
        $this->title = "{$this->name} от {$from}, до {$to}";

        $this->rows = $params['rows'];
        $this->data = $params['reports'];

        $this->allHeaders = \Config::get("reports.rows.{$this->type}");

    }

    protected function drawSkeleton()
    {
        $this->section = $this->phpWord->addSection([
            'orientation' => 'landscape'
        ]);
        $this->section->addText($this->title, $this->headerStyle);
        $this->table = $this->section->addTable("Table1");
        $this->phpWord->addTableStyle('Table1', $this->tableStyle);
    }


    protected function setFontStyles()
    {
        $this->fontStyle = new \PhpOffice\PhpWord\Style\Font();

        $this->fontStyle->setBold(false);
        $this->fontStyle->setName('Tahoma');
        $this->fontStyle->setSize(10);

        $this->headerStyle = array('size' => 16, 'bold' => true);
        $this->tableStyle = ['borderSize' => 1, 'borderColor' => '000000', 'width' => 100 * 100, 'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT];


    }

    protected function fillTableHeader()
    {
        $this->table->addRow();

        if (in_array('num', $this->rows)) {
            $this->table->addCell()->addText($this->allHeaders['num']);

        }
        foreach ($this->rows as $header) {
            if ($header != 'num') {
                $this->table->addCell()->addText($this->allHeaders[$header]);
            }
        }
    }

    protected function fillDocument()
    {
        $this->fillTableHeader();
        if (count($this->data) > 0) {
            switch ($this->type) {
                case 'report':
                    $this->fillReportDocument();
                    break;
                case 'reportinout':
                    $this->fillReportInOutDocument();
                    break;
                case 'meeting':
                    $this->fillMeetingDocument();
                    break;
                case 'seminar':
                    $this->fillSeminarDocument();
                    break;
                case 'consultation':
                    $this->fillConsultationDocument();
                    break;
                case 'all':
                    $this->fillAllDocument();
                    break;
            }
        } else {
            $this->fillEmptyDocument();
        }

    }

    /* Показать только ошибку */
    protected function fillEmptyDocument()
    {
        $this->section->addText('Нет данных по вашим критериям поиска', $this->headerStyle);
    }

    protected function fillReportDocument()
    {
        $mcfs = [];
        $i = 1;
        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $rKey => $report) {
            $this->table->addRow();

            if (in_array('num', $headers)) {
                $this->table->addCell()->addText($i);
                $i++;
            }

            foreach ($headers as $header) {
                if (in_array($header, $headers) && $header != 'num') {

                    if (array_key_exists($header, $report)) {
                        if ($header == 'tmc_name') {
                            $mcfs[$report->mcf_id]['count'] = 1;

                            $this->table->addCell()->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

                        } else {
                            if ($report->{$header} == 0) {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            } elseif ($report->{$header} === null) {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            } else {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            }

                        }
                    } else {
                        $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                    }
                }
            }
        }

    }

    protected function fillReportInOutDocument()
    {
        $mcfs = [];
        $i = 1;
        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $rKey => $report) {
            $this->table->addRow();

            if (in_array('num', $headers)) {
                $this->table->addCell()->addText($i);
                $i++;
            }

            foreach ($headers as $header) {
                if (in_array($header, $headers) && $header != 'num') {

                    if (array_key_exists($header, $report)) {
                        if ($header == 'tmc_name') {
                            $mcfs[$report->mcf_id]['count'] = 1;

                            $this->table->addCell()->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

                        } else {
                            if ($report->{$header} == 0) {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            } elseif ($report->{$header} === null) {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            } else {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            }

                        }
                    } else {
                        $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                    }
                }
            }
        }

    }

    protected function fillMeetingDocument()
    {
        $mcfs = [];
        $i = 1;
        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $rKey => $report) {
            $this->table->addRow();

            if (in_array('num', $headers)) {
                $this->table->addCell()->addText($i);
                $i++;
            }

            foreach ($headers as $header) {
                if (in_array($header, $headers) && $header != 'num') {

                    if (array_key_exists($header, $report)) {
                        if ($header == 'tmc_name') {
                            $mcfs[$report->mcf_id]['count'] = 1;

                            $this->table->addCell()->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

                        } else {
                            if ($report->{$header} == 0) {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            } elseif ($report->{$header} === null) {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            } else {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            }

                        }
                    } else {
                        $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                    }
                }
            }
        }
    }

    protected function fillSeminarDocument()
    {
        $mcfs = [];
        $i = 1;
        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $rKey => $report) {
            $this->table->addRow();

            if (in_array('num', $headers)) {
                $this->table->addCell()->addText($i);
                $i++;
            }

            foreach ($headers as $header) {
                if (in_array($header, $headers) && $header != 'num') {

                    if (array_key_exists($header, $report)) {
                        if ($header == 'tmc_name') {
                            $mcfs[$report->mcf_id]['count'] = 1;

                            $this->table->addCell()->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

                        } else {
                            if ($report->{$header} == 0) {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            } elseif ($report->{$header} === null) {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            } else {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            }

                        }
                    } else {
                        $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                    }
                }
            }
        }
    }

    protected function fillConsultationDocument()
    {
        $mcfs = [];
        $i = 1;
        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $rKey => $report) {
            $this->table->addRow();

            if (in_array('num', $headers)) {
                $this->table->addCell()->addText($i);
                $i++;
            }

            foreach ($headers as $header) {
                if (in_array($header, $headers) && $header != 'num') {

                    if (array_key_exists($header, $report)) {
                        if ($header == 'tmc_name') {
                            $mcfs[$report->mcf_id]['count'] = 1;

                            $this->table->addCell()->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

                        } else {
                            if ($report->{$header} == 0) {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            } elseif ($report->{$header} === null) {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            } else {
                                $this->table->addCell()->addText($report->{$header}, $this->fontStyle, ['align' => 'center']);
                            }

                        }
                    } else {
                        $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                    }
                }
            }
        }
    }


    protected function fillAllDocument()
    {
        $onlyConsultationFields = [
            'create_date',
            'consult_type',
            'cons_uid',
            'medic',
            'cons_tmc_name',
            'consult_doctor',
            'patient_indications_for_use_id',
            'part_count',
            'importance'
        ];
        $onlySeminarFields = [
            'title',
        ];
        $onlyMeetingFields = [
            'title',
        ];
        $tmcFields = [
            'mcf_name',
            'tmc_name',
            'tmc_address'
        ];
        $importances = ["high" => "высокая", "low" => "низкая", "medium" => "средняя"];
        $eventTypes = [
            "meeting" => "совещание",
            "seminar" => "семинар",
            "consultation" => "консультация",
        ];
        $i = 1;
        $indForUse = IndicationForUse::all()->pluck('name', 'id');

        $reports = $this->data;
        $headers = $this->rows;

        foreach ($reports as $r) {
            foreach ($r['reports'] as $report) {


                $this->table->addRow();

                if (in_array('num', $headers)) {
                    $this->table->addCell()->addText($i);
                    $i++;
                }

                foreach ($headers as $header) {
                    if (in_array($header, $headers) && $header != 'num') {

                        if (in_array($header, $onlyConsultationFields) && $report->event_type !== 'consultation') {
                            $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                        } elseif (in_array($header, $onlySeminarFields) && $report->event_type !== 'seminar') {
                            $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                        } elseif (in_array($header, $onlySeminarFields) && $report->event_type !== 'meeting') {
                            $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                        } elseif ($header == 'initiator_id') {
                            if ($r['tmc']->id != $report->initiator_id) {
                                $this->table->addCell()->addText('Входящая', $this->fontStyle);
                            } else {
                                $this->table->addCell()->addText('Исходящая', $this->fontStyle);
                            }
                        } elseif( $header == 'patient_indications_for_use_id') {
                            if($report->patient_indications_for_use_id != 0 || $report->patient_indications_for_use_id != null) {

                                $this->table->addCell()->addText($indForUse[$report->patient_indications_for_use_id], $this->fontStyle);
                            } else {
                                $this->table->addCell()->addText("Иное: ".$report->patient_indications_for_use_other, $this->fontStyle);
                            }
                        } elseif ($header == 'mcf_name') {
                            $this->table->addCell()->addText( $r['mcf']->name, $this->fontStyle);
                        } elseif ($header == 'tmc_name') {
                            $this->table->addCell()->addText( $r['tmc']->name , $this->fontStyle);
                        }elseif ($header == 'tmc_address') {
                            $this->table->addCell()->addText(  $r['tmc']->address, $this->fontStyle);
                        }elseif ($header == 'event_type') {
                            $this->table->addCell()->addText( $eventTypes[$report->event_type], $this->fontStyle);
                        }elseif ($header == 'title_meeting') {
                            if($report->event_type == "meeting"){
                                $this->table->addCell()->addText($report->title , $this->fontStyle);
                            } else {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            }

                        }elseif ($header == 'title_seminar') {
                            if($report->event_type == "seminar"){
                                $this->table->addCell()->addText($report->title , $this->fontStyle);
                            } else {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            }
                        }elseif ($header == 'importance') {
                            if($report->importance !== null){
                                $this->table->addCell()->addText($importances[$report->importance] , $this->fontStyle);
                            } else {
                                $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                            }

                        } else {
                            $this->table->addCell()->addText( $report->{$header}, $this->fontStyle);
                        }
                    }
                }
            }
        }
    }

    protected function TEMPfillReportDocument()
    {
        $mcfs = [];
        $reports = $this->data;

        foreach ($reports as $idx => $report) {
            $this->table->addRow();


            if (array_key_exists('num', $this->rows)) {
                // № по порядку
                $this->table->addCell()->addText($idx + 1);
            }

            foreach ($this->rows as $header) {
                if (array_key_exists($header, $this->rows) && $header != 'num') {
                    if ($header == 'tmc_name') {
                        if (!array_key_exists($report->mcf_id, $mcfs)) {
                            $mcfs[$report->mcf_id]['count'] = 1;

                        }
                        $this->table->addCell(100 * 25)->addText($mcfs[$report->mcf_id]['count'] . ") " . $report->tmc_name);

                        $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;
                    } else {
                        if ($report[$header] === 0) {
                            $this->table->addCell()->addText($report[$header], $this->fontStyle, ['align' => 'center']);
                        } elseif ($report[$header] === null) {
                            $this->table->addCell()->addText('-', $this->fontStyle, ['align' => 'center']);
                        } else {
                            $this->table->addCell(100 * 20)->addText($report->terminal_name, $this->fontStyle);
                        }
                    }
                }
            }

        }
    }


}
