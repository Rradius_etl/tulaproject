<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

function get_object_public_vars($object)
{
    return get_object_vars($object);
}

class EnvFile
{

    protected static  $fillable = [
        'APP_ENV',
        'APP_DEBUG',
        'APP_URL',
        'MAIL_DRIVER',
        'MAIL_HOST',
        'MAIL_PORT',
        'MAIL_USERNAME',
        'MAIL_PASSWORD',
        'MAIL_ENCRYPTION'
    ];


    public function __construct()
    {


    }


    public static function saveData($param =[])
    {
        $path = base_path('.env');

        foreach ($param as $key => $val) {
            if (in_array($key, self::$fillable)) {
                $attributes[$key] = $val;
            }
        }

        if (file_exists($path)) {
            $file = (file_get_contents($path, true));

            foreach ($attributes as $key => $value) {

                $file = preg_replace(
                    "/" . $key . "=(.*)/", $key . "=" . $value . "\r", $file);
            }

            file_put_contents($path, $file);
            return true;
        }


        return false;

    }


    public static function getVars()
    {
        $assoc_array = array();
        $path = base_path('.env');
        if (file_exists($path)) {
            $file = (file_get_contents($path, true));

            $my_array = explode("\r\n", $file);
            
            foreach ($my_array as $line) {

                $tmp = explode("=", $line);
                if (count($tmp) > 1)
                    if (in_array($tmp[0], self::$fillable))
                        $assoc_array[$tmp[0]] = $tmp[1];
            }



            return $assoc_array;
        }
    }
}
