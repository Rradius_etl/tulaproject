<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 09.12.2016
 * Time: 20:16
 */

namespace App\Models;


use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Thread;

class UserMessage extends Message
{
    public function files()
    {
        return $this->hasMany(MessageFilePivot::class, "message_id", "id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

     

}