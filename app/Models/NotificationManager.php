<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 15.12.2016
 * Time: 15:55
 */

namespace App\Models; 
use App\Models\Admin\TelemedEventParticipant;
use App\Models\Admin\TelemedUserPivot;
use Illuminate\Support\Facades\Request;
use Mockery\CountValidator\Exception;
use Sentinel;
use App\Models\UserNotification;
use App\Models\UserNotificationRecepient;
use \Illuminate\Support\Facades\Request as Req;

class NotificationManager
{
    public static function sendEmail($message,$user_ids)
    {
        $users = [];
        foreach($user_ids as $usr)
        {
            $user = User::find($usr);
            if($user != null)
            array_push($users,$user->email);
        }
        try {
            \Mail::send('emails.notification.template', ['m' => $message], function ($m) use ($users) {
                $m->from(config('mail.username'), config('mail.username'));
                $m->to($users)->subject("Новое уведомление.");
            });
        }
        catch (\Exception $e)
        {
            return false;
        }
        return true;
    }



    /**
     * Отправка уведомлении об опросе всем пользователем нескольких ТМЦ
    */
    public static function createPollNotificationForManyTmc(array $tmcs, $poll_id, $send_email = false,$tmc_id = null) {
        $user_list = [];
        $tmc = null;
        if($tmc_id != null)
        {
            $tmc = TelemedCenter::find($tmc_id);
        }

        if($tmc != null)
        {
            $message = "Пользователь ".$tmc->name." приглашает вас принять участие в опросе по <a href='".Request::root()."/public_polls/".$poll_id."'>ссылке</a>";
        }
        else
        {
            $message = "Пользователь ".Sentinel::getUser()->email." приглашает вас принять участие в опросе по <a href='".Request::root()."/public_polls/".$poll_id."'>ссылке</a>";
        }

        $mcfs = [];
        foreach ($tmcs as $tmc_id) {
            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])
                ->whereExists(function($q) use($tmc_id)
                {
                    $q->select(\DB::raw('1'))->from('telemed_centers as t')->whereNull('t.deleted_at')->where('t.id',\DB::raw('telemed_user_pivots.telemed_center_id'));
                })
                ->get();
            $tmc1 = TelemedCenter::find($tmc_id);
            if($tmc1 != null)
            {
                $mcf = $tmc1->medCareFac;
                $user =  $mcf->user;
                if($user != null)
                {
                    if(!in_array($user->id,$user_list))
                        array_push($user_list,$user->id);
                }
            }
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                }
            }
        }

            self::createPollNotification($message,$user_list, false ,$send_email);
    }


    public static function createPollNotificationForTmcUsers($tmc_id,$poll_id,$send_email = false, $out_telemed_id = null)
    {
        $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])
            ->whereExists(function($q) use($tmc_id)
            {
                $q->select(\DB::raw('1'))->from('telemed_centers as t')->whereNull('t.deleted_at')->where('t.id',\DB::raw('telemed_user_pivots.telemed_center_id'));
            })
            ->get();

        if(count($tmc_users)>0)
        {
            $users = $tmc_users->pluck("user_id")->toArray();
            $tmc = null;
            if($out_telemed_id != null)
            {
                $tmc = TelemedCenter::find($out_telemed_id);
            }

            $tmc1 = TelemedCenter::find($tmc_id);
            if($tmc1 != null)
            {
                $mcf = $tmc1->medCareFac;
                $user =  $mcf->user;
                if($user != null)
                {
                    if(!in_array($user->id,$users))
                        array_push($user_list,$user->id);
                }
            }

            if($tmc != null)
            {
                $message = "Пользователь ".$tmc->name." приглашает вас принять участие в опросе по <a href='".Request::root()."/public_polls/".$poll_id."'>ссылке</a>";
            }
            else
            {
                $message = "Пользователь ".Sentinel::getUser()->email." приглашает вас принять участие в опросе по <a href='".Request::root()."/public_polls/".$poll_id."'>ссылке</a>";
            }
            $need_callback = false;
            self::createPollNotification($message,$users,$need_callback,$send_email);
        }
    }

    public static function createPollNotification($message,$recepients,$need_callback,$send_email = false)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->type = 'poll';
        $notification->need_callback = $need_callback;
        if($notification->save())
        {
            self::addRecepeints($recepients,$notification->id);
            if(count($recepients)>0 && $send_email == true)
            {
                self::sendEmail($message,$recepients);
            }
        }
        return true;
    }


    public static function createNotificationForTmcUsers($message,$tmc_id,$need_callback,$send_email = false)
    {
        $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])
            ->whereExists(function($q) use($tmc_id)
            {
              $q->select(\DB::raw('1'))->from('telemed_centers as t')->whereNull('t.deleted_at')->where('t.id',\DB::raw('telemed_user_pivots.telemed_center_id'));
            })
            ->get();
        if(count($tmc_users)>0)
        {
            $users = $tmc_users->pluck("user_id");
            self::createNotification($message,$users,$need_callback,$send_email);
        }
    }

    public static function createNotificationForManyTmc($message,$telemed_centers,$need_callback,$send_email = false,$telemed_id = null)
    {
        $user_list = [];
        if($telemed_id != null)
        {
            $t_users = TelemedUserPivot::where(['telemed_center_id'=>$telemed_id])->get();
            foreach ($t_users as $usr)
            {
               array_push($user_list,$usr->user->id);
            }
        }
        foreach ($telemed_centers as $tmc_id) {
            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])
                ->whereExists(function($q) use($tmc_id)
                {
                    $q->select(\DB::raw('1'))->from('telemed_centers as t')->whereNull('t.deleted_at')->where('t.id',\DB::raw('telemed_user_pivots.telemed_center_id'));
                })
                ->get();
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                }
            }
        }

        self::createNotification($message,$user_list,$need_callback,$send_email);
    }
    public static function createNotificationForAdmin($message,$send_email = false)
    {
        $adminrole = \Sentinel::getRoleRepository()->findBySlug("admin");
        $admins = array_pluck($adminrole->users()->get()->toArray(),'id');
        self::createNotification($message,$admins,true,$send_email);
        return true;
    }

    public static function createNotificationFromTMC($message,$recepients,$need_callback,$tmc_id,$send_email)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;
        $notification->telemed_center_id = $tmc_id;
        if($notification->save())
        {
            self::addRecepeints($recepients,$notification->id,$send_email);
            self::sendEmail($message,$recepients);
        }
    }

    public static function createNotificationFromTMCForManyTmc($message,$telemed_centers,$need_callback,$tmc_id,$send_email = false)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;
        $notification->telemed_center_id = $tmc_id;
        $user_list = [];
        foreach ($telemed_centers as $tmc_id) {
            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])->get();
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                }
            }
        }
        if($notification->save())
        {
            self::addRecepeints($user_list,$notification->id,$send_email);
            self::sendEmail($message,$user_list);
        }
    }

    public static function createNotificationFromTMCForManyTmcUpdate($telemed_centers,$need_callback,$tmc_id,$send_email = false,$tmc,$event)
    {

        $eventName = ($event->event_type == 'seminar') ? 'заявка на создание семинара' : 'заявка на создание совещания';
        foreach ($telemed_centers as $tmc1) {

                if($tmc == null)
                    $message = "Пользователь - ".Sentinel::getUser()->email." уведомляет вас, что  " . $eventName . " изменена. Ознакомьтесь с изменениями. <a href='".Req::root()."/invitations/" . $tmc1->id . "' >Посмотреть </a>";
                else
                    $message = "Пользователь - ".$event->telemedCenter->name." уведомляет вас, что  " . $eventName . " изменена. Ознакомьтесь с изменениями. <a href='".Req::root()."/invitations/" . $tmc1->id . "' >Посмотреть </a>";
                $notification = new UserNotification();
                $notification->message = $message;
                $notification->need_callback = $need_callback;
                $notification->telemed_center_id = $tmc_id;
                $user_list = [];
                $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc1->telemed_center_id])->get();
                if(count($tmc_users)>0)
                {
                    $users = $tmc_users->pluck("user_id");
                    foreach($users as $user)
                    {
                        array_push($user_list,$user);
                    }
                }
                if($notification->save())
                {
                    self::addRecepeints($user_list,$notification->id,$send_email);
                    self::sendEmail($message,$user_list);
                }

        }
    }

    public static function createNotificationFromTMCtoMCF($message,$mcf_id,$need_callback,$tmc_out_id,$send_email)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;
        $notification->telemed_center_id = $tmc_out_id;

        $user_list = [];
        $tmcs = TelemedCenter::where(["med_care_fac_id"=>$mcf_id])->get();
        foreach($tmcs as $tmc)
        {
            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc->id])->get();
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                
                }
            }
        }


        if($notification->save())
        {
            self::addRecepeints($user_list,$notification->id,$send_email);
            self::sendEmail($message,$user_list);
        }
    }

    /**
     * 
    */
    public static function createNotificationToMCF($message,$mcf_id,$need_callback,$send_email)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;

        $user_list = [];
        $tmcs = TelemedCenter::where(["med_care_fac_id"=>$mcf_id])->get();
        foreach($tmcs as $tmc)
        {
            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc->id])->get();
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                 
                }
            }
        }


        if($notification->save())
        {
            self::addRecepeints($user_list,$notification->id,$send_email);
            self::sendEmail($message,$user_list);
        }
    }

    public static function createNotificationFromTMCtoTMC($message,$tmc_id,$need_callback,$tmc_out_id,$send_email)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;
        $notification->telemed_center_id = $tmc_out_id;

        $user_list = [];

            $tmc_users = TelemedUserPivot::where(['telemed_center_id'=>$tmc_id])->get();
            if(count($tmc_users)>0)
            {
                $users = $tmc_users->pluck("user_id");
                foreach($users as $user)
                {
                    array_push($user_list,$user);
                  
                }
            }


        if($notification->save())
        {
            self::addRecepeints($user_list,$notification->id,$send_email);
            self::sendEmail($message,$user_list);
        }
    }
    public static function createNotification($message,$recepients,$need_callback,$send_email = false)
    {
        $notification = new UserNotification();
        $notification->message = $message;
        $notification->need_callback = $need_callback;
        if($notification->save())
        {
            self::addRecepeints($recepients,$notification->id);
            if(count($recepients)>0 && $send_email == true)
            {
                self::sendEmail($message,$recepients);
            }
        }
        return true;
    }

    protected static function addRecepeints($recepients,$notification_id)
    {

        foreach($recepients as $recepient)
        {
            $rec = new UserNotificationRecepient();
            $rec->user_id = $recepient;
            $rec->notification_id = $notification_id;
            if($rec->save())
            {
                self::clearCache($recepient);
            }
        }
    }

    protected static function getUnreadNotificationsCountFromDB()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            $count = UserNotificationRecepient::where(['user_id'=>$user->id,'is_read'=>false])->count();
            return $count;
        }
        else
        {
            return false;
        }
    }

    protected static function getLastUnreadNotificationFromDB()
    {
        $user = Sentinel::getUser();
        $last = UserNotificationRecepient::where('user_id',$user->id)->where('is_read',false)->orderBy('created_at','desc')->with('notification')->first();
        if($last != null)
        {
            return $last;
        }
    }

    public static function getLastUnreadNotification()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            return \Cache::remember('usr_last_unr_' . $user->id, 120, function () use ($user)  {
                return self::getLastUnreadNotificationFromDB();
            });
        }
        return false;
    }

    public static function getUnreadNotificationsCount()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            return \Cache::remember('usr_unr_count_' . $user->id, 120, function () use ($user)  {
                return self::getUnreadNotificationsCountFromDB();
            });
        }
        return false;
    }


    /**
     * @return array(\Notification)
     **/
    protected static function getUnreadPollNotifications()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            $notifications = UserNotificationRecepient::where(['user_id'=>$user->id,'is_read'=>false])->whereExists(function ($query)
            {
                $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type','poll');

            });
            return $notifications;
        }
        else
        {
            return false;
        }
    }

    public static function getUnreadPollNotificationsCount()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            return \Cache::remember('usr_unr_poll_count_' . $user->id, 120, function () use ($user)  {
                return self::getUnreadPollNotificationsCountFromDB();
            });
        }
        return false;
    }

    protected static function getUnreadPollNotificationsCountFromDB()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            $count = UserNotificationRecepient::where(['user_id'=>$user->id,'is_read'=>false])->whereExists(function ($query)
            {
                $query->select(\DB::raw('1'))->from('notifications as n')->where('n.type','poll');
            })->count();
            return $count;
        }
        else
        {
            return false;
        }
    }

    protected static function clearCache($user_id)
    {
        \Cache::forget('usr_last_unr_'.$user_id);
        \Cache::forget('usr_unr_count_'.$user_id);
    }

    public static function clearUserCache()
    {
        $user = Sentinel::getUser();
        if($user != null)
        {
            \Cache::forget('usr_unr_poll_count_'.$user->id);
            \Cache::forget('usr_last_unr_'.$user->id);
            \Cache::forget('usr_unr_count_'.$user->id);
        }
        else
        {
            return false;
        }
    }


}