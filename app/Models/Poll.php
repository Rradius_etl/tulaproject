<?php

namespace App\Models;


use Carbon\Carbon;
use Eloquent as Model;
use App\Models\Admin\PollRow;
/**
 * @SWG\Definition(
 *      definition="Poll",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="author_id",
 *          description="author_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Poll extends Model
{

    public $table = 'polls';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'title',
        'author_id',
        'finished_at',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'author_id' => 'integer',
       
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'sometimes|required',
        'finished_at' => 'sometimes|required'
    ];

    /** Активные */
    public function scopeActive($query)
    {
        return $query->where('active', true)->where('finished_at','>', Carbon::now());
    }
    
    /**
     * Архив опросов
    */
    public function scopeInArchive($query)
    {
        return $query->where('finished_at','<', Carbon::now());
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class,'author_id',"id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollRows()
    {
        return $this->hasMany(\App\Models\Admin\PollRow::class,'poll_id','id');
    }

    public function telemedCenter()
    {
        return $this->belongsTo(TelemedCenter::class,'telemed_center_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollUserPivots()
    {
        return $this->hasMany(\App\Models\Admin\PollUserPivot::class);
    }

    public function totalVotes()
    {
       $count = \DB::table('poll_user_pivots as pup')->join('poll_rows as pr',"pup.poll_row_id","=","pr.id")
            ->where("pr.poll_id",$this->id)->count();
        return $count;
    }
}
