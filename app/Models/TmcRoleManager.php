<?php
namespace App\Models;

use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedUserPivot;
use Cache;

class TmcRoleManager
{
    public static function hasRole($role)
    {
       $roles = self::getRoles();
        if(in_array($role,$roles))
        {
            return true;
        }
        return false;
    }

    public static function countOfRoles($role)
    {
        $roles = self::getRoles();
        $res = array_count_values($roles);
        if(isset($res[$role]))
        {
            return $res[$role];
        }
        else
        {
            return 0;
        }
    }

    public static function getRoles()
    {
        $user = \Sentinel::getUser();
        if($user!=null) {
            return \Cache::remember('userroles' . $user->id, 120, function () use ($user)  {
                return self::getRolesWithUserFromDB($user);
            });
        }
        return false;
    }

    public static function getRolesWithUser(User $user)
    {
        if($user!=null) {
            return \Cache::remember('userroles' . $user->id, 120, function () use ($user)  {
                return self::getRolesWithUserFromDB($user);
            });
        }
        return false;
    }


    public static function clearCacheForTMCUsers($tmc_id)
    {
        $pivots = TelemedUserPivot::where('telemed_center_id',$tmc_id)->get();
        foreach($pivots as $pivot)
        {
            Cache::forget("userroles" . $pivot->user_id);
            Cache::forget("usertmcs". $pivot->user_id);
        }
        $tmc = TelemedCenter::withTrashed()->find($tmc_id);
        if($tmc != null)
        {
            $mcf = MedCareFac::withTrashed()->find($tmc->med_care_fac_id);
            if($mcf != null)
            {
                $mcf_user = $mcf->user;
                if($mcf_user != null)
                {
                    Cache::forget("userroles" . $mcf_user->id);
                    Cache::forget("usertmcs". $mcf_user->id);
                }
            }
        }
    }
    public static function clearCacheForUser($user_id)
    {
            Cache::forget("userroles" . $user_id);
            Cache::forget("usertmcs". $user_id);
    }

    public static function getTmcs()
    {
        $user = \Sentinel::getUser();
        if($user != null)
        {
            return \Cache::remember('usertmcs' . $user->id, 120, function () use ($user)  {
                $tmcs = [];
                $medcarefac = $user->medcarefacs;
                if($medcarefac != null)
                {
                    $t = $medcarefac->telemedCenters;
                    foreach($t as $telemed)
                    {
                        array_push($tmcs,["name"=>$telemed->name,"id"=>$telemed->id]);
                    }
                }
                $check = $user->tmcRoles;
                if(count($check)>0)
                    foreach($check as $role)
                    {
                        $t = $role->telemedCenter;
                        if($t != null)
                        array_push($tmcs,["name"=>$t->name,"id"=>$t->id]);
                    }
                return $tmcs;
            });
        }
    }

    public static function getRolesWithUserFromDB(User $user)
    {
        $roles = [];
        if($user!=null)
        {
            $medcarefac = $user->medcarefacs;
            if($medcarefac != null)
            {
                array_push($roles,"mcf_admin");
                return $roles;
            }
            $check = $user->tmcRoles()->with('telemedCenter')->get();
            if(count($check)>0)
                foreach($check as $role)
                {
                    if(!in_array($role->value,$roles)&&$role->telemedCenter!=null)
                    {
                        array_push($roles,$role->value);
                    }
                }
        }
        return $roles;
    }
}
