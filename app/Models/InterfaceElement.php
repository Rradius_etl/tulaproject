<?php

namespace App\Models;


use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="InterfaceElement",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disabled_users",
 *          description="disabled_users",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="disabled_roles",
 *          description="disabled_roles",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="params",
 *          description="params",
 *          type="string"
 *      )
 * )
 */
class InterfaceElement extends Model
{

    public $table = 'interface_elements';


    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'id',
        'name',
        'description',
        'disabled_users',
        'disabled_roles',
        'params'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'disabled_users' => 'string',
        'disabled_roles' => 'string',
        'params' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function disabledUsers()
    {
        return $this->belongsToMany(User::class, 'user_interface_elements', 'interface_element_id', 'user_id')->withTimestamps('created_at', 'updated_at');
    }

    public static function isVisible($elName)
    {
        $el = InterfaceElement::find($elName);


        if (empty($el)) {
            return true;
        } else {
            $disabledRoles = json_decode($el->disabled_roles);

            if (\Sentinel::check()) {

                $user = \Sentinel::getUser();

                if (empty($disabledRoles)) {
                    return self::isVisibleForUser($elName, $user, true);
                } else {

                    $roles = $user->roles->pluck('slug');

                    $access = true;
                    foreach ($roles as $role) {
                        if (in_array($role, $disabledRoles)) {
                            $access = false;
                        }
                    }

                    return self::isVisibleForUser($elName, $user, $access);

                }

            } else {
                /* Если пользователь неавтоизованный, то проверяем имеется ли роль guest в списке заблокирваонных */
                if (in_array('guest', $disabledRoles)) {
                    return false;
                } else {
                    return true;
                }
            }


        }

    }

    protected static function isVisibleForUser($elName, $user, $access)
    {
        $disabledInterfaceElements = $user->disabledInterfaceElements->pluck('id')->toArray();

        if(count($disabledInterfaceElements) > 0) {

            if (in_array($elName, $disabledInterfaceElements)) {
                return false;
            } else {
                return true;
            }
        } else {
            return $access;
        }

    }

}
