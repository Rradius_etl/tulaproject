<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Report",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="headers",
 *          description="headers",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tmcs",
 *          description="tmcs",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      )
 * )
 */
class Report extends Model
{

    public $table = 'user_reports';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public  static $reportTypes = [
        null => '--Выберите--',
        'report' => 'Отчет о проведенных видеоконференциях',
        'reportinout'=>"Отчет о проведенных видеоконференциях(входящие и исходящие)",
        'meeting' => 'Отчет по совещаниям',
        'seminar' => 'Отчет по обучающим семинарам',
        'consultation' => 'Отчет по клиническим консультациям',
        'all' => 'Отчет по всем видам видеоконференций',
    ];

    public $fillable = [
        'user_id',
        'headers',
        'name',
        'tmcs',
        'tmc_id',
        'roles',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tmc_id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'headers' => 'string',
        'tmcs' => 'string',
        'roles' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }


    public function showReadableType()
    {
        return self::$reportTypes[$this->type];
    }

    public function telemedcenters()
    {
        $tmcs = json_decode($this->tmcs);
        return TelemedCenter::whereIn('id', $tmcs)->get();
    }


    public function isCreatedByMcfAdmin()
    {

        if(\Sentinel::inRole('mcf_admin')) {
            $tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');

            if($this->tmc_id != null &&  in_array($this->tmc_id, $tmcs)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }
}
