<?php

namespace App\Models\Stats;

class TrackerSession extends \PragmaRX\Tracker\Vendor\Laravel\Models\Session
{



    public function scopeFromTo($query, $period, $alias = '')
    {
        $alias = $alias ? "$alias." : '';

        return $query
            ->where($alias.'updated_at', '>=', $period[0] ? $period[0] : 1)
            ->where($alias.'updated_at', '<=', $period[1] ? $period[1] : 1);
    }

}
