<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Response;
use CpChart\Factory\Factory as pCharts;

class GraphicalReport extends Model
{
    const IMG_WIDTH = 1440;
    const IMG_HEIGHT = 800;

    public static $eventTypes = [
        "meeting" => "совещание",
        "seminar" => "семинар",
        "consultation" => "консультация",
    ];

    public static $importances = [
        'low' => 'низкая',
        'medium' => 'средняя',
        'high' => 'высокая'
    ];


    public $data;
    public $rows;
    public $allHeaders;
    public $type;
    public $report;

    public $dateRange;
    public $title;
    public $fileName;

    // Graphic parameters
    public $factory;
    public $chartData;
    public $mainPicture;

    public $chartType;

    public $textField;
    public $numericField;

    public $abscissaTitle;
    public $ordinateTitle;

    public $finalWidth;
    public $finalHeight;

    public function __construct(array $attributes)
    {
        $this->init($attributes);

    }


    public function drawGraphic()
    {


        if (count($this->data) == 0 ) {
            return \Image::make('images/charts-error.png')->response();
        }

        switch ($this->type) {
            case 'report':
                $this->drawReportGraphic();
                break;
            case 'reportinout':
                $this->drawReportGraphicInOut();
                break;
            case 'meeting':
                $this->drawMeetingGraphic();
                break;
            case 'seminar':
                $this->drawSeminarGraphic();
                break;
            case 'consultation':
                $this->drawConsultationGraphic();
                break;
            case 'all':
                $this->drawAllGraphic();
                break;
        }
    }


    protected function init(array $params)
    {

        $this->report = $params['report'];
        $this->dateRange = $params['range'];

        $this->type = $this->report->type;

        $this->name = $this->report->name;

        $this->rows = json_decode($this->report->headers);
        $this->tmcs = json_decode($this->report->tmcs);

        // Название файла и Заголовок файла
        $this->fileName = "{$this->name} {$this->dateRange[0]}-{$this->dateRange[1]}";
        $this->title = "{$this->name} от {$this->dateRange[0]}, до {$this->dateRange[1]}";

        $this->allHeaders = \Config::get("reports.rows.{$this->type}");

        // Название Абциссы и ординаты
        $this->textField = $params['text_field'];
        $this->numericField = $params['numeric_field'];
        if($this->report->type=="all")
        {
            $this->abscissaTitle ="ТМЦ";
            $this->ordinateTitle = "Кол-во завершенных мероприятий";
        }
        else
        {
            $this->abscissaTitle = $this->allHeaders[$this->textField];
            $this->ordinateTitle = $this->allHeaders[$this->numericField];
        }

        //Тип графика
        $this->chartType = $params['chart_type'];

        //

        $this->factory = new pCharts();

        $this->chartData = $this->factory->newData();
    }

    protected function createImage()
    {
        // Create the image
        $this->mainPicture = $this->factory->newImage($this->finalWidth, $this->finalHeight, $this->chartData);
        /* Set the default font */
        $this->mainPicture->setFontProperties(array("FontName" => public_path("fonts/arial/arial.ttf"), "FontSize" => 12));
    }

    public function renderGraphic()
    {
        /*  $this->getData();
          return count($this->data);

          return [ 'qqq', $this->chartType, $this->data];*/


        $this->getData();
          /*return [$this->type, count($this->data),$this->data];*/
        if(count($this->data) > 0 ) {
            $this->drawGraphic();
        } else {
            return $this->drawGraphic();
        }

       //
        return $this->mainPicture->stroke();
    }


    protected function getData()
    {

        if ($this->report->type == 'all') {

            if ($this->chartType == 'linechart') {
                $this->getAllDataByTime();
            } else if ($this->chartType == 'barchart') {
                $this->getAllDataByTmc();
            }

        } else {

            $tableReport = new TableReport($this->report);
            $tableReport->range = $this->dateRange;

            $tableReport->getData();
            $this->data = $tableReport->data;
        }


    }

    /** Получить данные из БД для всех видов видеоконфереции сгрупированное по Дате*/
    protected function getAllDataByTime()
    {
        $range = $this->dateRange;
        $array = [];


        $tmcs = TelemedCenter::whereIn('id', $this->tmcs)->get();
        $i = 0;
        $reportsarray = [];
        foreach ($tmcs as $tmc) {
            $tmc_id = $tmc->id;
            $obj = [];
            $obj['tmc'] = $tmc;
            $obj['mcf'] = $tmc->medCareFac;

            $report = \DB::table('telemed_event_requests as ter')->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
                ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
                ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
                ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
                ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
                ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
                ->where(function ($query) use ($tmc_id) {
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where("ter.telemed_center_id", "=", $tmc_id);
                        $q->where("ter.telemed_center_id", "!=", \DB::raw("tep.telemed_center_id"));
                        $q->where('tep.decision', 'agreed');
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where(function ($q1) use ($tmc_id) {
                            $q1->whereNotNull('abon.request_id');
                            $q1->whereNotNull('cons.request_id');
                            $q1->where(function ($q2) use ($tmc_id) {
                                $q2->where('ter.telemed_center_id', $tmc_id);
                            });
                            $q1->where("ter.telemed_center_id", "!=", \DB::raw("tcr.telemed_center_id"));
                            $q1->whereNotNull('tcr.telemed_center_id');
                            $q1->where('ter.event_type', 'consultation');
                        });
                        $q->where('tcr.decision', 'agreed');
                    });
                })
                ->whereBetween('ter.start_date', $range)
                ->select('ter.id',
                    'ter.telemed_center_id as initiator_id',
                    "ter.event_type",
                    'ter.start_date',
                    'types.name as consult_type',
                    \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"),
                    \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"),
                    'cons_tmc.name as cons_tmc_name',
                    'abon.patient_indications_for_use_id',
                    'abon.patient_indications_for_use_other',
                    'ter.title',
                    \DB::raw("COUNT(tep.id) as part_count"),
                    'ter.importance',
                    'abon.created_at as create_date',
                    'abon.doctor_fullname as medic')
                ->groupBy("ter.id");
            if($i == 0)
            {
                $reports = $report;
            }
            else
            {
                $reports->unionAll($report);
            }
            $i++;

        }

        $range1 = new Carbon($this->dateRange[0]);
        $range2 = new Carbon($this->dateRange[1]);

        $difference = $range2->diffInDays($range1);
        $reports = collect($reports->get());

        if ($difference < 15) {
            $abscissaTitle = "Дата (день)";
            $this->data = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('d-m-Y');
            });
        } else if ($difference < 110) {
            $abscissaTitle = "Дата(Год и Номер недели года)";
            $this->data = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('Y-W');
            });
        } else if ($difference < 367) {
            $abscissaTitle = "Дата (месяц)";
            $this->data = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('m-Y');
            });
        } else if ($difference < 365.25 * 16 && $difference > 366) {
            $abscissaTitle = "Дата (Год)";
            $this->data = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('Y');
            });
        }

        $this->abscissaTitle = $abscissaTitle;
        $this->ordinateTitle = 'Количество видеоконферецнии';
        $this->data = $this->data->map(function ($x) {
            return $x->groupBy('event_type');
        });
        //echo  json_encode([$difference, $this->dateRange, $this->data]);die();
    }

    /** Получить данные из БД для всех видов видеоконфереции сгрупированное по ТМЦ */
    protected function getAllDataByTmc()
    {
        $range = $this->dateRange;
        $tmcs = TelemedCenter::whereIn('id', $this->tmcs)->get();

        $reportsarray = [];
        foreach ($tmcs as $tmc) {
            $tmc_id = $tmc->id;
            $obj = [];
            $obj['tmc'] = $tmc;
            $obj['mcf'] = $tmc->medCareFac;

            $reports = \DB::table('telemed_event_requests as ter')
                ->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
                ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
                ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
                ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
                ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
                ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
                ->where(function ($query) use ($tmc_id) {
                    $query->where(function ($q) use ($tmc_id) {
                        $q->where("tep.telemed_center_id", "=",$tmc_id);
                        $q->where("ter.telemed_center_id","!=", \DB::raw("tep.telemed_center_id"));
                        $q->where('tep.decision','agreed');
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where("ter.telemed_center_id", "=",$tmc_id);
                        $q->where("ter.telemed_center_id","!=", \DB::raw("tep.telemed_center_id"));
                        $q->where('tep.decision','agreed');
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where(function ($q1) use ($tmc_id) {
                            $q1->whereNotNull('abon.request_id');
                            $q1->where(function ($q2) use ($tmc_id) {
                                $q2->where('ter.telemed_center_id', $tmc_id);
                                $q2->orWhere("tcr.telemed_center_id", $tmc_id);
                            });
                            $q1->where("ter.telemed_center_id","!=",\DB::raw("tcr.telemed_center_id"));
                            $q1->whereNotNull('tcr.telemed_center_id');
                            $q1->where('ter.event_type', 'consultation');
                        });
                        $q->where('tcr.decision', 'agreed');
                    });
                })
                ->whereBetween('ter.start_date', $range)
                ->select('ter.id',
                    'ter.telemed_center_id as initiator_id',
                    "ter.event_type",
                    'ter.start_date',
                    'types.name as consult_type',
                    \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"),
                    \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"),
                    'cons_tmc.name as cons_tmc_name',
                    'abon.patient_indications_for_use_id',
                    'abon.patient_indications_for_use_other',
                    'ter.title',
                    \DB::raw("COUNT(tep.id) as part_count"),
                    'ter.importance',
                    'abon.created_at as create_date',
                    'abon.doctor_fullname as medic')
                ->groupBy("ter.id")
                ->get();

            if (count($reports) != 0) {
                $obj['reports'] = $reports;
                array_push($reportsarray, $obj);
            }

        }
        $this->data = $reportsarray;


    }


    public function downloadGraphic($fileFormat)
    {

        $this->getData();
        $this->drawGraphic();

        if ($fileFormat == 'pdf') {
            if ($this->report->type == 'all') {
                $url = route('reports.render.charts', ['diagram'=>$this->chartType,'id' => $this->report->id, 'from' => $this->dateRange[0]->format('Y-m-d'), 'to' => $this->dateRange[1]->format('Y-m-d')]);
            } else {
                $url = route('reports.render.charts', [
                    'id' => $this->report->id,
                    'from' => $this->dateRange[0]->format('Y-m-d'),
                    'to' => $this->dateRange[1]->format('Y-m-d'),
                    'text_field' => $this->textField,
                    'numeric_field' => $this->numericField,
                    'diagram'=>$this->chartType
                ]);
            }



            return \PDF::loadHTML('<img style="width:100%" src="' . $url . '" >')->setOrientation('landscape')->download($this->fileName . '.pdf');
        } else if ($fileFormat == 'jpeg' || $fileFormat == 'jpg' || $fileFormat == 'png') {
            return $this->mainPicture->stroke();

        }
    }

    /** Задать цвета линии */
    protected function setEventTypesPallete()
    {
        // Set colors of bars
        $this->chartData->setPalette(self::$eventTypes['consultation'], hex2rgb(\Config::get('color-scheme.consultation', '#33cc33')));
        $this->chartData->setPalette(self::$eventTypes['seminar'], hex2rgb(\Config::get('color-scheme.seminar', '#00ccff')));
        $this->chartData->setPalette(self::$eventTypes['meeting'], hex2rgb(\Config::get('color-scheme.meeting', '#ff6600')));
    }


    protected function sortByDate()
    {
        $range1 = new Carbon($this->dateRange[0]);
        $range2 = new Carbon($this->dateRange[1]);

        $difference = $range2->diffInDays($range1);

        $reports = [];
        if ($difference < 15) {

        } else if ($difference < 110) {

        } else if ($difference < 367) {

        } else if ($difference < 365.25 * 16 && $difference > 366) {

        }


        return $reports;
    }

    protected function textToNewLine($text)
    {
        return preg_replace("/\s/", " \n", $text);
    }

    protected function drawAllGraphic()
    {
        $tmcs = [];
        $array = [];
        $reports = $this->data;
        $seminars = [];
        $consultations = [];
        $meetings = [];

        $imgWidth = 1280;
        $imgHeight = 800;
        $this->finalWidth = $imgWidth;
        $this->finalHeight = $imgHeight;

        if ($this->chartType == 'linechart') {

            foreach ($this->data as $key => $value) {
                $array[] = $key;

                foreach ($value as $k => $v) {
                    if ($k == 'seminar') {
                        $seminars[] = count($value['seminar']);
                    } else {
                        $seminars[] = VOID;
                    }
                    if ($k == 'meeting') {
                        $meetings[] = count($value['meeting']);
                    } else {
                        $meetings[] = VOID;
                    }
                    if ($k == 'consultation') {
                        $consultations[] = count($value['consultation']);
                    } else {
                        $consultations[] = VOID;
                    }
                }

            }
           /* echo json_encode([$this->data, $array, $seminars, $meetings, $consultations]);
            die();*/
            $ordinateTitle = $this->ordinateTitle;

            $this->createImage();


            $this->chartData->addPoints($consultations, self::$eventTypes['consultation']);
            $this->chartData->addPoints($seminars, self::$eventTypes['seminar']);
            $this->chartData->addPoints($meetings, self::$eventTypes['meeting']);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = max(max($consultations), max($seminars), max($meetings));
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($array, $this->abscissaTitle);
            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            // Set colors of bars
            $this->setEventTypesPallete();

            $this->chartData->setSerieWeight(self::$eventTypes['consultation'], 2);
            $this->chartData->setSerieWeight(self::$eventTypes['seminar'], 2);
            $this->chartData->setSerieWeight(self::$eventTypes['meeting'], 2);

            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Write the chart title */
            $this->mainPicture->drawText(50, 30, $this->title, array("FontSize" => 14, "Align" => TEXT_ALIGN_BOTTOMLEFT));
            /* Write the Abscissa Title*/
            $this->mainPicture->drawText(50, $imgHeight - 30, $this->abscissaTitle, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));


            $this->mainPicture->drawRectangle(0, 0, $imgWidth, $imgHeight, array("R" => 0, "G" => 0, "B" => 0));


            /* Define the chart area */
            $this->mainPicture->setGraphArea(60, 60, $imgWidth - 50, $imgHeight - 100);

            /* Draw the scale */
            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            $scaleSettings = array("GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => false, "CycleBackground" => TRUE, 'Mode' => SCALE_MODE_MANUAL, 'Factors' => array(1), 'ManualScale' => $scaleConfig);
            $this->mainPicture->drawScale($scaleSettings);

            /* Write the chart legend */
            $this->mainPicture->drawLegend(900, $imgHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));


            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE, "DisplayColor" => DISPLAY_AUTO);
            $this->mainPicture->setShadow(FALSE);

            /* Draw the chart */
            $this->mainPicture->drawPlotChart($settings);


        } else if ($this->chartType == 'barchart') {

            if ($this->numericField == 'count') {


                foreach ($this->data as $r) {
                    $tmcs[] = $this->textToNewLine($r['tmc']->name);
                    $seminar_count = 0;
                    $cons_count = 0;
                    $meetings_count = 0;
                    foreach ($r['reports'] as $p) {
                        if ($p->initiator_id == $r['tmc']->id) {
                            if ($p->event_type == "consultation") {
                                $cons_count++;
                            } else if ($p->event_type == "meeting") {
                                $meetings_count++;
                            } else if ($p->event_type == "seminar") {
                                $seminar_count++;
                            }
                        }
                    }
                    $seminars[] = $seminar_count;
                    $consultations[] = $cons_count;
                    $meetings[] = $meetings_count;
                }


                $this->finalWidth = 1200;
                $this->finalHeight = count($tmcs) * 140 + 120;

                $this->createImage();

                /** Заполнить данными  */

                $ordinateTitle = 'Количество завершенных мероприятий(входящих)';

                $this->chartData->addPoints($consultations, self::$eventTypes['consultation']);
                $this->chartData->addPoints($seminars, self::$eventTypes['seminar']);
                $this->chartData->addPoints($meetings, self::$eventTypes['meeting']);

                $this->chartData->setAxisName(0, $ordinateTitle);
                // Get maximum value of Ordinate;
                $maxValue = max(max($consultations), max($seminars), max($meetings));
                $maxValue = ($maxValue < 10) ? 10 : $maxValue;


                $this->chartData->addPoints($tmcs, "ТМЦ");
                $this->chartData->setSerieDescription("ТМЦ", "ТМЦ");
                $this->chartData->setAbscissa("ТМЦ");

                $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
                $this->chartData->setAbscissa($this->abscissaTitle);


                // Set colors of bars
                $this->setEventTypesPallete();

            } else {


                foreach ($this->data as $r) {
                    $tmcs[] = $this->textToNewLine($r['tmc']->name);
                    $count = 0;
                    foreach ($r['reports'] as $p) {
                        if ($p->initiator_id == $r['tmc']->id) {
                            $count = $count + $p->part_count;
                        }
                    }
                    $array[] = $count;
                }

                $this->finalWidth = 1200;
                $this->finalHeight = count($tmcs) * 140 + 120;

                $this->createImage();

                /** Заполнить данными  */

                $ordinateTitle = 'Количество участников';

                $this->chartData->addPoints($array, "Количество участников");


                $this->chartData->setAxisName(0, $ordinateTitle);
                // Get maximum value of Ordinate;
                $maxValue = max($array);
                $maxValue = ($maxValue < 10) ? 10 : $maxValue;


                $this->chartData->addPoints($tmcs, "ТМЦ");
                $this->chartData->setSerieDescription("ТМЦ", "ТМЦ");
                $this->chartData->setAbscissa("ТМЦ");

                $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
                $this->chartData->setAbscissa($this->abscissaTitle);


                // Set colors of bars
                $this->setEventTypesPallete();

            }

            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            /* Draw the scale */
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));


            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));


        }


    }


    protected function drawReportGraphicInOut()
    {

        if($this->chartType == "barchart") {
            $this->finalWidth = 1200;
            $this->finalHeight = count($this->data) * 140 + 500;

            $this->createImage();
            //return $reports;
            /** Заполнить данными  */

            $numberFieldsArray = [];
            $textFieldsArray = [];


            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $textField = $item->{$this->textField};

                if (array_key_exists($this->numericField, $item)) {

                    $field = $item->{$this->numericField};

                    if (is_numeric($field)) {
                        $numericField = $field;
                    } else {
                        $numericField = 0;
                    }


                } else {
                    $numericField = 0;
                }

                $numberFieldsArray[$i] = $numericField;
                $textFieldsArray[$i] = $this->textToNewLine($textField);


            }

            $ordinateTitle = $this->ordinateTitle;

            $abscissaTitle = $this->abscissaTitle;

            $this->chartData->addPoints($numberFieldsArray, $ordinateTitle);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = count($this->data);
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($textFieldsArray, $abscissaTitle);
            $this->chartData->setSerieDescription($abscissaTitle, $abscissaTitle);
            $this->chartData->setAbscissa($abscissaTitle);

            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            /* Draw the scale */
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));


            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));
        }

        else if ($this->chartType == 'linechart') {
            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $numberFieldsArray[$i] = $item->count;
                $textFieldsArray[$i] = $this->textToNewLine($item->tmc_name);


            }

            $this->finalWidth = 1200;
            $this->finalHeight = count($textFieldsArray) * 160;

            $this->createImage();
            /* Build a dataset */

            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>10));
            for($i = 0; $i<count($numberFieldsArray); $i++)
            {
                $newarray = [];
                for($j = 0; $j<count($numberFieldsArray); $j++)
                {
                    if($i!=$j)
                    {
                        $newarray[] = VOID;
                    }
                    else{
                        $newarray[] = $numberFieldsArray[$i];
                    }
                }
                $this->chartData->addPoints($newarray,"Probe ".$i);

            }

            $this->chartData->setAxisName(0,"Кол-во событий");
            $this->chartData->addPoints($textFieldsArray,"Labels");
            $this->chartData->setSerieDescription("Labels","ТМЦ");
            $this->chartData->setAbscissa("Labels");
            /*            $this->chartData->addPoints($numberFieldsArray,"Количество совещаний");
                    $this->chartData->setAxisName(0,"Телемед. центры");
                        $this->chartData->addPoints($textFieldsArray,"Телемед. центры");

                    $this->chartData->setAbscissa("Кол-во совещаний");*/

            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            /* Create the 1st chart*/
            $this->mainPicture->setGraphArea(200,100,1000,count($textFieldsArray)*150);



            $this->mainPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
            $this->mainPicture->setShadow(TRUE,array("X"=>-1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>9));
            $this->mainPicture->drawPlotChart(array("BorderSize"=>1, "Surrounding"=>40, "BorderAlpha"=>100, "PlotSize"=>2, "PlotBorder"=>TRUE, "DisplayValues"=>TRUE, "DisplayColor"=>DISPLAY_AUTO));
            $this->mainPicture->setShadow(FALSE);




        }


    }


    protected function drawReportGraphic()
    {

        if($this->chartType == "barchart") {
            $this->finalWidth = 1200;
            $this->finalHeight = count($this->data) * 140 + 500;

            $this->createImage();
            //return $reports;
            /** Заполнить данными  */

            $numberFieldsArray = [];
            $textFieldsArray = [];


            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $textField = $item->{$this->textField};

                if (array_key_exists($this->numericField, $item)) {

                    $field = $item->{$this->numericField};

                    if (is_numeric($field)) {
                        $numericField = $field;
                    } else {
                        $numericField = 0;
                    }


                } else {
                    $numericField = 0;
                }

                $numberFieldsArray[$i] = $numericField;
                $textFieldsArray[$i] = $this->textToNewLine($textField);


            }

            $ordinateTitle = $this->ordinateTitle;

            $abscissaTitle = $this->abscissaTitle;

            $this->chartData->addPoints($numberFieldsArray, $ordinateTitle);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = count($this->data);
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($textFieldsArray, $abscissaTitle);
            $this->chartData->setSerieDescription($abscissaTitle, $abscissaTitle);
            $this->chartData->setAbscissa($abscissaTitle);

            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            /* Draw the scale */
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));


            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));
        }

        else if ($this->chartType == 'linechart') {
            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $numberFieldsArray[$i] = $item->count;
                $textFieldsArray[$i] = $this->textToNewLine($item->tmc_name);


            }

            $this->finalWidth = 1200;
            $this->finalHeight = count($textFieldsArray) * 160;

            $this->createImage();
            /* Build a dataset */

            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>10));
            for($i = 0; $i<count($numberFieldsArray); $i++)
            {
                $newarray = [];
                for($j = 0; $j<count($numberFieldsArray); $j++)
                {
                    if($i!=$j)
                    {
                        $newarray[] = VOID;
                    }
                    else{
                        $newarray[] = $numberFieldsArray[$i];
                    }
                }
                $this->chartData->addPoints($newarray,"Probe ".$i);

            }

            $this->chartData->setAxisName(0,"Кол-во событий");
            $this->chartData->addPoints($textFieldsArray,"Labels");
            $this->chartData->setSerieDescription("Labels","ТМЦ");
            $this->chartData->setAbscissa("Labels");
/*            $this->chartData->addPoints($numberFieldsArray,"Количество совещаний");
        $this->chartData->setAxisName(0,"Телемед. центры");
            $this->chartData->addPoints($textFieldsArray,"Телемед. центры");

        $this->chartData->setAbscissa("Кол-во совещаний");*/

            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
        /* Create the 1st chart*/
        $this->mainPicture->setGraphArea(200,100,1000,count($textFieldsArray)*150);



            $this->mainPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
            $this->mainPicture->setShadow(TRUE,array("X"=>-1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>9));
            $this->mainPicture->drawPlotChart(array("BorderSize"=>1, "Surrounding"=>40, "BorderAlpha"=>100, "PlotSize"=>2, "PlotBorder"=>TRUE, "DisplayValues"=>TRUE, "DisplayColor"=>DISPLAY_AUTO));
            $this->mainPicture->setShadow(FALSE);




    }


    }

    protected function drawMeetingGraphic()
    {
        if ($this->chartType == 'barchart') {
            $this->finalWidth = 1200;
            $this->finalHeight = count($this->data) * 140 + 120;

            $this->createImage();
            //return $reports;
            /** Заполнить данными  */

            $numberFieldsArray = [];
            $textFieldsArray = [];


            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $textField = $item->{$this->textField};

                if (array_key_exists($this->numericField, $item)) {

                    $field = $item->{$this->numericField};

                    if (is_numeric($field)) {
                        $numericField = $field;
                    } else {
                        $numericField = 0;
                    }


                } else {
                    $numericField = 0;
                }

                $numberFieldsArray[$i] = $numericField;
                $textFieldsArray[$i] = $this->textToNewLine($textField);


            }

            $ordinateTitle = $this->ordinateTitle;

            $abscissaTitle = $this->abscissaTitle;

            $this->chartData->addPoints($numberFieldsArray, $ordinateTitle);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = count($this->data);
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($textFieldsArray, $abscissaTitle);
            $this->chartData->setSerieDescription($abscissaTitle, $abscissaTitle);
            $this->chartData->setAbscissa($abscissaTitle);

            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            /* Draw the scale */
            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));


            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));
        } else if ($this->chartType == 'linechart') {
            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $numberFieldsArray[$i] = $item->count;
                $textFieldsArray[$i] = $this->textToNewLine($item->tmc_name);


            }

            $this->finalWidth = 1200;
            $this->finalHeight = count($textFieldsArray) * 160;

            $this->createImage();
            /* Build a dataset */

            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>10));
            for($i = 0; $i<count($numberFieldsArray); $i++)
            {
                $newarray = [];
                for($j = 0; $j<count($numberFieldsArray); $j++)
                {
                    if($i!=$j)
                    {
                        $newarray[] = VOID;
                    }
                    else{
                        $newarray[] = $numberFieldsArray[$i];
                    }
                }
                $this->chartData->addPoints($newarray,"Probe ".$i);

            }

            $this->chartData->setAxisName(0,"Кол-во совещаний");
            $this->chartData->addPoints($textFieldsArray,"Labels");
            $this->chartData->setSerieDescription("Labels","ТМЦ");
            $this->chartData->setAbscissa("Labels");
            /*            $this->chartData->addPoints($numberFieldsArray,"Количество совещаний");
                    $this->chartData->setAxisName(0,"Телемед. центры");
                        $this->chartData->addPoints($textFieldsArray,"Телемед. центры");

                    $this->chartData->setAbscissa("Кол-во совещаний");*/

            /* Create the 1st chart*/
            $this->mainPicture->setGraphArea(200,100,1000,count($textFieldsArray)*150);


            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            $this->mainPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
            $this->mainPicture->setShadow(TRUE,array("X"=>-1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>9));
            $this->mainPicture->drawPlotChart(array("BorderSize"=>1, "Surrounding"=>40, "BorderAlpha"=>100, "PlotSize"=>2, "PlotBorder"=>TRUE, "DisplayValues"=>TRUE, "DisplayColor"=>DISPLAY_AUTO));
            $this->mainPicture->setShadow(FALSE);



        }

    }

    protected function drawSeminarGraphic()
    {
        if ($this->chartType == 'linechart') {
            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $numberFieldsArray[$i] = $item->count;
                $textFieldsArray[$i] = $this->textToNewLine($item->tmc_name);


            }

            $this->finalWidth = 1200;
            $this->finalHeight = count($textFieldsArray) * 160;

            $this->createImage();
            /* Build a dataset */

            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>10));
            for($i = 0; $i<count($numberFieldsArray); $i++)
            {
                $newarray = [];
                for($j = 0; $j<count($numberFieldsArray); $j++)
                {
                    if($i!=$j)
                    {
                        $newarray[] = VOID;
                    }
                    else{
                        $newarray[] = $numberFieldsArray[$i];
                    }
                }
                $this->chartData->addPoints($newarray,"Probe ".$i);

            }

            $this->chartData->setAxisName(0,"Кол-во семинатор");
            $this->chartData->addPoints($textFieldsArray,"Labels");
            $this->chartData->setSerieDescription("Labels","ТМЦ");
            $this->chartData->setAbscissa("Labels");
            /*            $this->chartData->addPoints($numberFieldsArray,"Количество совещаний");
                    $this->chartData->setAxisName(0,"Телемед. центры");
                        $this->chartData->addPoints($textFieldsArray,"Телемед. центры");

                    $this->chartData->setAbscissa("Кол-во совещаний");*/

            /* Create the 1st chart*/
            $this->mainPicture->setGraphArea(200,100,1000,count($textFieldsArray)*150);


            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            $this->mainPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
            $this->mainPicture->setShadow(TRUE,array("X"=>-1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>9));
            $this->mainPicture->drawPlotChart(array("BorderSize"=>1, "Surrounding"=>40, "BorderAlpha"=>100, "PlotSize"=>2, "PlotBorder"=>TRUE, "DisplayValues"=>TRUE, "DisplayColor"=>DISPLAY_AUTO));
            $this->mainPicture->setShadow(FALSE);



        }
        else if($this->chartType == 'barchart')
        {
            $this->finalWidth = 1200;
            $this->finalHeight = count($this->data) * 140 + 120;

            $this->createImage();
            //return $reports;
            /** Заполнить данными  */

            $numberFieldsArray = [];
            $textFieldsArray = [];


            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $textField = $item->{$this->textField};

                if (array_key_exists($this->numericField, $item)) {

                    $field = $item->{$this->numericField};

                    if (is_numeric($field)) {
                        $numericField = $field;
                    } else {
                        $numericField = 0;
                    }


                } else {
                    $numericField = 0;
                }

                $numberFieldsArray[$i] = $numericField;
                $textFieldsArray[$i] = $this->textToNewLine($textField);


            }
            /* echo json_encode([$this->data, $this->numericField, $this->textField, $numberFieldsArray, $textFieldsArray]);
             ;*/

            $ordinateTitle = $this->ordinateTitle;

            $abscissaTitle = $this->abscissaTitle;

            $this->chartData->addPoints($numberFieldsArray, $ordinateTitle);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = count($this->data);
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($textFieldsArray, $abscissaTitle);
            $this->chartData->setSerieDescription($abscissaTitle, $abscissaTitle);
            $this->chartData->setAbscissa($abscissaTitle);

            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            /* Draw the scale */
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));

            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));
        }

    }

    protected function drawConsultationGraphic()
    {
        if ($this->chartType == 'linechart') {
            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];


                $numberFieldsArray[$i] = $item->count;
                $textFieldsArray[$i] = $this->textToNewLine($item->tmc_name);


            }

            $this->finalWidth = 1200;
            $this->finalHeight = count($textFieldsArray) * 160;

            $this->createImage();
            /* Build a dataset */

            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>10));
            for($i = 0; $i<count($numberFieldsArray); $i++)
            {
                $newarray = [];
                for($j = 0; $j<count($numberFieldsArray); $j++)
                {
                    if($i!=$j)
                    {
                        $newarray[] = VOID;
                    }
                    else{
                        $newarray[] = $numberFieldsArray[$i];
                    }
                }
                $this->chartData->addPoints($newarray,"Probe ".$i);

            }

            $this->chartData->setAxisName(0,"Кол-во консультаций");
            $this->chartData->addPoints($textFieldsArray,"Labels");
            $this->chartData->setSerieDescription("Labels","ТМЦ");
            $this->chartData->setAbscissa("Labels");
            /*            $this->chartData->addPoints($numberFieldsArray,"Количество совещаний");
                    $this->chartData->setAxisName(0,"Телемед. центры");
                        $this->chartData->addPoints($textFieldsArray,"Телемед. центры");

                    $this->chartData->setAbscissa("Кол-во совещаний");*/

            /* Create the 1st chart*/
            $this->mainPicture->setGraphArea(200,100,1000,count($textFieldsArray)*150);


            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            $this->mainPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
            $this->mainPicture->setShadow(TRUE,array("X"=>-1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $this->mainPicture->setFontProperties(array("FontName"=>public_path("fonts/arial/arial.ttf"),"FontSize"=>9));
            $this->mainPicture->drawPlotChart(array("BorderSize"=>1, "Surrounding"=>40, "BorderAlpha"=>100, "PlotSize"=>2, "PlotBorder"=>TRUE, "DisplayValues"=>TRUE, "DisplayColor"=>DISPLAY_AUTO));
            $this->mainPicture->setShadow(FALSE);



        }
        else if($this->chartType == "barchart")
        {
            $this->finalWidth = 1200;
            $this->finalHeight = count($this->data) * 140 + 120;

            $this->createImage();
            //return $reports;
            /** Заполнитhь данными  */

            $numberFieldsArray = [];
            $textFieldsArray = [];


            for ($i = 0; $i < count($this->data); $i++) {
                $item = $this->data[$i];

                $textField = $item->{$this->textField};

                if (array_key_exists($this->numericField, $item)) {

                    $field = $item->{$this->numericField};

                    if (is_numeric($field)) {
                        $numericField = $field;
                    } else {
                        $numericField = 0;
                    }

                } else {
                    $numericField = 0;
                }

                $numberFieldsArray[$i] = $numericField;
                $textFieldsArray[$i] = $this->textToNewLine($textField);

            }


            $ordinateTitle = $this->ordinateTitle;

            $abscissaTitle = $this->abscissaTitle;

            $this->chartData->addPoints($numberFieldsArray, $ordinateTitle);

            $this->chartData->setAxisName(0, $ordinateTitle);
            // Get maximum value of Ordinate;
            $maxValue = count($this->data);
            $maxValue = ($maxValue < 10) ? 10 : $maxValue;


            $this->chartData->addPoints($textFieldsArray, $abscissaTitle);
            $this->chartData->setSerieDescription($abscissaTitle, $abscissaTitle);
            $this->chartData->setAbscissa($abscissaTitle);

            $this->chartData->setSerieDescription($this->abscissaTitle, $this->abscissaTitle);
            $this->chartData->setAbscissa($this->abscissaTitle);


            /* Add a border to the picture */
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
            $this->mainPicture->drawGradientArea(0, 0, $this->finalWidth, $this->finalHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


            /* Define the chart area */

            $this->mainPicture->setGraphArea(160, 60, $this->finalWidth - 50, $this->finalHeight - 60);

            $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
            $this->mainPicture->drawText(300,15," от ".$this->dateRange[0]->format('Y-m-d')." до ".$this->dateRange[1]->format('Y-m-d'),array("FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLEMIDDLE));
            /* Draw the scale */
            $this->mainPicture->drawScale(array("TickAlpha" => 1000, 'DrawSubTicks' => FALSE, "CycleBackground" => TRUE, "GridR" => 0, "GridG" => 0, "GridB" => 0, "GridAlpha" => 10, 'Factors' => array(1), "Pos" => SCALE_POS_TOPBOTTOM, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $scaleConfig));


            /* Write the chart legend */
            $this->mainPicture->drawLegend($this->finalWidth - 360, $this->finalHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

            /* Turn on shadow computing */
            $this->mainPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

            $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
            /* Draw the chart */
            $this->mainPicture->drawBarChart(array("DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => TRUE, "Rounded" => TRUE, "Surrounding" => 30));
        }

    }


}
