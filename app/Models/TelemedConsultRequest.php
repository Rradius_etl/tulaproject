<?php

namespace App\Models;
 
use Eloquent as Model;
use App\Models\Admin\EventFilePivot;
/**
 * @SWG\Definition(
 *      definition="TelemedConsultRequest",
 *      required={""},
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="comment",
 *          description="comment",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uid_code",
 *          description="uid_code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="uid_year",
 *          description="uid_year",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="uid_id",
 *          description="uid_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="decision",
 *          description="decision",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telemed_center_id",
 *          description="telemed_center_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class TelemedConsultRequest extends Model
{

    public $table = 'telemed_consult_requests';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'request_id',
        'comment',
        'uid_code',
        'uid_year',
        'uid_id',
        'decision',
        'telemed_center_id',
        'decision_at',
        'med_care_fac_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'comment' => 'string',
        'uid_code' => 'integer',
        'uid_year' => 'integer',
        'uid_id' => 'integer',
        'decision' => 'string',
        'telemed_center_id' => 'integer',
        'completed' => 'boolean'
    ];

    protected $primaryKey = "request_id";
    public $incrementing = false;
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getFullUid()
    {
        return $this->uid_code."-".$this->uid_year."-".$this->uid_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function medCareFac()
    {
        return $this->belongsTo(\App\Models\Admin\MedCareFac::class,'med_care_fac_id','id');
    }
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class, 'telemed_center_id', 'id');
    }
    public function request()
    {

        return $this->belongsTo(\App\Models\Admin\TelemedRequest::class);
    }

    public function abonentSide()
    {
        return $this->belongsTo(TelemedConsultAbonentSide::class,'request_id', 'request_id');
    }

    public function getUID()
    {
        return implode([$this->uid_code, $this->uid_year, $this->uid_id], '-');

    }
    public function files()
    {
        return $this->hasMany(EventFilePivot::class,'request_id','request_id');
    }
    public function consultantSide()
    {
        return $this->belongsTo(\App\Models\TelemedConsultConsultantSide::class,'request_id','request_id');

    }
}
