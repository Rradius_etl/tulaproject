<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 09.12.2016
 * Time: 20:16
 */

namespace App\Models;


use App\Models\Admin\TelemedRequest;
use Cmgmyr\Messenger\Models\Thread;

class MessagesThread extends Thread
{
    protected $fillable = ['subject', 'request_id'];

    protected $casts = [
        'request_id' => 'integer'
    ];

    public function request()
    {
        //if ( $this->request_id == null ) return null;

        return $this->belongsTo(TelemedRequest::class,"request_id","id");
    }
}