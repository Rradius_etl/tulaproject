<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Doctor",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surname",
 *          description="surname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="middlename",
 *          description="middlename",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="spec",
 *          description="spec",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telemed_center_id",
 *          description="telemed_center_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Doctor extends Model
{

    public $table = 'doctors';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'surname',
        'middlename',
        'spec',
        'telemed_center_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'surname' => 'string',
        'middlename' => 'string',
        'spec' => 'string',
        'telemed_center_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }
}
