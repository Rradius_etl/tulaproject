<?php

namespace App\Models;

use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedRequest;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="TelemedCenter",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="med_care_fac_id",
 *          description="med_care_fac_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="director_fullname",
 *          description="director_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_fullname",
 *          description="coordinator_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_phone",
 *          description="coordinator_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_fullname",
 *          description="tech_specialist_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_phone",
 *          description="tech_specialist_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_contacts",
 *          description="tech_specialist_contacts",
 *          type="string"
 *      )
 * )
 */
class TelemedCenter extends Model
{

    public $table = 'telemed_centers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    use SoftDeletes;


    public $fillable = [
        'name',
        'director_fullname',
        'coordinator_fullname',
        'coordinator_phone',
        'tech_specialist_fullname',
        'tech_specialist_phone',
        'tech_specialist_contacts',
        'videoconf_equipment',
        'digit_img_demonstration',
        'equipment_location',
        'address',
        'terminal_name',
        'terminal_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'med_care_fac_id' => 'integer',
        'name' => 'string',
        'director_fullname' => 'string',
        'coordinator_fullname' => 'string',
        'coordinator_phone' => 'string',
        'tech_specialist_fullname' => 'string',
        'tech_specialist_phone' => 'string',
        'tech_specialist_contacts' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Admin\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function telemedCenterDescription()
    {
        return $this->hasOne(\App\Models\Admin\TelemedCenterDescription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedConsultRequests()
    {
        return $this->hasMany(\App\Models\Admin\TelemedConsultRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedRequestsMcfAbonents()
    {
        return $this->hasMany(\App\Models\Admin\TelemedRequestsMcfAbonent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedUserPivots()
    {
        return $this->hasMany(\App\Models\Admin\TelemedUserPivot::class);
    }

    public function medCareFac()
    {
        return $this->belongsTo(MedCareFac::class, 'med_care_fac_id', 'id');
    }

    protected static function boot() {
        parent::boot();
        static::created(function($tmc)
        {
            \TmcRoleManager::clearCacheForTMCUsers($tmc->id);
        });
        static::deleted(function($tmc) { // before delete() method call this
            \TmcRoleManager::clearCacheForTMCUsers($tmc->id);

            $reqs = TelemedRequest::where(function($query) use($tmc)
            {
                $query->where(['telemed_center_id'=>$tmc->id])
                    ->orWhere(function($q) use($tmc)
                    {
                        $q->whereExists(function($q)use($tmc)
                        {
                            $q->select(\DB::raw('1'))->from('telemed_consult_requests as tcr')->where('tcr.request_id',\DB::raw('telemed_event_requests.id'))
                                ->where('tcr.telemed_center_id',$tmc->id);
                        });
                    });
            })->where('canceled',false)
                ->whereNotExists(function($q)
                {
                    $q->select(\DB::raw('1'))->from('telemed_event_conclusion as tec')->where('tec.request_id',\DB::raw('telemed_event_requests.id'));
                })->get();
            foreach($reqs as $req)
            {
                $parts = $req->telemedEventParticipants()->get()->pluck('telemed_center_id')->toArray();
                $cons = $req->getConsultation;
                if($cons != null)
                {
                    if($cons->telemed_center_id != null)
                    {
                        array_push($parts,$cons->telemed_center_id);
                    }
                }
                if(!in_array($req->telemed_center_id,$parts))
                {
                    array_push($parts,$req->telemed_center_id);
                }
                $message = "Администратор портала удалил ТМЦ ".$tmc->name.", и поэтому незавершенная(ое) ".trans('backend.'.$req->event_type). " созданное этим ТМЦ ".$req->title." будет отменено";
                \NotificationManager::createNotificationForManyTmc($message,$parts,false,true,$tmc->id);
                $req->canceled = true;
                $req->save();
            }
            // do the rest of the cleanup...
        });
        static::restored(function($tmc) {
            \TmcRoleManager::clearCacheForTMCUsers($tmc->id);
        });
        static::updated(function($tmc) {
            \TmcRoleManager::clearCacheForTMCUsers($tmc->id);
        });
        static::updating(function($tmc) {
            \TmcRoleManager::clearCacheForTMCUsers($tmc->id);
        });
    }

}
