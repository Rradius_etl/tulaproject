<?php

namespace App\Models;

use App\Models\Report;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpWord\PhpWord;
use DB;

class TableReport extends Model
{
    public $name;
    public $rows;
    public $type;
    public $tmcs;

    public $range;

    public $data;

    public function __construct(Report $report)
    {
        $this->name = $report->name;
        $this->rows = json_decode($report->headers);
        $this->tmcs = json_decode($report->tmcs);
        $this->type = $report->type;
    }

    public function render($range)
    {
        $this->range = $range;

        $this->getData();

        $data = [
            'reports' => $this->data,
            'headers' => $this->rows,
            'full' => false,
            'title' => $this->name,
            'allHeaders' => \Config::get("reports.rows.{$this->type}")
        ];
        /* return $data;*/
        return view('reports.partial.' . $this->type, $data);

    }

    public function download($range, $format)
    {
        $self = $this;
        $this->range = $range;

        $fileName = "{$this->name} {$range[0]}-{$range[1]}";
        $this->getData();


        /** Render Excel file or CSV */
        if ($format == 'xls' || $format == 'csv') {

            return \Excel::create($fileName, function ($excel) {

                $excel->sheet('Страница 1', function ($sheet) {
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'DejaVu Sans',
                            'size' => 8,
                            'bold' => false
                        )
                    ));
                    $sheet->loadView('reports.partial.' . $this->type)
                        ->with('reports', $this->data)
                        ->with('headers', $this->rows)
                        ->with('full', true)
                        ->with('title', $this->name)
                        ->with('allHeaders', \Config::get("reports.rows.{$this->type}"));

                });

            })->export($format);


        } elseif ($format == 'pdf') {

            $data = [
                'reports' => $this->data,
                'headers' => $this->rows,
                'full' => true,
                'title' => $this->name,
                'allHeaders' => \Config::get("reports.rows.{$this->type}")
            ];

            $html = view('reports.partial.' . $this->type, $data);

            $pdf = \PDF::loadHTML($html);
            $pdf->setOption('encoding', 'utf-8');
            $pdf->setOrientation('landscape');

            return $pdf->download($fileName . '.pdf');

        } elseif ($format == 'docx'|| $format == 'odt') {

            $params = [
                'type' => $this->type,
                'reports' => $this->data,
                'rows' => $this->rows,
                'name' => $this->name,
                'dateRange' => $this->range,
                'fileFormat' => $format,
            ];
            //return $params;
            $docxFile = new DocxReport($params);
            $docxFile->download();


        }
    }

    public function getData()
    {
        $report_type = $this->type;

        switch ($report_type) {
            case 'report':
                $this->getReportData();
                break;
            case 'reportinout':
                $this->getInOutReportData();
                break;
            case 'meeting':
                $this->getMeetingData();
                break;
            case 'seminar':
                $this->getSeminarData();
                break;
            case 'consultation':
                $this->getConsultationData();
                break;
            case 'all':
                $this->getAllData();
                break;
        }
    }


    /**
     * Отчет по совещаниям в разрезе ТМЦ и
     * Отчет по обучающим семинарам в разрезе ТМЦ
     *
     */
    protected function getSeminarData()
    {
        $range = $this->range;

        $data = \DB::table("telemed_centers as tmc")->join("telemed_event_requests as ter", "tmc.id", "=", "ter.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->where('ter.event_type', "seminar")
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('telemed_event_participants as tep')
                    ->whereRaw('tep.request_id = ter.id and tep.decision = "agreed" and tep.telemed_center_id != ter.telemed_center_id');
            })
            ->whereBetween('ter.start_date', $range)
            ->select('tmc.terminal_name', 'tmc.terminal_number', "mcf.id as mcf_id", "mcf.name as mcf_name", 'tmc.name as tmc_name', "tmc.address as tmc_address", \DB::raw("count(ter.id) as count"), \DB::raw("count(case ter.importance when 'low' then 1 else null end) as low_count"), \DB::raw("count(case ter.importance when 'high' then 1 else null end) as high_count"), \DB::raw("count(case ter.importance when 'medium' then 1 else null end) as medium_count"), 'ter.event_type')->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');
        if (count($this->tmcs) > 0) {

            $data = $data->whereIn("tmc.id", $this->tmcs);
        }
        $this->data = $data->get();
        //return $reports;

    }

    /**
     * Отчет по совещаниям в разрезе ТМЦ и
     * Отчет по обучающим семинарам в разрезе ТМЦ
     *
     */
    protected function getMeetingData()
    {
        $range = $this->range;

        $data = \DB::table("telemed_centers as tmc")
            ->join("telemed_event_requests as ter", "tmc.id", "=", "ter.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id") /* todo */
            ->where('ter.event_type', "meeting")
            ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                      ->from('telemed_event_participants as tep')
                      ->whereRaw('tep.request_id = ter.id and tep.decision = "agreed" and tep.telemed_center_id != ter.telemed_center_id');
            })
            ->whereBetween('ter.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                "tmc.address as tmc_address",
                \DB::raw("count(ter.id) as count"),
                \DB::raw("count(case ter.importance when 'low' then 1 else null end) as low_count"),
                \DB::raw("count(case ter.importance when 'high' then 1 else null end) as high_count"),
                \DB::raw("count(case ter.importance when 'medium' then 1 else null end) as medium_count"),
                'ter.event_type')
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');
        if (count($this->tmcs) > 0) {
            $data = $data->whereIn("tmc.id", $this->tmcs);
        }

        //echo json_encode($data->get()); die();
        $this->data = $data->get();


    }


    /**
     * Отчет по видеоконференциям по отдельному ТМЦ
     * route: /admin/mc_report/{tmc_id}
     * (доступен администратору портала и администратору ТМЦ)
     */
    protected function getAllData()
    {
        $range = $this->range;
        $tmcs = TelemedCenter::whereIn('id', $this->tmcs)->get();

        $reportsarray = [];
        foreach ($tmcs as $tmc) {
            $tmc_id = $tmc->id;
            $obj = [];
            $obj['tmc'] = $tmc;
            $obj['mcf'] = $tmc->medCareFac;

            $reports = \DB::table('telemed_event_requests as ter')->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
                ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
                ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
                ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
                ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
                ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
                ->where(function ($query) use ($tmc_id) {
                    $query->where(function ($q) use ($tmc_id) {
                        $q->where("tep.telemed_center_id", "=",$tmc_id);
                        $q->where("ter.telemed_center_id","!=", DB::raw("tep.telemed_center_id"));
                        $q->where('tep.decision','agreed');
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where("ter.telemed_center_id", "=",$tmc_id);
                        $q->where("ter.telemed_center_id","!=", DB::raw("tep.telemed_center_id"));
                        $q->where('tep.decision','agreed');
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where(function ($q1) use ($tmc_id) {
                            $q1->whereNotNull('abon.request_id');
                            $q1->whereNotNull('cons.request_id');
                            $q1->where(function ($q2) use ($tmc_id) {
                                $q2->where('ter.telemed_center_id', $tmc_id);
                                $q2->orWhere("tcr.telemed_center_id", $tmc_id);
                            });
                            $q1->where("ter.telemed_center_id","!=",\DB::raw("tcr.telemed_center_id"));
                            $q1->whereNotNull('tcr.telemed_center_id');
                            $q1->where('ter.event_type', 'consultation');
                        });
                        $q->where('tcr.decision', 'agreed');
                    });
                })
                ->whereBetween('ter.start_date', $range)
                ->select('ter.id',
                    'ter.telemed_center_id as initiator_id',
                    "ter.event_type",
                    'ter.start_date',
                    'types.name as consult_type',
                    \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"),
                    \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"),
                     'cons_tmc.name as cons_tmc_name',
                    'abon.patient_indications_for_use_id',
                    'abon.patient_indications_for_use_other',
                    'ter.title',
                    \DB::raw("COUNT(tep.id) as part_count"),
                    'ter.importance',
                    'abon.created_at as create_date',
                    'abon.doctor_fullname as medic')
                ->groupBy("ter.id")
                ->get();

            if (count($reports) != 0) {
                $obj['reports'] = $reports;
                array_push($reportsarray, $obj);
            }

        }
        $this->data = $reportsarray;
        /*     echo json_encode($this->data);
             die();*/

    }


    /** Отчет о проведенных видеоконференциях в разрезе ТМЦ
     * /admin/reports
     *  */
    protected function getConsultationData()
    {
        $range = $this->range;

        $data = \DB::table("telemed_centers as tmc")
            ->join("telemed_event_requests as tcr", "tmc.id", "=", "tcr.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->join('telemed_consult_requests as tc',"tc.request_id","=","tcr.id")
            ->join("telemed_requests_mcf_abonent as abon", "abon.request_id", "=", "tcr.id")
            ->join("telemed_requests_mcf_consultant as tcc", "tcc.request_id", "=", "tcr.id")
            ->where(function($qq){
                $qq->where(function ($q)
                {
                    $q->where("tc.telemed_center_id","!=",\DB::raw("tcr.telemed_center_id"));
                    $q->where("tc.decision","=","agreed");
                    $q->where("tcr.event_type","=","consultation");
                });
            })
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                "tmc.address as tmc_address",
                \DB::raw("count(tcr.id) as count"),
                "mcf.consultations_norm as cons_norm")
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');



        if (count($this->tmcs) > 0) {

            $data = $data->whereIn("tmc.id", $this->tmcs);
        }
        $this->data = $data->get();
    }

    protected function getInOutReportData()
    {
        $range = $this->range;

        $out = \DB::table("telemed_centers as tmc")
            ->join("telemed_event_requests as tcr", "tmc.id", "=", "tcr.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->leftJoin('telemed_consult_requests as tc',"tc.request_id","=","tcr.id")
            ->leftJoin("telemed_requests_mcf_abonent as abon", "abon.request_id", "=", "tcr.id")
            ->leftJoin("telemed_requests_mcf_consultant as tcc", "tcc.request_id", "=", "tcr.id")
            ->where(function($qq){
                $qq->where(function($q){
                    $q->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('telemed_event_participants as tep')
                            ->where(function ($qq){
                                $qq ->whereRaw('tep.request_id = tcr.id and tep.decision = "agreed" and tep.telemed_center_id != tcr.telemed_center_id');
                            });
                    });

                    $q->whereIn("tcr.event_type",['meeting','seminar']);
                });
                $qq->orWhere(function ($q)
                {
                    $q->whereNotNull("abon.request_id");
                    $q->whereNotNull("tcc.request_id");
                    $q->where(function($qq){
                        $qq->where(['tcr.telemed_center_id'=>\DB::raw("tmc.id")]);
                        $qq->where("tcr.telemed_center_id","!=",DB::raw("tc.telemed_center_id"));
                    });
                    $q->where("tc.decision","=","agreed");
                    $q->where("tcr.event_type","=","consultation");
                });
            })
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                'tmc.id as tmc_id',
                "tmc.address as tmc_address",
                \DB::raw("count(tcr.id) as count"),
                \DB::raw("count(case tcr.event_type when 'consultation' then 1 else null end) as consult_count"),
                \DB::raw("count(case tcr.event_type when 'meeting' then 1 else null end) as meeting_count"),
                \DB::raw("count(case tcr.event_type when 'seminar' then 1 else null end) as seminar_count"),
                "mcf.consultations_norm as cons_norm")
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');


        $in1 = \DB::table("telemed_centers as tmc")
            ->join("telemed_event_participants as tep", "tmc.id", "=", "tep.telemed_center_id")
            ->join("telemed_event_requests as tcr", "tep.request_id", "=", "tcr.id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->where(function($qq){
                $qq->where(function($q){
                    $q->where(function ($qq2){
                        $qq2->where("tep.telemed_center_id","!=", \DB::raw("tcr.telemed_center_id"));
                        $qq2->where("tep.decision","agreed");
                    });

                    $q->whereIn("tcr.event_type",['meeting','seminar']);
                });
            })
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                'tmc.id as tmc_id',
                "tmc.address as tmc_address",
                \DB::raw("count(tcr.id) as count"),
                \DB::raw("count(case tcr.event_type when 'consultation' then 1 else null end) as consult_count"),
                \DB::raw("count(case tcr.event_type when 'meeting' then 1 else null end) as meeting_count"),
                \DB::raw("count(case tcr.event_type when 'seminar' then 1 else null end) as seminar_count"),
                "mcf.consultations_norm as cons_norm")
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');

        $in = \DB::table("telemed_centers as tmc")
            ->join('telemed_consult_requests as tc',"tc.telemed_center_id","=","tmc.id")
            ->join("telemed_event_requests as tcr", "tcr.id", "=", "tc.request_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->leftJoin("telemed_requests_mcf_abonent as abon", "abon.request_id", "=", "tcr.id")
            ->leftJoin("telemed_requests_mcf_consultant as tcc", "tcc.request_id", "=", "tcr.id")
            ->where(function($q){
                $q->where(function ($q1){
                    $q1->where("tc.telemed_center_id","!=",DB::raw("tcr.telemed_center_id"));
                    $q1->where("tc.decision","=","agreed");
                    $q1->whereNotNull("abon.request_id");
                    $q1->whereNotNull("tcc.request_id");
                });
            })
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                'tmc.id as tmc_id',
                "tmc.address as tmc_address",
                \DB::raw("count(tcr.id) as count"),
                \DB::raw("count(case tcr.event_type when 'consultation' then 1 else null end) as consult_count"),
                \DB::raw("count(case tcr.event_type when 'meeting' then 1 else null end) as meeting_count"),
                \DB::raw("count(case tcr.event_type when 'seminar' then 1 else null end) as seminar_count"),
                "mcf.consultations_norm as cons_norm")
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');

        $data = $out->unionAll($in)->unionAll($in1);
        if (count($this->tmcs)) {
            $data = $data->whereIn("tmc.id", $this->tmcs);
        }
        $data = collect($data->get());

        $data = $data->groupBy("tmc_id");
        $array = [];
        $oldobj = null;
        foreach ($data as $row)
        {
            $i = 0;
            foreach ($row as $row1)
            {
                if($oldobj!=null)
                {
                    $oldobj->count = $row1->count+ $oldobj->count;
                    $oldobj->consult_count = $oldobj->consult_count +$row1->consult_count;
                    $oldobj->meeting_count = $oldobj->meeting_count +$row1->meeting_count;
                    $oldobj->seminar_count = $oldobj->seminar_count +$row1->seminar_count;
                }
                if($i == 0)
                {
                    $oldobj = $row1;
                }
                $i++;
            }
            if($oldobj == null)
            {

            }
            $array[] = $oldobj;
            $oldobj = null;
        }
        $this->data = $array;
    }

    protected function getReportData()
    {
        $range = $this->range;
        $data = \DB::table("telemed_centers as tmc")
            ->join("telemed_event_requests as tcr", "tmc.id", "=", "tcr.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->leftJoin('telemed_consult_requests as tc',"tc.request_id","=","tcr.id")
            ->leftJoin("telemed_requests_mcf_abonent as abon", "abon.request_id", "=", "tcr.id")
            ->leftJoin("telemed_requests_mcf_consultant as tcc", "tcc.request_id", "=", "tcr.id")
            ->where(function($qq){
                $qq->where(function($q){
                    $q->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('telemed_event_participants as tep')
                            ->whereRaw('tep.request_id = tcr.id and tep.decision = "agreed" and tep.telemed_center_id != tcr.telemed_center_id');
                    });
                    $q->whereIn("tcr.event_type",['meeting','seminar']);
                });
                $qq->orWhere(function ($q)
                {
                    $q->whereNotNull("abon.request_id");
                    $q->whereNotNull("tcc.request_id");
                    $q->where("tc.telemed_center_id","!=",\DB::raw("tcr.telemed_center_id"));
                    $q->where("tc.decision","=","agreed");
                    $q->where("tcr.event_type","=","consultation");
                });
            })
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name',
                'tmc.terminal_number',
                "mcf.id as mcf_id",
                "mcf.name as mcf_name",
                'tmc.name as tmc_name',
                "tmc.address as tmc_address",
                \DB::raw("count(tcr.id) as count"),
                \DB::raw("count(case tcr.event_type when 'consultation' then 1 else null end) as consult_count"),
                \DB::raw("count(case tcr.event_type when 'meeting' then 1 else null end) as meeting_count"),
                \DB::raw("count(case tcr.event_type when 'seminar' then 1 else null end) as seminar_count"),
                "mcf.consultations_norm as cons_norm")
            ->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');

        if (count($this->tmcs)) {
            $data = $data->whereIn("tmc.id", $this->tmcs);
        }

        $this->data = $data->get();
    }


}
