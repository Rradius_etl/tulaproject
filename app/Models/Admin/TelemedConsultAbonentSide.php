<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="TelemedConsultAbonentSide",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="telemed_id",
 *          description="telemed_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_fullname",
 *          description="doctor_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="doctor_spec",
 *          description="doctor_spec",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="consult_type",
 *          description="consult_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="medical_care_fac_id",
 *          description="medical_care_fac_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="desired_consultant",
 *          description="desired_consultant",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="desired_date",
 *          description="desired_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="patient_uid_or_fname",
 *          description="patient_uid_or_fname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_birthday",
 *          description="patient_birthday",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="patient_gender",
 *          description="patient_gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_address",
 *          description="patient_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_social_status",
 *          description="patient_social_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_indications_for_use",
 *          description="patient_indications_for_use",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="patient_questions",
 *          description="patient_questions",
 *          type="string"
 *      )
 * )
 */
class TelemedConsultAbonentSide extends Model
{

    public $table = 'telemed_requests_mcf_abonent';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = "request_id";

    public $fillable = [
        'request_id',
        'telemed_id',
        'doctor_fullname',
        'doctor_spec',
        'consult_type',
        'medical_profile',
        'medical_care_fac_id',
        'desired_consultant',
        'desired_date',
        'desired_time',
        'patient_uid_or_fname',
        'patient_birthday',
        'patient_gender',
        'patient_address',
        'patient_social_status',
        'patient_indications_for_use',
        'patient_indications_for_use_id',
        'patient_questions'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'integer',
        'telemed_id' => 'integer',
        'medical_profile' => 'integer',
        'doctor_fullname' => 'string',
        'doctor_spec' => 'string',
        'consult_type' => 'integer',
        'medical_care_fac_id' => 'integer',
        'desired_consultant' => 'string',
        'desired_date' => 'date nullable',
        'patient_uid_or_fname' => 'string',
        'patient_birthday' => 'date',
        'patient_gender' => 'string',
        'patient_address' => 'string',
        'patient_social_status' => 'integer',
        'patient_indications_for_use' => 'string',
        'patient_indications_for_use_id' => 'integer',
        'patient_questions' => 'string',
        'completed' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function getPatientIndicationsForUseIdAttribute($value)
    {
        if ( $value == 0 || $value == null) {
            return 'other';
        } else {
            return $value;
        }
    }
    public function setPatientIndicationsForUseIdAttribute($value)
    {
        if ( $value == 'other' ) {
            $this->attributes['patient_indications_for_use_id'] = 0;
        } else {
            $this->attributes['patient_indications_for_use_id'] = $value;
        }
    }

    public function setDesiredDateAttribute($value)
    {
        if( $value == "" || $value == null) {
            $this->attributes['desired_date'] = null;
        } else {
            $this->attributes['desired_date'] = $value;
        }
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function medCareFac()
    {
        return $this->belongsTo(\App\Models\Admin\MedCareFac::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

    public function TelemedConsultConsultantSide()
    {
        return $this->belongsTo(\App\Models\TelemedConsultConsultantSide::class,"request_id","request_id");
    }

    public function request()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedRequest::class);
    }

    public function cons_request()
    {
        return $this->hasOne(\App\Models\TelemedConsultRequest::class,"request_id","request_id");
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }

    public function socialStatus()
    {
        return $this->belongsTo(SocialStatus::class,"patient_social_status","id");
    }

    public function medicalProfile()
    {
        return $this->belongsTo(MedicalProfile::class, 'medical_profile', 'id');
    }
}
