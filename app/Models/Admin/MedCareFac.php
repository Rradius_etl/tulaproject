<?php

namespace App\Models\Admin;
use App\Models\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sentinel;

/**
 * @SWG\Definition(
 *      definition="MedCareFac",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="med_care_spec_id",
 *          description="med_care_spec_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      )
 * )
 */
class MedCareFac extends Model
{

    public $table = 'med_care_facs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    use SoftDeletes;

    public $fillable = [
        'name',
        'code',
        'user_id',
        'consultations_norm'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'user_id'=>"integer nullable",
        'consultations_norm' => "integer"
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function telemedCenters()
    {
        return $this->hasMany(\App\Models\TelemedCenter::class);
    }

    public function medCareFacDescription()
    {
        return $this->hasOne(\App\Models\Admin\MedCareFacDescription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedRequestsMcfAbonents()
    {
        return $this->hasMany(\App\Models\Admin\TelemedRequestsMcfAbonent::class);
    }


    protected static function boot() {
        parent::boot();

        static::deleted(function($mcf) { // before delete() method call this
            $tmcs = $mcf ->telemedCenters()->get();
            foreach($tmcs as $t)
            {
                $t->delete();
            }
            \TmcRoleManager::clearCacheForUser($mcf->user_id);
            if($mcf->user != null)
            {
                $user = $mcf->user;
                $role = \Sentinel::getRoleRepository()->findBySlug("mcf_admin");
                $roles = $user->roles->pluck("slug");
                if($roles != null  && !in_array("mcf_admin",$roles))
                {
                    $user->roles()->attach($role);
                }

            }
            // do the rest of the cleanup...
        });
        static::created(function($mcf) { // before delete() method call this
            \TmcRoleManager::clearCacheForUser($mcf->user_id);
            // do the rest of the cleanup...
            $user = $mcf->user;
            if($user != null)
            {
                $roles = $user->roles->pluck("slug");
                $role = \Sentinel::getRoleRepository()->findBySlug("mcf_admin");
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'Админ ЛПУ',
                        'slug' => 'mcf_admin',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if($roles != null  && !in_array("mcf_admin",$roles))
                {
                    $user->roles()->attach($role);
                }

            }
        });
        static::restored(function($mcf) {
            $mcf->telemedCenters()->withTrashed()->get()
                ->each(function($subfolder) {
                    $subfolder->restore();
                });
            \TmcRoleManager::clearCacheForUser($mcf->user_id);
            $user = $mcf->user;
            if($user != null)
            {
                $roles = $user->roles->pluck("slug");
                $role = \Sentinel::getRoleRepository()->findBySlug("mcf_admin");
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'Админ ЛПУ',
                        'slug' => 'mcf_admin',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if($roles != null  && !in_array("mcf_admin",$roles))
                {
                    $user->roles()->attach($role);
                }
            }
        });
        static::updated(function($mcf) {
            \TmcRoleManager::clearCacheForUser($mcf->user_id);
            $user = $mcf->user;
            if($user != null)
            {
                $roles = $user->roles->pluck("slug");
                $role = \Sentinel::getRoleRepository()->findBySlug("mcf_admin");
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'Админ ЛПУ',
                        'slug' => 'mcf_admin',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if($roles != null  && !in_array("mcf_admin",$roles->toArray()))
                {
                    $user->roles()->attach($role);
                }

            }

        });
        static::updating(function($mcf) {
            \TmcRoleManager::clearCacheForUser($mcf->user_id);
            $current = MedCareFac::find($mcf->id);
            if($current->user_id != $mcf->user_id)
            {
                if($current->user_id != null)
                {
                    $user = $current->user;
                    $role = \Sentinel::getRoleRepository()->findBySlug("mcf_admin");
                    $roles = $user->roles->pluck("slug");
                    if($role == null)
                    {
                        $roleModel = [
                            'name' => 'Админ ЛПУ',
                            'slug' => 'mcf_admin',
                            'permissions' => [
                            ]
                        ];
                        $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                    }
                    if($roles != null  && in_array("mcf_admin",$roles->toArray()))
                    {
                        $user->roles()->detach($role);
                    }

                }
            }
        });
    }



}
