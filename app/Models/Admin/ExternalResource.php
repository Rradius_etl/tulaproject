<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="ExternalResource",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="link",
 *          description="link",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="order",
 *          description="order",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="logo_path",
 *          description="logo_path",
 *          type="string"
 *      )
 * )
 */
class ExternalResource extends Model
{

    public $table = 'external_resources';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'link',
        'order',
        'logo_path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'link' => 'string',
        'logo_path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'     => 'required|max:256',
        'link'      => 'url',
        'logo_path'  => 'mimes:jpg,jpeg,png|dimensions:max_width:1200,max_height:1200',
    ];


    /**
     * Get image thumbnail
     * @param $size string
     * @return string
     */
    public function logo($size = 'small')
    {
        $size = '/' . $size . '/';
        if ($this->logo_path == null || $this->logo_path == '') {
            return '/images/not-found.jpg';
        }
        if (starts_with($this->logo_path, 'http://')) {
            return $this->logo_path;
        } else {


            $path = dirname($this->logo_path);
            $filename = str_replace($path,'', $this->logo_path);

            return '/' . \Config::get('imagecache.route') . $size . $filename;

        }
    }
}
