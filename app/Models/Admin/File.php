<?php

namespace App\Models\Admin;

use App\Models\FileAccessToken;
use App\Models\MessageFilePivot;
use App\Models\User;
use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="File",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="original_name",
 *          description="original_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="file_path",
 *          description="file_path",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extension",
 *          description="extension",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="downloads_count",
 *          description="downloads_count",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class File extends Model
{

    public $table = 'files';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
    ];
    public $guarded = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'original_name' => 'string',
        'file_path' => 'string',
        'extension' => 'string',
        'downloads_count' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($file) { // before delete() method call this
            if(!is_null($file->accessToken)) {
                $file->accessToken->delete();
            }

            \Storage::disk('storage')->delete($file->file_path);
        });
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function messagepivots()
    {
        return $this->hasMany(MessageFilePivot::class);
    }

    public function eventpivots()
    {
        return $this->hasMany(EventFilePivot::class);
    }

    public function telemedEventRequests()
    {
        return $this->hasMany(\App\Models\Admin\TelemedEventRequest::class);
    }

    public function accessToken()
    {
        return $this->hasOne(FileAccessToken::class);
    }
    public function activeAccessToken()
    {
        return $this->accessToken()->active();
    }

    public static function getByHash($hash)
    {
        $token = FileAccessToken::active()->find($hash);

        if( count($token) ) {
            return File::find($token->file_id);
        } else {
            return abort(401, 'У вас нет доступа');
        }
    }


    /** открыть доступ по ссылке */
    public function addAccessByLink()
    {
        $token = $this->accessToken()->first();

        $user_id = \Sentinel::getUser()->id;
        // Если есть то проверяем создал ли юзер ранее,
            if( count($token) ) {

             //Если нет то создаем */
            if( !$token->user_id == $user_id     ) {
                $token =  FileAccessToken::create(['file_id' => $this->id, 'user_id' => $user_id]);
                
            } else {
                // Если есть то меняем статус на false
                $token->changeState(true);
            }

        } else {
            /* Если нет ни одного токена то создаем */
            $token =  FileAccessToken::create(['file_id' => $this->id, 'user_id' => $user_id]);
        }


        return $token;
    }

    /**
     *  Disable AccessLink for File
     * @return  void
    */
    public function disableAccessLink()
    {
        $token = $this->accessToken()->active()->first();

        $user_id = \Sentinel::getUser()->id;
        /* Если есть то меняем статус на false */
        if( count($token) && $token->user_id == $user_id  ) {
            $token->changeState(false);

            return $token;

        } else {
            return false;
        }
    }
    



}
