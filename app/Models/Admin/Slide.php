<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Config;
/**
 * @SWG\Definition(
 *      definition="Slide",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="img_path",
 *          description="img_path",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="order",
 *          description="order",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      )
 * )
 */
class Slide extends Model
{

    public $table = 'slides';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'order',
        'content',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'img_path' => 'string',
        'order' => 'integer',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order' => 'integer',

    ];


    /**
     * Get image thumbnail
     * @param $size string
     * @return string
     */
    public function image($size = 'slide')
    {
        $size = '/' . $size . '/';
        if ($this->img_path == null || $this->img_path == '') {
            return '/images/slide-1.jpg';
        }
        if (starts_with($this->img_path, 'http://')) {
            return $this->img_path;
        } else {

            $path = dirname($this->img_path);
            $filename = str_replace($path,'', $this->img_path);

            return '/' . Config::get('imagecache.route') . $size . $filename;
        }
    }

}
