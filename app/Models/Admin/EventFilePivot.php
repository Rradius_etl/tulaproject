<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="EventFilePivot",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="file_id",
 *          description="file_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EventFilePivot extends Model
{

    public $table = 'event_file_pivots';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'file_id',
        'request_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file_id' => 'integer',
        'request_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedEventRequest()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedEventRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function file()
    {
        return $this->belongsTo(\App\Models\Admin\File::class,"file_id","id");
    }

    protected static function boot() {
        parent::boot();

        static::deleted(function($pivot) { // before delete() method call this
            $file = File::find($pivot->file_id);
            if($file != null)
            {
                $file->delete();
            }
        });
    }

}
