<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="IndicationForUse",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="order",
 *          description="order",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class IndicationForUse extends Model
{
    use SoftDeletes;

    public $table = 'indications_for_use';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public static function getOptionValues($json = false)
    {
        if( $json ) {
            $ind_for_use = IndicationForUse::orderBy('order')->select('id', 'name', 'order')->get()
                ->prepend(['id' => '', 'name' => "----------------", 'order' => -99999 ])
                ->push(['id' => 'other', 'name' => "Иное", 'order' => 99999 ]);
            return $ind_for_use;

        }

        return IndicationForUse::orderBy('order')->lists('name','id')->prepend("----------------", '' )->put("other", "Иное");
    }
    
}
