<?php

namespace App\Models\Admin;

use App\Models\Admin\TelemedCenter;
use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="TelemedRequest",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="importance",
 *          description="importance",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="event_type",
 *          description="event_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="attached_file_id",
 *          description="attached_file_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="initiator_id",
 *          description="initiator_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class TelemedRequest extends Model
{

    public $table = 'telemed_event_requests';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'title',
        'importance',
        'decision_at',
        'event_type',
        'attached_file_id',
        'initiator_id',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'importance' => 'string',
        'decision' => 'string',
        'event_type' => 'string',
        'attached_file_id' => 'integer',
        'initiator_id' => 'integer',
        'canceled' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function files()
    {
        return $this->hasMany(\App\Models\Admin\EventFilePivot::class, 'request_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'initiator_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedEventParticipants()
    {
        return $this->hasMany(\App\Models\Admin\TelemedEventParticipant::class, 'request_id', 'id')->whereExists(function ($query) {
            $query->select(\DB::raw('1'))->from('telemed_centers as tc')->where('tc.id', (\DB::raw('telemed_event_participants.telemed_center_id')))->whereNull('tc.deleted_at');

        });

    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedCenter()
    {
        return $this->hasOne(TelemedCenter::class,'id','telemed_center_id');
    }


    public function telemedConclusion()
    {
        return $this->hasOne(EventConclusion::class,'request_id','id');
    }


    public function getConsultation()
    {
        return $this->hasOne(TelemedConsultRequest::class,'request_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedRequestsMcfAbonents()
    {
        return $this->hasOne(\App\Models\Admin\TelemedConsultAbonentSide::class,'request_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telemedRequestsMcfConsultants()
    {
        return $this->hasOne(\App\Models\Admin\TelemedConsultConsultantSide::class,'request_id','id');
    }
}
