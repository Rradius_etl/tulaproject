<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="MedCareFacDesc",
 *      required={""},
 *      @SWG\Property(
 *          property="med_care_fac_id",
 *          description="med_care_fac_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="key",
 *          description="key",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="value",
 *          description="value",
 *          type="string"
 *      )
 * )
 */
class MedCareFacDesc extends Model
{

    public $table = 'med_care_fac_descriptions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'key',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'med_care_fac_id' => 'integer',
        'key' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function medCareFac()
    {
        return $this->belongsTo(\App\Models\Admin\MedCareFac::class);
    }
}
