<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="TelemedEventParticipant",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="telemed_center_id",
 *          description="telemed_center_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class TelemedEventParticipant extends Model
{

    public $table = 'telemed_event_participants';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'request_id',
        'telemed_center_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'integer',
        'telemed_center_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedEventRequest()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedEventRequest::class);
    }
}
