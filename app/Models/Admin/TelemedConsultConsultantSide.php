<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="TelemedConsultConsultantSide",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="responsible_person_fullname",
 *          description="responsible_person_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="appointed_consultant_name",
 *          description="appointed_consultant_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="appointed_consultant_spec",
 *          description="appointed_consultant_spec",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_fullname",
 *          description="coordinator_fullname",
 *          type="string"
 *      )
 * )
 */
class TelemedConsultConsultantSide extends Model
{

    public $table = 'telemed_requests_mcf_consultant';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'request_id',
        'doctor_id',
        'appointed_person_id',
        'appointed_person',
        'responsible_person_fullname',
        'appointed_consultant_name',
        'appointed_consultant_spec',
        'planned_date',
        'coordinator_fullname'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'integer',
        'responsible_person_fullname' => 'string',
        'appointed_consultant_name' => 'string',
        'appointed_person_id' => 'integer',
        'coordinator_fullname' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedEventRequest()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedEventRequest::class);
    }
    public function cons_request()
    {
        return $this->hasOne(\App\Models\TelemedConsultRequest::class,"request_id","request_id");
    }
    public function files()
    {
        return $this->hasMany(EventFilePivot::class,"request_id","request_id");
    }

/*    protected static function boot() {
        parent::boot();
        static::created(function($item){
            $cons_req = $item->cons_request;
            if($cons_req!=null)
            {
                $tmc = $item->telemedCenter;
                $abon_side = $cons_req->abonentSide;
                $event = $item->telemedEventRequest;
                if($abon_side!=null && $tmc !=null)
                {
                    \NotificationManager::createNotificationFromTMCtoTMC("Ваша заявка ".$cons_req->getFullUid()." была рассмотрена координатором ТМЦ ".$tmc->name,$event->initiator_id,true,$tmc->id,true);
                }

            }
        });
        static::deleting(function($item)
        {
            $cons_req = $item->cons_request;
            if($cons_req!=null)
            {
                $tmc = $item->telemedCenter;
                $abon_side = $cons_req->abonentSide;
                $event = $item->telemedEventRequest;
                if($abon_side!=null && $tmc !=null)
                {
                    \NotificationManager::createNotificationForTmcUsers("Заявка на консультацию ".$cons_req->getFullUid()." была перенаправлена на другой ТМЦ администратором ЛПУ",$event->initiator_id,true,true);
                }

            }
        });
        static::updated(function($item){
            $cons_req = $item->cons_request;
            if($cons_req!=null)
            {
                $tmc = $item->telemedCenter;
                $abon_side = $cons_req->abonentSide;
                $event = $item->telemedEventRequest;
                if($abon_side!=null && $tmc !=null)
                {
                    \NotificationManager::createNotificationFromTMCtoTMC("Ваша заявка ".$cons_req->getFullUid()." была перерассмотрена координатором ТМЦ ".$tmc->name,$event->initiator_id,true,$tmc->id,true);
                }

            }

        });

    }*/
}
