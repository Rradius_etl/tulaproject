<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="EventConclusion",
 *      required={""},
 *      @SWG\Property(
 *          property="request_id",
 *          description="request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="minutes",
 *          description="minutes",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="seconds",
 *          description="seconds",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="calls_count",
 *          description="calls_count",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="comment",
 *          description="comment",
 *          type="string"
 *      )
 * )
 */
class EventConclusion extends Model
{

    public $table = 'telemed_event_conclusion';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = 'request_id';

    public $fillable = [
        'minutes',
        'seconds',
        'calls_count',
        'comment'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'request_id' => 'integer',
        'minutes' => 'integer',
        'seconds' => 'integer',
        'calls_count' => 'integer',
        'comment' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedEventRequest()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedRequest::class, 'request_id');
    }
    public function files()
    {
        return $this->hasMany(EventFilePivot::class,'request_id','request_id')->where('type','conclusion');
    }
}
