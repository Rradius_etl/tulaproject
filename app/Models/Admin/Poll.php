<?php

namespace App\Models\Admin;
use App\Models\Admin\PollRow;
use App\Models\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Poll",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="author_id",
 *          description="author_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Poll extends Model
{

    public $table = 'polls';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    use SoftDeletes;


    public $fillable = [
        'title',
        'author_id',
        'active',
        'finished_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'author_id' => 'integer',
         
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class,"author_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollRows()
    {
        return $this->hasMany(\App\Models\Admin\PollRow::class);
    }
    public function telemedCenter()
    {
        return $this->belongsTo(TelemedCenter::class,'telemed_center_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollUserPivots()
    {
        return $this->hasMany(\App\Models\Admin\PollUserPivot::class);
    }
}
