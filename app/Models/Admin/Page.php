<?php

namespace App\Models\Admin;

use App\Models\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Eloquent as Model;
use Config;
/**
 * @SWG\Definition(
 *      definition="Page",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="img_path",
 *          description="img_path",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="short_description",
 *          description="short_description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="modified_by",
 *          description="modified_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="meta_description",
 *          description="meta_description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="meta_keywords",
 *          description="meta_keywords",
 *          type="string"
 *      )
 * )
 */
class Page extends Model
{

    use Sluggable;
    public $table = 'pages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public $fillable = [
        'title',
        'slug',
        'short_description',
        'content',
        'published',
        'in_menu',
        'meta_description',
        'meta_keywords'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'img_path' => 'string',
        'short_description' => 'string',
        'content' => 'string',
        'created_by' => 'integer',
        'modified_by' => 'integer',
        'in_menu' => 'boolean',
        'published' => 'boolean',
        'meta_description' => 'string',
        'meta_keywords' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'     => 'required|max:256',
        'slug'      => 'alpha_dash',
        'img_path'  => 'mimes:jpg,jpeg,png|dimensions:max_width:3200,max_height:3200',
        'short_description' => 'max:800',
        'content',
        'meta_description',
        'meta_keywords'
    ];


    /**
     * Get image thumbnail
     * @param $size string
     * @return string
     */
    public function image($size = 'small')
    {
        $size = '/' . $size . '/';
        if ($this->img_path == null || $this->img_path == '') {
            return '/images/not-found.jpg';
        }
        if (starts_with($this->img_path, 'http://')) {
            return $this->img_path;
        } else {


            $path = dirname($this->img_path);
            $filename = str_replace($path,'', $this->img_path);

            return '/' . Config::get('imagecache.route') . $size . $filename;

        }
    }


    public function lastModifier()
    {
        return $this->belongsTo(User::class, 'modified_by', 'id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function scopePublished($query)
    {
        return $query->where('published',true);
    }

    public function scopeInMenu($query)
    {
        return $query->published()->where('in_menu',true);
    }

    public function hits()
    {
        return \Tracker::logByRouteName('page.view')
            ->where(function($query)
            {
                $query
                    ->where('parameter', 'pageSlug')
                    ->where('value', $this->slug);
            })
            ->count();
    }
}
