<?php

namespace App\Models\Admin;

use App\Models\TmcRoleManager;
use Eloquent as Model;
use App\Models\User;
use Sentinel;
/**
 * @SWG\Definition(
 *      definition="TelemedUserPivot",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="telemed_center_id",
 *          description="telemed_center_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="value",
 *          description="value",
 *          type="string"
 *      )
 * )
 */
class TelemedUserPivot extends Model
{

    public $table = 'telemed_user_pivots';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'user_id',
        'telemed_center_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'telemed_center_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function telemedCenter()
    {
        return $this->belongsTo(\App\Models\Admin\TelemedCenter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    protected static function boot() {
        parent::boot();
        static::created(function($pivot){
            $user = Sentinel::getUserRepository()->findById($pivot->user_id);
            $roles = $user->roles->pluck("slug");
            if($pivot->value == "coordinator")
            {
                $role = Sentinel::getRoleRepository()->findBySlug($pivot->value);
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'координатор ТМЦ',
                        'slug' => 'coordinator',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }


                if($roles != null  && !in_array("coordinator",$roles->toArray()))
                {
                    $user->roles()->attach($role);
                }
            }
            else if($pivot->value == "admin")
            {
                $role = Sentinel::getRoleRepository()->findBySlug("tmc_admin");
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'Админ ТМЦ',
                        'slug' => 'tmc_admin',
                        'permissions' => [
                        ]
                    ];

                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if($roles != null  && !in_array("tmc_admin",$roles->toArray()))
                {
                    $user->roles()->attach($role);
                }
            }
        });
        static::updating(function($pivot){
            $realPivot = TelemedUserPivot::find($pivot->id);
            $user = $pivot->user;

            if($realPivot->value != $pivot->value)
            {
                $user2 = Sentinel::getUserRepository()->findById($realPivot->user_id);
                if($realPivot->value == "coordinator")
                {
                    $role = Sentinel::getRoleRepository()->findBySlug($realPivot->value );
                }
                else if($realPivot->value == "admin")
                {
                    $role = Sentinel::getRoleRepository()->findBySlug("tmc_admin");
                }
                $status = TmcRoleManager::countOfRoles($pivot->value);

                if($status == 0)
                {
                    $user2->roles()->detach($role);
                }
            }
            else
            {
                if($realPivot->user_id != $pivot->user_id)
                {
                    $user2 = Sentinel::getUserRepository()->findById($realPivot->user_id);
                    if($realPivot->value == "coordinator")
                    {
                        $role = Sentinel::getRoleRepository()->findBySlug($realPivot->value);
                    }
                    else if($realPivot->value == "admin")
                    {
                        $role = Sentinel::getRoleRepository()->findBySlug("tmc_admin");
                    }
                    $status = TmcRoleManager::countOfRoles($realPivot->value);
                    if($status == 0)
                    {
                        $user2->roles()->detach($role);
                    }
                }
            }
        });
        static::updated(function($pivot){
            $user = Sentinel::getUserRepository()->findById($pivot->user_id);
            $roles = $user->roles->pluck("slug");
            if($pivot->value == "coordinator")
            {
                $role = Sentinel::getRoleRepository()->findBySlug($pivot->value);
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'координатор ТМЦ',
                        'slug' => 'coordinator',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if(!in_array("coordinator",$roles->toArray()))
                {
                    $user->roles()->attach($role);
                }
            }
            else if($pivot->value == "admin")
            {

                $role = Sentinel::getRoleRepository()->findBySlug("tmc_admin");
                if($role == null)
                {
                    $roleModel = [
                        'name' => 'Админ ТМЦ',
                        'slug' => 'tmc_admin',
                        'permissions' => [
                        ]
                    ];
                    $role =  Sentinel::getRoleRepository()->createModel()->fill($roleModel)->save();
                }
                if(!in_array("tmc_admin",$roles->toArray()))
                {
                    $user->roles()->attach($role);
                }
            }
        });
        static::deleted(function($pivot) { // before delete() method call this
            \TmcRoleManager::clearCacheForUser($pivot->user_id);
            $user = Sentinel::getUserRepository()->findById($pivot->user_id);
            if($pivot->value == "coordinator")
            {
                $role =  Sentinel::getRoleRepository()->findBySlug($pivot->value);   
                $status = TmcRoleManager::hasRole($pivot->value);
                if($status == false)
                {
                    $user->roles()->detach($role);
                }
            }
            else if($pivot->value == "admin")
            {
                $role =  Sentinel::getRoleRepository()->findBySlug("tmc_admin");
                $status = TmcRoleManager::hasRole($pivot->value);
                if($status == false)
                {
                    $user->roles()->detach($role);
                }
            }
            // do the rest of the cleanup...
        });
    }




    public static function check($user_id,$telemed_center_id,$value)
    {
        $telemedCenter = TelemedCenter::find($telemed_center_id);
        if($telemedCenter != null)
        {
            $user = User::find($user_id);
            if($user != null)
            {
                $is_mcf_admin = $user->medcarefacs;
                if($is_mcf_admin != null)
                {
                    return ['error'=>"пользователь уже является админом ЛПУ -".$is_mcf_admin->name];
                }
                $roles = $user->tmcRoles;
                $check_is_here_coordinator = TelemedUserPivot::where(["telemed_center_id"=>$telemed_center_id,'value'=>"coordinator"])->first();
                $is_user_coordinator = TelemedUserPivot::where(["user_id"=>$user_id,"value"=>"coordinator"])->first();
                $is_user_admin = TelemedUserPivot::where(["user_id"=>$user_id,"value"=>"admin"])->get();
                $is_telemed_mfc_list = false;
                foreach($roles as $role)
                {
                    $mfc_id = $role->TelemedCenter;
                    if($mfc_id->med_care_fac_id != $telemedCenter->med_care_fac_id)
                    {
                        return ['error'=>"пользователь привязан к другому ЛПУ -".$mfc_id->name];
                    }
                }
                if($check_is_here_coordinator != null)
                {
                    if($value == "coordinator")
                    {
                        if($is_user_coordinator!=null)
                        {
                            if($is_user_coordinator->telemed_center_id == $check_is_here_coordinator->telemed_center_id)
                            {
                                return ['error'=>" пользователь ".$is_user_coordinator->user->email." уже является координатором ТМЦ - ".$is_user_coordinator->telemedCenter->name];
                            }
                        }
                    }
                    else if($value == "admin")
                    {
                        if($user_id == $check_is_here_coordinator->user_id)
                        {
                            if($is_user_coordinator->telemed_center_id == $check_is_here_coordinator->telemed_center_id)
                            {
                            }
                            else
                            {
                                return ['error'=>" пользователь ".$check_is_here_coordinator->user->email." уже является координатором ТМЦ - ".$check_is_here_coordinator->telemedCenter->name];
                            }
                        }
                        else{
                            if($is_user_coordinator != null)
                            {
                                if($is_user_coordinator->telemed_center_id != $check_is_here_coordinator->telemed_center_id)
                                {
                                    return ['error'=>" пользователь ".$check_is_here_coordinator->user->email." уже является координатором ТМЦ - ".$check_is_here_coordinator->telemedCenter->name];

                                }
                            }
                        }
                    }
                }
                else
                {
                    if($value == "coordinator")
                    {
                        if($is_user_coordinator != null)
                        {
                            return ["error"=>" пользователь ".$is_user_coordinator->user->email." уже является координатором ТМЦ - ".$is_user_coordinator->telemedCenter->name];
                        }
                        else if(count($is_user_admin) > 0)
                        {
                            $can = true;
                            for($i = 0; $i<count($is_user_admin); $i++)
                            {
                                if($is_user_admin[$i]->telemed_center_id != $telemed_center_id)
                                {
                                    $admin = $is_user_admin[$i];
                                    $can = false;
                                    break;
                                }
                            }
                            if(!$can)
                            {
                                return ["error"=>" пользователь ".$admin->user->email." уже является администратором ТМЦ - ".$admin->telemedCenter->name];
                            }
                        }
                    }
                    else if($value == "admin")
                    {
                        if($is_user_coordinator != null)
                        {
                            return ["error"=>" пользователь ".$is_user_coordinator->user->email." уже является координатором ТМЦ - ".$is_user_coordinator->telemedCenter->name];
                        }
                    }
                }
            }
            else
            {
                return ["error"=>"такого пользователя не существует"];
            }
            }
            else
            {
                return ["error"=>"такого ТМЦ не существует"];
            }
        return true;
        }
}
