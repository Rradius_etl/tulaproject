<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="PollRow",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="author_id",
 *          description="author_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class PollRow extends Model
{

    public $table = 'poll_rows';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'title',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'author_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Admin\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollRows()
    {
        return $this->hasMany(\App\Models\Admin\PollRow::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pollUserPivots()
    {
        return $this->hasMany(\App\Models\Admin\PollUserPivot::class);
    }

    public function getVotes($total)
    {
        $count = \DB::table('poll_user_pivots as pup')->join('poll_rows as pr',"pup.poll_row_id","=","pr.id")
            ->where("pr.id",$this->id)->count();
        if( $count == 0 || $total == 0) return 0;
        return ['percent'=>round(((100 * $count) / $total), 2),'count'=>$count];
    }
}
