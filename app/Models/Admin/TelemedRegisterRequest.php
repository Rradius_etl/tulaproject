<?php

namespace App\Models\Admin;

use Eloquent as Model;
use App\Models\Admin\MedCareFac;
/**
 * @SWG\Definition(
 *      definition="TelemedRegisterRequest",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="med_care_fac_id",
 *          description="med_care_fac_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="director_fullname",
 *          description="director_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_fullname",
 *          description="coordinator_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coordinator_phone",
 *          description="coordinator_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_fullname",
 *          description="tech_specialist_fullname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_phone",
 *          description="tech_specialist_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tech_specialist_contacts",
 *          description="tech_specialist_contacts",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="decision",
 *          description="decision",
 *          type="string"
 *      )
 * )
 */
class TelemedRegisterRequest extends Model
{

    public $table = 'telemed_register_requests';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'med_care_fac_id',
        'name',
        'director_fullname',
        'coordinator_fullname',
        'coordinator_phone',
        'tech_specialist_fullname',
        'tech_specialist_phone',
        'tech_specialist_contacts',
        'decision',
        'videoconf_equipment',
        'digit_img_demonstration',
        'equipment_location',
        'address',
        'terminal_name',
        'terminal_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'med_care_fac_id' => 'integer',
        'name' => 'string',
        'director_fullname' => 'string',
        'coordinator_fullname' => 'string',
        'coordinator_phone' => 'string',
        'tech_specialist_fullname' => 'string',
        'tech_specialist_phone' => 'string',
        'tech_specialist_contacts' => 'string',
        'decision' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
    public function med_care_facs()
    {
        return $this->belongsTo(MedCareFac::class,'med_care_fac_id','id');
    }
    public function med_care_fac()
    {
        return $this->belongsTo(MedCareFac::class,'med_care_fac_id','id');
    }
}
