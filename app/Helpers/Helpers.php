<?php

function slugifyImage(\Intervention\Image\Image $image = null) {

    if( $image  == null )
        return 'file-' .date('d-m-Y') . '.jpg';
    return str_slug( str_limit($image->getClientOriginalName(), 15, '')) . '-' . $image->width() . 'x' . $image->height() . '-' . date('d-m-Y');

}

function camel2dashed($className) {
    return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $className));
}


function hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    }
    $rgb = array("R"=>$r,"G"=>$g,"B"=>$b,"Alpha"=>100);
    //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; // returns an array with the rgb values
}