<?php

namespace App\Http;

use App\Http\Middleware\DoctorListMiddleware;
use App\Http\Middleware\MedCareFacMiddleware;
use App\Http\Middleware\TelemedAdminMiddleware;
use App\Http\Middleware\TelemedAdminOrMfcAdminMiddleware;
use App\Http\Middleware\TelemedRoleMiddleware;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Flash;
class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \PragmaRX\Tracker\Vendor\Laravel\Middlewares\Tracker::class,

        ],
        'api' => [
            'throttle:60,1',
        ],

    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'med_care_fac_admin'=>MedCareFacMiddleware::class,
        'has_access_to_edit_tmc'=>TelemedAdminOrMfcAdminMiddleware::class,
        'auth' => \App\Http\Middleware\Authenticate::class,
        'is_user_of_telemed' => TelemedRoleMiddleware::class,
        'is_telemed_admin_or_mfc_admin'=>TelemedAdminMiddleware::class,
        'isadmin' => \App\Http\Middleware\AdminPanel::class,
        //'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        //'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

    ];
}
