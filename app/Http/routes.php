<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Input;
use App\Http\Controllers;


/**
 * Route for auth system
 */


Route::get('openfile/{hash}', ['as'=> 'files.open.get', 'uses' => 'OpenFilesController@getOpenFile']);
Route::get('openfiles', ['as'=> 'files.open.index', 'uses' => 'OpenFilesController@index']);

Route::get('tmc_chart/{tmc_id}', 'StatisticsController@getVideoConferenceReportCharts');
Route::get('admin/tmc_chart/{tmc_id}', 'Admin\StatisticsController@getVideoConferenceReportCharts');
Route::get('reports/render/charts/{id}', ['as'=> 'reports.render.charts', 'uses' => 'ReportController@renderCharts']);
Route::group(['middleware' => 'auth'], function () {

    // Admin Reports
    Route::resource('reports', 'ReportController', [
        'names' => [
            'index' => 'reports.index',
            'destroy' => 'reports.destroy',
            'show' => 'reports.show',
            'edit' => 'reports.edit',
            'store' => 'reports.store',
            'create' => 'reports.create',
            'update' => 'reports.update',
        ]
    ]);


    Route::get('reports/render/table/{id}', ['as'=> 'reports.render.table', 'uses' => 'ReportController@renderTable']);

    Route::get('reports/download/charts/{id}/{fileFormat}', ['as'=> 'reports.download.charts', 'uses' => 'ReportController@downloadCharts']);
    Route::get('reports/download/table/{id}/{format}', ['as'=> 'reports.download.table', 'uses' => 'ReportController@downloadTable']);
    Route::get('reports/init/graphics/{id}', ['as'=> 'reports.init.graphics', 'uses' => 'ReportController@initGraphics']);
    Route::get('reports/init/table/{id}', ['as'=> 'reports.init.table', 'uses' => 'ReportController@initTable']);
    // Calendar
    Route::group(['middleware' => ['is_user_of_telemed']], function(){
        Route::get('calendar', [ 'as' => 'calendar', 'uses' => 'CalendarController@index']);
    });
    // Выход пользователя из системы
    Route::get('logout',  [ 'as' => 'logout', 'uses' => 'AuthController@logoutuser'] );





    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('outgoing', ['as' => 'messages.outgoing', 'uses' => 'MessagesController@outgoing']);
        Route::get('unread', ['as' => 'messages.unread', 'uses' => 'MessagesController@unread']);
        Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });


    // Файлы
    Route::get('files/available', ['as' => 'files.available', 'uses' => 'FilesController@availableFiles']);
    Route::get('files/uploaded', ['as' => 'files.uploaded', 'uses' => 'FilesController@uploaded']);
    Route::get('getfile/{file_id}', ['as'=> 'files.get', 'uses' => 'FilesController@getFile']);
    Route::get('files/{file_id}/permissions', ['as'=> 'files.permissions.enable', 'uses' => 'FilesController@getAccessStatus']);
    Route::get('files/{file_id}/permissions/on', ['as'=> 'files.permissions.enable', 'uses' => 'FilesController@enableAccessByLink']);
    Route::get('files/{file_id}/permissions/off', ['as'=> 'files.permissions.enable', 'uses' => 'FilesController@disableAccessByLink']);


    // ТМК
    Route::get('new-consultation', ['as' => 'new-consultation', 'uses' => 'IndexController@newConsultation']);
    Route::post('new-consultation', ['as' => 'new-consultation', 'uses' => 'IndexController@addRequest']);
    Route::get('new-consultation-first', ['as' => 'new-consultation-first', 'uses' => 'IndexController@chooseTMC']);
    // Уведомления
    Route::get('notifications', ['as' => 'notifications.index', 'uses' => 'NotificationsController@index']);
    Route::get('notifications/unread', ['as' => 'notifications.unread', 'uses' => 'NotificationsController@unread']);
    Route::get('notifications/last', ['as' => 'notifications.last', 'uses' => 'NotificationsController@last']);
    Route::post('notifications/mark-as-read/{type}', ['as' => 'notifications.check-as-read', 'uses' => 'NotificationsController@markAsRead']);

    // Уведомления для опросов
    Route::get('polls/archive', ['as' => 'polls.archive.index', 'uses' => 'ProfileController@pollsArchive']);
    Route::get('polls/notifications', ['as' => 'polls.notifications.index', 'uses' => 'ProfileController@pollNotifications']);
    Route::get('polls/notifications/unread', ['as' => 'polls.notifications.unread', 'uses' => 'ProfileController@unreadPollNotifications']);
    Route::get('polls/notifications/last', ['as' => 'polls.notifications.last', 'uses' => 'NotificationsController@unreadPollNotifications']);


    Route::get("polls/all",['as'=>'polls.all','uses'=>"ProfileController@all"]);
    Route::get("polls/my",['as'=>'polls.my','uses'=>"ProfileController@myPolls"]);
    Route::post("polls/notify",['as'=>'polls.notify','uses'=>"ProfileController@notifyPoll"]);
    Route::post("polls/add",['as'=>'polls.add','uses'=>"ProfileController@add"]);
    Route::get("profile",['as'=>'profile','uses'=>"ProfileController@index"]);
    Route::get("profile/edit",['as'=>'profile.edit','uses'=>"ProfileController@edit"]);
    Route::post("profile/update",['as'=>'profile.update','uses'=>"ProfileController@update"]);
    Route::get("profile/deleteavatar",['as'=>'profile.deleteavatar','uses'=>"ProfileController@deleteavatar"]);

    Route::get('telemed-register-requests', ['as'=> 'telemed-register-requests.index', 'uses' => 'TelemedRegisterRequestController@index']);
    Route::post('telemed-register-requests', ['as'=> 'telemed-register-requests.store', 'uses' => 'TelemedRegisterRequestController@store']);
    Route::get('telemed-register-requests/create', ['as'=> 'telemed-register-requests.create', 'uses' => 'TelemedRegisterRequestController@create']);
    Route::put('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'telemed-register-requests.update', 'uses' => 'TelemedRegisterRequestController@update']);
    Route::patch('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'telemed-register-requests.update', 'uses' => 'TelemedRegisterRequestController@update']);
    Route::delete('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'telemed-register-requests.destroy', 'uses' => 'TelemedRegisterRequestController@destroy']);
    Route::get('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'telemed-register-requests.show', 'uses' => 'TelemedRegisterRequestController@show']);
    Route::get('telemed-register-requests/{TelemedRegisterRequest}/edit', ['as'=> 'telemed-register-requests.edit', 'uses' => 'TelemedRegisterRequestController@edit']);

    Route::get('telemed-centers', ['as'=> 'telemed-centers.index', 'uses' => 'TelemedCenterController@index']);
    Route::put('telemed-centers/{TelemedCenter}', ['as'=> 'telemed-centers.update', 'uses' => 'TelemedCenterController@update']);
    Route::patch('telemed-centers/{TelemedCenter}', ['as'=> 'telemed-centers.update', 'uses' => 'TelemedCenterController@update']);
    Route::delete('telemed-centers/{TelemedCenter}', ['as'=> 'telemed-centers.destroy', 'uses' => 'TelemedCenterController@destroy']);
    Route::get('telemed-centers/{TelemedCenter}', ['as'=> 'telemed-centers.show', 'uses' => 'TelemedCenterController@show']);
    Route::get('telemed-centers/{TelemedCenter}/edit', ['as'=> 'telemed-centers.edit', 'uses' => 'TelemedCenterController@edit']);


    Route::get('doctors/{TelemedId}', ['as'=> 'doctors.index', 'uses' => 'DoctorController@index']);
    Route::post('doctors/{TelemedId}', ['as'=> 'doctors.store', 'uses' => 'DoctorController@store']);
    Route::get('doctors/{TelemedId}/create', ['as'=> 'doctors.create', 'uses' => 'DoctorController@create']);
    Route::put('doctors/{TelemedId}/{Doctor}', ['as'=> 'doctors.update', 'uses' => 'DoctorController@update']);
    Route::patch('doctors/{TelemedId}/{Doctor}', ['as'=> 'doctors.update', 'uses' => 'DoctorController@update']);
    Route::delete('doctors/{TelemedId}/{Doctor}', ['as'=> 'doctors.destroy', 'uses' => 'DoctorController@destroy']);
    Route::get('doctors/{TelemedId}/{Doctor}', ['as'=> 'doctors.show', 'uses' => 'DoctorController@show']);
    Route::get('doctors/{TelemedId}/{Doctor}/edit', ['as'=> 'doctors.edit', 'uses' => 'DoctorController@edit']);



    Route::get('telemed-consult-requests', ['as'=> 'telemed-consult-requests.index', 'uses' => 'TelemedConsultRequestController@index']);
    Route::post('telemed-consult-requests', ['as'=> 'telemed-consult-requests.store', 'uses' => 'TelemedConsultRequestController@store']);
    Route::get('telemed-consult-requests/create', ['as'=> 'telemed-consult-requests.create', 'uses' => 'TelemedConsultRequestController@create']);
    Route::put('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'telemed-consult-requests.update', 'uses' => 'TelemedConsultRequestController@update']);
    Route::patch('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'telemed-consult-requests.update', 'uses' => 'TelemedConsultRequestController@update']);
    Route::get('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'telemed-consult-requests.show', 'uses' => 'TelemedConsultRequestController@show']);
    Route::get('telemed-consult-requests/{TelemedConsultRequest}/edit', ['as'=> 'telemed-consult-requests.edit', 'uses' => 'TelemedConsultRequestController@edit']);

    Route::get('tmc-telemed-consult-requests', ['as'=> 'tmc-telemed-consult-requests.index', 'uses' => 'TMCTelemedConsultRequestController@index']);
    Route::post('tmc-telemed-consult-requests', ['as'=> 'tmc-telemed-consult-requests.store', 'uses' => 'TMCTelemedConsultRequestController@store']);
    Route::get('tmc-telemed-consult-requests/create', ['as'=> 'tmc-telemed-consult-requests.create', 'uses' => 'TMCTelemedConsultRequestController@create']);
    Route::put('tmc-telemed-consult-requests/{TMCTelemedConsultRequest}', ['as'=> 'tmc-telemed-consult-requests.update', 'uses' => 'TMCTelemedConsultRequestController@update']);
    Route::patch('tmc-telemed-consult-requests/{TMCTelemedConsultRequest}', ['as'=> 'tmc-telemed-consult-requests.update', 'uses' => 'TMCTelemedConsultRequestController@update']);
    Route::get('tmc-telemed-consult-requests/{TMCTelemedConsultRequest}', ['as'=> 'tmc-telemed-consult-requests.show', 'uses' => 'TMCTelemedConsultRequestController@show']);
    Route::get('tmc-telemed-consult-requests/{TMCTelemedConsultRequest}/edit', ['as'=> 'tmc-telemed-consult-requests.edit', 'uses' => 'TMCTelemedConsultRequestController@edit']);

    Route::get('abonent-telemed-consult-requests', ['as'=> 'abonent-telemed-consult-requests.index', 'uses' => 'AbonentTelemedConsultRequestController@index']);
    Route::post('abonent-telemed-consult-requests', ['as'=> 'abonent-telemed-consult-requests.store', 'uses' => 'AbonentTelemedConsultRequestController@store']);
    Route::get('abonent-telemed-consult-requests/create', ['as'=> 'abonent-telemed-consult-requests.create', 'uses' => 'AbonentTelemedConsultRequestController@create']);
    Route::put('abonent-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'abonent-telemed-consult-requests.update', 'uses' => 'AbonentTelemedConsultRequestController@update']);
    Route::patch('abonent-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'abonent-telemed-consult-requests.update', 'uses' => 'AbonentTelemedConsultRequestController@update']);
    Route::delete('abonent-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'abonent-telemed-consult-requests.destroy', 'uses' => 'AbonentTelemedConsultRequestController@destroy']);
    Route::get('abonent-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'abonent-telemed-consult-requests.show', 'uses' => 'AbonentTelemedConsultRequestController@show']);
    Route::get('abonent-telemed-consult-requests/{TelemedConsultRequest}/edit', ['as'=> 'abonent-telemed-consult-requests.edit', 'uses' => 'AbonentTelemedConsultRequestController@edit']);
    Route::post('abonent-telemed-consult-requests/{TelemedConsultRequest}/savepattern', ['as'=> 'abonent-telemed-consult-requests.savepattern', 'uses' => 'AbonentTelemedConsultRequestController@savepattern']);

    Route::get('consultant-telemed-consult-requests', ['as'=> 'consultant-telemed-consult-requests.index', 'uses' => 'ConsultantTelemedConsultRequestController@index']);
    Route::post('consultant-telemed-consult-requests', ['as'=> 'consultant-telemed-consult-requests.store', 'uses' => 'ConsultantTelemedConsultRequestController@store']);
    Route::get('consultant-telemed-consult-requests/create', ['as'=> 'consultant-telemed-consult-requests.create', 'uses' => 'ConsultantTelemedConsultRequestController@create']);
    Route::put('consultant-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'consultant-telemed-consult-requests.update', 'uses' => 'ConsultantTelemedConsultRequestController@update']);
    Route::patch('consultant-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'consultant-telemed-consult-requests.update', 'uses' => 'ConsultantTelemedConsultRequestController@update']);
   /* Route::delete('consultant-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'consultant-telemed-consult-requests.destroy', 'uses' => 'ConsultantTelemedConsultRequestController@destroy']);*/
    Route::get('consultant-telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'consultant-telemed-consult-requests.show', 'uses' => 'ConsultantTelemedConsultRequestController@show']);
    Route::get('consultant-telemed-consult-requests/{TelemedConsultRequest}/edit', ['as'=> 'consultant-telemed-consult-requests.edit', 'uses' => 'ConsultantTelemedConsultRequestController@edit']);

    Route::get('polls/{TelemedId}', ['as'=> 'polls.index', 'uses' => 'PollController@index']);
    Route::post('polls/{TelemedId}', ['as'=> 'polls.store', 'uses' => 'PollController@store']);
    Route::get('polls/create/{TelemedId}', ['as'=> 'polls.create', 'uses' => 'PollController@create']);
    Route::put('polls/{TelemedId}/{Poll}', ['as'=> 'polls.update', 'uses' => 'PollController@update']);
    Route::patch('polls/{TelemedId}/{Poll}', ['as'=> 'polls.update', 'uses' => 'PollController@update']);
    Route::delete('polls/{TelemedId}/{Poll}', ['as'=> 'polls.destroy', 'uses' => 'PollController@destroy']);
    Route::get('polls/{TelemedId}/{Poll}', ['as'=> 'polls.show', 'uses' => 'PollController@show']);
    Route::get('polls/{TelemedId}/{Poll}/edit', ['as'=> 'polls.edit', 'uses' => 'PollController@edit']);




    Route::get('telemed-user-pivots/{TelemedId}', ['as'=> 'telemed-user-pivots.index', 'uses' => 'TelemedAdminUserPivotController@index']);
    Route::post('telemed-user-pivots/{TelemedId}', ['as'=> 'telemed-user-pivots.store', 'uses' => 'TelemedAdminUserPivotController@store']);
    Route::get('telemed-user-pivots/{TelemedId}/create', ['as'=> 'telemed-user-pivots.create', 'uses' => 'TelemedAdminUserPivotController@create']);
    Route::put('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'telemed-user-pivots.update', 'uses' => 'TelemedAdminUserPivotController@update']);
    Route::patch('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'telemed-user-pivots.update', 'uses' => 'TelemedAdminUserPivotController@update']);
    Route::delete('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'telemed-user-pivots.destroy', 'uses' => 'TelemedAdminUserPivotController@destroy']);
    Route::get('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'telemed-user-pivots.show', 'uses' => 'TelemedAdminUserPivotController@show']);
    Route::get('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}/edit', ['as'=> 'telemed-user-pivots.edit', 'uses' => 'TelemedAdminUserPivotController@edit']);


    Route::get('invitations', ['as'=> 'invitations.index', 'uses' => 'InvitationController@index']);
    Route::get('invitations/{Id}', ['as'=> 'invitations.show', 'uses' => 'InvitationController@show']);



    Route::get('public_polls/{Id}', ['as'=> 'public_polls.index', 'uses' => 'PublicPollsController@getPoll']);
    Route::post('send-answer', ['as'=> 'public_polls.index', 'uses' => 'PublicPollsController@answerPoll']);

    Route::get('finduser', ['as'=>'finduser', 'uses' => 'Admin\ApiController@findUser' ]);
    Route::get('findtmcs', ['as'=>'findtmcs', 'uses' => 'Admin\ApiController@findTmcs' ]);
    Route::get('findpolls', ['as'=>'findpolls', 'uses' => 'Admin\ApiController@findPolls' ]);
    Route::resource('consult-patterns', 'ConsultPatternController');

    Route::get('general-telemed-centers', ['as'=> 'general-telemed-centers.index', 'uses' => 'GeneralTelemedCenterController@index']);
    Route::get('general-telemed-centers/{TelemedCenter}', ['as'=> 'general-telemed-centers.show', 'uses' => 'GeneralTelemedCenterController@show']);

    Route::get('general-telemed-centers/{TelemedCenter}/send-message-tmc', ['as'=> 'general-telemed-centers.send-message-tmc', 'uses' => 'GeneralTelemedCenterController@sendMessageTmc']);
    Route::post('general-telemed-centers/{TelemedCenter}/send-message-tmc', ['as'=> 'general-telemed-centers.send-message-tmc', 'uses' => 'GeneralTelemedCenterController@sendMessageTmcPost']);


    // Статистика

    Route::get('statistics', [ 'as' => 'statistics.index', 'uses' =>'StatisticsController@index' ]);

    Route::get('tmc_report/{tmc_id}',[ 'as' => 'statistics.download', 'uses'=>'StatisticsController@getVideoConferenceReport']);  /* Роут не только для админа */


});
Route::group([
    'middleware' => ['auth','isadmin'],
], function () {
// Admin Dashboard
    Route::resource('home', 'HomeController', [
        'names' => [
            'index' => 'dashboard',
        ]
    ]);
});
Route::group([
    'middleware' => ['auth','isadmin'],
    'prefix' => 'admin'
], function () {

    Route::get('/', [ 'as' => 'admin', 'uses' =>'HomeController@index' ]);

    Route::get('routes',[ 'as'=> 'admin.routes.index', 'uses' => 'Admin\RoutesController@index']);

    // Route Пользователей
    Route::resource('users', 'Admin\UsersController', [
        'names' => [
            'index' => 'admin.users.index',
            'edit-interface' => 'admin.users.edit-interface',
            'update-interface' => 'admin.users.update-interface',
        ]
    ]);
    Route::get('users/{user}/edit-interface', [ 'as' => 'admin.users.edit-interface', 'uses' =>'Admin\UsersController@editInterface' ]);
    Route::post('users/edit-interface/{user}', ['as'=> 'admin.users.update-interface', 'uses' => 'Admin\UsersController@updateInterface']);

    //Ручная активация пользователя
    Route::get('manual-activate/{id}',['as'=>'manual-activate','uses'=> 'AuthController@manualActivation']);
    
    // Настройки сайта.
    Route::get('site-settings', [ 'as' => 'admin.site-settings', 'uses' =>'Admin\AdminController@siteSettings' ]);
    Route::patch('site-settings', [ 'as' => 'admin.site-settings', 'uses' =>'Admin\AdminController@saveSettings' ]);
    // Roles
    Route::resource('roles', 'Admin\RoleController');
    //Disable Admin
    Route::post('roles/disableadmin', 'Admin\UsersController@disableAdmin');
    //Make Admin
    Route::post('roles/makeadmin', 'Admin\UsersController@makeAdmin');

    Route::post('user/resend-activation-email', 'Admin\UsersController@resendActivationEmail');
    
    
    // Отчеты

    Route::get('consult_reports', 'Admin\StatisticsController@getReport');
    Route::get('reports', 'Admin\StatisticsController@getEventsReport');
    Route::get('conference_report', 'Admin\StatisticsController@getConferenceReport'); /* 2 type required */
    Route::get('tmc_report/{tmc_id}','Admin\StatisticsController@getVideoConferenceReport');  /* Роут не только для админа */
    Route::get('tmcs_report','Admin\StatisticsController@getVideoConferenceReportForManyTMC');

    // Admin Reports
    Route::resource('reports', 'Admin\ReportController', [
        'names' => [
            'index' => 'admin.reports.index',
            'store' => 'admin.reports.store',
            'create' => 'admin.reports.create',
            'update' => 'admin.reports.update',
            'show' => 'admin.reports.show',
            'destroy' => 'admin.reports.destroy',
            'edit' => 'admin.reports.edit',
        ]
    ]);
    Route::get('reports/transfer/{id}', ['as'=> 'admin.reports.transfer', 'uses' => 'Admin\ReportController@transfer']);
    Route::post('reports/transfer/{id}', ['as'=> 'admin.reports.transferProcess', 'uses' => 'Admin\ReportController@transferProcess']);
    Route::get('pages', ['as'=> 'admin.pages.index', 'uses' => 'Admin\PageController@index']);
    Route::post('pages', ['as'=> 'admin.pages.store', 'uses' => 'Admin\PageController@store']);
    Route::get('pages/create', ['as'=> 'admin.pages.create', 'uses' => 'Admin\PageController@create']);
    Route::put('pages/{pages}', ['as'=> 'admin.pages.update', 'uses' => 'Admin\PageController@update']);
    Route::patch('pages/{pages}', ['as'=> 'admin.pages.update', 'uses' => 'Admin\PageController@update']);
    Route::delete('pages/{pages}', ['as'=> 'admin.pages.destroy', 'uses' => 'Admin\PageController@destroy']);
    Route::get('pages/{pages}', ['as'=> 'admin.pages.show', 'uses' => 'Admin\PageController@show']);
    Route::get('pages/{pages}/edit', ['as'=> 'admin.pages.edit', 'uses' => 'Admin\PageController@edit']);


    Route::get('files', ['as'=> 'admin.files.index', 'uses' => 'Admin\FileController@index']);
    Route::post('files', ['as'=> 'admin.files.store', 'uses' => 'Admin\FileController@store']);
    Route::get('files/create', ['as'=> 'admin.files.create', 'uses' => 'Admin\FileController@create']);
    Route::put('files/{File}', ['as'=> 'admin.files.update', 'uses' => 'Admin\FileController@update']);
    Route::patch('files/{File}', ['as'=> 'admin.files.update', 'uses' => 'Admin\FileController@update']);
    Route::delete('files/{File}', ['as'=> 'admin.files.destroy', 'uses' => 'Admin\FileController@destroy']);
    Route::get('files/{File}', ['as'=> 'admin.files.show', 'uses' => 'Admin\FileController@show']);
    Route::get('files/{File}/edit', ['as'=> 'admin.files.edit', 'uses' => 'Admin\FileController@edit']);




    Route::get('telemed-centers', ['as'=> 'admin.telemed-centers.index', 'uses' => 'Admin\TelemedCenterController@index']);
    Route::post('telemed-centers', ['as'=> 'admin.telemed-centers.store', 'uses' => 'Admin\TelemedCenterController@store']);
    Route::get('telemed-centers/create', ['as'=> 'admin.telemed-centers.create', 'uses' => 'Admin\TelemedCenterController@create']);
    Route::put('telemed-centers/{TelemedCenter}', ['as'=> 'admin.telemed-centers.update', 'uses' => 'Admin\TelemedCenterController@update']);
    Route::patch('telemed-centers/{TelemedCenter}', ['as'=> 'admin.telemed-centers.update', 'uses' => 'Admin\TelemedCenterController@update']);
    Route::delete('telemed-centers/{TelemedCenter}', ['as'=> 'admin.telemed-centers.destroy', 'uses' => 'Admin\TelemedCenterController@destroy']);
    Route::get('telemed-centers/{TelemedCenter}', ['as'=> 'admin.telemed-centers.show', 'uses' => 'Admin\TelemedCenterController@show']);
    Route::get('telemed-centers/{TelemedCenter}/edit', ['as'=> 'admin.telemed-centers.edit', 'uses' => 'Admin\TelemedCenterController@edit']);


    Route::get('telemed-requests', ['as'=> 'admin.telemed-requests.index', 'uses' => 'Admin\TelemedRequestController@index']);
    Route::post('telemed-requests', ['as'=> 'admin.telemed-requests.store', 'uses' => 'Admin\TelemedRequestController@store']);
    Route::get('telemed-requests/create', ['as'=> 'admin.telemed-requests.create', 'uses' => 'Admin\TelemedRequestController@create']);
    Route::put('telemed-requests/{TelemedRequest}', ['as'=> 'admin.telemed-requests.update', 'uses' => 'Admin\TelemedRequestController@update']);
    Route::patch('telemed-requests/{TelemedRequest}', ['as'=> 'admin.telemed-requests.update', 'uses' => 'Admin\TelemedRequestController@update']);
    Route::delete('telemed-requests/{TelemedRequest}', ['as'=> 'admin.telemed-requests.destroy', 'uses' => 'Admin\TelemedRequestController@destroy']);
    Route::get('telemed-requests/{TelemedRequest}', ['as'=> 'admin.telemed-requests.show', 'uses' => 'Admin\TelemedRequestController@show']);
    Route::get('telemed-requests/{TelemedRequest}/edit', ['as'=> 'admin.telemed-requests.edit', 'uses' => 'Admin\TelemedRequestController@edit']);


    Route::get('telemed-consult-requests', ['as'=> 'admin.telemed-consult-requests.index', 'uses' => 'Admin\TelemedConsultRequestController@index']);
    Route::post('telemed-consult-requests', ['as'=> 'admin.telemed-consult-requests.store', 'uses' => 'Admin\TelemedConsultRequestController@store']);
    Route::get('telemed-consult-requests/create', ['as'=> 'admin.telemed-consult-requests.create', 'uses' => 'Admin\TelemedConsultRequestController@create']);
    Route::put('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'admin.telemed-consult-requests.update', 'uses' => 'Admin\TelemedConsultRequestController@update']);
    Route::patch('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'admin.telemed-consult-requests.update', 'uses' => 'Admin\TelemedConsultRequestController@update']);
    Route::delete('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'admin.telemed-consult-requests.destroy', 'uses' => 'Admin\TelemedConsultRequestController@destroy']);
    Route::get('telemed-consult-requests/{TelemedConsultRequest}', ['as'=> 'admin.telemed-consult-requests.show', 'uses' => 'Admin\TelemedConsultRequestController@show']);
    Route::get('telemed-consult-requests/{TelemedConsultRequest}/edit', ['as'=> 'admin.telemed-consult-requests.edit', 'uses' => 'Admin\TelemedConsultRequestController@edit']);


    Route::get('med-care-facs', ['as'=> 'admin.med-care-facs.index', 'uses' => 'Admin\MedCareFacController@index']);
    Route::post('med-care-facs', ['as'=> 'admin.med-care-facs.store', 'uses' => 'Admin\MedCareFacController@store']);
    Route::get('med-care-facs/create', ['as'=> 'admin.med-care-facs.create', 'uses' => 'Admin\MedCareFacController@create']);
    Route::put('med-care-facs/{MedCareFac}', ['as'=> 'admin.med-care-facs.update', 'uses' => 'Admin\MedCareFacController@update']);
    Route::patch('med-care-facs/{MedCareFac}', ['as'=> 'admin.med-care-facs.update', 'uses' => 'Admin\MedCareFacController@update']);
    Route::delete('med-care-facs/{MedCareFac}', ['as'=> 'admin.med-care-facs.destroy', 'uses' => 'Admin\MedCareFacController@destroy']);
    Route::get('med-care-facs/{MedCareFac}', ['as'=> 'admin.med-care-facs.show', 'uses' => 'Admin\MedCareFacController@show']);
    Route::get('med-care-facs/{MedCareFac}/edit', ['as'=> 'admin.med-care-facs.edit', 'uses' => 'Admin\MedCareFacController@edit']);


    Route::get('telemed-register-requests', ['as'=> 'admin.telemed-register-requests.index', 'uses' => 'Admin\TelemedRegisterRequestController@index']);
    Route::put('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.update', 'uses' => 'Admin\TelemedRegisterRequestController@update']);
    Route::patch('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.update', 'uses' => 'Admin\TelemedRegisterRequestController@update']);
    Route::delete('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.destroy', 'uses' => 'Admin\TelemedRegisterRequestController@destroy']);
    Route::get('telemed-register-requests/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.show', 'uses' => 'Admin\TelemedRegisterRequestController@show']);
    Route::get('telemed-register-requests/{TelemedRegisterRequest}/edit', ['as'=> 'admin.telemed-register-requests.edit', 'uses' => 'Admin\TelemedRegisterRequestController@edit']);
    Route::get('telemed-register-requests/agree/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.agree', 'uses' => 'Admin\TelemedRegisterRequestController@agree']);
    Route::get('telemed-register-requests/reject/{TelemedRegisterRequest}', ['as'=> 'admin.telemed-register-requests.reject', 'uses' => 'Admin\TelemedRegisterRequestController@reject']);

    Route::get('medical-profile-categories', ['as'=> 'admin.medical-profile-categories.index', 'uses' => 'Admin\MedicalProfileCategoryController@index']);
    Route::post('medical-profile-categories', ['as'=> 'admin.medical-profile-categories.store', 'uses' => 'Admin\MedicalProfileCategoryController@store']);
    Route::get('medical-profile-categories/create', ['as'=> 'admin.medical-profile-categories.create', 'uses' => 'Admin\MedicalProfileCategoryController@create']);
    Route::put('medical-profile-categories/{MedicalProfileCategory}', ['as'=> 'admin.medical-profile-categories.update', 'uses' => 'Admin\MedicalProfileCategoryController@update']);
    Route::patch('medical-profile-categories/{MedicalProfileCategory}', ['as'=> 'admin.medical-profile-categories.update', 'uses' => 'Admin\MedicalProfileCategoryController@update']);
    Route::delete('medical-profile-categories/{MedicalProfileCategory}', ['as'=> 'admin.medical-profile-categories.destroy', 'uses' => 'Admin\MedicalProfileCategoryController@destroy']);
    Route::get('medical-profile-categories/{MedicalProfileCategory}', ['as'=> 'admin.medical-profile-categories.show', 'uses' => 'Admin\MedicalProfileCategoryController@show']);
    Route::get('medical-profile-categories/{MedicalProfileCategory}/edit', ['as'=> 'admin.medical-profile-categories.edit', 'uses' => 'Admin\MedicalProfileCategoryController@edit']);


    Route::get('medical-profiles', ['as'=> 'admin.medical-profiles.index', 'uses' => 'Admin\MedicalProfileController@index']);
    Route::post('medical-profiles', ['as'=> 'admin.medical-profiles.store', 'uses' => 'Admin\MedicalProfileController@store']);
    Route::get('medical-profiles/create', ['as'=> 'admin.medical-profiles.create', 'uses' => 'Admin\MedicalProfileController@create']);
    Route::put('medical-profiles/{MedicalProfile}', ['as'=> 'admin.medical-profiles.update', 'uses' => 'Admin\MedicalProfileController@update']);
    Route::patch('medical-profiles/{MedicalProfile}', ['as'=> 'admin.medical-profiles.update', 'uses' => 'Admin\MedicalProfileController@update']);
    Route::delete('medical-profiles/{MedicalProfile}', ['as'=> 'admin.medical-profiles.destroy', 'uses' => 'Admin\MedicalProfileController@destroy']);
    Route::get('medical-profiles/{MedicalProfile}', ['as'=> 'admin.medical-profiles.show', 'uses' => 'Admin\MedicalProfileController@show']);
    Route::get('medical-profiles/{MedicalProfile}/edit', ['as'=> 'admin.medical-profiles.edit', 'uses' => 'Admin\MedicalProfileController@edit']);



    Route::get('telemed-user-pivots/{TelemedId}', ['as'=> 'admin.telemed-user-pivots.index', 'uses' => 'Admin\TelemedUserPivotController@index']);
    Route::post('telemed-user-pivots/{TelemedId}', ['as'=> 'admin.telemed-user-pivots.store', 'uses' => 'Admin\TelemedUserPivotController@store']);
    Route::get('telemed-user-pivots/{TelemedId}/create', ['as'=> 'admin.telemed-user-pivots.create', 'uses' => 'Admin\TelemedUserPivotController@create']);
    Route::put('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'admin.telemed-user-pivots.update', 'uses' => 'Admin\TelemedUserPivotController@update']);
    Route::patch('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'admin.telemed-user-pivots.update', 'uses' => 'Admin\TelemedUserPivotController@update']);
    Route::delete('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'admin.telemed-user-pivots.destroy', 'uses' => 'Admin\TelemedUserPivotController@destroy']);
    Route::get('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}', ['as'=> 'admin.telemed-user-pivots.show', 'uses' => 'Admin\TelemedUserPivotController@show']);
    Route::get('telemed-user-pivots/{TelemedId}/{TelemedUserPivot}/edit', ['as'=> 'admin.telemed-user-pivots.edit', 'uses' => 'Admin\TelemedUserPivotController@edit']);



    Route::get('news', ['as'=> 'admin.news.index', 'uses' => 'Admin\NewsController@index']);
    Route::post('news', ['as'=> 'admin.news.store', 'uses' => 'Admin\NewsController@store']);
    Route::get('news/create', ['as'=> 'admin.news.create', 'uses' => 'Admin\NewsController@create']);
    Route::put('news/{News}', ['as'=> 'admin.news.update', 'uses' => 'Admin\NewsController@update']);
    Route::patch('news/{News}', ['as'=> 'admin.news.update', 'uses' => 'Admin\NewsController@update']);
    Route::delete('news/{News}', ['as'=> 'admin.news.destroy', 'uses' => 'Admin\NewsController@destroy']);
    Route::get('news/{News}', ['as'=> 'admin.news.show', 'uses' => 'Admin\NewsController@show']);
    Route::get('news/{News}/edit', ['as'=> 'admin.news.edit', 'uses' => 'Admin\NewsController@edit']);


    Route::get('external-resources', ['as'=> 'admin.external-resources.index', 'uses' => 'Admin\ExternalResourceController@index']);
    Route::post('external-resources', ['as'=> 'admin.external-resources.store', 'uses' => 'Admin\ExternalResourceController@store']);
    Route::get('external-resources/create', ['as'=> 'admin.external-resources.create', 'uses' => 'Admin\ExternalResourceController@create']);
    Route::put('external-resources/{ExternalResource}', ['as'=> 'admin.external-resources.update', 'uses' => 'Admin\ExternalResourceController@update']);
    Route::patch('external-resources/{ExternalResource}', ['as'=> 'admin.external-resources.update', 'uses' => 'Admin\ExternalResourceController@update']);
    Route::delete('external-resources/{ExternalResource}', ['as'=> 'admin.external-resources.destroy', 'uses' => 'Admin\ExternalResourceController@destroy']);
    Route::get('external-resources/{ExternalResource}', ['as'=> 'admin.external-resources.show', 'uses' => 'Admin\ExternalResourceController@show']);
    Route::get('external-resources/{ExternalResource}/edit', ['as'=> 'admin.external-resources.edit', 'uses' => 'Admin\ExternalResourceController@edit']);

    Route::get('social-statuses', ['as'=> 'admin.social-statuses.index', 'uses' => 'Admin\SocialStatusController@index']);
    Route::post('social-statuses', ['as'=> 'admin.social-statuses.store', 'uses' => 'Admin\SocialStatusController@store']);
    Route::get('social-statuses/create', ['as'=> 'admin.social-statuses.create', 'uses' => 'Admin\SocialStatusController@create']);
    Route::put('social-statuses/{SocialStatus}', ['as'=> 'admin.social-statuses.update', 'uses' => 'Admin\SocialStatusController@update']);
    Route::patch('social-statuses/{SocialStatus}', ['as'=> 'admin.social-statuses.update', 'uses' => 'Admin\SocialStatusController@update']);
    Route::delete('social-statuses/{SocialStatus}', ['as'=> 'admin.social-statuses.destroy', 'uses' => 'Admin\SocialStatusController@destroy']);
    Route::get('social-statuses/{SocialStatus}', ['as'=> 'admin.social-statuses.show', 'uses' => 'Admin\SocialStatusController@show']);
    Route::get('social-statuses/{SocialStatus}/edit', ['as'=> 'admin.social-statuses.edit', 'uses' => 'Admin\SocialStatusController@edit']);

    Route::get('consultation-types', ['as'=> 'admin.consultation-types.index', 'uses' => 'Admin\ConsultationTypeController@index']);
    Route::post('consultation-types', ['as'=> 'admin.consultation-types.store', 'uses' => 'Admin\ConsultationTypeController@store']);
    Route::get('consultation-types/create', ['as'=> 'admin.consultation-types.create', 'uses' => 'Admin\ConsultationTypeController@create']);
    Route::put('consultation-types/{ConsultationType}', ['as'=> 'admin.consultation-types.update', 'uses' => 'Admin\ConsultationTypeController@update']);
    Route::patch('consultation-types/{ConsultationType}', ['as'=> 'admin.consultation-types.update', 'uses' => 'Admin\ConsultationTypeController@update']);
    Route::delete('consultation-types/{ConsultationType}', ['as'=> 'admin.consultation-types.destroy', 'uses' => 'Admin\ConsultationTypeController@destroy']);
    Route::get('consultation-types/{ConsultationType}', ['as'=> 'admin.consultation-types.show', 'uses' => 'Admin\ConsultationTypeController@show']);
    Route::get('consultation-types/{ConsultationType}/edit', ['as'=> 'admin.consultation-types.edit', 'uses' => 'Admin\ConsultationTypeController@edit']);



    Route::get('polls/{TelemedId}', ['as'=> 'admin.polls.index', 'uses' => 'PollController@index']);
    Route::post('polls/{TelemedId}', ['as'=> 'admin.polls.store', 'uses' => 'PollController@store']);
    Route::get('polls/create/{TelemedId}', ['as'=> 'admin.polls.create', 'uses' => 'PollController@create']);
    Route::put('polls/{TelemedId}/{Poll}', ['as'=> 'admin.polls.update', 'uses' => 'PollController@update']);
    Route::patch('polls/{TelemedId}/{Poll}', ['as'=> 'admin.polls.update', 'uses' => 'PollController@update']);
    Route::delete('polls/{TelemedId}/{Poll}', ['as'=> 'admin.polls.destroy', 'uses' => 'PollController@destroy']);
    Route::get('polls/{TelemedId}/{Poll}', ['as'=> 'admin.polls.show', 'uses' => 'PollController@show']);
    Route::get('polls/{TelemedId}/{Poll}/edit', ['as'=> 'admin.polls.edit', 'uses' => 'PollController@edit']);



    Route::get('slides', ['as'=> 'admin.slides.index', 'uses' => 'Admin\SlideController@index']);
    Route::post('slides', ['as'=> 'admin.slides.store', 'uses' => 'Admin\SlideController@store']);
    Route::get('slides/create', ['as'=> 'admin.slides.create', 'uses' => 'Admin\SlideController@create']);
    Route::put('slides/{Slide}', ['as'=> 'admin.slides.update', 'uses' => 'Admin\SlideController@update']);
    Route::patch('slides/{Slide}', ['as'=> 'admin.slides.update', 'uses' => 'Admin\SlideController@update']);
    Route::delete('slides/{Slide}', ['as'=> 'admin.slides.destroy', 'uses' => 'Admin\SlideController@destroy']);
    Route::get('slides/{Slide}', ['as'=> 'admin.slides.show', 'uses' => 'Admin\SlideController@show']);
    Route::get('slides/{Slide}/edit', ['as'=> 'admin.slides.edit', 'uses' => 'Admin\SlideController@edit']);


    // Statistics
    Route::get('statistics', ['as'=> 'admin.statistics.index', 'uses' => 'Admin\StatisticsController@index']);
    Route::get('stats/most-viewed', [ 'as' => 'stats.most-viewed', 'uses' =>'Stats@mostViewed' ]);
    Route::get('stats/api/paths', [ 'as' => 'tracker.stats.api.paths', 'uses' =>'Stats@apiPaths' ]);
    // Analytics
    Route::get('analytics', [ 'as' => 'admin.analytics.pageviews', 'uses' =>'Admin\AnalyticsController@index' ]);
    // Вывод графика
    Route::get('analytics/charts', [ 'as' => 'admin.analytics.charts', 'uses' =>'Admin\AnalyticsController@charts' ]);
    
    Route::get('analytics/online-users/{type?}', [ 'as' => 'admin.analytics.online-users', 'uses' =>'Admin\AnalyticsController@onlineUsers' ]);
    Route::get('analytics/pages', [ 'as' => 'admin.analytics.pages', 'uses' =>'Admin\AnalyticsController@pageViews' ]);

    // Блоки 
    Route::get('blocks', ['as'=> 'admin.blocks.index', 'uses' => 'Admin\BlockController@index']);
    Route::post('blocks', ['as'=> 'admin.blocks.store', 'uses' => 'Admin\BlockController@store']);
    Route::get('blocks/create', ['as'=> 'admin.blocks.create', 'uses' => 'Admin\BlockController@create']);
    Route::put('blocks/{Block}', ['as'=> 'admin.blocks.update', 'uses' => 'Admin\BlockController@update']);
    Route::patch('blocks/{Block}', ['as'=> 'admin.blocks.update', 'uses' => 'Admin\BlockController@update']);
    Route::delete('blocks/{Block}', ['as'=> 'admin.blocks.destroy', 'uses' => 'Admin\BlockController@destroy']);
    Route::get('blocks/{Block}', ['as'=> 'admin.blocks.show', 'uses' => 'Admin\BlockController@show']);
    Route::get('blocks/{Block}/edit', ['as'=> 'admin.blocks.edit', 'uses' => 'Admin\BlockController@edit']);

    // Интерфейс 
    Route::get('interfaces', ['as'=> 'admin.interfaces.index', 'uses' => 'Admin\InterfaceController@index']);
    Route::get('interfaces/{id}/edit', ['as'=> 'admin.interfaces.edit', 'uses' => 'Admin\InterfaceController@edit']);
    Route::patch('interfaces/{id}', ['as'=> 'admin.interfaces.update', 'uses' => 'Admin\InterfaceController@update']);
    Route::get('interfaces/{id}/edit-users', [ 'as' => 'admin.interfaces.edit-users', 'uses' =>'Admin\InterfaceController@editUsers' ]);
    Route::post('interfaces/update-users/{id}', ['as'=> 'admin.interfaces.update-users', 'uses' => 'Admin\InterfaceController@updateUsers']);

    Route::get('indication-for-uses', ['as'=> 'admin.indication-for-uses.index', 'uses' => 'Admin\IndicationForUseController@index']);
    Route::post('indication-for-uses', ['as'=> 'admin.indication-for-uses.store', 'uses' => 'Admin\IndicationForUseController@store']);
    Route::get('indication-for-uses/create', ['as'=> 'admin.indication-for-uses.create', 'uses' => 'Admin\IndicationForUseController@create']);
    Route::put('indication-for-uses/{IndicationForUse}', ['as'=> 'admin.indication-for-uses.update', 'uses' => 'Admin\IndicationForUseController@update']);
    Route::patch('indication-for-uses/{IndicationForUse}', ['as'=> 'admin.indication-for-uses.update', 'uses' => 'Admin\IndicationForUseController@update']);
    Route::delete('indication-for-uses/{IndicationForUse}', ['as'=> 'admin.indication-for-uses.destroy', 'uses' => 'Admin\IndicationForUseController@destroy']);
    Route::get('indication-for-uses/{IndicationForUse}', ['as'=> 'admin.indication-for-uses.show', 'uses' => 'Admin\IndicationForUseController@show']);
    Route::get('indication-for-uses/{IndicationForUse}/edit', ['as'=> 'admin.indication-for-uses.edit', 'uses' => 'Admin\IndicationForUseController@edit']);



    Route::get('generate-sitemap', ['as'=> 'admin.generate-sitemap', 'uses' => function(){
        $sitemap = App::make("sitemap");




        $sitemap->add(URL::to('/'), null, 1,  'weekly');
        $sitemap->add(URL::to('/news'), null, 0.8,  'dayly');

        $pages = \App\Models\Admin\Page::published()->get();
        foreach ($pages as $page)
        {
            $sitemap->add(URL::to($page->slug), $page->updated_at, 0.7, 'monthly');
        }

        $news = \App\Models\Admin\News::published()->get();
        foreach ($news as $article)
        {
            $sitemap->add(URL::to('/news/'.$article->slug), $article->updated_at, 0.7, null);
        }

        //return response()->json($sitemap);
        $sitemap->store('xml','sitemap');
        return $sitemap->render('xml');
    }]);
    Route::get('doctors/{TelemedId}', ['as'=> 'admin.doctors.index', 'uses' => 'Admin\DoctorController@index']);
    Route::post('doctors/{TelemedId}', ['as'=> 'admin.doctors.store', 'uses' => 'Admin\DoctorController@store']);
    Route::get('doctors/{TelemedId}/create', ['as'=> 'admin.doctors.create', 'uses' => 'Admin\DoctorController@create']);
    Route::put('doctors/{TelemedId}/{Doctor}', ['as'=> 'admin.doctors.update', 'uses' => 'Admin\DoctorController@update']);
    Route::patch('doctors/{TelemedId}/{Doctor}', ['as'=> 'admin.doctors.update', 'uses' => 'Admin\DoctorController@update']);
    Route::delete('doctors/{TelemedId}/{Doctor}', ['as'=> 'admin.doctors.destroy', 'uses' => 'Admin\DoctorController@destroy']);
    Route::get('doctors/{TelemedId}/{Doctor}', ['as'=> 'admin.doctors.show', 'uses' => 'Admin\DoctorController@show']);
    Route::get('doctors/{TelemedId}/{Doctor}/edit', ['as'=> 'admin.doctors.edit', 'uses' => 'Admin\DoctorController@edit']);
 
});

Route::group(['middleware' => ['guest']], function () {


    /* Пользователь забыл пароль и запросил сброс пароля. Это начало процесса -
    Сервисная страничка, показываем после заполнения рег формы, формы сброса и т.
    о том, что письмо отправлено и надо заглянуть в почтовый ящик. */
    Route::get('wait',['as'=>'wait','uses'=> 'AuthController@wait']);


    // Пользователь заполнил и отправил форму с E-Mail в запросе на сброс пароля
    Route::post('reset',['as'=>'reset','uses'=> 'AuthController@resetOrderProcess']);
    // Страница с запросом E-Mail пользователя
    Route::get('reset',  ['as' => 'reset-post', 'uses' => 'AuthController@resetOrder']);

// Пользователю пришло письмо со ссылкой на эту страницу для ввода нового пароля
    Route::get('reset/{id}/{code}',['as'=>'reset-code','uses'=> 'AuthController@resetComplete']);
// Пользователь ввел новый пароль и отправил.
    Route::post('reset/{id}/{code}',['as'=>'reset-code-post','uses'=> 'AuthController@resetCompleteProcess']);

    // Вызов страницы регистрации пользователя
    Route::get('register', ['as' => 'register', 'uses' => 'AuthController@register']);
// Пользователь заполнил форму регистрации и отправил
    Route::post('register', ['as' => 'register', 'uses' => 'AuthController@registerProcess']);
// Пользователь получил письмо для активации аккаунта со ссылкой сюда
    Route::get('activate/{id}/{code}',['as'=>'activate-code','uses'=> 'AuthController@activate']);

    // Вызов страницы авторизации
    Route::get('login',  ['as' => 'login', 'uses' => 'AuthController@login']);

    // Пользователь заполнил форму авторизации и отправил

  /*  // Password Reset Routes...
    Route::get('password/reset', 'AuthController@resetOrder');
    Route::post('password/email', 'AuthController@resetOrderProcess');
    Route::get('password/reset/{token}', 'AuthController@resetComplete');
    Route::post('password/reset', 'AuthController@resetCompleteProcess');*/


});
Route::post('login',  ['as' => 'login', 'uses' => 'AuthController@loginProcess']);
Route::get('relogin',  ['as' => 'relogin', 'uses' => function(){
    Sentinel::logout();
    return redirect()->route('login');
}]);
Route::get('/', ['as' => 'homepage', 'uses' => 'IndexController@index']);

Route::get('news', ['as'=> 'news', 'uses' => 'NewsController@index']);
Route::get('search', ['as'=> 'search', 'uses' => 'NewsController@search']);
Route::get('news/{slug}', ['as'=> 'news.view', 'uses' => 'NewsController@view']);


//Route::get('devel', 'DevelController@index');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('{pageSlug}', ['as' => 'page.view', 'uses' => 'PageController@view']);


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/





