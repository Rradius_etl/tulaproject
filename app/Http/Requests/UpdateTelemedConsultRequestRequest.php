<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\Admin\TelemedConsultAbonentSide;
class UpdateTelemedConsultRequestRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'created_time' => 'sometimes | required | date_format:Y-m-d',
            'doctor_fullname' => 'required',
            'med_care_fac_id' => 'required',
            'desired_date' => 'date_format:d.m.Y',
            'desired_time' => 'date_format:G:i',
            'consult_type' => 'required',
            'medical_profile' => 'required',

            'patient_birthday' => 'required|date_format:d.m.Y',
            'patient_indications_for_use_other' => 'required_if:patient_indications_for_use,other',
            'patient_indications_for_use_id' => 'required',
            'patient_uid_or_fname' => 'required',
            'coordinator_fullname' => 'required',
            'coordinator_phone' => 'required',

        ];
    }
}
