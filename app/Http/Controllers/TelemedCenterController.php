<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedOtherCriteria;
use App\Criteria\UserTelemedCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedCenterRequest;
use App\Http\Requests\Admin\UpdateTelemedCenterRequest;
use App\Repositories\TelemedCenterRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use App\Criteria\UserCriteria;

class TelemedCenterController extends InfyOmBaseController
{
    /** @var  TelemedCenterRepository */
    private $telemedCenterRepository;

    public function __construct(TelemedCenterRepository $telemedCenterRepo)
    {
        $this->middleware(["auth","is_user_of_telemed"]);
        $this->telemedCenterRepository = $telemedCenterRepo;
        $this->user = Sentinel::getUser();
        $crit = new UserTelemedCriteria();
            $myTmcs = \TmcRoleManager::getTmcs();
        if(count($myTmcs)) {
            $tmc = array_pluck($myTmcs,'id');
            $crit->setTelemedId($tmc);
            $this->crit=$crit;
            $this->telemedCenterRepository->pushCriteria($this->crit);
        }

    }

    /**
     * Список моих ТМЦ
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $this->telemedCenterRepository->pushCriteria(new RequestCriteria($request));
        $telemedCenters = $this->telemedCenterRepository->paginate(30);

        return view('telemed_centers.index',['search'=>$search])
            ->with('telemedCenters', $telemedCenters);
    }



    /** Подробности об моем ТМЦ */
    public function show($id)
    {
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        if (empty($telemedCenter)) {
            Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found') );

            return redirect(route('telemed-centers.index'));
        }

        return view('telemed_centers.show')->with('telemedCenter', $telemedCenter);
    }

    /**
     * Show the form for editing the specified TelemedCenter.
     *
     * @param  int $id
     *
     * @return Response
     */


    /** Страница редактирование ТМЦ */
    public function edit($id)
    {
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        if (empty($telemedCenter)) {
             Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found') );

            return redirect(route('telemed-centers.index'));
        }

        return view('telemed_centers.edit')->with('telemedCenter', $telemedCenter);
    }

    /**
     * Update the specified TelemedCenter in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedCenterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedCenterRequest $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'address'=>'required',
                'director_fullname'=>'required',
                'coordinator_fullname'=>'required',
                'coordinator_phone'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'tech_specialist_contacts'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'equipment_location'=>'required',
                'terminal_name'=>'required',
                'terminal_number'=>'required|integer'
            ]);
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);
        if (empty($telemedCenter)) {
             Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found') );

            return redirect(route('telemed-centers.index'));
        }

        $telemedCenter = $this->telemedCenterRepository->update($request->all(), $id);

       Flash::success(trans('backend.TelemedCenter') . trans('backend.success_updated'));

        return redirect(route('telemed-centers.index'));
    }


    public function TelemedCentersList()
    {
        
    }

}
