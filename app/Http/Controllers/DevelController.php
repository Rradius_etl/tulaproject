<?php

namespace App\Http\Controllers;


use App\Models\Admin\ConsultationType;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\SocialStatus;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\Admin\TelemedEventParticipant;
use App\Models\Admin\TelemedRequest;
use App\Models\Admin\TelemedUserPivot;
use App\Models\Doctor;
use App\Models\TelemedConsultAbonentSide;
use App\Models\TelemedConsultConsultantSide;

use App\Models\User as MyUser;
use Sentinel;
use Faker\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;

/** Генерация тестовых данных */
class DevelController extends Controller
{
    public function index()
    {
        $all_events = [];
        $dolzhnosti = ["Главныйврач", "зав.отделением", "врач-специалист", "хирург", "педиатр", "терапевт", "аллерголог", "эндокринолог", "анестезиолог", "медсестра", "сестра-хозяйка", "санитарка", "вахтер", "лифтер", "сантехник", "повар", "помощникпокухне", "бухгалтер", "завхоз"];
        $faker = Factory::create('ru_RU');;
        $forUse = ['diagnose_and_treatment', 'patient_consultation', 'hospital_possibility', 'meddata_equipment_decode', 'other'];

        $soc_statuses = SocialStatus::all()->pluck('id')->toArray();

        $tmp = [];
        $users_array = MyUser::all()->pluck('id')->toArray();
        $tmc_array = TelemedCenter::all()->pluck('id')->toArray();
        $doctors = Doctor::all()->pluck('id')->toArray();
        //return response()->json($users_array);
        $importances = ['low', 'medium', 'high'];
        $event_types = ['meeting', 'seminar', 'consultation'];
        $consult_types =  ConsultationType::select('name','id')->get()->pluck('name', 'id');
        $genders = ['male', 'female'];
        $decisions = ['pending', 'agreed', 'rejected']; //
        foreach (range(0, 130) as $value) {
            $event = new TelemedRequest();
            $event->initiator_id = Sentinel::getUser()->id;// $users_array[array_rand($users_array, 1)]; //
            $event->title = $faker->text(rand(5, 15));
            $event->decision = $decisions[array_rand($decisions, 1)];
            $event->importance = $importances[array_rand($importances, 1)];
            $event->event_type = $event_types[array_rand($event_types, 1)];

            $event->start_date = $faker->dateTimeBetween($startDate = '+33 days', $endData = '+64 days');
            $end = clone $event->start_date;
            $event->end_date = $end->add(new \DateInterval('PT1H'));

            /** Определить Телемед центр  **/
            $tcid = \TmcRoleManager::getTmcs()[0]['id'];

            if (!is_null($tcid)) {

                $med_care_fac = TelemedCenter::find($tcid)->medCareFac;


                if ($tcid != null) {

                    $event->telemed_center_id = $tcid;
                    $is_saved = $event->save();
                    /* bool*/
                    $part = new TelemedEventParticipant();
                    $part->request_id = $event->id;
                    $part->telemed_center_id = $tmc_array[array_rand($tmc_array, 1)];
                    $part->save();
                } else {
                    $mcf = MedCareFac::where('user_id', $event->initiator_id)->first();

                    if (!is_null($mcf)) {
                        $tmcs = $mcf->telemedCenters;
                        if (count($tmcs) > 0) {
                            $event->telemed_center_id = $tmcs[0]->id;
                            $is_saved = $event->save();
                            /* bool*/
                            $part = new TelemedEventParticipant();
                            $part->request_id = $event->id;
                            $part->telemed_center_id = $tmc_array[array_rand($tmc_array, 1)];
                            $part->save();
                        }
                    }
                }  /* ... Определить Телемед центр  **/

                array_push($all_events, $event);


                /* Abonent part */
                if ($event->event_type == 'consultation') {
                    $prevModel = TelemedConsultRequest::orderBy('uid_id', 'desc')->first();
                    $prevCons = is_null($prevModel) ? 1 : $prevModel->uid_id;
                    $consreq = new TelemedConsultRequest();
                    $consreq->request_id = $event->id;
                    $consreq->uid_year = 2016;
                    $consreq->uid_id = $prevCons + 1;
                    $consreq->uid_code = $med_care_fac->code;
                    $consreq->comment = $faker->sentence;
                    $consreq->decision = $event->decision;
                    $consreq->completed = rand(0, 1);
                    $consreq->med_care_fac_id = $med_care_fac->id;
                    $consreq->telemed_center_id = $event->telemed_center_id;
                    $consreq->save();


                    $abpart = new TelemedConsultAbonentSide();
                    $abpart->request_id = $event->id;
                    $abpart->telemed_center_id = $part->telemed_center_id;
                    $abpart->doctor_fullname = $faker->name;
                    $abpart->consult_type = $consult_types[array_rand($consult_types, 1)];
                    $abpart->doctor_spec = $dolzhnosti[array_rand($dolzhnosti, 1)];
                    $abpart->med_care_fac_id = $med_care_fac->id;
                    $abpart->desired_consultant = $faker->name;
                    $abpart->patient_uid_or_fname = $faker->name;
                    $abpart->patient_address = $faker->streetAddress;
                    $abpart->patient_social_status = $soc_statuses[array_rand($soc_statuses, 1)];;
                    $abpart->patient_indications_for_use = $forUse[array_rand($forUse, 1)];;
                    $abpart->patient_questions = $faker->sentence . '?';


                    $abpart->patient_birthday = $faker->dateTimeBetween($startDate = '-60 years', $endData = '-30 years');
                    $abpart->patient_gender = $genders[array_rand($genders, 1)];

                    $abpart->desired_date = $faker->dateTimeBetween($startDate = '+33 days', $endData = '+90 days');
                    $abpart->desired_time = $faker->time($format = 'H:i:s', $max = '23:59:59');

                    $abpart->save();


                    /** Create consultant part */
                    $conspart = new TelemedConsultConsultantSide;
                    $conspart->request_id = $event->id;
                    $conspart->responsible_person_fullname = $faker->name;
                    $conspart->appointed_person_id = null;
                    $planned_date_end = $faker->dateTimeBetween($startDate = $abpart->desired_date, $endData = clone $abpart->desired_date->add(new \DateInterval('PT380H')));
                    $conspart->planned_date = $planned_date_end;
                    $conspart->doctor_id = $doctors[array_rand($doctors, 1)];
                    $conspart->coordinator_fullname = $faker->name;

                    $conspart->save();

                }

            } else {
                return 'TelemedUserPivot не найден';
            }


        }

        return response()->json(['msg' => 'success 111', 'data' => $all_events]);

        $all = [];
        $all_mcf = MedCareFac::all()->pluck('id')->toArray();
        //return response()->json($all_mcf);
        foreach (range(0, 100) as $value) {
            $tmc = new TelemedCenter();
            $tmc->name = $faker->company;

            $tmc->med_care_fac_id = $all_mcf[array_rand($all_mcf, 1)];
            $tmc->director_fullname = $faker->name;
            $tmc->coordinator_fullname = $faker->name;
            $tmc->coordinator_phone = $faker->phoneNumber;
            $tmc->tech_specialist_fullname = $faker->name;
            $tmc->tech_specialist_phone = $faker->phoneNumber;
            $tmc->tech_specialist_contacts = $faker->address;
            $tmc->videoconf_equipment = $faker->domainWord;
            $tmc->digit_img_demonstration = rand(0, 1);
            $tmc->equipment_location = $faker->address;
            $tmc->save();

            $user = [
                'email' => $faker->email,
                'password' => 'qwerty',
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'middlename' => $faker->lastName,
                'position' => $dolzhnosti[array_rand($dolzhnosti, 1)]
            ];

            $res = \Sentinel::registerAndActivate($user);
            /* PIVOT */
            $pivot = new TelemedUserPivot();
            $pivot->user_id = $res->id;
            $pivot->telemed_center_id = $tmc->id;
            $pivot->value = 'coordinator';
            $pivot->save();


            $user = [
                'email' => $faker->email,
                'password' => 'qwerty',
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'middlename' => $faker->lastName,
                'position' => $dolzhnosti[array_rand($dolzhnosti, 1)]
            ];

            $res = \Sentinel::registerAndActivate($user);
            /* PIVOT */
            $pivot = new TelemedUserPivot();
            $pivot->user_id = $res->id;
            $pivot->telemed_center_id = $tmc->id;
            $pivot->value = 'admin';
            $pivot->save();


        };
        foreach (MedCareFac::all() as $value) {
            if ($value->user_id == null) {
                $user = [
                    'email' => $faker->email,
                    'password' => 'qwerty',
                    'name' => $faker->firstName,
                    'surname' => $faker->lastName,
                    'middlename' => $faker->lastName,
                    'position' => $dolzhnosti[array_rand($dolzhnosti, 1)]
                ];

                $res = \Sentinel::registerAndActivate($user);

                $value->user_id = $res->id;
                $value->save();

            }
        }


        return response()->json('success');

    }
}
