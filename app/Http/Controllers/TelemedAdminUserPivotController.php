<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedUserRoles;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedUserPivotRequest;
use App\Http\Requests\Admin\UpdateTelemedUserPivotRequest;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedUserPivot;
use App\Models\User;
use App\Repositories\Admin\TelemedUserPivotRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Cache;
/** Все что связанное к привязанию пользователей к ТМЦ от админа ТМЦ */
class TelemedAdminUserPivotController extends InfyOmBaseController
{
    /** @var  TelemedUserPivotRepository */
    private $telemedUserPivotRepository;

    public function __construct(TelemedUserPivotRepository $telemedUserPivotRepo)
    {
        $this->middleware(["auth","has_access_to_edit_tmc"]);
        $this->telemedUserPivotRepository = $telemedUserPivotRepo;
        $this->users = User::pluck('email','id');
        $roles = ["coordinator"=>"Координатор"];
        if(\TmcRoleManager::hasRole("mcf_admin"))
        {
            $roles['admin'] = "Администратор";
        }
        $this->roles = $roles;
    }

    /**
     * Display a listing of the TelemedUserPivot.
     *
     * @param Request $request
     * @return Response
     */
    public function index($TelemedId,Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);

        $this->telemedUserPivotRepository->pushCriteria(new RequestCriteria($request));
        $telemedUserPivots = $this->telemedUserPivotRepository->all();

        return view('telemed_user_pivots.index',['TelemedId'=>$TelemedId,'search'=>$search])
            ->with('telemedUserPivots', $telemedUserPivots);
    }

    /**
     * Show the form for creating a new TelemedUserPivot.
     *
     * @return Response
     */
    public function create($TelemedId)
    {
        return view('telemed_user_pivots.create',['TelemedId'=>$TelemedId,'users'=>$this->users,'roles'=>$this->roles]);
    }

    /**
     * Store a newly created TelemedUserPivot in storage.
     *
     * @param CreateTelemedUserPivotRequest $request
     *
     * @return Response
     */
    public function store($TelemedId,CreateTelemedUserPivotRequest $request)
    {
        $this->validate($request, [
            'value' => 'in:admin,coordinator|required',
            'user_id' => 'required',
        ]);
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);

        $input = $request->all();
        
        if($input['value'] == 'admin')
        {
            if(!\TmcRoleManager::hasRole("mcf_admin"))
            {
                Flash::error("Вы не имеете доступа создавать или редактировать роль администратора ТМЦ!");
                return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
            }
        }
        $res = TelemedUserPivot::check($input['user_id'],$TelemedId, $input['value']);

        if(!isset($res['error']))
        {

        }
        else
        {
            Flash::error($res['error']);
            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        

        if($this->telemedUserPivotRepository->create($input))
        {
            Cache::forget("userroles".$input['user_id']);
            Cache::forget("usertmcs".$input['user_id']);
        }

        Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_saved'));

        return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }

    /**
     * Display the specified TelemedUserPivot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($TelemedId,$id)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
            Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        return view('telemed_user_pivots.show',['TelemedId'=>$TelemedId])->with('telemedUserPivot', $telemedUserPivot);
    }

    /**
     * Show the form for editing the specified TelemedUserPivot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($TelemedId,$id)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
             Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('telemed-user-pivots.index'));
        }

        return view('telemed_user_pivots.edit',['TelemedId'=>$TelemedId,'users'=>$this->users,'telemedUserPivot'=>$telemedUserPivot,'roles'=>$this->roles]);
    }

    /**
     * Update the specified TelemedUserPivot in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedUserPivotRequest $request
     *
     * @return Response
     */
    public function update($TelemedId,$id, UpdateTelemedUserPivotRequest $request)
    {
        $this->validate($request, [
            'value' => 'in:admin,coordinator|required',
            'user_id' => 'required',
        ]);
        if($request->get('value') == 'admin')
        {
            if(!\TmcRoleManager::hasRole("mcf_admin"))
            {
                Flash::error("Вы не имеете доступа создавать или редактировать роль администратора ТМЦ!");
                return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
            }
        }

        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);



        if (empty($telemedUserPivot)) {
             Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );
            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }



        $input = $request->all();



        $res = TelemedUserPivot::check($input['user_id'],$TelemedId, $input['value']);

        if(!isset($res['error']))
        {

        }
        else
        {
            Flash::error($res['error']);
            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        $prevuser = $telemedUserPivot->user_id;
        if($this->telemedUserPivotRepository->update($request->all(), $id));
        {
            Cache::forget("userroles".$prevuser);
            Cache::forget("userroles".$request->all()['user_id']);
            Cache::forget("usertmcs".$request->all()['user_id']);
            Cache::forget("usertmcs".$prevuser);
        }
       Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_updated'));

        return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }

    /**
     * Remove the specified TelemedUserPivot from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($TelemedId,$id)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);

        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
            Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );
            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }
        if(!\TmcRoleManager::hasRole("mcf_admin")&&$telemedUserPivot->value == "admin")
        {
            Flash::error("Вы не имеете доступа создавать или редактировать роль администратора ТМЦ!");
            return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        $this->telemedUserPivotRepository->delete($id);
        Cache::forget("userroles".$telemedUserPivot->user_id);
        Cache::forget("usertmcs".$telemedUserPivot->user_id);
        Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_deleted'));

        return redirect(route('telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }
}
