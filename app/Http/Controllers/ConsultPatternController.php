<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedOtherCriteria;
use App\Criteria\TelemedPatternCriteria;
use App\Criteria\TelemedUserRoles;
use App\Http\Requests;
use App\Http\Requests\CreateConsultPatternRequest;
use App\Http\Requests\UpdateConsultPatternRequest;
use App\Models\Admin\ConsultationType;
use App\Models\Admin\IndicationForUse;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\MedicalProfile;
use App\Models\Admin\MedicalProfileCategory;
use App\Models\Admin\SocialStatus;
use App\Models\TelemedCenter;
use App\Repositories\ConsultPatternRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/** КОнтроллер для шаблонов для конльтации */
class ConsultPatternController extends InfyOmBaseController
{
    /** @var  ConsultPatternRepository */
    private $consultPatternRepository;

    private static $rules = [
        'request_id' => 'required',
        'created_time' => 'date_format:Y-m-d',
        'doctor_fullname' => 'required',
        'med_care_fac_id' => 'required',
        'patient_birthday' => 'required|date_format:d.m.Y',
        'desired_date' => 'date_format:d.m.Y',
        'desired_time' => 'date_format:G:i',
        'patient_indications_for_use_other' => 'required_if:patient_indications_for_use,other',
        'patient_indications_for_use' => 'required',
        'consult_type' => 'required',
        'consult_profile' => 'required',
        'patient_uid_or_fname' => 'required',
        'coordinator_fullname' => 'required',
        'coordinator_phone' => 'required',

    ];
    public static $genders = [
        'male' => 'мужской',
        'female' => 'женский',
    ];


    public $consult_types;

    public function __construct(ConsultPatternRepository $consultPatternRepo)
    {
        $this->middleware(["auth","is_user_of_telemed"]);
        $this->consultPatternRepository = $consultPatternRepo;
        $this->user = \Sentinel::check();
        $crit = new TelemedPatternCriteria();
        $crit->setTelemedId(\TmcRoleManager::getTmcs());
        $this->consultPatternRepository->pushCriteria($crit);
        $this->consult_types = $this->consult_types = ConsultationType::select('name','id')->get()->pluck('name', 'id');
    }

    /**
     * Display a listing of the ConsultPattern.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->consultPatternRepository->pushCriteria(new RequestCriteria($request));
        $consultPatterns = $this->consultPatternRepository->paginate(30);

        return view('consult_patterns.index')
            ->with('consultPatterns', $consultPatterns);
    }

    /**
     * Show the form for creating a new ConsultPattern.
     *
     * @return Response
     */
    public function create()
    {
        $tmc = TelemedCenter::find(\TmcRoleManager::getTmcs()[0]['id']);
        $mcf = $tmc->medCareFac;
        $mcfs = MedCareFac::where('id', '!=', $mcf->id)->pluck("name", "id");
        $socstatuses = SocialStatus::pluck('name', 'id');
        $genders = self::$genders;
        $consult_types = $this->consult_types;

        $medical_profiles_model = MedicalProfile::with('medicalProfileCategory')->get();
        $medical_profile_categories = MedicalProfileCategory::all()->pluck('name', 'id');
        $medical_profiles = [];
        $ind_for_use = IndicationForUse::getOptionValues();
        foreach ($medical_profiles_model as $item) {
            $key = $medical_profile_categories[$item->category_id];
            $medical_profiles[$key][$item->id] = $item->name;
        }

        return view('consult_patterns.create', compact('socstatuses','genders','consult_types','medical_profiles','ind_for_use','mcfs'));
    }

    /**
     * Store a newly created ConsultPattern in storage.
     *
     * @param CreateConsultPatternRequest $request
     *
     * @return Response
     */
    public function store(CreateConsultPatternRequest $request)
    {
        $input = $request->all();
        $input['telemed_center_id'] = \TmcRoleManager::getTmcs()[0]['id'];
        $consultPattern = $this->consultPatternRepository->create($input);

        Flash::success(trans('backend.ConsultPattern') . trans('backend.success_saved'));

        return redirect(route('consult-patterns.index'));
    }

    /**
     * Display the specified ConsultPattern.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $socstatuses = SocialStatus::pluck('name', 'id');
        $consultPattern = $this->consultPatternRepository->findWithoutFail($id);

        if (empty($consultPattern)) {
            Flash::error(trans('backend.ConsultPattern') . trans('backend.not_found') );

            return redirect(route('consult-patterns.index'));
        }

        return view('consult_patterns.show')->with('consultPattern', $consultPattern);
    }

    /**
     * Show the form for editing the specified ConsultPattern.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $consultPattern = $this->consultPatternRepository->findWithoutFail($id);

        if (empty($consultPattern)) {
             Flash::error(trans('backend.ConsultPattern') . trans('backend.not_found') );

            return redirect(route('consult-patterns.index'));
        }

        $tmc = TelemedCenter::find(\TmcRoleManager::getTmcs()[0]['id']);
        $mcf = $tmc->medCareFac;
        $mcfs = MedCareFac::where('id', '!=', $mcf->id)->pluck("name", "id");
        $socstatuses = SocialStatus::pluck('name', 'id');
        $genders = self::$genders;
        $consult_types = $this->consult_types;

        $medical_profiles_model = MedicalProfile::with('medicalProfileCategory')->get();
        $medical_profile_categories = MedicalProfileCategory::all()->pluck('name', 'id');
        $medical_profiles = [];
        $ind_for_use = IndicationForUse::getOptionValues();
        foreach ($medical_profiles_model as $item) {
            $key = $medical_profile_categories[$item->category_id];
            $medical_profiles[$key][$item->id] = $item->name;
        }
        \Former::populate($consultPattern);
        
        return view('consult_patterns.edit', compact('socstatuses','genders','consult_types','medical_profiles','ind_for_use','mcfs'))->with('consultPattern', $consultPattern);
    }

    /**
     * Update the specified ConsultPattern in storage.
     *
     * @param  int              $id
     * @param UpdateConsultPatternRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConsultPatternRequest $request)
    {
        $consultPattern = $this->consultPatternRepository->findWithoutFail($id);

        if (empty($consultPattern)) {
             Flash::error(trans('backend.ConsultPattern') . trans('backend.not_found') );

            return redirect(route('consult-patterns.index'));
        }
        $input = $request->all();
        $input['telemed_center_id'] = \TmcRoleManager::getTmcs()[0]['id'];
        $consultPattern = $this->consultPatternRepository->update($input, $id);

       Flash::success(trans('backend.ConsultPattern') . trans('backend.success_updated'));

        return redirect(route('consult-patterns.index'));
    }

    /**
     * Remove the specified ConsultPattern from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $consultPattern = $this->consultPatternRepository->findWithoutFail($id);

        if (empty($consultPattern)) {
            Flash::error(trans('backend.ConsultPattern') . trans('backend.not_found') );

            return redirect(route('consult-patterns.index'));
        }

        $this->consultPatternRepository->delete($id);

        Flash::success(trans('backend.ConsultPattern') . trans('backend.success_deleted'));

        return redirect(route('consult-patterns.index'));
    }
}
