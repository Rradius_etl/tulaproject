<?php

namespace App\Http\Controllers;

use App\Criteria\ReportRoleCriteria;
use App\Models\Admin\Role;
use App\Models\GraphicalReport;
use App\Models\TableReport;
use App\Http\Requests;
use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Models\Admin\TelemedCenter;
use App\Models\Report;
use App\Repositories\ReportRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReportController extends InfyOmBaseController
{
    /** @var  ReportRepository */
    private $reportRepository;

    protected static $format = 'Y-m-d';

    /** calculate range*/
    protected function getDateRange($request)
    {
        $format = self::$format;
        $defaults = [
            'from' => Carbon::now()->format($format),
            'to' => Carbon::now()->subWeek()->format($format)
        ];
        $from = Carbon::createFromFormat($format, $request->get('from', $defaults['from']))->startOfDay();
        $to = Carbon::createFromFormat($format, $request->get('to', $defaults['to']))->endOfDay();
        return [$from, $to];
    }

    public function __construct(ReportRepository $reportRepo)
    {
        $this->reportRepository = $reportRepo;
    }

    /**
     * Display a listing of the Report.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $user = \Sentinel::getUser();
        $roles = $user->roles->pluck('slug');

        $tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');
         

        $reportRoleCrit = new ReportRoleCriteria();
        $reportRoleCrit->setRoles($roles);
        $reportRoleCrit->setTmcs($tmcs);
        $this->reportRepository->pushCriteria($reportRoleCrit);
        $this->reportRepository->pushCriteria(new RequestCriteria($request));

        // Виды графиков или отчетов
        $reportTypes = Report::$reportTypes;
        $reports = $this->reportRepository->all();


        $my_tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');

        return view('reports.index', compact('my_tmcs', 'reportTypes'))
            ->with('reports', $reports);
    }



    /**
     *  Иинциализация отчета в виде таблицы
     **/
    public function initTable($id, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);
        $range = \Cache::get('report' . \Sentinel::getUser()->id);
        if ($range != null) {
            $ranges = json_decode($range, true);
        } else {
            $ranges = [];
        }
        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }

        return view('reports.init-table')->with('report', $report)->with("ranges", $ranges);
    }

    /**
     *  Иинциализация отчета в виде графика
     **/
    public function initGraphics($id, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);
        if (\Sentinel::check() == true)
            $range = \Cache::get('report' . \Sentinel::getUser()->id);
        if ($range != null) {
            $ranges = json_decode($range, true);
        } else {
            $ranges = [];
        }
        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }

        $fieldTypes = \Config::get("reports.fieldTypes.{$report->type}");
        $fields = \Config::get("reports.rows.{$report->type}");

        $textFields = [];

        $numericFields = [];

        foreach ($fields as $key => $val) {
            if ($fieldTypes[$key] == 'numeric') {
                $numericFields[$key] = $val;
            } elseif ($fieldTypes[$key] == 'text') {
                $textFields[$key] = $val;
            }
        }

        if (count($numericFields) < 1) {

            Flash::info('В отчетности по всем типам видеоконцеренции отсутствуют числовые поля, будет выводиться количество видеоконференции!');

            return view('reports.init-graphics', compact('numericFields', 'textFields', 'ranges'))
                ->with('report', $report)
                ->with('defaultNumericKey', null);
        } else {
            $keys = array_keys($numericFields);
            $defaultNumericKey = array_shift($keys);
            //return $defaultNumericKey;
            return view('reports.init-graphics', compact('numericFields', 'textFields', 'ranges'))
                ->with('report', $report)
                ->with('defaultNumericKey', $defaultNumericKey);
        }


    }

    /**
     * Вывести таблицу с отчетом
     * @param  int $id
     * @param Request $request
     *
     * @return Response
     **/
    public function renderTable($id, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }

        $range = self::getDateRange($request);
        $expiresAt = Carbon::now()->addDay();
        \Cache::put('report' . \Sentinel::getUser()->id, json_encode($range), $expiresAt);
        $tableReport = new TableReport($report);

        return $tableReport->render($range);
    }

    /**
     * Вывести график с возможностью сохранения
     * @param Request $request
     * @param  int $id
     *
     * @return Response
     **/
    public function renderCharts($id, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        $range = self::getDateRange($request);
        $chartType = $request->get('diagram', 'barchart');
        $params = [
            'report' => $report,
            'range' => $range,
            'text_field' => $request->get('text_field'),
            'numeric_field' => $request->get('numeric_field'),
            'chart_type' => $chartType
        ];
        $expiresAt = Carbon::now()->addDay();

        if (\Sentinel::check() == true)
            \Cache::put('report' . \Sentinel::getUser()->id, json_encode($range), $expiresAt);
        
        $graphicalReport = new GraphicalReport($params);

        return $graphicalReport->renderGraphic();
    }

    /**
     * Вывести график с возможностью сохранения
     * @param Request $request
     * @param  int $id
     *
     * @return Response
     **/
    public function downloadCharts($id, $fileFormat, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        $range = self::getDateRange($request);
        $chartType = $request->get('diagram', 'barchart');
        $params = [
            'report' => $report,
            'range' => $range,
            'text_field' => $request->get('text_field'),
            'numeric_field' => $request->get('numeric_field'),
            'chart_type' => $chartType
        ];

        $expiresAt = Carbon::now()->addDay();
        if (\Sentinel::check() == true)
            \Cache::put('report' . \Sentinel::getUser()->id, json_encode($range), $expiresAt);


        $graphicalReport = new GraphicalReport($params);

        return $graphicalReport->downloadGraphic($fileFormat);
    }


    /**
     *  Скачивание файлов очета
     **/
    public function downloadTable($id, $fileFormat, Request $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);
        $supportedFormats = ['docx', 'xls', 'pdf', 'odt'];
        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }
        if (!in_array($fileFormat, $supportedFormats)) {
            Flash::error('Поддерживается экспорт только в форматах ' . implode($supportedFormats, ', '));
            return redirect(route('reports.index'));
        }

        $range = self::getDateRange($request);

        $tableReport = new TableReport($report);


        return $tableReport->download($range, $fileFormat);
    }


    /**=================================**/

    /**
     * Show the form for editing the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }
        // Если прикреплен к ТМЦ запретить редактировать
        if($report->tmc_id == null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);

            Flash::error('Общие отчеты может править только Админ портала');

            return redirect(route('reports.index'));
        }
        // Если прикреплен к ТМЦ то проверяем доступ
        if($report->tmc_id != null) {
            if(!\Sentinel::inRole('mcf_admin')) {
                Flash::error('Отчеты ЛПУ могут править только админы ЛПУ');
                return redirect(route('reports.index'));
            } else {

                $tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');


                if(!in_array($report->tmc_id, $tmcs)) {

                    $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);

                    if($tmc == null) {
                        Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));
                    }

                    Flash::error("ТМЦ {$tmc->name} не относится к вашим ЛПУ" );

                    return redirect(route('reports.index'));
                }

            }

        }

        $reportType = $report->type;
        $reportRows = [];
        $reportHeaders = [];
        $defaultSelectedRows = json_decode($report->headers);
        $defaultTmcs = json_decode($report->tmcs);
        $reportRoles = json_decode($report->roles);
        $defaultRoles = $reportRoles == null ? [] : $reportRoles;
        if (!in_array('admin', $defaultRoles)) {
            $defaultRoles[] = 'admin';
        }
        //

        $rowNames = array_keys(\Config::get("reports.rows"));

        $my_tmcs =  array_pluck( \TmcRoleManager::getTmcs(), 'id');

        $tmcs =  [];
        $tmcs_array = TelemedCenter::whereIn('telemed_centers.id', $my_tmcs)
            ->join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
            ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
            ->get()
            ->groupBy('mcf_name');

        foreach ($tmcs_array as $mcf_name => $collection) {
            foreach ($collection as $key => $item) {
                $tmcs[$mcf_name][$item['id']] = $item['name'];
            };
        }

        if ($reportType != null && in_array($reportType, $rowNames)) {
            $reportHeaders = \Config::get("reports.rows.{$reportType}");
            $requiredSelectedRows = \Config::get("reports.options.defaults.{$reportType}");

            foreach ($reportHeaders as $key => $val) {
                $reportRows[$val] = [
                    'value' => $key,
                    'disabled' => in_array($key, $requiredSelectedRows) ? 'disabled' : false
                ];
            }
        }


        //return $reportRows;
        $roles = [];
        foreach (Role::all()->pluck('name', 'slug') as $k => $v) {
            $roles[$v] = ['value' => $k, 'disabled' => $k == 'admin' ? 'disabled' : false];
        }

        // return $roles;
        return view('reports.edit', [
            'name' => $report->name,
            'reportType' => $reportType,
            'reportRows' => $reportRows,
            'defaultSelectedRows' => $defaultSelectedRows,
            'tmcs' => $tmcs,
            'defaultTmcs' => $defaultTmcs,
            'roles' => $roles,
            'defaultRoles' => $defaultRoles,
        ])->with('report', $report);
    }


    /**
     * Show the form for creating a new Report.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        // Виды графиков или отчетов
        $reportTypes = Report::$reportTypes;
        return view('reports.create', compact('reportTypes'));


    }

    /**
     * Store a newly created Report in storage.
     *
     * @param CreateReportRequest $request
     *
     * @return Response
     */
    public function store(CreateReportRequest $request)
    {
        $input = $request->all();

        if ($request->has('step')) {
            $step = $request['step'];

            $rowNames = array_keys(\Config::get("reports.rows"));
            $reportType = $request->get('report_type');

            $reportRows = [];
            $reportHeaders = [];
            $defaultSelectedRows = [];

            if ($reportType != null && in_array($reportType, $rowNames)) {
                $reportHeaders = \Config::get("reports.rows.{$reportType}");
                $defaultSelectedRows = \Config::get("reports.options.defaults.{$reportType}");

                foreach ($reportHeaders as $key => $val) {
                    $reportRows[$val] = [
                        'value' => $key,
                        'disabled' => in_array($key, $defaultSelectedRows) ? 'disabled' : false
                    ];
                }
            }


            switch ($step) {
                /** Этап №1 */
                case 1:
                    return redirect()->route('reports.index');
                    break;
                /** Этап №2 */
                case 2:
                    $reportType = $request->get('report_type');

                    //return response()->json($reportHeaders);
                    return view('reports.create', compact('step', 'reportRows', 'defaultSelectedRows', 'reportType'));
                    break;
                /** Этап №3 */
                case 3:
                    $reportType = $request->get('report_type');

                    $rows = $request->get('rows');


                    // Получить список ТМЦ польховтеля
                    $my_tmcs_array = \TmcRoleManager::getTmcs();

                    $my_tmcs =  array_pluck($my_tmcs_array, 'id');
                    $myTmcsSelectOptions = array_pluck($my_tmcs_array, 'name','id');
                    $defaultRoles = ['admin'];
                    $tmcs = [];
                    $tmcs_array = TelemedCenter::whereIn('telemed_centers.id',$my_tmcs)->join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
                        ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
                        ->get()
                        ->groupBy('mcf_name');

                    foreach ($tmcs_array as $mcf_name => $collection) {
                        foreach ($collection as $key => $item) {
                            $tmcs[$mcf_name][$item['id']] = $item['name'];
                        };
                    }

                    return view('reports.create', compact('tmcs', 'rows', 'step', 'reportRows', 'defaultSelectedRows', 'reportType', 'myTmcsSelectOptions'));

                    //return response()->json(['rows' => $rows, $request->all()]);

                    break;
                /** Финальный Этап */
                case 'final':
                    //return $request->all();

                    if( empty($request->get('tmc_id'))) {
                        Flash::error('Ошибка сохранения. Не указан ТМЦ к которому необходимо привязать отчет!');

                        return redirect(route('reports.index'));
                    }
                    $user = \Sentinel::getUser();

                    $selected_rows = $request->get('selected_rows');
                    $selected_roles = $request->get('roles', []);
                    $tmcs = $request['tmcs'];
                    $report_type = $request['report_type'];
                    // work with rows
                    if (count($selected_rows))
                        $rows = array_merge($defaultSelectedRows, $selected_rows);
                    else
                        $rows = $defaultSelectedRows;


                    $reportRoles = ['admin','user','mcf_admin', 'coordinator', 'tmc_admin'];

                    $report = new Report();
                    $report->headers = json_encode($rows);
                    $report->tmcs = json_encode($tmcs);
                    $report->roles = json_encode($reportRoles);
                    $report->type = $report_type;
                    $report->user_id = $user->id;
                    $report->tmc_id = $request->get('tmc_id');
                    $report->name = $request['name'];

                    //return $report;

                    if ($report->save()) {
                        Flash::success(trans('backend.success_saved'));

                        return redirect(route('reports.init.table', $report->id));
                    } else {
                        Flash::error('Ошибка сохранения!');

                        return redirect(route('reports.index'));
                    }


                default:

                    Flash::error('Ошибка!');

                    return redirect(route('reports.index'));
            }
        }
    }

    /**
     * Display the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }

        // Получить название ТМЦ
        if($report->tmc_id != null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);
            
            if($tmc== null) {
                $tmc_name = 'удаленный ТМЦ';
            } else {
                $tmc_name = $tmc->name;
            }
            

        } else {
            $tmc_name = 'Не прикреплен к ТМЦ';
        }

        return view('reports.show', compact('tmc_name'))->with('report', $report);
    }


    /**
     * Update the specified Report in storage.
     *
     * @param  int $id
     * @param UpdateReportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportRequest $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        // Если прикреплен к ТМЦ запретить редактировать
        if($report->tmc_id == null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);

            Flash::error('Общие отчеты может править только Админ портала');

            return redirect(route('reports.index'));
        }
        // Если прикреплен к ТМЦ то проверяем доступ
        if($report->tmc_id != null) {
            if(!\Sentinel::inRole('mcf_admin')) {
                Flash::error('Отчеты ЛПУ могут править только админы ЛПУ');
                return redirect(route('reports.index'));
            } else {

                $tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');


                if(!in_array($report->tmc_id, $tmcs)) {

                    $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);

                    if($tmc == null) {
                        Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));
                    }

                    Flash::error("ТМЦ {$tmc->name} не относится к вашим ЛПУ" );

                    return redirect(route('reports.index'));
                }

            }

        }


        $data = [
            'name' => $request->get('name', 'Вы забыли указать имя при сохранении'),
            'tmcs' => json_encode($request->get('tmcs', [])),
            'headers' => json_encode($request->get('rows', [])),
            'roles' => json_encode($request->get('roles', ['admin'])),
        ];

        $report = $this->reportRepository->update($data, $id);

        Flash::success(trans('backend.success_updated'));

        return redirect(route('reports.index'));
    }


    /**
     * Remove the specified Report from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('reports.index'));
        }

        $tmcs =  array_pluck(\TmcRoleManager::getTmcs(), 'id');

        if(in_array($report->tmc_id, $tmcs)) {
            $this->reportRepository->delete($id);

            Flash::success(trans('backend.success_deleted'));

            return redirect(route('reports.index'));

        } else {
            Flash::error('Отчеты не прикрепленные к Вашим ТМЦ может редактировать или удалять только Администратор портала или администратор ЛПУ который владеет правами над этим ТМЦ');
            return redirect(route('reports.index'));
        }



    }


}