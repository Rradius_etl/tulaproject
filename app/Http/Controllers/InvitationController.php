<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedCriteria;
use App\Criteria\TelemedExstCriteria;
use App\Criteria\TelemedOtherCriteria;
use App\Http\Requests;
use App\Http\Requests\CreateInvitationRequest;
use App\Http\Requests\UpdateInvitationRequest;
use App\Repositories\InvitationRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
/** Список приглашения и их принятие или отказ */
class InvitationController extends InfyOmBaseController
{
    /** @var  InvitationRepository */
    private $invitationRepository;
    /** Инициализация данных */
    public function __construct(InvitationRepository $invitationRepo)
    {
        $this->middleware(['auth','is_user_of_telemed']);
        $this->invitationRepository = $invitationRepo;
        $telemedId = \Route::current()->getParameter('TelemedId');
        $crit = new TelemedExstCriteria();
        $crit->setTelemedId(\TmcRoleManager::getTmcs());
        $this->invitationRepository->pushCriteria($crit);
    }

    /**
     * Display a listing of the Invitation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $telemedid = "";
        $this->invitationRepository->pushCriteria(new RequestCriteria($request));
        if($request->has('telemed_id'))
        {
            if($request->get('telemed_id') != "")
            {
                $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
                if(in_array($request->get('telemed_id'),$tmcs))
                {
                    $this->invitationRepository->pushCriteria(new RequestCriteria($request));
                    $crit = new TelemedCriteria();
                    $crit->setTelemedId($request->get('telemed_id'));
                    $this->invitationRepository->pushCriteria($crit);
                    $invitations = $this->invitationRepository->paginate();
                }
                else
                {
                    abort(401,"вы не имеете прав к просмотру этого раздела");
                }
            }
            else
            {
                $invitations = $this->invitationRepository->paginate();
            }
            $telemedid = $request->get('telemed_id');
        }
        else
        {
            $invitations = $this->invitationRepository->paginate();
        }

        return view('invitations.index',['telemedid'=>$telemedid])
            ->with('invitations', $invitations);
    }

    /**
     * Show the form for creating a new Invitation.
     *
     * @return Response
     */
    public function show($id)
    {
        $invitation = $this->invitationRepository->findWithoutFail($id);

        if (empty($invitation)) {
            Flash::error(trans('backend.Invitation') . trans('backend.not_found') . 'о' );

            return redirect(route('invitations.index'));
        }
        if(isset($_GET['complete']))
        {
            if($invitation->decision == "pending")
            {
                Flash::success("Для начала нужно отклонить или принять заявку на участие в событии!!");
                return redirect(route('invitations.index'));
            }
            else
            {
                $invitation->completed = true;
                $invitation->save();
                Flash::success("Ваше решение было успешно подтверждено!");
                return redirect(route('invitations.index'));
            }
        }
        if(isset($_GET['confirm']))
        {
            $invitation->decision = "agreed";
            $invitation->save();
            Flash::success("Ваше решение было успешно изменено!");
            return redirect(route('invitations.index'));
        }
        else if(isset($_GET['reject']))
        {
            Flash::success("Ваше решение было успешно изменено!");
            $invitation->decision = "rejected";
            $invitation->save();
            return redirect(route('invitations.index'));
        }

        return view('invitations.show')->with('invitation', $invitation);
    }



}
