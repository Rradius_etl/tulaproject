<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateProfileRequest;
use App\Http\Requests\Admin\UpdateProfileRequest;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\Profile;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\Admin\TelemedRequest;
use App\Models\Admin\UserProfile;
use App\Models\FileUpload;
use App\Models\Poll;
use App\Models\TelemedCenter;
use App\Repositories\Admin\ProfileRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Barryvdh\Debugbar\Controllers\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use TmcRoleManager;
use NotificationManager;
use DB;

class ProfileController extends Controller
{
    /** @var  ProfileRepository */


    public function __construct()
    {
        $this->user = Sentinel::getUser();

    }

    /** Страцина профиля */
    public function index()
    {
        return view('profile.index')
            ->with('user', $this->user);
    }

    /** Страница редактирования профиля */
    public function edit()
    {
        return view('profile.edit')
            ->with('user', $this->user);
    }

    /** Удалить свой аватар */
    public function deleteavatar()
    {
        $old_file = $this->user->avatar_file_id ;
        $this->user->avatar_file_id = null;
        $this->user->save();
        if($old_file != null)
        {
            FileUpload::deletePublicFile($old_file);
        }
        Flash::success("Ваш аватар был успешно удален!");
        return redirect()->route("profile");
    }

    /** Сохранить изменения профиля */
    public function update(Request $request)
    {
        $old_file = null;
        $this->validate($request,[
            'avatar'=>'mimes:jpeg,jpg,bmp,png|max:6144',
        ]);
        $avatar = $request->file('avatar');
        $input = $request->all();
        if(isset($input['avatar_file_id']))
        {
            unset($input['avatar_file_id']);
        }
        if($avatar != null)
        {
           $avatar_id = FileUpload::saveAvatarFile($avatar);
            if($avatar_id != false)
            {
                $old_file = $this->user->avatar_file_id;
                $input['avatar_file_id'] = $avatar_id;
            }
        }
        $this->user->update($input);
        if($old_file != null)
        {
            FileUpload::deletePublicFile($old_file);
        }
       Flash::success("Профиль был успешно обновлен.");

        return redirect(route('profile'));
    }

    /**
     * Список доступных мне опросов
    */
    public function myPolls(Request $request)
    {


        $tmclist = \App\Models\Admin\TelemedCenter::get()->groupBy('med_care_fac_id');
        $array = [];
        foreach($tmclist as $k=>$tm)
        {
            $mcf = MedCareFac::find($k);
            foreach ($tm as $item) {
                $array[$mcf->name][$item->id] = $item->name;
            }
        }
        $tmclist = $array;

        $daterange = [];

        $organizator = "";
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }

        if($request->has('organizator'))
        {
            $organizator = $request->get('organizator');
        }
        $tmcs = \TmcRoleManager::getTmcs();
        $polls = Poll::active()->whereIn('telemed_center_id', array_pluck($tmcs, ['id']));
/*        if($request->has('daterange'))
        {
            $d = $request->get('daterange');
            $daterange[] = new Carbon(substr($d,0,10));
            $daterange[] =  new Carbon(substr($d,12,20));
            $polls = $polls->whereBetween("finished_at",$daterange);
        }*/
        if($search != "")
        {
            $polls = $polls
                ->where("title",'like',"%".$search."%");
        }
        if($organizator != "")
        {
            $polls->where("telemed_center_id",'=',$organizator);
        }

        if($request->has('orderBy')&&$request->has('sortedBy'))
        {
            $polls->orderBy($request->get('orderBy'),$request->get('sortedBy'));
        }

        if($request->has('from')&&$request->has('to')) {
            $from = Carbon::createFromFormat('Y-m-d', $request->get('from'));
            $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));

            $polls->whereBetween('finished_at', [$from,$to]);
        }

        $polls = $polls->whereIn('telemed_center_id',array_pluck($tmcs,'id'))->paginate(30);
        return view('polls.my_polls', compact('polls', 'all_tmcs','search','tmclist','organizator'));
    }

    /** Все активные опросы*/
    public function all(Request $request)
    {
        $organizator = "";
        $search = "";
        $tmclist = \App\Models\Admin\TelemedCenter::get()->groupBy('med_care_fac_id');
        $array = [];
        foreach($tmclist as $k=>$tm)
        {
            $mcf = MedCareFac::find($k);
            foreach ($tm as $item) {
                $array[$mcf->name][$item->id] = $item->name;
            }
        }
        $tmclist = $array;
        if($request->has('search'))
        {
            $search = $request->get('search');
        }

        if($request->has('organizator'))
        {
            $organizator = $request->get('organizator');
        }

        $polls = Poll::whereRaw("1=1");
        if($search != "")
        {
            $polls->where("title",'like',"%".$search."%");
        }

        if($organizator != "")
        {
            $polls->where("telemed_center_id",'=',$organizator);
        }

        if($request->has('orderBy')&&$request->has('sortedBy'))
        {
            $polls->orderBy($request->get('orderBy'),$request->get('sortedBy'));
        }
        if($request->has('from')&&$request->has('to')) {
            $from = Carbon::createFromFormat('Y-m-d', $request->get('from'));
            $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));
            $polls->whereBetween('finished_at', [$from,$to]);
        }
/*        if($request->has('daterange'))
        {
            $d = $request->get('daterange');
            $daterange[] = new Carbon(substr($d,0,10));
            $daterange[] =  new Carbon(substr($d,12,20));
            $polls = $polls->whereBetween("finished_at",$daterange);
        }*/
        $polls = $polls->active()->paginate(30);
        return view('polls.all_polls', ['search'=>$search,'tmclist'=>$tmclist,'organizator'=>$organizator])
            ->with('polls', $polls);
    }
    /**
     * Архив опросов
     **/
    public function pollsArchive(Request $request)
    {
        $organizator = "";
        $search = "";
        $tmclist = \App\Models\Admin\TelemedCenter::get()->groupBy('med_care_fac_id');
        $array = [];
        foreach($tmclist as $k=>$tm)
        {
            $mcf = MedCareFac::find($k);
            foreach ($tm as $item) {
                $array[$mcf->name][$item->id] = $item->name;
            }
        }
        $tmclist = $array;
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($request->has('organizator'))
        {
            $organizator = $request->get('organizator');
        }
        $polls = Poll::inArchive();
        $polls->whereIn('telemed_center_id',array_pluck(TmcRoleManager::getTmcs(),'id'));
        if($search != "")
        {
            $polls->where("title",'like',"%".$search."%");
        }
        if($organizator != "")
        {
            $polls->where("telemed_center_id",'=',$organizator);
        }
        if($request->has('orderBy')&&$request->has('sortedBy'))
        {
            $polls->orderBy($request->get('orderBy'),$request->get('sortedBy'));
        }

        if($request->has('from')&&$request->has('to')) {
            $from = Carbon::createFromFormat('Y-m-d', $request->get('from'));
            $to = Carbon::createFromFormat('Y-m-d', $request->get('to'));

            $polls->whereBetween('finished_at', [$from,$to]);
        }
/*        if($request->has('daterange'))
        {
            $d = $request->get('daterange');
            $daterange[] = new Carbon(substr($d,0,10));
            $daterange[] =  new Carbon(substr($d,12,20));
            $polls = $polls->whereBetween("finished_at",$daterange);
        }*/
        $polls = $polls->paginate(30);


        return view('polls.archive', compact('polls', 'all_tmcs','search','organizator','tmclist'));
    }

    /**
     * Список Уведомлении по опросам
     */
    public function pollNotifications(Request $request)
    {       $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($search != "")
        {
            $notifications = $this->user->getPollNotificationsWithSearch($search)->orderBy('created_at','desc')->paginate(30);
        }
        else
        {
            $notifications = $this->user->getPollNotifications()->orderBy('created_at','desc')->paginate(30);
        }
        //return $notifications;

        return view('polls.notifications', compact('notifications','search'));
    }
    /**
     * Список еще непрочитанных уведомлении по опросам
     */
    public function unreadPollNotifications(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($search != "")
        {
            $notifications = $this->user->getUnreadPollNotificationsWithSearch($search)->orderBy('created_at','desc')->paginate(30);
        }
        else
        {
            $notifications = $this->user->getUnreadPollNotifications()->orderBy('created_at','desc')->paginate(30);
        }
        return view('polls.unread_notifications', compact('notifications','search'));
    }

    /** Оповестить об опросе определенных ТМЦ */
    public function notifyPoll(Request $request)
    {


        //return $request->all();
        if( $request->has('poll_id') && $request->has('recipients') && count($request->get('recipients')) > 0 ) {
            $poll = Poll::find($request->get('poll_id'));
            if( $poll != null ) {

                $user= Sentinel::getUser();
                $fullName = $user->getfName();
                $tmcs = array_where($request->get('recipients'), function ($value, $key) {
                    return is_int($value);
                });
                if($request->has('send_email'))
                {
                    NotificationManager::createPollNotificationForManyTmc($tmcs, $poll->id,true,$poll->telemed_center_id);
                }
                else
                {
                    NotificationManager::createPollNotificationForManyTmc($tmcs, $poll->id,false,$poll->telemed_center_id);
                }


                return redirect()->back()->with('success', 'Все пользователи выбранных вами ТМЦ были уведомлены!');


            }

        } else {
            return redirect()->back()->with('error', 'Ошибка! Не получилось уведомить');
        }
    }

}
