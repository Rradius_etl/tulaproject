<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedOtherCriteria;
use App\Criteria\UserTelemedCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedCenterRequest;
use App\Http\Requests\Admin\UpdateTelemedCenterRequest;
use App\Models\Admin\TelemedUserPivot;
use App\Models\FileUpload;
use App\Repositories\TelemedCenterRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use App\Criteria\UserCriteria;
/** Список всех ТМЦ для неадминских ролей */
class GeneralTelemedCenterController extends InfyOmBaseController
{
    /** @var  TelemedCenterRepository */
    private $telemedCenterRepository;

    public function __construct(TelemedCenterRepository $telemedCenterRepo)
    {
        $this->middleware(["auth","is_user_of_telemed"]);
        $this->telemedCenterRepository = $telemedCenterRepo;
        $this->user = Sentinel::getUser();
    }

    /**
     * Display a listing of the TelemedCenter.
     *
     * @param Request $request
     * @return Response
     */


    public function sendMessage($id)
    {
        $users = TelemedUserPivot::where(["telemed_center_id"=>$id])->get();
    }


    public function sendMessagePost(\Request $request , $id)
    {
        $users = TelemedUserPivot::where(["telemed_center_id"=>$id])->get();
    }


    public function sendMessageTmc($id)
    {
        return view("general-telemed-centers.message");
    }


    public function sendMessageTmcPost(Request $request , $id)
    {
        $input = \Input::all();
        $this->validate($request,[
            'subject'=>"required",
             'message'=>'required'
        ]);
        $users = TelemedUserPivot::where(["telemed_center_id"=>$id])->select('id')->get();
        if(count($users) == 0)
        {
            return redirect()->route('general-telemed-centers.index')->withErrors("В этом телемедицинском центре нету пользователей");
        }
        $thread = Thread::create(
            [
                'subject' => $input['subject'],
            ]
        );
        // Message
        $message = Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
                'last_read' => new Carbon(),
            ]
        );
        // Recipients
            $thread->addParticipant(array_pluck($users,"id"));

        $files = $request->file('files');
        if($files[0]!=null)
        {
            FileUpload::saveMessageFiles($message->id,$files,$this->user->id,"files/");
        }


        return redirect()->route("messages")->withSuccess("Сообщение успешно отправлено пользователям этого ТМЦ!");

    }


    public function index(Request $request)
    {
        $search = "";
        if($request->has("search"))
        {
            $search = $request->get('search');
        }
        $this->telemedCenterRepository->pushCriteria(new RequestCriteria($request));
        $telemedCenters = $this->telemedCenterRepository->paginate(30);

        return view('general-telemed-centers.index',['search'=>$search])
            ->with('telemedCenters', $telemedCenters);
    }



    public function show($id)
    {
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        if (empty($telemedCenter)) {
            Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found') );

            return redirect(route('general-telemed-centers.index'));
        }

        return view('general-telemed-centers.show')->with('telemedCenter', $telemedCenter);
    }
}
