<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDoctorRequest;
use App\Http\Requests\Admin\UpdateDoctorRequest;
use App\Models\Admin\TelemedCenter;
use App\Repositories\DoctorRepository;
use Route;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
/** Все что связано с редактированием и удалением и получения списка врачей определенного ТМЦ */
class DoctorController extends InfyOmBaseController
{
    /** @var  DoctorRepository */
    private $doctorRepository;

    public function __construct(DoctorRepository $doctorRepo)
    {
        $this->middleware(["auth","has_access_to_edit_tmc"]);
        $this->doctorRepository = $doctorRepo;
        $currentRoute = Route::current();
        if( $currentRoute) {
            $id = $currentRoute->getParameter('TelemedId');
            $this->TelemedId= $id;
            $crit = new TelemedCriteria();
            $crit->setTelemedId($id);
            $this->doctorRepository->pushCriteria($crit);
        } else {
            die();
        }
    }

    /**
     * Display a listing of the Doctor.
     *
     * @param Request $request
     * @return Response
     */
    public function index($TelemedId,Request $request)
    {
        $this->doctorRepository->pushCriteria(new RequestCriteria($request));
        $doctors = $this->doctorRepository->all();

        return view('doctors.index',[
            "TelemedId"=>$this->TelemedId,
            'Telemed'   => TelemedCenter::find($this->TelemedId)
        ])
            ->with('doctors', $doctors);
    }

    /**
     * Show the form for creating a new Doctor.
     *
     * @return Response
     */
    public function create($TelemedId)
    {
        return view('doctors.create',['TelemedId'=>$TelemedId]);
    }

    /**
     * Store a newly created Doctor in storage.
     *
     * @param CreateDoctorRequest $request
     *
     * @return Response
     */
    public function store($TelemedId,CreateDoctorRequest $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'surname'=>'required',
                'middlename'=>'required',
                'spec'=>'required'
            ]);
        $input = $request->all();
        $input['telemed_center_id']=$TelemedId;
        $doctor = $this->doctorRepository->create($input);

        Flash::success(trans('backend.Doctor') . trans('backend.success_saved'));

        return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
    }

    /**
     * Display the specified Doctor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($TelemedId,$id)
    {
        $doctor = $this->doctorRepository->findWithoutFail($id);

        if (empty($doctor)) {
            Flash::error(trans('backend.Doctor') . trans('backend.not_found') );

            return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
        }

        return view('doctors.show',["TelemedId"=>$this->TelemedId])->with('doctor', $doctor);
    }

    /**
     * Show the form for editing the specified Doctor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($TelemedId,$id)
    {
        $doctor = $this->doctorRepository->findWithoutFail($id);

        if (empty($doctor)) {
             Flash::error(trans('backend.Doctor') . trans('backend.not_found') );

            return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
        }

        return view('doctors.edit',["TelemedId"=>$this->TelemedId])->with('doctor', $doctor);
    }

    /**
     * Update the specified Doctor in storage.
     *
     * @param  int              $id
     * @param UpdateDoctorRequest $request
     *
     * @return Response
     */
    public function update($TelemedId,$id, UpdateDoctorRequest $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'surname'=>'required',
                'middlename'=>'required',
                'spec'=>'required'
            ]);
        $doctor = $this->doctorRepository->findWithoutFail($id);

        if (empty($doctor)) {
             Flash::error(trans('backend.Doctor') . trans('backend.not_found') );

            return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
        }

        $doctor = $this->doctorRepository->update($request->all(), $id);

       Flash::success(trans('backend.Doctor') . trans('backend.success_updated'));

        return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
    }

    /**
     * Remove the specified Doctor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($TelemedId,$id)
    {
        $doctor = $this->doctorRepository->findWithoutFail($id);

        if (empty($doctor)) {
            Flash::error(trans('backend.Doctor') . trans('backend.not_found') );

            return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
        }

        $this->doctorRepository->delete($id);

        Flash::success(trans('backend.Doctor') . trans('backend.success_deleted'));

        return redirect(route('doctors.index',["TelemedId"=>$this->TelemedId]));
    }
}
