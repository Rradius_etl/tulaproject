<?php

namespace App\Http\Controllers;

use App\Models\Admin\Page;
use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    /** Открыть страницу(Посмотри эту сущность в админке, если не понятно) */
    public function view($slug)
    {
        $page = Page::where('slug', $slug)->first();

        if( empty($page)) {
            return abort(404);
        }
        
        return view('page.view', compact('page'));
    }
}
