<?php

namespace App\Http\Controllers;

use App\Criteria\DateRangeCriteria;
use App\Criteria\MedCareFacConsultRequestCriteria;
use App\Criteria\TMCMedCareFacConsultRequestCriteria;
use App\Criteria\UIDCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\CreateTelemedConsultRequestRequest;
use App\Http\Requests\UpdateTelemedConsultRequestRequest;
use App\Models\Admin\File;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedConsultRequest;
use App\Repositories\TelemedConsultRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use Session;
/** Список доступных заявок на консультации в мое ЛПУ(Для координаторов) */
class TMCTelemedConsultRequestController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    public function __construct(TelemedConsultRequestRepository $telemedConsultRequestRepo)
    {
        $this->middleware(["auth",'is_user_of_telemed']);
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
        $this->user = Sentinel::getUser();
        $crit = new TMCMedCareFacConsultRequestCriteria();

        $this->crit = $crit;

        $tmcs = \TmcRoleManager::getTmcs();
        $mcfs = [];
        if($tmcs!=null || count($tmcs) > 0)
        {
            $tmcs = array_pluck($tmcs, "id");
            $tmclist = \App\Models\TelemedCenter::whereIn("id",$tmcs)->get();
            foreach($tmclist as $tmc)
            {
                if(!in_array($tmc->med_care_fac_id,$mcfs))
                {
                    $mcfs[] = $tmc->med_care_fac_id;
                }
            }
        }
         $crit->setMcfs($mcfs);
        $this->telemedConsultRequestRepository->pushCriteria($crit);
    }

    /**
     * Display a listing of the TelemedConsultRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        $canceled = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $uidsearch = "";
        if($request->has('uidsearch'))
        {
            $uidsearch = $request->get('uidsearch');
        }
        if($request->has('canceled'))
        {
            $canceled = $request->get('canceled');
        }
        if($request->has('from')&&$request->has('to')) {
            $this->telemedConsultRequestRepository->pushCriteria(new DateRangeCriteria($request['from'], $request['to']));
        }


        $uidcrit = new UIDCriteria();
        $uidcrit->setParams("",$uidsearch);
        $this->telemedConsultRequestRepository->pushCriteria($uidcrit);
        $this->telemedConsultRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedConsultRequests = $this->telemedConsultRequestRepository->with('abonentSide')->paginate(30);

        return view('tmc_telemed_consult_requests.index',['search'=>$search,'uidsearch'=>$uidsearch,'canceled'=>$canceled])
            ->with('telemedConsultRequests', $telemedConsultRequests);
    }



    /**
     * Display the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->where('efp.type', 'abonent_side')
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();

        return view('tmc_telemed_consult_requests.show', compact('files'))->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Show the form for editing the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->consultantSide!=null)
        {
            Flash::error("Редактирование невозможно, консультант уже заполнил свою форму");

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $list = [];
        $tmcs = \TmcRoleManager::getTmcs();
        foreach($tmcs as $tmc)
        {
            $tmc1 = new TelemedCenter();
            $tmc1->name = $tmc['name'];
            $tmc1->id = $tmc['id'];
            $list[] = $tmc1;
        }

        return view('tmc_telemed_consult_requests.edit',["tmcs"=>collect($list)->pluck('name','id')])->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Update the specified TelemedConsultRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        


        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);



        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $rules = [
            'telemed_center_id' => 'required|in:' . implode(',', $tmcs)."|not_in:".$telemedConsultRequest->request->telemed_center_id
        ];

        $this->validate($request, $rules);
        if($telemedConsultRequest->telemed_center_id!=null)
        {
            Flash::error("ТМЦ ".$telemedConsultRequest->telemedCenter->name." уже принял заявку.");

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->consultantSide!=null)
        {
            Flash::error("Редактирование невозможно, консультант уже заполнил свою форму");

            return redirect(route('tmc-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $telemedConsultRequest->telemed_center_id = $request->get("telemed_center_id");
        $telemedConsultRequest->tmc_at = Carbon::now();
        $telemedConsultRequest->telemed_notificated = 0;
        $req = $telemedConsultRequest->request;
        $telemedConsultRequest->update();
        $tmc = TelemedCenter::find($request->get("telemed_center_id"));
        if($tmc != null)
        {
            \NotificationManager::createNotificationForTmcUsers("Ваша заявка на телемедицинскую консультацию - ".$telemedConsultRequest->getFullUid()." была перенаправлена на ТМЦ ".$tmc->name.", зайдите по <a href='". \URL::to("/abonent-telemed-consult-requests/".$req->id)."'>ссылке</a>",$req->initiator_id,false,true);

        }

       Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_updated'));

       return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

}
