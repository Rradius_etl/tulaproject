<?php

namespace App\Http\Controllers;

use App\Criteria\DateRangeCriteria;
use App\Criteria\MedCareFacConsultRequestCriteria;
use App\Criteria\TelemedConsultantSideCriteria;
use App\Criteria\UIDCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedConsultRequestRequest;
use App\Http\Requests\Admin\UpdateTelemedConsultRequestRequest;
use App\Models\Admin\EventFilePivot;
use App\Models\Admin\File;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedRequest;
use App\Models\Admin\TelemedUserPivot;
use App\Models\ApiModel;
use App\Models\Doctor;
use App\Models\FileUpload;
use App\Models\TelemedConsultAbonentSide;
use App\Models\TelemedConsultConsultantSide;
use App\Repositories\TelemedConsultRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use Carbon\Carbon;
class ConsultantTelemedConsultRequestController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    public function __construct(TelemedConsultRequestRepository $telemedConsultRequestRepo)
    {
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
        $this->middleware(['auth','is_user_of_telemed']);
        $this->user = Sentinel::getUser();
        $this->tmcs =  \TmcRoleManager::getTmcs();

        $crit = new TelemedConsultantSideCriteria();
        $crit->setTelemedId( $this->tmcs);
        $this->telemedConsultRequestRepository->pushCriteria($crit);


    }

    /**
     * Display a listing of the TelemedConsultRequest.
     *
     * @param Request $request
     * @return Response
     */

    public function index(Request $request)
    {
        $search = "";
        $uidsearch = "";
        $status = "";
        $type = "";
        $consdoctor = "";
        $abondoctor = "";
        $conssearch = "";
        $abonsearch = "";
        $consspec = "";
        $canceled = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($request->has('uidsearch'))
        {
            $uidsearch = $request->get('uidsearch');
        }
        if($request->has('type'))
        {
            $type = $request->get('type');
        }
        if($request->has('consdoctor'))
        {
            $consdoctor = $request->get('consdoctor');
        }
        if($request->has('abondoctor'))
        {
            $abondoctor = $request->get('abondoctor');
        }
        if($request->has('conssearch'))
        {
            $conssearch = $request->get('conssearch');
        }
        if($request->has('abonsearch'))
        {
            $abonsearch = $request->get('abonsearch');
        }
        if($request->has('consspec'))
        {
            $consspec = $request->get('consspec');
        }
        if($request->has('status'))
        {
            $status = $request->get('status');
        }
        if($request->has('canceled'))
        {
            $canceled = $request->get('canceled');
        }

        if($request->has('from')&&$request->has('to')) {
            $this->telemedConsultRequestRepository->pushCriteria(new DateRangeCriteria($request['from'], $request['to']));
        }
 
        
        $uidcrit = new UIDCriteria();
        $uidcrit->setParams($conssearch,$search);
        $this->telemedConsultRequestRepository->pushCriteria($uidcrit);

        $this->telemedConsultRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedConsultRequests = $this->telemedConsultRequestRepository->paginate(15);
        $mcfs = MedCareFac::pluck('name','id');
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        $array = [];
        $doctors = Doctor::whereIn('telemed_center_id',$tmcs)->get()->groupBy('telemed_center_id');
        foreach($doctors as $k=>$d)
        {
            $tmc = TelemedCenter::find($k);
            $array[$tmc->name] = [];
            foreach($d as $doctor)
            {
                $obj = [];
                $array[$tmc->name][$doctor->id]= $doctor->surname." ".$doctor->name." ".$doctor->middlename;
            }
        }
        $doctors = $array;
        return view('consultant_telemed_consult_requests.index',compact('canceled','search','uidsearch','mcfs','type','conssearch','abonsearch','consdoctor','abondoctor','status','consspec','doctors'))
            ->with('telemedConsultRequests', $telemedConsultRequests);
    }
    /**
     * Show the form for creating a new TelemedConsultRequest.
     *
     * @return Response
     */
    public function create()
    {
        $array = [];
        $doctors = Doctor::where(['telemed_center_id'=>$this->user->tmc[0]->id])->all();
        foreach($doctors as $doctor)
        {
            $array[$doctor->id] = $doctor->name." ".$doctor->surname." ".$doctor->middlename." ".$doctor->position;
        }
        return view('consultant_telemed_consult_requests.create',["doctors"=>$array]);
    }

    /**
     * Store a newly created TelemedConsultRequest in storage.
     *
     * @param CreateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedConsultRequestRequest $request)
    {
        $input = $request->all();

        $telemedConsultRequest = $this->telemedConsultRequestRepository->create($input);

        Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_saved'));

        return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    /**
     * Display the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->where('efp.type', 'abonent_side')
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();

        return view('consultant_telemed_consult_requests.show', compact('files'))->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Show the form for editing the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);
        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $req = $telemedConsultRequest->request;
        if($req->telemedConclusion != null)
        {
            Flash::error("Вы не можете редактировать или удалять заявки с заключениями");
            return redirect(route('consultant-telemed-consult-requests.index'));
        }
        if($req->canceled != 0)
        {
            Flash::error("Вы не можете редактировать или удалять отмененные заявки");
            return redirect(route('consultant-telemed-consult-requests.index'));
        }
        $consultside = $telemedConsultRequest->consultantSide;
        if($consultside==null)
        {
            $consultside = new TelemedConsultConsultantSide();
            $consultside->created_at = date("Y-m-d H:i:s",time());
        }
        $tmc = $telemedConsultRequest->telemedCenter;
        $array = [];
            $doctors = Doctor::where(['telemed_center_id'=>$telemedConsultRequest->telemed_center_id])->get();
        foreach($doctors as $doctor)
        {
            $array[$doctor->id] = $doctor->name." ".$doctor->surname." ".$doctor->middlename." ".$doctor->spec;
        }
        
        if(Sentinel::inRole("admin") || Sentinel::inRole('mcf_admin') || Sentinel::inRole('tmc_admin' )|| Sentinel::inRole('coordinator'))
        {
            $tmcusers = TelemedUserPivot::where('telemed_center_id',$telemedConsultRequest->telemed_center_id)->with('user')->get();
            $usersarray = [];
            $telemed = $telemedConsultRequest->telemedCenter;
            $mcfadmin = $telemed->medCareFac->user;
            if($mcfadmin != null)
            {
                $usersarray[(int)$mcfadmin->id] =  $mcfadmin->getfName()." - ".$mcfadmin->position;
            }
            foreach($tmcusers as $user)
            {
                $usersarray[(int)$user->id] = $user->user->getfName()." - ".$user->position;
            }

            return view('consultant_telemed_consult_requests.edit',
                ['tmcusers'=>$usersarray,'doctors'=>$array,'tmc'=>$tmc,'consultside'=>$consultside])
                ->with('telemedConsultRequest', $telemedConsultRequest);
        }
        return view('consultant_telemed_consult_requests.edit',['doctors'=>$array,'tmc'=>$tmc,'consultside'=>$consultside])
            ->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Update the specified TelemedConsultRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        //return response()->json($request->all());
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $req = $telemedConsultRequest->request;
        if($req->telemedConclusion != null)
        {
            Flash::error("Вы не можете редактировать или удалять заявки с заключениями");
            return redirect(route('consultant-telemed-consult-requests.index'));
        }
        if($req->canceled != 0)
        {
            Flash::error("Вы не можете редактировать или удалять отмененные заявки");
            return redirect(route('consultant-telemed-consult-requests.index'));
        }
        $rules = [
            'planned_date' => 'required_with:confirm|Date|after:'. Carbon::now(),
            'doctor_id' => 'required_with:confirm',
            'appointed_person_id'=>'required'
        ];
        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $validator->after(function ($validator) use ($telemedConsultRequest,$request) {
            $error = ApiModel::checkTimeConsultWithError($telemedConsultRequest->request->telemed_center_id,$telemedConsultRequest->telemed_center_id,$request->get('planned_date'),"Y-m-d H:i",$telemedConsultRequest->request_id);
            if($error!=1)
            {
                $validator->errors()->add("planned_time",$error);
            }
            if($request->has('doctor_id'))
            {
                $count = Doctor::where(['telemed_center_id'=>$telemedConsultRequest->telemed_center_id,"id"=>$request->get('doctor_id')])->count();
                if($count == 0)
                {
                    $validator->errors()->add("doctor_id","Вы пытаетесь привязать доктора непривязанному к вашему ТМЦ!");
                }
            }
            if($request->has('appointed_person_id'))
            {
                /** Удалить appointed_person_id если String значение */
                if( is_int($request->get('appointed_person_id')) )  {
                    $tmcusers = TelemedUserPivot::where('telemed_center_id',$telemedConsultRequest->telemed_center_id)->with('user')->get();
                    $usersarray = [];
                    $telemed = $telemedConsultRequest->telemedCenter;
                    $mcfadmin = $telemed->medCareFac->user;
                    if($mcfadmin != null)
                    {
                        array_push($usersarray,$mcfadmin->id);
                    }
                    foreach($tmcusers as $user)
                    {
                        array_push($usersarray,$user->id);
                    }

                    if(!in_array($request->get('appointed_person_id'),$usersarray))
                    {
                        $validator->errors()->add("appointed_person_id","Вы пытаетесь привязать пользователя к полю(ФИО и должность ответственного лица, принявшего заявку ) непривязанному к вашему ТМЦ!");
                    }

                } else {
                    $request['appointed_person'] = $request['appointed_person_id'];
                    $request['appointed_person_id'] = null;
                }

            }
        });

        if($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $input = $request->all();
        if(isset($input['deleteditems']))
        {
            $filesToDelete = $input['deleteditems'];
            foreach($filesToDelete as $k=>$v)
            {
                FileUpload::removePivot($id,$k);
            }

        }
        $files = $request->file('files');
        if($files[0]!=null)
        {
            FileUpload::saveFiles($id,$files,$this->user->id,"files/");
        }
        $consultside = $telemedConsultRequest->consultantSide;
        if($consultside==null)
        {
            $consultside = new TelemedConsultConsultantSide($request->all());
            $consultside->request_id = $id;
            $consultside->planned_date = $request->get('planned_date');
            $consultside->save();
        }
        else
        {
            $consultside->request_id = $id;
            $consultside->planned_date = $request->get('planned_date');
            $consultside->update($request->all());
        }

        $req = TelemedRequest::find($telemedConsultRequest->request_id);

        if($telemedConsultRequest->completed == 1)
        {

        }
        if(isset($input['confirm']))
        {
            $telemedConsultRequest->decision = "agreed";
            $req->decision = "agreed";
            $dec = 'принял';
        }
        else if(isset($input['reject']))
        {
            $telemedConsultRequest->decision = "rejected";
            $req->decision = "rejected";
            $dec = "отклонил";
        }
        if(isset($input['comment']))
        {
            $telemedConsultRequest->comment = $input['comment'];
        }
        $telemedConsultRequest->update();
        $startdate = new Carbon($input['planned_date']);
        $enddate = new Carbon($input['planned_date']);
        $enddate->addHour();
        $req->start_date = $startdate;
        $req->end_date = $enddate;
        if($req->update())
        {
            $message = "Пользователь " . $telemedConsultRequest->telemedCenter->name." ".$dec." вашу  заявку на консультацию в ваше ЛПУ, зайдите по <a href='".\Illuminate\Support\Facades\Request::root()."/abonent-telemed-consult-requests/".$req->id."'>ссылке</a>";

            \NotificationManager::createNotificationFromTMCtoTMC($message,$telemedConsultRequest->request->telemed_center_id,true,$telemedConsultRequest->telemed_center_id,true);
        }

       Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_updated'));

        return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    /**
     * Remove the specified TelemedConsultRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $this->telemedConsultRequestRepository->delete($id);

        Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_deleted'));

        return redirect(route('consultant-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }
}
