<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedRequestRequest;
use App\Http\Requests\Admin\UpdateTelemedRequestRequest;
use App\Repositories\Admin\TelemedRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
/** Класс не используется вообще **/
class TelemedRequestController extends InfyOmBaseController
{
    /** @var  TelemedRequestRepository */
    private $telemedRequestRepository;

    public function __construct(TelemedRequestRepository $telemedRequestRepo)
    {
        $this->telemedRequestRepository = $telemedRequestRepo;
    }

    /**
     * Display a listing of the TelemedRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->telemedRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedRequests = $this->telemedRequestRepository->paginate(30);

        return view('admin.telemed_requests.index')
            ->with('telemedRequests', $telemedRequests);
    }

    /**
     * Show the form for creating a new TelemedRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.telemed_requests.create');
    }

    /**
     * Store a newly created TelemedRequest in storage.
     *
     * @param CreateTelemedRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedRequestRequest $request)
    {
        $input = $request->all();

        $telemedRequest = $this->telemedRequestRepository->create($input);

        Flash::success(trans('backend.TelemedRequest') . trans('backend.success_saved'));

        return redirect(route('admin.telemed-requests.index'));
    }

    /**
     * Display the specified TelemedRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedRequest = $this->telemedRequestRepository->findWithoutFail($id);

        if (empty($telemedRequest)) {
            Flash::error(trans('backend.TelemedRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-requests.index'));
        }

        return view('admin.telemed_requests.show')->with('telemedRequest', $telemedRequest);
    }

    /**
     * Show the form for editing the specified TelemedRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedRequest = $this->telemedRequestRepository->findWithoutFail($id);

        if (empty($telemedRequest)) {
             Flash::error(trans('backend.TelemedRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-requests.index'));
        }

        return view('admin.telemed_requests.edit')->with('telemedRequest', $telemedRequest);
    }

    /**
     * Update the specified TelemedRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedRequestRequest $request)
    {
        $telemedRequest = $this->telemedRequestRepository->findWithoutFail($id);

        if (empty($telemedRequest)) {
             Flash::error(trans('backend.TelemedRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-requests.index'));
        }

        $telemedRequest = $this->telemedRequestRepository->update($request->all(), $id);

       Flash::success(trans('backend.TelemedRequest') . trans('backend.success_updated'));

        return redirect(route('admin.telemed-requests.index'));
    }

    /**
     * Remove the specified TelemedRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedRequest = $this->telemedRequestRepository->findWithoutFail($id);

        if (empty($telemedRequest)) {
            Flash::error(trans('backend.TelemedRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-requests.index'));
        }

        $this->telemedRequestRepository->delete($id);

        Flash::success(trans('backend.TelemedRequest') . trans('backend.success_deleted'));

        return redirect(route('admin.telemed-requests.index'));
    }
}
