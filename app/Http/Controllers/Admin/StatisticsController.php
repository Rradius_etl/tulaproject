<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\IndicationForUse;
use App\Models\Admin\TelemedCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use CpChart\Factory\Factory as pCharts;

use PhpOffice\PhpWord\PhpWord;

class StatisticsController extends Controller
{
    protected static $format = 'Y-m-d';

    public static $reportHeader = [ //Отчет по клиническим консультациям в разрезе ТМЦ
        '№ по порядку',
        "Наименование учреждения здравоохранения",
        "Наименование ТМЦ",
        'Адрес ТМЦ',
        "E.164 ID (номер терминала)",
        "H.323 ID (название терминала)",
        "Количество клинических консультаций",
        "Норма клинических консультаций",
    ];

    public static $allTypeEventsHeader = [ //Отчет по всем видеоконференциям по отдельному ТМЦ
        '№ по порядку',
        'Наименование учреждения здравоохранения',
        'Наименование ТМЦ',
        'Адрес ТМЦ',
        'Дата подачи заявки',
        'Дата проведения ВКС',
        'Входящая/ исходящая ВКС',
        'Вид ВКС',
        'Тип ТМК',
        'Номер заявки',
        'Лечащий врач',
        'Наименование ТМЦ консультанта',
        'Врач консультант',
        'Показание к применению',
        'Тема обучающего семинара',
        'Тема совещания',
        'Количество участников',
        'Степень важности',
    ];
    public static $statisticsHeader = [  // Статистические формы для роли «администратор»
        '№ по порядку',
        "Наименование учреждения здравоохранения",
        "Наименование ТМЦ",
        "Адрес ТМЦ",
        "E.164 ID (номер терминала)",
        "H.323 ID (название терминала)",
        "Общее количество видеоконференций",
        "Из них совещаний",
        "Из них обучающих семинаров",
        "Из них клинических консультаций",
        "Норма клинических консультаций",
        "Количество вызовов",
        "Минут разговора",
        "Секунд разговора"
    ];
    public static $conferenceHeader = [  // Отчет по совещаниям в разрезе ТМЦ
        '№ по порядку',
        'Наименование учреждения здравоохранения',
        'Наименование ТМЦ',
        'Адрес ТМЦ',
        'E.164 ID (номер терминала)',
        'H.323 ID (название терминала)',
        'Количество совещаний',
        'В том числе со степенью важности высокая',
        'В том числе со степенью важности средняя',
        'В том числе со степенью важности низкая'
    ];

    public static $seminarHeader = [  // Отчет по обучающим семинарам в разрезе ТМЦ
        '№ по порядку',
        'Наименование учреждения здравоохранения',
        'Наименование ТМЦ',
        'Адрес ТМЦ',
        'E.164 ID (номер терминала)',
        'H.323 ID (название терминала)',
        'Количество обучающих семинаров',
        'В том числе со степенью важности высокая',
        'В том числе со степенью важности средняя',
        'В том числе со степенью важности низкая'
    ];


    public static $eventTypes = [
        "meeting" => "совещание",
        "seminar" => "семинар",
        "consultation" => "консультация",
    ];

    public static $importances = [
        'low' => 'низкая',
        'medium' => 'средняя',
        'high' => 'высокая'
    ];

    public static $consultTypes = [
        'planned' => 'Плановый',
        'express' => 'Срочный',
        'emergency' => 'Экстренный',
    ];

    public static $indForUse = [
        'diagnose_and_treatment' => 'Уточнение диагноза и лечения',
        'patient_consultation' => 'Консультация по ведению больного',
        'hospital_possibility' => 'Возможность госпитализации',
        'meddata_equipment_decode' => 'Расшифровка данных медицинского оборудования',
        'other' => 'Иное',
    ];

    /** calculate range*/
    protected function getDateRange($request)
    {
        $format = self::$format;
        $defaults = [
            'from' => Carbon::now()->format($format),
            'to' => Carbon::now()->subWeek()->format($format)
        ];
        $from = Carbon::createFromFormat($format, $request->get('from', $defaults['from']))->startOfDay();
        $to = Carbon::createFromFormat($format, $request->get('to', $defaults['to']))->endOfDay();
        return [$from, $to];
    }

    /** route: /admin/consult_reports
     * Отчет по клиническим консультациям в разрезе ТМЦ
     * */
    public function getReport(Request $request)
    {
        $fileFormat = $request->get('format', 'ajax');
        $range = self::getDateRange($request);

        $reports = \DB::table("telemed_centers as tmc")->join("telemed_consult_requests as tcr", "tmc.id", "=", "tcr.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.request_id')
            ->join("telemed_requests_mcf_consultant as tcc", "tcc.request_id", "=", "tcr.request_id")
            ->whereBetween('tcc.planned_date', self::getDateRange($request))
            ->join("med_care_facs as mcf", "mcf.id", "=", "tcr.med_care_fac_id")
            ->select('tmc.terminal_name', 'tmc.terminal_number', "mcf.id as mcf_id", "mcf.name as mcf_name", 'tmc.name as tmc_name', "tmc.address as tmc_address", \DB::raw("count(tcr.request_id) as count"), "mcf.consultations_norm as cons_norm")->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc')->get();
        /* return $reports;*/

        $view = "reports_and_statistics.admin_consults";

        $fileName = $fileName = 'Отчет за ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();
        $addData = [];
        if ($fileFormat == 'docx') {
            self::getReportAsDocx($reports, self::$reportHeader, $fileName, $addData);
        }  else {
            return self::DownloadAsFile($reports, $fileFormat, $view, self::$reportHeader, $fileName);
        }


    }

    /** Получить отчет в формате docx **/
    protected static  function getReportAsDocx($reports, $headers, $title, $additionalData) {
        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000', 'width' => 100 * 100,  'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();
        $mcfs = [];

        foreach ($headers as $header) {
            $table->addCell()->addText($header);
        }

        foreach ($reports as $idx => $report) {
            $table->addRow();
            // № по порядку
            $table->addCell()->addText($idx + 1);
            // Наименование учреждения здравоохранения
            $table->addCell(100 * 25)->addText($report->mcf_name);
            // Наименование ТМЦ
            if(!array_key_exists($report->mcf_id,$mcfs)) {
                $mcfs[$report->mcf_id]['count'] = 1;
            }
            $table->addCell(100 * 25)->addText($mcfs[$report->mcf_id]['count'].") ".$report->tmc_name);
            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

            // Адрес ТМЦ
            $table->addCell(100 * 20)->addText($report->tmc_address);
            // E.164 ID (номер терминала)
            $table->addCell()->addText($report->terminal_number, $fontStyle, ['align' => 'center']);
            // H.323 ID (название терминала)
            $table->addCell(100 * 20)->addText($report->terminal_name, $fontStyle, ['align' => 'center']);
            // Количество клинических консультаций
            $table->addCell()->addText($report->count, $fontStyle, ['align' => 'center']);
            // Норма клинических консультаций
            if($report->cons_norm !== 0)
                $table->addCell()->addText($report->cons_norm, $fontStyle, ['align' => 'center']);
            else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
        }

        self::setDocxHeaders($title);
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $file = $objWriter->save('php://output');
        return $file;
    }
    /** Отчет о проведенных видеоконференциях в разрезе ТМЦ
     * /admin/reports
     *  */
    public function getEventsReport(Request $request)
    {
        $fileFormat = $request->get('format', 'ajax');

        $range = self::getDateRange($request);

        $reports = \DB::table("telemed_centers as tmc")->join("telemed_event_requests as tcr", "tmc.id", "=", "tcr.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'tcr.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->whereBetween('tcr.start_date', $range)
            ->select('tmc.terminal_name', 'tmc.terminal_number', "mcf.id as mcf_id", "mcf.name as mcf_name", 'tmc.name as tmc_name', "tmc.address as tmc_address", \DB::raw("count(tcr.id) as count"), \DB::raw("count(case tcr.event_type when 'consultation' then 1 else null end) as consult_count"),
                \DB::raw("count(case tcr.event_type when 'meeting' then 1 else null end) as meeting_count"), \DB::raw("count(case tcr.event_type when 'seminar' then 1 else null end) as seminar_count"), "mcf.consultations_norm as cons_norm")->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');
        if($request->has("tmcs"))
        {
            $tmcs = explode(",", $request->get('tmcs'));
            $reports = $reports->whereIn("tmc.id",$tmcs);
        }
        $reports = $reports->get();

        $view = "reports_and_statistics.event-reports-template";

        $fileName = $fileName = 'Отчет за ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();
        if ($fileFormat == 'docx') {
            $addData = [];
            return self::getEventsReportAsDocx($reports, self::$statisticsHeader, $fileName, $addData);
        } else {
            return self::DownloadAsFile($reports, $fileFormat, $view, self::$statisticsHeader, $fileName);
        }
    }

    protected static function getEventsReportAsDocx($reports, $headers, $title, $additionalData)
    {
        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000'];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();
        $mcfs = [];

        foreach ($headers as $header) {
            $table->addCell()->addText($header);
        }

        foreach ($reports as $idx => $report) {
            $table->addRow();
            // № по порядку
            $table->addCell()->addText($idx + 1);
            // Наименование учреждения здравоохранения
            $table->addCell()->addText($report->mcf_name);
            // Наименование ТМЦ
            if(!array_key_exists($report->mcf_id,$mcfs)) {
                $mcfs[$report->mcf_id]['count'] = 1;
            }
            $table->addCell()->addText($mcfs[$report->mcf_id]['count'].") ".$report->tmc_name);
            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

            // Адрес ТМЦ
            $table->addCell()->addText($report->tmc_address);
            // E.164 ID (номер терминала)
            $table->addCell()->addText($report->terminal_number, $fontStyle, ['align' => 'center']);
            // H.323 ID (название терминала)
            $table->addCell()->addText($report->terminal_name, $fontStyle, ['align' => 'center']);
            // Общее количество видеоконференций
            $table->addCell()->addText($report->count, $fontStyle, ['align' => 'center']);
            // Из них совещаний
            $table->addCell()->addText($report->meeting_count, $fontStyle, ['align' => 'center']);
            // Из них обучающих семинаров
            $table->addCell()->addText($report->seminar_count, $fontStyle, ['align' => 'center']);
            // Из них клинических консультаций
            $table->addCell()->addText($report->consult_count, $fontStyle, ['align' => 'center']);
            // Норма клинических консультаций
            if($report->cons_norm !== 0)
                $table->addCell()->addText($report->cons_norm, $fontStyle, ['align' => 'center']);
            else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Количество вызовов
            $table->addCell()->addText(' ', $fontStyle, ['align' => 'center']);
            // Минут разговора
            $table->addCell()->addText(' ', $fontStyle, ['align' => 'center']);
            // Секунд разговора
            $table->addCell()->addText(' ', $fontStyle, ['align' => 'center']);


        }

        self::setDocxHeaders($title);
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $file = $objWriter->save('php://output');
        return $file;
    }

    /**
     * Отчет по совещаниям в разрезе ТМЦ и
     * Отчет по обучающим семинарам в разрезе ТМЦ
     *
     */
    public function getConferenceReport(Request $request)
    {
        $val = \Validator::make($request->all(),
            ['type' => 'required|in:seminar,meeting']);
        if (!$val->passes()) {
            return response()->json(["errors" => $val->errors()], 422);
        }

        $fileFormat = $request->get('format', 'ajax');
        $range = self::getDateRange($request);

        $reports = \DB::table("telemed_centers as tmc")->join("telemed_event_requests as ter", "tmc.id", "=", "ter.telemed_center_id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
            ->join("med_care_facs as mcf", "mcf.id", "=", "tmc.med_care_fac_id")
            ->where('ter.event_type', $request->get('type'))
            ->whereBetween('ter.start_date', $range)
            ->select('tmc.terminal_name', 'tmc.terminal_number', "mcf.id as mcf_id", "mcf.name as mcf_name", 'tmc.name as tmc_name', "tmc.address as tmc_address", \DB::raw("count(ter.id) as count"), \DB::raw("count(case ter.importance when 'low' then 1 else null end) as low_count"), \DB::raw("count(case ter.importance when 'high' then 1 else null end) as high_count"), \DB::raw("count(case ter.importance when 'medium' then 1 else null end) as medium_count"), 'ter.event_type')->groupBy("tmc.id")
            ->orderBy('tmc.name', 'asc');
        if($request->has("tmcs"))
        {
            $tmcs = explode(",", $request->get('tmcs'));
            $reports = $reports->whereIn("tmc.id",$tmcs);
        }
        $reports = $reports->get();
        //return $reports;

        $view = "reports_and_statistics.conference-reports-template";
        $reportName = ($request->get('type') == 'seminar') ? 'Отчет по обучающим семинарам в разрезе ТМЦ ' : 'Отчет по совещаниям в разрезе ТМЦ';
        $fileName = $fileName =  $reportName . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();
        $header = ($request->get('type') == 'seminar') ? self::$seminarHeader : self::$conferenceHeader;
        if( $fileFormat == 'docx' ) {
            $addData = [];
            return self::getConferenceReportAsDocx($reports, $header, $fileName, $addData);
        } else {
            return self::DownloadAsFile($reports, $fileFormat, $view, $header, $fileName);
        }


    }
    protected static function getConferenceReportAsDocx($reports, $headers, $title, $additionalData){

        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000', 'width' => 100 * 100,  'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();
        $mcfs = [];

        foreach ($headers as $header) {
            $table->addCell()->addText($header);
        }

        foreach ($reports as $idx => $report) {
            $table->addRow();
            // № по порядку
            $table->addCell()->addText($idx + 1);
            // Наименование учреждения здравоохранения
            $table->addCell(100 * 25)->addText($report->mcf_name);
            // Наименование ТМЦ
            if(!array_key_exists($report->mcf_id,$mcfs)) {
                $mcfs[$report->mcf_id]['count'] = 1;
            }
            $table->addCell(100 * 25)->addText($mcfs[$report->mcf_id]['count'].") ".$report->tmc_name);
            $mcfs[$report->mcf_id]['count'] = $mcfs[$report->mcf_id]['count'] + 1;

            // Адрес ТМЦ
            $table->addCell(100 * 20)->addText($report->tmc_address);
            // E.164 ID (номер терминала)
            $table->addCell()->addText($report->terminal_number, $fontStyle, ['align' => 'center']);
            // H.323 ID (название терминала)
            $table->addCell(100 * 20)->addText($report->terminal_name, $fontStyle, ['align' => 'center']);
            // Количество клинических консультаций
            $table->addCell()->addText($report->count, $fontStyle, ['align' => 'center']);
            //   В том числе со степенью  важности высокая,
            $table->addCell()->addText($report->high_count, $fontStyle, ['align' => 'center']);
            // В том числе со степенью важности средняя
            $table->addCell()->addText($report->medium_count, $fontStyle, ['align' => 'center']);
            //В том числе со степенью важности низкая
            $table->addCell()->addText($report->low_count, $fontStyle, ['align' => 'center']);
        }

        self::setDocxHeaders($title);
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $file = $objWriter->save('php://output');
        return $file;
    }

    /**
     * Отчет по видеоконференциям по отдельному ТМЦ
     * (доступен администратору портала и администратору ТМЦ)
     * PieChert LineChart версия
     */
    public function getVideoConferenceReportCharts(Request $request, $tmc_id)
    {
        $range = self::getDateRange($request);

        $format = $request->get('download', 'url');

        $diagram_type = ($request->get('diagram') == '' || $request->get('diagram') == null) ? 'linechart' : $request->get('diagram');

        // return $range;
        $range1 = new Carbon($range[0]);
        $range2 = new Carbon($range[1]);
        /*        $range1 = new Carbon('2016-12-01');
                $range2 = new Carbon("2017-12-01");*/
        $difference = $range2->diffInDays($range1);
        $tmc = TelemedCenter::find($tmc_id);
        if ($tmc == null) {
            return response()->json(['error' => 'такого ТМЦ не существует!'], 404);
        }

        $mcf = $tmc->medCareFac;
        $reports = \DB::table('telemed_event_requests as ter')->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", "=", 'ter.id')
            ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
            ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
            ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
            ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
            ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
            ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
            ->where(function ($query) use ($tmc_id) {
                $query->where(function ($q) use ($tmc_id) {
                    // $q->where('tep.decision','agreed');
                    $q->where("ter.telemed_center_id", $tmc_id);
                    $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                });
                $query->orWhere(function ($q) use ($tmc_id) {
                    $q->where('tep.decision', 'agreed');
                    $q->where("tep.telemed_center_id", $tmc_id);
                    $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                });
                $query->orWhere(function ($q) use ($tmc_id) {
                    $q->where(function ($q1) use ($tmc_id) {
                        $q1->whereNotNull('abon.request_id');
                        $q1->where(function ($q2) use ($tmc_id) {
                            $q2->where('ter.telemed_center_id', $tmc_id);
                            $q2->orWhere("tcr.telemed_center_id", $tmc_id);
                        });
                        $q1->whereNotNull('tcr.telemed_center_id');
                        $q1->where('ter.event_type', 'consultation');
                    });
                    $q->where('tcr.decision', 'agreed');
                });
            })
            ->whereBetween('ter.start_date', $range)
            ->select('ter.id', 'ter.telemed_center_id as initiator_id', "ter.event_type", 'ter.start_date', 'types.name as consult_type',
                \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"),
                \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"),
                'cons_tmc.name as cons_tmc_name', 'abon.patient_indications_for_use_id', 'abon.patient_indications_for_use_other', 'ter.title',
                \DB::raw("COUNT(tep.id) as part_count"), 'ter.importance', 'abon.created_at as create_date', 'abon.doctor_fullname as medic')
            ->groupBy("ter.id")
            ->get();

        $reports = collect($reports);
        //return response()->json($reports);

        $text = "";
        if ($difference < 15) {
            $text = 'Соотношение количества видеоконференций по их типу по дням промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата";
            $reports = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('d-m-Y');
            });
        } else if ($difference < 110) {
            $text = 'Соотношение количества видеоконференций по их типу по неделям промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата(Год и Номер недели года)";
            $reports = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('W-0');
            });
        } else if ($difference < 367) {
            $text = 'Соотношение количества видеоконференций по их типу по месяцам промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата";
            $reports = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('m-Y');
            });
        } else if ($difference < 365.25 * 16 && $difference > 366) {
            $text = 'Соотношение количества видеоконференций по их типу по годам промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата";
            $reports = $reports->groupBy(function ($date) {
                return Carbon::parse($date->start_date)->format('Y');
            });
        }
        $reports = $reports->map(function ($x) {
            return $x->groupBy('event_type');
        });

        if ($diagram_type == 'linechart') {
            return
                self::generateLineChart($reports, $format, $text, 'Количество завершенных мероприятии', $abscissaTitle);
        } else if ($diagram_type == 'barchart') {
            return
                self::generateBarChart($reports, $format, $text, 'Количество завершенных мероприятии', $abscissaTitle);
        }


    }


    /**
     * Отчет по видеоконференциям по отдельному ТМЦ
     * route: /admin/mc_report/{tmc_id}
     * (доступен администратору портала и администратору ТМЦ)
     */
    public function getVideoConferenceReport(Request $request, $tmc_id)
    {
        $fileFormat = $request->get('format', 'ajax');
        $range = self::getDateRange($request);
        $tmc = TelemedCenter::find($tmc_id);

        if ($tmc == null) {
            return response()->json(['error' => 'такого ТМЦ не существует!'], 404);
        }

        $mcf = $tmc->medCareFac;
        $reports = \DB::table('telemed_event_requests as ter')->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
            ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
            ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
            ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
            ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
            ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
            ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
            ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
            ->where(function ($query) use ($tmc_id) {
                $query->where(function ($q) use ($tmc_id) {
                    /* $q->where('tep.decision','agreed');*/
                    $q->where("ter.telemed_center_id", $tmc_id);
                    $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                });
                $query->orWhere(function ($q) use ($tmc_id) {
                    $q->where('tep.decision', 'agreed');
                    $q->where("tep.telemed_center_id", $tmc_id);
                    $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                });
                $query->orWhere(function ($q) use ($tmc_id) {
                    $q->where(function ($q1) use ($tmc_id) {
                        $q1->whereNotNull('abon.request_id');
                        $q1->where(function ($q2) use ($tmc_id) {
                            $q2->where('ter.telemed_center_id', $tmc_id);
                            $q2->orWhere("tcr.telemed_center_id", $tmc_id);
                        });
                        $q1->whereNotNull('tcr.telemed_center_id');
                        $q1->where('ter.event_type', 'consultation');
                    });
                    $q->where('tcr.decision', 'agreed');
                });
            })
            ->whereBetween('ter.start_date', $range)
            ->select('ter.id', 'ter.telemed_center_id as initiator_id', "ter.event_type", 'ter.start_date', 'types.name as consult_type', \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"), \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"), 'cons_tmc.name as cons_tmc_name', 'abon.patient_indications_for_use_id', 'abon.patient_indications_for_use_other', 'ter.title', \DB::raw("COUNT(tep.id) as part_count"), 'ter.importance', 'abon.created_at as create_date', 'abon.doctor_fullname as medic')
            ->groupBy("ter.id")
            ->get();
        //return $reports;


        $view = "reports_and_statistics.all-type-events-template";

        $fileName = $fileName = 'Отчет по видеоконференциям по отдельному ТМЦ ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();

        if ($fileFormat == 'docx') {
            $addData = ['mcf' => $mcf, 'tmc' => $tmc, 'indForUse' => IndicationForUse::all()->pluck('name', 'id')];

            return self::getVideoConferenceReportAsDocx($reports, self::$allTypeEventsHeader, $fileName, $addData);

        } else {
            return self::DownloadAsFile($reports, $fileFormat, $view, self::$allTypeEventsHeader, $fileName, $tmc, $mcf);
        }


    }

    public function getVideoConferenceReportForManyTMC(Request $request)
    {
        $tmcs = explode(",", $request->get('tmcs'));
        $fileFormat = $request->get('format', 'ajax');
        $range = self::getDateRange($request);
        $tmcs = TelemedCenter::whereIn('id', $tmcs)->get();
        if (count($tmcs) == 0) {
            return response()->json(['error' => 'таких ТМЦ не существует!'], 404);
        }
        $reportsarray = [];
        foreach ($tmcs as $tmc) {
            $tmc_id = $tmc->id;
            $obj = [];
            $obj['tmc'] = $tmc;
            $obj['mcf'] = $tmc->medCareFac;

            $reports = \DB::table('telemed_event_requests as ter')->leftJoin('telemed_event_participants as tep', "tep.request_id", "=", "ter.id")
                ->join('telemed_event_conclusion as teco', "teco.request_id", '=', 'ter.id')
                ->leftJoin('telemed_consult_requests as tcr', "tcr.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_abonent as abon', "abon.request_id", "=", "ter.id")
                ->leftJoin('telemed_requests_mcf_consultant as cons', "cons.request_id", "=", "ter.id")
                ->leftJoin('telemed_centers as cons_tmc', "cons_tmc.id", "=", "tcr.telemed_center_id")
                ->leftJoin('doctors as d', "d.id", "=", "cons.doctor_id")
                ->leftJoin('consult_types as types', "abon.consult_type", '=', 'types.id')
                ->where(function ($query) use ($tmc_id) {
                    $query->where(function ($q) use ($tmc_id) {
                        /* $q->where('tep.decision','agreed');*/
                        $q->where("ter.telemed_center_id", $tmc_id);
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        /*  $q->where('tep.decision', 'agreed');*/
                        $q->where("tep.telemed_center_id", $tmc_id);
                        $q->whereIn('ter.event_type', ['seminar', 'meeting']);
                    });
                    $query->orWhere(function ($q) use ($tmc_id) {
                        $q->where(function ($q1) use ($tmc_id) {
                            $q1->whereNotNull('abon.request_id');
                            $q1->where(function ($q2) use ($tmc_id) {
                                $q2->where('ter.telemed_center_id', $tmc_id);
                                $q2->orWhere("tcr.telemed_center_id", $tmc_id);
                            });
                            $q1->whereNotNull('tcr.telemed_center_id');
                            $q1->where('ter.event_type', 'consultation');
                        });
                        $q->where('tcr.decision', 'agreed');
                    });
                })
                ->whereBetween('ter.start_date', $range)
                ->select('ter.id', 'ter.telemed_center_id as initiator_id', "ter.event_type", 'ter.start_date', 'types.name as consult_type', \DB::raw("CONCAT(tcr.uid_code,'-',tcr.uid_year,'-',tcr.uid_id) as cons_uid"), \DB::raw("CONCAT(d.surname,' ',d.name,' ',d.middlename) as consult_doctor"), 'cons_tmc.name as cons_tmc_name', 'abon.patient_indications_for_use_id', 'abon.patient_indications_for_use_other', 'ter.title', \DB::raw("COUNT(tep.id) as part_count"), 'ter.importance', 'abon.created_at as create_date', 'abon.doctor_fullname as medic')
                ->groupBy("ter.id")
                ->get();
            $obj['reports'] = $reports;
            array_push($reportsarray, $obj);
        }

        //return $reports;


        $view = "reports_and_statistics.all-type-events-many-tmcs-template";

        $fileName = $fileName = 'Отчет по видеоконференциям по различным ТМЦ ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();

        if( $fileFormat == 'docx') {
            $addData = ['tmc' => $tmc, 'indForUse' => IndicationForUse::all()->pluck('name', 'id')];
            return self::getVideoConferenceReportForManyTmcAsDocx($reportsarray, self::$allTypeEventsHeader, $fileName, $addData);
        } else {
            return self::DownloadAsFile($reportsarray, $fileFormat, $view, self::$allTypeEventsHeader, $fileName);
        }
    }
    protected static function getVideoConferenceReportForManyTmcAsDocx($reports, $headers, $title, $additionalData) {

        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000'];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();

        foreach ($headers as $header) {
            $table->addCell()->addText($header);
        }
        $i = 1;
        //return $reports;
        foreach($reports as $r) {
            foreach ($r['reports'] as $idx => $report) {
                $table->addRow();
                // № по порядку
                $table->addCell()->addText($i);
                $i++;
                // Наименование учреждения здравоохранения
                $table->addCell()->addText($r['mcf']->name);
                // Наименование ТМЦ
                $table->addCell()->addText($r['tmc']->name);
                // Адрес ТМЦ
                $table->addCell()->addText($r['tmc']->address);
                // Дата подачи заявки
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->create_date);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Дата проведения ВКС
                $table->addCell()->addText($report->start_date);
                // Входящая/ исходящая ВКС
                $inOut = '';
                if ($additionalData['tmc']->id != $report->initiator_id) $inOut = 'Входящая'; else $inOut = 'Исходящая';
                $table->addCell()->addText($inOut);
                // Вид ВКС
                $table->addCell()->addText(self::$eventTypes[$report->event_type]);
                // Тип ТМК
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->consult_type);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Номер заявки
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->cons_uid);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Лечащий врач
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->medic);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Наименование ТМЦ консультанта
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->cons_tmc_name);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Врач консультант
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText(is_null($report->consult_doctor) ? '*не указан*' : $report->consult_doctor);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Показание к применению
                if ($report->event_type == 'consultation') {
                    if ($report->patient_indications_for_use_id != 'other')
                        $table->addCell()->addText(array_key_exists($report->patient_indications_for_use_id, $additionalData['indForUse']) ? $additionalData['indForUse'][$report->patient_indications_for_use_id] : 'не указано');
                    else
                        $table->addCell()->addText("Иное: " . $report->patient_indications_for_use_other);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Тема обучающего семинара
                if ($report->event_type == 'seminar') {
                    $table->addCell()->addText($report->title);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Тема совещания
                if ($report->event_type == 'meeting') {
                    $table->addCell()->addText($report->title);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Количество участников
                if ($report->event_type == 'consultation') {
                    $table->addCell()->addText($report->part_count, $fontStyle, ['align' => 'center']);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }
                // Степень важности
                if ($report->event_type == 'consultation') {
                    if (array_key_exists($report->importance, self::$importances))
                        $table->addCell()->addText(self::$importances[$report->importance]);
                    else
                        $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                } else {
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
                }

            }
        }


        self::setDocxHeaders($title);
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $file = $objWriter->save('php://output');
        return $file;
    }
    /*public function getAllTypesReport($tmc_id, Request $request)
    {
        $reports = $this->getVideoConferenceReport($tmc_id);


    }*/

    public function DownloadAsFile($data, $format, $view, $headers, $fileName = 'newfile', $tmc = null, $mcf = null)
    {
        //return $mcf;


        if ($format == 'ajax') {
            //return 'dasdas';
            return view($view)->with('headers', $headers)
                ->with('reports', $data)
                ->with('eventTypes', self::$eventTypes)
                ->with('importances', self::$importances)
                ->with('consultTypes', self::$consultTypes)
                ->with('indForUse', self::$indForUse)
                ->with('tmc', $tmc)
                ->with('mcf', $mcf)
                ->with('full', true)
                ->with('format', $format);


        } elseif ($format == 'xls' || $format == 'csv') {

            $fileName = $fileName . '.' . $format;


            //return var_dump($reports);
            return \Excel::create($fileName, function ($excel) use ($data, $view, $tmc, $mcf, $headers, $format) {

                $excel->sheet('Страница 1', function ($sheet) use ($data, $view, $tmc, $mcf, $headers, $format) {
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'DejaVu Sans',
                            'size' => 8,
                            'bold' => false
                        )
                    ));
                    $sheet->loadView($view)->with('headers', $headers)
                        ->with('reports', $data)
                        ->with('eventTypes', self::$eventTypes)
                        ->with('importances', self::$importances)
                        ->with('consultTypes', self::$consultTypes)
                        ->with('indForUse', self::$indForUse)
                        ->with('tmc', $tmc)
                        ->with('mcf', $mcf)
                        ->with('format', $format)
                        ->with('full', true);

                });

            })->export($format);

        } elseif ($format == 'pdf') {
            $html = view($view)->with('headers', $headers)
                ->with('reports', $data)
                ->with('eventTypes', self::$eventTypes)
                ->with('importances', self::$importances)
                ->with('consultTypes', self::$consultTypes)
                ->with('indForUse', self::$indForUse)
                ->with('tmc', $tmc)
                ->with('mcf', $mcf)
                ->with('format', $format)
                ->with('full', true);

            $pdf = \PDF::loadHTML($html);

            $pdf->setOption('encoding', 'utf-8');
            $pdf->setOrientation('landscape');

            return $pdf->download($fileName . '.pdf');

        } else {
            abort(501);
        }

    }

    //
    public function index()
    {
        $array = [];
        $tmcs = TelemedCenter::join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
            ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
            ->get();

        foreach ($tmcs as $key => $item) {
            $array[$item['mcf_name']] = array($item['id'] => $item['name']);
        }

        return view('admin.statistics.index', [
            'tmcs' => $tmcs,
            'lists' => $array
        ]);
    }

    /** Нарисовать колончатый график  */
    private static function generateBarChart($reports, $format = 'jpeg', $reportTitle = 'Отчет', $ordinateTitle = 'Количество завершенных мероприятии', $abscissaTitle = 'Абсцисса')
    {

        $imgWidth = 1280;
        $imgHeight = 800;

        $factory = new pCharts();

        // Create and populate data
        $myData = $factory->newData();

        /* Если нет данных вернуть изображение с сообщением */
        if (count($reports) == 0) {
            return \Image::make('images/charts-error.png')->response();
        } else {
            // Create the image
            $myPicture = $factory->newImage($imgWidth, $imgHeight, $myData);

            /* Set the default font */
            $myPicture->setFontProperties(array("FontName" => public_path("fonts/arial/arial.ttf"), "FontSize" => 12));
        }
        $seminars = [];
        $consultations = [];
        $meetings = [];
        //return $reports;
        foreach ($reports as $key => $report) {

            foreach ($report as $k => $v) {
                if ($k == 'consultation') {
                    $consultations[] = intval(count($v));
                    $seminars[] = VOID;
                    $meetings[] = VOID;
                } else if ($k == 'seminar') {
                    $seminars[] = intval(count($v));
                    $consultations[] = VOID;
                    $meetings[] = VOID;
                } else if ($k == 'meeting') {
                    $meetings[] = count($v);
                    $consultations[] = VOID;
                    $seminars[] = VOID;
                }

            }
        }
        //return response()->json([$consultations, $seminars, $meetings]);

        $myData->addPoints($consultations, self::$eventTypes['consultation']);
        $myData->addPoints($seminars, self::$eventTypes['seminar']);
        $myData->addPoints($meetings, self::$eventTypes['meeting']);
        $myData->setAxisName(0, $ordinateTitle);
        // Get maximum value of Ordinate;
        $maxValue = max(max($consultations), max($seminars), max($meetings));
        $maxValue = ($maxValue < 4) ? 4 : $maxValue;
        $datesArray = array_keys($reports->toArray());

        //  Add points to  Abscissa
        $myData->addPoints($datesArray, $abscissaTitle);

        $myData->setSerieDescription($abscissaTitle, $abscissaTitle);
        $myData->setAbscissa($abscissaTitle);

        // Set colors of bars
        $myData->setPalette(self::$eventTypes['consultation'], hex2rgb(\Config::get('color-scheme.consultation', '#33cc33')));
        $myData->setPalette(self::$eventTypes['seminar'], hex2rgb(\Config::get('color-scheme.seminar', '#00ccff')));
        $myData->setPalette(self::$eventTypes['meeting'], hex2rgb(\Config::get('color-scheme.meeting', '#ff6600')));

        /* Add a border to the picture */
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


        /* Write the chart title */
        $myPicture->drawText(50, 30, $reportTitle, array("FontSize" => 14, "Align" => TEXT_ALIGN_BOTTOMLEFT));
        /* Write the Abscissa Title*/
        $myPicture->drawText(50, $imgHeight - 30, $abscissaTitle, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));


        $myPicture->drawRectangle(0, 0, $imgWidth, $imgHeight, array("R" => 0, "G" => 0, "B" => 0));


        /* Define the chart area */
        $myPicture->setGraphArea(60, 60, $imgWidth - 50, $imgHeight - 100);

        /* Draw the scale */
        $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
        $scaleSettings = array("GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => false, "CycleBackground" => TRUE, 'Mode' => SCALE_MODE_MANUAL, 'Factors' => array(1), 'ManualScale' => $scaleConfig);
        $myPicture->drawScale($scaleSettings);

        /* Write the chart legend */
        $myPicture->drawLegend(900, $imgHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

        /* Turn on shadow computing */
        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));


        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
        $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
        /* Draw the chart */
        $myPicture->drawBarChart($settings);

        // Prepare file to download or show
        $fileName = hash('ripemd160', $reportTitle . round(microtime(true) * 1000));

        /* Render the picture (choose the best way) */
        if ($format == 'pdf') {
            return \PDF::loadHTML('<img src="' . str_replace('download=pdf', 'download=url', request()->fullUrl()) . '" >')->setOrientation('landscape')->download($fileName . '.pdf');
        } else if ($format == 'jpeg' || $format == 'url') {
            return $myPicture->stroke();
        }

    }


    /**
     * нарисовать линейный график
     *
     */
    private static function generateLineChart($reports, $format = 'jpeg', $reportTitle = 'Отчет', $ordinateTitle = 'Количество завершенных мероприятии', $abscissaTitle = 'Абсцисса')
    {

        $imgWidth = 1280;
        $imgHeight = 800;

        $factory = new pCharts();

        // Create and populate data
        $myData = $factory->newData();

        /* Если нет данных вернуть изображение с сообщением */
        if (count($reports) == 0) {
            return \Image::make('images/charts-error.png')->response();
        } else {
            // Create the image
            $myPicture = $factory->newImage($imgWidth, $imgHeight, $myData);

            /* Set the default font */
            $myPicture->setFontProperties(array("FontName" => public_path("fonts/arial/arial.ttf"), "FontSize" => 12));
        }
        $seminars = [];
        $consultations = [];
        $meetings = [];
        //return $reports;
        foreach ($reports as $key => $report) {

            foreach ($report as $k => $v) {
                if ($k == 'consultation') {
                    $consultations[] = intval(count($v));
                    $seminars[] = VOID;
                    $meetings[] = VOID;
                } else if ($k == 'seminar') {
                    $seminars[] = intval(count($v));
                    $consultations[] = VOID;
                    $meetings[] = VOID;
                } else if ($k == 'meeting') {
                    $meetings[] = count($v);
                    $consultations[] = VOID;
                    $seminars[] = VOID;
                }

            }
        }
        //return response()->json([$consultations, $seminars, $meetings]);

        /* Create and populate the pData object */
        $myData->addPoints($consultations, self::$eventTypes['consultation']);
        $myData->addPoints($seminars, self::$eventTypes['seminar']);
        $myData->addPoints($meetings, self::$eventTypes['meeting']);

        $myData->setAxisName(0, $ordinateTitle);
        // Get maximum value of Ordinate;
        $maxValue = max(max($consultations), max($seminars), max($meetings));
        $maxValue = ($maxValue < 4) ? 4 : $maxValue;
        $datesArray = array_keys($reports->toArray());

        //  Add points to  Abscissa
        $myData->addPoints($datesArray, $abscissaTitle);

        $myData->setSerieDescription($abscissaTitle, $abscissaTitle);
        $myData->setAbscissa($abscissaTitle);

        // Set colors of bars
        $myData->setPalette(self::$eventTypes['consultation'], hex2rgb(\Config::get('color-scheme.consultation', '#33cc33')));
        $myData->setPalette(self::$eventTypes['seminar'], hex2rgb(\Config::get('color-scheme.seminar', '#00ccff')));
        $myData->setPalette(self::$eventTypes['meeting'], hex2rgb(\Config::get('color-scheme.meeting', '#ff6600')));

        $myData->setSerieWeight(self::$eventTypes['consultation'], 2);
        $myData->setSerieWeight(self::$eventTypes['seminar'], 2);
        $myData->setSerieWeight(self::$eventTypes['meeting'], 2);
        /* Add a border to the picture */
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


        /* Write the chart title */
        $myPicture->drawText(50, 30, $reportTitle, array("FontSize" => 14, "Align" => TEXT_ALIGN_BOTTOMLEFT));
        /* Write the Abscissa Title*/
        $myPicture->drawText(50, $imgHeight - 30, $abscissaTitle, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));


        $myPicture->drawRectangle(0, 0, $imgWidth, $imgHeight, array("R" => 0, "G" => 0, "B" => 0));


        /* Define the chart area */
        $myPicture->setGraphArea(60, 60, $imgWidth - 50, $imgHeight - 100);

        /* Draw the scale */
        $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
        $scaleSettings = array("GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => false, "CycleBackground" => TRUE, 'Mode' => SCALE_MODE_MANUAL, 'Factors' => array(1), 'ManualScale' => $scaleConfig);
        $myPicture->drawScale($scaleSettings);

        /* Write the chart legend */
        $myPicture->drawLegend(900, $imgHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

        /* Turn on shadow computing */
        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));


        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
        $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE, "DisplayColor" => DISPLAY_AUTO);
        $myPicture->setShadow(FALSE);

        /* Draw the chart */
        $myPicture->drawLineChart($settings);

        // Prepare file to download or show
        $fileName = hash('ripemd160', $reportTitle . round(microtime(true) * 1000));

        /* Render the picture (choose the best way) */
        if ($format == 'pdf') {
            return \PDF::loadHTML('<img src="' . str_replace('download=pdf', 'download=url', request()->fullUrl()) . '" >')->setOrientation('landscape')->download($fileName . '.pdf');
        } else if ($format == 'jpeg' || $format == 'url') {
            return $myPicture->stroke();
        }
    }

    /** Генерация docx (MS WORRD) документа для  "Отчет по видеоконференциям по отдельному ТМЦ" */
    protected static function getVideoConferenceReportAsDocx($reports, $headers, $title, $additionalData)
    {

        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000'];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();

        foreach ($headers as $header) {
            $table->addCell()->addText($header);
        }
        foreach ($reports as $idx => $report) {
            $table->addRow();
            // № по порядку
            $table->addCell()->addText($idx + 1);
            // Наименование учреждения здравоохранения
            $table->addCell()->addText($additionalData['mcf']->name);
            // Наименование ТМЦ
            $table->addCell()->addText($additionalData['tmc']->name);
            // Адрес ТМЦ
            $table->addCell()->addText($additionalData['tmc']->address);
            // Дата подачи заявки
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->create_date);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Дата проведения ВКС
            $table->addCell()->addText($report->start_date);
            // Входящая/ исходящая ВКС
            $inOut = '';
            if ($additionalData['tmc']->id != $report->initiator_id) $inOut = 'Входящая'; else $inOut = 'Исходящая';
            $table->addCell()->addText($inOut);
            // Вид ВКС
            $table->addCell()->addText(self::$eventTypes[$report->event_type]);
            // Тип ТМК
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->consult_type);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Номер заявки
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->cons_uid);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Лечащий врач
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->medic);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Наименование ТМЦ консультанта
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->cons_tmc_name);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Врач консультант
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText(is_null($report->consult_doctor) ? '*не указан*' : $report->consult_doctor);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Показание к применению
            if ($report->event_type == 'consultation') {
                if ($report->patient_indications_for_use_id != 'other')
                    $table->addCell()->addText(array_key_exists($report->patient_indications_for_use_id, $additionalData['indForUse']) ? $additionalData['indForUse'][$report->patient_indications_for_use_id] : 'не указано');
                else
                    $table->addCell()->addText("Иное: " . $report->patient_indications_for_use_other);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Тема обучающего семинара
            if ($report->event_type == 'seminar') {
                $table->addCell()->addText($report->title);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Тема совещания
            if ($report->event_type == 'meeting') {
                $table->addCell()->addText($report->title);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Количество участников
            if ($report->event_type == 'consultation') {
                $table->addCell()->addText($report->part_count, $fontStyle, ['align' => 'center']);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }
            // Степень важности
            if ($report->event_type == 'consultation') {
                if (array_key_exists($report->importance, self::$importances))
                    $table->addCell()->addText(self::$importances[$report->importance]);
                else
                    $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            } else {
                $table->addCell()->addText('-', $fontStyle, ['align' => 'center']);
            }

        }

        self::setDocxHeaders($title);
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $file = $objWriter->save('php://output');
        return $file;


    }

    protected static function setDocxHeaders($title)
    {
        header('Pragma: no-cache');
        // Mark file as already expired for cache; mark with RFC 1123 Date Format up to
        // 1 year ahead for caching (ex. Thu, 01 Dec 1994 16:00:00 GMT)
        header('Expires: 0');
        // Forces cache to re-validate with server
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        // DocX Content Type
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        // Tells browser we are sending file
        header('Content-Disposition: attachment; filename=' . $title . '.docx;');
        // Tell proxies and gateways method of file transfer
        header('Content-Transfer-Encoding: binary');
    }
}
