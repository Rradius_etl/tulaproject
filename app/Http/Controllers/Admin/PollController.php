<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreatePollRequest;
use App\Http\Requests\Admin\UpdatePollRequest;
use App\Repositories\Admin\PollRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PollController extends InfyOmBaseController
{
    /** @var  PollRepository */
    private $pollRepository;

    public function __construct(PollRepository $pollRepo)
    {
        $this->pollRepository = $pollRepo;
    }

    /**
     * Display a listing of the Poll.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pollRepository->pushCriteria(new RequestCriteria($request));
        $polls = $this->pollRepository->all();

        return view('admin.polls.index')
            ->with('polls', $polls);
    }

    /**
     * Show the form for creating a new Poll.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.polls.create');
    }

    /**
     * Store a newly created Poll in storage.
     *
     * @param CreatePollRequest $request
     *
     * @return Response
     */
    public function store(CreatePollRequest $request)
    {
        $input = $request->all();

        $poll = $this->pollRepository->create($input);

        Flash::success(trans('backend.Poll') . trans('backend.success_saved'));

        return redirect(route('admin.polls.index'));
    }

    /**
     * Display the specified Poll.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $poll = $this->pollRepository->findWithoutFail($id);

        if (empty($poll)) {
            Flash::error(trans('backend.Poll') . trans('backend.not_found') );

            return redirect(route('admin.polls.index'));
        }

        return view('admin.polls.show')->with('poll', $poll);
    }

    /**
     * Show the form for editing the specified Poll.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $poll = $this->pollRepository->findWithoutFail($id);

        if (empty($poll)) {
             Flash::error(trans('backend.Poll') . trans('backend.not_found') );

            return redirect(route('admin.polls.index'));
        }

        return view('admin.polls.edit')->with('poll', $poll);
    }

    /**
     * Update the specified Poll in storage.
     *
     * @param  int              $id
     * @param UpdatePollRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePollRequest $request)
    {
        $poll = $this->pollRepository->findWithoutFail($id);

        if (empty($poll)) {
             Flash::error(trans('backend.Poll') . trans('backend.not_found') );

            return redirect(route('admin.polls.index'));
        }

        $poll = $this->pollRepository->update($request->all(), $id);

       Flash::success(trans('backend.Poll') . trans('backend.success_updated'));

        return redirect(route('admin.polls.index'));
    }

    /**
     * Remove the specified Poll from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $poll = $this->pollRepository->findWithoutFail($id);

        if (empty($poll)) {
            Flash::error(trans('backend.Poll') . trans('backend.not_found') );

            return redirect(route('admin.polls.index'));
        }

        $this->pollRepository->delete($id);

        Flash::success(trans('backend.Poll') . trans('backend.success_deleted'));

        return redirect(route('admin.polls.index'));
    }
}
