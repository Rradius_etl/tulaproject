<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //

    public function index()
    {

        return 0;
    }

    public static $fieldTypes = [

        'website-settings.site-name' => 'text',
        'website-settings.welcome-text' => 'text',
        'website-settings.meta-description' => 'textarea',
        'website-settings.meta-keywords' => 'text',
        'website-settings.slider-time'      =>      'number',
        'website-settings.welcome-enabled'      =>      'select',
        'website-settings.slider-autoplay'      =>      'select',
        // mail
        'mail.driver'      =>      'select',
        'mail.encryption'      =>      'select',
        'mail.host'      =>      'text',
        'mail.port'      =>      'text',
        'mail.username'      =>      'text',
        'mail.password'      =>      'text',
        'mail.email'      =>      'email',
        // Color Scheme
        'color-scheme.consultation'      =>      'color',
        'color-scheme.meeting'      =>      'color',
        'color-scheme.seminar'      =>      'color',

    ];

    public static $options = [
        'mail.driver' => [
            'smtp' => 'SMTP',
            'mail' => 'PHP mail',
            'sendmail' => 'sendmail',

        ],
        'mail.encryption' => [
            'ssl' => 'SSL',
            'tls' => 'TLS',
        ],
        'website-settings.slider-autoplay' => [
            'true'  =>  'Да',
            'false'  =>  'Нет',
        ],
        'website-settings.welcome-enabled' => [
            'true'  =>  'Да',
            'false'  =>  'Нет',
        ]
    ];

    /** Страница настроек **/
    public function siteSettings()
    {

       
        $fieldTypes = self::$fieldTypes;
        
        $settings['website-settings'] = \Config::get('website-settings');



        $settings['mail'] = \Config::get('mail');
        
        $settings['color-scheme'] =\Config::get('color-scheme');
        $selectOptions = self::$options;
        
        //return response()->json($settings);
        return view('admin.settings.site-settings', compact('settings', 'fieldTypes', 'selectOptions'));
    }

    public function saveSettings(Request $request)
    {
        $input = $request->all();

        $rules = [
            'website-settings_email' => 'sometimes|required|email',
            'website-settings_slider-time' => 'sometimes|required|integer',
            'website-settings_slider-autoplay' => 'sometimes|required',
            'website-settings_slider-welcome-enabled' => 'sometimes|required',


        ];

        $messages = [
            'APP_DEBUG.min' => ':attribute может быть только true или false'
        ];

        $validator = Validator::make($input, $rules, $messages);




        if( $validator->passes()) {



            //return response()->json($a);

            if( $saved = self::saveData($input) ) {
                return \Redirect::back()->withSuccess('Настройки успешно сохранены');
            } else {
                return \Redirect::back()->withInput()->withErrors('Что то пошло не так');
            }

        } else {
            return \Redirect::back()->withInput()->withErrors($validator->messages());
        }
    }

    /** Сохранение изменений с проверкой на ключевые значения **/

    public function saveData(array  $input)
    {
        try {
            foreach ($input as $key => $value) {

                $config_name = str_replace('_', '.', $key);
                if( array_key_exists($config_name, self::$fieldTypes)) {
                    \Config::write($config_name, $value);
                    //  $a = array_add($a, $config_name, $value);
                }
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }


    }
}
