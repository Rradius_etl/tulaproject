<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateMedicalProfileRequest;
use App\Http\Requests\Admin\UpdateMedicalProfileRequest;
use App\Models\Admin\MedicalProfileCategory;
use App\Repositories\Admin\MedicalProfileRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MedicalProfileController extends InfyOmBaseController
{
    /** @var  MedicalProfileRepository */
    private $medicalProfileRepository;

    public function __construct(MedicalProfileRepository $medicalProfileRepo)
    {
        $this->medicalProfileRepository = $medicalProfileRepo;
    }

    /**
     * Display a listing of the MedicalProfile.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->medicalProfileRepository->pushCriteria(new RequestCriteria($request));
        $medicalProfiles = $this->medicalProfileRepository->with('medicalProfileCategory')->paginate(30);

        return view('admin.medical_profiles.index')
            ->with('medicalProfiles', $medicalProfiles);
    }

    /**
     * Show the form for creating a new MedicalProfile.
     *
     * @return Response
     */
    public function create()
    {
        $categories = MedicalProfileCategory::all()->lists('name', 'id');
        return view('admin.medical_profiles.create', compact('categories'));
    }

    /**
     * Store a newly created MedicalProfile in storage.
     *
     * @param CreateMedicalProfileRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalProfileRequest $request)
    {
        $input = $request->all();

        $medicalProfile = $this->medicalProfileRepository->create($input);

        Flash::success(trans('backend.MedicalProfile') . trans('backend.success_saved_f'));

        return redirect(route('admin.medical-profiles.index'));
    }

    /**
     * Display the specified MedicalProfile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medicalProfile = $this->medicalProfileRepository->findWithoutFail($id);

        if (empty($medicalProfile)) {
            Flash::error(trans('backend.MedicalProfile') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profiles.index'));
        }

        return view('admin.medical_profiles.show')->with('medicalProfile', $medicalProfile);
    }

    /**
     * Show the form for editing the specified MedicalProfile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medicalProfile = $this->medicalProfileRepository->findWithoutFail($id);

        if (empty($medicalProfile)) {
             Flash::error(trans('backend.MedicalProfile') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profiles.index'));
        }

        $categories = MedicalProfileCategory::all()->lists('name', 'id');

        return view('admin.medical_profiles.edit', compact('categories'))->with('medicalProfile', $medicalProfile);
    }

    /**
     * Update the specified MedicalProfile in storage.
     *
     * @param  int              $id
     * @param UpdateMedicalProfileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalProfileRequest $request)
    {
        $medicalProfile = $this->medicalProfileRepository->findWithoutFail($id);

        if (empty($medicalProfile)) {
             Flash::error(trans('backend.MedicalProfile') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profiles.index'));
        }

        $medicalProfile = $this->medicalProfileRepository->update($request->all(), $id);

       Flash::success(trans('backend.MedicalProfile') . trans('backend.success_updated_f'));

        return redirect(route('admin.medical-profiles.index'));
    }

    /**
     * Remove the specified MedicalProfile from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medicalProfile = $this->medicalProfileRepository->findWithoutFail($id);

        if (empty($medicalProfile)) {
            Flash::error(trans('backend.MedicalProfile') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profiles.index'));
        }

        $this->medicalProfileRepository->delete($id);

        Flash::success(trans('backend.MedicalProfile') . trans('backend.success_deleted_f'));

        return redirect(route('admin.medical-profiles.index'));
    }
}
