<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedConsultRequestRequest;
use App\Http\Requests\Admin\UpdateTelemedConsultRequestRequest;
use App\Repositories\TelemedConsultRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
/** Класс не используется вообще **/
class TelemedConsultRequestController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    public function __construct(TelemedConsultRequestRepository $telemedConsultRequestRepo)
    {
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
    }

    /**
     * Display a listing of the TelemedConsultRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->telemedConsultRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedConsultRequests = $this->telemedConsultRequestRepository->all();

        return view('admin.telemed_consult_requests.index')
            ->with('telemedConsultRequests', $telemedConsultRequests);
    }

    /**
     * Show the form for creating a new TelemedConsultRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.telemed_consult_requests.create');
    }

    /**
     * Store a newly created TelemedConsultRequest in storage.
     *
     * @param CreateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedConsultRequestRequest $request)
    {
        $input = $request->all();

        $telemedConsultRequest = $this->telemedConsultRequestRepository->create($input);

        Flash::success(trans('backend.TelemedConsultRequest') . trans('backend.success_saved'));

        return redirect(route('admin.telemed-consult-requests.index'));
    }

    /**
     * Display the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-consult-requests.index'));
        }

        return view('admin.telemed_consult_requests.show')->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Show the form for editing the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-consult-requests.index'));
        }

        return view('admin.telemed_consult_requests.edit')->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Update the specified TelemedConsultRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedConsultRequestRequest $request)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-consult-requests.index'));
        }

        $telemedConsultRequest = $this->telemedConsultRequestRepository->update($request->all(), $id);

       Flash::success(trans('backend.TelemedConsultRequest') . trans('backend.success_updated'));

        return redirect(route('admin.telemed-consult-requests.index'));
    }

    /**
     * Remove the specified TelemedConsultRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-consult-requests.index'));
        }

        $this->telemedConsultRequestRepository->delete($id);

        Flash::success(trans('backend.TelemedConsultRequest') . trans('backend.success_deleted'));

        return redirect(route('admin.telemed-consult-requests.index'));
    }
}
