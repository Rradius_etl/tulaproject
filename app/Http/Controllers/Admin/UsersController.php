<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\InterfaceElement;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\AppBaseController;
use Sentinel;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class UsersController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /** Правила валидации **/
    public static $rules = [
        'name' => 'sometimes|required',
        'surname' => 'sometimes|required',
        'middlename' => 'sometimes|required',
        'position' => 'sometimes|required',
        'email' => 'sometimes|required|email|unique:users',
    ];

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = Sentinel::getUserRepository();
        $this->roleRepository = Sentinel::getRoleRepository();

    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /** Поиск **/
        if (($request->input("search") != null)) {
            $users = $this->userRepository
                ->where('email', "like", "%" . $request->input("search") . "%")->paginate(15);
        } else {
            $users = $this->userRepository->paginate(15);
        }
        
        
        return view('admin.users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {

        $roles = $this->roleRepository->all()->lists('name', 'id');


        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, self::$rules);
        $input = $request->all();
        $credentials = ['email' => $request->email];
        $input['password'] = $this->generateStrongPassword();
        if ($user = Sentinel::findByCredentials($credentials)) {
            return redirect(route("admin.users.create"))
                ->withErrors('Такой Email уже зарегистрирован.');
        }

        if ($sentuser = Sentinel::register($input)) {
            $activation = Activation::create($sentuser);
            $code = $activation->code;
            $password = $input['password'];
            /** Отправка почты **/
            $sent = Mail::send('emails.auth.account_activate', compact('sentuser', 'code', 'password'), function ($m) use ($sentuser) {
                $m->from(config('mail.username'), config('mail.username'));
                $m->to($sentuser->email)->subject('Активация аккаунта');
            });
            if ($sent === 0) {
                return Redirect::to('register')
                    ->withErrors('Ошибка отправки письма активации.');
            }

            /** Присвоивание ролей */
            foreach ($request->get('roles') as $id) {
                $role = Sentinel::findRoleById($id);
                $role->users()->attach($sentuser);
            }
            Flash::success('Аккаунт был создан. Письмо с активацией отправлено на электронную почту');
            return redirect(route("admin.users.index"))
                ->with('userId', $sentuser->getUserId());
        }
        return redirect(route("admin.users.create"))
            ->withInput()
            ->withErrors('Failed to register.');


    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findById($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('admin.users.index'));
        }

        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findById($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('admin.users.index'));
        }

        $roles = $this->roleRepository->all()->lists('name', 'id');
        $user_rules = $user->roles();
        $current_rules = $user_rules->lists('id')->toArray();


        return view('admin.users.edit', compact('roles', 'current_rules'))->with('user', $user)->with('isActive', \Activation::completed($user));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        return $this->processForm('update', $id);

    }

    /**
     * Show the form for editing the specified User interface access.
     *
     * @param  int $id
     *
     * @return Response
     */
    /** Функция редактирования видимости элементов интерфейса для определенного пользователя **/
    public function editInterface($id)
    {
        $user = $this->userRepository->findById($id);


        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('admin.users.index'));
        }

        $disabledInterfaceElements = $user->disabledInterfaceElements->pluck('id')->toArray();
        $interfaceElements = InterfaceElement::all();

        //return $disabledInterfaceElements;

        return view('admin.users.edit-interface', compact('interfaceElements', 'disabledInterfaceElements'))
            ->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function updateInterface($id, Request $request)
    {
        $input = $request->all();

        $user = User::find($id);
        //return response()->json($input);
        if( $input['val'] == 'true' ) {
            $user->disabledInterfaceElements()->detach($input['element_id']);
            return response()->json(0);
        } else {
            $user->disabledInterfaceElements()->attach($input['element_id']);
            return response()->json(1);
        }

    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findById($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('admin.users.index'));
        }
        try{
            $user->delete();
        }
        catch (\Illuminate\Database\QueryException $e)
        {
            Flash::error('Нельзя удалить! Пользователь призяван к одной или нескольким сущностям');

            return redirect(route('admin.users.index'));
        }

        Flash::success('User deleted successfully.');

        return redirect(route('admin.users.index'));
    }
    
    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {


        $input = array_filter(Input::all());

        $rules = self::$rules;

        if ($id) {
            $user = $this->userRepository->createModel()->find($id);
            $rules['email'] .= ",email,{$user->email},email";
            $messages = $this->validateUser($input, $rules);

            /** Add Roles */
            $new_roles = Input::get('roles');
            $user_rules = $user->roles();
            $current_rules = $user_rules->lists('id')->toArray();

            $roles_to_save = array_diff($new_roles, $current_rules);

            $roles_to_delete = array_diff($current_rules, $new_roles);
            //return response()->json($roles_to_delete);

            foreach ($roles_to_save as $id) {
                $role = Sentinel::findRoleById($id);
                $role->users()->attach($user);
            }
            foreach ($roles_to_delete as $id) {
                $role = Sentinel::findRoleById($id);
                $role->users()->detach($user);
            }
            /** end add roles*/


            if ($messages->isEmpty()) {
                $aaa = $this->userRepository->update($user, $input);
            }
            //return response()->json($aaa);
        } else {
            $messages = $this->validateUser($input, $rules);
            if ($messages->isEmpty()) {
                $user = $this->userRepository->create($input);
                $code = Activation::create($user);
                Activation::complete($user, $code);
            }
        }
        if ($messages->isEmpty()) {
            Flash::success('Данные пользователя успешно сохранены.');
            return Redirect::to('admin/users');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    protected function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    /**
     * Validates a user.
     *
     * @param  array $data
     * @param  mixed $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        $validator->passes();
        return $validator->errors();
    }

}
