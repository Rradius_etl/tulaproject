<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSocialStatusRequest;
use App\Http\Requests\Admin\UpdateSocialStatusRequest;
use App\Repositories\Admin\SocialStatusRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SocialStatusController extends InfyOmBaseController
{
    /** @var  SocialStatusRepository */
    private $socialStatusRepository;

    public function __construct(SocialStatusRepository $socialStatusRepo)
    {
        $this->socialStatusRepository = $socialStatusRepo;
    }

    /**
     * Display a listing of the SocialStatus.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->socialStatusRepository->pushCriteria(new RequestCriteria($request));
        $socialStatuses = $this->socialStatusRepository->all();

        return view('admin.social_statuses.index')
            ->with('socialStatuses', $socialStatuses);
    }

    /**
     * Show the form for creating a new SocialStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.social_statuses.create');
    }

    /**
     * Store a newly created SocialStatus in storage.
     *
     * @param CreateSocialStatusRequest $request
     *
     * @return Response
     */
    public function store(CreateSocialStatusRequest $request)
    {
        $input = $request->all();

        $socialStatus = $this->socialStatusRepository->create($input);

        Flash::success(trans('backend.SocialStatus') . trans('backend.success_saved'));

        return redirect(route('admin.social-statuses.index'));
    }

    /**
     * Display the specified SocialStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $socialStatus = $this->socialStatusRepository->findWithoutFail($id);

        if (empty($socialStatus)) {
            Flash::error(trans('backend.SocialStatus') . trans('backend.not_found') );

            return redirect(route('admin.social-statuses.index'));
        }

        return view('admin.social_statuses.show')->with('socialStatus', $socialStatus);
    }

    /**
     * Show the form for editing the specified SocialStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $socialStatus = $this->socialStatusRepository->findWithoutFail($id);

        if (empty($socialStatus)) {
             Flash::error(trans('backend.SocialStatus') . trans('backend.not_found') );

            return redirect(route('admin.social-statuses.index'));
        }

        return view('admin.social_statuses.edit')->with('socialStatus', $socialStatus);
    }

    /**
     * Update the specified SocialStatus in storage.
     *
     * @param  int              $id
     * @param UpdateSocialStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSocialStatusRequest $request)
    {
        $socialStatus = $this->socialStatusRepository->findWithoutFail($id);

        if (empty($socialStatus)) {
             Flash::error(trans('backend.SocialStatus') . trans('backend.not_found') );

            return redirect(route('admin.social-statuses.index'));
        }

        $socialStatus = $this->socialStatusRepository->update($request->all(), $id);

       Flash::success(trans('backend.SocialStatus') . trans('backend.success_updated'));

        return redirect(route('admin.social-statuses.index'));
    }

    /**
     * Remove the specified SocialStatus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $socialStatus = $this->socialStatusRepository->findWithoutFail($id);

        if (empty($socialStatus)) {
            Flash::error(trans('backend.SocialStatus') . trans('backend.not_found') );

            return redirect(route('admin.social-statuses.index'));
        }

        $this->socialStatusRepository->delete($id);

        Flash::success(trans('backend.SocialStatus') . trans('backend.success_deleted'));

        return redirect(route('admin.social-statuses.index'));
    }
}
