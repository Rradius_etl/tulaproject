<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSlideRequest;
use App\Http\Requests\Admin\UpdateSlideRequest;
use App\Models\Admin\Slide;
use App\Repositories\Admin\SlideRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use File;
class SlideController extends InfyOmBaseController
{
    /** @var  SlideRepository */
    private $slideRepository;

    public function __construct(SlideRepository $slideRepo)
    {
        $this->slideRepository = $slideRepo;
    }

    /**
     * Display a listing of the Slide.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->slideRepository->pushCriteria(new RequestCriteria($request));
        $slides = $this->slideRepository->paginate(20);

        return view('admin.slides.index')
            ->with('slides', $slides);
    }

    /**
     * Show the form for creating a new Slide.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.slides.create');
    }

    /**
     * Store a newly created Slide in storage.
     *
     * @param CreateSlideRequest $request
     *
     * @return Response
     */
    public function store(CreateSlideRequest $request)
    {
        $input = $request->all();

        $slide =  new Slide($request->all());
        /** Сохранение изображения **/
        try {
            $locationDirectory = "files/slideshow/";
            //return response()->json();
            $mainImage = $request->file('img_path');
            if ($mainImage != null) {
                $extension = $mainImage->getClientOriginalExtension();
                $filename = $mainImage->getClientOriginalName();
                $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                    $slide->img_path = $mainImageFileName;
                }
            }
        } catch (\Exception $e) {

            Storage::disk('public')->delete($slide->img_path);

            return \Redirect::back()->withErrors($e->getMessage())->withInput();

        }
        $slide->save();
         

        Flash::success(trans('backend.Slide') . trans('backend.success_saved'));

        return redirect(route('admin.slides.index'));
    }

    /**
     * Display the specified Slide.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $slide = $this->slideRepository->findWithoutFail($id);

        if (empty($slide)) {
            Flash::error(trans('backend.Slide') . trans('backend.not_found') );

            return redirect(route('admin.slides.index'));
        }

        return view('admin.slides.show')->with('slide', $slide);
    }

    /**
     * Show the form for editing the specified Slide.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $slide = $this->slideRepository->findWithoutFail($id);

        if (empty($slide)) {
             Flash::error(trans('backend.Slide') . trans('backend.not_found') );

            return redirect(route('admin.slides.index'));
        }

        return view('admin.slides.edit')->with('slide', $slide);
    }

    /**
     * Update the specified Slide in storage.
     *
     * @param  int              $id
     * @param UpdateSlideRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlideRequest $request)
    {
        $slide = $this->slideRepository->findWithoutFail($id);

        if (empty($slide)) {
             Flash::error(trans('backend.Slide') . trans('backend.not_found') );

            return redirect(route('admin.slides.index'));
        }

        $slide = $this->slideRepository->update($request->all(), $id);


        $mainImage = $request->file('img_path');

        if ($mainImage != null) {  // If new img_path Image uploaded

            try {
                /** Сохранение изображения **/
                $locationDirectory = "files/slideshow/";
                //return response()->json();
                $mainImage = $request->file('img_path');
                if ($mainImage != null) {
                    $extension = $mainImage->getClientOriginalExtension();
                    $filename = $mainImage->getClientOriginalName();
                    $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                    $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                    if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                        Storage::disk('public')->delete($slide->img_path);
                        $slide->img_path = $mainImageFileName;
                    }
                }
            } catch (\Exception $e) {



                return \Redirect::back()->withErrors($e->getMessage())->withInput();

            }

        }

        $slide->save();
        
       Flash::success(trans('backend.Slide') . trans('backend.success_updated'));

        return redirect(route('admin.slides.index'));
    }

    /**
     * Remove the specified Slide from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $slide = $this->slideRepository->findWithoutFail($id);

        if (empty($slide)) {
            Flash::error(trans('backend.Slide') . trans('backend.not_found') );

            return redirect(route('admin.slides.index'));
        }
        /** Удаление изображения **/
        Storage::disk('public')->delete($slide->img_path);
        $this->slideRepository->delete($id);

        
        
        Flash::success(trans('backend.Slide') . trans('backend.success_deleted'));

        return redirect(route('admin.slides.index'));
    }
}
