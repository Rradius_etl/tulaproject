<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class RoutesController extends Controller
{
    /** Список маршрутов **/
    public function index(Request $request)
    {
        $perPage = 20;

        $routeCollection = \Route::getRoutes();
        $routes = [];


        foreach ($routeCollection as $key =>  $value) {
            $routes[$key]['name'] =  $value->getName();
            $routes[$key]['path'] =  $value->getPath();
        }
        //Create a new Laravel collection from the array data
        $routes = collect($routes);

        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        if( $currentPage > 0 )
            $routes = $routes->slice(($currentPage - 1), $perPage);




        //Create our paginator and pass it to the view
        $paginatedSearchResults = new LengthAwarePaginator($routes,  count($routeCollection), $perPage);

        $paginatedSearchResults->setPath('/admin/routes');

        //return response()->json($routes);
        return view('admin.routes.index', [
            'routes' => $paginatedSearchResults
        ]);
    }
}
