<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateMedicalProfileCategoryRequest;
use App\Http\Requests\Admin\UpdateMedicalProfileCategoryRequest;
use App\Repositories\Admin\MedicalProfileCategoryRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MedicalProfileCategoryController extends InfyOmBaseController
{
    /** @var  MedicalProfileCategoryRepository */
    private $medicalProfileCategoryRepository;

    public function __construct(MedicalProfileCategoryRepository $medicalProfileCategoryRepo)
    {
        $this->medicalProfileCategoryRepository = $medicalProfileCategoryRepo;
    }

    /**
     * Display a listing of the MedicalProfileCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->medicalProfileCategoryRepository->pushCriteria(new RequestCriteria($request));
        $medicalProfileCategories = $this->medicalProfileCategoryRepository->all();

        return view('admin.medical_profile_categories.index')
            ->with('medicalProfileCategories', $medicalProfileCategories);
    }

    /**
     * Show the form for creating a new MedicalProfileCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.medical_profile_categories.create');
    }

    /**
     * Store a newly created MedicalProfileCategory in storage.
     *
     * @param CreateMedicalProfileCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalProfileCategoryRequest $request)
    {
        $input = $request->all();

        $medicalProfileCategory = $this->medicalProfileCategoryRepository->create($input);

        Flash::success(trans('backend.MedicalProfileCategory') . trans('backend.success_saved_f'));

        return redirect(route('admin.medical-profile-categories.index'));
    }

    /**
     * Display the specified MedicalProfileCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medicalProfileCategory = $this->medicalProfileCategoryRepository->findWithoutFail($id);

        if (empty($medicalProfileCategory)) {
            Flash::error(trans('backend.MedicalProfileCategory') . trans('backend.not_found_F') );

            return redirect(route('admin.medical-profile-categories.index'));
        }

        return view('admin.medical_profile_categories.show')->with('medicalProfileCategory', $medicalProfileCategory);
    }

    /**
     * Show the form for editing the specified MedicalProfileCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medicalProfileCategory = $this->medicalProfileCategoryRepository->findWithoutFail($id);

        if (empty($medicalProfileCategory)) {
             Flash::error(trans('backend.MedicalProfileCategory') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profile-categories.index'));
        }

        return view('admin.medical_profile_categories.edit')->with('medicalProfileCategory', $medicalProfileCategory);
    }

    /**
     * Update the specified MedicalProfileCategory in storage.
     *
     * @param  int              $id
     * @param UpdateMedicalProfileCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalProfileCategoryRequest $request)
    {
        $medicalProfileCategory = $this->medicalProfileCategoryRepository->findWithoutFail($id);

        if (empty($medicalProfileCategory)) {
             Flash::error(trans('backend.MedicalProfileCategory') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profile-categories.index'));
        }

        $medicalProfileCategory = $this->medicalProfileCategoryRepository->update($request->all(), $id);

       Flash::success(trans('backend.MedicalProfileCategory') . trans('backend.success_updated_f'));

        return redirect(route('admin.medical-profile-categories.index'));
    }

    /**
     * Remove the specified MedicalProfileCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medicalProfileCategory = $this->medicalProfileCategoryRepository->findWithoutFail($id);

        if (empty($medicalProfileCategory)) {
            Flash::error(trans('backend.MedicalProfileCategory') . trans('backend.not_found_f') );

            return redirect(route('admin.medical-profile-categories.index'));
        }

        $this->medicalProfileCategoryRepository->delete($id);

        Flash::success(trans('backend.MedicalProfileCategory') . trans('backend.success_deleted_f'));

        return redirect(route('admin.medical-profile-categories.index'));
    }
}
