<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateRoleRequest;
use App\Http\Requests\Admin\UpdateRoleRequest;
use App\Repositories\Admin\RoleRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Response;
use Cartalyst\Sentinel\Roles\IlluminateRoleRepository;
class RoleController extends InfyOmBaseController
{
    /** @var  RoleRepository */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepo)
    {
        $this->roleRepository = Sentinel::getRoleRepository();
    }

    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
        $roles = $this->roleRepository->paginate(15);

        return view('admin.roles.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
         $input = $request->all();
        $json= [];
        if(isset($input['permissionsKey']))
            foreach($input['permissionsKey'] as $k =>$v)
            {
                $json[$v]=(boolean)$input['permissionsValue'][$k];
            }
        $input['permissions']=($json);
        $this->roleRepository->createModel()->create($input);

        Flash::success(trans('backend.Role') . trans('backend.success_saved_F'));

        return redirect(route('admin.roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            Flash::error(trans('backend.Role') . trans('backend.not_found_f') );

            return redirect(route('admin.roles.index'));
        }

        return view('admin.roles.show')->with('role', $role);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->roleRepository->findById($id);

        if (empty($role)) {
             Flash::error(trans('backend.Role') . trans('backend.not_found_f') );

            return redirect(route('admin.roles.index'));
        }

        return view('admin.roles.edit')->with('role', $role);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  int              $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
       $role = $this->roleRepository->findById($id);

        if (empty($role)) {
             Flash::error(trans('backend.Role') . trans('backend.not_found_f') );

            return redirect(route('admin.roles.index'));
        }


        $input = $request->all();
        $json= [];

        if(isset($input['permissionsKey']))
        foreach($input['permissionsKey'] as $k =>$v)
        {
            $json[$v]=(boolean)$input['permissionsValue'][$k];
        }

       $input['permissions'] = $json;


        $role->update($input);        

       Flash::success(trans('backend.Role') . trans('backend.success_updated_f'));

        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $role = $this->roleRepository->findById($id);

        if (empty($role)) {
            Flash::error(trans('backend.Role') . trans('backend.not_found_f') );

            return redirect(route('admin.roles.index'));
        }

        $cantDeleteRoles = ['admin','user','mcf_admin','coordinator', 'tmc_admin'];

        if( in_array($role->slug, $cantDeleteRoles) ) {

            Flash::error('Не возможно удалить этот роль. Потому что эта роль необходима для правильной работы портала');
        } else {
            $role->delete();

            Flash::success(trans('backend.Role') . trans('backend.success_deleted_f'));
        }


        return redirect(route('admin.roles.index'));
    }
}
