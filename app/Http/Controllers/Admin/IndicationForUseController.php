<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateIndicationForUseRequest;
use App\Http\Requests\Admin\UpdateIndicationForUseRequest;
use App\Repositories\Admin\IndicationForUseRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IndicationForUseController extends InfyOmBaseController
{
    /** @var  IndicationForUseRepository */
    private $indicationForUseRepository;

    public function __construct(IndicationForUseRepository $indicationForUseRepo)
    {
        $this->indicationForUseRepository = $indicationForUseRepo;
    }

    /**
     * Display a listing of the IndicationForUse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->indicationForUseRepository->pushCriteria(new RequestCriteria($request));
        $indicationForUses = $this->indicationForUseRepository->all();

        return view('admin.indication_for_uses.index')
            ->with('indicationForUses', $indicationForUses);
    }

    /**
     * Show the form for creating a new IndicationForUse.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.indication_for_uses.create');
    }

    /**
     * Store a newly created IndicationForUse in storage.
     *
     * @param CreateIndicationForUseRequest $request
     *
     * @return Response
     */
    public function store(CreateIndicationForUseRequest $request)
    {
        $input = $request->all();

        $indicationForUse = $this->indicationForUseRepository->create($input);

        Flash::success( trans('backend.success_saved'));

        return redirect(route('admin.indication-for-uses.index'));
    }

    /**
     * Display the specified IndicationForUse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $indicationForUse = $this->indicationForUseRepository->findWithoutFail($id);

        if (empty($indicationForUse)) {
            Flash::error( trans('backend.not_found') );

            return redirect(route('admin.indication-for-uses.index'));
        }

        return view('admin.indication_for_uses.show')->with('indicationForUse', $indicationForUse);
    }

    /**
     * Show the form for editing the specified IndicationForUse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $indicationForUse = $this->indicationForUseRepository->findWithoutFail($id);

        if (empty($indicationForUse)) {
             Flash::error( trans('backend.not_found') );

            return redirect(route('admin.indication-for-uses.index'));
        }

        return view('admin.indication_for_uses.edit')->with('indicationForUse', $indicationForUse);
    }

    /**
     * Update the specified IndicationForUse in storage.
     *
     * @param  int              $id
     * @param UpdateIndicationForUseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndicationForUseRequest $request)
    {
        $indicationForUse = $this->indicationForUseRepository->findWithoutFail($id);

        if (empty($indicationForUse)) {
             Flash::error( trans('backend.not_found') );

            return redirect(route('admin.indication-for-uses.index'));
        }

        $indicationForUse = $this->indicationForUseRepository->update($request->all(), $id);

       Flash::success( trans('backend.success_updated'));

        return redirect(route('admin.indication-for-uses.index'));
    }

    /**
     * Remove the specified IndicationForUse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $indicationForUse = $this->indicationForUseRepository->findWithoutFail($id);

        if (empty($indicationForUse)) {
            Flash::error( trans('backend.not_found') );

            return redirect(route('admin.indication-for-uses.index'));
        }

        $this->indicationForUseRepository->delete($id);

        Flash::success( trans('backend.success_deleted'));

        return redirect(route('admin.indication-for-uses.index'));
    }
}
