<?php

namespace App\Http\Controllers\Admin;

use App\Models\Stats\TrackerLog;
use App\Models\Stats\TrackerPath;
use App\Models\Stats\TrackerSession;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use PhpOffice\PhpWord\PhpWord;
use PragmaRX\Tracker\Support\Minutes;
use CpChart\Factory\Factory as pCharts; 

class AnalyticsController extends Controller
{
    protected static $format = 'Y-m-d';
    public static $onlineUsersHeader = [
        'email' => 'Пользователь',
        'updated_at' => 'Последняя активность'
    ];


    public static $pageViewsHeader = [
        'client_ip' => 'Ip адрес',
        'path' => 'Путь',
        'hits' => 'Просмотров',
        'updated_at'  =>  'Последняя активность',
    ];

    /** calculate range*/
    protected function getDateRange($request, $asString = false)
    {
        $format = self::$format;
        $defaults = [
            'from' => Carbon::now()->startOfDay()->format($format),
            'to' => Carbon::now()->subWeek()->endOfDay()->format($format)
        ];
        $from = Carbon::createFromFormat($format, $request->get('from', $defaults['from']))->startOfDay();
        $to = Carbon::createFromFormat($format, $request->get('to', $defaults['to']))->endOfDay();
        if ($asString) {
            return [$from->format($format), $to->format($format)];
        } else {
            return [$from, $to];
        }

    }

    /** главная страница страницы аналитики **/
    public function index(Request $request)
    {
        return view('admin.analytics.index', [
            'title' => 'Посещаемость портала'
        ]);
    }

    /** График посещаемости */
    public function charts(Request $request)
    {
        $range = self::getDateRange($request, true);
        $carbonRange = self::getDateRange($request);
        $headers = self::$pageViewsHeader;

        $range1 = $carbonRange[0];
        $range2 = $carbonRange[1];

        $format = $request->get('download', 'url');
        $diagram_type = ($request->get('diagram') == '' || $request->get('diagram') == null )  ? 'linechart' : $request->get('diagram') ;

        //return TrackerLog::fromTo($range)->selectRaw("DATE(created_at) as date, count(*) as total")->orderBy('date')->groupBy(\DB::raw('DATE(created_at)'))->get();
        $dateQuery = "1";
        $text = "";
        $difference = $range2->diffInDays($range1);
        /* TODO: Спросить помощи у Тауке */
        if ($difference < 15) {
            $text = 'Количество посещении портала по дням промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата (день-месяц-год)";

            $dateQuery = "DATE_FORMAT(created_at,'%d-%m-%Y')";
        } else if ($difference < 110) {
            $text = 'Количество посещении портала  по неделям промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата(Год и Номер недели года)";

            $dateQuery = "DATE_FORMAT(created_at,'%Y-%u')";
        } else if ($difference < 365.25 * 2) {
            $text = 'Количество посещении портала  по месяцам промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата (Месяц и год)";

            $dateQuery = "DATE_FORMAT(created_at,'%m-%Y')";
        }  else if($difference < 365.25*16 &&  $difference >=  365.25 * 2 ) {
            $text = 'Количество посещении портала  по годам промежутка времени от ' . $range1->format('d-m-Y') . " до " . $range2->format('d-m-Y');
            $abscissaTitle = "Дата (годы)";

            $dateQuery = "DATE_FORMAT(created_at,'%Y')";
        }

        $reports = collect(TrackerLog::fromTo($range)->selectRaw($dateQuery . " as date, count(*) as total")->orderBy('date')->groupBy(\DB::raw($dateQuery))->get());

        if($diagram_type == 'linechart') {
            return self::generateLineChart($reports, $format, $text, 'Количество посещений', $abscissaTitle);
        } else if($diagram_type == 'barchart' ) {
            return self::generateBarChart($reports, $format, $text, 'Количество посещений', $abscissaTitle);
        }


        //return  response()->json($reports);
    }

    /** Пользователи Онлайн */
    public function onlineUsers(Request $request, $type = 'table')
    {
        $fileFormat = $request->get('format', 'with-layout');
        $range = self::getDateRange($request);

        $headers = self::$onlineUsersHeader;


        if( $type == 'table') {
            if ($fileFormat == 'with-layout') {
                $title = 'Сейчас на сайте';
                $url = '/admin/analytics/online-users';
                return view('admin.analytics.view', compact('headers', 'title', 'url'));
            }
        }





        $reports = TrackerSession::period(Minutes::make(90), 'tracker_sessions')
            ->orderBy('tracker_sessions.updated_at', 'desc')
            ->join('users', 'users.id', '=', 'tracker_sessions.user_id')
            ->join('tracker_log', 'tracker_log.session_id', '=', 'tracker_sessions.id')
            ->join('tracker_route_paths', 'tracker_log.route_path_id', '=', 'tracker_route_paths.id')
            ->join('tracker_routes', 'tracker_routes.id', '=', 'tracker_route_paths.route_id')
            ->where('tracker_log.method', 'GET')
            ->selectRaw('users.email as email, tracker_sessions.updated_at as updated_at, tracker_routes.name')
            ->get();
        //->groupBy('name');


        if( $type == 'graphic' ) {
            $reports = $reports->groupBy('name');

        }


        //return response()->json($reports);
        $view = "admin.analytics.partial.online-users";

        $fileName = $fileName = 'Активные пользователи - ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();

        return self::DownloadAsFile($reports, $fileFormat, $view, $headers, $fileName);

    }

    /** Количество визитов */
    public function pageViews(Request $request)
    {
        $fileFormat = $request->get('format', 'with-layout');
        $range = self::getDateRange($request);

        $headers = self::$pageViewsHeader;

        if ($fileFormat == 'with-layout') {
            $title = 'Количество визитов по страницам';
            $url = '/admin/analytics/pages';
            return view('admin.analytics.view', compact('headers', 'title', 'url'));
        }

        $reports = TrackerPath::select(['tracker_paths.id', 'tracker_paths.path'])
            ->join('tracker_log as tl', function ($join) use ($range) {
                $join->on('tracker_paths.id', '=', 'tl.path_id')
                    ->where('tl.updated_at', '>', $range[0])
                    ->where('tl.updated_at', '<', $range[1]);
            })
            ->selectRaw('count(tl.id) as hits')
            ->groupBy('tracker_paths.id')
            ->join('tracker_sessions as ts', function ($join) {
                $join->on('tl.session_id', '=', 'ts.id');

            })
            ->selectRaw('ts.client_ip as client_ip, ts.updated_at as updated_at')
            ->orderBy('hits', 'desc')
            ->get();

        $reports = $reports->map(function ($item, $key) {
            $item->path =  \URL::to($item->path) ;
            return $item;
        });
        //return response()->json($reports);
        $view = "admin.analytics.partial.pages";

        $fileName = $fileName = 'Количество визитов - ' . $range[0]->toDateString() . ' - ' . $range[1]->toDateString();

        return self::DownloadAsFile($reports, $fileFormat, $view, $headers, $fileName);

    }
    /** Download as docx file */
    protected static function DownloadAsDocx($reports, $headers, $title, $fileFormat){
        $phpWord = new PhpWord();

        // Set Font Style
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(false);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(10);

        $section = $phpWord->addSection([
            'orientation' => 'landscape'
        ]);

        $header = array('size' => 16, 'bold' => true);
        $styleTable = ['borderSize' => 1, 'borderColor' => '000000', 'width' => 100 * 100,  'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT];
        $phpWord->addTableStyle('Table1', $styleTable);


        $section->addText($title, $header);
        $table = $section->addTable("Table1");

        $table->addRow();
        $table->addCell()->addText('№ по порядку');
        foreach ($headers as $header) {

            $table->addCell()->addText($header);
        }

        foreach ($reports as $idx => $report) {
            $table->addRow();
            // № по порядку
            $table->addCell()->addText($idx + 1);
            foreach ($headers as $key => $value) {
                $table->addCell(100 * 25)->addText($report->$key);
            }


        }
        $fileName = 'Количество визитов по страницам';


        // Saving the document as file...

        if($fileFormat == 'odt') {
            $file = $phpWord->save($fileName . '.odt', 'ODText', true);
        } else {
            $file = $phpWord->save($fileName . '.docx', 'Word2007', true);
        }

        return $file;
    }

    public function DownloadAsFile($data, $format, $view, $headers, $fileName = 'newfile', $tmc = null, $mcf = null)
    {
        //return $mcf;


        if ($format == 'ajax') {
            //return 'dasdas';
            return view($view)->with('headers', $headers)
                ->with('reports', $data)
                ->with('full', true)
                ->with('format', $format);


        } elseif ($format == 'xls') {

            $fileName = $fileName . '.' . $format;


            //return var_dump($reports);
            return \Excel::create($fileName, function ($excel) use ($data, $view, $tmc, $mcf, $headers, $format) {

                $excel->sheet('Страница 1', function ($sheet) use ($data, $view, $tmc, $mcf, $headers, $format) {
                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'DejaVu Sans',
                            'size' => 8,
                            'bold' => false
                        )
                    ));
                    $sheet->loadView($view)->with('headers', $headers)
                        ->with('reports', $data)
                        ->with('format', $format)
                        ->with('full', true);

                });

            })->export($format);

        } elseif ($format == 'pdf') {
            $html = view($view)->with('headers', $headers)
                ->with('reports', $data)
                ->with('format', $format)
                ->with('full', true);

            $pdf = \PDF::loadHTML($html);

            $pdf->setOption('encoding', 'utf-8');
            $pdf->setOrientation('landscape');

            return $pdf->download($fileName . '.pdf');

        } elseif ($format == 'docx' || $format == 'odt' ) {
           self::DownloadAsDocx($data, $headers, $fileName, $format);
        }
    }

    /** Нарисовать диаграмму **/
    private static function generateBarChart($reports, $format = 'jpeg', $reportTitle = 'Отчет', $ordinateTitle = 'Количество', $abscissaTitle = 'Абсцисса')
    {

        $imgWidth = 1280;
        $imgHeight = 800;

        $factory = new pCharts();

        // Create and populate data
        $myData = $factory->newData();

        /* Если нет данных вернуть изображение с сообщением */
        if (count($reports) == 0) {
            return \Image::make('images/charts-error.png')->response();
        } else {
            // Create the image
            $myPicture = $factory->newImage($imgWidth, $imgHeight, $myData);

            /* Set the default font */
            $myPicture->setFontProperties(array("FontName" => public_path("fonts/arial/arial.ttf"), "FontSize" => 12));
        }

        //return response()->json([$consultations, $seminars, $meetings]);


        foreach ($reports as $report) {
            $views[] = $report['total'];
            $datesArray[] = $report['date'];
        }

        $myData->addPoints($views, $ordinateTitle);

        $myData->setAxisName(0, $ordinateTitle);
        // Get maximum value of Ordinate;
        $maxValue = max($views);
        $maxValue = ($maxValue < 4) ? 4 : $maxValue;


        //  Add points to  Abscissa
        $myData->addPoints($datesArray, $abscissaTitle);

        $myData->setSerieDescription($abscissaTitle, $abscissaTitle);
        $myData->setAbscissa($abscissaTitle);

        // Set colors of bars
        $myData->setPalette($ordinateTitle, hex2rgb('#33cc33'));


        /* Add a border to the picture */
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));


        /* Write the chart title */
        $myPicture->drawText(50, 30, $reportTitle, array("FontSize" => 14, "Align" => TEXT_ALIGN_BOTTOMLEFT));
        /* Write the Abscissa Title*/
        $myPicture->drawText(50, $imgHeight - 30, $abscissaTitle, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));


        $myPicture->drawRectangle(0, 0, $imgWidth, $imgHeight, array("R" => 0, "G" => 0, "B" => 0));


        /* Define the chart area */
        $myPicture->setGraphArea(60, 60, $imgWidth - 50, $imgHeight - 100);

        /* Draw the scale */
        $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
        $scaleSettings = array("GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => false, "CycleBackground" => TRUE, 'Mode' => SCALE_MODE_MANUAL, 'Factors' => array(1), 'ManualScale' => $scaleConfig);
        $myPicture->drawScale($scaleSettings);

        /* Write the chart legend */
        $myPicture->drawLegend(900, $imgHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

        /* Turn on shadow computing */
        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));


        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
        $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => TRUE);
        /* Draw the chart */
        $myPicture->drawBarChart($settings);

        // Prepare file to download or show
        $fileName = hash('ripemd160', $reportTitle . round(microtime(true) * 1000));
        $imgPath = 'statistics-images/' . $fileName . '.jpeg';

        /* Render the picture (choose the best way) */
        //return $myPicture->stroke();

        if ($format == 'pdf') {
            return \PDF::loadHTML('<img src="' . str_replace('download=pdf','download=url', request()->fullUrl()) . '" >')->setOrientation('landscape')->download($fileName . '.pdf');
        } else if ($format == 'jpeg' || $format == 'url' ) {
            return $myPicture->stroke();
        }

    }


    /**
     * Нарисовать линейный график
     * @return Response
    */

    private static function generateLineChart($reports, $format = 'jpeg', $reportTitle = 'Отчет', $ordinateTitle = 'Количество', $abscissaTitle = 'Абсцисса')
    {

        $imgWidth = 1280;
        $imgHeight = 800;

        $factory = new pCharts();

        // Create and populate data
        $myData = $factory->newData();

        /* Если нет данных вернуть изображение с сообщением */
        if (count($reports) == 0) {
            return \Image::make('images/charts-error.png')->response();

        } else {
            // Create the image
            $myPicture = $factory->newImage($imgWidth, $imgHeight, $myData);

            /* Set the default font */
            $myPicture->setFontProperties(array("FontName" => public_path("fonts/arial/arial.ttf"), "FontSize" => 12));
        }

        //return response()->json([$consultations, $seminars, $meetings]);



        foreach ($reports as $report) {
            $views[] = $report['total'];
            $datesArray[] = $report['date'];
        }

        $myData->addPoints($views, $ordinateTitle);

        $myData->setAxisName(0, $ordinateTitle);
        // Get maximum value of Ordinate;
        $maxValue = max($views);
        $maxValue = ($maxValue < 4) ? 4 : $maxValue;


        //  Add points to  Abscissa
        $myData->addPoints($datesArray, $abscissaTitle);
        $myData->setSerieWeight($abscissaTitle, 2);
        $myData->setSerieDescription($abscissaTitle, $abscissaTitle);
        $myData->setAbscissa($abscissaTitle);

        // Set colors of bars
        $myData->setPalette($ordinateTitle, hex2rgb('#edab8f'));


        /* Add a border to the picture */
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_VERTICAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 100));
        $myPicture->drawGradientArea(0, 0, $imgWidth, $imgHeight, DIRECTION_HORIZONTAL, array("StartR" => 240, "StartG" => 240, "StartB" => 240, "EndR" => 180, "EndG" => 180, "EndB" => 180, "Alpha" => 20));



        /* Write the chart title */
        $myPicture->drawText(50, 30, $reportTitle, array("FontSize" => 14, "Align" => TEXT_ALIGN_BOTTOMLEFT));
        /* Write the Abscissa Title*/
        $myPicture->drawText(50, $imgHeight - 30, $abscissaTitle, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));


        $myPicture->drawRectangle(0, 0, $imgWidth, $imgHeight, array("R" => 0, "G" => 0, "B" => 0));


        /* Define the chart area */
        $myPicture->setGraphArea(60, 60, $imgWidth - 50, $imgHeight - 100);

        /* Draw the scale */
        $scaleConfig = array(0 => array("Min" => 0, "Max" => $maxValue));
        $scaleSettings = array("GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => false, "CycleBackground" => TRUE, 'Mode' => SCALE_MODE_MANUAL, 'Factors' => array(1), 'ManualScale' => $scaleConfig);
        $myPicture->drawScale($scaleSettings);

        /* Write the chart legend */
        $myPicture->drawLegend(900, $imgHeight - 30, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL, "BoxWidth" => 14, "BoxHeight" => 14, "IconAreaWidth" => 20, "IconAreaHeight" => 5));

        /* Turn on shadow computing */
        $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));


        $settings = array("Surrounding" => -3, "InnerSurrounding" => 3, "DisplayValues" => false, "DisplayColor"=>DISPLAY_AUTO);
        /* Draw the chart */
        $myPicture->drawLineChart($settings);
        $myPicture->drawPlotChart( ["PlotSize"=>4,"PlotBorder"=>false,"BorderSize"=>1, "DisplayValues" => TRUE, "DisplayColor" => false ]);
        // Prepare file to download or show
        $fileName = $reportTitle;

        /* Render the picture (choose the best way) */
        if ($format == 'pdf') {
            return \PDF::loadHTML('<img src="' . str_replace('download=pdf','download=url', request()->fullUrl()) . '" >')->setOrientation('landscape')->download($fileName . '.pdf');
        } else if ($format == 'jpeg' || $format == 'url' ) {
            return $myPicture->stroke();
        }
    }


}
