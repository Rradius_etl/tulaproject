<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateFileRequest;
use App\Http\Requests\Admin\UpdateFileRequest;
use App\Models\FileUpload;
use App\Repositories\Admin\FileRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FileController extends InfyOmBaseController
{
    /** @var  FileRepository */
    private $fileRepository;

    public function __construct(FileRepository $fileRepo)
    {
        $this->fileRepository = $fileRepo;
    }

    /**
     * Display a listing of the File.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if(empty($request->get('orderBy'))) {
            $request['orderBy'] = 'created_at';
            $request['sortedBy'] = 'desc';
        }
        $this->fileRepository->pushCriteria(new RequestCriteria($request));
        $files = $this->fileRepository->paginate(30);


        return view('admin.files.index')
            ->with('files', $files);
    }

    /**
     * Show the form for creating a new File.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.files.create');
    }

    /**
     * Store a newly created File in storage.
     *
     * @param CreateFileRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
         
        
        $saved = FileUpload::upload($request, \Sentinel::getUser()->id, "files/");

        if($saved) {
            Flash::success(trans('backend.File') . trans('backend.success_saved'));
        } else {
            Flash::error(trans('backend.File') .  'не записан. Возникла непредвиденная ошибка');
        }
        

        return redirect(route('admin.files.index'));
    }

    /**
     * Display the specified File.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $file = $this->fileRepository->findWithoutFail($id);

        if (empty($file)) {
            Flash::error(trans('backend.File') . trans('backend.not_found') );

            return redirect(route('admin.files.index'));
        }

        return view('admin.files.show')->with('file', $file);
    }

    /**
     * Show the form for editing the specified File.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $file = $this->fileRepository->findWithoutFail($id);

        if (empty($file)) {
             Flash::error(trans('backend.File') . trans('backend.not_found') );

            return redirect(route('admin.files.index'));
        }

        return view('admin.files.edit')->with('file', $file);
    }

    /**
     * Update the specified File in storage.
     *
     * @param  int              $id
     * @param UpdateFileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFileRequest $request)
    {
        $file = $this->fileRepository->findWithoutFail($id);

        if (empty($file)) {
             Flash::error(trans('backend.File') . trans('backend.not_found') );

            return redirect(route('admin.files.index'));
        }

        $file = $this->fileRepository->update($request->all(), $id);

       Flash::success(trans('backend.File') . trans('backend.success_updated'));

        return redirect(route('admin.files.index'));
    }

    /**
     * Remove the specified File from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $file = $this->fileRepository->findWithoutFail($id);

        if (empty($file)) {
            Flash::error(trans('backend.File') . trans('backend.not_found') );

            return redirect(route('admin.files.index'));
        }

        $this->fileRepository->delete($id);

        Flash::success(trans('backend.File') . trans('backend.success_deleted'));

        return redirect()->back();
    }
}
