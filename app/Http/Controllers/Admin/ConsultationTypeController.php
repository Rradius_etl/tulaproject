<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateConsultationTypeRequest;
use App\Http\Requests\Admin\UpdateConsultationTypeRequest;
use App\Repositories\Admin\ConsultationTypeRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ConsultationTypeController extends InfyOmBaseController
{
    /** @var  ConsultationTypeRepository */
    private $consultationTypeRepository;

    public function __construct(ConsultationTypeRepository $consultationTypeRepo)
    {
        $this->consultationTypeRepository = $consultationTypeRepo;
    }

    /**
     * Display a listing of the ConsultationType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->consultationTypeRepository->pushCriteria(new RequestCriteria($request));
        $consultationTypes = $this->consultationTypeRepository->all();

        return view('admin.consultation_types.index')
            ->with('consultationTypes', $consultationTypes);
    }

    /**
     * Show the form for creating a new ConsultationType.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.consultation_types.create');
    }

    /**
     * Store a newly created ConsultationType in storage.
     *
     * @param CreateConsultationTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateConsultationTypeRequest $request)
    {
        $input = $request->all();

        $consultationType = $this->consultationTypeRepository->create($input);

        Flash::success(trans('backend.ConsultationType') . trans('backend.success_saved'));

        return redirect(route('admin.consultation-types.index'));
    }

    /**
     * Display the specified ConsultationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $consultationType = $this->consultationTypeRepository->findWithoutFail($id);

        if (empty($consultationType)) {
            Flash::error(trans('backend.ConsultationType') . trans('backend.not_found') );

            return redirect(route('admin.consultation-types.index'));
        }

        return view('admin.consultation_types.show')->with('consultationType', $consultationType);
    }

    /**
     * Show the form for editing the specified ConsultationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $consultationType = $this->consultationTypeRepository->findWithoutFail($id);

        if (empty($consultationType)) {
             Flash::error(trans('backend.ConsultationType') . trans('backend.not_found') );

            return redirect(route('admin.consultation-types.index'));
        }

        return view('admin.consultation_types.edit')->with('consultationType', $consultationType);
    }

    /**
     * Update the specified ConsultationType in storage.
     *
     * @param  int              $id
     * @param UpdateConsultationTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConsultationTypeRequest $request)
    {
        $consultationType = $this->consultationTypeRepository->findWithoutFail($id);

        if (empty($consultationType)) {
             Flash::error(trans('backend.ConsultationType') . trans('backend.not_found') );

            return redirect(route('admin.consultation-types.index'));
        }

        $consultationType = $this->consultationTypeRepository->update($request->all(), $id);

       Flash::success(trans('backend.ConsultationType') . trans('backend.success_updated'));

        return redirect(route('admin.consultation-types.index'));
    }

    /**
     * Remove the specified ConsultationType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $consultationType = $this->consultationTypeRepository->findWithoutFail($id);

        if (empty($consultationType)) {
            Flash::error(trans('backend.ConsultationType') . trans('backend.not_found') );

            return redirect(route('admin.consultation-types.index'));
        }

        $this->consultationTypeRepository->delete($id);

        Flash::success(trans('backend.ConsultationType') . trans('backend.success_deleted'));

        return redirect(route('admin.consultation-types.index'));
    }
}
