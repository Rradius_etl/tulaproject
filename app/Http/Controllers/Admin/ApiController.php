<?php

namespace App\Http\Controllers\Admin;



use App\Models\Poll;
use App\Models\TelemedCenter;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;

class ApiController extends Controller
{
    //
    /** Поиск пользователей **/
    public function findUser()
    {
        if(isset($_GET['search']))
        {
            $users = User::where("email","like","%".$_GET['search']."%")->select("email","id")->get();
            return response()->json($users);
        }
    }

    /** Поиск по ТМЦ **/
    public function findTmcs()
    {
        if(isset($_GET['search']))
        {

            $all_tmcs = TelemedCenter::where("name","like","%".$_GET['search']."%")
                ->select('id', 'name')->get();
            return response()->json($all_tmcs);
        }
    }

    /** Поиск по опросам **/
    public function findPolls()
    {
        if(isset($_GET['search'])) {
            $tmcs = \TmcRoleManager::getTmcs();
            $polls = Poll::active()->where("title", "like", "%" . $_GET['search'] . "%")->whereIn('telemed_center_id', array_pluck($tmcs, ['id']))->get();
            return response()->json($polls);
        }
    }
}
