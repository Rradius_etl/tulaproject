<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreatePageRequest;
use App\Http\Requests\Admin\UpdatePageRequest;
use App\Models\Admin\Page;
use App\Repositories\Admin\PageRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use File;
class PageController extends InfyOmBaseController
{
    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pageRepository->pushCriteria(new RequestCriteria($request));
        $pages = $this->pageRepository->with('author', 'lastModifier')->paginate(20);
        
        return view('admin.pages.index')
            ->with('pages', $pages);
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param CreatePageRequest $request
     *
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $input = $request->all();

        $page = new Page($request->all());

        try {

            if( empty($page->slug) ) {
                $page->slug = str_slug($page->title,'-');
            }
            $page->created_by = \Sentinel::check()->getUserId();
            $page->modified_by = \Sentinel::check()->getUserId();
            $locationDirectory = "files/";
            //return response()->json();
            $mainImage = $request->file('img_path');
            if ($mainImage != null) {
                $extension = $mainImage->getClientOriginalExtension();
                $filename = $mainImage->getClientOriginalName();
                $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                $mainImageFileName= $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                    $page->img_path = $mainImageFileName;
                }
            }
        } catch (\Exception $e) {

            Storage::disk('public')->delete($page->img_path);

            return \Redirect::back()->withErrors($e->getMessage())->withInput();

        }
        
        
     $page->save();

        Flash::success(trans('backend.Page') . trans('backend.success_saved_f'));

        return redirect(route('admin.pages.index'));
    }

    /**
     * Display the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            Flash::error(trans('backend.Page') . trans('backend.not_found_f') );

            return redirect(route('admin.pages.index'));
        }

        return view('admin.pages.show')->with('page', $page);
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
             Flash::error(trans('backend.Page') . trans('backend.not_found_f') );

            return redirect(route('admin.pages.index'));
        }

        return view('admin.pages.edit')->with('page', $page);
    }

    /**
     * Update the specified Page in storage.
     *
     * @param  int              $id
     * @param UpdatePageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageRequest $request)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
             Flash::error(trans('backend.Page') . trans('backend.not_found_f') );

            return redirect(route('admin.pages.index'));
        }

        $page = $this->pageRepository->update($request->all(), $id);

        $page->modified_by = \Sentinel::check()->getUserId();
        
        if( empty($page->slug) ) {
            $page->slug = str_slug($page->title,'-');

        }
        $mainImage = $request->file('img_path');

        if ($mainImage != null) {  // If new img_path Image uploaded
            /** Сохранение изображения **/
            try {

                $locationDirectory = "files/";
                //return response()->json();
                $mainImage = $request->file('img_path');
                if ($mainImage != null) {
                    $extension = $mainImage->getClientOriginalExtension();
                    $filename = $mainImage->getClientOriginalName();
                    $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                    $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                    if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                        Storage::disk('public')->delete($page->img_path);
                        $page->img_path = $mainImageFileName;
                    }
                }
            } catch (\Exception $e) {

            

                return \Redirect::back()->withErrors($e->getMessage())->withInput();

            }

        }

        $page->save();
       Flash::success(trans('backend.Page') . trans('backend.success_updated_f'));

        return redirect(route('admin.pages.index'));
    }

    /**
     * Remove the specified Page from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            Flash::error(trans('backend.Page') . trans('backend.not_found_f') );

            return redirect(route('admin.pages.index'));
        }
        /** Удаление изображения **/
        Storage::disk('public')->delete($page->img_path);
        $this->pageRepository->delete($id);

        Flash::success(trans('backend.Page') . trans('backend.success_deleted_f'));

        return redirect(route('admin.pages.index'));
    }
}
