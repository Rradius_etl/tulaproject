<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\TelemedUserRoles;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedUserPivotRequest;
use App\Http\Requests\Admin\UpdateTelemedUserPivotRequest;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedUserPivot;
use App\Models\User;
use App\Repositories\Admin\TelemedUserPivotRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Cache;
/** Контроллер для списка пользоваталей и их редактирования ТМЦ **/
class TelemedUserPivotController extends InfyOmBaseController
{
    /** @var  TelemedUserPivotRepository */
    private $telemedUserPivotRepository;

    public function __construct(TelemedUserPivotRepository $telemedUserPivotRepo)
    {
        $this->telemedUserPivotRepository = $telemedUserPivotRepo;
        $this->users = User::pluck('email','id');
    }

    /**
     * Display a listing of the TelemedUserPivot.
     *
     * @param Request $request
     * @return Response
     */
    public function index($TelemedId,Request $request)
    {
        /** Фильтр по ТМЦ **/
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);

        $this->telemedUserPivotRepository->pushCriteria(new RequestCriteria($request));
        $telemedUserPivots = $this->telemedUserPivotRepository->all();

        return view('admin.telemed_user_pivots.index',['TelemedId'=>$TelemedId])
            ->with('telemedUserPivots', $telemedUserPivots);
    }

    /**
     * Show the form for creating a new TelemedUserPivot.
     *
     * @return Response
     */
    public function create($TelemedId)
    {
        return view('admin.telemed_user_pivots.create',['TelemedId'=>$TelemedId,'users'=>$this->users]);
    }

    /**
     * Store a newly created TelemedUserPivot in storage.
     *
     * @param CreateTelemedUserPivotRequest $request
     *
     * @return Response
     */
    public function store($TelemedId,CreateTelemedUserPivotRequest $request)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);

        $input = $request->all();
        $this->validate($request, [
            'value' => 'in:admin,coordinator|required',
            'user_id' => 'required',
        ]);
        

        $res = TelemedUserPivot::check($input['user_id'],$TelemedId, $input['value']);

        if(!isset($res['error']))
        {

        }
        else
        {
            Flash::error($res['error']);
            return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        

        if($this->telemedUserPivotRepository->create($input))
        {
            Cache::forget("userroles".$input['user_id']);
            Cache::forget("usertmcs".$input['user_id']);
        }

        Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_saved'));

        return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }

    /**
     * Display the specified TelemedUserPivot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($TelemedId,$id)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
            Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        return view('admin.telemed_user_pivots.show',['TelemedId'=>$TelemedId])->with('telemedUserPivot', $telemedUserPivot);
    }

    /**
     * Show the form for editing the specified TelemedUserPivot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($TelemedId,$id)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
             Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('admin.telemed-user-pivots.index'));
        }

        return view('admin.telemed_user_pivots.edit',['TelemedId'=>$TelemedId,'users'=>$this->users,'telemedUserPivot'=>$telemedUserPivot]);
    }

    /**
     * Update the specified TelemedUserPivot in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedUserPivotRequest $request
     *
     * @return Response
     */
    public function update($TelemedId,$id, UpdateTelemedUserPivotRequest $request)
    {
        $crit = new TelemedUserRoles();
        $crit->setTelemedId($TelemedId);
        $this->crit=$crit;
        $this->telemedUserPivotRepository->pushCriteria($crit);
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
             Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }



        $input = $request->all();
        $this->validate($request, [
            'value' => 'in:admin,coordinator|required',
            'user_id' => 'required',
        ]);

        $res = TelemedUserPivot::check($input['user_id'],$TelemedId, $input['value']);

        if(!isset($res['error']))
        {

        }
        else
        {
            Flash::error($res['error']);
            return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        $prevuser = $telemedUserPivot->user_id;
        /** Очистить кеш ролей и списка тмц для определнных пользователей в случае успешного сохранения этой сущности **/
        if($this->telemedUserPivotRepository->update($request->all(), $id));
        {
            Cache::forget("userroles".$prevuser);
            Cache::forget("userroles".$request->all()['user_id']);
            Cache::forget("usertmcs".$request->all()['user_id']);
            Cache::forget("usertmcs".$prevuser);
        }
       Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_updated'));

        return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }

    /**
     * Remove the specified TelemedUserPivot from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($TelemedId,$id)
    {
        $telemedUserPivot = $this->telemedUserPivotRepository->findWithoutFail($id);

        if (empty($telemedUserPivot)) {
            Flash::error(trans('backend.TelemedUserPivot') . trans('backend.not_found') );

            return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
        }

        $this->telemedUserPivotRepository->delete($id);
        Cache::forget("userroles".$telemedUserPivot->user_id);
        Cache::forget("usertmcs".$telemedUserPivot->user_id);
        Flash::success(trans('backend.TelemedUserPivot') . trans('backend.success_deleted'));

        return redirect(route('admin.telemed-user-pivots.index',['TelemedId'=>$TelemedId]));
    }
}
