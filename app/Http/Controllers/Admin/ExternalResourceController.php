<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateExternalResourceRequest;
use App\Http\Requests\Admin\UpdateExternalResourceRequest;
use App\Models\Admin\ExternalResource;
use App\Repositories\Admin\ExternalResourceRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use File;

class ExternalResourceController extends InfyOmBaseController
{
    /** @var  ExternalResourceRepository */
    private $externalResourceRepository;

    public function __construct(ExternalResourceRepository $externalResourceRepo)
    {
        $this->externalResourceRepository = $externalResourceRepo;
    }

    /**
     * Display a listing of the ExternalResource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->externalResourceRepository->pushCriteria(new RequestCriteria($request));
        $externalResources = $this->externalResourceRepository->paginate(20);

        return view('admin.external_resources.index')
            ->with('externalResources', $externalResources);
    }

    /**
     * Show the form for creating a new ExternalResource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.external_resources.create');
    }

    /**
     * Store a newly created ExternalResource in storage.
     *
     * @param CreateExternalResourceRequest $request
     *
     * @return Response
     */
    public function store(CreateExternalResourceRequest $request)
    {
        $input = $request->all();

        $externalResource =  new ExternalResource($request->all());
        
        try {
 
           
            $locationDirectory = "files/resources/";
            //return response()->json();
            $mainImage = $request->file('logo_path');
            if ($mainImage != null) {
                $extension = $mainImage->getClientOriginalExtension();
                $filename = $mainImage->getClientOriginalName();
                $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                    $externalResource->logo_path = $mainImageFileName;
                }
            }
        } catch (\Exception $e) {

            Storage::disk('public')->delete($externalResource->logo_path);

            return \Redirect::back()->withErrors($e->getMessage())->withInput();

        }

        $externalResource->save();
        
        Flash::success(trans('backend.ExternalResource') . trans('backend.success_saved'));

        return redirect(route('admin.external-resources.index'));
    }

    /**
     * Display the specified ExternalResource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $externalResource = $this->externalResourceRepository->findWithoutFail($id);

        if (empty($externalResource)) {
            Flash::error(trans('backend.ExternalResource') . trans('backend.not_found') );

            return redirect(route('admin.external-resources.index'));
        }

        return view('admin.external_resources.show')->with('externalResource', $externalResource);
    }

    /**
     * Show the form for editing the specified ExternalResource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $externalResource = $this->externalResourceRepository->findWithoutFail($id);

        if (empty($externalResource)) {
             Flash::error(trans('backend.ExternalResource') . trans('backend.not_found') );

            return redirect(route('admin.external-resources.index'));
        }

        return view('admin.external_resources.edit')->with('externalResource', $externalResource);
    }

    /**
     * Update the specified ExternalResource in storage.
     *
     * @param  int              $id
     * @param UpdateExternalResourceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExternalResourceRequest $request)
    {
        $externalResource = $this->externalResourceRepository->findWithoutFail($id);

        if (empty($externalResource)) {
             Flash::error(trans('backend.ExternalResource') . trans('backend.not_found') );

            return redirect(route('admin.external-resources.index'));
        }

        $externalResource = $this->externalResourceRepository->update($request->all(), $id);


        if ($externalResource != null) {  // If new logo_path Image uploaded

            try {

                $locationDirectory = "files/resources/";
                //return response()->json();
                $mainImage = $request->file('logo_path');
                if ($mainImage != null) {
                    $extension = $mainImage->getClientOriginalExtension();
                    $filename = $mainImage->getClientOriginalName();
                    $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                    $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                    if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                        Storage::disk('public')->delete($externalResource->logo_path);
                        $externalResource->logo_path = $mainImageFileName;
                    }
                }
            } catch (\Exception $e) {



                return \Redirect::back()->withErrors($e->getMessage())->withInput();

            }

        }

        $externalResource->save();

       Flash::success(trans('backend.ExternalResource') . trans('backend.success_updated'));

        return redirect(route('admin.external-resources.index'));
    }

    /**
     * Remove the specified ExternalResource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $externalResource = $this->externalResourceRepository->findWithoutFail($id);

        if (empty($externalResource)) {
            Flash::error(trans('backend.ExternalResource') . trans('backend.not_found') );

            return redirect(route('admin.external-resources.index'));
        }

        $this->externalResourceRepository->delete($id);

        Flash::success(trans('backend.ExternalResource') . trans('backend.success_deleted'));

        return redirect(route('admin.external-resources.index'));
    }
}
