<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\TelemedRegisterRequestCriteria;
use App\Criteria\TelemedSearchCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedCenterRequest;
use App\Http\Requests\Admin\UpdateTelemedCenterRequest;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedUserPivot;
use App\Models\NotificationManager;
use App\Repositories\Admin\TelemedCenterRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TelemedCenterController extends InfyOmBaseController
{
    /** @var  TelemedCenterRepository */
    private $telemedCenterRepository;

    public function __construct(TelemedCenterRepository $telemedCenterRepo)
    {
        $this->telemedCenterRepository = $telemedCenterRepo;
    }

    /** Функция не используется вообще **/
    public function setCoordnator($telemedId)
    {
        if (isset($_GET['user_id'])) {
            $pivot = TelemedUserPivot::where(["telemed_center_kz" => $telemedId, 'user_id' => (int)$_GET['user_id']])->first();
            if ($pivot == null) {
                $newpivot = new TelemedUserPivot();
                $newpivot->user_id = (int)$_GET['user_id'];
                $newpivot->telemed_center_id = $telemedId;
                $newpivot->value = "coordinator";
                $newpivot->save();
                Flash::success(trans('backend.TelemedCenter') . "Пользователь успешно привязан к роли");
                return redirect(route('admin.telemed-centers.index'));
            } else {
                Flash::error(trans('backend.TelemedCenter') . "Такая привязка уже существует");
                return redirect(route('admin.telemed-centers.index'));
            }
        } else {
            Flash::error(trans('backend.TelemedCenter') . "недостаточно параметров");
            return redirect(route('admin.telemed-centers.index'));
        }
    }

    /** Функция не используется вообще **/
    public function setAdministrator($telemedId)
    {
        if (isset($_GET['user_id'])) {
            $pivot = TelemedUserPivot::where(["telemed_center_kz" => $telemedId, 'user_id' => (int)$_GET['user_id']])->first();
            if ($pivot == null) {
                $newpivot = new TelemedUserPivot();
                $newpivot->user_id = (int)$_GET['user_id'];
                $newpivot->telemed_center_id = $telemedId;
                $newpivot->value = "admin";
                $newpivot->save();
                Flash::success(trans('backend.TelemedCenter') . "Пользователь успешно привязан к роли");
                return redirect(route('admin.telemed-centers.index'));
            } else {
                Flash::error(trans('backend.TelemedCenter') . "Такая привязка уже существует");
                return redirect(route('admin.telemed-centers.index'));
            }
        } else {
            Flash::error(trans('backend.TelemedCenter') . "недостаточно параметров");
            return redirect(route('admin.telemed-centers.index'));
        }
    }

    /**
     * Display a listing of the TelemedCenter.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        $mcf_id = "";
        /** Получение параметров поиска **/
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($request->has('mcf_id'))
        {
            $mcf_id = $request->get('mcf_id');
        }
        if($request->has('mode'))
        {
            $mode = $request->get('mode');
        }
        else
        {
            $mode = 'all';
        }
        $crit = new TelemedSearchCriteria();

        $this->telemedCenterRepository->pushCriteria(new RequestCriteria($request));
        if($mode == 'all')
        {
            $crit->setTelemedId($mcf_id,false);

        }
        else if($mode == 'trash')
        {
            $crit->setTelemedId($mcf_id,true);

        }
        $this->telemedCenterRepository->pushCriteria($crit);
        $telemedCenters = $this->telemedCenterRepository->paginate(30);


        $mcfs = MedCareFac::get()->pluck('name','id');
        return view('admin.telemed_centers.index',['mode'=>$mode,'search'=>$search,'mcfs'=>$mcfs,'mcf_id'=>$mcf_id])
            ->with('telemedCenters', $telemedCenters);
    }

    /**
     * Show the form for creating a new TelemedCenter.
     *
     * @return Response
     */
    public function create()
    {
        $items = MedCareFac::pluck('name', 'id');
        return view('admin.telemed_centers.create', ['items' => $items]);
    }

    /**
     * Store a newly created TelemedCenter in storage.
     *
     * @param CreateTelemedCenterRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedCenterRequest $request)
    {
        $input = $request->all();
        $this->validate($request,
            [
                'name'=>'required',
                'address'=>'required',
                'director_fullname'=>'required',
                'coordinator_fullname'=>'required',
                'coordinator_phone'=>'required',
                'tech_specialist_contacts'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'equipment_location'=>'required',
                'terminal_name'=>'required',
                'terminal_number'=>'required|integer'
            ]);
        $TelemedCenter = $this->telemedCenterRepository->create($input);
       if($TelemedCenter != null)
       {
           $mcfadmin = $TelemedCenter->medCareFac->user;
           if($mcfadmin != null)
           {
               NotificationManager::createNotification("Администратор портала зарегистрировал ТМЦ - ".$TelemedCenter->name." для вашего ЛПУ.",[$mcfadmin->id],true,true);
           }
       }

        Flash::success(trans('backend.TelemedCenter') . trans('backend.success_saved'));

        return redirect(route('admin.telemed-centers.index'));
    }

    /**
     * Display the specified TelemedCenter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        if (empty($telemedCenter)) {
            Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));

            return redirect(route('admin.telemed-centers.index'));
        }

        return view('admin.telemed_centers.show')->with('telemedCenter', $telemedCenter);
    }

    /**
     * Show the form for editing the specified TelemedCenter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);
        $items = MedCareFac::pluck('name', 'id');
        $userpivots = TelemedUserPivot::where(['telemed_center_id' => $id])->get();
        if (empty($telemedCenter)) {
            Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));

            return redirect(route('admin.telemed-centers.index'));
        }

        return view('admin.telemed_centers.edit', ['items' => $items, 'userpivots' => $userpivots])->with('telemedCenter', $telemedCenter);
    }

    /**
     * Update the specified TelemedCenter in storage.
     *
     * @param  int $id
     * @param UpdateTelemedCenterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedCenterRequest $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'address'=>'required',
                'director_fullname'=>'required',
                'coordinator_fullname'=>'required',
                'coordinator_phone'=>'required',
                'tech_specialist_contacts'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'equipment_location'=>'required',
                'terminal_name'=>'required',
                'terminal_number'=>'required|integer'
            ]);
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        if (empty($telemedCenter)) {
            Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));

            return redirect(route('admin.telemed-centers.index'));
        }
        $input = $request->all();
        if (isset($input['med_care_fac_id']))
            unset($input['med_care_fac_id']);
        if ($this->telemedCenterRepository->update($input, $id)) {
            $users = $telemedCenter->telemedUserPivots;
            foreach ($users as $user) {
                \Cache::forget("usertmcs" . $user->user_id);
            }
            $mcf = $telemedCenter->medCareFac;
            \Cache::forget("usertmcs" . $mcf->user_id);
        }

        Flash::success(trans('backend.TelemedCenter') . ' успешно изменен');

        return redirect(route('admin.telemed-centers.index'));
    }

    /**
     * Remove the specified TelemedCenter from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $trash = false;
        $telemedCenter = $this->telemedCenterRepository->findWithoutFail($id);

        /** Проверка для режима восстановления или же удаления **/
        if($request->has('force_delete') && $request->get('force_delete') == true ) {
            $telemedCenter = TelemedCenter::onlyTrashed()->find($id)->forceDelete();
            Flash::error(trans('backend.TelemedCenter') . ' безвозвратно уделен из базы данных');
            return redirect(route('admin.telemed-centers.index'));
        }
        if (empty($telemedCenter)) {
            $telemedCenter = TelemedCenter::onlyTrashed()->find($id);
            $trash = true;
            if (empty($telemedCenter))
            {
                Flash::error(trans('backend.TelemedCenter') . trans('backend.not_found'));
                return redirect(route('admin.telemed-centers.index'));
            }
            
        }
        if($trash == false)
        {
            Flash::success(trans('backend.TelemedCenter') . ' успешно удален');
            $this->telemedCenterRepository->delete($id);
        }
        else if($trash == true)
        {
            Flash::success(trans('backend.TelemedCenter') . " успешно восстановлен!");
            $telemedCenter->restore();
        }




        return redirect(route('admin.telemed-centers.index'));
    }
}
