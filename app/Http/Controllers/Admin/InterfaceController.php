<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Role;
use App\Models\Admin\TelemedCenter;
use App\Models\InterfaceElement;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class InterfaceController extends Controller
{
    /** Страница со списком элементов интерфейса **/
    public function index()
    {
        $interfaceElements = InterfaceElement::all();


        return view('admin.interfaces.index', compact(['interfaceElements']));
    }

    /** Страница редактирования элемента интерфейса **/
    public function edit($elName)
    {

        $interfaceElement = InterfaceElement::find($elName);
        $roles = array_add(Role::all()->pluck('name', 'slug'),'guest', 'Неавторизованный');
        $disabledRoles = json_decode($interfaceElement->disabled_roles);
        return view('admin.interfaces.edit', compact('interfaceElement', 'roles', 'disabledRoles'));
    }

    /** Сохранение изменений формы от edit **/
    public function update($elName, Request $request)
    {
        $interfaceElement = InterfaceElement::find($elName);

        
        if (empty($interfaceElement)) {
            Flash::error(trans('backend.InterfaceElement') . trans('backend.not_found'));

            return redirect(route('admin.interfaces.index'));
        }

        $interfaceElement->disabled_roles = json_encode($request->get('roles', []));
        $interfaceElement->save();

        return redirect(route('admin.interfaces.edit', $elName));

    }

    /** Страница изменний элементов интерфейса для определенных пользователей **/
    public function editUsers($elName, Request $request)
    {

        $interfaceElement = InterfaceElement::find($elName);

        if( $request->has('search')) {
            $users  = User::where('email', 'like', "%{$request['search']}%")
                ->orWhere('name', 'like', "%{$request['search']}%")
                ->orWhere('surname', 'like', "%{$request['search']}%")
                ->orWhere('middlename', 'like', "%{$request['search']}%")->paginate(20);
            ;
        } else {
            $users = User::paginate(20);
        }

        $disabledUsers = $interfaceElement->disabledUsers()->pluck('id')->toArray();

        return view('admin.interfaces.edit-users', compact('interfaceElement', 'users', 'disabledUsers'));
    }

    /** Сохранение изменений от формы от editUsers **/
    public function updateUsers($elName, Request $request)
    {

        $input = $request->all();

        $interfaceElement = InterfaceElement::find($elName);

        //return response()->json($input);
        if( $input['val'] == 'true' ) {
            $interfaceElement->disabledUsers()->detach($input['user_id']);
            return response()->json(0);
        } else {
            $interfaceElement->disabledUsers()->attach($input['user_id']);
            return response()->json(1);
        }

    }
}
