<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Role;
use App\Models\GraphicalReport;
use App\Models\TableReport;
use App\Http\Requests;
use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Models\Admin\TelemedCenter;
use App\Models\Report;
use App\Repositories\ReportRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReportController extends InfyOmBaseController
{
    /** @var  ReportRepository */
    private $reportRepository;

    protected static $format = 'Y-m-d';

    /** calculate range*/
    protected function getDateRange($request)
    {
        $format = self::$format;
        $defaults = [
            'from' => Carbon::now()->format($format),
            'to' => Carbon::now()->subWeek()->format($format)
        ];
        $from = Carbon::createFromFormat($format, $request->get('from', $defaults['from']))->startOfDay();
        $to = Carbon::createFromFormat($format, $request->get('to', $defaults['to']))->endOfDay();
        return [$from, $to];
    }

    public function __construct(ReportRepository $reportRepo)
    {
        $this->reportRepository = $reportRepo;
    }

    /**
     * Display a listing of the Report.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reportRepository->pushCriteria(new RequestCriteria($request));
        $reports = $this->reportRepository->paginate(15);

        // Виды графиков или отчетов
        $reportTypes = Report::$reportTypes;

        return view('admin.reports.index', compact('reportTypes'))
            ->with('reports', $reports);
    }

    /**
     * Show the form for editing the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }
        // Если прикреплен к ТМЦ запретить редактировать
        if($report->tmc_id != null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);
            if($tmc) {
                Flash::error('Отчет закреплен к ТМЦ '. $tmc->name);
            } else {
                Flash::error('Отчет закреплен к определенной ТМЦ ');
            }

            return redirect(route('admin.reports.index'));
        }

        $reportType = $report->type;
        $reportRows = [];
        $reportHeaders = [];
        $defaultSelectedRows = json_decode($report->headers);
        $defaultTmcs = json_decode($report->tmcs);
 
      

        //
        $rowNames = array_keys(\Config::get("reports.rows"));
        $tmcs =  [];
        $tmcs_array = TelemedCenter::join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
            ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
            ->get()
            ->groupBy('mcf_name');

        foreach ($tmcs_array as $mcf_name => $collection) {
            foreach ($collection as $key => $item) {
                $tmcs[$mcf_name][$item['id']] = $item['name'];
            };
        }

        if ($reportType != null && in_array($reportType, $rowNames)) {
            $reportHeaders = \Config::get("reports.rows.{$reportType}");
            $requiredSelectedRows = \Config::get("reports.options.defaults.{$reportType}");

            foreach ($reportHeaders as $key => $val) {
                $reportRows[$val] = [
                    'value' => $key,
                    'disabled' => in_array($key, $requiredSelectedRows) ? 'disabled' : false
                ];
            }
        }
 
        return view('admin.reports.edit', [
            'name' => $report->name,
            'reportType' => $reportType,
            'reportRows' => $reportRows,
            'defaultSelectedRows' => $defaultSelectedRows,
            'tmcs' => $tmcs,
            'defaultTmcs' => $defaultTmcs,
        ])->with('report', $report);
    }


    /**
     * Show the form for creating a new Report.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        // Виды графиков или отчетов
        $reportTypes = Report::$reportTypes;
        return view('admin.reports.create', compact('reportTypes'));


    }

    /**
     * Store a newly created Report in storage.
     *
     * @param CreateReportRequest $request
     *
     * @return Response
     */
    public function store(CreateReportRequest $request)
    {
        $input = $request->all();

        if ($request->has('step')) {
            $step = $request['step'];

            $rowNames = array_keys(\Config::get("reports.rows"));
            $reportType = $request->get('report_type');

            $reportRows = [];
            $reportHeaders = [];
            $defaultSelectedRows = [];

            if ($reportType != null && in_array($reportType, $rowNames)) {
                $reportHeaders = \Config::get("reports.rows.{$reportType}");
                $defaultSelectedRows = \Config::get("reports.options.defaults.{$reportType}");

                foreach ($reportHeaders as $key => $val) {
                    $reportRows[$val] = [
                        'value' => $key,
                        'disabled' => in_array($key, $defaultSelectedRows) ? 'disabled' : false
                    ];
                }
            }


            switch ($step) {
                /** Этап №1 */
                case 1:
                    return redirect()->route('admin.reports.index');
                    break;
                /** Этап №2 */
                case 2:
                    $reportType = $request->get('report_type');

                    //return response()->json($reportHeaders);
                    return view('admin.reports.create', compact('step', 'reportRows', 'defaultSelectedRows', 'reportType'));
                    break;
                /** Этап №3 */
                case 3:
                    $reportType = $request->get('report_type');

                    $rows = $request->get('rows');

                    $tmcs = [];
                    $tmcs_array = TelemedCenter::join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
                        ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
                        ->get()
                        ->groupBy('mcf_name');

                    foreach ($tmcs_array as $mcf_name => $collection) {
                        foreach ($collection as $key => $item) {
                            $tmcs[$mcf_name][$item['id']] = $item['name'];
                        };
                    }

                    return view('admin.reports.create', compact('tmcs', 'rows', 'step', 'reportRows', 'defaultSelectedRows', 'reportType'));

                    //return response()->json(['rows' => $rows, $request->all()]);

                    break;
                /** Финальный Этап */
                case 'final':
                    //return $request->all();

                    $user = \Sentinel::getUser();

                    $selected_rows = $request->get('selected_rows');
         
                    $tmcs = $request['tmcs'];
                    $report_type = $request['report_type'];
                    // work with rows
                    if (count($selected_rows))
                        $rows = array_merge($defaultSelectedRows, $selected_rows);
                    else
                        $rows = $defaultSelectedRows;

                    $report = new Report();
                    $report->headers = json_encode($rows);
                    $report->tmcs = json_encode($tmcs);
                    $report->roles = json_encode( ['admin']);
                    $report->type = $report_type;
                    $report->user_id = $user->id;
                    $report->name = $request['name'];

                    //return $report;

                    if ($report->save()) {
                        Flash::success(trans('backend.success_saved'));

                        return redirect(route('reports.init.table', $report->id));
                    } else {
                        Flash::error('Ошибка сохранения!');

                        return redirect(route('admin.reports.index'));
                    }


                default:

                    Flash::error('Ошибка!');

                    return redirect(route('admin.reports.index'));
            }
        }
    }

    /**
     * Display the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        // Получить название ТМЦ
        if($report->tmc_id != null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);
            $tmc_name = $tmc->name;

        } else {
            $tmc_name = 'Не прикреплен к ТМЦ';
        }

        return view('admin.reports.show', compact('tmc_name'))->with('report', $report);
    }


    /**
     * Update the specified Report in storage.
     *
     * @param  int $id
     * @param UpdateReportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportRequest $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        if($report->tmc_id != null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);
            if($tmc) {
                Flash::error('Отчет закреплен к ТМЦ '. $tmc->name);
            } else {
                Flash::error('Отчет закреплен к определенной ТМЦ ');
            }

            return redirect(route('admin.reports.index'));
        }

        $data = [
            'name' => $request->get('name', 'Вы забыли указать имя при сохранении'),
            'tmcs' => json_encode($request->get('tmcs', [])),
            'headers' => json_encode($request->get('rows', [])),
            'roles' => json_encode( ['admin']),
        ];

        $report = $this->reportRepository->update($data, $id);

        Flash::success(trans('backend.success_updated'));


        return redirect(route('admin.reports.index'));
    }

    /**
     * Remove the specified Report from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        if($report->tmc_id != null) {
            $tmc = TelemedCenter::withTrashed()->find($report->tmc_id);
            if($tmc) {
                Flash::error('Отчет закреплен к ТМЦ '. $tmc->name);
            } else {
                Flash::error('Отчет закреплен к определенной ТМЦ ');
            }

            return redirect(route('admin.reports.index'));
        }
        $this->reportRepository->delete($id);

        Flash::success(trans('backend.success_deleted'));

        return redirect(route('admin.reports.index'));
    }


    public function transfer($id)
    {

        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        $tmcs = [null => 'Выберите из списка'];
        $tmcs_array = TelemedCenter::join('med_care_facs as m', 'm.id', '=', 'telemed_centers.med_care_fac_id')
            ->select('telemed_centers.id as id', 'telemed_centers.name as name', 'm.name as mcf_name')
            ->get()
            ->groupBy('mcf_name');

        foreach ($tmcs_array as $mcf_name => $collection) {
            foreach ($collection as $key => $item) {
                $tmcs[$mcf_name][$item['id']] = $item['name'];
            };
        }


        return view('admin.reports.transfer',  compact( 'tmcs'))->with('report', $report);


    }
    public function transferProcess($id, UpdateReportRequest $request)
    {

        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error(trans('backend.Report') . trans('backend.not_found'));

            return redirect(route('admin.reports.index'));
        }

        //return $request->all();
        $data = [
            'tmc_id' => is_numeric($request->get('tmc_id')) ? $request->get('tmc_id') : null,

        ];
        //return $data;
        $report = $this->reportRepository->update($data, $id);

        Flash::success('Вы успешно передали отчет другой ТМЦ');


        return redirect(route('admin.reports.index'));

    }
}