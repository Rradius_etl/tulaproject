<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedRegisterRequestRequest;
use App\Http\Requests\Admin\UpdateTelemedRegisterRequestRequest;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedRegisterRequest;
use App\Models\NotificationManager;
use App\Repositories\Admin\TelemedRegisterRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use App\Criteria\TelemedRegisterRequestCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TelemedRegisterRequestController extends InfyOmBaseController
{
    /** @var  TelemedRegisterRequestRepository */
    private $telemedRegisterRequestRepository;

    public function __construct(TelemedRegisterRequestRepository $telemedRegisterRequestRepo)
    {
        $this->telemedRegisterRequestRepository = $telemedRegisterRequestRepo;
        $crit = new TelemedRegisterRequestCriteria();
        if(isset($_GET['mode'])&&in_array($_GET['mode'],['pending','rejected','agreed','all']))
        {
            $crit->setMode($_GET['mode']);
        }else
        {
            $crit->setMode("all");
        }
        $this->crit=$crit;
        $this->telemedRegisterRequestRepository->pushCriteria($crit);
    }

    // принять заявку
    public function agree($TelemedRegisterRequest)
    {
        $trr = TelemedRegisterRequest::findOrFail($TelemedRegisterRequest);
        $trr->decision = "agreed";
        $trr->save();
        $TelemedCenter = New TelemedCenter($trr->toArray());
        $TelemedCenter->save();
        $mcfadmin = $TelemedCenter->medCareFac->user;
        if($mcfadmin != null)
        {
            NotificationManager::createNotification("Ваша заявка на регистрацию ТМЦ - ".$TelemedCenter->name." была одобрена администратором портала.",[$mcfadmin->id],true,true);
        }
        Flash::success(trans('backend.TelemedRegisterRequest') . ' успешно изменена');
       return redirect(route('admin.telemed-register-requests.index'));
    }
    // отклонить заявку
    public function reject($TelemedRegisterRequest)
    {
        $trr = TelemedRegisterRequest::findOrFail($TelemedRegisterRequest);
        $trr->decision = "rejected";
        $trr->save();
        $mcfadmin = $trr->med_care_facs->user;
        if($mcfadmin != null)
        {
            NotificationManager::createNotification("Ваша заявка на регистрацию ТМЦ - ".$trr->name." была отклонена администратором портала.",[$mcfadmin->id],true,true);
        }
        Flash::success(trans('backend.TelemedRegisterRequest') . ' успешно изменена');
      return  redirect(route('admin.telemed-register-requests.index'));
    }

    /**
     * Display a listing of the TelemedRegisterRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $this->telemedRegisterRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedRegisterRequests = $this->telemedRegisterRequestRepository->paginate(30);

        return view('admin.telemed_register_requests.index',['search'=>$search])
            ->with('telemedRegisterRequests', $telemedRegisterRequests);
    }


    /**
     * Display the specified TelemedRegisterRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
            Flash::error( trans('backend.not_found') );

            return redirect(route('admin.telemed-register-requests.index'));
        }

        return view('admin.telemed_register_requests.show')->with('telemedRegisterRequest', $telemedRegisterRequest);
    }

    /**
     * Show the form for editing the specified TelemedRegisterRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
             Flash::error(trans('backend.TelemedRegisterRequest') . trans('backend.not_found') );

            return redirect(route('admin.telemed-register-requests.index'));
        }

        return view('admin.telemed_register_requests.edit')->with('telemedRegisterRequest', $telemedRegisterRequest);
    }

    /**
     * Update the specified TelemedRegisterRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedRegisterRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedRegisterRequestRequest $request)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);
        /** Валидация данных с формы **/
        $this->validate($request,
            [
                'name'=>'required',
                'address'=>'required',
                'director_fullname'=>'required',
                'coordinator_fullname'=>'required',
                'coordinator_phone'=>'required',
                'tech_specialist_contacts'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'equipment_location'=>'required',
                'terminal_name'=>'required',
                'terminal_number'=>'required|integer'
            ]);
        if (empty($telemedRegisterRequest)) {
             Flash::error( trans('backend.not_found') );

            return redirect(route('admin.telemed-register-requests.index'));
        }

        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->update($request->all(), $id);

       Flash::success(trans('backend.TelemedRegisterRequest') . ' успешно изменена');

        return redirect(route('admin.telemed-register-requests.index'));
    }

    /**
     * Remove the specified TelemedRegisterRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
            Flash::error( trans('backend.not_found') );

            return redirect(route('admin.telemed-register-requests.index'));
        }

        $this->telemedRegisterRequestRepository->delete($id);

        Flash::success( 'упешно удалена');

        return redirect(route('admin.telemed-register-requests.index'));
    }
}
