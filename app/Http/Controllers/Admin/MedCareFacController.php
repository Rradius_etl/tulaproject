<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateMedCareFacRequest;
use App\Http\Requests\Admin\UpdateMedCareFacRequest;
use App\Models\Admin\MedCareFac;
use App\Models\User;
use App\Repositories\Admin\MedCareFacRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Cache;
class MedCareFacController extends InfyOmBaseController
{
    /** @var  MedCareFacRepository */
    private $medCareFacRepository;

    public function __construct(MedCareFacRepository $medCareFacRepo)
    {
        $this->medCareFacRepository = $medCareFacRepo;
        $array = [""=>"------------------"];
    }

    /**
     * Display a listing of the MedCareFac.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->medCareFacRepository->pushCriteria(new RequestCriteria($request));
        if($request->has('mode'))
        {
            $mode = $request->get('mode');
        }
        else
        {
            $mode = 'all';
        }
        if($mode == 'all')
        {
            $medCareFacs = $this->medCareFacRepository->paginate(30);
        }
        else if($mode == 'trash')
        {
            $medCareFacs = $this->medCareFacRepository->onlyTrashed()->paginate(30);
        }

        return view('admin.med_care_facs.index',['medCareFacs'=>$medCareFacs,'mode'=>$mode]);
    }

    /**
     * Show the form for creating a new MedCareFac.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.med_care_facs.create');
    }

    /**
     * Store a newly created MedCareFac in storage.
     *
     * @param CreateMedCareFacRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),
            [
                'consultations_norm'=>'required|integer|min:0',
                'code'=>'required|integer',
                'name'=>'required',
            ]);
        $input = $request->all();
        if(isset($input['user_id'])&&($input['user_id']==null||$input['user_id']==""))
        {
            unset($input['user_id']);
        }

        $validator->after(function($v) use($input){
            if(isset($input['user_id'])&&$input['user_id'] != null) {
                $mcf = MedCareFac::where(['user_id'=>$input['user_id']])->first();
                if($mcf != null)
                {
                    $v->errors()->add('user_id', 'данный пользователь уже привязан к ЛПУ - '.$mcf->name);

                }
            }
        });
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        if($this->medCareFacRepository->create($input))
        {
            if(isset($input['user_id'])&&$input['user_id'] != null)
            {
                $user = User::find($input['user_id']);
                if($user != null)
                {
                    $roles = $user->tmcRoles;
                    foreach($roles as $role)
                    {
                        $role->delete();
                    }
                }
            }

            if(isset($input['user_id'])&&$input['user_id'] != null) {
                Cache::forget("userroles" . $input['user_id']);
                Cache::forget("usertmcs".$input['user_id']);
            }
        }
        Flash::success(trans('backend.MedCareFac') . trans('backend.success_saved_m'));

        return redirect(route('admin.med-care-facs.index'));
    }

    /**
     * Display the specified MedCareFac.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medCareFac = $this->medCareFacRepository->findWithoutFail($id);

        if (empty($medCareFac)) {
            Flash::error(trans('backend.MedCareFac') . trans('backend.not_found_m') );

            return redirect(route('admin.med-care-facs.index'));
        }


        return view('admin.med_care_facs.show')->with('medCareFac', $medCareFac);
    }

    /**
     * Show the form for editing the specified MedCareFac.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medCareFac = $this->medCareFacRepository->findWithoutFail($id);

        if (empty($medCareFac)) {
             Flash::error(trans('backend.MedCareFac') . trans('backend.not_found_m') );

            return redirect(route('admin.med-care-facs.index'));
        }

        return view('admin.med_care_facs.edit')->with('medCareFac', $medCareFac);
    }

    /**
     * Update the specified MedCareFac in storage.
     *
     * @param  int              $id
     * @param UpdateMedCareFacRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),
            [
                'consultations_norm'=>'required|integer|min:0',
                'code'=>'required|integer',
                'name'=>'required',
            ]);

        $medCareFac = $this->medCareFacRepository->findWithoutFail($id);

        if (empty($medCareFac)) {
             Flash::error(trans('backend.MedCareFac') . trans('backend.not_found_m') );

            return redirect(route('admin.med-care-facs.index'));
        }
        $input = $request->all();
        if(!isset($input['user_id']))
        {
            $input['user_id']=null;
        }
        else if($input['user_id']==null)
        {
            $input['user_id']=null;
        }

        /** Проверка на приаязку предпологаемого пользователя к другим ЛПУ **/
        $validator->after(function($v) use($medCareFac,$input){
            if($input['user_id'] != null) {
                $mcf = MedCareFac::where(['user_id'=>$input['user_id']])->where('id','!=',$medCareFac->id)->first();
                if($mcf != null)
                {
                    $v->errors()->add('user_id', 'данный пользователь уже привязан к ЛПУ - '.$mcf->name);

                }
            }
        });

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

         if($this->medCareFacRepository->update($input, $id))
         {
             if($input['user_id'] != null)
             {
                 $user = User::find($input['user_id']);
                 if($user != null)
                 {
                     $roles = $user->tmcRoles;
                     foreach($roles as $role)
                     {
                         $role->delete();
                     }
                 }
             }

             /** Очистить кеш списка ролей и ТМЦ для определенных пользователей **/
             if(($input['user_id'] == null && $medCareFac->user_id != null)) {
                 Cache::forget("userroles" . $medCareFac->user_id);
                 Cache::forget("usertmcs".$medCareFac->user_id);
             }
             else if($input['user_id'] != null && $medCareFac->user_id != null &&  $medCareFac->user_id != $input['user_id'])
             {
                 Cache::forget("userroles" . $input['user_id']);
                 Cache::forget("userroles" . $medCareFac->user_id);
                 Cache::forget("usertmcs".$medCareFac->user_id);
                 Cache::forget("usertmcs".$input['user_id']);
             }
             else if($input['user_id'] != null)
             {
                 Cache::forget("userroles" . $input['user_id']);
                 Cache::forget("usertmcs".$input['user_id']);
             }
         }
       Flash::success(trans('backend.MedCareFac') . trans('backend.success_updated_m'));

        return redirect(route('admin.med-care-facs.index'));
    }

    /**
     * Remove the specified MedCareFac from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medCareFac = MedCareFac::find($id);
        $trash = false;
        if (empty($medCareFac)) {
            $medCareFac = MedCareFac::onlyTrashed()->find($id);
            $trash = true;
            if (empty($medCareFac)) {
                Flash::error(trans('backend.MedCareFac') . trans('backend.not_found_m') );
                return redirect(route('admin.med-care-facs.index'));
            }
        }
        if($trash == true)
        {
            Flash::success(trans('backend.MedCareFac') . " успешно восстановлено");
            $medCareFac->restore($id);
        }
        else
        {
            Flash::success(trans('backend.MedCareFac') . trans('backend.success_deleted_m'));
            $this->medCareFacRepository->delete($id);
        }
        Cache::forget("userroles" . $medCareFac->user_id);
        Cache::forget("usertmcs". $medCareFac->user_id);

        return redirect(route('admin.med-care-facs.index'));
    }
}
