<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBlockRequest;
use App\Http\Requests\Admin\UpdateBlockRequest;
use App\Repositories\Admin\BlockRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BlockController extends InfyOmBaseController
{
    /** @var  BlockRepository */
    private $blockRepository;

    public function __construct(BlockRepository $blockRepo)
    {
        $this->blockRepository = $blockRepo;
    }

    /**
     * Display a listing of the Block.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blockRepository->pushCriteria(new RequestCriteria($request));
        $blocks = $this->blockRepository->paginate(20);

        return view('admin.blocks.index')
            ->with('blocks', $blocks);
    }

    /**
     * Show the form for creating a new Block.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.blocks.create');
    }

    /**
     * Store a newly created Block in storage.
     *
     * @param CreateBlockRequest $request
     *
     * @return Response
     */
    public function store(CreateBlockRequest $request)
    {
        $input = $request->all();

        $block = $this->blockRepository->create($input);

        Flash::success(trans('backend.Block') . trans('backend.success_saved'));

        return redirect(route('admin.blocks.index'));
    }

    /**
     * Display the specified Block.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error(trans('backend.Block') . trans('backend.not_found') );

            return redirect(route('admin.blocks.index'));
        }

        return view('admin.blocks.show')->with('block', $block);
    }

    /**
     * Show the form for editing the specified Block.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
             Flash::error(trans('backend.Block') . trans('backend.not_found') );

            return redirect(route('admin.blocks.index'));
        }

        return view('admin.blocks.edit')->with('block', $block);
    }

    /**
     * Update the specified Block in storage.
     *
     * @param  int              $id
     * @param UpdateBlockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlockRequest $request)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
             Flash::error(trans('backend.Block') . trans('backend.not_found') );

            return redirect(route('admin.blocks.index'));
        }

        $block = $this->blockRepository->update($request->all(), $id);

       Flash::success(trans('backend.Block') . trans('backend.success_updated'));

        return redirect(route('admin.blocks.index'));
    }

    /**
     * Remove the specified Block from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error(trans('backend.Block') . trans('backend.not_found') );

            return redirect(route('admin.blocks.index'));
        }

        $this->blockRepository->delete($id);

        Flash::success(trans('backend.Block') . trans('backend.success_deleted'));

        return redirect(route('admin.blocks.index'));
    }
}
