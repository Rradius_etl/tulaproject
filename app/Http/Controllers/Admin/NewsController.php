<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateNewsRequest;
use App\Http\Requests\Admin\UpdateNewsRequest;
use App\Models\Admin\News;
use App\Repositories\Admin\NewsRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Sentinel;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use File;

class NewsController extends InfyOmBaseController
{
    /** @var  NewsRepository */
    private $newsRepository;

    public function __construct(NewsRepository $newsRepo)
    {
        $this->newsRepository = $newsRepo;
    }

    /**
     * Display a listing of the News.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->newsRepository->pushCriteria(new RequestCriteria($request));
        $news = $this->newsRepository->paginate(20);

        return view('admin.news.index')
            ->with('news', $news);
    }

    /**
     * Show the form for creating a new News.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created News in storage.
     *
     * @param CreateNewsRequest $request
     *
     * @return Response
     */
    public function store(CreateNewsRequest $request)
    {
        $input = $request->all();

        $news =  new News($request->all());

        try {

            if( empty($news->slug) ) {
                $news->slug = str_slug($news->title,'-');
            }
            $news->created_by = \Sentinel::check()->getUserId();
            $news->modified_by = \Sentinel::check()->getUserId();
            $locationDirectory = "files/";
            //return response()->json();
            $mainImage = $request->file('img_path');
            if ($mainImage != null) {
                $extension = $mainImage->getClientOriginalExtension();
                $filename = $mainImage->getClientOriginalName();
                $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                    $news->img_path = $mainImageFileName;
                }
            }
        } catch (\Exception $e) {

            Storage::disk('public')->delete($news->img_path);

            return \Redirect::back()->withErrors($e->getMessage())->withInput();

        }

        $news->save();
        Flash::success("Новость" . trans('backend.success_saved_f'));

        return redirect(route('admin.news.index'));
    }

    /**
     * Display the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error("Новость" . trans('backend.not_found_f') );

            return redirect(route('admin.news.index'));
        }

        return view('admin.news.show')->with('news', $news);
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
             Flash::error("Новость" . trans('backend.not_found_f') );

            return redirect(route('admin.news.index'));
        }

        return view('admin.news.edit')->with('news', $news);
    }

    /**
     * Update the specified News in storage.
     *
     * @param  int              $id
     * @param UpdateNewsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewsRequest $request)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
             Flash::error("Новость" . trans('backend.not_found_f') );

            return redirect(route('admin.news.index'));
        }

        $news = $this->newsRepository->update($request->all(), $id);


        $news->modified_by = Sentinel::getUser()->id;

        if( empty($news->slug) ) {
            $news->slug = str_slug($news->title,'-');

        }
        $mainImage = $request->file('img_path');

        if ($mainImage != null) {  // If new img_path Image uploaded
            /** Сохранение изображения **/
            try {

                $locationDirectory = "files/";
                //return response()->json();
                $mainImage = $request->file('img_path');
                if ($mainImage != null) {
                    $extension = $mainImage->getClientOriginalExtension();
                    $filename = $mainImage->getClientOriginalName();
                    $unique_name = str_slug(str_replace($extension, '', $filename), '-');
                    $mainImageFileName = $locationDirectory . $unique_name . '_'.uniqid() . '.' . $extension;
                    if (Storage::disk('public')->put($mainImageFileName, File::get($mainImage))) {
                        Storage::disk('public')->delete($news->img_path);
                        $news->img_path = $mainImageFileName;
                    }
                }
            } catch (\Exception $e) {



                return \Redirect::back()->withErrors($e->getMessage())->withInput();

            }

        }

        $news->save();

       Flash::success("Новость" . trans('backend.success_updated_f'));

        return redirect(route('admin.news.index'));
    }

    /**
     * Remove the specified News from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error("Новость" . trans('backend.not_found_f') );

            return redirect(route('admin.news.index'));
        }
        /** Удаление изображения **/
        Storage::disk('public')->delete($news->img_path);
        $this->newsRepository->delete($id);



        Flash::success("Новость" . trans('backend.success_deleted_f'));

        return redirect(route('admin.news.index'));
    }
}
