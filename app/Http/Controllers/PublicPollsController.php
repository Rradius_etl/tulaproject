<?php

namespace App\Http\Controllers;

use App\Criteria\TelemedCriteria;
use App\Http\Requests;
use App\Http\Requests\CreatePollRequest;
use App\Http\Requests\UpdatePollRequest;
use App\Models\Poll;
use App\Models\Admin\PollRow;
use App\Models\Admin\PollUserPivot;
use App\Models\TelemedCenter;
use App\Repositories\PollRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Route;

class PublicPollsController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth']);
        $this->user = \Sentinel::getUser();
    }

    /**
     * Display a listing of the Poll.
     *
     * @param Request $request
     * @return Response
     */

    /** Страница опроса(публичная) */
    public function getPoll($id)
    {
        $state = false;
        $poll = Poll::findOrFail($id);
        $isanswered = PollUserPivot::where(['poll_id' => $id, 'user_id' => $this->user->id])->first();
        if($isanswered != null)
        {
            $state = $isanswered->poll_row_id;
        }
        if(isset($_GET['need_result']))
        {
            return view('public_polls.results', ['poll' => $poll,'state'=>$state,]);
        }
        return view('public_polls.show', ['poll' => $poll,'state'=>$state]);
    }

    /** Ответить на опрос */
    public function answerPoll(Request $request)
    {
        $this->validate($request, [
            'poll_id' => 'required|integer',
            'poll_row_id' => 'required|integer'
        ]);

        $poll_id = $request->get('poll_id');
        $poll = \App\Models\Admin\Poll::findOrFail($request->get('poll_id'));
        if ($poll->active == 0)
        {
            abort(404);
        }
        if($poll->finished_at != null)
        {
            if(Carbon::now()->diffInSeconds(new Carbon($poll->finished_at),false) <=0)
            {
                abort(400);
            }
        }
        $poll_row_id = $request->get('poll_row_id');

        $pollquestion = PollRow::where(['poll_id' => $poll_id, 'id' => $poll_row_id])->first();

        $isanswered = PollUserPivot::where(['poll_id' => $poll_id, 'user_id' => $this->user->id])->first();

        if (empty($pollquestion)) {
            abort(404);
        } else {

            if ($isanswered == null) {
                $answer = new PollUserPivot();
                $answer->poll_id = $poll_id;
                $answer->poll_row_id = $poll_row_id;
                $answer->user_id = $this->user->id;
                if ($answer->save()) {
                    return response()->json(['success' => true]);
                } else {
                    return Response::json(['error' => "произошла непредвиденная ошибка, попробуйте повторить операцию позже."], 500);
                }

            } else {
                return Response::json(['error' => "вы уже участвовали в этом опросе!"], 422);
            }
        }
    }


}
