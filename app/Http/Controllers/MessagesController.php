<?php
namespace App\Http\Controllers;
use App\Models\FileUpload;
use App\Models\MessagesThread;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Sentinel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->user = Sentinel::getUser();
    }

    public function index(Request $request)
    {
        $sortBy = $request->get('sortedBy', 'desc');

        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }


        $currentUserId = $this->user->id;
        // All threads, ignore deleted/archived participants
        $threads = $this->user->threads()
            ->join(\DB::raw('(select * from messages m where created_at = (select MAX(created_at) from messages m1 where m.thread_id = m1.thread_id) ORDER BY m.thread_id ASC ) as m'), 'm.thread_id', '=', 'threads.id')->orderBy('m.created_at', $sortBy);
            $threads->where('m.user_id','!=',$currentUserId);
        if( $search != "" ) {
            $threads->where('m.body','like',"%$search%");
            $threads->orWhere('threads.subject','like',"%$search%");
        }
        $threads =  $threads->paginate(20);
        // All threads that user is participating in
        // $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

        //return response()->json( compact('threads', 'currentUserId'));
        return view('messenger.index', compact('threads', 'currentUserId'));
    }

    /** Непрочитанные письма */
    public function unread(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $sortBy = $request->get('sortedBy', 'desc');
        $currentUserId = $this->user->id;
        // All threads, ignore deleted/archived participants
        $threads = $this->user->threads()
            ->join(\DB::raw('(select * from messages m where created_at = (select MAX(created_at) from messages m1 where m.thread_id = m1.thread_id) ORDER BY m.thread_id ASC ) as m'), 'm.thread_id', '=', 'threads.id')
            ->where(function ($q){
                $q->whereNull('participants.last_read');
                $q->orWhere(\DB::raw('Date(participants.last_read)'), '<', \DB::raw('Date(threads.updated_at)'));

            })
            ->orderBy('m.created_at', $sortBy);
        if( $search != "" ) {
            $threads->where('m.body','like',"%$search%");
            $threads->orWhere('threads.subject','like',"%$search%");
        }
        $threads = $threads->paginate(20);
        return view('messenger.unread', compact('threads', 'currentUserId'));
    }
    /** Исходящие письма */
    public function outgoing(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $orderBy = $request->get('orderBy', 'created_at');
        $sortBy = $request->get('sortedBy', 'desc');

        $currentUserId = $this->user->id;
        // All threads, ignore deleted/archived participants
        $messages = $this->user->messages()->orderBy($orderBy,$sortBy)->with('thread','recipients');
        if( $search != "" ) {

            $messages->whereExists(function ($query) use ($search) {
                $query->select(\DB::raw('1'))->from('threads as t')->where('t.id', \DB::raw('messages.thread_id'))->where('t.subject','like','%'.$search.'%');
            });
            $messages->orWhere('messages.body','like',"%$search%");
        }
        $messages = $messages->paginate(20);
        
        return view('messenger.outgoing', compact('messages', 'currentUserId'));
    }
 
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $threads = $this->user->threads()->pluck('threads.id')->toArray();
            $thread = MessagesThread::findOrFail($id);
            if(!in_array($id,$threads))
            {
                abort(401);
            }
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'Тема с идентификатором: ' . $id . ' не найдена.');
            return redirect('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Sentinel::getUser()->id;
        $thread->markAsRead($userId);
        return view('messenger.show', compact('thread'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Sentinel::getUser()->id)->pluck('email','id');
        return view('messenger.create', compact('users'));
    }

    protected static function sendEmail($thread,$message,$user_ids)
    {
        $user_ids = $user_ids->toArray();
        $users = [];
        $message = $message."<p> Зайдите по <a href='".\URL::to("/messages/".$thread->id)."'>ссылке</a> чтобы ответить на сообщение </p>";
        foreach($user_ids as $usr)
        {
            if($usr != Sentinel::getUser()->id)
            {
                $user = User::find($usr);
                if($user != null)
                    array_push($users,$user->email);
            }
        }
        try {
            \Mail::send('emails.notification.template', ['m' => $message], function ($m) use ($users, $thread) {
                $m->from(config('mail.username'), config('mail.username'));
                $m->to($users)->subject($thread->subject);
            });
        }
        catch (\Exception $e)
        {
            return false;
        }
        return true;
    }



    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request )
    {
        $input = Input::all();

        if( $request->has('request_id')) {
            $thread = MessagesThread::create(
                [
                    'subject' => $input['subject'],
                    'request_id' =>  $input['request_id']
                ]
            );
        } else {
            $thread = MessagesThread::create(
                [
                    'subject' => $input['subject']
                ]
            );
        }
        // Message
       $message =  Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
                'last_read' => new Carbon,
            ]
        );

        $files = $request->file('files');
        if($files[0]!=null)
        {
            FileUpload::saveMessageFiles($message->id,$files,$this->user->id,"files/");
        }
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        if($request->has("send_email"))
        {
            self::sendEmail($thread,"<p>Новое сообщение от пользователя: ".Sentinel::getUser()->email."</p>"."<p>".strip_tags($input['message'])."</p>",$thread->participants->pluck("user_id"));
        }
        return redirect('messages/outgoing')->with('success', 'Сообщение успешно отправлено!');
    }
    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update(Request $request ,$id)
    {
        $input = $request->all();
        try {
            $thread = MessagesThread::findOrFail($id);
            $threads = $this->user->threads()->pluck('threads.id')->toArray();
            if(!in_array($id,$threads))
            {
                abort(401);
            }

        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('messages');
        }
        $thread->activateAllParticipants();
        // Message
        $message = Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
                'body'      => Input::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Sentinel::getUser()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }

        $files = $request->file('files');
        if($files[0]!=null)
        {
            FileUpload::saveMessageFiles($message->id,$files,$this->user->id,"files/");
        }
        if($request->has("send_email"))
        {
            self::sendEmail($thread,"<p>Новое сообщение от пользователя: ".Sentinel::getUser()->email."</p>"."<p>".strip_tags($input['message'])."</p>",$thread->participants->pluck("user_id"));
        }
        return redirect('messages/' . $id);
    }
}