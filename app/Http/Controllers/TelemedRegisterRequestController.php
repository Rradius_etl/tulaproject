<?php

namespace App\Http\Controllers;

use App\Criteria\MedCareFacCriteria;
use App\Criteria\UserCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTelemedRegisterRequestRequest;
use App\Http\Requests\Admin\UpdateTelemedRegisterRequestRequest;
use App\Models\NotificationManager;
use App\Repositories\TelemedRegisterRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
/** Удаление, добавление, редактирование заявок на добавления ТМЦ админом ЛПУ */
class TelemedRegisterRequestController extends InfyOmBaseController
{
    /** @var  TelemedRegisterRequestRepository */
    private $telemedRegisterRequestRepository;

    public  $mcf;
    public function __construct(TelemedRegisterRequestRepository $telemedRegisterRequestRepo)
    {
        $this->middleware("auth");
        $this->middleware("med_care_fac_admin");
        $this->telemedRegisterRequestRepository = $telemedRegisterRequestRepo;
        $this->user = Sentinel::getUser();
        $crit = new MedCareFacCriteria();
        if($this->user) {
            $mcf = $this->user->medcarefacs;
            if($mcf != null)
            {
                $crit->setMedCareFac_id($mcf->id);
            }
            else
            {
                $crit->setMedCareFac_id(0);
            }
            $this->telemedRegisterRequestRepository->pushCriteria($crit);
        }


    }

    /**
     * Display a listing of the TelemedRegisterRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $this->telemedRegisterRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedRegisterRequests = $this->telemedRegisterRequestRepository->paginate(30);

        return view('telemed_register_requests.index',['search'=>$search])
            ->with('telemedRegisterRequests', $telemedRegisterRequests);
    }

    /**
     * Show the form for creating a new TelemedRegisterRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('telemed_register_requests.create');
    }

    /**
     * Store a newly created TelemedRegisterRequest in storage.
     *
     * @param CreateTelemedRegisterRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedRegisterRequestRequest $request)
    {
        $this->validate($request,
        [
            'name'=>'required',
            'address'=>'required',
            'director_fullname'=>'required',
            'coordinator_fullname'=>'required',
            'coordinator_phone'=>'required',
            'tech_specialist_contacts'=>'required',
            'tech_specialist_fullname'=>'required',
            'tech_specialist_phone'=>'required',
            'equipment_location'=>'required',
            'terminal_name'=>'required',
            'terminal_number'=>'required|integer'
        ]);
        $medcarefac_id = $this->user->medcarefacs;
        $input = $request->all();
        $input['med_care_fac_id'] = $medcarefac_id->id;
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->create($input);

        NotificationManager::createNotificationForAdmin("Администратор ЛПУ ".$medcarefac_id->name." - ".$this->user->email." создал заявку на регистацию ТМЦ по ссылке, чтобы посмотреть список заявок, перейдите по ссылке <a href='".route("admin.telemed-register-requests.index")."'>ссылка</a>.");

        Flash::success(trans('backend.TelemedRegisterRequest') . trans('backend.success_saved') . 'а');

        return redirect(route('telemed-register-requests.index'));
    }

    /**
     * Display the specified TelemedRegisterRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
            Flash::error(trans('backend.not_found') );

            return redirect(route('telemed-register-requests.index'));
        }

        return view('telemed_register_requests.show')->with('telemedRegisterRequest', $telemedRegisterRequest);
    }

    /**
     * Show the form for editing the specified TelemedRegisterRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
             Flash::error( trans('backend.not_found') );

            return redirect(route('telemed-register-requests.index'));
        }

        return view('telemed_register_requests.edit')->with('telemedRegisterRequest', $telemedRegisterRequest);
    }

    /**
     * Update the specified TelemedRegisterRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedRegisterRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedRegisterRequestRequest $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'address'=>'required',
                'director_fullname'=>'required',
                'coordinator_fullname'=>'required',
                'coordinator_phone'=>'required',
                'tech_specialist_contacts'=>'required',
                'tech_specialist_fullname'=>'required',
                'tech_specialist_phone'=>'required',
                'equipment_location'=>'required',
            ]);
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
             Flash::error(trans('backend.TelemedRegisterRequest') . trans('backend.not_found') );

            return redirect(route('telemed-register-requests.index'));
        }

        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->update($request->all(), $id);

       Flash::success(trans('backend.TelemedRegisterRequest') . trans('backend.success_updated'));

        return redirect(route('telemed-register-requests.index'));
    }

    /**
     * Remove the specified TelemedRegisterRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedRegisterRequest = $this->telemedRegisterRequestRepository->findWithoutFail($id);

        if (empty($telemedRegisterRequest)) {
            Flash::error(trans('backend.not_found') );

            return redirect(route('telemed-register-requests.index'));
        }

        $this->telemedRegisterRequestRepository->delete($id);

        Flash::success(trans('backend.success_deleted'));

        return redirect(route('telemed-register-requests.index'));
    }
}
