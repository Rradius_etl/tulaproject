<?php

namespace App\Http\Controllers;

use App\Criteria\DateRangeCriteria;
use App\Criteria\MedCareFacConsultRequestCriteria;
use App\Criteria\UIDCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\CreateTelemedConsultRequestRequest;
use App\Http\Requests\UpdateTelemedConsultRequestRequest;
use App\Models\Admin\File;
use App\Models\Admin\TelemedCenter;
use App\Models\Admin\TelemedConsultRequest;
use App\Repositories\TelemedConsultRequestRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use Session;
/** Все что связанное к спискам заявков на консультации со стороны админа ЛПУ */
class TelemedConsultRequestController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    public function __construct(TelemedConsultRequestRepository $telemedConsultRequestRepo)
    {
        $this->middleware(["auth",'med_care_fac_admin']);
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
        $this->user = Sentinel::getUser();
        $crit = new MedCareFacConsultRequestCriteria();

        $this->crit = $crit;

        $this->mcf = $this->user->medcarefacs;
        if(  $this->mcf != null)
        {
            $crit->setMedCareId($this->mcf->id);

        }
        else
        {
            $crit->setMedCareId(0);

        }
        $this->telemedConsultRequestRepository->pushCriteria($crit);
    }

    /**
     * Display a listing of the TelemedConsultRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $search = "";
        $canceled = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        $uidsearch = "";
        if($request->has('uidsearch'))
        {
            $uidsearch = $request->get('uidsearch');
        }
        if($request->has('canceled'))
        {
            $canceled = $request->get('canceled');
        }
        if($request->has('from')&&$request->has('to')) {
            $this->telemedConsultRequestRepository->pushCriteria(new DateRangeCriteria($request['from'], $request['to']));
        }


        $uidcrit = new UIDCriteria();
        $uidcrit->setParams("",$uidsearch);
        $this->telemedConsultRequestRepository->pushCriteria($uidcrit);
        $this->telemedConsultRequestRepository->pushCriteria(new RequestCriteria($request));
        $telemedConsultRequests = $this->telemedConsultRequestRepository->with('abonentSide')->paginate(30);

        return view('telemed_consult_requests.index',['search'=>$search,'uidsearch'=>$uidsearch,'canceled'=>$canceled])
            ->with('telemedConsultRequests', $telemedConsultRequests);
    }

    /**
     * Show the form for creating a new TelemedConsultRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('telemed_consult_requests.index');
    }

    /**
     * Store a newly created TelemedConsultRequest in storage.
     *
     * @param CreateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTelemedConsultRequestRequest $request)
    {
        $input = $request->all();

        $telemedConsultRequest = $this->telemedConsultRequestRepository->create($input);

        Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_saved'));

        return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    /**
     * Display the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->where('efp.type', 'abonent_side')
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();

        return view('telemed_consult_requests.show', compact('files'))->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Show the form for editing the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $this->tmcs = TelemedCenter::where(['med_care_fac_id'=>   $this->mcf->id])->where("id","!=",$telemedConsultRequest->request->telemed_center_id)->get();
        return view('telemed_consult_requests.edit',["tmcs"=>$this->tmcs->pluck('name','id')])->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Update the specified TelemedConsultRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');



        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);



        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $rules = [
            'telemed_center_id' => 'required|in:' . implode(',', $tmcs)."|not_in:".$telemedConsultRequest->request->telemed_center_id
        ];



        $this->validate($request, $rules);
        if($request->get("telemed_center_id") != $telemedConsultRequest->telemed_center_id)
        {
            $tmc = TelemedCenter::find($request->get("telemed_center_id"));
            $req = $telemedConsultRequest->request;
            \NotificationManager::createNotificationForTmcUsers("Ваша заявка на телемедицинскую консультацию - ".$telemedConsultRequest->getFullUid()." была перенаправлена на ТМЦ ".$tmc->name.", зайдите по <a href='". \URL::to("/abonent-telemed-consult-requests/".$req->id)."'>ссылке</a>",$req->initiator_id,false,true);
            $consside = $telemedConsultRequest->consultantSide;
            if($consside != null)
            {
                if($consside->delete())
                {
                    \NotificationManager::createNotificationForTmcUsers("Заявка на телемедицинскую консультацию - ".$telemedConsultRequest->getFullUid()." была перенаправлена на ТМЦ ".$tmc->name." администратором ЛПУ",$req->initiator_id,false,true);
                }
                
            }   
        }

        $telemedConsultRequest->telemed_center_id = $request->get("telemed_center_id");
        $telemedConsultRequest->tmc_at = Carbon::now();
        $telemedConsultRequest->telemed_notificated = 0;
        $telemedConsultRequest->update();

       Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_updated'));

        return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    /**
     * Remove the specified TelemedConsultRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedConsultRequest = TelemedConsultRequest::findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }

        $this->telemedConsultRequestRepository->delete($id);

        Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_deleted'));

        return redirect(route('telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }
}
