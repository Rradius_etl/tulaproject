<?php

namespace App\Http\Controllers;

use App\Models\Admin\Block;
use App\Models\Admin\ConsultationType;
use App\Models\Admin\IndicationForUse;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\MedicalProfile;
use App\Models\Admin\MedicalProfileCategory;
use App\Models\Admin\News;
use App\Models\Admin\Slide;
use App\Models\Admin\SocialStatus;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\Admin\TelemedRequest;
use App\Models\FileUpload;
use App\Models\TelemedCenter;
use App\Models\TelemedConsultAbonentSide;
use Carbon\Carbon;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class IndexController extends Controller
{
    /**Правила валидации абонентской формы на заявку на консультацию */
    private static $rules = [
        'request_id' => 'required',
        'doctor_fullname' => 'required',
        'med_care_fac_id' => 'required',
        'patient_birthday' => 'required|date_format:d.m.Y',
        'desired_date' => 'date_format:d.m.Y',
        'desired_time' => 'date_format:G:i',
        'patient_indications_for_use_other' => 'required_if:patient_indications_for_use_id,other',
        'patient_indications_for_use_id' => 'required',
        'consult_type' => 'required',
        'medical_profile' => 'required',
        'patient_uid_or_fname' => 'required',
        'coordinator_fullname' => 'required',
        'coordinator_phone' => 'required',
    ];
    public static $validateMessages = [
        'patient_birthday.date_format' => 'Укажите дату рождения пациента',
    ];
    public static $genders = [
        'male' => 'мужской',
        'female' => 'женский',
    ];


    public $consult_types;


    public function __construct()
    {
        $this->user = \Sentinel::getUser();
        $this->consult_types = ConsultationType::select('name', 'id')->get()->pluck('name', 'id');
    }

    public function index()
    {
        $additional_blocks = Block::orderBy('order', 'asc')->limit(15)->get();
        $news = News::published()->limit(5)->orderBy('created_at', 'desc')->get();
        $slides = Slide::where('active', true)->orderBy('order', 'asc')->limit(10)->get();
        $welcome_text = \Config::get('website-settings.welcome-text');
        return view('welcome', compact('news', 'slides', 'additional_blocks', 'welcome_text'));
    }

    /** Страница для выбора ТМЦ от которого создастся заявка на консультацию */
    public function chooseTMC()
    {
        return view("frontend.choose-tmc");
    }

    /** Форма для новой заявка на консультацию */
    public function newConsultation()
    {


        $ind_for_use = IndicationForUse::getOptionValues();

        if (\Session::has("success")) {
            return view('frontend.new-consult');
        } else {
            DB::beginTransaction();
            try {
                $socstatuses = SocialStatus::pluck('name', 'id');
                if (!isset($_GET['telemed_center_id'])) {
                    \Flash::error("Не хватает параметров!");
                    return redirect()->back();
                }
                $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');

                $tmc = TelemedCenter::find($_GET['telemed_center_id']);
                if ($tmc == null) {
                    \Flash::error("ТМЦ не найден!");
                    return redirect()->back();
                }

                if (!in_array($_GET['telemed_center_id'], $tmcs)) {

                    \Flash::error("Вы не являетесь участником ТМЦ " . $tmc->name);
                    return redirect()->back();
                }
                $mfc = $tmc->medCareFac;
                $req_id = TelemedConsultRequest::where(['uid_code' => $mfc->code, 'uid_year' => date('Y', time())])->whereExists(function ($query) use ($tmc) {
                    $query->select(DB::raw("1"))->from("telemed_event_requests as ter")
                        ->where("ter.telemed_center_id", $tmc->id)
                        ->where("ter.id", "=", \DB::raw('telemed_consult_requests.request_id'))
                        ->where('ter.canceled', 0);
                })->orderBy("uid_id", "desc")->first();
                if ($req_id == null) {
                    $req = new TelemedRequest();
                    $req->event_type = "consultation";
                    $req->initiator_id = $this->user->id;
                    $req->telemed_center_id = $tmc->id;
                    $req->save();
                    $cons = new TelemedConsultRequest();
                    $cons->request_id = $req->id;
                    $cons->uid_year = date('Y', time());
                    $cons->uid_code = $mfc->code;
                    $uid = TelemedConsultRequest::where(['uid_year' => Date('Y', time()), 'uid_code' => $mfc->code])->orderBy("uid_id", "desc")->first();
                    if ($uid != null) {
                        $cons->uid_id = $uid->uid_id + 1;
                        $id = $cons->uid_id;
                    } else {
                        $cons->uid_id = 1;
                        $id = $cons->uid_id;
                    }
                    $cons->save();

                } else {

                    if ($req_id->abonentSide != null) {
                        $req = new TelemedRequest();
                        $req->event_type = "consultation";
                        $req->initiator_id = $this->user->id;
                        $req->telemed_center_id = $tmc->id;
                        $req->save();
                        $cons = new TelemedConsultRequest();
                        $cons->request_id = $req->id;
                        $cons->uid_year = date('Y', time());
                        $cons->uid_code = $mfc->code;
                        $uid = TelemedConsultRequest::where(['uid_year' => Date('Y', time()), 'uid_code' => $mfc->code])->orderBy("uid_id", "desc")->first();
                        if ($uid != null) {
                            $cons->uid_id = $uid->uid_id + 1;
                            $id = $cons->uid_id;
                        } else {
                            $cons->uid_id = 1;
                            $id = $cons->uid_id;
                        }
                        $cons->save();
                    } else {
                        $id = $req_id->uid_id;
                        $req = $req_id->request;
                        $req_id->created_at = Carbon::now();
                        $req_id->save();

                    }

                }
                DB::commit();
                $mcfs = MedCareFac::where('id', '!=', $mfc->id)->pluck("name", "id");
                $genders = self::$genders;
                $consult_types = $this->consult_types;


                $medical_profiles_model = MedicalProfile::with('medicalProfileCategory')->get();
                $medical_profile_categories = MedicalProfileCategory::all()->lists('name', 'id');

                foreach ($medical_profiles_model as $item) {
                    $key = $medical_profile_categories[$item->category_id];
                    $medical_profiles[$key][$item->id] = $item->name;
                }


                return view('frontend.new-consult', compact('genders', 'medical_profiles', 'consult_types', 'req', 'socstatuses', 'tmc', 'id', 'mfc', 'ind_for_use', 'mcfs'));
            } catch (\Exception $e) {
                return $e;
                DB::rollBack();
            }
        }
    }

    /** Сохранение формы заявки на консультацию */
    public function addRequest(Request $request)
    {

        $validator = \Validator::make($request->all(), self::$rules, self::$validateMessages);

        if ($validator->fails()) {
            //return 1;
            if ($request->ajax()) {
                return \Response::json(['errors' => $validator->messages()], 422);
            } else {
                return \Redirect::back()->withErrors($validator->messages())->withInput();
            }

        } else {
            DB::beginTransaction();
            try {

                $input = $request->all();
                $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
                $input['patient_birthday'] = Carbon::createFromFormat('d.m.Y', $input['patient_birthday'])->toDateString();

                if (isset($input['desired_date']) && $input['desired_date'] != '') {
                    $input['desired_date'] = Carbon::createFromFormat('d.m.Y', $input['desired_date'])->toDateString();
                }

                $req_abon = new TelemedConsultAbonentSide($input);
                $req = \App\Models\TelemedConsultRequest::where(['request_id' => $input['request_id']])->first();
                if ($req == null) {
                    abort(404);
                }
                $curtmc = TelemedCenter::withTrashed()->find($req->request->telemed_center_id);
                $validator->after(function ($valid) use ($tmcs, $req, $curtmc) {
                    if (!in_array($req->request->telemed_center_id, $tmcs)) {
                        $valid->errors()->add('TMC', 'Вы не являетесь участником ТМЦ "' . $curtmc->name . '"');
                    }
                    if ($req->canceled == 1) {
                        $valid->errors()->add('TMC', "Это событие уже отменено");
                    }
                });
                if ($validator->fails()) {
                    //return 1;
                    if ($request->ajax()) {
                        return \Response::json(['errors' => $validator->messages()], 422);
                    } else {
                        return \Redirect::back()->withErrors($validator->messages())->withInput();
                    }

                }
                $req->med_care_fac_id = $input['med_care_fac_id'];
                $req->telemed_center_id = null;
                $req->created_at = Carbon::now();
                $req->update();
                $req_abon->telemed_center_id = $curtmc->id;
                $req_abon->save();
                DB::commit();
                $msg = 'Заявка на телемедицинскую клиническую консультацию принята. Вы будете уведомлены, когда ЛПУ-консультант рассмотрит Вашу заявку';

                $req_url = \URL::to("/telemed-consult-requests/" . $req->request_id);
                $message = "Пользователь " . $curtmc->name . " создал заявку на консультацию в ваше ЛПУ, зайдите по <a href='{$req_url}'>ссылке</a>";
                $mcfadmin = MedCareFac::find($input['med_care_fac_id']);
                if ($mcfadmin != null) {
                    if ($mcfadmin->user_id != null) {
                        \NotificationManager::createNotificationFromTMC($message, [$mcfadmin->user_id], $req->request->telemed_center_id, true, true);
                    }
                }
                $req_url = \URL::to("/tmc-telemed-consult-requests/" . $req->request_id);
                $message = "Пользователь " . $curtmc->name . " создал заявку на консультацию в ваше ЛПУ, зайдите по <a href='{$req_url}'>ссылке</a>";
                \NotificationManager::createNotificationFromTMCtoMCF($message, $mcfadmin->id, false, $req->request->telemed_center_id, true);

                /* Загрузка файлов */
                if ($request->hasFile('file')) {
                    FileUpload::saveAbonentSideFile($req->request->id, $request->file('file'), $this->user->id, "files/");
                }
                /* ./Загрузка файлов */

                if ($request->ajax()) {
                    return response()->json(["success" => 1, 'message' => $msg]);
                } else {

                    \Flash::success($msg);
                    return \Redirect::route('abonent-telemed-consult-requests.index', ['orderBy' => 'created_at', 'sortedBy' => 'desc']);
                }
            } catch (\Exception $e) {
                $debug = config('app.debug');
                DB::rollback();
                if ($debug == true) {
                    return response()->json(['errors' => [
                        'error' => $e->getMessage(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile(),
                    ]
                    ], 422);
                }
                if ($request->ajax()) {
                    return response()->json(
                        ['errors' =>
                            [
                                'error' => "Что то пошло не так"
                            ]
                        ], 422);
                } else {
                    abort(500);
                }
            }
        }

    }


    /** Функция не используется */
    public function confirmConsultation()
    {

        $doctors[] = 'sdasdasdas dasdasdasdas';
        $doctors[] = 'sdasdasdas dasdasdasdas';
        $doctors[] = 'sdasdasdas dasdasdasdas';
        $doctors[] = 'sdasdasdas dasdasdasdas';
        $doctors[] = 'sdasdasdas dasdasdasdas';

        $durations = [
            60 => "1 часа",
            120 => "2 часа",
            180 => "3 часа",
            240 => "4 часа",
            300 => "5 часов",
            360 => "6 часов",
        ];
        return view('frontend.confirm-consult', compact('doctors', 'durations'));
    }

    /** Функция не используется */
    public function confirmConsultationProcess($id, Request $request)
    {
        return response()->json($request);
    }
}

