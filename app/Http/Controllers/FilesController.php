<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Admin\File;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = \Sentinel::getUser();
    }
    /** Получить фаил по идентификатору */
    public function getFile($file_id)
    {

        $file = File::find($file_id);
        if ($file != null) {

            $can = false;
            if(\Sentinel::inRole('admin'))
            {
                $can = true;
            }
            else {
                if ($this->user->id == $file->user_id) {
                    $can = true;
                } else {
                    $tmcs = array_pluck(\TmcRoleManager::getTmcs(), "id");
                    $check_for_tmc = \DB::table('files')->join("event_file_pivots as e", "e.file_id", "=", "files.id")->where(['files.id' => $file_id])
                        ->where(function ($query) use ($tmcs) {
                            $query->whereExists(function ($q) use ($tmcs) {
                                $q->select('*')->from("telemed_event_participants as tep")->where(['e.request_id' => "tep.request_id"])->whereIn("tep.telemed_center_id", $tmcs);
                            });
                            $query->orWhere(['files.user_id' => $this->user->id]);
                            $query->orWhere(function ($q1) use ($tmcs) {
                                $q1->whereExists(function ($q) use ($tmcs) {
                                    $q->select('*')->from("telemed_consult_requests as tcp")->where(['tcp.request_id' => \DB::raw("e.request_id")])->whereIn("tcp.telemed_center_id", $tmcs);
                                });
                            });
                            $query->orWhere(function ($q) use ($tmcs) {
                                $q->whereExists(function ($q2) use ($tmcs) {
                                    $q2->select('*')->from("telemed_event_requests as ter")->whereIn("ter.telemed_center_id", $tmcs)->where(['e.request_id' => \DB::raw("ter.id")]);
                                });
                            });
                        })->count();
                    if ($check_for_tmc > 0) {
                        $can = true;
                    } else {
                        $check_for_message = \DB::table('files')->join("message_file_pivots as e", "e.file_id", "=", "files.id")->join("messages as m", "e.message_id", "=", "m.id")->where(['files.id' => $file_id])
                            ->where(function ($query) use ($tmcs) {
                                $query->where(['files.user_id' => $this->user->id]);
                                $query->orWhere(function ($q1) use ($tmcs) {
                                    $q1->whereExists(function ($q) use ($tmcs) {
                                        $q->select('*')->from("participants as part")->where(['part.thread_id' => \DB::raw("m.thread_id"), "part.user_id" => $this->user->id]);
                                    });
                                });
                            })->count();
                        if ($check_for_message > 0) {
                            $can = true;
                        }
                    }
                }
            }

            if (!$can) {
                abort(401, "Нету доступа");
            }
            if (file_exists(storage_path() . "/" . $file->file_path)) {
                $path = str_replace("/", "\\", $file->file_path);
                $file->downloads_count = $file->downloads_count + 1;
                $file->update();
                return response()->download(storage_path() . "/" . $file->file_path, $file->original_name);
            } else {
                abort(404, "Фаил был удален");
            }
        } else {
            abort(404, "такого файла не существует");
        }
    }

    /**
     * Show uploaded files of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploaded()
    {
        $files = $this->user->uploadedFiles()->paginate(20);
        //return response()->json($files);
        return view('files.uploaded', compact('files'));

    }

    /** Проверить доступ по ссылке */
    public function getAccessStatus($file_id) {
        $token = File::find($file_id)->accessToken()->active()->first();
        if( count($token)) {
            return response()->json([
                'message' => 'Доступ по ссылке включен. <br> Теперь доступ к этому файлу имеют все у кого есть ссылка',
                'url'   => \URL::to('openfile/' . $token->hash),
                'state' => $token->state
            ]);
        } else {
            return response()->json(['message' => 'Доступ по ссылке отключен.', 'state' => false ]);
        }
    }

    /** Создать доступ по ссылке */
    public function enableAccessByLink($file_id)
    {
        $file = File::find($file_id);
        $token = $file->addAccessByLink();
        if( $token ) {
            return response()->json([
                'message' => 'Доступ по ссылке включен. <br> Теперь доступ к этому файлу имеют все у кого есть ссылка',
                'url'   => \URL::to('openfile/' . $token->hash),
                'state' => $token->state
            ]);
        } else {
            return response()->json(['message' => 'Такого файла не существует', 'state' => false],406);
        }


    }

    /** Деактивация доступа по ссылке */
    public function disableAccessByLink($file_id)
    {
        $file = File::find($file_id);

        if( count($file)) {
            $token = $file->disableAccessLink();
            if( $token ) {
                return response()->json(['message' => 'Доступ по ссылке отключен. (Конфиденциальный)', 'state' => $token->state]);
            } else {
                return response()->json(['message' => 'Такого файла не существует', 'state' => false ],406);
            }

        } else {
            return abort(404);
        }

    }

    /** Доступные фаилы */
    public function availableFiles()
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        $user = $this->user;
        $messagefiles = File::leftJoin("message_file_pivots as mfp", "mfp.file_id", "=", "files.id")->leftJoin("messages as m", "mfp.message_id", "=", "m.id")->leftJoin("event_file_pivots as e", "e.file_id", "=", "files.id")
                ->where(function ($query) use ($tmcs) {
                    $query->whereExists(function ($q) use ($tmcs) {
                        $q->select('*')->from("participants as part")->where(['part.thread_id' => \DB::raw("m.thread_id"), "part.user_id" => $this->user->id]);
                    });
                    $query->orWhere(function($q2) use($tmcs)
                    {
                        $q2->whereExists(function ($q) use ($tmcs) {
                            $q->select('*')->from("telemed_event_participants as tep")->where(['e.request_id' => "tep.request_id"])->whereIn("tep.telemed_center_id", $tmcs);
                        });
                    });
                    /*$query->orWhere(['files.user_id' => $this->user->id]);*/
                    $query->orWhere(function ($q1) use ($tmcs) {
                        $q1->whereExists(function ($q) use ($tmcs) {
                            $q->select('*')->from("telemed_consult_requests as tcp")->where(['tcp.request_id' => \DB::raw("e.request_id")])->whereIn("tcp.telemed_center_id", $tmcs);
                        });
                });
                $query->orWhere(function ($q) use ($tmcs) {
                    $q->whereExists(function ($q2) use ($tmcs) {
                        $q2->select('*')->from("telemed_event_requests as ter")->whereIn("ter.telemed_center_id", $tmcs)->where(['e.request_id' => \DB::raw("ter.id")]);
                    });
                });
                $query->orWhere(function ($q) use ($tmcs) {
                   $q->where("files.is_public","=",true);
                });
            })->select("files.*")->paginate(30);

        $files = $messagefiles;
        return view('files.available', compact('files'));


    }
}
