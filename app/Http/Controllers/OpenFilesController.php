<?php

namespace App\Http\Controllers;

use App\Models\Admin\File;
use Illuminate\Http\Request;

use App\Http\Requests;

class OpenFilesController extends Controller
{


    public function index(Request $request)
    {
        $files = File::with('activeAccessToken')->has('activeAccessToken');
        if($request->has('search')) {
            $files->where('original_name', 'like', "%{$request->get('search')}%");
        }
        $files = $files->paginate(25);
        return view('files.open', compact('files'));
    }

    public function getOpenFile($hash)
    {

        $file = File::getByHash($hash);
        //return response()->json($file);
        if (count($file)) {
            $file->increment('downloads_count');

            $exists = \Storage::disk('storage')->exists( $file->file_path);
            //return response()->json($exists);

            if($exists) {
                return response()->download(storage_path() . "/" . $file->file_path, $file->original_name);
            } else {
                return abort(404, "Файл {$file->original_name} не найден");
            }



        } else {
            return abort(404, 'Файл не найден');
        }

        //return File::find(28)->addAccessByLink();
        // return File::find(28)->disableAccessLink();
    }
}
