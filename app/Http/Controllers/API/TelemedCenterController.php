<?php
/**
 * Created by PhpStorm.
 * User: seerd
 * Date: 01.12.2016
 * Time: 20:05
 */

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Models\Admin\MedCareFac;
use App\Models\TelemedCenter;
use Illuminate\Http\Request;
class TelemedCenterController extends  Controller
{

    /** Получить список ЛПУ или ЛПУ с привязанными ТМЦ **/
    public function index(Request $request)
    {
        if( $request->has('type') && $request->get('type') == 'short') {
            $medCareFacs = MedCareFac::limit(999)->get();
        } else {
            $medCareFacs = MedCareFac::with('telemedCenters')->limit(999)->get();
        }

        return response()->json($medCareFacs);
    }

    /** Получить список ТМЦ с названием его ЛПУ **/
    public function getList()
    {
        $tmc =\DB::table('telemed_centers')
            ->join('med_care_facs', 'med_care_facs.id', '=', 'telemed_centers.med_care_fac_id')
            ->select('telemed_centers.id','telemed_centers.name','med_care_facs.name as mcf_name')
            ->whereNull('telemed_centers.deleted_at')
            ->orderBy('mcf_name')->get();

        return response()->json($tmc);
    }
 
}