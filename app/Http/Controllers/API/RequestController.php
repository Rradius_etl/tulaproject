<?php

namespace App\Http\Controllers\API;

use App\Models\Admin\ConsultationType;
use App\Models\Admin\DeliveryGroup;
use App\Models\Admin\EventConclusion;
use App\Models\Admin\EventFilePivot;
use App\Models\Admin\File;
use App\Models\Admin\IndicationForUse;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\MedicalProfile;
use App\Models\Admin\MedicalProfileCategory;
use App\Models\Admin\SocialStatus;
use App\Models\Admin\TelemedConsultAbonentSide;
use App\Models\Admin\TelemedConsultConsultantSide;
use App\Models\Admin\TelemedEventParticipant;
use App\Models\Admin\TelemedRequest;
use App\Models\Admin\TelemedUserPivot;
use App\Models\ApiModel;
use App\Models\ConsultPattern;
use App\Models\Doctor;
use App\Models\FileUpload;
use App\Models\Invitation;
use App\Models\NotificationManager;
use App\Models\TelemedCenter;
use App\Models\TelemedConsultRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use \Illuminate\Support\Facades\Request as Req;
class RequestController extends Controller
{

    public function __construct()
    {
        $this->user = Sentinel::getUser();
        $this->middleware(["auth", "is_user_of_telemed"]);
    }


    private static $abonent_rules = [
        'data.doctor_fullname' => 'required',
        'data.med_care_fac_id' => 'required',
        'data.patient_birthday' => 'required|date_format:d.m.Y',
        'data.desired_date' => 'date_format:d.m.Y',
        'data.desired_time' => 'date_format:G:i',
        'data.patient_indications_for_use_other' => 'required_if:patient_indications_for_use_id,other',
        'data.patient_indications_for_use_id' => 'required',
        'data.consult_type' => 'required',
        'data.medical_profile' => 'required',
        'data.patient_uid_or_fname' => 'required',
        'data.coordinator_fullname' => 'required',
        'data.coordinator_phone' => 'required',
    ];
    private static $messages = [
        'data.med_care_fac_id.required' => 'Обязательно выберите ЛПУ-консультанта'
    ];
    private static $consultant_rules = [
        'data.planned_date' => 'required_with:data.confirm|Date|after:',
        'data.responsible_person_fullname' => 'required_with:data.confirm',
        'data.doctor_id' => 'required_with:data.confirm',
    ];

    private static $consult_messages = [
        'data.planned_date.required_with' => 'Планируемые дата и время проведения консультации не заполнены',
        'data.responsible_person_fullname.required_with' => 'Ответственное лицо, принявшего заявку не указан',
        'data.doctor_id.required_with' => 'Врач-консультант не назначен',
    ];

    /**
     * Редактирование абонентской части заявки на консультацию.
     *
     * @param  int $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function editAbonentSide($id, Request $request)
    {

        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), "id");

        $validator = Validator::make($request->all(), self::$abonent_rules, self::$messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        } else {
            $telemedConsultRequest = TelemedConsultAbonentSide::where(["request_id" => $id])->whereIn("telemed_center_id", $tmcs)->first();
            if($telemedConsultRequest->request->telemedConclusion != null)
            {
                return response()->json(['errors'=>['error'=>['вы не можете редактировать консультацию с написанным заключением']]],422);
            }
            if($telemedConsultRequest->cons_request->decision != "pending")
            {
                return response()->json(['errors'=>['error'=>['вы не можете редактировать консультацию, так как консультант уже принял решение']]],422);
            }
            if (empty($telemedConsultRequest)) {
                abort(404);
            }
            if ($telemedConsultRequest->request->decision != "pending" || $telemedConsultRequest->request->canceled == 1) {
                return response()->json([
                    'errors' => ["error" =>
                        [
                            "Вы не можете редактировать данную запись, так как консультант уже принял решение"
                        ]
                    ]
                ], 422);
            }
         
            $input = $request->all();

            $input = $input['data'];

            $input['desired_date'] = \Carbon\Carbon::createFromFormat('d.m.Y', $input['desired_date'])->toDateTimeString();
            $input['patient_birthday'] = \Carbon\Carbon::createFromFormat('d.m.Y', $input['patient_birthday'])->toDateTimeString();
            if ($telemedConsultRequest->cons_request->med_care_fac_id != $input['med_care_fac_id']) {
                return response()->json([
                    'errors' => ["error" =>
                        [
                            "Вы не можете редактировать поле ЛПУ-консультанта, так как админ ЛПУ уже направил на запрос на рассмотрение своему ТМЦ"
                        ]
                    ]
                ], 422);
            }

            if($telemedConsultRequest->update($input))
            {

            }

            if( $request->hasFile('file')) {
                FileUpload::saveAbonentSideFile($id, $request['file'], $this->user->id, "files/");
            }
            return response()->json(["success" => true, 'files' => $request->all(), 'debug' => count($request['file']), 'message' => 'Изменения успешно сохранены']);
        }
    }

    /** Редактирование заключения по определенному событию **/
    public function updateConclusion($id, Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        $event = TelemedRequest::findOrFail($id);
        if(!in_array($event->telemed_center_id,$tmcs))
        {
            return response()->json(['error'=>'у вас нет прав редактировать этот раздел!'],401);
        }
        $cons = $event->telemedConclusion;
        if($cons != null)
        {
        }
        else
        {
            $cons = new EventConclusion($request->all());
            $cons->request_id = $id;
        }
        if($cons->save())
        {
            $files = $request->file('files');
            if ($files[0] != null) {
                FileUpload::saveConclusionFiles($id, $files, $this->user->id, "files/");
            }

            if (isset($input['deletedfiles'])) {
                $filesToDelete = $input['deletedfiles'];
                foreach ($filesToDelete as $v) {
                    FileUpload::removePivot($id, $v);
                }
            }
        }
    }

    /** Получить заключение по определенному событию **/
    public function getConclusion($id)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');

        $conc = EventConclusion::findOrFail($id);

        return response()->json(['conclusion'=>$conc]);
    }

    /** Добавить заключение к определенному событию **/
    public function createConclusion($id,Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        $event = TelemedRequest::findOrFail($id);

        //return response()->json(  [$event->start_date, Carbon::now()->toDateTimeString(), Carbon::now()->diffInSeconds(New Carbon($event->start_date),false) ], 422 );
        if($event->canceled == 1)
        {
            return response()->json(['error'=>['Вы не можете создать заключение по отмененной заявке!']],422);
        }
        if($event->start_date == null)
        {
            return response()->json(['error'=>['Укажите время события!']],422);
        }
        if(Carbon::now()->diffInSeconds(New Carbon($event->start_date),false) > 0)
        {
            return response()->json(['error'=>['Вы не можете создать заключение по заявке, которая еще не прошла по времени ее начала!'],'start_date' => New Carbon($event->start_date), 'now' => Carbon::now()],422);
        }
        if($event->event_type == 'consultation')
        {
            $event_cons = $event->getConsultation;
            if($event_cons!= null)
            {
                if(!in_array($event_cons->telemed_center_id,$tmcs))
                {
                    return response()->json(['error'=> ['у вас нет прав редактировать этот раздел!']],401);
                }
                if($event_cons->decision != 'agreed')
                {
                    return response()->json(['error'=>['Вы не можете создать заключение по несогласованной заявке!']],422);
                }
                if($event->telemedRequestsMcfAbonents == null)
                {
                    return response()->json(['error'=>['Абонент не заполнил свою форму!']],422);
                }
                if($event->telemedRequestsMcfConsultants == null)
                {
                    return response()->json(['error'=>['Консультант не заполнил свою форму!']],422);
                }
            }
            else
            {
                return response()->json(['error'=>['not found']],404);
            }
        }
        else
        {
            if(!in_array($event->telemed_center_id,$tmcs))
            {
                return response()->json(['error'=>['у вас нет прав редактировать этот раздел!']],401);
            }
            if(count($event->telemedEventParticipants) == 0)
            {
                return response()->json(['error'=>['Ни один ТМЦ не принял ваше приглашение на '.trans('backend.'.$event->event_type)]],422);
            }
        }
        if($event->telemedConclusion != null)
        {
            return response()->json(['error'=> ['данное мероприятия уже имеет заключение'] ],422);
        }
        $cons = new EventConclusion($request->all());
        $cons->request_id = $id;
        $cons->user_id = Sentinel::getUser()->id;
        if($cons->save())
        {
            $files = $request->file('files');
            if ($files[0] != null) {
                FileUpload::saveConclusionFiles($id, $files, $this->user->id, "files/");
            }

            return response()->json(["success" => true, 'message' => 'Ваше заключение успешно сохранено']);
        } else {
            return response()->json(['error'=>['Что то пошло не так, попробуйте повторить позднее']],500);
        }
    }

    /** Редактирования части консультанта по определенной заявке на консультацию **/
    public function editConsultSide($id, Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), "id");

        $consultant_rules = [
            'data.planned_date' => 'required_with:data.confirm|Date|after:' . Carbon::now(),
            'data.appointed_person_id' => 'required_with:data.confirm',
            'data.coordinator_fullname' => 'required',
            'data.doctor_id' => 'required_with:data.confirm',
        ];
        $validator = Validator::make($request->all(), $consultant_rules, self::$consult_messages);

        $input = $request->all();
        $input = $input['data'];

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        } else {
            $tmcs = array_pluck(\TmcRoleManager::getTmcs(), "id");

            $telemedConsultRequest = TelemedConsultRequest::where(["request_id" => $id])->whereIn("telemed_center_id", $tmcs)->first();
            if($telemedConsultRequest->request->telemedConclusion != null)
            {
                return response()->json(['errors'=>['error'=>['вы не можете редактировать консультацию с написанным заключением']]],422);
            }
            if (empty($telemedConsultRequest)) {
                abort(404);
            }
            $consultside = $telemedConsultRequest->consultantSide;

            if (empty($consultside)) {
                $consultside = new TelemedConsultConsultantSide($input);
                $consultside->request_id = $id;

            } else {
                if ($telemedConsultRequest->request->canceled == 1) {
                    return \Response::json([
                        'errors' => ["error" =>
                            [
                                "Вы не можете редактировать данную запись, так как вы уже приняли решение отменить консультацию"
                            ]
                        ]
                    ], 422);
                }
            }
            $secondValidator = Validator::make($request->all(), []);

            $secondValidator->after(function ($secondValidator) use ($telemedConsultRequest, $request, $input) {

                if (isset($input['confirm'])) {
                    $error = ApiModel::checkTimeConsultWithError($telemedConsultRequest->request->telemed_center_id, $telemedConsultRequest->telemed_center_id, $input['planned_date'], "Y-m-d H:i", $telemedConsultRequest->request_id);
                    if ($error != 1) {
                        $secondValidator->errors()->add("planned_time", $error);
                    }
                }

                if(isset($input['appointed_person_id']))
                {

                    $tmcusers = TelemedUserPivot::where('telemed_center_id',$telemedConsultRequest->telemed_center_id)->with('user')->get();
                    $usersarray = [];
                    $telemed = $telemedConsultRequest->telemedCenter;
                    $mcfadmin = $telemed->medCareFac->user;
                    if($mcfadmin != null)
                    {
                        array_push($usersarray,$mcfadmin->id);
                    }
                    foreach($tmcusers as $user)
                    {
                        array_push($usersarray,$user->id);
                    }

                    if( ctype_digit($input['appointed_person_id'])) {
                        if(!in_array($input['appointed_person_id'],$usersarray))
                        {
                            $secondValidator->errors()->add("appointed_person_id","Вы пытаетесь привязать пользователя к полю(ФИО и должность ответственного лица, принявшего заявку ) непривязанному к вашему ТМЦ!");
                        }
                    }


                }



            });

            if ($secondValidator->fails()) {
                return response()->json(['errors' => $secondValidator->errors()], 422);
            } else {

                try {
                    \DB::beginTransaction();


                    $req = TelemedRequest::find($consultside->request_id);
                    if (isset($input['confirm'])) {

                        $message = 'Заявка успешно согласована';
                        $consultside->planned_date = $input['planned_date'];
                        $consultside->doctor_id = $input['doctor_id'];

                        if( !ctype_digit($input['appointed_person_id'])) {
                            /** Если текстовый запись то присваиваем NULL, а значение сохраняем в другое поле appointed_person */

                            $input['appointed_person'] = $input['appointed_person_id'];
                            $input['appointed_person_id'] = null;
                        }
                        $consultside->update($input);

                        $telemedConsultRequest->decision = "agreed";
                        $req->decision = "agreed";



                        $startdate = new Carbon($input['planned_date']);
                        $enddate = new Carbon($input['planned_date']);
                        $enddate->addHour();
                        $req->start_date = $startdate;
                        $req->end_date = $enddate;
                        $dec  = "принял";

                    } else if (isset($input['reject'])) {
                        $dec  = "отклонил";
                        $message = 'Заявка успешно отклонена';

                        $telemedConsultRequest->decision = "rejected";
                        $req->decision = "rejected";
                    }
                    if (isset($input['comment'])) {
                        $telemedConsultRequest->comment = $input['comment'];


                    }

                    $req->update();
                    $telemedConsultRequest->update();

                    $notificationMessage = "Пользователь " . $telemedConsultRequest->telemedCenter->name." ".$dec." вашу  заявку на консультацию в ваше ЛПУ, зайдите по <a href='" . \URL::to("/abonent-telemed-consult-requests/".$req->id) ."'>ссылке</a>";
                    \NotificationManager::createNotificationFromTMCtoTMC($notificationMessage, $req->telemed_center_id, false, $telemedConsultRequest->telemed_center_id, false);

                    if (isset($input['deletedfiles'])) {
                        $filesToDelete = $input['deletedfiles'];
                        foreach ($filesToDelete as $v) {
                            FileUpload::removePivot($id, $v);
                        }
                    }
                    $files = $request->file('file');
                    if ($files[0] != null) {
                        FileUpload::saveFiles($id, $files, $this->user->id, "files/");
                    }
                    \DB::commit();
                    return response()->json(["success" => true, "message" => $message]);
                } catch (\Exception $e) {
                    \DB::rollBack();
                    return response($e, 500);
                }

            }


        }
    }

    /**
     *  Получить список заявок всех типов или по выбору;
     ***/
    public function index(Request $request)
    {
        $rules = [
            'tmc_id' => "numeric",
            'event_type' => "in:meeting,consultation,seminar",
            'month' => "required|integer|between:1,12",
            'year' => "required|integer|between:2016,9999",
        ];
        $this->validate($request, $rules);
        $res = [];
        if ($request->has('my_calendar')) {
            $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
            if ($request->has('telemed_id') && is_numeric($request->get('telemed_id')))
            {

              if(!in_array($request->get('telemed_id'),$tmcs))
              {
                  abort(401);
              }
              else
              {

                  $res = ApiModel::getMyTMCEvents([$request->get('telemed_id')], ["year" => $request->get("year"), "month" => $request->get("month")]);
              }

            }
            else
            {
                $res = ApiModel::getMyTMCEvents($tmcs, ["year" => $request->get("year"), "month" => $request->get("month")]);
            }
        }
        else  if ($request->has('tmc_id') && is_numeric($request->get('tmc_id'))) {
            $id = $request->get('tmc_id');
            $res = ApiModel::getTMCEvents($id, ["year" => $request->get("year"), "month" => $request->get("month")]);
        }
        return response()->json($res);
    }

    public function view($id, $type = 'event', Request $request)
    {
        if ($type == 'event') {
            $event = TelemedRequest::with(['telemedEventParticipants.telemedCenter.medCareFac', 'user'])->find($id);
        } else {
            $event = TelemedConsultRequest::with(['consultantSide', 'abonentSide', 'request.telemedCenter', 'medCareFac', 'request.telemedEventParticipants.telemedCenter.medCareFac'])->find($id);
        }
        return response()->json($event);
    }

    /** Получить список докторов по определнному ТМЦ **/
    public function doctors($id)
    {
        $doctors = Doctor::where('telemed_center_id', $id)
            ->select(\DB::raw('CONCAT(doctors.surname, " ", doctors.name, " ", doctors.middlename, " - ", doctors.spec ) as name, doctors.id'))->get();
        return response()->json($doctors->prepend(['id' => '', 'name' => '--- Выберите врача ---']));
    }

    /** Получить список ТМЦ текущего пользователя **/
    public function getTmcs()
    {
        return response()->json(["tmcs" => \TmcRoleManager::getTmcs()]);
    }

    /** Получить список всех ЛПУ **/
    public function getAllMcfs()
    {
        $mcfs = MedCareFac::select('name', 'id')->get();
        return response()->json(["mcfs" => $mcfs]);
    }

    /**Получить список всех ТМЦ **/
    public function getAllTmcs()
    {
        return response()->json(["tmcs" => TelemedCenter::select('id', 'name')->get()]);
    }

    /** Получить все опции по показанию к применению **/
    public function getIndicationsForUse()
    {
        return response()->json(IndicationForUse::getOptionValues(true));
    }


    /** Получение начальных данных для предваритеьлного заполнения */
    public function getConsultationDetails(Request $request)
    {

        $medical_profiles = \DB::table('medical_profiles')
            ->join('medical_profile_categories', 'medical_profiles.category_id', '=', 'medical_profile_categories.id')
            ->select('medical_profiles.id', 'medical_profiles.name', 'medical_profile_categories.name as category_name')
            ->limit('999')
            ->get();
         

        /* Соц статусы */
        $social_statuses = SocialStatus::select('name', 'id')->limit('999')->get();

        /* Все телемедцентры юзера */
        $tmcs = \TmcRoleManager::getTmcs();

        if($request->has('telemed_center_id'))
        {
            if(!is_int((int) $request->get('telemed_center_id')))
            {
                return response()->json(['error'=>['error'=>"неправильный формат параметра!"]],422);
            }
            $tmc_id = $request->get('telemed_center_id');
            if(!in_array($tmc_id,array_pluck($tmcs,'id')))
            {
                return response()->json(['error'=>['error'=>"вы не являетесь участников этого ТМЦ!"]],422);
            }
        }
        else
        {
            return response()->json(['error'=>['error'=>"не хватает параметров!"]],422);
        }
        /* Первый телемд центр*/
        $tmc = TelemedCenter::findOrFail($tmc_id);

        $mcf = $tmc->medCareFac;

        /* ЛПУ */
        $mcf = $tmc->medCareFac;


        /* Учреждения  */
        $mcfs = MedCareFac::where('id', '!=', $mcf->id)->select('name', 'id')->get();

        if ($request->type == 'short') {
            return response()->json(compact('medical_profiles', 'social_statuses', 'mcfs'));
        } else {
            $id = TelemedConsultRequest::where(['uid_year' => Date('Y', time()), 'uid_code' => $mcf->code])
                ->whereExists(function($query) use ($tmc)
                {
                    $query->select(\DB::raw("1"))->from("telemed_event_requests as ter")
                        ->where("ter.telemed_center_id",$tmc->id)
                        ->where("ter.id", "=", \DB::raw('telemed_consult_requests.request_id'))
                        ->where('ter.canceled',0);
                })
                ->orderBy("uid_id", "desc")->first();
            if ($id != null) {
                if ($id->abonentSide != null) {
                    $req = new TelemedRequest();
                    $req->event_type = "consultation";
                    $req->initiator_id = $this->user->id;
                    $req->telemed_center_id = $tmc->id;
                    $req->save();
                    $cons = new TelemedConsultRequest();
                    $cons->request_id = $req->id;
                    $cons->uid_year = date('Y', time());
                    $cons->uid_code = $mcf->code;
                    $uid = TelemedConsultRequest::where(['uid_year' => Date('Y', time()), 'uid_code' => $mcf->code])->orderBy("uid_id", "desc")->first();
                    if($uid != null)
                    {
                            $cons->uid_id = $uid->uid_id + 1;
                    }
                    else
                    {
                        $cons->uid_id = 1;
                    }
                    $cons->save();
                } else {
                    $cons = $id;
                    $id->created_at = Carbon::now();
                    $id->save();
                }
            } else {
                $req = new TelemedRequest();
                $req->event_type = "consultation";
                $req->initiator_id = $this->user->id;
                $req->telemed_center_id = $tmc->id;
                $req->save();
                $cons = new TelemedConsultRequest();
                $cons->request_id = $req->id;
                $cons->uid_year = date('Y', time());
                $cons->uid_code = $mcf->code;
                $uid = TelemedConsultRequest::where(['uid_year' => Date('Y', time()), 'uid_code' => $mcf->code])->orderBy("uid_id", "desc")->first();
                if($uid != null)
                {
                    $cons->uid_id = $uid->uid_id + 1;
                }
                else
                {
                    $cons->uid_id = 1;
                }
                $cons->save();
            }

            //$cons->req = $cons->request;
            $cons = $cons->toArray();
            unset($cons['created_at']);
            unset($cons['updated_at']);
            unset($cons['telemed_center_id']);
            unset($cons['med_care_fac_id']);
            unset($cons['decision_at']);
            unset($cons['abonent_side']);
            unset($cons['completed']);
            unset($cons['comment']);
            unset($cons['decision']);


            return response()->json(compact('tmc', 'mcfs', 'medical_profiles', 'cons', 'social_statuses', 'req'));
        }
    }

    /** Сохранение события(семинаров и совещаний) **/
    public function create(Request $request)
    {
        $types = ["meeting" => "совещание", "seminar" => "семинар"];
        $input = \Request::all();
        if (isset($input['data'])) {
            $valid = Validator::make($input, [
                'data.date' => 'required|Date|after:' . Carbon::now(),
                'data.event_type' => 'required|in:meeting,seminar',
                'data.participants' => 'required|array',
                'data.importance' => 'required|in:low,medium,high',
                'data.title' => 'required',
                'data.telemed_center_id' => 'required|integer'
            ]);
            $data = $input['data'];
            $files = $request->file('file');

            if ($valid->passes()) {
                $data['participants'] = array_pluck($data['participants'], 'id');
                $date = new Carbon($data['date']);
                $enddate = new Carbon($data['date']);
                $enddate->addHour()->subSecond();
                $valid->after(function ($validator) use ($data, $date, $enddate, $types) {
                    for ($i = 0; $i < count($data['participants']); $i++) {
                        $telemed = TelemedCenter::find($data['participants'][$i]);
                        if ($telemed == null) {
                            $validator->errors()->add('data.participants', 'Вы пытаетесь привязать несуществующий ТМЦ');
                            break;
                        }
                    }

                    $tmcs = \TmcRoleManager::getTmcs();
                    $found = false;
                    for ($i = 0; $i < count($tmcs); $i++) {
                        if ($tmcs[$i]['id'] == $data['telemed_center_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $validator->errors()->add('TMC', "Вы не являетесь участником этого ТМЦ!");
                    }

                    $check = ApiModel::checkTime($data['participants'], $date, $enddate);
                    if (count($check) > 0) {
                        $errormsg = "На данное время ТМЦ." . TelemedCenter::find($check[0]->telemed_center_id)->name . " назначил " . $types[$check[0]->event_type] . ". Списки участников совпадают";
                        $validator->errors()->add('data.date', $errormsg);
                    }

                });
                if ($valid->passes()) {
                    $writtenFiles = [];
                    try {
                        \DB::beginTransaction();
                        $date = new Carbon($data['date']);
                        $enddate = new Carbon($data['date']);
                        $enddate->addHour();
                        $req = new TelemedRequest();
                        $req->initiator_id = $this->user->id;
                        $req->start_date = $date;
                        $req->end_date = $enddate;
                        $req->event_type = $data['event_type'];
                        $req->importance = $data['importance'];
                        $req->title = $data['title'];
                        $req->telemed_center_id = $data["telemed_center_id"];
                        if ($req->save()) {
                            $add_array = [];
                            foreach ($data['participants'] as $tr) {
                                $part = new TelemedEventParticipant();
                                $part->telemed_center_id = $tr;
                                $part->request_id = $req->id;
                                $part->save();
                                array_push($add_array,$tr);
                            }
                            if(count($add_array) >0 )
                            {
                                $note_message = "ТМЦ -" .
                                    $req->telemedCenter->name .
                                    " добавил ваш ТМЦ - " .
                                    $part->telemedCenter->name .
                                    " в список участников события, чтобы принять или отклонить приглашение на событие пройдите по ссылке <a href='". \URL::to("/invitations/" . $part->id) . "' target='_blank'>ссылка</a>";
                                NotificationManager::createNotificationFromTMCForManyTmc($note_message, $add_array, true,$req->telemed_center_id,true);
                            }
                            if ($files[0] != null) {
                                $res = FileUpload::saveFiles($req->id, $files, $this->user->id, "files/");
                            }
                        }
                        \DB::commit();
                    } catch (\Exception $e) {
                        \DB::rollBack();
                        abort(500);
                    }
                    return response()->json(["success" => true, 'message' => 'Заявка с темой "' . $req->title . '" успешно добавлена', 'id' => $req->id]);
                } else {
                    return \Response::json([
                        'errors' => $valid->messages()->getMessages()
                    ], 422);
                }

            } else {
                return \Response::json([
                    'errors' => $valid->messages()->getMessages()
                ], 422);
            }
        } else {
            return \Response::json([
                "error" => "missing parameters"
            ], 404);
        }
    }

    /** Редактирование определенного семинара или совещания **/
    public function edit(Request $request, $id)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        $event = TelemedRequest::where(['id' => $id,'canceled'=>0])->whereIn('telemed_center_id', $tmcs)->first();
        if ($event != null) {
            if($event->telemedConclusion != null)
            {
                return response()->json(['errors'=>['error'=>["вы не можете редактировать событие с написанным заключением"]]],422);
            }
            $types = ["meeting" => "совещание", "seminar" => "семинар","consultation"=>"консультация"];
            $input = $request->all();
            if (isset($input['data'])) {
                $valid = Validator::make($input, [
                    'data.date' => 'required|Date|after:' . Carbon::now(),
                    'data.event_type' => 'required|in:meeting,seminar',
                    'data.participants' => 'required|array',
                    'data.importance' => 'required|in:low,medium,high',
                    'data.title' => 'required',
                    'data.telemed_center_id' => 'required|integer'
                ]);
                $data = $input['data'];

                $files = $request->file('file');

                if ($valid->passes()) {
                    $date = new Carbon($data['date']);
                    $enddate = new Carbon($data['date']);
                    $enddate->addHour()->subSecond();
                    $data['participants'] = array_pluck($data['participants'], 'id');

                    $valid->after(function ($validator) use ($data, $date, $enddate, $types, $event) {
                        for ($i = 0; $i < count($data['participants']); $i++) {
                            $telemed = TelemedCenter::find($data['participants'][$i]);
                            if ($telemed == null) {
                                $validator->errors()->add('data.participants', 'Вы пытаетесь привязать несуществующий ТМЦ');
                                break;
                            }
                        }
                        $tmcs = \TmcRoleManager::getTmcs();
                        $found = false;
                        for ($i = 0; $i < count($tmcs); $i++) {
                            if ($tmcs[$i]['id'] == $data['telemed_center_id']) {
                                $found = true;
                                break;
                            }
                        }
                        if (!$found) {
                            $validator->errors()->add('data.date', "Вы не являетесь участником этого ТМЦ!");
                        }
                        $types = ["meeting" => "совещание", "seminar" => "семинар","consultation"=>"консультация"];
                        $check = ApiModel::checkTime($data['participants'], $date, $enddate, $event->id);

                        if (count($check) > 0) {
                            $errormsg = "На данное время ТМЦ." . TelemedCenter::find($check[0]->telemed_center_id)->name . " назначил " . $types[$check[0]->event_type] . ". Списки участников совпадают";
                            $validator->errors()->add('data.date', $errormsg);
                        }

                    });

                    if ($valid->passes()) {

                        $writtenFiles = [];
                        try {
                            \DB::beginTransaction();
                            $input = ['initiator_id' => $this->user->id,
                                'start_date' => $date,
                                'end_date' => $enddate,
                                'event_type' => $data['event_type'],
                                'importance' => $data['importance'],
                                'title' => $data['title'],
                                'telemed_center_id' => $data["telemed_center_id"]];
                            if ($event->update($input)) {
                                $parts = array_pluck($event->telemedEventParticipants()->select('telemed_center_id')->get(), "telemed_center_id");
                                $items_to_delete = array_diff($parts, $data['participants']);
                                $deleted_array = [];
                                foreach ($items_to_delete as $item) {
                                    $part = TelemedEventParticipant::where(['request_id' => $event->id, 'telemed_center_id' => $item])->first();
                                    if($part != null)
                                    {
                                        if($part->delete())
                                        {
                                            array_push($deleted_array,$item);
                                        }
                                    }
                                }
                                if(count($deleted_array) > 0)
                                {

                                        NotificationManager::createNotificationFromTMCForManyTmc("ТМЦ -" . $event->telemedCenter->name . " исключил ваш ТМЦ - " . $part->telemedCenter->name . " из списка участников события -" . $event->title, $deleted_array, true,$event->telemed_center_id,true);

                                }
                                $items_to_create = array_diff($data['participants'], $parts);
                                foreach ($items_to_create as $tr) {
                                    $part = new TelemedEventParticipant();
                                    $part->telemed_center_id = $tr;
                                    $part->request_id = $event->id;
                                    $part->save();
                                }
                                if(count($items_to_create) > 0)
                                {
                                        NotificationManager::createNotificationFromTMCForManyTmc("ТМЦ -" . $event->telemedCenter->name . " добавил ваш ТМЦ - " . $part->telemedCenter->name . " в список участников события, чтобы принять или отклонить приглашение на событие пройдите по ссылке <a href='". \URL::to("/invitations/" . $part->id) . "' target='_blank'>ссылка</a>",$items_to_create, true,$event->telemed_center_id,true);
                                }
                                if (isset($data['deletedfiles'])) {
                                    foreach ($data['deletedfiles'] as $f) {
                                        FileUpload::removePivot($event->id, $f);
                                    }
                                }
                                if ($files[0] != null) {
                                    $res = FileUpload::saveFiles($event->id, $files, $this->user->id, "files/");
                                }
                            }
                            \DB::commit();
                        } catch (\Exception $e) {
                            \DB::rollBack();
                            return $e->getMessage();
                            abort(500);
                        }
                            //return response()->json(['participants' => $event->telemedEventParticipants->pluck('id')],500);


                            $tmc = $event->telemedCenter;
                            NotificationManager::createNotificationFromTMCForManyTmcUpdate($event->telemedEventParticipants, true,$event->telemed_center_id,true,$tmc,$event);
                            $message = 'Изменения успешно сохранены. Все участники были уведомлены';



                        return response()->json(["success" => true, 'message' => $message]);
                    } else {
                        return \Response::json([
                            'errors' => $valid->messages()->getMessages()
                        ], 422);
                    }

                } else {
                    return \Response::json([
                        'errors' => $valid->messages()->getMessages()
                    ], 422);
                }
            } else {
                return \Response::json([
                    "error" => "missing parameters"
                ], 422);
            }
        } else {
            return \Response::json([
                "error" => "not found"
            ], 404);
        }
    }

    /** Получить список шаблонов для завяок на консультации определенного пользователя **/
    public function getPatterns()
    {
        $columns = [
            'id',
            'consult_type',
            'desired_consultant',
            'desired_date',
            'desired_time',
            'doctor_fullname',
            'med_care_fac_id',
            'medical_profile',
            'name',
            'patient_address',
            'patient_birthday',
            'patient_gender',
            'patient_indications_for_use',
            'patient_indications_for_use_id',
            'patient_indications_for_use_other',
            'patient_questions',
            'patient_social_status',
            'patient_uid_or_fname',
        ];
        $patterns = ConsultPattern::where(['telemed_center_id' => \TmcRoleManager::getTmcs()[0]['id']])
            ->select($columns)->get();
        return response()->json($patterns);
    }

    /** Получить список типов консультации **/
    public function getConsultationTypes()
    {
        $consultTypes = ConsultationType::select('name','id')->get();
        return response()->json($consultTypes);
    }

    /** Функция не используется вообще **/
    public function deleteEvent($id)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        $event = TelemedRequest::where(['id' => $id])->whereIn("telemed_center_id", $tmcs)->first();
        if ($event != null && $event->event_type != "consultation") {
            if ($event->event_type != "consultation") {
                try {
                    \DB::beginTransaction();
                    $files = $event->files;
                    foreach ($files as $file) {
                        FileUpload::removePivot($id, $file->file_id);
                    }
                    $event->delete();
                    \DB::commit();
                } catch (\Exception $e) {
                    \DB::rollBack();
                    abort(500);
                }
            } else if ($event->event_type == "consultation") {

            }

        } else {
            abort(404);
        }
    }

    /** Получить список фаилов по определенному событию **/
    public function getEventFiles($id)
    {
        
        //Тут ошибка. Показывает только тмц, которые подходят под пользователя лпу, а должны быть те,
        //которые добавлены, как участники семенара
        
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(), 'id');
        
        
        $event = TelemedRequest::where(['id' => $id])->where(function ($query) use ($tmcs) {
            $query->whereIn("telemed_center_id", $tmcs);
            $query->orWhere(function ($q) use ($tmcs) {
                $q->whereExists(function ($q1) use ($tmcs) {
                    $q1->select(\DB::raw(1))
                        ->from('telemed_consult_requests as tcr')
                        ->whereIn('tcr.telemed_center_id', $tmcs)
                        ->where("tcr.request_id", \DB::raw("telemed_event_requests.id"));
                });
            });
        })->first();
        
        if ($event == null) {
            return response()->json(['error' => "not found"], 404);
        }

        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $event->id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();

        return response()->json($files, 200);
    }

    /** Получить данные об текущем пользователе **/
    public function getCurrentUser()
    {
        $user = Sentinel::getUser();
        $user->full_name = $user->getfName();
        return response()->json($user, 200);
    }

    /** Получить группы рассылки(ТМЦ) **/
    public function getDeliveryGroups()
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        $groups = DeliveryGroup::whereIn('telemed_center_id',$tmcs)->get();
        return response()->json(['data'=>$groups]);
    }

    /** Добавить группу рассылки(ТМЦ) **/
    public function addDeliveryGroup(Request $request)
    {
        $validator = Validator::make($request->all(),[
           'telemed_center_ids' => 'required|json',
            'name' => 'required|string'
        ]);
        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()],422);
        }
        $array = [];
        $tmcs = json_decode($request->get('telemed_center_ids'),true);
        foreach($tmcs as $tmc)
        {
            $res = \App\Models\Admin\TelemedCenter::find($tmc['id']);
            if($res!=null)
            {
                $obj =[];
                $obj['id'] = $res->id;
                array_push($array,$obj);
            }
        }
        $tmc1 = \TmcRoleManager::getTmcs()[0]['id'];
        $dg = new DeliveryGroup();
        $dg->name = $request->get('name');
        $dg->telemed_center_ids = json_encode($array);
        $dg->telemed_center_id = $tmc1;
        if($dg->save())
        {
            return response()->json(['success'=>1, 'message' => 'Группа рассылки сохранена успешно под именем ' . $dg->name]);
        }
        else
        {
            return response()->json(['errors'=>['error'=>'что то пошло не так']],500);
        }
    }
    /** Удалить группу рассылки(ТМЦ) **/
    public function deleteDeliveryGroup($id)
    {
       $dg = DeliveryGroup::find($id);
        if($dg != null)
        {
            $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
            if(in_array($dg->telemed_center_id,$tmcs))
            {
                if($dg->delete())
                {
                    return response()->json(['success'=>1]);
                }
                else
                {
                    return response()->json(['errors'=>['error'=>'что то пошло не так']],500);
                }
            }
            else
            {
                return response()->json(['errors'=>['access'=>'У вас нет доступа для просмотра этого материала!']],401);
            }
        }
        else
        {
            return response()->json(['errors'=>['not-found'=>'не найден!']],404);
        }
    }
    /** Отмена определенного события **/
    public function eventCancel($id,Request $request)
    {
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        $event = TelemedRequest::findOrFail($id);
        if(!in_array($event->telemed_center_id,$tmcs))
        {
            abort(401);
        }
        if($event->telemed_center_id == null)
        {
            abort(404);
        }
        if($event->canceled == 1)
        {
            return response()->json(['errors'=>['error'=>['Это событие уже отменено']]],422);
        }
        if($event->telemedConclusion)
        {
            return response()->json(['errors'=>['error'=>['Вы не можете отменить прошедшее событие!']]],422);
        }
        // Может быть тут поможет софт делет.
        $event->canceled = true;
        if($event->save())
        {
            $message = 'Пользователь '.$event->telemedCenter->name.' отменил '.trans('backend.'.$event->event_type) .' с темой "'.$event->title.'"';
            $participants = $event->telemedEventParticipants()->select("telemed_center_id")->get()->pluck('telemed_center_id')->toArray();

                \NotificationManager::createNotificationFromTMCForManyTmc($message,$participants,true,$event->telemed_center_id,true);
        }
    }

    /** Детали части заявки на консультацию заполненным консультантом **/
    public function consultSideDetails($request_id)
    {
        $req = \App\Models\Admin\TelemedConsultRequest::find($request_id);

        if( $req == null) {
            abort(404, 'Такая заявка не найдена');
        }

        if( $req->telemed_center_id == null ) {
            return response()->json(['errors'=> ['error' => ['Дананя заявка еще не привязана ни к одному ТМЦ']]], 422);
        }
        if( !in_array($req->telemed_center_id, array_pluck(\TmcRoleManager::getTmcs(), 'id')) ) {
            abort(404);
        }

        $users= User::whereExists(function ($query) use($req) {
            
            $query->select(\DB::raw('1'))
                ->from('telemed_user_pivots as tup')
                ->where('tup.user_id', \DB::raw('users.id'))
                ->where('tup.telemed_center_id',$req->telemed_center_id);
            
        })->select(\DB::raw('CONCAT(surname, " ", name, " ", middlename, " - ", position ) as name, id'))->get();




        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $req->request_id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->where('efp.type', null)
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();
        return response()->json([
           'users' => $users,
           'coordinator_fullname'  => TelemedCenter::find($req->telemed_center_id)->coordinator_fullname,
            'consult_side_files' => $files
        ]);

    }

    /** Решить: Пойду или не пойду на событие **/
    public function setParticipationDecision(Request $request, $request_id)
    {
        $rules = [
            'tmc_id' => "int",
            'decision' => "in:agreed,rejected",
        ];
        $this->validate($request, $rules);
        $tmc_id = $request->get("tmc_id");
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),'id');
        if(!in_array($tmc_id,$tmcs))
        {
            abort(401);
        }
        $inv = Invitation::where('telemed_center_id',$tmc_id)->where("request_id",$request_id)->first();

        if($inv != null)
        {
            if($inv->decision != "pending")
            {
                return response()->json(['success'=>0, 'message' => 'Вы уже приняли решение насчет этого мероприятия, повторное изменение не возможно']);
            }
            else
            {
               $inv->decision = $request->get('decision');
               $inv->save();
                if($request->get('decision') == 'agreed') {
                    $message = 'Вы подтвердили свое участие в событии';
                } else {
                    $message = 'Вы успешно отказались от участия';
                }
              
                return response()->json(['success'=>1, 'message' => $message]);

            }
        }
        else
        {
            abort(404);
        }
    }


}

