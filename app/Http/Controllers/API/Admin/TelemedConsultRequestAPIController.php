<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateTelemedConsultRequestAPIRequest;
use App\Http\Requests\API\Admin\UpdateTelemedConsultRequestAPIRequest;
use App\Models\Admin\TelemedConsultRequest;
use App\Repositories\TelemedConsultRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TelemedConsultRequestController
 * @package App\Http\Controllers\API\Admin
 */

class TelemedConsultRequestAPIController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    public function __construct(TelemedConsultRequestRepository $telemedConsultRequestRepo)
    {
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->telemedConsultRequestRepository->pushCriteria(new RequestCriteria($request));
        $this->telemedConsultRequestRepository->pushCriteria(new LimitOffsetCriteria($request));

        $telemedConsultRequests = $this->telemedConsultRequestRepository->with(['medCareFac', 'telemedCenter'])->all();

        return $this->sendResponse($telemedConsultRequests->toArray(), 'TelemedConsultRequests retrieved successfully');
    }

    /**
     * @param CreateTelemedConsultRequestAPIRequest $request
     * @return Response
     */
    public function store(CreateTelemedConsultRequestAPIRequest $request)
    {
        $input = $request->all();

        $telemedConsultRequests = $this->telemedConsultRequestRepository->create($input);

        return $this->sendResponse($telemedConsultRequests->toArray(), 'TelemedConsultRequest saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/telemedConsultRequests/{id}",
     *      summary="Display the specified TelemedConsultRequest",
     *      tags={"TelemedConsultRequest"},
     *      description="Get TelemedConsultRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TelemedConsultRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TelemedConsultRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TelemedConsultRequest $telemedConsultRequest */
        $telemedConsultRequest = $this->telemedConsultRequestRepository->find($id);

        if (empty($telemedConsultRequest)) {
            return Response::json(ResponseUtil::makeError('TelemedConsultRequest not found'), 404);
        }

        return $this->sendResponse($telemedConsultRequest->toArray(), 'TelemedConsultRequest retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTelemedConsultRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/telemedConsultRequests/{id}",
     *      summary="Update the specified TelemedConsultRequest in storage",
     *      tags={"TelemedConsultRequest"},
     *      description="Update TelemedConsultRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TelemedConsultRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TelemedConsultRequest that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TelemedConsultRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TelemedConsultRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTelemedConsultRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var TelemedConsultRequest $telemedConsultRequest */
        $telemedConsultRequest = $this->telemedConsultRequestRepository->find($id);

        if (empty($telemedConsultRequest)) {
            return Response::json(ResponseUtil::makeError('TelemedConsultRequest not found'), 404);
        }

        $telemedConsultRequest = $this->telemedConsultRequestRepository->update($input, $id);

        return $this->sendResponse($telemedConsultRequest->toArray(), 'TelemedConsultRequest updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/telemedConsultRequests/{id}",
     *      summary="Remove the specified TelemedConsultRequest from storage",
     *      tags={"TelemedConsultRequest"},
     *      description="Delete TelemedConsultRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TelemedConsultRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TelemedConsultRequest $telemedConsultRequest */
        $telemedConsultRequest = $this->telemedConsultRequestRepository->find($id);

        if (empty($telemedConsultRequest)) {
            return Response::json(ResponseUtil::makeError('TelemedConsultRequest not found'), 404);
        }

        $telemedConsultRequest->delete();

        return $this->sendResponse($id, 'TelemedConsultRequest deleted successfully');
    }
}
