<?php

namespace App\Http\Controllers;

use App\Criteria\ActiveCriteria;
use App\Criteria\DateRangeCriteria;
use App\Criteria\TelemedCriteria;
use App\Http\Requests;
use App\Http\Requests\CreatePollRequest;
use App\Http\Requests\UpdatePollRequest;
use App\Models\Admin\Poll;
use App\Models\Admin\PollRow;
use App\Models\Admin\PollUserPivot;
use App\Models\TelemedCenter;
use App\Repositories\PollRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Route;

class PollController extends InfyOmBaseController
{
    /** @var  PollRepository */
    private $pollRepository;

    public function __construct(PollRepository $pollRepo)
    {
        $this->middleware(['auth', 'has_access_to_edit_tmc']);
        $this->pollRepository = $pollRepo;
        $this->user = \Sentinel::getUser();
        $id = Route::current()->getParameter('TelemedId');
        $crit = new TelemedCriteria();
        $crit->setTelemedId($id);
        $this->pollRepository->pushCriteria($crit);
    }

    /**
     * Display a listing of the Poll.
     *
     * @param Request $request
     * @return Response
     */
    public function index($TelemedId, Request $request)
    {

        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }


        $this->pollRepository->pushCriteria(new RequestCriteria($request));

        if($request->has('from')&&$request->has('to')) {
            $this->pollRepository->pushCriteria(new DateRangeCriteria($request->get('from'), $request->get('to')));
        }
/*        if($request->has('daterange'))
        {
            $d = $request->get('daterange');
            $daterange[] = (substr($d,0,10));
            $daterange[] =  (substr($d,12,20));
            $this->pollRepository->pushCriteria(new DateRangeCriteriaPoll($daterange[0], $daterange[1],"finished_at"));
        }*/
        $polls = $this->pollRepository->paginate(30);
        $tmc = TelemedCenter::find($TelemedId);
        return view('polls.index', ["TelemedId" => $TelemedId, 'tmc' => $tmc,'search'=>$search])
            ->with('polls', $polls);
    }

    
    /**
     * Show the form for creating a new Poll.
     *
     * @return Response
     */
    public function create($TelemedId)
    {
        return view('polls.create', ["TelemedId" => $TelemedId]);
    }

    /**
     * Store a newly created Poll in storage.
     *
     * @param CreatePollRequest $request
     *
     * @return Response
     */
    public function store($TelemedId, CreatePollRequest $request)
    {
        $input = $request->all();
        $poll = new Poll($input);
        $poll->telemed_center_id = $TelemedId;
        $poll->author_id = $this->user->id;
        $poll->save();

        if (isset($input['newitemname'])) {
            foreach ($input['newitemname'] as $item) {
                $pollrow = new PollRow();
                $pollrow->title = $item;
                $pollrow->poll_id = $poll->id;
                $pollrow->telemed_center_id = $TelemedId;
                $pollrow->save();
            }
        }

        Flash::success(trans('backend.Poll') . trans('backend.success_saved'));

        return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
    }

    /**
     * Display the specified Poll.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($TelemedId, $id)
    {
        $state = false;

        $isanswered = PollUserPivot::where(['poll_id' => $id, 'user_id' => $this->user->id])->first();
        if ($isanswered != null) {
            $state = $isanswered->poll_row_id;
        }

        $poll = $this->pollRepository->with('pollRows')->findWithoutFail($id);

        if (empty($poll)) {
            Flash::error(trans('backend.Poll') ." ". trans('backend.not_found'));

            return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
        }
        //die('nyanya');
        $poll->totalVotes = $poll->pollRows->sum('votes');
        return view('polls.show', compact('TelemedId', 'state'))->with('poll', $poll);
    }

    /**
     * Show the form for editing the specified Poll.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($TelemedId, $id)
    {
        $poll = $this->pollRepository->findWithoutFail($id);


        if (empty($poll)) {
            Flash::error(trans('backend.Poll') ." ". trans('backend.not_found'));

            return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
        }
        $count = $poll->where('id', $id)->whereDate('finished_at', '>', Carbon::now())->count();
        if ($count == 0) {

            Flash::error('Срок опроса прошел. Вы не можете внести изменения');

            return redirect(route('polls.index', ["TelemedId" => $TelemedId]));

        }

        return view('polls.edit', ["TelemedId" => $TelemedId])->with('poll', $poll);
    }

    /**
     * Update the specified Poll in storage.
     *
     * @param  int $id
     * @param UpdatePollRequest $request
     *
     * @return Response
     */
    public function update($TelemedId, $id, UpdatePollRequest $request)
    {
        $poll = $this->pollRepository->findWithoutFail($id);
        $input = $request->all();
        if (empty($poll)) {
            Flash::error(trans('backend.Poll') ." ". trans('backend.not_found'));

            return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
        }
        $poll->telemed_center_id = $TelemedId;
        $poll->active = $input['active'];
        $poll = $this->pollRepository->update($request->all(), $id);

        if (isset($input['newitemname'])) {
            foreach ($input['newitemname'] as $item) {
                $pollrow = new PollRow();
                $pollrow->title = $item;
                $pollrow->poll_id = $poll->id;
                $pollrow->save();
            }
        }

        if (isset($input['deleteditems'])) {
            foreach ($input['deleteditems'] as $k => $v) {
                $pollrow = PollRow::where(['id' => $k, 'poll_id' => $poll->id]);
                if ($pollrow != null) {
                    $pollrow->delete();
                }
            }
        }

        Flash::success(trans('backend.Poll') ." успешно изменен!");

        return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
    }

    /**
     * Remove the specified Poll from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($TelemedId, $id)
    {
        $poll = $this->pollRepository->findWithoutFail($id);

        if (empty($poll)) {
            Flash::error(trans('backend.Poll') ." ". trans('backend.not_found'));

            return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
        }

        $this->pollRepository->delete($id);

        Flash::success(trans('backend.Poll') ." успешно удален!");

        return redirect(route('polls.index', ["TelemedId" => $TelemedId]));
    }


}
