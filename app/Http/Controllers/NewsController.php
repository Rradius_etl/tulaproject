<?php

namespace App\Http\Controllers;

use App\Models\Admin\News;
use Illuminate\Http\Request;

use App\Http\Requests;

class NewsController extends Controller
{
    /** Список всех новостей */
    public function index()
    {

        $news = News::published()->paginate(3);
        return view('frontend.news', compact('news'));
    }

    /** Посмотреть определенную новость */
    public function view($slug)
    {
        $article = News::where('slug', $slug)->first();

        if (empty($article)) {
            return abort(404, 'Такая статья не существует');
        }

        $article->hits++;
        $article->save();

        return view('frontend.news-item', compact('article'));
    }

    /** Поиск по новостям */
    public function search(Request $request)
    {
        $q = $request->get('q', '');
        $query = strip_tags($q);

        if( strlen($query) > 1 ) {

            $news = News::published()->where('title', 'LIKE', "%$query%")
                ->orWhere('content', 'LIKE', "%$query%")
                ->paginate(20);

            return view('search.index', compact('news','q'));
        } else {
            \Flash::warning('Введите запрос в поле поиска. И его длина должна быть минимум 2 символов');
            return view('search.index', compact('news','q'));
        }


    }


}
