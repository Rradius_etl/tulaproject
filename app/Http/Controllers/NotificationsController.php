<?php

namespace App\Http\Controllers;

use App\Models\Admin\PollUserPivot;
use App\Models\UserNotificationRecepient;
use Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
/** Список уведомлений */
class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->user = Sentinel::getUser();
    }

    /** Список уведомлений */
    public function index(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }

        if($search != "")
        {
            $notifications = $this->user->getNotificationsWithSearch($search)->orderBy('created_at','desc')->paginate(30);
        }
        else
        {
            $notifications = $this->user->getNotifications()->orderBy('created_at','desc')->paginate(30);
        }


        //return $notifications;
        return view('notifications.index', compact('notifications','search'));
    }

    /** Список непрочитанных уведомлений */
    public function unread(Request $request)
    {
        $search = "";
        if($request->has('search'))
        {
            $search = $request->get('search');
        }
        if($search != "")
        {
            $notifications = $this->user->getUnreadNotificationsWithSearch($search)->orderBy('created_at','desc')->paginate(30);
        }
        else
        {
            $notifications = $this->user->getUnreadNotifications()->orderBy('created_at','desc')->paginate(30);
        }
        return view('notifications.unread', compact('notifications','search'));
    }

    /** Последнее непрочитанное уведомление */
    public function last()
    {
        $notifications = $this->user->getUnreadNotifications()->orderBy('created_at','desc')->limit(3);
        return response()->json($notifications);
    }


    /**
     * Последние три уведомлении по опросам
     **/
    public function lastUnreadPollNotifications()
    {
        $notifications = $this->user->getUnreadPollNotifications()->orderBy('created_at','desc')->limit(3);
        return response()->json($notifications);
    }

    /** Отметить как прочитанное */
    public function markAsRead($type, Request $request)
    {
        $rules = [
            'data' => 'required|array',
            'page' => 'required|integer'
        ];
        //$this->validate($request, $rules);

        foreach ( $request->get('data') as $id ) {
            $pivot = UserNotificationRecepient::with('notification')->where(['id' => $id, 'user_id' => $this->user->id])->first();

            if( !empty($pivot) && $pivot->is_read == false ) {
                $pivot->is_read = true;
                if($pivot->save())
                {
                    \NotificationManager::clearUserCache();
                    if( $pivot->notification->need_callback && $pivot->notification->telemed_center_id != null ) {
                        \NotificationManager::createNotificationForTmcUsers('Пользователь ' .$this->user->getfName().'  прочитал ваше уведомление: ' . $pivot->notification->message, $pivot->notification->telemed_center_id, false,true);
                    }
                }
            }
        }

        if( $type == 'unread') {
            $notifications = $this->user->getUnreadNotifications()->orderBy('created_at','desc')->paginate(30);
        } elseif ( $type == 'all') {
            $notifications = $this->user->getNotifications()->orderBy('created_at','desc')->paginate(30);
        }
        elseif( $type == 'unreadpolls') {
            $notifications = $this->user->getUnreadPollNotifications()->orderBy('created_at','desc')->paginate(30);
        } elseif ( $type == 'allpolls') {
            $notifications = $this->user->getPollNotifications()->orderBy('created_at','desc')->paginate(30);
        }

        if( !$request->has('single') && !$request['single'] == true ) {
            return view('notifications.table', compact('notifications'));
        }


    }


}
