<?php

namespace App\Http\Controllers;

use App\Criteria\DateRangeCriteria;
use App\Criteria\MedCareFacConsultRequestCriteria;
use App\Criteria\NewRequestCriteria;
use App\Criteria\TelemedOtherCriteria;
use App\Criteria\UIDAbonCriteria;
use App\Criteria\UIDCriteria;
use App\Http\Requests\Admin;
use App\Http\Requests\CreateTelemedConsultRequestRequest;
use App\Http\Requests\UpdateTelemedConsultRequestRequest;
use App\Models\Admin\ConsultationType;
use App\Models\Admin\File;
use App\Models\Admin\IndicationForUse;
use App\Models\ConsultPattern;
use App\Models\FileUpload;
use App\Models\TelemedCenter;
use App\Repositories\TelemedConsultRequestAbonentRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use DB;
use App\Models\Admin\SocialStatus;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\MedicalProfile;
use App\Models\Admin\MedicalProfileCategory;
use Carbon\Carbon;
use App\Models\TelemedConsultAbonentSide;
use App\Models\Admin\TelemedRequest;
class AbonentTelemedConsultRequestController extends InfyOmBaseController
{
    /** @var  TelemedConsultRequestRepository */
    private $telemedConsultRequestRepository;

    private static $rules = [
        'request_id' => 'required',
        'created_time' => 'date_format:Y-m-d',
        'doctor_fullname' => 'required',
        'med_care_fac_id' => 'required',

        'patient_birthday' => 'required|date_format:d.m.Y',
        'desired_date' => 'date_format:d.m.Y',
        'desired_time' => 'date_format:G:i',
        'patient_indications_for_use_other' => 'required_if:patient_indications_for_use,other',
        'patient_indications_for_use' => 'required',
        'consult_type' => 'required',
        'consult_profile' => 'required',

        'patient_uid_or_fname' => 'required',
        'coordinator_fullname' => 'required',
        'coordinator_phone' => 'required',

    ];
    public static $genders = [
        'male' => 'мужской',
        'female' => 'женский',
    ];


    public $consult_types;


    /** Инициализация данных **/
    public function __construct(TelemedConsultRequestAbonentRepository $telemedConsultRequestRepo)
    {
        $this->middleware(['auth','is_user_of_telemed']);
        $this->telemedConsultRequestRepository = $telemedConsultRequestRepo;
        $this->user = Sentinel::getUser();
        $this->tmc = \TmcRoleManager::getTmcs();
        $crit = new TelemedOtherCriteria();
        $crit->setTelemedId($this->tmc);
        $this->telemedConsultRequestRepository->pushCriteria($crit);
        $this->consult_types = ConsultationType::select('name','id')->get()->pluck('name', 'id');
    }
    

    /**
     * Display a listing of the TelemedConsultRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $conssearch = "";
        $uidsearch = "";
        $canceled = "";
        $search = "";
        if($request->has('uidsearch'))
        {
            $uidsearch = $request->get('uidsearch');
        }
        if($request->has('conssearch'))
        {
            $conssearch = $request->get('conssearch');
        }
        if($request->has('canceled'))
        {
            $canceled = $request->get('canceled');
        }
        if($request->has('search'))
        {
            $search = $request->get('search');
        }

        if($request->has('from')&&$request->has('to')) {
            $this->telemedConsultRequestRepository->pushCriteria(new DateRangeCriteria($request['from'], $request['to']));
        }

        $uidcrit = new UIDAbonCriteria();
        $uidcrit->setSearch($search);
        $uidcrit->setMCF($conssearch);
        $this->telemedConsultRequestRepository->pushCriteria($uidcrit);
        $mcfs = MedCareFac::pluck('name','id');
        $this->telemedConsultRequestRepository->pushCriteria(new NewRequestCriteria($request));
        $telemedConsultRequests = $this->telemedConsultRequestRepository->paginate(15);
        return view('my_telemed_consult_requests.index',['uidsearch'=>$uidsearch,'conssearch'=>$conssearch,'mcfs'=>$mcfs,'canceled'=>$canceled])
            ->with('telemedConsultRequests', $telemedConsultRequests);
    }

    /**
     * Show the form for creating a new TelemedConsultRequest.
     *
     * @return Response
     */
    public function create()
    {

        
        $ind_for_use = IndicationForUse::getOptionValues();
        DB::beginTransaction();
        try {
            $socstatuses = SocialStatus::pluck('name', 'id');
            $tmc = TelemedCenter::find(\TmcRoleManager::getTmcs()[0]['id']);
            $mfc = $tmc->medCareFac;
            $mfc = $tmc->medCareFac;
            $req_id = TelemedConsultRequest::where(['uid_code' => $mfc->code, 'uid_year' => date('Y', time())])->whereExists(function($query)
            {
                $query->select(DB::raw("1"))->from("telemed_event_requests as ter")->where("ter.telemed_center_id",\TmcRoleManager::getTmcs()[0]['id']);
            })->orderBy("uid_id", "desc")->first();
            if ($req_id == null) {
                $id = 1;
                $req = new TelemedRequest();
                $req->telemed_center_id = $tmc->id;
                $req->event_type = "consultation";
                $req->initiator_id = $this->user->id;
                $req->save();

                $dec = new TelemedConsultRequest();
                $dec->med_care_fac_id = $mfc->id;
                $dec->uid_code = $mfc->code;
                $dec->uid_year = date("Y", time());
                $dec->uid_id = $id;
                $dec->request_id = $req->id;
                $dec->save();


            } else {
                if ($req_id->request->telemedRequestsMcfAbonents != null) {
                    $id = $req_id->uid_id + 1;
                    $req = new TelemedRequest();
                    $req->event_type = "consultation";
                    $req->initiator_id = $this->user->id;
                    $req->telemed_center_id = $tmc->id;
                    $req->save();

                    $dec = new TelemedConsultRequest();
                    $dec->uid_code = $mfc->code;
                    $dec->uid_year = date("Y", time());
                    $dec->med_care_fac_id = $mfc->id;
                    $dec->uid_id = $id;
                    $dec->request_id = $req->id;
                    $dec->save();
                } else {
                    $id = $req_id->uid_id;
                    $req = $req_id->request;
                }

            }
            $mcfs = MedCareFac::where('id', '!=', $mfc->id)->pluck("name", "id");
            $genders = self::$genders;
            $consult_types = $this->consult_types;


            $medical_profiles_model = MedicalProfile::with('medicalProfileCategory')->get();
            $medical_profile_categories = MedicalProfileCategory::all()->lists('name', 'id');

            foreach ($medical_profiles_model as $item) {
                $key = $medical_profile_categories[$item->category_id];
                $medical_profiles[$key][$item->id] = $item->name;
            }

            //return response()->json($medical_profiles);
            DB::commit();
            return view('my_telemed_consult_requests.create', compact('genders', 'medical_profiles', 'consult_types', 'req', 'socstatuses', 'tmc', 'id', 'mfc', 'ind_for_use', 'mcfs'));
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }


        
        return view('my_telemed_consult_requests.create');
    }

    /**
     * Store a newly created TelemedConsultRequest in storage.
     *
     * @param CreateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            $this->validate($request, self::$rules);
            $input = $request->all();
            $input['patient_birthday'] = Carbon::createFromFormat('d.m.Y', $input['patient_birthday'])->toDateString();
            $input['desired_date'] = Carbon::createFromFormat('d.m.Y', $input['desired_date'])->toDateString();
            $req_abon = new TelemedConsultAbonentSide($input);
            $req = \App\Models\TelemedConsultRequest::where(['request_id'=>$input['request_id']])->first();
            $req->med_care_fac_id = $input['med_care_fac_id'];
            $tmcs = \TmcRoleManager::getTmcs();

            $req_abon->telemed_center_id = \TmcRoleManager::getTmcs()[0]['id'];
            $req_abon->save();
            DB::commit();
            $message = "Пользователь " . \TmcRoleManager::getTmcs()[0]['name']." создал заявку на консультацию в ваше ЛПУ, зайдите по <a href='".\Illuminate\Support\Facades\Request::root()."/telemed-consult-requests'>ссылке</a>";
            $mcfadmin = MedCareFac::find($input['med_care_fac_id']);
            if($mcfadmin!= null)
            {
                if($mcfadmin->user_id != null)
                {
                    \NotificationManager::createNotification($message,[$mcfadmin->user_id],false,false);
                }
            }
            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']))->withSuccess('Заявка на телемедицинскую клиническую консультацию принята. Вы будете уведомлены когда ЛПУ-консультант рассмотрит Вашу заявку');
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        return view('my_telemed_consult_requests.show')->with('telemedConsultRequest', $telemedConsultRequest);
    }

    /**
     * Show the form for editing the specified TelemedConsultRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);
        //return $telemedConsultRequest;
        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->request->canceled != 0)
        {
            Flash::error("Вы не можете редактировать или удалять отмененные заявки");
            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->consultRequest->decision != "pending")
        {
            Flash::error("Вы не можете редактировать или удалять отказанные или согласованные заявки");
            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $ind_for_use = IndicationForUse::getOptionValues();

        $socstatuses = SocialStatus::pluck('name', 'id');
        $tmc =  TelemedCenter::find(\TmcRoleManager::getTmcs()[0]['id']);
        $mfc = $tmc->medCareFac;

        $mcfs = MedCareFac::where('id', '!=', $mfc->id)->pluck("name", "id");
        $genders = self::$genders;
        $consult_types = $this->consult_types;

        $uid = $telemedConsultRequest->consultRequest->getUID();
 

        $medical_profiles_model = MedicalProfile::with('medicalProfileCategory')->get();
        $medical_profile_categories = MedicalProfileCategory::all()->lists('name', 'id');

        foreach ($medical_profiles_model as $item) {
            $key = $medical_profile_categories[$item->category_id];
            $medical_profiles[$key][$item->id] = $item->name;
        }

        $files= File::join('event_file_pivots as efp', 'files.id', '=', 'efp.file_id')->where('efp.request_id', $id)
            ->where('efp.file_id', \DB::raw('files.id'))
            ->where('efp.type', 'abonent_side')
            ->select('files.*','files.id as file_id','efp.id as id')
            ->get();

        \Former::populate($telemedConsultRequest);
        return view('my_telemed_consult_requests.edit', compact('genders', 'medical_profiles', 'consult_types', 'telemedConsultRequest', 'socstatuses', 'tmc', 'uid', 'mfc', 'ind_for_use', 'mcfs', 'files')
        );
    }

    /**
     * Update the specified TelemedConsultRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTelemedConsultRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelemedConsultRequestRequest $request)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
             Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->request->canceled != 0)
        {
            Flash::error("Вы не можете редактировать или удалять отмененные заявки");
            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->consultRequest->decision != "pending")
        {
            Flash::error("Вы не можете редактировать или удалять отказанные или согласованные заявки");

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        $input = $request->all();

        $input['desired_date'] = \Carbon\Carbon::createFromFormat( 'd.m.Y',$input['desired_date'])->toDateTimeString()  ;
        $input['patient_birthday'] = \Carbon\Carbon::createFromFormat( 'd.m.Y',$input['patient_birthday'])->toDateTimeString()  ;
        $telemedConsultRequest = $this->telemedConsultRequestRepository->update($input, $id);
        $cons = $telemedConsultRequest->consultRequest;

        if($cons->telemedCenter!=null)
        {
            $message = "Пользователь " . $telemedConsultRequest->request->telemedCenter->name." изменил заявку на консультацию(".$cons->getUID().") в ваше ТМЦ, зайдите по <a href='".\Illuminate\Support\Facades\Request::root()."/abonent-telemed-consult-requests'>ссылке</a>";
            \NotificationManager::createNotificationFromTMCtoTMC($message,$cons->telemedCenter->id,true,$telemedConsultRequest->request->telemed_center_id,true);
        }
        else
        {
            $mcfadmin = $telemedConsultRequest->consultRequest->medCareFac;
            $message = "Пользователь " . $telemedConsultRequest->request->telemedCenter->name." изменил заявку на консультацию(".$cons->getUID().") в ваше ЛПУ, зайдите по <a href='".\Illuminate\Support\Facades\Request::root()."/telemed-consult-requests'>ссылке</a>";
            if($mcfadmin!= null)
            {
                if($mcfadmin->user_id != null)
                {
                    \NotificationManager::createNotificationFromTMC($message,[$mcfadmin->user_id],$telemedConsultRequest->request->telemed_center_id,true,true);
                }
            }
        }

        /* Загрузка файлов */
        if ($request->hasFile('file')) {
            FileUpload::saveAbonentSideFile($id, $request->file('file'), $this->user->id, "files/");
        }
        /* ./Загрузка файлов */


       Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_updated'));

        return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    /**
     * Remove the specified TelemedConsultRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);

        if (empty($telemedConsultRequest)) {
            Flash::error(trans('backend.TelemedConsultRequest') ." ". trans('backend.not_found') );

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        if($telemedConsultRequest->consultRequest->decision != "pending")
        {
            Flash::error("Вы не можете редактировать или удалять отказанные или согласованные заявки");

            return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
        }
        else
        {
            DB::beginTransaction();
            $telemedConsultRequest->delete();
            $telemedConsultRequest->consultRequest->delete();
            $telemedConsultRequest->consultRequest->request->delete();
            DB::commit();
        }

        Flash::success(trans('backend.TelemedConsultRequest') ." ". trans('backend.success_deleted'));

        return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
    }

    // Save pattern
    public function savepattern($id, Request $request)
    {
        $telemedConsultRequest = $this->telemedConsultRequestRepository->findWithoutFail($id);
        if($telemedConsultRequest!=null)
        {
            $lastid = ConsultPattern::orderBy("id","desc")->first();
            if($lastid == null)
            {
                $lastid = 1;
            }
            else
            {
                $lastid = $lastid->id + 1;
            }


            $pat = new ConsultPattern($telemedConsultRequest->toArray());
            $pat->telemed_center_id = $telemedConsultRequest->telemed_center_id;

            if($request->has('name')) {
                $pat->name = $request->get('name');
            } else {
                $pat->name = "новый шаблон ".$lastid;
            }

            if($pat->save())
            {
                Flash::success("Шаблон сохранен успешно! Название шаблона - ".$pat->name);
                return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
            }
            else
            {
                Flash::error('возникли непредвиденные ошибки. Попробуйте повторить эту операция позже');
                return redirect(route('abonent-telemed-consult-requests.index',['orderBy'=>'created_at','sortedBy'=>'desc']));
            }
        }
    }
}
