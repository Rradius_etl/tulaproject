<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use Sentinel;
use Activation;
use Reminder;
use Validator;
use Mail;
use Storage;
use CurlHttp;
use Cache;
class AuthController extends Controller
{

    /**
     * Show login page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * Show Register page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('auth.register');
    }


    /**
     * Show wait page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wait()
    {
        return view('auth.wait');
    }


    /**
     * Process login users
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function loginProcess(Request $request)
    {
        try
        {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
            ]);
            $remember = (bool) $request->remember;

            if ($user = Sentinel::authenticate($request->all(), $remember))
            {
                \TmcRoleManager::getRolesWithUser($user);
                return Redirect::intended('/profile')->with('afterLogin', true);
            }
            $errors = trans('auth.wrong credentials');
            return Redirect::back()
                ->withInput()
                ->withErrors($errors);
        }
        catch (NotActivatedException $e)
        {
            $sentuser= $e->getUser();
            $activation = Activation::create($sentuser);
            $code = $activation->code;
            try{
                $sent = Mail::send( 'emails.auth.account_activate', compact('sentuser', 'code'), function($m) use ($sentuser)
                {
                    $m->from( config('mail.username'), config('mail.username') );
                    $m->to($sentuser->email)->subject(trans('email.subjects.Account activation'));
                });

                if ($sent === 0)
                {
                    return Redirect::to('login')
                        ->withErrors(trans('auth.activation message not sent'));
                }
            }catch (\Swift_TransportException $ex){
                $errors = trans('auth.Account is not activated');
                return view('auth.login')->withErrors($errors);
            }
            $errors = trans('auth.Account is not activated. Message sent again');
            return view('auth.login')->withErrors($errors);
        }
        catch (ThrottlingException $e)
        {
            $delay = $e->getDelay();
            $errors =  trans('auth.account locked', ['delay' =>$delay]);
        }
        return Redirect::back()
            ->withInput()
            ->withErrors($errors);
    }


    /**
     * Process register user from site
     *
     * @param Request $request
     * @return $this
     */
    public function registerProcess(Request $request)
    { 

        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'middlename' => 'required',
            'position' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);
        $input = $request->all();
        $credentials = $request->all();

        if($user = Sentinel::findByCredentials($credentials))
        {
            unset($input['email']);
            return Redirect::to('register')
                ->withErrors(trans('auth.same email exists'), 'custom')->withInput($input);
        }

        if ($sentuser = Sentinel::register($credentials))
        {
            $activation = Activation::create($sentuser);
            $code = $activation->code;
            
            try{
                $sent = Mail::send('emails.auth.account_activate', compact('sentuser', 'code'), function($m) use ($sentuser)
                    {
                        $m->from(config('mail.username'),config('mail.username'));
                        $m->to($sentuser->email)->subject(trans('email.subjects.Account activation'));
                    });
            
                if ($sent === 0)
                {
                    return Redirect::to('register')
                        ->withErrors(trans('auth.message not sent'), 'custom');
                }

                $role = Sentinel::findRoleBySlug('user');
                $role->users()->attach($sentuser);


                return Redirect::to('login')
                    ->withSuccess(trans('auth.Your account has been created'))
                    ->with('userId', $sentuser->getUserId());
            }catch (\Swift_TransportException $ex){
                return Redirect::to('login')
                    ->withInput()
                    ->withSuccess(trans('auth.Your account has been created but dont activate'));
            }
        }
        return Redirect::to('register')
            ->withInput()
            ->withErrors(trans('auth.Failed to register'), 'custom');
    }


    /**
     *  Activate user account by user id and activation code
     *
     * @param $id
     * @param $code
     * @return $this
     */
    public function activate($id, $code)
    {
        $sentuser = Sentinel::findById($id);

        if( is_null($sentuser)) {
            return Redirect::to("login")
                ->withErrors('Такой пользователь не зарегистрирован в системе');
        }
        if ( ! Activation::complete($sentuser, $code))
        {
            return Redirect::to("login")
                ->withErrors(trans('auth.Invalid or expired activation code'));
        }

        return Redirect::to('login')
            ->withSuccess(trans('auth.Account activated'));
    }
    
    public function manualActivation($id){
        $user = Sentinel::findById($id);
        
        if( is_null($user)) {
            return Redirect::to("login")
                ->withErrors('Такой пользователь не зарегистрирован в системе');
        }
        
        if (Sentinel::activate($user)){
            return Redirect::to('admin/users/'.$id.'/edit')
            ->withSuccess(trans('auth.Account activated'));
        } else {
            return Redirect::to("login")
                ->withErrors('Пользователь не активирован');
        }
        
    }


    /**
     * Show form for begin process reset password
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resetOrder()
    {
        return view('auth.passwords.email');
    }


    /**
     * Begin process reset password by email
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function resetOrderProcess(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);
        $email = $request->email;
        $sentuser = Sentinel::findByCredentials(compact('email'));
        if ( ! $sentuser)
        {
            return Redirect::back()
                ->withInput()
                ->withErrors(trans('auth.User not found'));
        }
        $reminder = Reminder::exists($sentuser) ?: Reminder::create($sentuser);
        $code = $reminder->code;

        $sent = Mail::send('emails.auth.account_reminder', compact('sentuser', 'code'), function($m) use ($sentuser)
        {
            $m->from(config('mail.username'),config('mail.username'));
            $m->to($sentuser->email)->subject(trans('email.subjects.Password reset'));
        });
        if ($sent === 0)
        {
            return Redirect::to('reset')
                ->withErrors(trans('auth.message not sent'));
        }
        return Redirect::to('wait');
    }

    /**
     * Show form for complete reset password
     *
     * @param $id
     * @param $code
     * @return mixed
     */
    public function resetComplete($id, $code)
    {
        $user = Sentinel::findById($id);
        return view('auth.passwords.reset');
    }


    /**
     * Complete reset password
     *
     * @param Request $request
     * @param $id
     * @param $code
     * @return $this
     */
    public function resetCompleteProcess(Request $request, $id, $code)
    {
        $this->validate($request, [
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);
        $user = Sentinel::findById($id);
        if ( ! $user)
        {
            return Redirect::back()
                ->withInput()
                ->withErrors('');
        }
        if ( ! Reminder::complete($user, $code, $request->password))
        {
            return Redirect::to('login')
                ->withErrors(trans('auth.Invalid or expired password reset code'));
        }
        return Redirect::to('login')
            ->withSuccess(trans('auth.Password reset successfully'));
    }

    /**
     * @return mixed
     */
    public function logoutuser()
    {
        $user = Sentinel::logout();
        Cache::forget("userroles".$user->id);
        return Redirect::intended('/');
    }

}