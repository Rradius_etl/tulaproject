<?php
/**
 * User: dastan
 * Date: 14/06/2016 *
 */



// Home Page
Breadcrumbs::register('homepage', function($breadcrumbs)
{
    $breadcrumbs->push(trans('routes.homepage'), route('homepage'));
});


// Home Page => Authorization
Breadcrumbs::register('login', function($breadcrumbs)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push(trans('routes.login'), route('login'));
});

// Home Page => News
Breadcrumbs::register('news', function($breadcrumbs)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push(trans('routes.news'), route('news'));

});

// Home Page => News
Breadcrumbs::register('page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push($page->title, route('page.view', $page->slug));

});


// Home Page => News
Breadcrumbs::register('news.view', function($breadcrumbs)
{
    $breadcrumbs->parent('news');
    $breadcrumbs->push('', route('news.view', ''));

});


// Home Page => new-consultation
Breadcrumbs::register('new-consultation', function($breadcrumbs)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push(trans('routes.new-consultation'), route('new-consultation'));

});

/**  ADMIN PANEL PART **/

// Home Dashboard
Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push(trans('routes.dashboard'), route('dashboard'));
});



// Users
Breadcrumbs::register('admin.users.index', function($breadcrumbs)
{

    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('routes.users'), route('admin.users.index'));
});
