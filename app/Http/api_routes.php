<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/


Route::get('telemed-consult-requests', ['as'=>"api.TelemedConsultRequest.index",'uses'=> 'Admin\TelemedConsultRequestAPIController@index']);
Route::post('telemed-consult-requests', ['as'=>"api.TelemedConsultRequest.",'uses'=> 'Admin\TelemedConsultRequestAPIController@store']);
Route::get('telemed-consult-requests/{TelemedConsultRequest}', ['as'=>"api.TelemedConsultRequest.show",'uses'=> 'Admin\TelemedConsultRequestAPIController@show']);
Route::put('telemed-consult-requests/{TelemedConsultRequest}', ['as'=>"api.TelemedConsultRequest.update",'uses'=> 'Admin\TelemedConsultRequestAPIController@update']);
Route::patch('telemed-consult-requests/{TelemedConsultRequest}', ['as'=>"api.TelemedConsultRequest.update",'uses'=> 'Admin\TelemedConsultRequestAPIController@update']);
Route::delete('telemed-consult-requests{TelemedConsultRequest}', ['as'=>"api.TelemedConsultRequest.destroy",'uses'=> 'Admin\TelemedConsultRequestAPIController@destroy']);


Route::get('telemed-centers', ['as'=>"api.TelemedCenters.index",'uses'=> 'TelemedCenterController@index' ]);
/* Список для заполнения Select */
Route::get('telemed-centers/list', ['as'=>"api.TelemedCenters.list",'uses'=> 'TelemedCenterController@getList']);

/* Работа со всеми  заявками */

Route::get('consultation-details',['as'=>"api.event.details",'uses'=> 'RequestController@getConsultationDetails' ] );

Route::get('consult-side-details/{id}', ['as'=>"api.events.consultSide.update",'uses'=> 'RequestController@consultSideDetails']);

Route::get('event-requests', ['as'=>"api.event",'uses'=> 'RequestController@index']);
Route::get('event-requests/{id}/{type}', ['as'=>"api.events.view",'uses'=> 'RequestController@view' ])
    ->where(['id' => '[0-9]+']);
Route::post('event-requests/create', ['as'=>"api.events.create",'uses'=> 'RequestController@create'] );
Route::delete('event-requests/cancel/{id}', ['as'=>"api.events.cancel",'uses'=> 'RequestController@eventCancel']);

// Cохранить изменения часть лпу-консультанта
Route::post('event-requests/edit-consultant/{id}', ['as'=>"api.events.consultSide.update",'uses'=>'RequestController@editConsultSide' ]);
// Сохранить изменения часть ЛПУ-абонента
Route::post('event-requests/edit-abonent/{id}', ['as'=>"api.events.abonentSide.update",'uses'=> 'RequestController@editAbonentSide']);

// Сохранить изменения совещания и семниара
Route::post('event-requests/edit/{id}', ['as'=>"api.events.update",'uses'=> 'RequestController@edit' ]);
// Докторы ТМЦ
Route::get('doctors/{id}', ['as'=>"api.doctors.index",'uses'=> 'RequestController@doctors' ]);
// вывести список ТМЦ пользователя
Route::get('gettmcs', ['as'=>"api.TelemedCenters.my",'uses'=> 'RequestController@getTmcs' ]);
// Вывести список всех ТМЦ
Route::get('get-all-tmcs', ['as'=>"api.TelemedCenters.my",'uses'=> 'RequestController@getAllTmcs' ]);
// Вывести список всех ЛПУ
Route::get('get-all-mcfs', ['as'=>"api.TelemedCenters.my",'uses'=> 'RequestController@getAllMcfs' ]);
// Вывести показания к пирменению
Route::get('ind_for_use', ['as'=>"api.indForUse.index",'uses'=>'RequestController@getIndicationsForUse' ]);

Route::get('get-patterns', ['as'=>"api.patterns.index",'uses'=> 'RequestController@getPatterns']);
Route::get('get-consultation-types', ['as'=>"api.consultation-types.index",'uses'=> 'RequestController@getConsultationTypes']);
Route::get('get-user', ['as'=>"api.users.view",'uses'=> 'RequestController@getCurrentUser']);

Route::get('event-files/{Id}',['as'=>"api.event-files",'uses'=> 'RequestController@getEventFiles']);

Route::get('get-conclusion/{id}',['as'=>"api.conclusions.view",'uses'=>  'RequestController@getConclusion']);

Route::post('create-conclusion/{id}', ['as'=>"api.conclusions.create",'uses'=> 'RequestController@createConclusion'] );
Route::post('update-conclusion/{id}', ['as'=>"api.conclusions.update",'uses'=> 'RequestController@updateConclusion']);

/* Delivery templates */
Route::get('get-delivery-group', ['as'=>"api.delivery-group.show",'uses'=> 'RequestController@getDeliveryGroups']);
Route::post('add-delivery-group', ['as'=>"api.delivery-group.create",'uses'=> 'RequestController@addDeliveryGroup']);
Route::post('delete-delivery-group/{id}', ['as'=>"api.delivery-group.delete",'uses'=> 'RequestController@deleteDeliveryGroup']);

Route::post('delete-delivery-group/{id}', ['as'=>"api.event.cancel",'uses'=> 'RequestController@eventCancel']);


Route::post('events/invitation/{request_id}', ['as'=>"api.event.invitation.decline",'uses'=> 'RequestController@setParticipationDecision']);

