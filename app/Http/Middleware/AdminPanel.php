<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;
use Acacha\AdminLTETemplateLaravel\AdminLTE;
use Request;

class AdminPanel
{
    /** Проверка доступа на админ панель
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::guest()) return redirect('login');
        if(Sentinel::inRole('admin')) return $next($request);
        return Redirect::route("homepage");
    }
}