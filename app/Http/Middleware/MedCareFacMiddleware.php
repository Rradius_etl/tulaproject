<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;
class MedCareFacMiddleware
{
    /**
     * Handle an incoming request.
     * Проверка на роль админа ЛПУ
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Sentinel::getUser();
        $res = \TmcRoleManager::hasRole("mcf_admin");
        if(!$res)
        {
            abort(401,"У вас нету доступа к этому разделу сайта!");
        }
        else
        {
            return $next($request);
        }
    }
}
