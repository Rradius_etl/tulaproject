<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
class TelemedRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = \TmcRoleManager::getRoles();
        if(count($roles)>0)
        {
            return $next($request);
        }
        else
        {
            abort(401,"У вас нету доступа к этому разделу сайта!");
        }
    }
}
