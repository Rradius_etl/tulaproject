<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
class TelemedAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $res = \TmcRoleManager::getRoles();
        if(in_array("mcf_admin",$res)||in_array("admin",$res))
        {
            return $next($request);
        }
        else
        {
            abort(401,"У вас нету доступа к этому разделу сайта!");
        }
    }
}
