<?php

namespace App\Http\Middleware;

use App\Models\Admin\TelemedCenter;
use Closure;
use Flash;
class TelemedAdminOrMfcAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->TelemedId;
        $tmc = TelemedCenter::findOrFail($id);
        $user = \Sentinel::getUser();
        $access = false;
        $tmcs = array_pluck(\TmcRoleManager::getTmcs(),"id");

        $roles = \TmcRoleManager::getRoles();
        $can = $user->tmcRoles;
        $ismfcadmin = $user->medcarefacs;

        if(!in_array($id,$tmcs)&&(!in_array('admin',$roles)||!in_array('mfc_admin',$roles)))
        {
            Flash::error("Вы не имеете право редактировать в этом разделе");

            return \Redirect::route('telemed-centers.index');

        }
        
        return $next($request);
    }
}
