<?php

namespace App\Console;

use App\Models\Admin\ConsultationType;
use App\Models\Admin\MedCareFac;
use App\Models\Admin\TelemedConsultRequest;
use App\Models\NotificationManager;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {



        // Уведомления для нерассмотренных заявкок на телемед. консультаций
        $schedule->call(function () {

            $types = ConsultationType::all();

            foreach($types as $type)
            {
                if($type->hours ==0 && $type->minutes == 0 && $type->days == 0)
                {

                }
                else
                {
                    $from = Carbon::now()
                        ->subDays($type->days)
                        ->subHours($type->hours)
                        ->subMinutes($type->minutes + 1);

                    $to = Carbon::now()
                        ->subDays($type->days)
                        ->subHours($type->hours)
                        ->subMinutes($type->minutes);

                    $consultsAdmin = DB::table("telemed_consult_requests as tcr")
                        ->join("telemed_requests_mcf_abonent as ab","ab.request_id","=","tcr.request_id")
                        ->join('consult_types as types',"ab.consult_type",'=','types.id')
                        ->whereBetween("ab.created_at",[ $from, $to])
                        ->whereNotNull('tcr.med_care_fac_id')
                        ->where("tcr.decision","=","pending")
                        ->where('ab.consult_type', $type->id)
                        ->where(function($q)
                        {
                            $q->where('tcr.mcf_notificated',"=", 0)->where("tcr.telemed_notificated",'=',0);
                        })
                        ->select("tcr.*","ab.telemed_center_id as abon_tmc_id","ab.consult_type as consult_type")
                        ->get();
                    //echo $from->toDateTimeString(). " -  ". $to->toDateTimeString(). "\n ";
                    //echo 'count:'.count($consultsAdmin)."\n ";
                    foreach($consultsAdmin as $c) {
                        if ($c->telemed_center_id == null) {
                            if ($c->mcf_notificated == 0) {
                                $mcf  = MedCareFac::find($c->med_care_fac_id);
                                $mcf_admin = $mcf->user;

                                if ($mcf_admin != null) {
                                    $link = link_to( \URL::to('/telemed-consult-requests/'.$c->request_id), "{$c->uid_code} - {$c->uid_year} - {$c->uid_id}");

                                    NotificationManager::createNotification("Заявка на телемедицинскую консультацию(".$type->name.") под номером {$link} не была рассмотрена вами   и не была передана ни в один из телемедицинских центров вашего ЛПУ", [$mcf_admin->id], false, true);

                                    $link = link_to( \URL::to('/tmc-telemed-consult-requests/'.$c->request_id), "{$c->uid_code} - {$c->uid_year} - {$c->uid_id}");
                                    NotificationManager::createNotificationToMCF("Заявка на телемедицинскую консультацию(".$type->name.") под номером {$link} не была рассмотрена вами   и не была передана ни в один из телемедицинских центров вашего ЛПУ", $mcf->id, false, true);

                                    DB::table('telemed_consult_requests')
                                        ->where('request_id', $c->request_id)
                                        ->update(['mcf_notificated' => 1]);
                                } else {

                                    $link = link_to( \URL::to('/tmc-telemed-consult-requests/'.$c->request_id), "{$c->uid_code} - {$c->uid_year} - {$c->uid_id}");
                                    NotificationManager::createNotificationToMCF("Заявка на телемедицинскую консультацию(".$type->name.") под номером {$link} не была рассмотрена вами   и не была передана ни в один из телемедицинских центров вашего ЛПУ", $mcf->id, false, true);

                                    DB::table('telemed_consult_requests')
                                        ->where('request_id', $c->request_id)
                                        ->update(['mcf_notificated' => 1]);
                                }
                            }
                        }
                    }
                    $consultsTMC = DB::table("telemed_consult_requests as tcr")
                        ->join("telemed_requests_mcf_abonent as ab","ab.request_id","=","tcr.request_id")
                        ->join('consult_types as types',"ab.consult_type",'=','types.id')
                        ->whereBetween("tcr.tmc_at",[$from, $to])
                        ->whereNotNull('tcr.med_care_fac_id')
                        ->where("tcr.decision","=","pending")
                        ->where('ab.consult_type', $type->id)
                        ->where(function($q)
                        {
                            $q->where('tcr.mcf_notificated',"=", 1)->where("tcr.telemed_notificated",'=',0)->whereNotNull("tcr.telemed_center_id");
                        })
                        ->select("tcr.*","ab.telemed_center_id as abon_tmc_id","ab.consult_type as consult_type")
                        ->get();


                    foreach($consultsTMC as $c) {
                        $link = link_to( \URL::to('/consultant-telemed-consult-requests/'.$c->request_id), "{$c->uid_code} - {$c->uid_year} - {$c->uid_id}");
                        NotificationManager::createNotificationForTmcUsers("Заявка на телемедицинскую консультацию({$type->name}) под номером {$link}  не была рассмотрена вами", $c->telemed_center_id, false, true);
                        DB::table('telemed_consult_requests')
                            ->where('request_id', $c->request_id)
                            ->update(['telemed_notificated' => 1]);
                    }
                }
            }

        })->everyMinute();
    }
}
