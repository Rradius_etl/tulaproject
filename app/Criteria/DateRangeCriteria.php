<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DateRangeCriteria
 * @package namespace App\Criteria;
 */
class DateRangeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public $attribute;
    public $from;
    public  $to;
    public function __construct($from , $to, $attribute ='created_at')
    {
        $this->from = Carbon::createFromFormat('Y-m-d',$from)->setTime(0, 0, 0);
        $this->to = Carbon::createFromFormat('Y-m-d',$to)->setTime(23, 59, 59);
        $this->attribute = $attribute;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereBetween($this->attribute, [$this->from, $this->to]);
        return $model;
    }
}
