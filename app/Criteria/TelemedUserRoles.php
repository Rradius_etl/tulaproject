<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedUserRoles implements CriteriaInterface
{


    public function setTelemedId($id){
        $this->mode = $id;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('telemed_center_id','=', $this->mode);
        return $model;
    }
}
