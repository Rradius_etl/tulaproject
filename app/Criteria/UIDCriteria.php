<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class UIDCriteria implements CriteriaInterface
{


    public function setParams($conssearch,$search)
    {
        $this->conssearch = $conssearch;
        $this->search = $search;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->conssearch != "")
        {
            $model = $model->where('med_care_fac_id',$this->conssearch);
        }



            if($this->search !="") {
                $model->where(function($q)
                {
                    $q->whereRaw('CONCAT(uid_code,"-",uid_year,"-",uid_id) like "%' . $this->search . '%"');
                    $q->orWhere("telemed_consult_requests.comment",'like' , '%'.$this->search . '%');
                    $decisions = [ 'agreed'                        => 'согласовано',
                        'pending'                        => 'на рассмотрении',
                        'rejected'                        => 'отклонено',
                        'canceled'                        => 'завершено'];
                    if(in_array($this->search, $decisions)) {
                        $q->orWhere(function($q1) use($decisions)
                        {
                               $a = array_flip($decisions);
                            $q1->whereRaw('telemed_consult_requests.decision = "' . $a[$this->search] . '"');
                        });
                    }
                    $q->orWhere(function($query1) {
                        $query1->whereExists(function ($query) {
                            $query->select(\DB::raw('1'))->from('telemed_event_requests as ter')->where(['ter.id' => \DB::raw('telemed_consult_requests.request_id')])
                                ->whereExists(function ($q) {
                                    $q->select(\DB::raw('1'))->from('telemed_centers as tmcs')->where(['tmcs.id' => \DB::raw('ter.telemed_center_id')])
                                        ->whereExists(function ($q) {
                                            $q->select(\DB::raw('1'))->from('med_care_facs as mcf')->where(['mcf.id' => \DB::raw('tmcs.med_care_fac_id')])->where("mcf.name", 'like', '%' . $this->search . '%');
                                        });;
                                });
                        });
                    });

                    $q->orWhere(function($query1){
                        $query1->whereExists(function ($query) {
                            $query->select(\DB::raw('1'))->from('telemed_requests_mcf_consultant as cons')->where(['cons.request_id' => \DB::raw('telemed_consult_requests.request_id')])
                                ->whereExists(function ($q) {
                                    $q->select(\DB::raw('1'))->from('doctors as d')->where(['d.id' => \DB::raw('cons.doctor_id')])->whereRaw('CONCAT(d.name," ",d.surname," ",d.middlename) like "%' . $this->search . '%"');
                                });
                        });
                    });

                    $q->orWhere(function($query1) {
                        $query1->whereExists(function ($query) {
                            $query->select(\DB::raw('1'))->from('telemed_requests_mcf_abonent as abon')->where(['abon.request_id' => \DB::raw('telemed_consult_requests.request_id')])
                                ->where(function($query2)
                                {
                                    $query2->where('abon.doctor_fullname', 'like', "%" . $this->search . "%")->orWhere("abon.patient_uid_or_fname",'like' , '%'.$this->search . '%')
                                    ->orWhere("abon.patient_uid_or_fname",'like' , '%'.$this->search . '%')
                                    ->orWhere("abon.desired_consultant",'like' , '%'.$this->search . '%')
                                    ->orWhere("abon.patient_questions",'like' , '%'.$this->search . '%');
                                });

                        });
                    });


                });

/*                $model = $model->whereExists(function ($query) {
                    $query->select(\DB::raw('1'))->from('telemed_requests_mcf_consultant as cons')->where(['cons.request_id' => \DB::raw('telemed_consult_requests.request_id')])
                        ->whereExists(function ($q) {
                            $q->select(\DB::raw('1'))->from('users as u')->where(['u.id' => \DB::raw('cons.appointed_person_id')])
                                ->where('u.position', 'like', "%" . $this->consspec . "%");;
                        });
                });*/

            }

        return $model;
    }
}
