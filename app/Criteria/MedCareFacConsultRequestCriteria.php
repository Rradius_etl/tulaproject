<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class MedCareFacConsultRequestCriteria implements CriteriaInterface
{


    public function setMedCareId($med_care_id)
    {
        $this->med_care_id = $med_care_id;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('med_care_fac_id','=', $this->med_care_id)->whereRaw("(select count(*) from telemed_requests_mcf_abonent as t where t.request_id = telemed_consult_requests.request_id)>0");
        return $model;
    }
}
