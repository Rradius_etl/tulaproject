<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedExstCriteria implements CriteriaInterface
{


    public function setTelemedId($telemed_centers)
    {
        $this->telemed_centers = $telemed_centers;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $array = [];
        foreach($this->telemed_centers as $t)
        {
            array_push($array,$t['id']);
        }
        $model = $model->whereIn("telemed_event_participants.telemed_center_id",$array);
        $model = $model->whereExists(function($q)
        {
            $q->select(\DB::raw('1'))->from('telemed_event_requests as tr')
                ->where('tr.id',\DB::raw('telemed_event_participants.request_id'))
                ->whereExists(function($q)
                {
                    $q->select(\DB::raw('1'))->from('telemed_centers as tc')
                        ->where('tc.id',\DB::raw('tr.telemed_center_id'))
                        ->whereNull('tc.deleted_at');
                });
        });
        return $model;
    }
}
