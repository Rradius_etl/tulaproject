<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class MedCareFacCriteria implements CriteriaInterface
{


    public function setMedCareFac_id($med_care_fac_id)
    {
        $this->med_care_fac_id = $med_care_fac_id;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where("med_care_fac_id","=",$this->med_care_fac_id);
        return $model;
    }
}
