<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class UserTelemedCriteria implements CriteriaInterface
{


    public function setTelemedId($telemed_centers)
    {
        $this->telemed_centers = $telemed_centers;
    }
    public function apply($model, RepositoryInterface $repository)
    {

        $model = $model->wherein("id",$this->telemed_centers);
        return $model;
    }
}
