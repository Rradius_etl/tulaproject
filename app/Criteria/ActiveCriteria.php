<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class ActiveCriteria implements CriteriaInterface
{


    public function setStatus($active)
    {
        $this->active = $active;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('active',true);
        return $model;
    }
}
