<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class UIDAbonCriteria implements CriteriaInterface
{

    public function setSearch($search)
    {
        $this->search = $search;
    }

    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;
    }
    public function setMCF($mcf)
    {
        $this->mcf = $mcf;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if($this->mcf != "")
        {
            $model = $model->whereExists(function ($query) {
                $query->select(\DB::raw('1'))->from("telemed_consult_requests as tcr")->where("tcr.request_id", \DB::raw("telemed_requests_mcf_abonent.request_id"))
                    ->where("tcr.med_care_fac_id",$this->mcf);
            });
        }
        if($this->search!="") {
            $model = $model->where(function($q)
            {
                /* TODO: TAUKE*/
                $search = strtolower(trim($this->search));

                $q->whereExists(function ($query) {
                    $query->select(\DB::raw('1'))->from("telemed_consult_requests as tcr")
                        ->where("tcr.request_id", \DB::raw("telemed_requests_mcf_abonent.request_id"))
                        ->whereRaw('CONCAT(tcr.uid_code,"-",tcr.uid_year,"-",tcr.uid_id) like "%' . $this->search . '%"');
                });
                $decisions = [ 'agreed'                        => 'согласовано',
                    'pending'                        => 'на рассмотрении',
                    'rejected'                        => 'отклонено',
                    'canceled'                        => 'завершено'];
                if(in_array($this->search, $decisions)) {
                    $q->orwhere(function ($query2) use($decisions) {


                        $query2->whereExists(function ($query) use($decisions) {
                            $query->select(\DB::raw('1'))->from("telemed_consult_requests as tcr")
                                ->where("tcr.request_id", \DB::raw("telemed_requests_mcf_abonent.request_id"));


                            $query->where(function ($q1) use ($decisions) {
                                $q1->whereExists(function ($query) use ($decisions) {
                                    $a = array_flip($decisions);
                                    $query->whereRaw('tcr.decision = "' . $a[$this->search] . '"');
                                });
                            });


                        });

                    });
                }

                $q->orwhere(function($query2){
                    $query2->whereExists(function ($query) {
                        $query->select(\DB::raw('1'))->from("telemed_consult_requests as tcr")->where("tcr.request_id", \DB::raw("telemed_requests_mcf_abonent.request_id"))
                            ->whereExists(function ($q) {
                                $q->select(\DB::raw('1'))->from('med_care_facs as mcf')->where(['mcf.id' => \DB::raw('tcr.med_care_fac_id')])->where("mcf.name", 'like', '%' . $this->search . '%');;
                            });
                    });
                });
                $q->orwhere(function($query2)
                {
                    $query2->where('doctor_fullname', 'like', "%" . $this->search . "%")->orWhere("patient_uid_or_fname",'like' , '%'.$this->search . '%')
                        ->orWhere("patient_uid_or_fname",'like' , '%'.$this->search . '%')
                        ->orWhere("desired_consultant",'like' , '%'.$this->search . '%')
                        ->orWhere("patient_questions",'like' , '%'.$this->search . '%');
                });

                $q->orWhere(function($query1){
                    $query1->whereExists(function ($query) {
                        $query->select(\DB::raw('1'))->from('telemed_requests_mcf_consultant as cons')->where(['cons.request_id' => \DB::raw('telemed_requests_mcf_abonent.request_id')])
                            ->whereExists(function ($q) {
                                $q->select(\DB::raw('1'))->from('doctors as d')->where(['d.id' => \DB::raw('cons.doctor_id')])->whereRaw('CONCAT(d.name," ",d.surname," ",d.middlename) like "%' . $this->search . '%"');
                            });
                    });
                });

                $q->orWhere(function($query1) {
                    $query1->whereExists(function ($query) {
                        $query->select(\DB::raw('1'))->from('telemed_consult_requests as cons')->where(['cons.request_id' => \DB::raw('telemed_requests_mcf_abonent.request_id')])
                            ->where(function($query2)
                            {
                                $query2->where('cons.comment', 'like', "%" . $this->search . "%");
                            });

                    });
                });
            });



        }
        return $model;
    }
}
