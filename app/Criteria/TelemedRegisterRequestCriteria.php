<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedRegisterRequestCriteria implements CriteriaInterface
{


    public function setMode($mode){
        $this->mode = $mode;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->mode=="all")
        {
                
        }else
        {
            $model = $model->where('decision','=', $this->mode);
        }

        return $model;
    }
}
