<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class InvitationsCriteria implements CriteriaInterface
{
    public function setTelemedId($telemed_center_id)
    {
        $this->telemed_center_id = $telemed_center_id;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('telemed_center_id','=', $this->telemed_center_id);
        return $model;
    }
}
