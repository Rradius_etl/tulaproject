<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class ReportRoleCriteria implements CriteriaInterface
{

    public $roles;
    public  $tmcs;

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function setTmcs($tmcs)
    {
        $this->tmcs = $tmcs;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        foreach ($this->roles as $role) {
            $model = $model->orWhere('roles', 'like', "%\"{$role}\"%")
                ->whereIn('tmc_id', $this->tmcs);
        }
        return $model;
    }
}
