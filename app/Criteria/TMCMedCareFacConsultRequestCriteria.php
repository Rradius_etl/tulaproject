<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TMCMedCareFacConsultRequestCriteria implements CriteriaInterface
{

    public function setMcfs($mcfs)
    {
        $this->mcfs = $mcfs;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('med_care_fac_id', $this->mcfs)->whereRaw("(select count(*) from telemed_requests_mcf_abonent as t where t.request_id = telemed_consult_requests.request_id)>0");
        $model = $model->whereRaw("(select count(*) from telemed_requests_mcf_consultant as c where c.request_id = telemed_consult_requests.request_id) = 0");
        $model = $model->whereNull("telemed_center_id");
        $model = $model->whereRaw("(select count(*) from telemed_event_requests as e where e.id = telemed_consult_requests.request_id and e.canceled = 0) = 1");
        return $model;
    }
}
