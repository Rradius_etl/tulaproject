<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedPatternCriteria implements CriteriaInterface
{


    public function setTelemedId($telemed_centers)
    {
        $this->telemed_centers = $telemed_centers;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $array = [];
        foreach($this->telemed_centers as $t)
        {
            array_push($array,$t['id']);
        }
        $model = $model->whereIn("telemed_center_id",$array);
        return $model;
    }
}
