<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedSearchCriteria implements CriteriaInterface
{


    public function setTelemedId($mcf_id,$trashed)
    {
        $this->mcf_id = $mcf_id;
        $this->trashed = $trashed;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->trashed == true)
        {
           $model = $model->onlyTrashed();
        }
        if($this->mcf_id != '')
        {
            $model = $model->where('med_care_fac_id','=', $this->mcf_id);
        }
        return $model;
    }
}
