<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria
 * @package namespace App\Criteria;
 */
class TelemedConsultantSideCriteria implements CriteriaInterface
{


    public function setTelemedId($telemed_centers)
    {
        $this->telemed_centers = $telemed_centers;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $a = [];
        foreach($this->telemed_centers as $t)
        {
            array_push($a, $t['id']);
        }
        $model = $model->whereIn("telemed_consult_requests.telemed_center_id",$a)->whereRaw("(select count(*) from telemed_requests_mcf_abonent as t where t.request_id = telemed_consult_requests.request_id)>0");
        return $model;
    }
}
