<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DateRangeCriteria
 * @package namespace App\Criteria;
 */
class DateRangeCriteriaPoll implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public $attribute;
    public $from;
    public  $to;
    public function __construct($from , $to, $attribute ='created_at')
    {
        $this->from = new Carbon($from);
        $this->to = new Carbon($to);
        $this->attribute = $attribute;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereBetween($this->attribute, [$this->from, $this->to]);
        return $model;
    }
}
