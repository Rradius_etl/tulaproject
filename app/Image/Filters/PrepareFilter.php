<?php
namespace App\Image\Filters;

/**
 *  Prepare file to save in file system
 * apply filter:
 *  $img->filter(new PrepareFilter()); or $img->filter(new PrepareFilter(800, 600));
*/
use Image;
use Intervention\Image\Filters\FilterInterface;

class PrepareFilter implements FilterInterface
{

    /**
     * Default size of filter effects
     */
    const DEFAULT_WIDTH = 150;
    const DEFAULT_HEIGHT = 100;

    /**
     * Max sizes
    */
    const WIDTH_MAX = 1600;
    const HEIGHT_MAX = 900;

    // width
    private   $w;

    // height
    private   $h;

    /**
     * Creates new instance of filter
     *
     * @param integer $w
     * @param integer $h
     */
    public function __construct($w = null, $h = null)
    {
        $this->w = is_numeric($w) ? intval($w) : self::DEFAULT_WIDTH;
        $this->h = is_numeric($h) ? intval($h) : self::DEFAULT_HEIGHT;
    }

    /**
     * Applies filter effects to given image
     *
     * @param  Intervention\Image\Image $image
     * @return Intervention\Image\Image
     */
    public function applyFilter(\Intervention\Image\Image $image)
    {
        // If large than WIDTH_MAX and HEIGHT_MAX ->  resize
        if ($image->height() > self::HEIGHT_MAX)
            $image->heighten(self::HEIGHT_MAX);

        if ($image->width() > self::WIDTH_MAX)
            $image->widen(self::WIDTH_MAX);



        return $image;
    }
}