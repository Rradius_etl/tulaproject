<?php

namespace App\Image\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Logo implements FilterInterface
{

    public  $w = 85; // set max width
    public  $h = 85; // set max height
    public function applyFilter(Image $image)
    {
        $this->checkDimension($image);
        return $image;
    }


    private function checkDimension(Image $image)
    {
        $changeWidth = false;
        $changeHeight = false;
        if ($image->width() > $this->w ) {
            $changeWidth = true;
        }

        if(  $image->height() > $this->h ) {
            $changeHeight = true;
        }

        if( $changeWidth ) {
            $this->changeWidth($image);
            $this->checkDimension($image);
        } elseif ( $changeHeight ) {
            $this->changeHeight($image);
            $this->checkDimension($image);
        } else {
            return $image;
        }

    }

    private function changeWidth(Image $image)
    {
        return $image->resize($this->w, null, function ($constraint) {
            $constraint->aspectRatio();
        });

    }
    private function changeHeight(Image $image)
    {
        return $image->resize(null, $this->h, function ($constraint) {
            $constraint->aspectRatio();
        });
    }

}