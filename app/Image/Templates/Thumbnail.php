<?php

namespace App\Image\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Thumbnail implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        if ($image->width() > 150)
            $image->widen(150);

        if ($image->height() > 100)
            $image->heighten(100);

        return $image;
    }
}