<?php

namespace App\Presenters;

use App\Transformers\CustomPaginationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomPaginationPresenter
 *
 * @package namespace App\Presenters;
 */
class CustomPaginationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomPaginationTransformer();
    }
}
