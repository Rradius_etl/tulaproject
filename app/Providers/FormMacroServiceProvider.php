<?php

namespace App\Providers;

use Html;
use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Html::macro('tag', function ($tag="div",$html="",$attributes=[]) {
            if ( $tag == null ) $tag = 'div';
            $obj = "<" . $tag;
            if ( is_array($attributes) && count( $attributes ) > 0 ) {
                foreach ( $attributes as $key => $value) {
                    if( is_string($key) && is_string($value) ) {
                        $obj .=  " $key=\"$value\" ";
                    }
                }


            }
            $obj .=  ">" . $html . "</$tag>";

            return $obj;
        });

        Html::macro('nullOrEmpty', function( $attributeValue ) {
            if( $attributeValue == null || $attributeValue=="" ) {
                return Html::tag('span',trans('backend.notset'), ['class' => 'text-danger']);

            } else {
                return Html::tag('span',$attributeValue);
            }


        });

        Html::macro('icon', function($class = ''){
            return '<i class="fa '. $class .'" ></i>';
        });
    }



    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
