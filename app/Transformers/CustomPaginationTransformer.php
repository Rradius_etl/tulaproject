<?php

namespace App\Transformers;

use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Support\HtmlString;

/**
 * Class CustomPaginationTransformer
 * @package namespace App\Transformers;
 */
class CustomPaginationTransformer extends BootstrapThreePresenter
{

    public function getActivePageWrapper($text)
    {
        return '<li class="active"><span>'.$text .'</span></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><a href="#">'.$text.'</a></li>';
    }

    protected function getPageLinkWrapper($url, $page, $rel = null)
    {
        if ($page == $this->paginator->currentPage()) {
            return $this->getActivePageWrapper($page);
        }

        return $this->getAvailablePageWrapper($url, $page, $rel);
    }
    protected function currentPage()
    {
        return $this->paginator->currentPage();
    }

    public function render()
    {
        if ($this->hasPages())
        {
            return new HtmlString(sprintf(
                '<ul class="pagination">%s %s %s</ul>',
                $this->getPreviousButton('<i class="fa fa-chevron-left" aria-hidden="true"></i>'),
                $this->getLinks(),
                $this->getNextButton('<i class="fa fa-chevron-right" aria-hidden="true"></i>')
            ));
        }

        return '';
    }
}
