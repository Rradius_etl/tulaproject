<?php

use Faker\Factory as Faker;
use App\Models\Admin\TelemedConsultRequest;
use App\Repositories\Admin\TelemedConsultRequestRepository;

trait MakeTelemedConsultRequestTrait
{
    /**
     * Create fake instance of TelemedConsultRequest and save it in database
     *
     * @param array $telemedConsultRequestFields
     * @return TelemedConsultRequest
     */
    public function makeTelemedConsultRequest($telemedConsultRequestFields = [])
    {
        /** @var TelemedConsultRequestRepository $telemedConsultRequestRepo */
        $telemedConsultRequestRepo = App::make(TelemedConsultRequestRepository::class);
        $theme = $this->fakeTelemedConsultRequestData($telemedConsultRequestFields);
        return $telemedConsultRequestRepo->create($theme);
    }

    /**
     * Get fake instance of TelemedConsultRequest
     *
     * @param array $telemedConsultRequestFields
     * @return TelemedConsultRequest
     */
    public function fakeTelemedConsultRequest($telemedConsultRequestFields = [])
    {
        return new TelemedConsultRequest($this->fakeTelemedConsultRequestData($telemedConsultRequestFields));
    }

    /**
     * Get fake data of TelemedConsultRequest
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTelemedConsultRequestData($telemedConsultRequestFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'comment' => $fake->word,
            'uid_code' => $fake->randomDigitNotNull,
            'uid_year' => $fake->randomDigitNotNull,
            'uid_id' => $fake->randomDigitNotNull,
            'decision' => $fake->word,
            'completed' => $fake->word,
            'med_care_fac_id' => $fake->randomDigitNotNull,
            'telemed_center_id' => $fake->randomDigitNotNull,
            'decision_at' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $telemedConsultRequestFields);
    }
}
