<?php

use App\Models\TelemedConsultRequest;
use App\Repositories\TelemedConsultRequestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TelemedConsultRequestRepositoryTest extends TestCase
{
    use MakeTelemedConsultRequestTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TelemedConsultRequestRepository
     */
    protected $telemedConsultRequestRepo;

    public function setUp()
    {
        parent::setUp();
        $this->telemedConsultRequestRepo = App::make(TelemedConsultRequestRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->fakeTelemedConsultRequestData();
        $createdTelemedConsultRequest = $this->telemedConsultRequestRepo->create($telemedConsultRequest);
        $createdTelemedConsultRequest = $createdTelemedConsultRequest->toArray();
        $this->assertArrayHasKey('id', $createdTelemedConsultRequest);
        $this->assertNotNull($createdTelemedConsultRequest['id'], 'Created TelemedConsultRequest must have id specified');
        $this->assertNotNull(TelemedConsultRequest::find($createdTelemedConsultRequest['id']), 'TelemedConsultRequest with given id must be in DB');
        $this->assertModelData($telemedConsultRequest, $createdTelemedConsultRequest);
    }

    /**
     * @test read
     */
    public function testReadTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $dbTelemedConsultRequest = $this->telemedConsultRequestRepo->find($telemedConsultRequest->id);
        $dbTelemedConsultRequest = $dbTelemedConsultRequest->toArray();
        $this->assertModelData($telemedConsultRequest->toArray(), $dbTelemedConsultRequest);
    }

    /**
     * @test update
     */
    public function testUpdateTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $fakeTelemedConsultRequest = $this->fakeTelemedConsultRequestData();
        $updatedTelemedConsultRequest = $this->telemedConsultRequestRepo->update($fakeTelemedConsultRequest, $telemedConsultRequest->id);
        $this->assertModelData($fakeTelemedConsultRequest, $updatedTelemedConsultRequest->toArray());
        $dbTelemedConsultRequest = $this->telemedConsultRequestRepo->find($telemedConsultRequest->id);
        $this->assertModelData($fakeTelemedConsultRequest, $dbTelemedConsultRequest->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $resp = $this->telemedConsultRequestRepo->delete($telemedConsultRequest->id);
        $this->assertTrue($resp);
        $this->assertNull(TelemedConsultRequest::find($telemedConsultRequest->id), 'TelemedConsultRequest should not exist in DB');
    }
}
