<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TelemedConsultRequestApiTest extends TestCase
{
    use MakeTelemedConsultRequestTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->fakeTelemedConsultRequestData();
        $this->json('POST', '/api/v1/telemedConsultRequests', $telemedConsultRequest);

        $this->assertApiResponse($telemedConsultRequest);
    }

    /**
     * @test
     */
    public function testReadTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $this->json('GET', '/api/v1/telemedConsultRequests/'.$telemedConsultRequest->id);

        $this->assertApiResponse($telemedConsultRequest->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $editedTelemedConsultRequest = $this->fakeTelemedConsultRequestData();

        $this->json('PUT', '/api/v1/telemedConsultRequests/'.$telemedConsultRequest->id, $editedTelemedConsultRequest);

        $this->assertApiResponse($editedTelemedConsultRequest);
    }

    /**
     * @test
     */
    public function testDeleteTelemedConsultRequest()
    {
        $telemedConsultRequest = $this->makeTelemedConsultRequest();
        $this->json('DELETE', '/api/v1/telemedConsultRequests/'.$telemedConsultRequest->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/telemedConsultRequests/'.$telemedConsultRequest->id);

        $this->assertResponseStatus(404);
    }
}
