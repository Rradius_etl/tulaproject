<?php
/**
 *  Website default configurations
*/
return [
    'site-name' => 'Региональный телемедицинский портал Тульской области',
    'meta-description' =>  'Региональный телемедицинский портал Тульской области',
    'meta-keywords' => 'телемедицина, Тульский область, ewe, more',
    'administrator-email' => 'Email администратора РТМП ТО',
    'slider-time' => '2',
    'slider-autoplay' => 'true',
    'welcome-enabled' => 'true',
    'welcome-text' => 'Добро пожаловать! Мы приветствуем вас на нашем сайте',
    //'' => '',
];