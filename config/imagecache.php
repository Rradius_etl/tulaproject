<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'images',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        public_path('files'),
        public_path('files/slideshow'),
        public_path('files/resources'),
        public_path('upload'),
        public_path('img'),
        public_path('images'),
        public_path('avatars')

    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => array(
        'avatar'=> 'App\Image\Templates\Avatar',
        'small-avatar'=> 'App\Image\Templates\SmallAvatar',
        'logo' => 'App\Image\Templates\Logo',
        'small' => 'App\Image\Templates\Small',
        'medium' => 'App\Image\Templates\Medium',
        'large' => 'App\Image\Templates\Large',
        'thumbnail' => 'App\Image\Templates\Thumbnail',
        'news-big' => 'App\Image\Templates\NewsBigThumbnail',
        'news-small' => 'App\Image\Templates\NewsThumbnail',
        'slide' => 'App\Image\Templates\Slide',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

);
