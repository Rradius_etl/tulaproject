<?php
/**
 *  Color Scheme
*/
return [
    'consultation' => '#0080c0',
    'meeting' => '#800040',
    'seminar' => '#008040',
];