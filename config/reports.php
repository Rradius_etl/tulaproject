<?php
/*
|--------------------------------------------------------------------------
| Prettus Repository Config
|--------------------------------------------------------------------------
|
|
*/
return [
    'rows' => [
        'report'  => [  // Статистические формы для роли «администратор»
            'num' => '№ по порядку',
            'mcf_name' => "Наименование учреждения здравоохранения",
            'tmc_name' => "Наименование ТМЦ",
            'tmc_address' => "Адрес ТМЦ",
            'terminal_number' => "E.164 ID (номер терминала)",
            'terminal_name' => "H.323 ID (название терминала)",
            'count' => "Общее количество видеоконференций",
            'meeting_count' => "Из них совещаний",
            'seminar_count' => "Из них обучающих семинаров",
            'consult_count' => "Из них клинических консультаций",
            'cons_norm' => "Норма клинических консультаций",
            'calls_count' => "Количество вызовов",
            'duration_minutes' => "Минут разговора",
            'duration_seconds' => "Секунд разговора"
        ],
        'reportinout'  => [  // Статистические формы для роли «администратор»
            'num' => '№ по порядку',
            'mcf_name' => "Наименование учреждения здравоохранения",
            'tmc_name' => "Наименование ТМЦ",
            'tmc_address' => "Адрес ТМЦ",
            'terminal_number' => "E.164 ID (номер терминала)",
            'terminal_name' => "H.323 ID (название терминала)",
            'count' => "Общее количество видеоконференций",
            'meeting_count' => "Из них совещаний",
            'seminar_count' => "Из них обучающих семинаров",
            'consult_count' => "Из них клинических консультаций",
            'cons_norm' => "Норма клинических консультаций",
            'calls_count' => "Количество вызовов",
            'duration_minutes' => "Минут разговора",
            'duration_seconds' => "Секунд разговора"
        ],
        'meeting'  =>   [  // Отчет по совещаниям в разрезе ТМЦ
            'num' => '№ по порядку',
            'mcf_name' => 'Наименование учреждения здравоохранения',
            'tmc_name' => 'Наименование ТМЦ',
            'tmc_address' => 'Адрес ТМЦ',
            'terminal_number' => 'E.164 ID (номер терминала)',
            'terminal_name' => 'H.323 ID (название терминала)',
            'count' => 'Количество совещаний',
            'high_count' => 'В том числе со степенью важности высокая',
            'medium_count' => 'В том числе со степенью важности средняя',
            'low_count' => 'В том числе со степенью важности низкая'
        ],
        'seminar'  =>  [  // Отчет по обучающим семинарам в разрезе ТМЦ
            'num' => '№ по порядку',
            'mcf_name' => 'Наименование учреждения здравоохранения',
            'tmc_name' => 'Наименование ТМЦ',
            'tmc_address' => 'Адрес ТМЦ',
            'terminal_number' => 'E.164 ID (номер терминала)',
            'terminal_name' => 'H.323 ID (название терминала)',
            'count' => 'Количество обучающих семинаров',
            'high_count' => 'В том числе со степенью важности высокая',
            'medium_count' => 'В том числе со степенью важности средняя',
            'low_count' => 'В том числе со степенью важности низкая'
        ],
        'consultation'  =>  [ //Отчет по клиническим консультациям в разрезе ТМЦ
            'num' => '№ по порядку',
            'mcf_name' => "Наименование учреждения здравоохранения",
            'tmc_name' => "Наименование ТМЦ", 
            'tmc_address' => 'Адрес ТМЦ',
            'terminal_number' => "E.164 ID (номер терминала)",
            'terminal_name' => "H.323 ID (название терминала)",
            'count' => "Количество клинических консультаций",
            'cons_norm' => "Норма клинических консультаций",
        ],
        'all'  =>  [ //Отчет по всем видеоконференциям по отдельному ТМЦ
            'num' => '№ по порядку',
            'mcf_name' => 'Наименование учреждения здравоохранения', // TODO: пустой. заполнить
            'tmc_name' => 'Наименование ТМЦ', // TODO: пустой. заполнить
            'tmc_address' => 'Адрес ТМЦ', // TODO: пустой. заполнить
            'create_date' => 'Дата подачи заявки',
            'start_date' => 'Дата проведения ВКС',
            'initiator_id' => 'Входящая/ исходящая ВКС',  // TODO: пустой. заполнить
            'event_type' => 'Вид ВКС',
            'consult_type' => 'Тип ТМК',
            'cons_uid' => 'Номер заявки',
            'medic' => 'Лечащий врач',
            'cons_tmc_name' => 'Наименование ТМЦ консультанта',
            'consult_doctor' => 'Врач консультант',
            'patient_indications_for_use_id' => 'Показание к применению',
            'title_seminar' => 'Тема обучающего семинара', // todo: тут изененил названия
            'title_meeting' => 'Тема совещания', // todo: тут изененил названия
            'part_count' => 'Количество участников',
            'importance' => 'Степень важности',
        ]
    ],
    'fieldTypes' => [
        'report'  => [  // Статистические формы для роли «администратор»
            'num' => 'iterator',
            'mcf_name' => "text",
            'tmc_name' => "text",
            'tmc_address' => "text",
            'terminal_number' => "text",
            'terminal_name' => "text",
            'count' => "numeric",
            'meeting_count' => "numeric",
            'seminar_count' => "numeric",
            'consult_count' => "numeric",
            'cons_norm' => "numeric",
            'calls_count' => "numeric",
            'duration_minutes' => "numeric",
            'duration_seconds' => "numeric"
        ],
        'reportinout'  => [  // Статистические формы для роли «администратор»
            'num' => 'iterator',
            'mcf_name' => "text",
            'tmc_name' => "text",
            'tmc_address' => "text",
            'terminal_number' => "text",
            'terminal_name' => "text",
            'count' => "numeric",
            'meeting_count' => "numeric",
            'seminar_count' => "numeric",
            'consult_count' => "numeric",
            'cons_norm' => "numeric",
            'calls_count' => "numeric",
            'duration_minutes' => "numeric",
            'duration_seconds' => "numeric"
        ],
        'meeting'  =>   [  // Отчет по совещаниям в разрезе ТМЦ
            'num' => 'iterator',
            'mcf_name' => 'text',
            'tmc_name' => 'text',
            'tmc_address' => 'text',
            'terminal_number' => 'text',
            'terminal_name' => 'text',
            'count' => 'numeric',
            'high_count' => 'numeric',
            'medium_count' => 'numeric',
            'low_count' => 'numeric'
        ],
        'seminar'  =>  [  // Отчет по обучающим семинарам в разрезе ТМЦ
            'num' => 'iterator',
            'mcf_name' => 'text',
            'tmc_name' => 'text',
            'tmc_address' => 'text',
            'terminal_number' => 'text',
            'terminal_name' => 'numeric',
            'count' => 'numeric',
            'high_count' => 'numeric',
            'medium_count' => 'numeric',
            'low_count' => 'numeric'
        ],
        'consultation'  =>  [ //Отчет по клиническим консультациям в разрезе ТМЦ
            'num' => 'iterator',
            'mcf_name' => 'text',
            'tmc_name' => 'text',
            'tmc_address' => 'text',
            'terminal_number' => 'text',
            'terminal_name' => 'text',
            'count' => 'numeric',
            'cons_norm' => "numeric",

        ],
        'all'  =>  [ //Отчет по всем видеоконференциям по отдельному ТМЦ
            'num' => 'iterator',
            'mcf_name' => 'text',
            'tmc_name' => 'text',
            'tmc_address' => 'text',
            'create_date' => 'date',
            'start_date' => 'date',
            'initiator_id' => 'expression',  // TODO: пустой. заполнить
            'event_type' => 'text',
            'consult_type' => 'text',
            'cons_uid' => 'text',
            'medic' => 'text',
            'cons_tmc_name' => 'text',
            'consult_doctor' => 'text',
            'patient_indications_for_use_id' => 'text',
            'title_seminar' => 'text', // todo: тут изененил названия
            'title_meeting' => 'text', // todo: тут изененил названия
            'part_count' => 'numeric',
            'importance' => 'options',
        ]
    ],
    /**
     *  Тут настройки
    **/
    'options'   => [
        'defaults' => [
            'report' => [
                'mcf_name',
                'tmc_name',
                'count',
            ],
            'reportinout' => [
                'mcf_name',
                'tmc_name',
                'count',
            ],
            'meeting' => [
                'mcf_name',
                'tmc_name',
                'count',
            ],
            'seminar' => [
                'mcf_name',
                'tmc_name',
                'count',
            ],
            'consultation' => [
                'mcf_name',
                'tmc_name',
                'count',
            ],
            'all' => [
                'mcf_name',
                'tmc_name',
            ]
        ],
        'names' => [
            'report' =>'Статистика проведения видеоконференции',
            'reportinout' =>'Статистика проведения видеоконференции(входящие и исходящие)',
            'meeting' => 'Отчет по совещаниям',
            'seminar' => 'Отчет по обучающим семинарам',
            'consultation' => 'Отчет по клиническим консультациям',
            'all' => 'Отчет по всем видам видеоконференций'
        ]
    ]

];