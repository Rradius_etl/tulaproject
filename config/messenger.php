<?php

return [

    'user_model' => \App\User::class,

    'message_model' => \App\Models\UserMessage::class,

    'participant_model' => Cmgmyr\Messenger\Models\Participant::class,

    'thread_model' => \App\Models\MessagesThread::class,

    /**
     * Define custom database table names.
     */
    'messages_table' => 'messages',

    'participants_table' => 'participants',

    'threads_table' => 'threads',
];
