var addUrlParam = function(uri, key, value){
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }


};

$(document).ready(function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $('.input-group-addon.for-date').click(function (e) {
        var a = $(this).siblings('input').trigger('click');
    })

    $('input[type=file]').on('change', function () {
        var $list = $(this).next('.files-list');
        if( $list.length > 0) {
            $list.empty();
            $.each(this.files, function(key, val) {
                console.log(val)
                var $li = $('<li />', {
                    text: val.name
                })
                $list.append($li);
            });
        } else {
            var $list = $('<ul />', {
                'class': 'files-list',
            })
            $.each(this.files, function(key, val) {
                console.log(val)
                var $li = $('<li />', {
                    text: val.name
                })
                $list.append($li);
            });
            console.info(this.files)
            $(this).after($list);
        }


    })


})