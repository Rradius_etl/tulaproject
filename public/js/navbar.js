jQuery( document ).ready(function( $ ) {
    if(!(jsCookies.check("lineHeight")))
    {
        jsCookies.set("lineHeight", 0, 7);
    }

    var fontScale = jsCookies.get("fontScale") || 0;
    if( fontScale != 0 )
    fontRescale(fontScale);
    jsCookies.set("fontScale", fontScale, 7);
});

jQuery("#goToOriginal").on('click', function(){
    var url = window.location.href;
    console.log(url)
    window.location.reload();
});

function fontReset() {
    jQuery("*").not(".ignore-all, .ignore-font, nav.navbar-custom, nav.navbar-custom *").each(function(idx) {
        jQuery(this).css('font-size', '');
        jQuery(this).css('line-height', '');
    })
    jsCookies.set("fontScale", 0, 7);
}
function fontRescale(fontScale) {
    var fontSize;
    var lineHeightScale = jsCookies.get("lineHeight");
    jQuery("*").not(".ignore-all, .ignore-font, nav.navbar-custom, nav.navbar-custom *").each(function(idx) {
        fontSize = jQuery(this).css('font-size');

        fontSize = parseInt(fontSize) + 2 * fontScale;

        lineHeight = fontSize* Math.pow(1.1, lineHeightScale);
        jQuery(this).animate({'line-height': lineHeight + 'px'}, 0);

        fontSize = fontSize + 'px';
        jQuery(this).animate({fontSize: fontSize}, 250);
    });

    var totalScale = parseInt(jsCookies.get("fontScale"));
    jsCookies.set("fontScale", totalScale + fontScale, 7);
}


function setColors(color) {
    jQuery("*").not(".ignore-all, .ignore-color, nav.navbar-custom, nav.navbar-custom *").removeClass("impaired-white impaired-black impaired-darkblue").addClass(color);
    jsCookies.set("colorScheme", color, 7);
}

function setImageClass(klass) {
    var selectors = "img, .impaired-img";
    jQuery(selectors).removeClass("hidden gray").addClass(klass);
    if (klass == 'hidden') {
        jQuery(selectors).css("display", "").attr('style', function(i,s) { return s + ' display: none !important' });
    } else {
        jQuery(selectors).show();
    }
    klass = typeof klass == "undefined" ? "" : klass;
    jsCookies.set("ImageClass", klass, 7);
}

function setLineHeight(diff) {
    var lineHeight;
    var totalScale = parseInt(jsCookies.get("lineHeight")) + diff;
    if ( (totalScale > 14) || (totalScale < -7)) {
        return 0
    }
    jQuery("*").not(".ignore-all, .ignore-line-height, nav.navbar-custom, nav.navbar-custom *").each(function(idx) {
        lineHeight = jQuery(this).css('font-size');
        lineHeight = parseFloat(lineHeight) * Math.pow(1.1, totalScale);
        lineHeight = lineHeight + 'px';
        jQuery(this).animate({'line-height': lineHeight}, 250);
    });
    jsCookies.set("lineHeight", totalScale, 7);
}

jQuery('#audio_btn').on('click', function () {
    document.getElementById("audio").play();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return '';
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

jQuery("#robokassaPay").val("Оплатить подписку для " + getParameterByName('hostname'));
jQuery("#robokassaPayHostname").val(getParameterByName('hostname') + getParameterByName('path'));



// create my jsCookies function
var jsCookies = {

    // this gets a cookie and returns the cookies value, if no cookies it returns blank ""
    get: function(c_name) {
        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    },

    // this sets a cookie with your given ("cookie name", "cookie value", "good for x days")
    set: function(c_name, value, expiredays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays);
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/";
    },

    // this checks to see if a cookie exists, then returns true or false
    check: function(c_name) {
        c_name = jsCookies.get(c_name);
        if (c_name != null && c_name != "") {
            return true;
        } else {
            return false;
        }
    }

};
// end my jsCookies function

// USAGE - get    ::   jsCookies.get("cookie_name_here");  [returns the value of the cookie]
// USAGE - set    ::   jsCookies.set("cookie_name", "cookie_value", 5 );  [give name, val and # of days til expiration]
// USAGE - check  ::   jsCookies.check("cookie_name_here");  [returns only true or false if the cookie exists or not]


var MYLIBRARY = MYLIBRARY || (function(){
        var _args = {}; // private

        return {
            init : function(Args) {
                _args = Args;
            },
            get : function(i) {
                return _args[i];
            }
        };
    }());

MYLIBRARY.init([]);


function toggleImpaired() {
    $('.navbar-custom').toggle()
}