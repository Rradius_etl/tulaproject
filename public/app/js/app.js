var app = angular.module('App', [
    'ui.calendar',
    'ngDialog',
    'treeControl',
    'ui.bootstrap.datetimepicker',
    'ui.dateTimeInput',
    'ngFileUpload',
    'angularjs-dropdown-multiselect',
    'vAccordion',
    'ngAnimate',
    'selectize'

]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);

//moment.tz.setDefault('local');

app.controller('CalendarController', function ($scope, $rootScope, Upload, API, $controller, $compile, $http, $q, uiCalendarConfig, ngDialog) {
    moment.locale('ru');

    $rootScope.calendarSlotsDuration = '00:30:00'; //  '00:30:00' duration
    /** MAIN CONFIGS*/
    $scope.config = {
        selectedEventType: 'all',
        currentMonth: moment().format('M'),
        currentYear: moment().format('YYYY'),
    };

    $scope.tmcUsers = [], $scope.medical_profiles = [];
    $scope.treeOptions = {
        multiSelection: true,
        nodeChildren: "telemed_centers",
        dirSelectable: false,
        allowDeselect: true,
        isSelectable: function (node) {
            return !node.disabled;
        },
        injectClasses: {
            "ul": "c-ul",
            "li": "c-li",
            "liSelected": "c-liSelected",
            "iExpanded": "c-iExpanded",
            "iCollapsed": "c-iCollapsed",
            "iLeaf": "c-iLeaf",
            "label": "c-label",
            "labelSelected": "c-labelSelected"
        }

    };
    $scope.multiParticipantsOptions = {
        displayProp: 'name',
        enableSearch: true,
        showCheckAll: false,
        translationTexts: {
            "checkAll": "Выбрать все",
            "uncheckAll": "Убрать все",
            "selectionCount": "выбрано",
            "searchPlaceholder": "Поиск по имени ТМЦ",
            "buttonDefaultText": "Выбрать участников",
            "dynamicButtonTextSuffix": "выбран",

        }
    };
    $scope.datepickerOptions = {
        dropdownSelector: '#date-dropdown',
        minView: 'day'
    };
    $scope.datepickerBirthday = {
        dropdownSelector: '#date-dropdown',
        minView: 'day',
        startView: 'year'
    };
    $scope.datetimepickerOptions = {
        dropdownSelector: '#datetime-dropdown',
    };


    /*$scope.attachFiles = function (files, file, newFiles, duplicateFiles, invalidFiles, event) {
        console.log('files',files );
        console.log('file', file);
        console.log('newFiles', newFiles);
        console.log('duplicateFiles', duplicateFiles);
        console.log('invalidFiles', invalidFiles);
        console.log('event', event);

    }*/
	
	$scope.patientBirthdayFilter = function($dates) {
	  const todaySinceMidnight = new Date();
		todaySinceMidnight.setUTCHours(0,0,0,0);
		$dates.filter(function (date) {
		  return date.utcDateValue > todaySinceMidnight.getTime();
		}).forEach(function (date) {
		  date.selectable = false;
		});
	};
	
    $scope.filterDatetimepicker = function ($dates) {
        var today = moment().startOf('day');
        angular.forEach($dates, function (el, index) {
            var date = moment(el.utcDateValue);
            if (date <= today)
                el.selectable = false;
            //console.log('data', date);
        })
    };

    $scope.genders = [
        {id: "male", name: "мужской"},
        {id: "female", name: "женский"},
    ];

    /* Получить показания к применению */
    API.getIndForUse().then(function (data) {
        $scope.ind_for_use = data;
        console.log('ind_for_use', $scope.ind_for_use);
    });
    // Получить типы тмк
    API.getConsultationTypes().then(function (data) {
        $scope.consult_types = data;
        console.info('consult_types ', data)
    });;
    $scope.eventTypesArray = [
        {id: "consultation", name: "Клинические консультации"},
        {id: "meeting", name: "Совещания"},
        {id: "seminar", name: "Семинары"}
    ];

    $scope.decisionsArray = [
        {"id": "pending", "name": "На рассмотрении", 'class': 'label label-success'},
        {"id": "agreed", "name": "Принято", 'class': 'label label-primary'}, // TODO: Исправить в функции  $scope.checkForStatus
        {"id": "rejected", "name": "Отклонено", 'class': 'label label-danger'},
        {"id": "canceled", "name": "Отменено", 'class': 'label label-danger'},
        {"id": "completed", "name": "Завершено", 'class': 'label label-success'}
    ];

    $scope.importanceArray = [
        {"id": "low", "name": "низкая", 'class': 'label label-success'},
        {"id": "medium", "name": "средняя", 'class': 'label label-info'},
        {"id": "high", "name": "высокая", 'class': 'label label-danger'},
    ];


    $scope.selectedCalendars = [];
    $scope.participantsArray = [];

    $scope.myCalendars = [
        {id: 'my', name: 'Мои календари', telemed_centers: $scope.eventTypesArray},
    ];

    $scope.singleConfig = {
        create: false,
        valueField: 'id',
        labelField: 'name',
        maxItems: 1,
        searchField: 'name'
    };

    $scope.selectizeConfig = {
        render: {
            option_create: function (data, escape) {
                return '<div class="create">Ручной ввод: <strong>' + escape(data.input) + '</strong>&hellip;</div>';
            }
        },
        create: true,
        sortField: 'name',
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        maxItems: 1

    };

    /**  Вытащить статус **/
    $scope.checkForStatus = function (oldRequest) {
        //console.log(oldRequest);

        // Если тип ВКС консультация то просто проверяем на статус
        if(oldRequest.event_type == 'consultation') {
            if ( oldRequest.canceled == true ) {
                return 'canceled';
            } else if ( oldRequest.completed == true ) {
                return 'completed';
            } else {
                return oldRequest.decision;
            }
        } else {
            // Если тип ВКС семинар или совещание
            if ( oldRequest.canceled == true ) {
                return 'canceled';
            } else if ( oldRequest.completed == true ) {
                return 'completed';
            } else if(oldRequest.decision == 'agreed') {
                return 'pending';
            } else {
                return oldRequest.decision;
            }
        }

    };
    /**
     * Отказаться от участия в данной мероприятии
     *  **/
    $scope.DeclineInvitation = function (oldRequest) {

        API.setInvitationStatus(oldRequest.id, {tmc_id: $rootScope.SelectedTMC, decision: 'rejected'}).then( function (data) {

            ngDialog.openConfirm({
                template: 'warning-dialog',
                className: 'ngdialog-theme-plain',
                data: {message: data.message}
            })
            $scope.newRequest.part_decision = "rejected";
        })

    };
    /**
     * Отказаться от участия в данной мероприятии
     *  **/
    $scope.AcceptInvitation = function (oldRequest) {

        API.setInvitationStatus(oldRequest.id, {tmc_id: $rootScope.SelectedTMC, decision: 'agreed'}).then( function (data) {
            console.info('qqqq-data', data)
            ngDialog.openConfirm({
                template: 'warning-dialog',
                className: 'ngdialog-theme-plain',
                data: {message: data.message}
            });
            $scope.newRequest.part_decision = "agreed";
        })

    };
    /** Узнать отклонил ли клиент запрос на участие в семинаре или совещании */
    $scope.checkForInvitationStatus = function (oldRequest) {
        //console.log('checkForInvitationStatus', oldRequest);
        return oldRequest.part_decision;
    };
    /** Submit Form**/
    $scope.submitForm = function (files, newRequest, event, side, eventFiles) {

        console.log('event is submit', event);

        var
            model = angular.copy(newRequest),
            data,
            url,
            eventDate,
            eventFiles = (typeof eventFiles !== undefined ) ? eventFiles : [];

        side = (typeof  side !== undefined ) ? side : null;

        event.isNew = (typeof  event.isNew !== undefined ) ? event.isNew : true;
        console.log(newRequest);


        model.deletedfiles = [];
        console.log('eventFiles', eventFiles)
        angular.forEach(eventFiles, function (value, key) {
            if (value.deleted == true) {
                this.push(value.id)
            }
        }, model.deletedfiles);
        console.log('model.deletedfiles', model.deletedfiles)

        /* Если консультация то еще проевряем ЛПУ-абонент или ЛПУ-консультант */
        if (model.event_type == 'consultation') {

            model.patient_birthday = moment(model.patient_birthday).format('DD.MM.YYYY');


            if (event.isNew) {

                data = model;
                eventDate = moment(model.date);
                model.desired_date = moment(model.date).format('DD.MM.YYYY');
                model.desired_time = moment(model.date).format('HH:mm');
                url = 'new-consultation';
                console.log('eventDate', model.date, eventDate, model.desired_date, model.desired_time)
                console.log('model.patient_birthday', model.patient_birthday, model.patient_birthday)

            } else {

                if (side == 'consultant-side') {
                    model.confirm = true;
                    ( model.planned_date == null ) ? model.planned_date = '' : model.planned_date = moment(model.planned_date).format('YYYY-MM-DD HH:mm');
                    eventDate = model.planned_date;

                    url = '/api/event-requests/edit-consultant/' + model.id;

                } else if (side == 'abonent-side') {
                    eventDate = moment(model.date);
                    model.desired_date = moment(model.date).format('DD.MM.YYYY');
                    model.desired_time = moment(model.date).format('HH:mm');
                    url = '/api/event-requests/edit-abonent/' + model.id
                }
                console.log('url-abonent', url)
                delete model.id;
                delete model.date;
                data = {
                    data: model
                };
            }
            console.log('side', side)

            delete model.date;

        } else {
            eventDate = moment(angular.copy(model.date));
            model.date = moment(model.date).format('DD.MM.YYYY HH:mm');
            model.telemed_center_id = $rootScope.SelectedTMC;

            data = {
                data: model
            };

            if (event.isNew)
                url = 'api/event-requests/create';
            else
                url = 'api/event-requests/edit/' + model.id;
        }

        data.file = files;

        console.log('обхект', data);

        $scope.postData(url, data, event, eventDate);

    };

    /** Обработка нажатия кнопки Отклонить  */
    $scope.Decline = function (event) {
        console.log('event in decline', event)
        var url = '/api/event-requests/edit-consultant/' + $scope.newRequest.id;
        var data = {
            files: null,
            data: {
                reject: true,
                coordinator_fullname: $scope.newRequest.coordinator_fullname
            }
        };
        $scope.postData(url, data, event);
    };

    /** Отмена завяка и удаление из календаря */
    $scope.cancelMyEvent = function (newRequest) {

        ngDialog.openConfirm({
            template: 'warning-dialog',
            className: 'ngdialog-theme-plain',
            data: {
                message: 'Вы точно хотите отменить Вашу заявку?',
                showConfirmButton: true,
                confirmButtonText: 'Да, Отменить заявку',
                confirmValue: newRequest.id
            }
        }).then(function (event_id) {
            // perform delete operation
            console.log('Удалиить', event_id);
            API.cancelMyEvent(event_id);
            $scope.newRequest.decision = 'canceled';

            angular.forEach(uiCalendarConfig.calendars, function (value, key) {
                if (key !== 'miniCalendar') {
                    var _cal = uiCalendarConfig.calendars[key];
                    _cal.fullCalendar('removeEvents', function (calEvent) {
                        return calEvent.event_id == event_id;
                    });



                }
            });
            ngDialog.closeAll();

        }, function (value) {
            console.log('просто закрыьт')

        });


    }
    $scope.postData = function (url, data, event, date) {
        console.log('postDATA', data);
        console.log('postDATA date', date);


        Upload.upload({
            url: url,
            data: data,
            cache: true,
            method: 'POST'
        }).then(function (resp) {
            delete $scope.newRequest.errors;
            console.log(resp);


            if (resp.status == 200) {
                ngDialog.closeAll();
                ngDialog.openConfirm({
                    template: 'warning-dialog',
                    className: 'ngdialog-theme-plain',
                    data: {message: resp.data.message}
                });
            }

            /** Если объект события каледнадря имеется то меняем его.
             *  А если нет события то создаем новый объект и ставим его в календарь как новое событие
             * */

            if (angular.isDefined(event.calEvent)) {

                if( typeof  date !== 'undefined') {
                    event.calEvent.start = moment(date);
                    uiCalendarConfig.calendars[event.calendarName].fullCalendar('updateEvent', event.calEvent);
                }


            } else {
                var newCalEvent = {
                    event_id: ( $scope.newRequest.event_type == 'consultation') ? $scope.newRequest.request_id : resp.data.id,
                    title: ( $scope.newRequest.event_type == 'consultation') ? $scope.newRequest.request_uid : $scope.newRequest.title,
                    importance: $scope.newRequest.importance,
                    start: date,
                    end: date.clone().add('1', 'hours'),
                    stick: true,
                    editable: false,
                    type: $scope.newRequest.event_type,
                    className: classPrefix + $scope.newRequest.event_type,
                    timezone: false,
                    direction: 'out'
                };

                uiCalendarConfig.calendars[event.calendarName].fullCalendar('renderEvent', newCalEvent, true);

            }


        }, function (resp) {
            if (resp.status == 422) {

                $scope.newRequest.errors = resp.data.errors;
 
                console.error('FORM SEND ERROR', $scope.newRequest.errors)
            } else {
                alert('Что то пошло не так');
            }

        }, function (evt) {

            if (typeof evt.config.data.file !== 'undefined') {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            }

        });
    };

    /**===============    ТУТ НАЧИНАЕТСЯ ФУНКЦИОНАЛ  ============================  */

    $scope.selectCalendar = function (node, selected) {
        if (selected) {
            /* Если выбран то показать календарь*/
            if (node.name != 'Остутствует')
                $scope.newCalendar(node);

        } else {
            /* Если убрали флажок то убрать и календарь*/
            $scope.destroyCalendar(node.id);
        }
    };


    /**
     * Показать список всех участников
     * @var all - все
     * @var selected - только выбранные
     * @var saveMode - Режим сохранения групп рассылки
     * */
    $scope.openParticipantsModal = function (all, selected, saveMode) {

        if (selected.length > 0) {
            ngDialog.open({
                template: 'participants-dialog',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                controller: 'DeliveryGroupController',
                resolve: {
                    data: function () {
                        return {participants: participantsFilter(all, selected), saveMode: saveMode}
                    }
                }
            });
        } else {
            ngDialog.openConfirm({
                template: 'warning-dialog',
                className: 'ngdialog-theme-plain',
                data: {message: 'Нет ни одного участника. Сперва добавьте участников'}
            })
        }


    };
    $scope.selectDeliveryGroup = function (all) {
        API.getDeliveryGroup().then(function (resp) {
            console.log('all del gro', resp);
            ngDialog.openConfirm({
                template: 'get-participants-dialog',
                className: 'ngdialog-theme-plain',
                controller: 'DeliveryGroupController',
                resolve: {
                    data: function () {
                        return {templates: resp.data, participantsArray: all}
                    }
                }
            })

        })

    }

    $rootScope.loadDeliveryPattern = function (tmcs) {
        console.log('loadDeliveryPattern', tmcs)
        $scope.newRequest.participants = tmcs;
    }

    /** Инициализировать мой календарь */
    $scope.initMyCalendar = function (tmc_id) {
        var Workspace = angular.element('#workspace');
        var MyCalendarEl = jQuery('<div/>', {
            'class': 'calendar',
            'data-text': 'Мой календарь',
            'data-id': 0,
            'calendar': 'myCalendar',
            'ui-calendar': 'uiConfig.calendar',
            'ng-model': 'eventSources'
        });
        Workspace.append(MyCalendarEl);
        $compile(MyCalendarEl)($scope);

        var params = {
            my_calendar: 1,
            telemed_id: tmc_id
        };

        $scope.getEventsToCalendar('myCalendar', params);


    }


    /** Создание нового календаря начинается от этой фнукции */
    $scope.newCalendar = function (tmc) {
        console.info('NEW CALENDAR')
        var calendars = angular.element('#workspace .calendar');



        if (calendars.length == 10) {

            ngDialog.openConfirm({
                template: 'warning-dialog',
                className: 'ngdialog-theme-plain',
                data: {message: 'Создание большого количества календарей может привести к снижению производительности'}
            })

        } else {

            var last = calendars.last();
            var ID = tmc.id;
            var newCalendarID = 'calendar' + ID;
            var CalendarEl = jQuery('<div/>', {
                'class': 'calendar',
                'data-text': tmc.name,
                'data-id': ID,
                'calendar': newCalendarID,
                'ui-calendar': 'uiConfig.calendar'
            });


            last.after(CalendarEl);

            //var newLast = angular.element('#workspace .calendar').last();
            //var newCalendar = newLast.fullCalendar($scope.uiConfig.calendar);

            $compile(CalendarEl)($scope);

            var params = {
                tmc_id: ID
            };

            console.log('uiCalendarConfig.calendars', uiCalendarConfig.calendars)
            $scope.getEventsToCalendar(newCalendarID, params);
            calcCalendarWidth();
        }
    }
    /** Уничтожить календарь */
    function destroyCalendar(id) {
        var calendarID;
        if (isInt(id)) {
            calendarID = 'calendar' + id;
        } else {
            calendarID = id;
        }

        var numberID = calendarID.replace('calendar', '');

        /** TODO: Сделать удаление из масиисва $scope.selectedCalendars. Не работет пока что*/
        angular.forEach($scope.selectedCalendars, function (value, key) {
            if (value.id == numberID) {
                this.slice(key, 1);
            }
        }, $scope.selectedCalendars);

        if (angular.isDefined(uiCalendarConfig.calendars[calendarID])) {
            uiCalendarConfig.calendars[calendarID].fullCalendar('destroy');
            uiCalendarConfig.calendars[calendarID].remove();
            delete uiCalendarConfig.calendars[calendarID];
        }
        calcCalendarWidth();
    }

    $scope.destroyCalendar = destroyCalendar;


    /**  Получить список Телемед Центров*/
    API.getMedCareFacs().then(function (data) {

        $scope.allTelemedCenters = data;

    });

    /** Apply event filter to calendar  / Фильтровать по типу */
    $scope.applyCalendarEventFilter = function (type, $event) {
        var el = $event.currentTarget;

        $(el).siblings().removeClass('selected');
        $(el).addClass('selected');

        console.log('applyCalendarEventFilter', type)
        $scope.config.selectedEventType = type;

        angular.forEach(uiCalendarConfig.calendars, function (value, key) {
            if (key != 'miniCalendar')
                uiCalendarConfig.calendars[key].fullCalendar('rerenderEvents');
        });
    }


    /** Полуение и добавление событии в календарь */
    $scope.getEventsToCalendar = function (calendarID, params) {
        setTimeout(function () {
            var _this = uiCalendarConfig.calendars[calendarID];
            console.info(params, uiCalendarConfig.calendars);
            console.log(_this);

            if( typeof _this != 'undefined')
                API.getEvents(params, $scope.config, calendarID).then(function (source) {
                    _this.fullCalendar('removeEvents');
                    _this.fullCalendar('addEventSource', source);
                    _this.fullCalendar('rerenderEvents');
                })

            var view = _this.fullCalendar('getView');
            var diff = view.end.diff(view.start, 'days');
            $scope.centerDate = view.start.clone().add(parseInt(diff / 2), 'days');
        }, 300)


    };

    $scope.getUserFName = function (user) {
        var initiator = [user.name, user.middlename, user.surname].join(' ');
        return initiator || 'Ошибка, Инициатор не указан';
    }

    $scope.showEventDetails = function (calEvent, jsEvent, view) {
        var template, controller, calendarName = view.el.closest('[ui-calendar]').attr(('calendar'));


        // console.log(calEvent)
        var event_type = ( calEvent.type == 'consultation') ? calEvent.type : 'event';

        API.getEventDetails(calEvent.event_id, event_type).then(function (data) {


            console.log('d:' + calEvent.direction, data);
            if (calEvent.type == 'consultation') {

                if (calEvent.direction == 'undefined') {
                    $scope.readMode = true;
                    $scope.editMode = false;
                } else {
                    /** TODO: Тут исправить decision на консльт дезижн */
                    if (data.completed === true) {
                        $scope.readMode = true;
                        $scope.editMode = false;

                    } else if (data.completed === false) {
                        $scope.readMode = false;
                        $scope.editMode = true;
                    }
                }

                /** Заполнить модель данными из БД  данными ТМК
                 * Если уже запланированную дату указали то ставим его
                */
                if(data.abonent_side.desired_date != null) {
                    var date = moment.utc(data.abonent_side.desired_date),
                        duration = moment.duration(data.abonent_side.desired_time).asMinutes();

                    date.minute(duration);
                } else {
                    var date = undefined;
                }


                //console.info('date mf', date);
                //console.log('aaaaa', $rootScope.myTMCArray, calEvent.telemed_center_id)

                if ($rootScope.myTMCArray.indexOf(calEvent.telemed_center_id) != -1) {
                    /* Если исходящий */

                    $scope.newRequest = {
                        id: data.abonent_side.request_id,
                        event_type: calEvent.type,
                        consult_decision: calEvent.consult_decision,
                        decision: calEvent.decision,
                        completed: calEvent.completed,
                        canceled: calEvent.canceled,
                        request_uid: [data['uid_code'], data['uid_year'], data['uid_id']].join('-'),
                        created_time: moment(data.request.created_at, 'YYYY.MM.DD HH:mm:ss').format('DD.MM.YYYY'),
                        date:date,
                        doctor_fullname: data.abonent_side.doctor_fullname,
                        doctor_spec: data.abonent_side.doctor_spec,
                        med_care_fac_id: data.med_care_fac_id,
                        desired_consultant: data.abonent_side.desired_consultant,
                        consult_type: data.abonent_side.consult_type,
                        medical_profile: data.abonent_side.medical_profile,
                        patient_uid_or_fname: data.abonent_side.patient_uid_or_fname,
                        patient_birthday: data.abonent_side.patient_birthday,
                        patient_gender: data.abonent_side.patient_gender,
                        patient_social_status: parseInt(data.abonent_side.patient_social_status),
                        patient_address: data.abonent_side.patient_address,
                        patient_questions: data.abonent_side.patient_questions,
                        patient_indications_for_use_id: data.abonent_side.patient_indications_for_use_id,
                        patient_indications_for_use_other: data.abonent_side.patient_indications_for_use_other,
                        coordinator_phone: data.request.telemed_center.coordinator_phone,
                        participants: data.request.telemed_event_participants.map(function (object) {
                            return {
                                id: object.telemed_center.id,
                                name: object.telemed_center.name,
                                mcf_name: object.telemed_center.med_care_fac.name,
                            };
                        }),
                        telemed_center_id: data.abonent_side.telemed_center_id,
                        coordinator_fullname: data.request.telemed_center.coordinator_fullname

                    };

                    template = 'abonent-side-tpl';
                    controller = 'CreateEventController';

                } else {
                    /*Если входящий*/
                    var keys =  Object.keys(data.consultant_side);

                    $scope.abonentSideModel = {
                        id: data.abonent_side.request_id,
                        event_type: calEvent.type,
                        request_uid: [data['uid_code'], data['uid_year'], data['uid_id']].join('-'),
                        created_time: moment(data.request.created_at, 'YYYY.MM.DD HH:mm:ss').format('DD.MM.YYYY'),
                        date: date,
                        doctor_fullname: data.abonent_side.doctor_fullname,
                        doctor_spec: data.abonent_side.doctor_spec,
                        med_care_fac_id: data.med_care_fac_id,
                        desired_consultant: data.abonent_side.desired_consultant,
                        consult_type: data.abonent_side.consult_type,
                        medical_profile: data.abonent_side.medical_profile,
                        patient_uid_or_fname: data.abonent_side.patient_uid_or_fname,
                        patient_birthday: data.abonent_side.patient_birthday,
                        patient_gender: data.abonent_side.patient_gender,
                        patient_social_status: data.abonent_side.patient_social_status,
                        patient_address: data.abonent_side.patient_address,
                        patient_questions: data.abonent_side.patient_questions,
                        patient_indications_for_use_id: data.abonent_side.patient_indications_for_use_id,
                        patient_indications_for_use_other: data.abonent_side.patient_indications_for_use_other,
                        coordinator_fullname: data.request.telemed_center.coordinator_fullname,
                        coordinator_phone: data.request.telemed_center.coordinator_phone,
                        telemed_center_id: data.abonent_side.telemed_center_id,
                        participants: data.request.telemed_event_participants.map(function (object) {
                            return {
                                id: object.telemed_center.id,
                                name: object.telemed_center.name,
                                mcf_name: object.telemed_center.med_care_fac.name,
                            };
                        }),

                    };
                    // Consultant side variables

                    if( keys.length > 0) {
                        $scope.newRequest = {
                            event_type: calEvent.type,
                            consult_decision: calEvent.consult_decision,
                            decision: calEvent.decision,
                            completed: calEvent.completed,
                            canceled: calEvent.canceled,
                            id: data.abonent_side.request_id,
                            comment: data.comment,
                            appointed_person_id: data.consultant_side.appointed_person_id == null ? data.consultant_side.appointed_person : data.consultant_side.appointed_person_id,
                            responsible_person_fullname: (data.consultant_side !== null) ? data.consultant_side.responsible_person_fullname : '',
                            planned_date: (data.consultant_side !== null) ? data.consultant_side.planned_date : null,
                            doctor_id: (data.consultant_side !== null) ? data.consultant_side.doctor_id : '',
                            telemed_center_id: data.telemed_center_id,
                            request_id: data.request_id,
                        };

                    }





                    API.getConsultPartDetails(calEvent.event_id).then(function (data) {
                        $scope.tmcUsers.splice(0,$scope.tmcUsers.length)
                        $scope.tmcUsers.push(data.users);
                        $scope.newRequest.coordinator_fullname = data.coordinator_fullname;
                        $scope.eventFiles = data.consult_side_files;


                    });
                    template = 'consultation-side-tpl';
                    controller = 'ConsultantPartController';

                }

            } else {
                console.info('не консультация', calEvent)
                /* Если не консультация */
                if (calEvent.direction == 'undefined') {
                    $scope.readMode = true;
                } else {
                    $scope.readMode = false;

                }

                /** Заполнить модель данными из БД  данными семинара или совещания */
                $scope.newRequest = {
                    id: data.id,
                    event_type: calEvent.type,
                    completed: calEvent.completed,
                    canceled: calEvent.canceled,
                    decision: calEvent.decision,
                    part_decision: calEvent.part_decision,
                    telemed_center_id: data.telemed_center_id,
                    participants: data.telemed_event_participants.map(function (object) {

                        return {
                            id: object.telemed_center.id,
                            name: object.telemed_center.name,
                            mcf_name: object.telemed_center.med_care_fac.name,
                        };


                    }),
                    date: moment(data.start_date).toDate(),
                    importance: data.importance,
                    title: data.title,

                };
                if( calEvent.direction == 'in' ){
                    template = 'event-view-dialog';
                    $scope.newRequest.initiator = $scope.getUserFName(data.user)
                } else {
                    template = 'event-create-dialog';
                }

                controller = 'CreateEventController' //EventDetailsController
            }

            console.log('opened template', template, 'ctrl: ' + controller);
            ngDialog.open({
                width: '60%',
                template: template,
                className: 'ngdialog-theme-plain',
                controller: controller,
                scope: $scope,
                resolve: {
                    data: function () {
                        return {isNew: false, calendarName: calendarName, calEvent: calEvent}
                    }
                }
            });
        })
    };

    /** При клике по дню календаря */
    $scope.dayClickCallback = function (date, jsEvent, view) {
        /**/

        $(".fc-state-highlight").removeClass("fc-state-highlight");
        $("td[data-date="+date.format('YYYY-MM-DD')+"]").addClass("fc-state-highlight");

        //console.log('jsEvent', jsEvent)


        var calendarName = view.el.closest('[ui-calendar]').attr(('calendar'));

        //console.log('calendarName', calendarName)
        console.log('view', view)
        var dayTypeViews = ['basicDay', 'agendaDay', 'listDay'];
        // Если показывается день

            if (date.get('hour') == 0) {
                date.set({'hour': 18})
            }


        console.log('click date', date)
        //console.log('calendar', view.calendar.getCalendar());
        if (date < moment()) {
            ngDialog.openConfirm({
                template: 'warning-dialog',
                className: 'ngdialog-theme-plain',
                data: {message: 'Вы не можете создавать заявки в прошлом'}
            })


        } else {
            var dateBefore = date.clone();
            dateBefore.startOf('day');
            var dateAfter = date.clone();
            dateAfter.endOf("day");


            var dayEvents = uiCalendarConfig.calendars['myCalendar'].fullCalendar('clientEvents', function (event) {

                if (event.start.isBetween(dateBefore, dateAfter)) {
                    return true;
                }
                return false;
            });

            ngDialog.open({
                template: 'day-dialog',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                controller: 'dayClickController',

                resolve: {
                    data: function () {
                        return {date: date, view: view, events: dayEvents, calendarName: calendarName}
                    }
                }

            });
        }

    };

    /** Изменить View: Переключаться между показом меяца, дня, недели */
    $scope.setView = function (viewName, $event) {
        var el = $event.currentTarget;

        $(el).siblings().removeClass('active');
        $(el).addClass('active');
        $('#calendar').fullCalendar('option', 'weekends', [1, 3, 5]);


        angular.forEach(uiCalendarConfig.calendars, function (value, key) {
            if (key !== 'miniCalendar') {
                var _cal = uiCalendarConfig.calendars[key];

                var currentDate =   _cal.fullCalendar('getDate');


                if (viewName == 'workingWeek') {
                    viewName = 'agendaWeek';
                    _cal.fullCalendar('option', 'hiddenDays', [6, 0]);
                    _cal.fullCalendar('changeView', viewName);

                } else  {
                    _cal.fullCalendar('option', 'hiddenDays', []);
                    _cal.fullCalendar('changeView', viewName);
                }
                // Показывать первый день месяца или первую неделю
                _cal.fullCalendar('gotoDate', currentDate.startOf('month'));
                /**********/
                if (viewName == 'month') {
                    _cal.fullCalendar('option', 'height', 'auto');
                } else {
                    var calendarHeight = window.innerHeight - (48 + 34);



                    _cal.fullCalendar('option', 'height', calendarHeight);



                }

            }


        });






    };

    $scope.setInterval = function (interval, $event) {
        $rootScope.calendarSlotsDuration = interval;
        var el = $event.currentTarget;
        $(el).siblings().removeClass('active');
        $(el).addClass('active')


        angular.forEach(uiCalendarConfig.calendars, function (value, key) {
            if (key !== 'miniCalendar') {
                uiCalendarConfig.calendars[key].fullCalendar('option', 'slotDuration', interval);
            }

        });


    };


    $scope.newEventPopup = function (eventType, dayDetails, calendarName) {
        /* DayDetails Отправляется при клике ячейки дня */
        if (dayDetails !== undefined) {
            $scope.newRequest = {
                participants: [],
                event_type: eventType,
                date: dayDetails.date.toDate(),
                patient_birthday: null
            };

            $scope.readMode = false;
            $scope.editMode = false;
        } else {
            $scope.newRequest = {
                participants: [],
                event_type: eventType,
                patient_birthday: null
            };
            $scope.editMode = true;
            $scope.readMode = true;
        }

        if (calendarName === undefined) {
            $scope.readMode = false;
            $scope.editMode = false;
        }


        var template = ( eventType == 'consultation' ) ? 'abonent-side-tpl' : 'event-create-dialog';
        ngDialog.open({
            width: '60%',
            template: template,
            className: 'ngdialog-theme-plain',
            controller: 'CreateEventController',
            scope: $scope,
            rootScope: $rootScope,
            resolve: {
                data: function () {
                    return {isNew: true, calendarName: calendarName}
                }
            }
        });
    }


    $scope.before;
    $scope.lastChanged = [];
    $scope.currentmonth;
    $scope.currentyear;
    /** Менять вид всех календарей когда меняеют один */



    $scope.changeViewForAllCalendars = function (view, calendarName) {

            if($rootScope.SelectedTMC !== undefined)
            {

                console.log("dede");
                var diff = view.end.diff(view.start, 'days');
                $scope.centerDate = view.start.clone().add(parseInt(diff / 2), 'days');

                $scope.config.currentMonth = $scope.centerDate.format('M');
                $scope.config.currentYear = $scope.centerDate.format('YYYY');
                $scope.currentmonth = $scope.config.currentMonth;
                $scope.currentyear = $scope.config.currentYear;


                var deferred = $q.defer();
                var promise = deferred.promise;
                promise.then(function () {

                    angular.forEach(uiCalendarConfig.calendars, function (value, key) {

                        if (key !== 'miniCalendar') {
                            if (key == 'myCalendar') {
                                var params = {
                                    my_calendar: 1,
                                    telemed_id: $rootScope.SelectedTMC
                                };

                            } else {
                                var calEl = view.el.parent().parent();
                                /* var tmc_id = calEl.data('id');*/
                                var tmc_id = key.match(/calendar(\d+)/i);

                                var params = {
                                    tmc_id: tmc_id[1]
                                };
                            }


                            $scope.before = key;
                            console.info('HERERERE', params)
                            $scope.getEventsToCalendar(key, params);
                            $scope.lastChanged.push(key);
                            uiCalendarConfig.calendars[key].fullCalendar('gotoDate', $scope.centerDate);

                        }
                        else {

                        }

                    })

                }).then(function () {

                    $scope.lastChanged = [];

                });
                deferred.resolve();

            }


        console.log($rootScope.SelectedTMC);




    }


    $scope.changeViewSimple = function (view, calendarName) {


        var diff = view.end.diff(view.start, 'days');
        var centerDate = view.start.clone().add(parseInt(diff / 2), 'days');

        $scope.config.currentMonth = centerDate.format('M');
        $scope.config.currentYear = centerDate.format('YYYY');


        angular.forEach(uiCalendarConfig.calendars, function (value, key) {


            if (key == 'myCalendar') {

                var params = {
                    my_calendar: 1,
                    telemed_id: $rootScope.SelectedTMC
                };
            } else {
                var calEl = view.el.parent().parent();
                var tmc_id = calEl.data('id');
                console.log('currentCalName', calendarName)
                var params = {
                    tmc_id: tmc_id
                };
            }

            if (key !== 'miniCalendar') {
                console.log('calendar key', key)
                $scope.getEventsToCalendar(key, params);

                uiCalendarConfig.calendars[key].fullCalendar('gotoDate', centerDate);


            }


        });


    }

    $scope.miniConfig = {
        calendar: {
            lang: 'ru',
            contentHeight: 'auto',
            editable: true,
            maxEvents: 1,
            allDaySlot: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            timezone: false,
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                dow: [1, 2, 3, 4, 5], //

                start: '08:00', // a start time (10am in this example)
                end: '18:00', // an end time (6pm in this example)
            },
            navLinks: true,


            viewRender: function (view, element) {
                var calEl = view.el.parent().parent();
                var currentCalName = calEl.attr('calendar');


                if ($scope.before != currentCalName && $scope.title != view.title) {
                    $scope.title = view.title;
                    $scope.changeViewForAllCalendars(view, currentCalName);
                }

            },
            dayClick: function (date, cell) {
                //console.log(date)
                if (uiCalendarConfig.calendars['myCalendar'] !== undefined) {
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', date)
                }
            }
        }
    };
    $scope.wait = true;
    $scope.title = "";
    /* Calendar defaults */
    $scope.uiConfig = {
        calendar: {
            contentHeight: 658,
            customButtons: {
                close: {
                    text: '×',
                    click: function () {
                        return destroyCalendar($(this).offsetParent().parent().attr('calendar'))
                    }
                },
                /*toggle: {
                 text: '▢',
                 click: function () {
                 return $(this).offsetParent().parent().toggleClass('minified');
                 }
                 },*/

            },
            clickMode: 'dblclick',
            lang: 'ru',
            height: 'auto',
            editable: true,
            header: {
                left: 'prev',
                center: 'title, close, toggle',
                right: 'next'
            },
            allDaySlot: false,
            timezone: false ,
            slotDuration: $rootScope.calendarSlotsDuration,

            scrollTime: '08:00:00',
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                dow: [1, 2, 3, 4, 5], //

                start: '08:00', // a start time (10am in this example)
                end: '18:00', // an end time (6pm in this example)
            },
            //minTime: '08:00:00',
            //maxTime: '18:30:00',
            eventClick: $scope.showEventDetails,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            dayDblClick: $scope.dayClickCallback,
            loading: function () {
                
            },
            viewRender: function (view, element) {

                var calEl = view.el.parent().parent();
                var currentCalName = calEl.attr('calendar');

                if (calEl.find('.fc-caption').length < 1) {
                    var caption = jQuery('<div/>', {
                        'class': 'fc-caption',
                        'text': calEl.data('text')
                    });
                    calEl.prepend(caption);
                }

                    if (view.name == "month") {

                        var count = Object.keys(uiCalendarConfig.calendars).length;

                        if ($scope.before != currentCalName && $scope.title != view.title) {
                            $scope.title = view.title;

                            $scope.changeViewForAllCalendars(view, currentCalName);
                        }
                        setTimeout(function () {
                            $scope.before = "";
                        }, 100);
                    } else {

                        var datesRange = {
                            from: $scope.centerDate.clone().subtract(2, 'weeks'),
                            to: $scope.centerDate.clone().add(2, 'weeks')
                        };


                        console.info('drange', datesRange);
                        if (!view.start.isBetween(datesRange.from, datesRange.to) && !view.end.isBetween(datesRange.from, datesRange.to)) {
                            $scope.changeViewForAllCalendars(view, currentCalName);
                        }
                    }

            },
            eventRender: function eventRender(event, element, view) {

                if ($scope.config.selectedEventType == 'all') {
                    return true;
                } else if ($scope.config.selectedEventType == event.type) {
                    return true
                } else {
                    return false
                }

            },
            eventAfterAllRender: function (view) {
                if( view.name != 'month') {

                    var $agenda  = view.el;
                    var $scroller = $agenda.find('.fc-body');
                    var $firstTime = $agenda.find("tr[data-time='08:00:00']");

                    var relativeY = $firstTime.offset().top - $scroller.offset().top;

                    setTimeout(function () {
                        $scroller.scrollTop(relativeY);
                    }, 100);

                }
            }
        }
    };


    /*** =========      Запуск скриптов        =============**/
    /**  INIT MY CALENDAR **/
    /*$scope.getEventsToCalendar('myCalendar', {my_calendar: 1});*/
    $rootScope.firstrun = true;

    API.getMyTelemedCenters().then(function (data) {
        $scope.myTelemedCenters = data;
        var first = parseInt($scope.myTelemedCenters[Object.keys($scope.myTelemedCenters)[0]]['id']);

        $rootScope.SelectedTMC = first;
        $scope.initMyCalendar(first);


        $rootScope.myTMCArray = [];

        angular.forEach($scope.myTelemedCenters, function (el, index) {

            this.push(el.id);
        }, $rootScope.myTMCArray);

    })

    $scope.$watch(function () {
        $rootScope.SelectedTMC = $scope.SelectedTMC;

    })

});


app.controller('CreateEventController', function ($scope, $rootScope, data, Upload, ngDialog, API, $filter) {
    console.log('ctrl CreateEventController')

    $scope.showTemplates = false;
    $scope.selectedPattern = null;
    $scope.consult_profiles = [];

    $scope.event = {
        isNew: (data.isNew !== undefined ) ? data.isNew : false,
        calEvent: data.calEvent,
        calendarName: (data.calendarName !== undefined ) ? data.calendarName : 'myCalendar',
    };

    $scope.editMode = ($scope.event.isNew == true) ? $scope.editMode = false : true;

    if($scope.editMode) {
        $scope.cantWriteConclusion = $scope.event.calEvent.start.isBefore(moment()) ? false : true;
    } else {
        $scope.cantWriteConclusion = true;
    }



    $scope.writeConclusion = function () {
        ngDialog.open({
            width: '40%',
            template: 'event-conclusion-dialog',
            className: 'ngdialog-theme-plain',
            controller: 'WriteConclusionController',
            scope: $scope,
            resolve: {
                data: function () {
                    return {calEvent: data.calEvent}
                }
            }
        });
    };



    if (typeof  $scope.newRequest.med_care_fac_id === undefined) {
        $scope.newRequest.med_care_fac_id = mcfs[0].id
    }

    console.log('newRequest on ' + $scope.event.calendarName, $scope.newRequest)


    /** Получить начальные данные для ТМК если создается новый */
    if ($scope.event.isNew === true) {

        console.log('Создается новый ТМК')

        if ($scope.newRequest.event_type == 'consultation') {


            API.getPatterns().then(function (data) {
                $scope.requestPatterns = [];

                angular.forEach(data, function (obj, idx) {
                    obj.patient_birthday = moment(obj.patient_birthday).toDate();
                    this.push(obj);
                },$scope.requestPatterns)

            });

            API.getConsultationDetails('', $rootScope.SelectedTMC).then(function (data) {
                console.log('getConsultationDetails', $rootScope.SelectedTMC, data);
                console.log('ngDialogData', data);
                $scope.mcfs = data.mcfs;
                $scope.social_statuses = data.social_statuses;
                $scope.medical_profiles = data.medical_profiles;



                console.log('$scope.medical_profiles', $scope.medical_profiles)

                var defaultValues = {
                    'created_time': moment().format('DD.MM.YYYY'),
                    'event_type': 'consultation',
                    'request_uid': [data.cons.uid_code, data.cons.uid_year, data.cons.uid_id].join('-'),
                    'request_id': data.cons.request_id,
                    'coordinator_fullname': data.tmc.coordinator_fullname,
                    'coordinator_phone': data.tmc.coordinator_phone,
                    'telemed_center_id': data.tmc.id
                };
                $scope.newRequest = angular.merge($scope.newRequest, defaultValues);

                if ($scope.event.calendarName != 'myCalendar') {
                    var tmc, tmc_id = parseInt($scope.event.calendarName.replace('calendar', ''));

                    /**  Если выбрали календарь другого ТМЦ вставляет его ЛПУ как ЛПУ-Консультанта */
                    API.getParticipants().then(function (telemedCenters) {

                        tmc = $filter('filter')(telemedCenters, {id: tmc_id}, true)[0];

                        var found = $filter('filter')($scope.mcfs, {name: tmc.mcf_name}, true)[0];

                        $scope.newRequest.med_care_fac_id = found.id;
                    })

                }
            })
        } else {


            API.getParticipants().then(function (data) {
                $scope.participantsArray = data;

                /**  Если выбрали календарь другого ТМЦ в совещании или семинаре, то этот ТМЦ добавляется в список участников автоматом */
                if ($scope.event.calendarName != 'myCalendar') {

                    var tmc_id = parseInt($scope.event.calendarName.replace('calendar', ''));
                    var found = $filter('filter')($scope.participantsArray, {id: tmc_id}, true)[0];

                    $scope.newRequest.participants.push(found);
                }

            })


        }
    } else {
        /** Можно ли писать заключение*/
        /** Если не создание нового */

        console.log('date test', $scope.event.calEvent.start, $scope.cantWriteConclusion);

        API.getConsultationDetails('short', $rootScope.SelectedTMC).then(function (data) {
            console.log('getConsultationDetails data', data)
            $scope.mcfs = data.mcfs;
            $scope.social_statuses = data.social_statuses;
            $scope.medical_profiles = data.medical_profiles ;
            
            console.log('РЕДАКТИРУЕТСЯ ТМК', $scope.newRequest);
        })

        API.getParticipants().then(function (data) {
            $scope.participantsArray = data;
        })
    }

    if (angular.isDefined($scope.event.calEvent)) {

        API.getFiles($scope.event.calEvent.event_id).then(function (data) {
            $scope.eventFiles = data;
        });
    }


    /** Определение функций  */

    $scope.deleteFile = function (file_pivot_obj) {
        $scope.eventFiles[$scope.eventFiles.indexOf(file_pivot_obj)].deleted = true;
    };
    $scope.undeleteFile = function (file_pivot_obj) {
        $scope.eventFiles[$scope.eventFiles.indexOf(file_pivot_obj)].deleted = false;
    };
    $scope.MakeFieldsEditable = function () {
        $scope.editMode = false;
        console.log('edit mode', $scope.editMode)
    }
    
    /** Load from pattern */
    $scope.applyRequestPattern = function () {
        var found = $filter('filter')($scope.requestPatterns, {id: $scope.selectedPattern});
        if( found.length == 1 ) {
            var pattern = found[0];
            pattern.created_at = null;
            pattern.desired_time = null;
            pattern.updated_at = null;
            pattern.date = moment().toDate();
            $scope.newRequest = angular.merge($scope.newRequest, pattern);
        }
    };


})

/** Контроллер Модального окна "Часть лпу консультанта"  */
app.controller('ConsultantPartController', function ($scope, $rootScope, data, Upload, ngDialog, API, $q, uiCalendarConfig, ngDialog) {

    API.getConsultationDetails('', $rootScope.SelectedTMC).then(function (data) {
         console.log('getConsultationDetails data', data)
        $scope.social_statuses = data.social_statuses;
 
        $scope.medical_profiles = data.medical_profiles ;
        console.log('АБОНЕНТСКАЯ ЧАСТЬ', $scope.abonentSideModel);
        //console.log('РЕДАКТИРУЕТСЯ ТМК', $scope.newRequest);
    });


  
    API.getAllTelemedCenters().then(function (data) {
        $scope.telemedCenters = data;
    
    });
 
    API.getAllMedCareFacs().then(function (data) {
        $scope.allMcfs = data;
        console.info('getAllMedCareFacs ', data)
    });


    API.getConsultationTypes().then(function (data) {
        $scope.consult_types = data;
       // console.info('consult_types ', data)
    });;

    $scope.activeTab = 0;
    $scope.editMode = ($scope.editMode !== undefined) ? $scope.editMode : false;

    $scope.event = {
        isNew: (data.isNew !== undefined ) ? data.isNew : false,
        calEvent: data.calEvent,
        calendarName: data.calendarName
    };

    $scope.cantWriteConclusion = $scope.event.calEvent.start.isBefore(moment()) ? false : true;

    $scope.writeConclusion = function () {
        ngDialog.open({
            width: '40%',
            template: 'event-conclusion-dialog',
            className: 'ngdialog-theme-plain',
            controller: 'WriteConclusionController',
            scope: $scope,
            resolve: {
                data: function () {
                    return {calEvent: data.calEvent}
                }
            }
        });
    };


    /** Можно ли писать заключение*/


    console.log('date test', $scope.event.calEvent.start, $scope.cantWriteConclusion)



    if ($scope.readMode == true) {
        $scope.editMode = true;
    }
    $scope.MakeFieldsEditable = function () {

        $scope.editMode = false;
        console.log('edit mode', $scope.editMode)
    };
    console.log('isnew', $scope.event.isNew, 'calEvent', $scope.event.calEvent)
    console.log('consultant side ' + $scope.event.calendarName, $scope.newRequest);
    API.getDoctors($rootScope.SelectedTMC).then(function (data) {
        $scope.doctorsArray = data;
    });


    $scope.changeTab = function (val) {
        $scope.activeTab = val;
    }
});


app.controller('dayClickController', function ($scope, data, API, $q, uiCalendarConfig, ngDialog) {

    $scope.eventFiles = null;
    $scope.eventType = null;
    $scope.date = data.date;

    console.log('dayClickController data', data);


    $scope.selectEventType = function (event_type) {
        $scope.eventType = event_type;
        $scope.closeThisDialog()
        $scope.newEventPopup(event_type, data, data.calendarName);
    }

});

app.controller('DeliveryGroupController', function ($scope, data, API, ngDialog) {
    $scope.newDeliveryGroup = {saveMode: data.saveMode, success: false, successMessage: ''};
    console.log('init ctrl')
    $scope.participants = data.participants;
    $scope.deliveryGroupTemplates = [];
    $scope.participantsArray = data.participantsArray
    angular.forEach(data.templates, function (el, index) {
        this.push({
            id: el.id,
            name: el.name,
            tmcs: participantsFilter($scope.participantsArray, (JSON.parse(el.telemed_center_ids))),


        })
    }, $scope.deliveryGroupTemplates);

    /** Cохранить группу рассылки в БД */
    $scope.saveDeliveryPattern = function (telemed_center_ids, name) {
        var obj = {
            telemed_center_ids: JSON.stringify(telemed_center_ids),
            name: name
        };
        console.log('dasdadasd dasdasdasdasdashdasid', obj)
        API.addDeliveryGroup(obj).then(function (resp) {
            console.log(resp)
            $scope.newDeliveryGroup.success = true;
            $scope.newDeliveryGroup.successMessage = resp.message;

        });
    }


    $scope.deleteDeliveryPattern = function (pattern) {
        API.deleteDeliveryGroup(pattern.id);
        var index = $scope.deliveryGroupTemplates.indexOf($scope.deliveryGroupTemplates);
        console.log('idx', index)
        $scope.deliveryGroupTemplates.splice(index, 1)

    }


})

app.controller('WriteConclusionController', function ($scope, data, API, $q, ngDialog, Upload) {
    $scope.Conclusion = {
        minutes: null,
        seconds: null,
        comment: null,
        call_count: null
    };

    API.getConclusion(data.calEvent.event_id).then(function (respData) {
        $scope.Conclusion = respData.conclusion;
        console.log('conclusion', respData)
    });
    console.log(data.calEvent)

    $scope.submitConclusion = function (files, Conclusion, eventFiles) {
        Conclusion.minutes = ( Conclusion.minutes == 0 ) ? null : Conclusion.minutes;
        Conclusion.seconds = ( Conclusion.seconds == 0 ) ? null : Conclusion.seconds;
        Conclusion.call_count = ( Conclusion.call_count == 0 ) ? null : Conclusion.call_count;
        console.log($scope.Conclusion);
        console.log(files);
        var url = '/api/create-conclusion/' + data.calEvent.event_id;

        var obj = Conclusion;
        obj.files = files;
        Upload.upload({
            url: url,
            data: obj,
            cache: true,
            method: 'POST'
        }).then(function (resp) {
            delete $scope.Conclusion.errors;
            console.log(resp);

            if (resp.status == 200) {
                ngDialog.closeAll();
                ngDialog.openConfirm({
                    template: 'warning-dialog',
                    className: 'ngdialog-theme-plain',
                    data: {message: resp.data.message}
                });
            }
        }, function (resp) {
            console.log('resp: ', resp);
            if (resp.status == 422 || resp.status == 401) {
                console.log('Error status: ' + resp.status);

                $scope.Conclusion.errors = resp.data.error;

                console.log('errrrros', $scope.Conclusion.errors)
            } else {
                alert('Что то пошло не так');
            }

        }, function (evt) {

            if (typeof evt.config.data.file !== 'undefined') {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            }

        });

    }
})


/* Нажатие н на кнопку ОБЗОР */
app.directive('uploadfile', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {

            element.bind('click', function (e) {
                angular.element('#files').trigger('click');
            });
        }
    };
})
    .directive('inputAddon', function () {
        var showDropdown = function (element) {
            var event;
            event = document.createEvent('MouseEvents');
            event.initMouseEvent('mousedown', true, true, window);
            element.dispatchEvent(event);
        };
    return {
        restrict: 'A',
        link: function (scope, element) {

            element.bind('click', function (e) {
                var p = element.parent();
                var s = p.find('select');
                console.info('parent', p, s)
                showDropdown(s[0])
            });
        }
    };
})
    .directive('ngDropdownMultiselectDisabled', function () {
    return {
        restrict: 'A',
        controller: function ($scope, $element, $attrs) {
            var $btn;
            $btn = $element.find('button');
            return $scope.$watch($attrs.ngDropdownMultiselectDisabled, function (newVal) {
                return $btn.attr('disabled', newVal);
            });
        }
    };
});
;

app.filter('groupBy', ['$parse', 'filterWatcher', function ($parse, filterWatcher) {
    return function (collection, property) {

        if (!angular.isObject(collection) || angular.isUndefined(property)) {
            return collection;
        }

        return filterWatcher.isMemoized('groupBy', arguments) ||
            filterWatcher.memoize('groupBy', arguments, this,
                _groupBy(collection, $parse(property)));

        /**
         * groupBy function
         * @param collection
         * @param getter
         * @returns {{}}
         */
        function _groupBy(collection, getter) {
            var result = {};
            var prop;

            angular.forEach(collection, function (elm) {
                prop = getter(elm);

                if (!result[prop]) {
                    result[prop] = [];
                }
                result[prop].push(elm);
            });
            return result;
        }
    }
}]);

app.provider('filterWatcher', function () {

    this.$get = ['$window', '$rootScope', function ($window, $rootScope) {

        /**
         * Cache storing
         * @type {Object}
         */
        var $$cache = {};

        /**
         * Scope listeners container
         * scope.$destroy => remove all cache keys
         * bind to current scope.
         * @type {Object}
         */
        var $$listeners = {};

        /**
         * $timeout without triggering the digest cycle
         * @type {function}
         */
        var $$timeout = $window.setTimeout;

        /**
         * @description
         * get `HashKey` string based on the given arguments.
         * @param fName
         * @param args
         * @returns {string}
         */
        function getHashKey(fName, args) {
            function replacerFactory() {
                var cache = [];
                return function (key, val) {
                    if (angular.isObject(val) && !isNull(val)) {
                        if (~cache.indexOf(val)) return '[Circular]';
                        cache.push(val)
                    }
                    if ($window == val) return '$WINDOW';
                    if ($window.document == val) return '$DOCUMENT';
                    if (isScope(val)) return '$SCOPE';
                    return val;
                }
            }

            return [fName, JSON.stringify(args, replacerFactory())]
                .join('#')
                .replace(/"/g, '');
        }

        /**
         * @description
         * fir on $scope.$destroy,
         * remove cache based scope from `$$cache`,
         * and remove itself from `$$listeners`
         * @param event
         */
        function removeCache(event) {
            var id = event.targetScope.$id;
            angular.forEach($$listeners[id], function (key) {
                delete $$cache[key];
            });
            delete $$listeners[id];
        }

        /**
         * @description
         * for angular version that greater than v.1.3.0
         * it clear cache when the digest cycle is end.
         */
        function cleanStateless() {
            $$timeout(function () {
                if (!$rootScope.$$phase)
                    $$cache = {};
            }, 2000);
        }

        /**
         * @description
         * Store hashKeys in $$listeners container
         * on scope.$destroy, remove them all(bind an event).
         * @param scope
         * @param hashKey
         * @returns {*}
         */
        function addListener(scope, hashKey) {
            var id = scope.$id;
            if (angular.isUndefined($$listeners[id])) {
                scope.$on('$destroy', removeCache);
                $$listeners[id] = [];
            }
            return $$listeners[id].push(hashKey);
        }

        /**
         * @description
         * return the `cacheKey` or undefined.
         * @param filterName
         * @param args
         * @returns {*}
         */
        function $$isMemoized(filterName, args) {
            var hashKey = getHashKey(filterName, args);
            return $$cache[hashKey];
        }

        /**
         * @description
         * store `result` in `$$cache` container, based on the hashKey.
         * add $destroy listener and return result
         * @param filterName
         * @param args
         * @param scope
         * @param result
         * @returns {*}
         */
        function $$memoize(filterName, args, scope, result) {
            var hashKey = getHashKey(filterName, args);
            //store result in `$$cache` container
            $$cache[hashKey] = result;
            // for angular versions that less than 1.3
            // add to `$destroy` listener, a cleaner callback
            if (isScope(scope)) {
                addListener(scope, hashKey);
            } else {
                cleanStateless();
            }
            return result;
        }

        return {
            isMemoized: $$isMemoized,
            memoize: $$memoize
        }
    }];
});

/** Вычисляет сколько слотов в timeline **/
function evaluateSlotsCount(interval, rows) {
    var rows = typeof rows != 'undefined' ? rows : 24;
    console.log('interval', interval, rows)
    switch (interval) {
        case '00:10:00' :
            return rows * 6;
            break;
        case  '00:15:00' :
            return rows * 3;
            break
        case '00:30:00' :
            return rows * 2;
            break
        default:
            return rows;
    }
}