var classPrefix = 'event-';
app.factory('API', function ($http, $log, $q) {
    return {
        getMedCareFacs: function (type) {
            /** Получить список ТМЦ вложенные в их родители (ЛПУ) */
            var deferred = $q.defer();
            var type = (type !== undefined ) ? type : '';
            $http.get('/api/telemed-centers/?type=' + type).success(function (data) {

                if (type == 'short') {
                    deferred.resolve(data);
                } else {
                    var telemed_centers = [];
                    angular.forEach(data,
                        function (value) {
                            //console.log(value)
                            if (value.telemed_centers.length < 1) {
                                value.telemed_centers.push({'name': 'Остутствует', disabled: true});
                            }
                            telemed_centers.push(value)

                        })

                    deferred.resolve({
                        id: 'all',
                        name: 'Общие календари',
                        telemed_centers: telemed_centers,
                        disabled: true
                    });
                }

            }).error(function (msg, code) {
                deferred.reject(msg);
                $log.error(code);
                alert('Ошибка при загрузке данных. Календарь непавильно функционирует. Обновите страницу')
            });
            return deferred.promise;
        },
        getParticipants: function () {
            /* Получить спсиок участников */
            var deferred = $q.defer();
            $http.get('/api/telemed-centers/list', {cache: true}).success(function (data) {
                deferred.resolve(data);

            }).error(function (msg, code) {
                deferred.reject(msg);
                $log.error(code);
            });
            return deferred.promise;
        },
        getEvents: function (params, config, CalendarID) {
            /* Получить список  событии */
            var deferred = $q.defer();
            if (params === undefined) {
                params = {
                    my_calendar: 1
                };
            }

            params.month = config.currentMonth;
            params.year = config.currentYear;
            $http.get('/api/event-requests', {
                params: params
            }).success(function (data) {

                if (isInt(params.tmc_id)) {
                    var all_events = convertToNormalEvents(data, 'undefined', CalendarID);

                } else {
                    var all_events = convertToNormalEvents(data.in, 'in', CalendarID).concat(convertToNormalEvents(data.out, 'out', CalendarID));
                }

                /*console.log('all_events', all_events)*/
                var eventSource = {
                    id: params.tmc_id,
                    events: all_events
                };

                deferred.resolve(eventSource);


            }).error(function (msg, code) {

                if (code == 401) {
                    alert('Ваша сессия устарела. Войдите на сайте еще раз и поставьте галочку запомнить меня чтобы такое в дальнейшем не случалось');
                }
                deferred.reject(code);


                $log.error(code);
            });
            return deferred.promise;
        },
        
        getEventDetails: function (id, event_type) {
            var deferred = $q.defer();
            event_type = ( event_type !== undefined) ? event_type : 'event';
            $http.get('/api/event-requests/' + id + '/' + event_type, {cache: true}).success(function (data) {
                deferred.resolve(data);

            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        cancelMyEvent: function (id) {
            /** Отмена заявки */
            console.log('data', id)
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: '/api/event-requests/cancel/'+ id,

            }).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);

                $log.error(code);
            });
            return deferred.promise;
        },
        getMyTelemedCenters: function () {
            var deferred = $q.defer();
            $http.get('/api/gettmcs', {
                cache: true
            }).success(function (data) {
                deferred.resolve(data.tmcs);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getAllTelemedCenters: function () {
            var deferred = $q.defer();
            $http.get('/api/get-all-tmcs', {
                cache: true
            }).success(function (data) {
                deferred.resolve(data.tmcs);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getAllMedCareFacs: function () {
            var deferred = $q.defer();
            $http.get('/api/get-all-mcfs', {
                cache: true
            }).success(function (data) {
                deferred.resolve(data.mcfs);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getConsultationDetails: function (type, telemed_center_id) {
            var type = (typeof  type !== undefined ) ? type : '';
            var deferred = $q.defer();
            $http.get('/api/consultation-details?type=' + type + '&telemed_center_id=' + telemed_center_id).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.error(msg.error);
                alert(msg.error.error + ' Обновите страницу!')
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getConsultPartDetails: function (request_id) {
            
            var deferred = $q.defer();
            
            $http.get('/api/consult-side-details/'+ request_id).success(function (data) {
                
                deferred.resolve(data);
                
            }).error(function (msg, code) {

                deferred.reject(code);
                
                $log.error(code);
                
            });
            return deferred.promise;
        },
        getPatterns: function () {
            var deferred = $q.defer();
            $http.get('/api/get-patterns', {cache: true}).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getConsultationTypes: function () {
            var deferred = $q.defer();
            $http.get('/api/get-consultation-types', {cache: true}).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getDoctors: function (id) {
            var deferred = $q.defer();
            $http.get('/api/doctors/' + id).success(function (data) {

                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getIndForUse: function () {
            var deferred = $q.defer();
            $http.get('/api/ind_for_use', {
                cache: true
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getFiles: function (id) {
            var deferred = $q.defer();
            $http.get('/api/event-files/' + id).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getUser: function () {
            var deferred = $q.defer();
            $http.get('/api/get-user').success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getConclusion: function (id) {
            var deferred = $q.defer();
            $http.get('/api/get-conclusion/' + id).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        getDeliveryGroup: function () {
            var deferred = $q.defer();
            $http.get('/api/get-delivery-group').success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);
                $log.error(code);
            });
            return deferred.promise;
        },
        addDeliveryGroup: function (data) {
            console.log('data', data)
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '/api/add-delivery-group',
                data: data,

            }).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);

                $log.error(code);
            });
            return deferred.promise;
        },
        deleteDeliveryGroup: function (id) {

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '/api/delete-delivery-group/' + id,
            }).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);

                $log.error(code);
            });
            return deferred.promise;
        },
        setInvitationStatus: function (request_id, params) {

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '/api/events/invitation/' + request_id,
                data:  params
            }).success(function (data) {
                console.log(data)
                deferred.resolve(data);
            }).error(function (msg, code) {
                deferred.reject(code);

                $log.error(code);
            });
            return deferred.promise;
        },

    }
});


/** Functions */

function array_flip(trans) {
    var key, tmp_ar = {};

    for (key in trans) {
        if (trans.hasOwnProperty(key)) {
            tmp_ar[trans[key]] = key;
        }
    }

    return tmp_ar;
}

function addClassByDate(date) {

    var dataAttr = getDataAttr(date);
    jQuery(".mini-calendar [data-date='" + dataAttr + "']").addClass("has-event");
}

function getDataAttr(date) {
    // console.log('s', date)
    var textDate = date.year() + "-" + date.format('MM')  + "-" + date.date();
    //console.log('textDate', textDate)
    return textDate;
};


function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
}


function convertToNormalEvents(data, direction, CalendarID) {
    var  events = [];

    angular.forEach(data, function (value) {
        this.push( ObjectToNormalEvent(value, direction) );
    },events);

    return events;
}

function ObjectToNormalEvent(obj, direction, CalendarID) {


    if (obj.event_type == 'consultation') {

        /* Если консультант уже согласовал ставим  start_date как время начала */
        if (obj.consult_decision != 'pending' && obj.start_date != null) {
            var startDate = moment(obj.start_date, 'YYYY-MM-DD HH:mm:ss');
        } else {
            var startDate = moment(obj.desired_date + ' ' + obj.desired_time, 'YYYY-MM-DD HH:mm:ss');

        }
        startDate.seconds(0);
        var endDate = startDate.clone().add(1, 'hours');
        var title = [obj.uid_code, obj.uid_year, obj.uid_id].join('-');


    } else {

        var startDate = moment(obj.start_date, 'YYYY-MM-DD HH:mm:ss');
        var endDate = moment(obj.end_date, 'YYYY-MM-DD HH:mm:ss');
        var title = obj.title;
    }


    if (direction == 'in') {
        var symbolPrefix = '⇩ '
    } else if (direction == 'out') {
        var symbolPrefix = '⇧ ';
    } else {
        var symbolPrefix = '';
    }

    jQuery('.mini-calendar .fc-day-number').each(function (idx, el) {
        addClassByDate(startDate)
    });

    return {
        event_id: obj.id,
        title: symbolPrefix + title,
        importance: obj.importance,
        start: startDate,
        end: endDate,
        stick: true,
        editable: false,
        type: obj.event_type,
        className: classPrefix + obj.event_type,
        timezone: 'local',
        consult_decision: obj.consult_decision,
        decision: obj.decision,
        part_decision: obj.part_decision,
        direction: direction,
        telemed_center_id: obj.telemed_center_id,
        canceled: !!obj.canceled,
        completed: !!obj.completed
    }
}
/** Filter participants */
function participantsFilter(all, selected) {

    array = [];

    angular.forEach(selected, function (p, key) {
        var self = this;
        angular.forEach(all, function (value, key) {
            if (value.id == p.id) {
                self.push(value);
            }
        })
    }, array);

    return array;
}

/**
 * @param value
 * @returns {boolean}
 */
function isNull(value) {
    return value === null;
}
/**
 * @description
 * Test if given object is a Scope instance
 * @param obj
 * @returns {Boolean}
 */
function isScope(obj) {
    return obj && obj.$evalAsync && obj.$watch;
}


/** Equal php in_array
 * Example: alert([1, 2, 3].contains(2)); // => true
 * */
Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}



$(document).ready(function () {
    calcCalendarWrapperWidth()


    calcCalendarWidth();
    window.onresize = function() {

        calcCalendarWidth();
        calcCalendarWrapperWidth();
    };

});

function calcCalendarWrapperWidth(){
    var wrapper = $('#workspace');
    var w = $('.main-side').width() - 250;
    wrapper.width(w);
}
function calcCalendarWidth(){
    var wrapper = $('#workspace-wrapper');
    var calendars = $('#workspace-wrapper .calendar');

    if( calendars.length < 6 ) {
        var newWidth = parseInt( ( wrapper.width() - 250) / calendars.length ) -20;


    } else {
        var newWidth = parseInt( ( wrapper.width() - 250) / 5 ) -20;
    }


    calendars.each(function (key, el) {
        $(el).width(newWidth);
    })

};